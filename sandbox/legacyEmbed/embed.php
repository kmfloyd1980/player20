<?php

function debug($data) {
    $calledFrom = debug_backtrace();
    echo '<pre><strong>' . $calledFrom[0]['file'] . '</strong>';
    echo ' (line <strong>' . $calledFrom[0]['line'] . '</strong>)';
    echo "\n". str_replace('<', '&lt;', str_replace('>', '&gt;', print_r($data, true))) . "\n</pre>\n";
}

// Total stub method assuming the logic for determining whether a tracking group
// should use the "old" embed or P2.0
function isP20Partner() {
	// Actual logic is for LOSERS
	return true;
}

if (isP20Partner() === false) {
	// Do current legacy embed stuff
} else {
	// NEW HOTNESS
	// The template depends only on built-in server vars, basically no processing required
	header('Content-type: application/javascript');
	ob_start();
	include('embed.tpl.php.js');
	ob_end_flush();
}