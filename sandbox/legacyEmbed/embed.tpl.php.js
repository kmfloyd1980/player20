// Inject the P20 lib if necessary
if (typeof(Ndn_Widget) === 'undefined') {
	// Inject!
	var element;
    element = document.createElement("script");
    element.type = 'text/javascript';
    element.src = 'http://dev.assets.newsinc.com.s3.amazonaws.com/philip/p20build/TESTVERSION/js/main.js';
    document.getElementsByTagName('head')[0].appendChild(element);
}

// Push the player param stuff to _n2we
var _nw2e = _nw2e || [];
_nw2e.push({
	containerElementId: '<?=$_REQUEST['parent']?>',
	config: {
		// The exact format of this key is TBD; but basically it'll be some static
		// parameter that indicates to us that we're dealing with a legacy embed
		type: '__legacyEmbed',
		// Here we just pass the request URL in it entirety
		legacyParams: {
			requestUrl: '<?=$_SERVER['REQUEST_URI']?>'
		}
	}
});