<!doctype html>
<?php 

$showPending = 0;

function wrapCode($language, $code) {
	echo '<div class="ndn_code">';
	echo '<pre class="brush: ' . $language . ';">';
	echo str_replace('<', '&lt;', $code);
	echo '</pre>';
	echo '</div>';
}

function wrapInlineCode($code) {
	echo '<span class="ndn_code">' . str_replace('<', '&lt;', $code) . '</span>';
}

?>
<html>
<head>
    <link href="img/favicon.ico" title="Icon" type="image/x-icon" rel="icon" />
    <link href="img/favicon.ico" title="Icon" type="image/x-icon" rel="shortcut icon" />

	<title>NDN Player Suite: Implementation Guide</title>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script type="text/javascript" src="js/lib/jquery/plugins/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="js/lib/jquery/plugins/jquery.validate.js"></script>
	
	<script type="text/javascript" src="js/lib/syntaxhighlighter_3.0.83/scripts/shCore.js"></script>
	<script type="text/javascript" src="js/lib/syntaxhighlighter_3.0.83/scripts/shBrushXml.js"></script>
	<script type="text/javascript" src="js/lib/syntaxhighlighter_3.0.83/scripts/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="js/lib/syntaxhighlighter_3.0.83/styles/shCoreDefault.css"/>
	
	<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.0/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<style>
		/* css for timepicker */
		.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
		.ui-timepicker-div dl { text-align: left; }
		.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
		.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
		.ui-timepicker-div td { font-size: 90%; }
		.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
		
		.ui-timepicker-rtl{ direction: rtl; }
		.ui-timepicker-rtl dl { text-align: right; }
		.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
		
		
		.ui_tpicker_timezone_label,
		.ui_tpicker_minute_label,
		.ui_tpicker_hour_label,
		.ui_tpicker_time_label {
			font-size: 11px;
			line-height: 22px;
		}
		
		/* other stuff */
		body {
			margin: 0;
			padding: 0;
			font-family: Helvetica, Arial;
			font-size: 12px;
			color: #555;
		}
		
		.header {
			width: 100%;
			/* padding-left: 30px; */
			height: 50px;
			background: url('img/background-3a.png') repeat-x 0 -20px;
			border-style: solid;
			border-width: 0 0 1px 0;
			border-color: #333;
		}
		
		.main-container {
			padding: 40px;
		}
		
		.main-container h2 {
			margin: 0;
		}
		
		.section-container {
			width: 100%;
		}
		
		.section-container .client-detail {
			height: 28px;
			position: relative;
			clear: both;
		}
		
		.section-container .client-detail .parameter-label {
			opacity: 0.6;
		}
		
		.section-container .client-detail label {
			float: left;
			width: 180px;
			padding: 6px 0 0 4px;
		}
		
		.section-container .client-detail.parameter-in-use .parameter-label {
			opacity: 1;
		}
		
		.section-container .client-detail.parameter-in-use label {
			font-weight: bold;
		}
		
		.section-container .client-detail label,
		.section-container .client-detail input {
			line-height: 16px;
		}
		
		.section-container .client-detail input {
			width: 180px;
			right: 4px;
			position: absolute;
			/*
			text-align: right;
			*/
		}
		
		a {
			text-decoration: underline;
			color: blue;
		}
		
		a:hover {
			text-decoration: none;
		}
		
		.example .example-content {
			width: 400px;
			overflow-x: scroll;
		}
		
		.section-container .feed-container {
			border-width: 0 0 1px 0;
			border-color: #eee;
			border-style: solid;
			padding: 20px 0 0 0;
		}
		
		.section-container .feed-container .feeds {
			padding: 10px 0 0 10px;
			margin: 0 auto;
		}
		
		.section-container .feed-container .feed-options {
			float: right;
		}
		
		.section-container .feed-container .feed-title {
			font-size: 14px;
			font-weight: bold;
			display: block;
			
			cursor: hand;
			cursor: pointer;
		}
		
		.section-container .feed-container .feed-title:hover {
			color: black;
		}
		
		.section-container .feed-url a:hover {
			background-color: #fafafa;
		}
		
		.section-container .feed-container .feed-subtitle {
			display: block;
			font-size: 11px;
			font-weight: bold;
			margin: 1.6em 0 0.5em 0;
			padding: 0;
		}
		
		.section-container .feed-container .feed-url {
			clear: both;
			width: 100%;
		}
		
		.section-container .feed-url a {
			display: block;
			width: 100%;
			
			font-family: monospace;
			white-space: pre;
			margin: 0 0 1em 0;
			text-decoration: none;
			
			border: 1px dashed #555;
			padding: 4px;
			
			overflow-x: scroll;
			background: #eee;
			color: black;
		}
		
		.section-container .feed-container .feed-parameters {
			width: 370px;
			padding: 20px;
			border: 1px solid #ddd;
		}
		
		.section-container .feed-container .feed-url .missing-required-params {
			border: 1px dashed red;
			color: red;
		}
		
		.section-container .feed-container .feed-parameters input {
			border: 1px solid #ddd;
			text-indent: 2px;
		}
		
		.section-container .feed-container .feed-parameters input.error {
			color: red;
			border: 1px solid red;
		}
		
		.section-container .feed-container .feed-parameters label.error {
			clear: both;
			float: right;
			margin: 0 4px 10px 0;
			color: red; /* #990000; */
			font-style: italic;
			text-align: right;
			font-weight: bold;
		}
		
		.ui_tpicker_timezone select {
			width: 150px;
		}
		
		.rss-tag-descriptions table {
			font-size: 12px;
		}
		
		.rss-tag-descriptions table th {
			background: #eee;
			height: 25px;
			padding: 0 6px;
		}
		
		.rss-tag-descriptions table td {
			border-width: 1px 0 0 0;
			border-style: dashed;
			border-color: #ccc;
			margin: 0 2px 0 0;
		}
		
		.rss-tag-descriptions table .first td {
			border-width: 0;
		}
		
		
		.rss-tag-descriptions table .example {
			width: 400px;
			overflow-x: scroll;
		}
		
		.rss-tag-descriptions table .description {
			width: 400px;
		}
		
		.rss-feed-name {
			white-space: pre;
			color: black;
			text-decoration: underline;
		}
		
		div.ndn_code {
			margin-left: -4px;
		}
		
		span.ndn_code {
			background: #eee;
			color: black;
			font-family: "Consolas", "Bitstream Vera Sans Mono", "Courier New", Courier, monospace;
			white-space: pre;
			margin: 1em 0px;
		}
		
		a {
			color: #095796;
			text-decoration: none;
		}
		
		a:hover {
			text-decoration: underline;
		}
		
		.syntaxhighlighter {
			border-style: dashed;
			border-color: gray;
			border-width: 1px;
			padding: 4px;
		}
		
		h2 {
			margin-top: 10px;
			padding-top: 10px;
			border-width: 1px 0 0 0;
			border-style: dashed;
			border-color: gray;
		}
		
		.configSettings .optional {
			font-style: italic;
			font-weight: bold;
			color: black;
		}
		
		.configSettings li {
			margin: 10px 0;
		}
	</style>
	
	<script type="text/javascript">
		jQuery(function($) {
			SyntaxHighlighter.defaults['toolbar'] = false;
			SyntaxHighlighter.defaults['gutter'] = false;
			SyntaxHighlighter.all();
		});
	</script>
</head>
<body>
	<div class="header">
		<a href="http://www.newsinc.com/" target="_blank" style="width:700px; margin:0 auto;display:block;"><img src="img/ndn-logo.png" style="display:block;" /></a>
	</div>
	
	<div class="main-container" style="width:730px;margin:0 auto;">
		<div class="section-container landing-page-details">
			<h1>NDN Player Suite: Implementation Guide</h1>
			<br /><br />
			
			<h2>Overview</h2>
			<p>
				In order to embed NDN's new player and launcher products, only two things need to be added to a webpage's HTML: 
				<ul>
					<li>the inclusion of one <?=wrapInlineCode("<script>")?> tag</li>
					<li>and the inclusion of as many products as desired on your page using configurable <?=wrapInlineCode("<div>")?> elements</li>
				</ul>
			</p>
			<p>
				The <?=wrapInlineCode("<script>")?> tag needed is placed within the page's <?=wrapInlineCode("<head></head>")?> section and is shown below:
			</p>
			<p>
				<?php wrapCode('xml', '<script type="text/javascript" src="http://launch.newsinc.com/js/embed.js" id="_nw2e-js"></script>'); ?>
			</p>
			<p>
				The HTML attributes of the configurable <?=wrapInlineCode("<div>")?> elements vary from product to product. A list of needed and optional attributes are described below:
				<ul class="configSettings">
					<li><?=wrapInlineCode('class="ndn_embed"')?> - The "ndn_embed" class must be present in order to indicate that this element embeds an NDN product</li>
					<li><?=wrapInlineCode('data-config-widget-id=""')?> - The widget database id provided to a client via our Control Room</li>
					<li><?=wrapInlineCode('data-config-type=""')?> - The type of NDN product being embedded</li>
					<li><?=wrapInlineCode('data-config-tracking-group=""')?> - This setting is required and identifies the distribution partner (this is a <span style="font-weight:bold;">critical</span> config setting for distribution partners as the value of this setting is responsible for tracking all of a distribution partner's earnings)</li>
					<li><?=wrapInlineCode('data-config-playlist-id=""')?> <span class="optional">(optional)</span> - This setting works for both our player and launcher products and specifies a specific playlist to be used for the current product; by providing this optional config setting, the playlist or playlists that would be used by default are overridden by the playlist specified</li>
					<li><?=wrapInlineCode('data-config-video-id=""')?> <span class="optional">(optional)</span> - This setting only works for our player products and specifies a specific video to be played within the product's video player</li>
					<li><?=wrapInlineCode('data-config-site-section=""')?> <span class="optional">(optional)</span> - This is the "sitesection" value used for analytics; if this config setting is not provided, the default sitesection is pulled from our server based on the provided "tracking group" config setting</li>
					
					<?php if ($showPending): ?>
						<li><?=wrapInlineCode('data-config-provider-id=""')?> <span class="optional">(optional)</span> - Specifies a content provider that when paired with the "data-config-provider-story-id" attribute identifies one or more videos to load into the video player to be played first <a href="test.html?env=qa&embedCode=%3Cp%20style%3D%22color%3ABlack%3B%22%3E%3Ci%3E%3Cb%3EConsult%20with%20your%20NDN%20Account%20Manager%20for%20available%20provider%20IDs%20and%20provider%20story%20IDs.%3C%2Fb%3E%3C%2Fi%3E%3C%2Fp%3E%0A%3Cp%3EProvider%20ID%3A%20448%2C%20Provider%20Story%20ID%3A%20619CF73F-05CA-4CF6-8222-7B5EE97C18DE%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%22448%22%20data-config-provider-story-id%3D%22619CF73F-05CA-4CF6-8222-7B5EE97C18DE%22%3E%3C%2Fdiv%3E%0A%3Cbr%20%2F%3E%0A%3Cp%3EProvider%20ID%3A%201334%2C%20Provider%20Story%20ID%3A%206890C4C0-938E-4D41-9B13-415CBC61486D%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%221334%22%20data-config-provider-story-id%3D%226890C4C0-938E-4D41-9B13-415CBC61486D%22%3E%3C%2Fdiv%3E%0A%3Cbr%20%2F%3E%0A%3Cp%3EProvider%20ID%3A%201334%2C%20Provider%20Story%20ID%3A%20649719CA-7526-4F41-975C-5C4E3072E36E%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%221334%22%20data-config-provider-story-id%3D%22649719CA-7526-4F41-975C-5C4E3072E36E%22%3E%3C%2Fdiv%3E" target="_blank">(see examples)</a></li>
						<li><?=wrapInlineCode('data-config-provider-story-id=""')?> <span class="optional">(optional)</span> - Specifies a content provider's story that when paired with the appropriate "data-config-provider-id" attribute identifies one or more videos to load into the video player to be played first. The content provider (specified by the "data-config-provider-id" attribute) may tag videos using a "story ID" in its metadata when the content is uploaded to NDN; those story IDs may then be available via an RSS feed offered by the content provider. <a href="test.html?env=qa&embedCode=%3Cp%20style%3D%22color%3ABlack%3B%22%3E%3Ci%3E%3Cb%3EConsult%20with%20your%20NDN%20Account%20Manager%20for%20available%20provider%20IDs%20and%20provider%20story%20IDs.%3C%2Fb%3E%3C%2Fi%3E%3C%2Fp%3E%0A%3Cp%3EProvider%20ID%3A%20448%2C%20Provider%20Story%20ID%3A%20619CF73F-05CA-4CF6-8222-7B5EE97C18DE%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%22448%22%20data-config-provider-story-id%3D%22619CF73F-05CA-4CF6-8222-7B5EE97C18DE%22%3E%3C%2Fdiv%3E%0A%3Cbr%20%2F%3E%0A%3Cp%3EProvider%20ID%3A%201334%2C%20Provider%20Story%20ID%3A%206890C4C0-938E-4D41-9B13-415CBC61486D%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%221334%22%20data-config-provider-story-id%3D%226890C4C0-938E-4D41-9B13-415CBC61486D%22%3E%3C%2Fdiv%3E%0A%3Cbr%20%2F%3E%0A%3Cp%3EProvider%20ID%3A%201334%2C%20Provider%20Story%20ID%3A%20649719CA-7526-4F41-975C-5C4E3072E36E%3A%3C%2Fp%3E%0A%3Cdiv%20class%3D%22ndn_embed%22%20style%3D%22width%3A590px%3Bheight%3A332px%3B%22%20data-config-widget-id%3D%224%22%20data-config-tracking-group%3D%2233333%22%20data-config-type%3D%22VideoPlayer%2FSingle%22%20data-config-provider-id%3D%221334%22%20data-config-provider-story-id%3D%22649719CA-7526-4F41-975C-5C4E3072E36E%22%3E%3C%2Fdiv%3E" target="_blank">(see examples)</a></li>
					<?php endif; ?>
					
					<li><?=wrapInlineCode('data-config-distributor-id=""')?> <span class="optional">(optional)</span> - The distributor database id provided to a partner that distributes our videos on their website. Using this config setting enables the use of Perfect Pixel, which has been integrated into the NDN Player Suite <a href="perfect-pixel.html">(read more)</a>.</li>
					
					<?php if ($showPending): ?>
						<li><?=wrapInlineCode('data-config-accepts-config-from-url=""')?> <span class="optional">(optional)</span> - Using this config setting allows for additional config settings to be pulled directly from the query string of the current webpage's URL (<a href="demos/landing-page.html" target="_blank">see demo here</a>)</li>
					<?php endif; ?>
					
					<li><?=wrapInlineCode('data-config-width=""')?> <span class="optional">(optional)</span> - The width of the configurable <?=wrapInlineCode('<div>')?> element measured in pixels; may contain <?=wrapInlineCode('"h"')?> variable to dynamically calculate the width based on the configurable <?=wrapInlineCode('<div>')?> element's height. For example, setting this property's value to <?=wrapInlineCode('"16/9h"')?> will automatically resize the product's width dimension needed to maintain a 16:9 aspect ratio using the product's height. This config setting is only useful for responsive products, such as the Single player, where the width and height CSS properties must be applied to its configurable <?=wrapInlineCode('<div>')?> element.</li>
					<li><?=wrapInlineCode('data-config-height=""')?> <span class="optional">(optional)</span> - The height of the configurable <?=wrapInlineCode('<div>')?> element measured in pixels; may contain <?=wrapInlineCode('"w"')?> variable to dynamically calculate the height based on the configurable <?=wrapInlineCode('<div>')?> element's width. For example, setting this property's value to <?=wrapInlineCode('"9/16w"')?> will automatically resize the product's height dimension needed to maintain a 16:9 aspect ratio using the product's width. This config setting is only useful for responsive products, such as the Single player, where the width and height CSS properties must be applied to its configurable <?=wrapInlineCode('<div>')?> element.</li>
				</ul>
			</p>
			
			<h2>Video Players</h2>
			<p>
				NDN video player products allow for a specified video and/or playlist of videos to be viewed within a webpage. For any of the videos loaded and viewed within this video player, the content may be revisited through several sharing options we provide to the user.
			</p>
			<p>
				There are currently three video player products:
				<ul>
					<li><a href="#VideoPlayer_Single">Single Player</a></li>
					<li><a href="#VideoPlayer_Inline300">Inline 300 Player</a></li>
					<li><a href="#VideoPlayer_Inline590">Inline 590 Player</a></li>
				</ul>
			</p>
			
			<h2>Video Launchers</h2>
			<p>
				NDN video launcher products provide preview thumbnails of videos in a playlist or multiple playlists, which launch a video player product in a new window when clicked.
			</p>
			<p>
				There are currently two video launcher products:
				<ul>
					<li><a href="#VideoLauncher_Slider">Slider Launcher</a></li>
					<li><a href="#VideoLauncher_Playlist300x250">Playlist 300x250 Launcher</a></li>
					<?php /** ?>
					<li><a href="#VideoLauncher_Horizontal425">Horizontal 425 Launcher</a></li>
					<li><a href="#VideoLauncher_Horizontal550">Horizontal 550 Launcher</a></li>
					<li><a href="#VideoLauncher_VerticalTower">Vertical Tower Launcher</a></li>
					<?php /**/ ?>
				</ul>
			</p>
			
			<a name="VideoPlayer_Single"></a>
			<h2>Single Player</h2>
			<img src="img/VideoPlayer/Single.png" style="display:block;margin:10px auto;width:700px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	style="width:590px;height:332px;" 
	data-config-widget-id="1"
	data-config-type="VideoPlayer/Single"
	data-config-tracking-group="10557" 
	data-config-video-id="24887508" 
	data-config-site-section="site_section_id"></div>'); ?>
				
				This product always fills the entire space of its configurable <?=wrapInlineCode("<div>")?> element, so the <?=wrapInlineCode("width")?> 
				and <?=wrapInlineCode("height")?> CSS properties must always be provided. These CSS properties may be specified directly on the 
				<?=wrapInlineCode("<div>")?> element (like shown in the example embed code above) or the inline CSS may be omitted and be specified in a
				CSS stylesheet. Because this product responds directly to the width and height of its configurable <?=wrapInlineCode("<div>")?> element,
				this product may be used with ease inside a webpage that uses <a href="#responsiveDesignUseCase">responsive design</a>.
			</p>	
			
			<a name="VideoPlayer_Inline300"></a>
			<h2>Inline 300 Player</h2>
			<img src="img/VideoPlayer/Inline300.png" style="display:block;margin:10px auto;width:300px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	data-config-widget-id="1"
	data-config-type="VideoPlayer/Inline300"
	data-config-tracking-group="10557" 
	data-config-video-id="24887508" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>
			<p>
				The Inline 300 Player has a fixed width and height of 300x495 pixels; the <?=wrapInlineCode('style=""')?> attribute is unnecessary and excluded as seen in the example embed code above.
			</p>
			
			<a name="VideoPlayer_Inline590"></a>
			<h2>Inline 590 Player</h2>
			<img src="img/VideoPlayer/Inline590.png" style="display:block;margin:10px auto;width:590px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	data-config-widget-id="1"
	data-config-type="VideoPlayer/Inline590"
	data-config-tracking-group="10557" 
	data-config-video-id="24887508" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				The Inline 590 Player has a fixed width and height of 590x530 pixels; the <?=wrapInlineCode('style=""')?> attribute is unnecessary and excluded as seen in the example embed code above.
			</p>
			
			<a name="VideoLauncher_Slider"></a>
			<h2>Slider Launcher</h2>
			<img src="img/VideoLauncher/Slider.png" style="display:block;margin:10px auto;width:300px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	data-config-widget-id="1"
	data-config-type="VideoLauncher/Slider"
	data-config-tracking-group="10557" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				This product is responsive and will fill the width of your right rail from 300 up to 350 pixels; the height will scale accordingly. The <?=wrapInlineCode('style=""')?> attribute is unnecessary and excluded as seen in the example embed code above.
			</p>
			
			<a name="VideoLauncher_Playlist300x250"></a>
			<h2>Playlist 300x250 Launcher</h2>
			<img src="img/VideoLauncher/Playlist300x250.png" style="display:block;margin:10px auto;width:300px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	data-config-widget-id="1"
	data-config-type="VideoLauncher/Playlist300x250"
	data-config-tracking-group="10557" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				The Playlist 300x250 Launcher has a fixed width and height of 300x250 pixels; the <?=wrapInlineCode('style=""')?> attribute is unnecessary and excluded as seen in the example embed code above.
			</p>
			
			<?php /** ?>
			<a name="VideoLauncher_Horizontal425"></a>
			<h2>Horizontal 425 Launcher</h2>
			<img src="img/VideoLauncher/Horizontal-425.png" style="display:block;margin:10px auto;width:425px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	style="width:425px;"
	data-config-widget-id="1"
	data-config-type="VideoLauncher/Horizontal"
	data-config-tracking-group="10557" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				The Horizontal 425 Launcher has a fixed width and height of 425x140 pixels.
			</p>
			
			<a name="VideoLauncher_Horizontal550"></a>
			<h2>Horizontal 550 Launcher</h2>
			<img src="img/VideoLauncher/Horizontal.png" style="display:block;margin:10px auto;width:550px;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	style="width:550px;"
	data-config-widget-id="1"
	data-config-type="VideoLauncher/Horizontal"
	data-config-tracking-group="10557" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				The Horizontal 550 Launcher has a fixed width and height of 550x140 pixels.
			</p>
			
			<a name="VideoLauncher_VerticalTower"></a>
			<h2>Vertical Tower Launcher</h2>
			<img src="img/VideoLauncher/VerticalTower.png" style="display:block;margin:10px auto;" />
			<p>
				<b>Example embed code:</b>
				<?php wrapCode('xml', '<div 
	class="ndn_embed" 
	style="width:550px;"
	data-config-widget-id="1"
	data-config-type="VideoLauncher/VerticalTower"
	data-config-tracking-group="10557" 
	data-config-site-section="site_section_id"></div>'); ?>
			</p>	
			<p>
				The Vertical Tower Launcher has a fixed width and height of 125x545 pixels.
			</p>
			<?php /**/ ?>
			
			<a name="responsiveDesignUseCase"></a>
			<h2>Responsive Design Use Case</h2>
			<p>
				<ul>
					<li><a href="responsive-design-demo/demo.html" target="_blank">See a demo of responsive design being used with our Single player</a></li>
					<li><a href="responsive-design-demo.zip">Download the demo in a zip file</a></li>
				</ul>
			</p>
			
			<a name="jsApiEmbed"></a>
			<h2>JavaScript API: Dynamic Embed</h2>
			<p>
				While the NDN Player Suite provides the feature to embed products on a page with configurable <?=wrapInlineCode("<div>")?> elements, it also allows
				for products to be embedded on a page dynamically using its JavaScript API. In order to use its JavaScript API, the required <?=wrapInlineCode("<script>")?>
				tag described earlier is expected to already be included within the page's HTML. Additionally, the following JavaScript must also be executed by the browser
				before using the JavaScript API:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
</script>'); ?>
			</p>
			<p>
				An example
				<?php wrapCode('html', '<div
	id="my-ndn-player"
	style="width:400px;height:300px;"
	data-config-widget-id="1"
	data-config-type="VideoPlayer/Single"
	data-config-tracking-group="10557">
		<img
			src="http://assets.newsinc.com/2013-players-suite/implementation/img/demo-thumbnail.jpg"
			onclick="_ndnq.push([\'embed\', this.parentNode]);"
		/>
</div>
	'); ?>
				<span style="font-weight:bold;font-style:italic;">Important:</span> Note that the <?wrapInlineCode('class="ndn_embed"')?> attribute is excluded in this example so that the configurable <?wrapInlineCode('<div>')?> element
				does not get embedded automatically once the browser has finished downloading the NDN Player Suite's JavaScript library.
			</p>
			<p>
				The actual JavaScript API in this example is included in the <?=wrapInlineCode("<img>")?> element's <?=wrapInlineCode('onclick=""')?> attribute:
				<?php wrapCode('js', '_ndnq.push([\'embed\', this.parentNode]);'); ?>
				In the above code, <?=wrapInlineCode("this.parentNode")?> refers to the <?=wrapInlineCode("<img>")?> element's parent <?=wrapInlineCode("<div>")?> element, but if you do not have a direct reference to this element using JavaScript,
				then a string specifying the configurable <?=wrapInlineCode("<div>")?> element's <?=wrapInlineCode('id=""')?> attribute can be provided instead, for example:
				<?php wrapCode('js', '_ndnq.push([\'embed\', \'my-ndn-player\']);'); ?>
			</p>

			<a name="jsApiHooks"></a>
			<h2>JavaScript API: Hooks</h2>
			<p>
				The NDN Player Suite allows for custom JavaScript code to be executed at certain times when a user is engaged with our video player products.
			</p>
			<p>
				For the following code examples provided, presume the required <?=wrapInlineCode("<script>")?> tag and the following HTML is already included within the page:
				 
				<?php wrapCode('xml', '<div class="main-container">
	<div 
		id="my-player-1"
		class="ndn_embed" 
		style="width:590px;height:332px;" 
		data-config-widget-id="1"
		data-config-type="VideoPlayer/Single"
		data-config-tracking-group="10557" 
		data-config-video-id="24887508" 
		data-config-site-section="site_section_id">
	</div>
		
	<div 
		id="my-player-2"
		class="ndn_embed" 
		data-config-widget-id="1"
		data-config-type="VideoPlayer/Inline590"
		data-config-tracking-group="10557" 
		data-config-video-id="24887508" 
		data-config-site-section="site_section_id">
	</div>
</div>'); ?>
			</p>
			<p>
				Notice that the <?=wrapInlineCode('id=""')?> attribute has been added to the two video player products in order to distinguish and identify them. 
				The value for this attribute can be set to anything you'd like as long as it is unique to the page and adheres to 
				<a href="http://www.w3.org/TR/html-markup/global-attributes.html#common.attrs.id" target="_blank">HTML standards</a>.
			</p>
			<style>
				.jsApiHooks_example {
					padding-top: 20px;
				}
			</style>
			<p class="jsApiHooks_example">
				<h3>The "videoEnd" event</h3>
				An example of executing custom JavaScript upon the <b>"videoEnd"</b> event being triggered by the product embedded within the <?=wrapInlineCode('<div id="my-player-1">')?> element:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'hook\', \'my-player-1/videoEnd\', function(event) {
		alert(\'Video has ended for the player embedded within the <div> element \' +
			\'with id="\' + event.containerElement.id + \'"!\');
	}]);
</script>'); ?>
			<p class="jsApiHooks_example">
				<h3>The "videoLoad" event</h3>
				An example of executing custom JavaScript upon the <b>"videoLoad"</b> event being triggered by the product embedded within the <?=wrapInlineCode('<div id="my-player-1">')?> element:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'hook\', \'my-player-1/videoLoad\', function(event) {
		alert(\'Video has loaded for the player embedded within the <div> element \' +
			\'with id="\' + event.containerElement.id + \'"!\');
	}]);
</script>'); ?>
			<p class="jsApiHooks_example">
				<h3>The "placementUnavailable" event</h3>
				An example of executing custom JavaScript upon the <b>"placementUnavailable"</b> event being triggered by the product embedded within the <?=wrapInlineCode('<div id="my-player-1">')?> element:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'hook\', \'my-player-1/placementUnavailable\', function(event) {
		alert(\'No placement via Perfect Pixel was available for the embed code provided by the <div> element \' +
			\'with id="\' + event.containerElement.id + \'"!\');
	}]);
</script>'); ?>
			</p>
			<p class="jsApiHooks_example">
				<h3>The "videoPause" event</h3>
				An example of executing custom JavaScript upon the <b>"videoPause"</b> event being triggered by the product embedded within the <?=wrapInlineCode('<div id="my-player-2">')?> element:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'hook\', \'my-player-2/videoPause\', function(event) {
		alert(\'Video was paused (at \' + event.data.currentTime + \' seconds) for the player \' +
			\'embedded within the <div> element with id="\' + event.containerElement.id + \'"!\');
	}]);
</script>'); ?>
			</p>
			<p class="jsApiHooks_example">
				<h3>Listening for events on all products embedded on a page</h3>
				The above examples demonstrate how to listen to a specific product's events, but if you'd like to listen for an event across all products embedded on a page, 
				then an asterisk (<?wrapInlineCode('*')?>) may be used in place of a configurable <?=wrapInlineCode('<div>')?> element's <?=wrapInlineCode('id=""')?> attribute:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'hook\', \'*/videoPause\', function(event) {
		alert(\'Video was paused (at \' + event.data.currentTime + \' seconds) for the player \' +
			\'embedded within the <div> element with id="\' + event.containerElement.id + \'"!\');
	}]);
</script>'); ?>
				The above code will trigger the custom JavaScript provided for the product embedded within the <?=wrapInlineCode('<div id="my-player-1">')?> element as well as the 
				<?=wrapInlineCode('<div id="my-player-2">')?> element. You may use the <?=wrapInlineCode('event.containerElement.id')?> property in JavaScript to distinguish 
				which product triggered the event.
			</p>
			

			<a name="jsApiControllingTheProduct"></a>
			<h2>JavaScript API: Controlling the product</h2>
			<p>
				The NDN Player Suite allows for embedded products to be controlled by a limited set of recognized commands via JavaScript.
			</p>
			<p class="jsApiHooks_example">
				<h3>The "videoPause" command</h3>
				An example of using JavaScript to pause an NDN Player Suite product's video player by issuing the <b>"videoPause"</b> command for the product embedded within the <?=wrapInlineCode('<div id="my-player-2">')?> element:
				<?php wrapCode('js', '<script type="text/javascript">
	var _ndnq = _ndnq || [];
	_ndnq.push([\'command\', \'my-player-2/videoPause\']);
</script>'); ?>
			</p>
			<br/>
			<a name="troubleshooting"></a>
			<h2>Troubleshooting</h2>			
			<p>
				If an NDN Player Suite product fails to load, please follow the steps below:
			</p>	
			<ul>
				<li>Turn off any ad blocker plugin or extension.</li>
				<li>If your browser blocks popup windows, make sure popup windows are allowed for the website you are visiting</li>
				<li>Remove any CSS being applied to the configurable <?php wrapInlineCode('<div>') ?> element (except for the "width" and "height" CSS properties if they are necessary) and its contents. CSS may be inadvertantly be getting applied to the product's contents, especially if the website's CSS uses selectors that rely on an element's <?php wrapInlineCode('id') ?> attribute to style elements containing the configurable <?php wrapInlineCode('<div>') ?> element or make heavy use of the <?php wrapInlineCode('!important') ?> modifier.</li>
				<li>Verify the necessary <?=wrapInlineCode("<script>")?> tag is on the webpage being visited.</li>
				<li>If the product still fails to load, place the necessary <?=wrapInlineCode("<script>")?> tag immediately following the configurable <?php wrapInlineCode('<div>') ?> element(s) within the page's HTML.</li>
			</ul>
			
		</div>
	</div>
</body>
</html>