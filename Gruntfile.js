var fs = require('fs'),
	aws = require('aws-sdk');

/**
 * Simple helper object to do some pretty straight-forward tasks
 * @var {Object}
 */
var _helper = {
	/**
	 * Pass any number of JSON objects to be merged into the first JSON object provided when calling this method
	 */
	extend: function() {
		var result = arguments[0];
		
		for (var i = 1; i < arguments.length; i++) {
			var object = arguments[i];
			if (typeof object == 'object') {
				for (var key in object) {
					if (object.hasOwnProperty(key)) {
						result[key] = object[key];
					}
				}
			}
		}
		
		return result;
	}
};

module.exports = function(grunt) {
	var buildNumber = grunt.option('build') || 0;
	
	var optimizeMethod = grunt.option('optimize')
		? grunt.option('optimize')
		: 'none';
	
	var isCompressingBuild = !!grunt.option('compress');

	var appEnv = grunt.option('env');
	
	var appDomain = grunt.option('domain')
		? grunt.option('domain')
		: ((appEnv != 'prod' ? appEnv + '-' : '') + 'launch.newsinc.com');
	
	// Whether a build is actually being built (where the RequireJS library is added to the embed.js file, etc.)
	var isBuilding = !!grunt.option('env');

	var getEmbedJsHtml = function(basePath) {
		var requireJsHtml = '<script type="text/javascript" src="' + basePath + '/js/require.js"></script>';

		return (!grunt.option('env') ? requireJsHtml : '') + '<script type="text/javascript" src="' + basePath + '/js/embed.js" id="_nw2e-js"></script>';
	};
	
	var jsTestsHtml = (function() {
	    var scriptTags = [];
	    
	    /**
	     * @param {String} absolutePath For example: "tests/js/models/Ndn/Utils/AspectRatioTests.js"
	     * @param {String} rootDirectory For example: "tests/js"
	     * @param {String} subDirectory For example: "models/Ndn/Utils"
	     * @param {String} filename For examle: "AspectRatioTests.js"
	     */
	    var recurseFilesHandler = function(absolutePath, rootDirectory, subDirectory, filename) {
            scriptTags.push('<script src="/' + absolutePath + '"></script>');
	    };
	    
	    // Only recurse through the "tests/js" folder if it exists
	    if (grunt.file.exists('tests/js')) {
	        grunt.file.recurse('tests/js', recurseFilesHandler);
	    }
	    
	    return scriptTags.join("\n");
	})();
	
	var pkg = grunt.file.readJSON('package.json');

	// Project configuration
	grunt.initConfig({
		pkg: pkg,
		build: {
			appSettings: grunt.option('appSettings') || '',
			environment: appEnv,
			version: buildNumber,
			playerSuiteVersion: pkg.version,
			appDomain: appDomain,
			optimizeMethod: optimizeMethod,
			isCompressingBuild: isCompressingBuild,
			jsTestsHtml: jsTestsHtml,
			embedJsHtml: getEmbedJsHtml('.'),
			legacyEmbedJsHtml: getEmbedJsHtml('..'),
            topLevelEmbedJsHtml: getEmbedJsHtml('..')
		},
		
		clean: {
			all: {
				src: [
					'build',
                    'landing-page.html',
					'share.html',
					'routingrules.json',
					'js/appurl.js',
					'js/build-details.js',
					'js/templates/embed.html',
					'tests/index.html'
				]
			},
			unneededBuiltFiles: {
				src: [
					'build/**/*.tpl.js',
					'build/**/*.tpl.html',
					'build/js/templates'
				]
			}
		},
		
		recess: {
			lint: {
				options: {
					noUnderscores: false,
					noOverqualifying: false,
					strictPropertyOrder: false,
					noUniversalSelectors: false,
					zeroUnits: false,
					noIDs: false
				},
				src: ['less/NdnEmbed.less','less/NdnEmbed2.less']
			},
			build: {
				options: {
					compile: true,
					compress: isCompressingBuild
				},
				files: {
					'build/css/NdnEmbed.css': [
						'less/NdnEmbed.less'
					],
					'build/css/NdnEmbed2.css': [
   						'less/NdnEmbed2.less'
   					]
				}
			},
			dist: {
				options: {
					compile: true,
					compress: true
				},
				files: {
					'dist/css/NdnEmbed.css': [
						'less/NdnEmbed.less'
					],
					'dist/css/NdnEmbed2.css': [
						'less/NdnEmbed2.less'
					]
				}
			}
		},
		
		requirejs: {
			compile: {
				options: {
					paths: {
						// TODO: Does this need to be added manually?
						requireLib: 'require',

						amp_lib: 'lib/amp.premier/amp.premier',
						jquery: 'lib/jquery',
						underscore: 'lib/underscore',
						backbone: 'lib/backbone',
						mustache: 'lib/mustache',
						addThis: 'empty:',

						bootstrap_tabs: 'lib/bootstrap_tabs',
						jquery_bxslider: 'lib/jquery_plugins/jquery.bxslider',
						jquery_imagesloaded: 'lib/jquery_plugins/jquery.imagesLoaded.min',
						jquery_autoellipsis: 'lib/jquery_plugins/jquery.autoellipsis',
						jquery_simplePagination: 'lib/jquery_plugins/jquery.simplePagination',
						jquery_resize: 'lib/jquery_plugins/jquery.ba-resize',
						jquery_responsive_containers: 'lib/jquery_plugins/jquery.responsive-containers',
						text: 'lib/require_plugins/text'
					},
					namespace: 'Ndn_Require',
					modules: (function() {
						var result = [];

						// The "/js/embed.js" bundle (this should be created as light as possible)
						result.push({
							name: 'embed',
							include: [
					        	'appurl',
					        	'requireLib',
					        	'main',
					        	'embed'
							],
							create: true
						});
						
						// The bundled JavaScript needed for the "/js/legacy-migration.js" file
						result.push({
							name: 'models/Ndn/Legacy/LegacyMigration',
							include: ['models/Ndn/Legacy/LegacyMigration'],
							exclude: ['embed'],
					        create: true
						});
						
						var widgetTypes = [
		                   'VideoPlayer/Default',
		                   'VideoPlayer/Studio',
		                   'VideoPlayer/Inline300',
		                   'VideoPlayer/Inline590',
		                   'VideoPlayer/16x9',
		                   'VideoPlayer/PTR',
		                   'VideoLauncher/Slider',
		                   'VideoLauncher/Playlist300x250',
		                   'VideoLauncher/Horizontal',
		                   'VideoLauncher/VerticalTower'
						];

						// The everything-else bundle
						result.push({
							name: 'models/Ndn/Widget',
							include: (function() {
								var result = ['models/Ndn/Widget'];
								
								for (var i = 0; i < widgetTypes.length; i++) {
									var widgetType = widgetTypes[i];
									result.push('models/Ndn/Widget/' + widgetType + 'Model');
								}
                                
								return result;
							})(),
							exclude: [
					        	'requireLib',
					        	'embed'
					        ],
							create: true
						});
						
						return result;
					})(),
					appdir: './js',
					baseUrl: './js',
					dir: 'build/js',
					removeCombined: true,
					optimize: optimizeMethod
				}
			}
		},
		
		concat: {
			buildFiles: (function() {
				var htmlTemplateFilePaths = [
					'landing-page',
					'share',
					'legacy-migration/embed',
					'legacy-migration/Inline300',
					'legacy-migration/Inline300RmmSoundOn',
					'legacy-migration/Inline590',
					'legacy-migration/Inline590RmmSoundOn',
					'legacy-migration/Launcher',
					'legacy-migration/RmmSoundOn',
					'tests/index'
				];
				
				var files = {};

				for (var i = 0; i < htmlTemplateFilePaths.length; i++) {
					var htmlTemplateFilePath = htmlTemplateFilePaths[i];
					
					files[htmlTemplateFilePath + '.html'] = [htmlTemplateFilePath + '.tpl.html'];
				}

                files['embed.html'] = ['landing-page.tpl.html'];
                // files['js/templates/embed.html'] = ['landing-page.tpl.html'];
				
				
				// If actually producing a build (and not just generating the needed files for an unbuilt environment)
				if (isBuilding) {
					_helper.extend(files, {
						'js/legacy-migration.js': ['js/appurl.tpl.js', 'js/legacy-migration.tpl.js'],
						'js/appurl.js': ['js/appurl.tpl.js'],
						'js/build-details.js': ['js/build-details.tpl.js'],
						'routingrules.json': ['routingrules.tpl.json']
					});
				}
				
				return {
					options: {
						process: true
					},
					files: files
				};
			})(),
			
			wrapEmbedJs: {
				src: [
			      'embed-wrapper-begin.js',
			      'build/js/embed.js',
			      'embed-wrapper-end.js'
			    ],
				dest: 'build/js/embed.js'
			}
		},
		
		s3: {
			options: {
				key: grunt.option('awskey') || process.env.AWS_ACCESS_KEY_ID,
				secret: grunt.option('awssecret') || process.env.AWS_SECRET_ACCESS_KEY,
				bucket: grunt.option('bucket'),
				access: 'public-read'
			},
			push: {
				upload: [
			         {
			        	 src: 'build/**/*',
			        	 dest: '<%= build.version %>/',
			        	 rel: 'build'
			         },
					 {
						 src: 'build/legacy-migration/**/*',
						 dest: '/legacy-migration/',
						 rel: 'build/legacy-migration'
					 },
			         {
			        	 src: 'build/crossdomain.xml',
			        	 dest: '/crossdomain.xml',
			         },
			         {
			        	 src: 'build/landing-page.html',
			        	 dest: '/landing-page.html'
			         },
                     {
                         src: 'build/embed.html',
                         dest: '/embed.html'
                     },
			         {
			        	 src: 'build/share.html',
			        	 dest: '/share.html'
			         },
			         {
			             src: 'build/img/favicon.ico',
			             dest: '/img/favicon.ico'
			         }
				]
			},
		},
		
		copy: {
			main: {
				files: [
			        {src: ['crossdomain.xml'], dest: 'build/', filter: 'isFile'}, // The "/crossdomain.xml" file
			        {src: ['landing-page.html', 'share.html'], dest: 'build/', filter: 'isFile'}, // The html files in the repository's root folder
			        {src: 'landing-page.html', dest: 'build/embed.html', filter: 'isFile'}, // The html index
			        {src: ['testpages/**'], dest: 'build/'}, // Files and folders within the "/testpages" folder // TODO: Can this be removed?
			        {src: ['resources/**'], dest: 'build/'}, // Files and folders within the "/resources" folder
			        {src: ['font/**'], dest: 'build/'}, // Files and folders within the "/font" folder
			        {src: ['legacy-migration/**'], dest: 'build/'}, // Files within the "/legacy-migration" folder
			        {src: ['img/**'], dest: 'build/'} // Files and folders within the "/img" folder
				]
			}
		},

    invalidate_cloudfront: {
      options: {
        key: grunt.option('cloudfront_key'),
        secret: grunt.option('cloudfront_secret'),
        distribution: grunt.option('cloudfront_id'),
      },
      redirects: {
        files: [
          {dest: "js/embed.js"},
          {dest: "js/dynamic.js"},
          {dest: "js/embed-silo.js"},
          {dest: "js/build-details.js"},
          {dest: "js/require.js"},
          {dest: "js/lib/amp.premier/AkamaiPremierPlayer.swf"},
          {dest: "js/lib/amp.premier/amp.premier.facebook.xml"},
          {dest: "landing-page.html"},
          {dest: "share.html"},
          {dest: "crossdomain.xml"},
          {dest: 'legacy-migration/embed.html'},
          {dest: 'legacy-migration/Inline300.html'},
          {dest: 'legacy-migration/Inline300RmmSoundOn.html'},
          {dest: 'legacy-migration/Inline590.html'},
          {dest: 'legacy-migration/Inline590RmmSoundOn.html'},
          {dest: 'legacy-migration/Launcher.html'},
          {dest: 'legacy-migration/RmmSoundOn.html'}
        ]
      }
    },
		
		qunit: {
			src: ['tests/index.html']
		}
	});
	
	// Load all the needed plugins for the tasks about to be registered
	(function() {
		var npmTasks = [
			'grunt-recess', // Needed for "recess" task
			'grunt-contrib-requirejs', // Needed for "requirejs" task
			'grunt-file-creator', // Needed for "filecreator" task
			'grunt-contrib-concat', // Needed for "concat" task
			'grunt-s3', // Needed for "s3" task
			'grunt-contrib-copy', // Needed for the "copy" task
			'grunt-contrib-clean', // Needed for the "clean" task
			'grunt-contrib-clean', // Needed for the "clean" task
			'grunt-contrib-qunit', // Needed for the "test" task (or is it "qunit" task?)
      'grunt-invalidate-cloudfront' // Needed for the invalidate_cloudfront task
		];
		
		for (var i = 0; i < npmTasks.length; i++) {
			grunt.loadNpmTasks(npmTasks[i]);
		}
	})();
	
	// TODO: Does this need to be modified? The name parameters do not appear to match the other tasks that are registered within this file
	grunt.registerTask('test', 'qunit:src');

	// TODO: Is the below code necessary? Consider refactoring.
	// This task modifies the requirejs config to add modules by walking the paths in _autoInclude
	grunt.registerTask('requirejs:config', 'Configure requirejs', function() {
		var done = this.async();
		
		grunt.log.writeln('Adding module includes');
		grunt.config.set('requirejs.compile.options.modules', grunt.config.get('requirejs.compile.options.modules'));
		grunt.log.writeln('Config modified!');

		done();
	});

  grunt.registerTask('s3:website', 'Configure S3 website', function() {
    var done = this.async();

    aws.config.update({accessKeyId: grunt.option('awskey'), secretAccessKey: grunt.option('awssecret')});

    var s3 = new aws.S3();

    var payload = {
      Bucket: grunt.option('bucket')
    };

    s3.client.deleteBucketWebsite(payload, function(err, resp) {
      if (err) console.log(err); // throw err;
      console.log(resp);
      done();
    });

  });

  grunt.registerTask('s3:redirects', 'Configure S3 object level redirects', function() {
    var done = this.async();

    aws.config.update({accessKeyId: grunt.option('awskey'), secretAccessKey: grunt.option('awssecret')});

    var s3 = new aws.S3();

    var payload = {
      Bucket: grunt.option('bucket'),
      Body: new Buffer(0),
      ACL: 'public-read'
    };

    var redirects = [
      {'f':'js/embed.js', 'ct':'application/javascript'},
      {'f':'js/embed-silo.js', 'ct':'application/javascript'},
      {'f':'js/dynamic.js', 'ct':'application/javascript'},
      {'f':'js/require.js', 'ct':'application/javascript'},
      {'f':'js/legacy-migration.js', 'ct':'application/javascript'},
      {'f':'js/lib/amp.premier/AkamaiPremierPlayer.swf', 'ct':'application/x-shockwave-flash'},
      {'f':'js/lib/amp.premier/amp.premier.facebook.xml', 'ct':'application/xml'}
    ];

    var completed = 0;

    console.log('Changing redirects to build ' + grunt.option('build'));
    for (var i=0; i < redirects.length; i++) {
      payload.Key = redirects[i]['f'];
      payload.WebsiteRedirectLocation = '/' + grunt.option('build') + '/' + redirects[i]['f'];
      payload.ContentType = redirects[i]['ct'];

      (function(Key) {
        s3.client.putObject(payload, function(err, resp) {
          if (err) throw err;
          console.log(" - " + Key);
          completed++;
          if (completed == redirects.length) { done(); }
        });
      })(payload.Key);
    };
  });

	// Define javascript task
	grunt.registerTask('requirejs:build', 'Build Javascript', function() {
		grunt.task.run(['requirejs:config', 'requirejs:compile']);
	});

	// Define build task
	grunt.registerTask('build', 'Build Everything', function() {
		grunt.task.run([
			'concat:buildFiles',
			'copy',
			'recess:lint',
			'recess:build',
			
			// Compile JavaScript using RequireJS, then remove the unneeded files from the "/build" folder
			'requirejs:build',
			'concat:wrapEmbedJs',
			'clean:unneededBuiltFiles'
		]);
	});

	// Define push task
	grunt.registerTask('push', 'Push everything to S3', function() {
		grunt.task.run(['s3:push', 's3:redirects']);
	});

	// Define the default task
	grunt.registerTask('default', ['concat:buildFiles']);
}
