define(['jquery'], function($) {
    var mouseMoveIndex = 0;
    
    /**
     * @param {Object} [options] Structured data concerning the draggable behavior of the current element. Format:
     * <pre>{
     *  'handle': String, // The CSS selector relative to the current element that serves as a handle to drag the current element (defaults to the current element)
     *  'cursor': String, // The type of cursor to display when hovering over the current element's handle element (defaults to "move")
     *  'placementHook': Function, // A function to handle custom placement logic of the current element, defaults to: function(coordinates) { return {top: coordinates.top, left: coordinates.left}; }. Return false using this hook in order to override this plugin's placement logic altogether. 
     * }</pre>
     */
    $.fn.drags = function(options) {
        options = $.extend({
            handle: '',
            cursor: 'move',
            placementHook: ''
        }, options);
        
        var container = $(this),
            handleElement = options.handle ? container.find(options.handle) : container;
        
        // Set the appropriate cursor when mousing over the element that serves as the handle to drag the specific dom element
        handleElement.css('cursor', options.cursor);
        
        handleElement.on('mousedown', function(event) {
            var containerStartPositionTop = container.offset().top,
                containerStartPositionLeft = container.offset().left,
                startMousePositionY = event.pageY,
                startMousePositionX = event.pageX,
                startZIndex = container.css('z-index');
            
            var parentElementListeners = $(window); // container.parents();
            
            function onMouseMove(event) {
                var currentMousePositionY = event.pageY,
                    currentMousePositionX = event.pageX;
                
                var newCoordinates = {
                    top: containerStartPositionTop - (startMousePositionY - currentMousePositionY),
                    left: containerStartPositionLeft - (startMousePositionX - currentMousePositionX)
                };
                
                if (options.placementHook) {
                    newCoordinates = options.placementHook(newCoordinates);
                }
                
                if (newCoordinates) {
                    container.offset(newCoordinates);
                }
            }
            
            function onMouseUp(event) {
                if (options.onMouseUpHook) {
                    options.onMouseUpHook();
                }
                
                // Clean up the "mousemove" and "mouseup" event listeners that are no longer needed
                parentElementListeners.off('mousemove', onMouseMove);
                parentElementListeners.off('mouseup', onMouseUp);
            }
            
            parentElementListeners.on('mousemove', onMouseMove);
            parentElementListeners.on('mouseup', onMouseUp);
        });
    };
});