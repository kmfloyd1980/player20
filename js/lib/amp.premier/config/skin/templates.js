define([
        'models/Ndn/TemplateSet',
        'models/Ndn/Widget',
        'text!lib/amp.premier/config/skin/ndn_assets.xml',
        'text!lib/amp.premier/config/skin/ndn_assets_small.xml'
    ],
	function(Ndn_TemplateSet, Ndn_Widget, template_ndn_assets, template_ndn_assets_small) {
		var defaultParams = {
			'akamaiJsLibFolderUrl': Ndn_Widget.getAppUrl() + '/js/lib/amp.premier',
			'akamaiResourcesFolderUrl': Ndn_Widget.getAppUrl() + '/resources'
		};
		
		return new Ndn_TemplateSet({
			'ndn_assets': [template_ndn_assets, defaultParams],
			'ndn_assets_small': [template_ndn_assets_small, defaultParams]
		});
	}
);