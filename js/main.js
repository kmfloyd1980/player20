/**
 * This is just a dummy "main.js" file to get rid of the 404/403 errors when requesting this file, which is some artifact of using requirejs.
 */
define([], function() {});