var _ndnq = _ndnq || [];
var _nw2e = _nw2e || [];

(function() {
	// Get the "src" attribute of the current <script> tag being executed (that has been redirected to this JavaScript file)
	var scriptElements = document.getElementsByTagName('script'),
		currentScriptIndex = scriptElements.length - 1,
		currentScriptSrc = scriptElements[currentScriptIndex].src;
	
	// Find the most recent <script> tag that is pointed to the "embed.newsinc.com" domain
	while (!currentScriptSrc.match(/^http:\/{2}embed\.newsinc\.com\//) && currentScriptIndex > 0) {
		currentScriptSrc = scriptElements[--currentScriptIndex].src;
	}
	
	// If a <script> tag pointed to "embed.newsinc.com" could be found, attempt to force migrate the legacy player using the <script> tag's "src" attribute
	if (currentScriptIndex >= 0) {
		_ndnq.push(['when', 'initialized', function() {
			_nw2e.require(['models/Ndn/Legacy/LegacyMigration'], function(Ndn_Legacy_LegacyMigration) {
				Ndn_Legacy_LegacyMigration.migrate(currentScriptSrc);
			});
		}]);

		// Add the needed <script> tag if it is not on the page already
		if (document.getElementById('_nw2e-js') === null) {
			_nw2e.legacyMigrationAddedEmbedJs = true;
			(function() {
				var nw = document.createElement('script'); nw.type = 'text/javascript'; nw.async = true; nw.src = _nw2e.appUrl + '/js/embed.js'; nw.id = '_nw2e-js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(nw, s);
			})();
		}
	}
})();