define([
        'underscore',
        'backbone',
        'mustache'
    ],
	function(_, Backbone, Mustache) {
		return Backbone.Model.extend({
			defaults: {
				/**
				 * @var {Object<String,String>}
				 */
				rendered: [],
				
				/**
				 * Format: 
				 * <pre>{
				 * 	{String}: [ // The template's name/identifier
				 * 		{String}, // The template
				 * 		{Object}, // The default parameters to use when rendering template
				 * 	],
				 * 	// ... All other templates
				 * }</pre>
				 * @var {Array<Array>}
				 */
				templates: {}
			},
			
			initialize: function(templatesData) {
				this.set('templates', templatesData);
			},
			
			/**
			 * @param {String} templateName The name identifying the template to render
			 * @param {Object} [params] The parameters to use when generating this template (default parameters are used for the parameters not provided)
			 * @return {String} The rendered template using either the parameters provided or the template's default parameters
			 */
			getTemplate: function(templateName, params) {
				var template = this.get('templates')[templateName][0],
					templateDefaultParams = this.get('templates')[templateName][1];
				
				if (typeof params == 'undefined') {
					// If there is no cached template rendered with its default parameters used, create it and load it into the cache
					if (typeof this.get('rendered')[templateName] == 'undefined') {
						this.get('rendered')[templateName] = Mustache.to_html(template, templateDefaultParams);
					}
					
					return this.get('rendered')[templateName];
				}
				else {
					return Mustache.to_html(template, _.extend({}, templateDefaultParams, params));
				}
			}
		});
	}
);