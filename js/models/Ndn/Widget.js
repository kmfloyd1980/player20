var _nw2e = _nw2e || [];

/**
 * @module Ndn
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'require',
    'models/Ndn/App',
    'models/Ndn/PlayerServices',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/Utils/UserAgent',
    'models/Ndn/Widget/Config',
    'models/Ndn/LoyaltyProgram',
    'models/Ndn/EventHub',
    'models/Ndn/Analytics/ChartbeatStrategy',
    'models/Ndn/Debugger',
    'lib/respond',
    
    'text!templates/embed.html'
], function(
    $,
    _,
    Backbone,
    require,
    Ndn_App,
    Ndn_PlayerServices,
    Ndn_Utils_UrlParser,
    Ndn_Utils_UserAgent,
    Ndn_Widget_Config,
    Ndn_LoyaltyProgram,
    Ndn_EventHub,
    Ndn_Analytics_ChartbeatStrategy,
    Ndn_Debugger,
    respond,
    
    embedTemplate
) {
    // Prevent Ndn_Widget from being defined multiple times
    if (_nw2e.Ndn_Widget) {
        return _nw2e.Ndn_Widget;
    }
    
    /**
     * The zero-based-index to identify the next embedded product on this page
     * @var {int} _nextEmbedIndex
     * @memberof Ndn_Widget.
     * @private
     */
    var _nextEmbedIndex = 0;
    
    /**
     * @param {HTMLElement} target The dom element to get the full HTML for
     * @return {String} The provided dom element's full HTML, for example if an empty <div> element was provided, it will return "<div></div>" instead of ""
     */
    var _getElementFullHtml = function(target) {
        var wrap = document.createElement('div');
        wrap.appendChild(target.cloneNode(true));
        return wrap.innerHTML;
    };
    
    /**
     * @param {Ndn_Widget} widget
     * @param {DOMNode} containerElement
     * 
     * TODO: Refactor this using the Ndn_Utils_AspectRatio module! CONTAINS FIX THAT IS NEEDED!!
     */
    var _handleConfigDimensions = function(widget, containerElement) {
        widget.set('config', Ndn_Widget_Config.processDefaultDimensions(widget.get('config'), Ndn_Widget_Config.hasUpdatedEmbedCode(containerElement)));
        
        /**
         * @param {String} dimension The dimension to maintain aspect-ratio for; recognized values: "width", "height"
         */
        function maintainAspectRatio(dimension) {
            // Only allow these two parameter values to be passed
            if (dimension != 'width' && dimension != 'height') return;
            
            var dimensionParameter = dimension != 'width' ? 'width' : 'height',
                dimensionParameterVariable = dimensionParameter.substr(0, 1); // Either "w" or "h"
            
            // If the other dimension is dynamically being generated as well, ignore it completely (otherwise an infinite-loop could result)
            if ((widget.get('config')[dimensionParameter] || '').match(/[w|h]/gi)) return;
            
            // Modify the calculation used to dynamically generate this dimension's value by adding implied multiplication operator ("*")
            widget.get('config')[dimension] = widget.get('config')[dimension].replace(/([\d).])([h|w])/gi, '$1*$2').replace(/([h|w])([wh(])/gi, '$1*$2');
            
            widget.on('containerResize', function() {
                var dimensionParameterValue = containerElement[dimensionParameter]();
                
                // Generate the calculation needed to determine the dynamically generated dimension (and ensure that only valid math calculations are evaluated)
                var newDimensionValueCalculation = widget.get('config')[dimension].replace(new RegExp(dimensionParameterVariable, 'gi'), dimensionParameterValue).replace(/[^-()\d\/*+.]/g, '');
                
                // Set the dynamically generated dimension's new value
                containerElement.css(dimension, eval(newDimensionValueCalculation) + 'px');
            });
        }
        
        // When the product's container element is detected to have been resized, make sure that its width and height dimensions are updated appropriately
        containerElement.resize(function() {
            widget.synchronizeContainerDimensions();
        });
        
        // Apply "width" and "height" CSS properties to the widget's container element (if manually provided in the config)
        $.each(['width', 'height'], function(i, dimension) {
            if (widget.get('config')[dimension]) {
                // Ensure this value is a string
                widget.get('config')[dimension] += '';
                
                if (widget.get('config')[dimension].match(/h|w/gi)) {
                    // If the "w" or "h" letters are present, then maintain this dimension's value via the provided aspect ratio
                    maintainAspectRatio(dimension);
                }
                else {
                    if (widget.get('config')[dimension].match(/%|[a-z]/gi)) {
                        // If the "%" character (or alpha-numeric characters that are not "w" or "h", such as "em" or "px") is present for this dimension,
                        // set its value directly as the "width" or "height" CSS property's value
                        containerElement.css(dimension, widget.get('config')[dimension]);
                    }
                    else {
                        // Take the number and add the "px" suffix to indicate this dimension is measured using pixels
                        containerElement.css(dimension, widget.get('config')[dimension] + 'px');
                    }
                }
            }
        });
    };
    
    /**
     * @param {jQueryElement} containerElement
     * @param {Object} config
     * @param {Object} embedDetails Structured data concerning this embed. Format:
     * <pre>{
     *  'providedContainerElementHtml': String, // The provided container element's HTML
     *  'placedBy': Array<String>, // List of recognizeable string values pertaining to how this video is placed
     * }</pre>
     */
    var _embedContainer = function(containerElement, config, embedDetails) {
        // console.log('_embedContainer(', arguments, ');');
        
        var providedContainerElementHtml = embedDetails.providedContainerElementHtml,
            placedBy = embedDetails.placedBy;
        
        return $.Deferred(function(deferred) {
            // If this container element has already begun its embed process then do not attempt to embed it again
            if (containerElement.hasClass('ndn_embedding') || containerElement.hasClass('ndn_embedded')) {
                deferred.reject('duplicateEmbedProcess');
                return;
            }
            
            // Prepare this dom element to become a container for an embedded widget, also indicate that this dom element 
            // has been recognized as a future embed container by adding the "ndn_embedding" class. The order in which these
            // classes are added are important; see the Ndn_App.embedWidgetsFromContainersOnPage() method and the jQuery 
            // selector that it uses.
            containerElement
            .addClass('ndn_embedding') // Indicate that this product is currently in the process of being embedded
            .addClass('ndn_embed') // Important that this class gets added AFTER the "ndn_embedding" class has been added (if it hasn't been added already)
            .addClass('ndn_embedContainer'); // Identify the container element as an embed container for an NDN Player Suite product
            
            // Get a unique identifier for this widget being embedded
            var embedIndex = Ndn_Widget.getNextEmbedIndex(),
                widgetUid = 'ndn-widget-embed-' + (embedIndex + 1);
            
            // Mark this widget's territory by adding the widget's UID as a class to the container element as well as binding the 
            // widget UID to the container element's data
            containerElement
            .addClass(widgetUid)
            .data('ndn-widget-uid', widgetUid);
            
            // Determine the container element's "id" attribute, and if it does not have one, set its "id" attribute to this widget's unique identifier (widget UID)
            var containerElementId = (function() {
                if (typeof containerElement.attr('id') == 'undefined') {
                    containerElement.attr('id', widgetUid);
                }
                
                return containerElement.attr('id');
            })();
            
            // Detect if this embed code was placed via our WordPress plugin
            if (containerElement.hasClass('ndn_embed_wordpress')) placedBy.push('wordpress');
            
            // When/if this container element does eventually begin its embed process, resolve the deferred object returned by this method
            Ndn_EventHub.on('hook:' + containerElementId + '/widgetEmbedStart', function(event) {
                // console.log('WIDGET EMBED START: event ==', event);
                deferred.resolve(event.system.widget);
            });
            
            // If this container element is meant to be integrated with Perfect Pixel, but has not yet received its "legacyEmbedUrl" setting, 
            // require the appropriate Perfect Pixel JavaScript library
            if (config['distributorId'] && !config['legacyEmbedUrl']) {
                (function() {
                    var perfectPixelEnvDomain = Ndn_Utils_UrlParser.getUrlVar('ndn_ppEnv')
                        ? Ndn_Utils_UrlParser.getUrlVar('ndn_ppEnv') + '-dme.newsinc.com'
                        : 'dmeserve.newsinc.com';
                    
                    var requireConfig = {paths:{}};
                    requireConfig.paths['perfectPixel' + config['distributorId']] = window.location.protocol + '//' + perfectPixelEnvDomain + '/dpid/' + config['distributorId'] + '/PPEmbed';
                    var requirePerfectPixel = _nw2e.requireConfig(requireConfig);
                    
                    // Require the Perfect Pixel library for the provided distributor ID
                    requirePerfectPixel(['perfectPixel' + config['distributorId']], function(perfectPixel) {});
                })();
                
                // Discontinue executing the embed from within NDN Player Suite
                // deferred.reject();
                // TODO: THERE NEEDS TO BE A WAY TO DETECT WHAT PERFECT PIXEL IS DOING HERE,
                //       SO THAT WE MAY KNOW WHETHER THERE WAS A PLACEMENT HERE IN THE FIRST
                //       PASS OF EMBEDS!!!
                return;
            }
            
            // Identify when a product is being placed as a result of using Perfect Pixel
            if (config['distributorId'] && config['legacyEmbedUrl']) placedBy.push('ppembed');

            // Identify when a product is being placed as a result of being launched into a new browser window by a NDN Player Suite launcher product
            if (Ndn_App.isInLaunchedBrowserWindow()) placedBy.push('fromSlider');
            
            // Require and instantiate the needed model for the specified widget
            var modelLocation = 'models/Ndn/Widget/' + config['type'] + 'Model';
            
            require([modelLocation], function(WidgetModel) {
                // Instantiate the widget model to be embedded
                var widget = 
                new WidgetModel(config)
                .set({
                    'initialContainerElementHtml': providedContainerElementHtml,
                    'containerElement': containerElement.get(0),
                    'embedIndex': embedIndex,
                    'widgetUid': widgetUid,
                    'placedBy': placedBy
                });
                
                // If certain products should be embedded by an iframe
                if ((Ndn_App.getSetting('forceIframes') || Ndn_App.getSetting('forceIframesUsingSrc')) && !Ndn_App.isEmbeddedByIframe()) {
                    // If this product is one of the products that should be embedded by an iframe
                    if (_.contains(['VideoPlayer/Inline590', 'VideoLauncher/Slider', 'VideoLauncher/Playlist300x250'], config['type'])) {
                        // Hardcode the width and height of the container element for these two different types of products
                        if (config['type'] == 'VideoPlayer/Inline590') {
                            containerElement.css({
                                'width': '590px',
                                'height': '530px'
                            });
                        }
                        else {
                            containerElement.css({
                                'width': '300px',
                                'height': '250px'
                            });
                        }
                        
                        var iframeId = 'ndn-forced-iframe-embed-' + embedIndex;
                        
                        // Create the iframe element used to wrap the embedded product
                        var iframe =
                        $(document.createElement('iframe'))
                        .attr('id', iframeId)
                        .addClass('ndn_embed_iframe')
                        .css({
                            width: '100%',
                            height: '100%',
                            overflow: 'hidden',
                            border: 0
                        })
                        .attr('scrolling', 'no');
                        
                        if (Ndn_App.getSetting('forceIframesUsingSrc')) {
                            var appUrlInfo = Ndn_App.getAppUrlInfo(Ndn_App.getAppUrl()),
                                iframeBaseUrl = appUrlInfo 
                                    ? appUrlInfo.baseUrl 
                                    : 'http://' + Ndn_App.getAppDomain(),
                                iframeSrc = iframeBaseUrl + '/?' + $.param(config)
                                    + '&ndn_appSettings=' + encodeURIComponent($.param(Ndn_App.getOverrideSettings()))
                                    + '&ndn_iframeId=' + iframeId;
                                    
                            if (Ndn_Debugger.isEnabled()) {
                                var modeLabels = [];
                                $.each(Ndn_Debugger.getEnabledModes(), function(modeLabel) {
                                    modeLabels.push(modeLabel);
                                });
                                iframeSrc += '&ndn_debug=' + modeLabels.join(',');
                            }
                            
                            // Set the iframe element's "src" attribute appropriately, then append said element to the configurable <div> element
                            containerElement.append(iframe.attr('src', iframeSrc));
                        }
                        else {
                            // Append the iframe element to the configurable <div> element
                            containerElement.append(iframe);
                            
                            var content = 
                            embedTemplate
                            .replace(/\{{2}embedCode\}{2}/g, Ndn_Widget_Config.getEmbedCode(config, false, 'landing-page-embed-container'))
                            .replace(/\{{2}iframeId\}{2}/g, iframeId)
                            .replace(/\{{2}appOverrideSettings\}{2}/g, $.param(Ndn_App.getOverrideSettings()))
                            .replace(/\{{2}appUrl\}{2}/g, Ndn_App.getAppUrl())
                            .replace(/\{{2}embedCodeScriptTag\}{2}/g, Ndn_App.getEmbedCodeScriptTag(Ndn_App.getAppUrl()));
                            
                            // Write the appropriate content to the iframe
                            try {
                                var iframeDocument = iframe.get(0).contentWindow.document;
                                iframeDocument.open();
                                iframeDocument.write(content);
                                iframeDocument.close();
                            }
                            catch(e) {}
                        }
                        
                        // Indicate that this widget is embedded
                        containerElement
                        .addClass('ndn_embedded')
                        .removeClass('ndn_embedding');
                        
                        // Discontinue the rest of the embed process as the rest of the embed process is handled by the newly created iframe
                        return deferred.reject('iframeEmbed');
                    }
                }
                
                Ndn_App.integrateWidget(widget);
                
                // As soon as the split version model is available, check whether Chartbeat analytics are enabled for this product
                widget.on('splitVersionModelAvailable', function() {
                    widget.getSettings().done(function(settings) {
                        var chartbeatSettings = settings.settings.analytics.chartbeat;
                        
                        // If Chartbeat analytics is enabled and this product's "type" config setting is one that allows Chartbeat analytics to track it
                        if (chartbeatSettings.enabled && _.contains(['VideoPlayer/Default', 'VideoPlayer/Studio', 'VideoPlayer/16x9'], widget.get('config')['type'])) {
                            // Initialize this product's Chartbeat adapter, which relies on the split version model being available but not embedded yet
                            Ndn_Analytics_ChartbeatStrategy.enableForNdnProduct(widget, chartbeatSettings.uid);
                        }
                    });
                });
                
                // Make sure that the CSS needed for this widget is injected
                _injectCss();
                
                _handleConfigDimensions(widget, containerElement);
                
                widget.onceConfigIsComplete(function() {
                    widget.triggerEvent('configComplete', widget.get('config'), 'hook,analytics');
                });
                
                widget.getSettings().done($.proxy(function() {
                    widget.embedSplitVersion()
                    .done(function() {
                        // Indicate that the widget has been successfully embedded and then remove the class indicating that it is in the process of being embedded,
                        // IMPORTANT NOTE: Removing the "ndn_embedding" class AFTER adding the "ndn_embedded" class is important; see: Ndn_App.embedWidgetsFromContainersOnPage() method
                        containerElement
                        .addClass('ndn_embedded')
                        .removeClass('ndn_embedding');
                        
                        // TODO: Consider refactoring out the need for this "onceConfigIsComplete()" wrapper method, as I believe the config will always be complete by this point in time.
                        widget.onceConfigIsComplete(function() {
                            widget.trigger('widgetEmbedEnd'); // TODO: Consider merging this code with the .triggerEvent() method call below
                            widget.triggerEvent('widgetEmbedEnd', {}, 'hook,analytics');
                        });
                    });
                }, this))
                .fail(function(response) {
                    // If this widget's settings were unable to be successfully retrieved from the server, hide this widget's container element completely
                    widget.getContainerElement().attr('style', 'display:none !important');
                    
                    if (response && response.reason == 'storyVideoUnavailable') {
                        widget.triggerEvent('storyVideoUnavailable', {
                            providerId: widget.get('config')['providerId'],
                            providerStoryId: widget.get('config')['providerStoryId']
                        }, 'hook');
                    }
                });
            });
        }).promise();
    };
    
    /**
     * Inject the player suite's needed CSS (but only a maximum of one time)
     */
    var _injectCss = (function() {
        var _hasInjectedCss = false;
        
        return function() {
            if (!_hasInjectedCss) {
                // Inject our stylesheets
                if (Ndn_Widget.isBuilt()) {
                    if (document.createStyleSheet) {
                        document.createStyleSheet(_nw2e.appUrl + '/css/NdnEmbed.css');
                        document.createStyleSheet(_nw2e.appUrl + '/css/NdnEmbed2.css');
                    }
                    else {
                        $('head').append('<link class="_nw2eStyles" rel="stylesheet" type="text/css" href="' + _nw2e.appUrl + '/css/NdnEmbed.css" />');
                        $('head').append('<link class="_nw2eStyles" rel="stylesheet" type="text/css" href="' + _nw2e.appUrl + '/css/NdnEmbed2.css" />');
                    }
                    
                    // Apply responsive polyfill to our styles to support IE
                    respond.update();
                }
                else {
                    if (Ndn_Utils_UrlParser.getUrlVar('lessc') == 'dynamic') {
                        // If "lessc=dynamic" is found in the query string of the current page, dynamically compile the less CSS file
                        $('head').append('<link rel="stylesheet" type="text/css" href="' + _nw2e.appUrl + '/_build/lessc.php?path=/less/NdnEmbed.less" />');
                        $('head').append('<link rel="stylesheet" type="text/css" href="' + _nw2e.appUrl + '/_build/lessc.php?path=/less/NdnEmbed2.less" />');
                    }
                    else {
                        // Otherwise, output less CSS using less.js
                        $('head')
                        .append('<link rel="stylesheet/less" type="text/css" href="' + _nw2e.appUrl + '/less/NdnEmbed.less" />')
                        .append('<link rel="stylesheet/less" type="text/css" href="' + _nw2e.appUrl + '/less/NdnEmbed2.less" />')
                        .append('<script src="' + _nw2e.appUrl + '/js/lib/less.js"></script>');
                    }
                }
                
                // Indicate that this CSS has been injected
                _hasInjectedCss = true;
            }
        };
    })();
    
    /**
     * The Ndn_Widget class.
     * @class Ndn_Widget
     */
    var Ndn_Widget = Backbone.Model.extend(/** @lends Ndn_Widget */ {
        defaults: {
            'config': {},
            'templates': [],
            'templatePaths': [],
            
            /**
             * The config as originally provided (before any of the config is overridden by player services)
             * @var {Object}
             */
            'initialConfig': {},
            
            /**
             * List of recognizeable string values pertaining to how this video is placed
             * @var {Array<String>}
             */
            'placedBy': null,
            
            /**
             * The container element that this widget is embedded to
             * @var {DOMNode}
             */
            'containerElement': null,
            
            /**
             * Zero-based-index identifying this product embedded on the page
             * @var {int}
             */
            'embedIndex': null,
            
            /**
             * The widget's unique identifier on the webpage
             * @var {String}
             */
            'widgetUid': null,
            
            /**
             * Whether this widget has been attached to the window's dom yet (useful for when adding a video player to the view)
             * @var {Boolean}
             */
            'isAttached': false,
            
            /**
             * Whether this widget's config is complete and ready for use
             * @var {Boolean}
             */
            'configComplete': false,
            
            /**
             * The settings associated with this widget based upon its "widgetId" setting, which is retrieved from player services; 
             * see Ndn_PlayerServices.getWidgetSettings() method for this property's structured format
             * @var {Object}
             */
            'settings': null,
            
            /**
             * The loyalty program associated with the current widget (or null if no loyalty program is associated)
             * @var {Ndn_LoyaltyProgram}
             */
            'loyaltyProgram': null,
            
            /**
             * Whether this widget has been recognized as being positioned "below the fold" at least once
             * @var {Boolean}
             */
            'hasBeenPositionedBelowTheFold': false,
            
            /**
             * Whether this product's settings are already pending
             * @var {Boolean}
             */
            'settingsPending': false,
            
            /**
             * The next DFP "ppos" value to send concerning this widget
             * @var {int}
             */
            'dfpPodPositionValue': null,
            
            /**
             * The initial HTML of this product's container element before it began its embed process via the NDN Player Suite
             * @var {String}
             */
            'providedContainerElementHtml': null,
            
            /**
             * @var {Ndn_Widget}
             */
            'parentWidget': null,
            
            /**
             * @var {Ndn_Widget}
             */
            'splitVersionModel': null,
            
            /**
             * @var {Boolean}
             */
            'ptrBehavior': false,
            
            /**
             * Whether this product is being presented within a modal window created by the NDN Player Suite
             * @var {Boolean}
             */
            'isModal': false,
            
            /**
             * Whether this product utilizes ad studies, which override the placements of companion ads
             * @var {Boolean}
             */
            'usesAdStudies': false,
            
            /**
             * The main carousel of the Slider product.
             * @var {Object}
             */
            'carousel': null,
            
            /**
             * The optional secondary carousel of the Slider product which contains smaller thumbnails.
             * @var {Object}
             */
            'thumbnailCarousel': null
        },
        
        /**
         * @param {Object} config Structured data concerning the config settings of the widget
         * @param {Ndn_Widget} [parentWidget]
         * @memberof Ndn_Widget#
         * @lends Ndn_Widget
         */
        initialize: function(config, parentWidget) {
            this.set({
                'parentWidget': parentWidget || this,
                'initialConfig': _.extend({}, config),
                'config': config
            });
            
            this.set('dfpPodPositionValue', 1);
            
            if (this.isParentWidget()) {
                // If a loyalty program is specified in the config
                if (config['loyaltyProgramId']) {
                    this.set({
                        'loyaltyProgram': new Ndn_LoyaltyProgram({
                            'id': config['loyaltyProgramId'],
                            'customData': (function() {
                                try {
                                    return $.parseJSON(config['loyaltyProgramData']);
                                }
                                catch(e) {
                                    return {};
                                }
                            })()
                        })
                    });
                }
            }
        },
        
        /**
         * @param {String} eventName The name of the event to trigger
         * @param {Object} [eventData] The data to pass with the specified event to trigger
         * @param {String} [eventNamespaces] Comma-delimited namespaces to trigger this event for (such as "hooks,analytics")
         */
        triggerEvent: function(eventName, eventData, eventNamespaces) {
            // console.log('Ndn_Widget.trigger(', arguments, ');');
            
            if (!eventNamespaces) {
                Backbone.Model.prototype.trigger.apply(this, arguments);
            }
            else {
                var eventNamespaces = eventNamespaces.split(',');
                
                $.each(eventNamespaces, $.proxy(function(index, eventNamespace) {
                    eventNamespace = eventNamespace.replace(/\s/g, '');
                    Backbone.Model.prototype.trigger.call(this, eventNamespace + ':' + eventName, eventData);
                }, this));
            }
        },
        
        isNdnPlayerSuiteProduct: function() {
            return true;
        },
        
        /**
         * Initialize and embed the split version model's model (if needed) and view for this parent widget
         */
        embedSplitVersion: function() {
            return $.Deferred($.proxy(function(deferred) {
                var splitVersion = this.get('config')['splitVersion'];
                
                if (splitVersion) {
                    var widget = this;
                    
                    // Identify what type of widget is embedded to the dom via the "class" attribute of its container
                    widget.getContainerElement().addClass(Ndn_Widget.getDomClassIdentifier(widget.get('config')['splitVersion']));
                    
                    // Require and instantiate the needed model for the specified widget
                    var modelLocation = 'models/Ndn/Widget/' + splitVersion + 'Model';
                    
                    require([modelLocation], function(SplitVersionModel) {
                        // Instantiate the split version model to be embedded
                        var split = 
                            new SplitVersionModel(widget.get('config'))
                        .set({
                            'containerElement': widget.getContainerElement().get(0)
                        });
                        
                        split.setParentWidget(widget);
                        widget.set('splitVersionModel', split);
                        
                        // Instantiate the split version model's view
                        // Associate the widget with its view and vice versa
                        var SplitModelView = split.get('widgetView');
                        split.set('view', new SplitModelView({
                            'el': widget.getContainerElement().get(0),
                            'model': split
                        }));
                        
                        widget.trigger('splitVersionModelAvailable');
                        
                        split.embed().done(deferred.resolve);
                    });
                }
                else {
                    this.set('splitVersionModel', this);
                    
                    // Identify what type of widget is embedded to the dom via the "class" attribute of its container
                    this.getContainerElement().addClass(Ndn_Widget.getDomClassIdentifier(this.get('config')['type']));
                    
                    var WidgetView = this.get('widgetView');
                    
                    // Associate the widget with its view and vice versa
                    this.set('view', new WidgetView({
                        'el': this.getContainerElement().get(0),
                        'model': this
                    }));
                    
                    this.trigger('splitVersionModelAvailable');
                    
                    this.embed().done(deferred.resolve);
                }
            }, this)).promise();
        },
        
        /**
         * @param {Boolean} isModal Whether this product is being presented within a modal window created by the NDN Player Suite
         */
        setIsModal: function(isModal) {
            this.set('isModal', isModal);
        },
        
        /**
         * @return {Boolean} Whether this product is being presented within a modal window created by the NDN Player Suite
         */
        isModal: function() {
            return this.get('isModal');
        },
        
        /**
         * @return {Boolean} Whether this is the parent widget instance or not
         */
        isParentWidget: function() {
            return this.getParentWidget() === this;
        },
        
        /**
         * @return {Object} Structured data concerning the data available to the Ndn_EventHub module when it observes events triggered by this object. Format:
         * <pre>{
         *   data: Object, // Structured data that is passed each time via the Ndn_EventHub
         *   identifiers: [ // List of identifiers to use when describing events passing through the Ndn_EventHub module
         *       String, // This widget's container element's "id" attribute
         *       String, // The "#" character followed by this widget's embed index
         *   ]
         * }</pre>
         */
        getObservableData: function() {
            return {
                data: {
                    containerElement: this.getContainerElement().get(0),
                    embedIndex: this.get('embedIndex')
                },
                identifiers: [
                    this.getContainerElement().attr('id'),
                    '#' + this.get('embedIndex')
                ]
            };
        },
        
        /**
         * @param {Function} callback The closure to execute once this widget's config is complete and ready for use
         */
        onceConfigIsComplete: function(callback) {
            if (this.get('configComplete')) {
                callback();
            }
            else {
                this.once('configComplete', callback);
            }
        },
        
        /**
         * @param {String} templateName The name of a template belonging to the current widget
         * @return {String} The specified template's content
         */
        getTemplate: function(templateName) {
            var templates = this.get('templates');
            if (typeof templates[templateName] != 'undefined') {
                return templates[templateName];
            }
            else {
                return null;
            }
        },
        
        /**
         * Pre-requisite: This method assumes that the current widget/product's config is complete
         * @return {String} The identifier specifying what split version is being used
         */
        getSplitVersionId: function() {
            var widgetConfig = this.get('config');
            return widgetConfig['splitVersion'] || widgetConfig['type'];
        },
        
        /**
         * @see this.getPltValue()
         */
        getWidgetTypeId: function() {
            return this.getPltValue();
        },

        /**
         * @return {int} An integer that identifies certain scenarios about the provided config settings that is used for analytics, referred to as the "plt" value because it is passed in a parameter named "plt"
         */
        getPltValue: function() {
            return Ndn_Widget_Config.getPltValue(this.get('config'), this.getCachedSettings());
        }, 
        
        /**
         * @return {Boolean} Whether this product utilizes ad studies, which override the placements of companion ads
         */
        usesAdStudies: function() {
            return this.get('usesAdStudies');
        },
        
        /**
         * Ensure that this product's container element has its width and height dimensions up-to-date
         */
        synchronizeContainerDimensions: function() {
            this.trigger('containerResize');
        },
        
        getSplitVersionModel: function() {
            return this.get('splitVersionModel');
        },
        
        /**
         * @return {Boolean} Whether this product is exercising the PTR behavior
         */
        isPtr: function() {
            return this.getParentWidget().get('ptrBehavior') === true;
        },
        
        setIsPtr: function(toggle) {
            this.getParentWidget().set('ptrBehavior', toggle);
        },
        
        /**
         * @return {Ndn_Widget} The parent widget of the current widget
         */
        getParentWidget: function() {
            return this.get('parentWidget');
        },
        
        /**
         * @param {Ndn_Widget} [parent] The widget to become the parent widget of the current widget
         */
        setParentWidget: function(parent) {
            this.set('parentWidget', parent);
        },
        
        /**
         * Placeholder method responsible for embedding the root template
         */
        embed: function() {
            // console.log('Ndn_Widget.embed();');				
        },
        
        /**
         * @param {jQueryElement} [containerElement] If provided, this dom element is used to find its "fold position ID" instead of using the widget's container dom element, which is used by default
         * @return {String} The number identifying this widget's "fold position"; "0" when the widget is embedded by solely using the iframe element;
         * "1" when the widget is embedded using JavaScript and is at least 50% above the fold; "2" when the widget is embedded using JavaScript and is not
         * at least 50% above the fold; "3" when the widget is above the fold and has been recognized as being below the fold before
         */
        getFoldPositionId: function(containerElement) {
            if (typeof containerElement == 'undefined') containerElement = $(this.get('containerElement'));
            
            if (Ndn_Widget.isEmbeddedByIframe()) {
                return '0';
            }
            
            var fold = $(window).height() + $(window).scrollTop();
            var aboveTheFold = fold >= containerElement.offset().top + Math.floor(containerElement.height() / 2);
            
            var result;
            if (aboveTheFold) {
                result = '1'; // this.get('hasBeenPositionedBelowTheFold') ? '3' : '1';
            }
            else {
                this.set({
                    'hasBeenPositionedBelowTheFold': true
                });
                result = '2';
            }
            
            return result;
        },
        
        /**
         * @param {jQueryElement} [containerElement] If provided, this dom element is used to find its "fold position ID" instead of using the widget's container dom element, which is used by default
         * @return {Boolean} Whether or not at least 50% of the containerElement is visible.
         */
        isInView: function(containerElement) {
            if (typeof containerElement == 'undefined') containerElement = $(this.get('containerElement'));
            
            if (Ndn_Widget.isEmbeddedByIframe()) {
                return false;
            }
            
            var fold = $(window).height() + $(window).scrollTop();
            var aboveTheFold = fold >= containerElement.offset().top + Math.floor(containerElement.height() / 2);
            
            var scrolledAboveTop = $(document).scrollTop() >= containerElement.offset().top + Math.floor(containerElement.height() / 2);
            
            if (!aboveTheFold || scrolledAboveTop) {
                return false; 
            }
            
            return true;
        },
        
        /**
         * Whether thumbnails are displayed.  Currently only happens in PTR, where this method is overridden in the Model.
         * @return {Boolean}
         */
        getThumbnailsEnabled: function() {
            return false;
        },
        
        /**
         * @return {Boolean} Whether this widget should be muted.
         */
        shouldBeMuted: function() {
            if (this != this.getSplitVersionModel()) {
                return this.getSplitVersionModel().shouldBeMuted();
            }
            
            // If about to load an RMM and this widget's config has sound turned off for RMMs, mute the player; otherwise, unmute the player and set its volume to 75%
            if (this.get('videoPlayer').isShowingRmm() && !this.get('config')['rmmSoundOn']) {
                return true;
            } else {
                return false;
            }
        },
        
        /**
         * @return {Boolean} Whether this widget is currently embedded (whether it has a view object yet)
         */
        isEmbedded: function() {
            return this.get('view') && this.get('view').el;
        },
        
        /**
         * @param {Function} callback The closure to execute once this widget has begun getting embedded on the page (once the widget has been assigned a view object and that view object has a container element)
         */
        onceEmbedded: function(callback) {
            if (this.isEmbedded()) {
                callback();
            }
            else {
                this.once('embed', callback);
            }
        },
        
        /**
         * @return {DOMNode}
         */
        getContainerElement: function() {
            return $(this.get('containerElement'));
        },
        
        getCachedSettings: function() {
            return this.isParentWidget()
                ? this.get('settings')
                : this.getParentWidget().getCachedSettings();
        },
        
        /**
         * @return {Object} Structured data concerning this product's settings that have been processed in order to finalize what this product's settings are. Format:
         * <pre>{
         *  'defaultWidgetConfig': {
         *      ...
         *      'splitVersion': String, // The 
         *  },
         *  ... // Settings returned exactly how they are specified via Ndn_PlayerServices.getWidgetSettings()
         *      'settings': {
         *          ...
         *          'behavior': {
         *              ...
         *              'rmm': {
         *                  'enabled': Boolean, // Whether RMM is enabled for this product
         *                  'continuousPlay': Integer, // The number of RMM 6-second bumper videos to play continuously
         *              },
         *              'ptr': {
         *                  'enabled': Boolean, // Whether PTR is enabled for this product
         *                  'previewLength': Integer, // The length of the initial preview for when PTR is enabled
         *                  'playsInIframe': Boolean, // Whether the PTR behavior will always be executed when embedded within an iframe
         *              }
         *          },
         *          'browser': Object, // Structured data concerning settings determined at the browser-level (see Ndn_Widget_Config.getBrowserSettings() for more details)
         *          ...
         *      }
         *  ...
         * }</pre>
         */
        processSettings: function(settings) {
            var result = settings;
            
            // This product's config
            var config = this.get('config');
            
            // Whether this product was embedded as a "VideoLauncher/Slider" product
            var isSlider = this.getParentWidget().get('config')['type'] == 'VideoLauncher/Slider';
            
            // The behavior settings
            var behaviorSettings = result.settings.behavior,
                providedBehaviorSettings = behaviorSettings.provided;
            
            // Process the RMM settings
            (function() {
                // Initialize the "rmm" behavior settings
                behaviorSettings.rmm = {};
                
                // Settings provided by player services
                var providedSettings = providedBehaviorSettings.rmm; // rmmSettings.provided;
                
                // Whether RMM is enabled for this product
                var rmmEnabled = Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically();
                
                // Whether the current page is the current website's homepage
                var isOnHomepage = _.contains(['/', ''], window.location.pathname + '');
                
                // Whether the provided RMM continuous play functionality provided should be used
                var shouldUseProvidedRmmContinuousPlay = rmmEnabled && isSlider && !isOnHomepage && providedSettings.continuousPlayPercentage > 0 && (Math.random() * 100 <= providedSettings.continuousPlayPercentage);
                
                // If rotating RMM is enabled, then make sure PTR is disabled
                if (rmmEnabled && shouldUseProvidedRmmContinuousPlay && providedSettings.continuousPlay > 0) result.defaultWidgetConfig.splitVersion = '';
                
                if (Ndn_Debugger.inMode('rmmCp')) {
                    console.log('providedSettings ==', providedSettings);
                    console.log('rmmEnabled ==', rmmEnabled);
                    console.log('isOnHomepage ==', isOnHomepage);
                    console.log('isSlider ==', isSlider);
                    console.log('shouldUseProvidedRmmContinuousPlay ==', shouldUseProvidedRmmContinuousPlay);
                }
                
                result.settings.behavior.rmm = {
                    'enabled': rmmEnabled,
                    'continuousPlay': shouldUseProvidedRmmContinuousPlay
                        ? providedSettings.continuousPlay
                        : 0
                };
                
                // TODO: Refactor this out, add unit tests
                // For backwards-compatibility
                _.extend(result.settings.rmm, result.settings.behavior.rmm);
            })();
            
            // Process PTR behavior
            behaviorSettings.ptr = {
                'enabled': Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically() && result.defaultWidgetConfig.splitVersion == 'VideoPlayer/PTR',
                'previewLength': providedBehaviorSettings.ptr.previewLength,
                'playsInIframe': providedBehaviorSettings.ptr.playsInIframe
            };
            
            // Process the floating behavior (that allows this product to get placed in a float container when it goes out of the browser's view)
            behaviorSettings.float = {
                'enabled': this.getParentWidget().get('config')['type'] == 'VideoPlayer/Default'
                    && providedBehaviorSettings.float.enabled
                    && !Ndn_Widget.isEmbeddedByIframe()
                    && Ndn_Utils_UserAgent.isDesktop(),
                'initialPosition': providedBehaviorSettings.float.initialPosition
            };
            
            // Process the "playOnInViewToggle" behavior
            result.settings.behavior.playOnInViewToggle = {
                'enabled': providedBehaviorSettings.playOnInViewToggle.enabled && config['type'] == 'VideoPlayer/Default'
            };
            
            // TODO: Remove the circular logic here
            // Add the browser settings for reference
            result.settings.browser = Ndn_Widget_Config.getBrowserSettings(result);
            
            return result;
        },
        
        /**
         * @return {jQuery.Deferred} A deferred object that responds with this widget's settings
         */
        getSettings: function() {
            // console.log('settingsPending ==', this.get('settingsPending'));
            // console.log('Ndn_Widget.getSettings(), this.get("settings") ==', this.get('settings'), '; config ==', this.get('config'));
            
            if (!this.isParentWidget()) {
                return this.getParentWidget().getSettings();
            }
            
            return $.Deferred($.proxy(function(deferred) {
                if (this.get('settings')) {
                    // If the settings have already been fetched before, return those fetched settings
                    deferred.resolve(this.get('settings'));
                }
                else if (this.get('settingsPending')) {
                    // Once the settings are definitively received, then resolve this jQuery.Deferred object
                    this.once('settingsReceived', function(settings) {
                        deferred.resolve(settings);
                    });
                }
                else {
                    // Indicate that the call to fetch this product's settings has been made (so that future calls to this method do not make a new call to player services)
                    this.set('settingsPending', true);
                    
                    // Now actually make the player services call to fetch the product's settings
                    Ndn_PlayerServices.getWidgetSettings(this)
                    .done($.proxy(function(response) {
                        // When this widget receives its widget settings from player services, use that information to finalize this product's config settings
                        this.set('config', Ndn_Widget_Config.getFinalizedConfig(this.get('config'), response['defaultWidgetConfig'], response)); // this.set('config', Ndn_Widget_Config.applyConfigDefaults(this.get('config'), response['defaultWidgetConfig']));
                        
                        // Finalize this product's settings
                        response = this.processSettings(response);
                        
                        // Closure concerning the conclusion logic to execute once this product's settings have been finalized
                        var concludePlayerSettings = $.proxy(function() {
                            if (Ndn_Debugger.inMode('playerSettings')) {
                                console.log('[Ndn_Widget] playerSettings ==', response);
                            }
                            
                            // Store this product's processed settings
                            this.set('settings', response);
                            
                            // Indicate that this widget's config is complete and is ready for use
                            this.set('configComplete', true);
                            this.trigger('configComplete', this.get('config'));
                            
                            deferred.resolve(this.get('settings'));
                            
                            // Indicate that the settings have been received
                            this.trigger('settingsReceived', this.get('settings'));
                        }, this);
                        
                        // TODO: Additional logic concerning whether this is a Single player will be needed once
                        //       multiple players on a page is in scope.
                        concludePlayerSettings();
                    }, this))
                    .fail(function(response) {
                        deferred.reject(response);
                    });
                }
            }, this)).promise();
        },
        
        /**
         * Pre-requisite: This widget's settings must have already been received and the split version model must already be instantiated at the point when this method is called
         * @return {Boolean} Whether this widget and the browser currently being used is able to play RMM
         */
        isAbleToPlayRmm: function() {
            return true; // _.contains(['VideoLauncher/Slider', 'VideoLauncher/Playlist300x250'], this.getSplitVersionModel().get('config')['type']);
        },
        
        /**
         * TODO: Document and finish coding this method.
         */
        getDebugInfo: function() {
            if (this.get('videoPlayer')) {
                return this.get('videoPlayer').getDebugInfo();
            }
        },
        
        /**
         * @return {String} The "id" attribute of the container dom element that is responsible for holding this widget's player (if this widget has one)
         */
        getPlayerContainerElementId: function() {
            return this.getParentWidget().get('widgetUid') + '-player';
        },
        
        /**
         * @return {int} The next DFP "ppos" value to be used by this widget (by calling this method, the "ppos" value automatically gets incremented for the next time it is called)
         */
        getDfpPodPositionValue: function() {
            var result = this.get('dfpPodPositionValue');
            this.set('dfpPodPositionValue', result + 1);
            return result;
        },
        
        /**
         * @return {Boolean} Whether or not this widget can launch to Studio instead of the default NDN player.
         */
        isLaunchToStudioEnabled: function() {
            return !!this.getParentWidget().get('settings').launchToStudio && (this.getParentWidget().get('settings').launchToStudio.enabled || false)
        },
        
        /**
         * @param {Object} videoConfig Structured data describing the video for which a landing page url is generated.  'videoId' is recommended at a minimum.
         * Anything in the defaultConfig can also be overirdden.
         * @return {String} The URL to the page responsible for launching a product in a new window
         */
        getLandingPageUrl: function(videoConfig) {
            var widgetConfig   = this.get('config'),
                defaultConfig  = {
                    'type': 'VideoPlayer/16x9',
                    'trackingGroup': widgetConfig.trackingGroup,
                    'widgetId': widgetConfig.widgetId,
                    'playlistId': widgetConfig.playlistId,
                    'siteSection': widgetConfig.siteSection,
                    'embedOriginUrl': encodeURIComponent(Ndn_Widget.getEmbedOriginUrl())
                },
                videoConfig    = _.extend(videoConfig, defaultConfig),
                landingPageUrl = '';
            
            // Make sure the videoId is a string.
            videoConfig.videoId = '' + videoConfig.videoId;
            
            if (this.isLaunchToStudioEnabled()) {
                landingPageUrl = this.getParentWidget().get('settings').launchToStudio.url;
                
                var params = {
                    videoId: videoConfig.videoId,
                    playlistId: videoConfig.playlistId
                };
                
                landingPageUrl += '?' + $.param(params);
            }
            else {
                landingPageUrl = 'http://' + Ndn_Widget.getAppDomain() + '/';
                landingPageUrl += '?type=' + videoConfig.type;
                landingPageUrl += '&' + $.param(_.omit(videoConfig, 'type'));
            }
            
            return landingPageUrl;
        },
        
        getPlacedByAnalyticsString: function() {
            var placedByMap = {
                'jsapi': 1,
                'ppembed': 2,
                'wordpress': 3,
                'fromSlider': 4
            };
            
            var placedByNumbers = [];
            $.each(this.get('placedBy'), function() {
                var placedByEnumValue = this;
                if (typeof placedByMap[placedByEnumValue] != 'undefined') placedByNumbers.push(placedByMap[placedByEnumValue]); 
            });
            
            return '.' + placedByNumbers.join('.') + (placedByNumbers.length ? '.' : '');
        },
        
        /**
         * Extract video data for the launchers to avoid changing the original objects.
         * @param {Array} list An array of Video Objects as returned from the VideoPlayer.
         * @return {Array} A list of new data Objects with all the data necessary to display videos in launchers. 
         */
        getVideoDataForLaunchers: function(list) {
            var dataList = [];
            
            _.each(list, $.proxy(function(video) {
                var data = {
                    'videoId': video.videoId,
                    'videoTitle': video.videoTitle,
                    'landingPageUrl': this.getLandingPageUrl({'videoId': video.videoId}),
                    'thumbnailUrl': video.thumbnailUrl,
                    'producer':  {
                        'logoUrl': video.producer.logoUrl,
                        'name': video.producer.name
                    },
                    'stillImageUrl': video.stillImageUrl
                };
                
                dataList.push(data);
            }, this));
            
            return dataList;
        },
        
        /**
         * Destroy this embedded product
         */
        destruct: function() {
            var containerElementId = this.getContainerElement().attr('id');
            
            Ndn_App.unintegrateWidget(this);
            this.getContainerElement().remove();
            this.triggerEvent('destruct', {containerElementId: containerElementId}, 'hook,analytics');
        }
    },
    {
        /**
         * @return {String} Any suffix to include to the end of URLs to other pages within the same application
         */
        getAppUrlSuffix: function() {
            return Ndn_App.getAppUrlSuffix();
        },
        
        /**
         * @param {String} hookId The hook identifier
         * @param {Function(Object)} hookCallback The handler function for when the hook occurs
         */
        addHook: function(hookId, hookCallback) {
            return Ndn_App.addHook(hookId, hookCallback);
        },
        
        /**
         * @return {String} The URL of the page responsible for embedding widgets
         */
        getEmbedOriginUrl: function() {
            return Ndn_App.getEmbedOriginUrl(arguments);
        },
        
        /**
         * @return {Ndn_AdsTargeting} The instance responsible for managing all ads targeting
         */
        getAdsTargeting: function() {
            return Ndn_App.getAdsTargeting(arguments);
        },
        
        /**
         * Determine if we're using an optimized build of this app
         *
         * @return {Boolean} True if we're in a "built" environment
         */
        isBuilt: function() {
            return Ndn_App.isBuilt(arguments);
        },
        
        /**
         * @return {Boolean} Whether the current page is being served through the browser using SSL
         */
        isOnSsl: function() {
            return Ndn_App.isOnSsl();
        },
        
        /**
         * @return {String} The application's base URL
         */
        getAppUrl: function() {
            return Ndn_App.getAppUrl(arguments);
        },
        
        /**
         * @return {String} The domain name of this application's URL
         */
        getAppDomain: function() {
            return Ndn_App.getAppDomain(arguments);
        },
        
        /**
         * @return {Boolean} Whether this widget is embedded solely by using an <iframe /> element
         */
        isEmbeddedByIframe: function() {
            return Ndn_App.isEmbeddedByIframe(arguments);
        },
        
        /**
         * @return {Boolean} Whether the current page is a page within the current application, ie. "/landing-page.html?..."
         */
        isOnAppPage: function() {
            return Ndn_App.isOnAppPage(arguments);
        },
        
        /**
         * return {int} The next zero-based-index describing which embedded product the next one will be
         */
        getNextEmbedIndex: function() {
            return _nextEmbedIndex++;
        },
        
        /**
         * @param {String} widgetType The 'type' as specified by a widget's config data, ie. "Default"
         * @return {String} The class identifier for a widget's embed container so a widget's template can be styled correctly via CSS
         */
        getDomClassIdentifier: function(widgetType) {
            return 'ndn_widget_' + widgetType.charAt(0).toUpperCase() + widgetType.slice(1).replace('/',  '-');
        },
        
        /**
         * @param {(HTMLElement|String)} containerElement Either the actual container dom element for the widget to be embedded or the "id" attribute of said element
         * @param {Object} [config] Structured data describing the widget to be embedded; any config settings provided on the container dom element override these provided config settings
         * @param {Array<String>} [placedBy] List of recognizeable string values pertaining to how this video is placed
         * @return {jQuery.Deferred} Resolves when a product begins its embed process; TODO: Document scenarios where it fails!
         */
        embed: function(containerElement, config, placedBy) {
            placedBy = placedBy || [];
            
            // Determine whether a dom element was provided and if not, keep track of the container element's "id" attribute that was passed instead
            var domElementProvided = (typeof containerElement.nodeType != 'undefined' && containerElement.nodeType == 1),
                containerElementId = !domElementProvided ? containerElement : null;
            
            // Wrap container element we are embedding to in jQuery
            containerElement = domElementProvided
                ? $(containerElement)
                : $('#' + containerElementId);
            
            // Get the string that describes the complete HTML of the provided container element to get embedded
            var providedContainerElementHtml = _getElementFullHtml(containerElement.get(0));
            
            // If the container element cannot be found, throw exception
            if (!containerElement.length) {
                if (domElementProvided) {
                    throw 'Invalid dom element provided for embed';
                }
                else {
                    throw 'Unable to find dom element with id "' + containerElementId + '" for embed';
                }
            }
            
            return $.Deferred(function(deferred) {
                Ndn_Widget_Config.processConfigFromElement(containerElement, config).done(function(config) {
                    _embedContainer(containerElement, config, {
                        providedContainerElementHtml: providedContainerElementHtml,
                        placedBy: placedBy
                    })
                    .done(function(widget) {
                        deferred.resolve(widget);
                    })
                    .fail(function() {
                        deferred.reject();
                    });
                });
            }).promise();
        }
    });
    
    // Remember that the Ndn_Widget library has been loaded and save a reference to it in the global "_nw2e" variable
    _nw2e.Ndn_Widget = Ndn_Widget;
    
    return Ndn_Widget;
});
