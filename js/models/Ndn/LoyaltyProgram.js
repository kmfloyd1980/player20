define([
		'backbone',
		'jquery',
		'mustache'
	],
	function(Backbone, $, Mustache) {
		var _getAjaxUrlTemplate = function(programId) {
			if (programId == 'ndntritonservice') {
				return 'http://loyalty.newsinc.com/ndntritonservice.svc/GetCredit/{{memberId}}/{{siteId}}';
			}
		};
		
		var _getAjaxUrl = function(programId, customData) {
			var template = _getAjaxUrlTemplate(programId);
			return Mustache.to_html(_getAjaxUrlTemplate(programId), customData);
		};
	
		return Backbone.Model.extend({
			defaults: {
				/**
				 * @var {String}
				 */
				'id': null,
				
				/**
				 * @var {Object}
				 */
				'customData': null,
				
				/**
				 * The ajax URL to hit when the loyalty program is executed
				 * @var {String}
				 */
				'ajaxUrl': ''
			},
			
			initialize: function() {
				Backbone.Model.prototype.initialize.apply(this, arguments);
				
				this.set({
					'ajaxUrl': _getAjaxUrl(this.get('id'), this.get('customData'))
				});
			},
			
			execute: function() {
				// Make an ajax call for this loyalty program so that this video's ad view may be recorded
				$.getJSON(this.get('ajaxUrl'), function(response) {});
			}
		});
	}
);