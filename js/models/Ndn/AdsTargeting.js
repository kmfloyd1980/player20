/**
 * This is a class that must only be initialized by Ndn_Widget to act as a hub for all widgets and all their needed ads targeting.
 */
define([
        'jquery',
		'backbone',
		'models/Ndn/Timer',
		'models/Ndn/AdsTargeting/LegolasMedia',
		'models/Ndn/AdsTargeting/Lotame'
	],
	function(
		$,
		Backbone,
		Ndn_Timer,
		Ndn_AdsTargeting_LegolasMedia,
		Ndn_AdsTargeting_Lotame
	) {
		"use strict";

		return Backbone.Model.extend({
			defaults: {
				/**
				 * Types ads targeting being used
				 * @var {Array<Ndn_AdsTargeting_Abstract>}
				 */
				'types': {
					'legolas': new Ndn_AdsTargeting_LegolasMedia(),
					'lotame': new Ndn_AdsTargeting_Lotame()
				},
				
				/**
				 * Whether segments data has been received or receiving data has been attempted but has timed out
				 * @var {Boolean}
				 */
				'isDeliverable': false,
				
				/**
				 * The timeout timer to indicate that the attempt at getting this ads targeting data has timed out
				 * @var {Ndn_Timer}
				 */
				'timeoutTimer': null
			},
			
			initialize: function() {
				// Get the number of different types of ads targeting
				var numberOfTypesToDeliver = 0;
				$.each(this.get('types'), function(index) {
					numberOfTypesToDeliver++;
				});

				// Once all the ads targeting types are deliverable, indicate that this is so
				var self = this;
				$.each(this.get('types'), function() {
					this.onceIsDeliverable(function() {
						if (--numberOfTypesToDeliver == 0) {
							self.trigger('deliver');
						}
					});
				});

				// Indicate that all ads targeting types are deliverable once the 'deliver' event is heard
				this.on('deliver', $.proxy(function() {
					this.set({
						'isDeliverable': true
					});
				}, this));
				
				// Place a half-second timeout waiting on all ads targeting types
				this.set({
					'timeoutTimer': new Ndn_Timer($.proxy(function() {
						this.trigger('deliver');
					}, this), 500).start()
				});
			},
			
			/**
			 * @return {Boolean} Whether all the ads targeting types are deliverable
			 */
			isDeliverable: function() {
				return this.get('isDeliverable');
			},
			
			/**
			 * @param {Function} callback The closure to execute once all ads targeting types are deliverable
			 */
			onceIsDeliverable: function(callback) {
				if (this.isDeliverable()) {
					callback();
				}
				else {
					this.once('deliver', callback);
				}
			},
			
			/**
			 * @return {?Ndn_AdsTargeting_Abstract}
			 */
			getInstance: function(type) {
				return this.get('types')[type] || null;
			}
		});
	}
);