define([
		'backbone',
		'jquery'
	],
	function(Backbone, $) {
		/**
		 * Structured data concerning playlist data that has been fetched from player services
		 * @var {Object}
		 */
		var _cachedPlaylistsData = {};
		
		/**
		 * Structured data concerning cached isolated video data not tied to any particular playlist in the response received from player services. Format:
		 * <pre>{
		 * 	int: Object, // The video's database id => the video's structured data
		 * 	... // All other cached video data
		 * }</pre>
		 * @var {Object}
		 */
		var _cachedVideosData = {};
		
		return {
			/**
			 * @param {Object} playlistData The playlist data to cache
			 * @param {int} playlistItemsLimit The number of playlist items this playlist's data's response was limited to
			 */
			cachePlaylistData: function(playlistData, playlistItemsLimit) {
				// If this playlist data is cacheable
				if (playlistData.playlistId) {
					var cacheNeedsUpdate = typeof _cachedPlaylistsData[playlistData.playlistId] == 'undefined' || 
						(!_cachedPlaylistsData[playlistData.playlistId].isCompletedLoaded && _cachedPlaylistsData[playlistData.playlistId].items.length < playlistData.items.length);
					
					if (cacheNeedsUpdate) {
						playlistData.isCompletelyLoaded = (playlistData.items.length < playlistItemsLimit);
						_cachedPlaylistsData[playlistData.playlistId] = playlistData;
						
						// Cache each of the playlist items
						$.each(playlistData.items, $.proxy(function(index, videoData) {
							this.cacheVideoData(videoData);
						}, this));
					}
				}
			},
			
			/**
			 * @param {int} playlistId The database id of the playlist to receive from cache (if it has been cached)
			 * @param {int} playlistItemsLimit The number of playlist items that need to be cached
			 * @return {?Object} Cached structured data concerning the playlist that has been completely loaded or has at least the number of playlist items 
			 * specified by the "playlistItemsLimit" parameter cached (returns null otherwise)
			 * 
			 * TODO: If the number of playlist items exceeds the limit, the items are truncated to only include up to the limit specified.
			 */
			getPlaylistData: function(playlistId, playlistItemsLimit) {
				if (typeof _cachedPlaylistsData[playlistId] != 'undefined' && (_cachedPlaylistsData[playlistId].isCompletelyLoaded || _cachedPlaylistsData[playlistId].items.length >= playlistItemsLimit)) {
					/*
					var result = $.extend({}, _cachedPlaylistsData[playlistId]);
					result.items = result.items.splice(0, playlistItemsLimit);
					return result;
					*/
					return _cachedPlaylistsData[playlistId];
				}
				else {
					return null;
				}
			},
			
			/**
			 * @param {Object} videoData Structured data concerning an isolated video. Format:
			 * <pre>{
			 * 	'videoId': int, // The video's database id
			 * 	... // The rest of this video's structured data
			 * }</pre>
			 */
			cacheVideoData: function(videoData) {
				if (typeof _cachedVideosData[videoData.videoId] == 'undefined') {
					_cachedVideosData[videoData.videoId] = videoData;
				}
			},
			
			/**
			 * @return {Object} Structured data concerning an isolated video that has been cached (or null if it has not been cached). Format:
			 * <pre>{
			 * 	'videoId': int, // The video's database id
			 * 	... // The rest of this video's structured data
			 * }</pre>
			 */
			getVideoData: function(videoId) {
				if (typeof _cachedVideosData[videoId] != 'undefined') {
					return _cachedVideosData[videoId];
				}
			},
			
			getCache: function() {
				return _cachedVideosData;
			}
		};
	}
);