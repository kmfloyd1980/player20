var _nw2e = _nw2e || [];

/**
 * This is a class that must only be initialized by Ndn_App to act as a hub for all widgets and all their needed analytics handling.
 */
define([
        'jquery',
		'underscore',
		'backbone',
		'models/Ndn/Utils/UrlParser',
		'models/Ndn/Utils/UserAgent',
		'models/Ndn/Utils/BrowserStorage',
		'models/Ndn/Analytics/Ndn',
		'models/Ndn/Analytics/Quantcast',
		'models/Ndn/Analytics/Google'
	],
	function(
		$,
		_,
		Backbone,
		Ndn_Utils_UrlParser,
		Ndn_Utils_UserAgent,
		Ndn_Utils_BrowserStorage,
		Ndn_Analytics_Ndn,
		Ndn_Analytics_Quantcast,
		Ndn_Analytics_Google
	) {
		if (_nw2e.Ndn_Analytics) {
			return _nw2e.Ndn_Analytics;
		}
		else {
			_nw2e.Ndn_Analytics = {};
		}
	
		/**
		 * @var {String} Unique identifier specifying this page view's session (using version 4 UUID)
		 */
		var _pageViewSessionId;
	
		/**
		 * @var {String} Unique identifier specifying the user(using version 4 UUID)
		 */
		var _uniqueUserId = '';
		
		// The prefix to use, which may specify a specific analytics environment to use (or the empty string for production)
		var _analyticsEnvPrefix = (function() {
			var result = Ndn_Utils_UrlParser.getUrlVar('ndn_analyticsEnv') || Ndn_Utils_UrlParser.getUrlVar('analyticsServerPrefix') || '';
			result = result.replace(/[^a-zA-Z0-9]/g, '');
			return result ? result + '-' : '';
		})();

		/**
		 * @return {String} UUID v4
		 */
		var _generateUuid = function() {
			return ((typeof(window.crypto) != 'undefined' && 
            			typeof(window.crypto.getRandomValues) != 'undefined') ?
    		function() {
		        // If we have a cryptographically secure PRNG, use that
		        // http://stackoverflow.com/questions/6906916/collisions-when-generating-uuids-in-javascript
		        var buf = new Uint16Array(8);
		        window.crypto.getRandomValues(buf);
		        var S4 = function(num) {
		            var ret = num.toString(16);
		            while(ret.length < 4){
		                ret = "0"+ret;
		            }
		            return ret;
		        };
		        return (S4(buf[0])+S4(buf[1])+"-"+S4(buf[2])+"-4"+S4(buf[3]).slice(0,3)+"-"+S4(buf[4])+"-"+S4(buf[5])+S4(buf[6])+S4(buf[7]));
		    }

		    :

		    function() {
		        // Otherwise, just use Math.random
		        // http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/2117523#2117523
		        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		            return v.toString(16);
		        });
		    })()
		};
		
		/**
		 * Attempts to fetch the unique user ID and page view session ID via an ajax call to NDN's analytics server. If the ajax call times out after
		 * x-number of milliseconds, then a page view session ID is generated client-side. If the unique user ID remains the empty string until it is
		 * retrieved via the ajax call (if it ever is).
		 * @var {jQuery.Deferred}
		 */
		var _fetchIds = (function() {
			return $.Deferred(function(deferred) {
				_nw2e.onReady(function() {
					// Timeout after 2 seconds by rejecting this deferred object
					setTimeout(function() {
						deferred.reject();
					}, 2000);
					
					// Pass along the unique user ID if it is being stored client-side
					_uniqueUserId = Ndn_Utils_BrowserStorage.get('ndn_uut');
					$.getJSON(window.location.protocol + '//' + _analyticsEnvPrefix + 'c.newsinc.com/getids?uid=' + (_uniqueUserId || '') + '&callback=?', function(data) {
						// If data is not returned, then reject this deferred object
						if (!data) deferred.reject();
						
						// Update the unique user ID with data returned from this ajax call
						_uniqueUserId = data.uid;
						
				    	deferred.resolve({
				    		'uniqueUserId': data.uid,
				    		'pageViewSessionId': data.insid
				    	});
					});
				});
			}).promise()
			.fail(function() {
				// console.log('generating UUID client-side...');
				
				// Fallback by generating a page view session ID on the client-side
				_pageViewSessionId = _generateUuid();
			})
			.done(function(result) {
				// console.log('receiving UUID from server...');
				
				_uniqueUserId = result['uniqueUserId'];
				_pageViewSessionId = _pageViewSessionId || result['pageViewSessionId'];
				
				// Store the unique user ID value in a cookie
				(function(cookieName, cookieValue) {
		    	    var expires = new Date();
		    	    expires.setDate(expires.getDate() + (20 * 356));
		    	    document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expires.toGMTString() + ';domain=.newsinc.com;path=/';
		    	})("ANALYTICS_USER_TOKEN", _uniqueUserId);
				
				// Store the unique user ID value in local storage (or as a fallback, a cookie on the distributor's domain)
				Ndn_Utils_BrowserStorage.set('ndn_uut', _uniqueUserId, 256 * 10);
			});
		})();
		
		/**
		 * The "PPID" value that gets passed to DFP
		 * @var {String}
		 */
		var _ppidValue = _generateUuid().replace(/-/g, '');
		
		/**
		 * The observable object to hijack its .trigger() and .on() methods from for this library
		 * @var {Backbone.Model}
		 */
		var _observable = new Backbone.Model();
		
		var Ndn_Analytics = _.extend(_nw2e.Ndn_Analytics, {
			/**
			 * @return {jQuery.Deferred}
			 */
			getIds: function() {
				return $.Deferred(function(deferred) {
					_fetchIds.always(function() {
						deferred.resolve({
							'pageViewSessionId': _pageViewSessionId,
							'uniqueUserId': _uniqueUserId
						})
					});
				}).promise();
			},
			
			getUniqueUserId: function() {
				return _uniqueUserId;
			},
			
			getPageViewSessionId: function() {
				return _pageViewSessionId;
			},
			
			getPpidValue: function() {
				return _ppidValue;
			},

			trigger: function() {
				return Backbone.Model.prototype.trigger.apply(_observable, arguments);
			},
			
			on: function() {
				return Backbone.Model.prototype.on.apply(_observable, arguments);
			},
			
			once: function() {
				return Backbone.Model.prototype.once.apply(_observable, arguments);
			}
		});
		
		// Initialize each of the types of reporting that is done within the application
		$.each([
			Ndn_Analytics_Quantcast,
			Ndn_Analytics_Ndn,
			Ndn_Analytics_Google
		], function() {
			this.initialize(Ndn_Analytics);
		});
		
		return Ndn_Analytics;
	}
);
