var _nw2e = _nw2e || [];
var _ndnq = _ndnq || [];

define([
    'jquery',
    'underscore',
    'backbone',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/Hooks',
    'models/Ndn/Analytics',
    'models/Ndn/Debugger'
], function(
	$,
	_,
	Backbone,
	Ndn_Utils_UrlParser,
	Ndn_Hooks,
	Ndn_Analytics,
	Ndn_Debugger
) {
	// Determine whether debugging is enabled for the NDN Player Suite
	var debugEnabled = Ndn_Debugger.isEnabled(), // (debugFlag in hashParams) && hashParams[debugFlag] != '0' && (hashParams[debugFlag] + '').toLowerCase() != 'false',
		debugEvents = Ndn_Debugger.inMode('events'), // debugEnabled && (hashParams[debugFlag] + '').toLowerCase() == 'events',
		_storeEventData = debugEnabled;
	
	// alert('debugEvents == ' + debugEvents);
	
	// Only log all possible events passing through the event hub when "#ndn_debug=events" is present in the URL
	if (debugEvents) {
		_ndnq.push(['when', 'initialized', function() {
			_nw2e.Ndn_EventHub.on('all', function() {
				console.log('[Ndn_EventHub]', arguments[0], arguments[1]); 
			});
		}]);
	}
	
	/**
	 * @param {String} fullEventName The full event name (such as "hook:videoLoad")
	 * @param {String} eventNamespace The event namespace (such as "hook")
	 * @return {Boolean} Whether the specified event occurred within the specified event namespace
	 */
	var _isInEventNamespace = function(fullEventName, eventNamespace) {
		return _getEventName(fullEventName, eventNamespace) !== null;
	};
	
	/**
	 * @param {String} fullEventName The full event name (such as "hook:videoLoad")
	 * @param {String} eventNamespace The event namespace (such as "hook")
	 * @return {?String} The event name without the specified event namespace (such as "videoLoad") or null if the full event name provided does not match the specified event namespace
	 */
	var _getEventName = function(fullEventName, eventNamespace) {
		if (fullEventName.match(new RegExp("^" + eventNamespace + ":"))) {
			var result = fullEventName.match(new RegExp("^" + eventNamespace + ":(.*)$"));
			return result[1];
		}
		else {
			return null;
		}
	};
	
	/**
	 * @param {String} [eventNamespace] The event namespace to retrieve a chronological index for; if not provided, a timeline index for ALL events is returned
	 * @return {int} The chronological index specifying when a timeline event has occurred
	 */
	var _getTimelineEventIndex = (function() {
		var timelineEventIndices = {};
		
		return function(eventNamespace) {
			if (typeof eventNamespace == 'undefined') eventNamespace = '';
			
			timelineEventIndices[eventNamespace] = timelineEventIndices[eventNamespace] || 0;
			
			return timelineEventIndices[eventNamespace]++;
		};
	})();
	
	/**
	 * @param {String} eventNamespace 
	 * @param {String} containerElementId
	 * @param {String} eventName
	 * @return {Object} Structured data concerning the indices reserved for the specified event
	 */
	var _getReservedIndices = (function() {
		/**
		 * Structured data concerning the next index available for the respective event types. Format:
		 * <pre>{
		 * 	String: { // The event namespace (such as "analytics" or "hook") => structured data concerning events of this namespace
		 * 		String: { // The container element's "id" attribute => structured data concerning the number of events triggered so far for a specific type of event
		 * 			String: int, // The event name (the type of the event) => the number of events triggered so far for this type of event
		 * 			... // Any other events that have been triggered
		 * 			'*': int, // The number of events triggered so far for the product embedded within this container element
		 * 		},
		 * 		... // Any other structured data pertaining to specific container element "id" attributes
		 * 		'*': { // Structured data concerning the number of events triggered so far for any specific type of event
		 * 			String: int, // The event name (the type of the event) => the number of events triggered so far for this type of event
		 * 			... // Any other events that have been triggered
		 *	 		'*': int, // The number of events triggered so far for any type of event
		 * 		}
		 * 	},
		 * 	... // Any other event namespaces
		 * }</pre>
		 * @var {Object}
		 */
		var eventIndices = {};
		
		return function(eventNamespace, containerElementId, eventName) {
			eventIndices[eventNamespace] = eventIndices[eventNamespace] || {};
			eventIndices[eventNamespace][containerElementId] = eventIndices[eventNamespace][containerElementId] || {};
			eventIndices[eventNamespace][containerElementId][eventName] = eventIndices[eventNamespace][containerElementId][eventName] || 0;
			eventIndices[eventNamespace][containerElementId]['*'] = eventIndices[eventNamespace][containerElementId]['*'] || 0;
			eventIndices[eventNamespace]['*'] = eventIndices[eventNamespace]['*'] || {};
			eventIndices[eventNamespace]['*'][eventName] = eventIndices[eventNamespace]['*'][eventName] || 0;
			eventIndices[eventNamespace]['*']['*'] = eventIndices[eventNamespace]['*']['*'] || 0;
			
			return {
				'containerElementEventTypeIndex': eventIndices[eventNamespace][containerElementId][eventName]++,
				'containerElementEventIndex': eventIndices[eventNamespace][containerElementId]['*']++,
				'eventTypeIndex': eventIndices[eventNamespace]['*'][eventName]++,
				'anyContainerElementEventIndex': eventIndices[eventNamespace]['*']['*']++,
				'timelineEventIndex': _getTimelineEventIndex()
			};
		};
	})();
	
	/**
	 * Structured data concerning all the "hub events" that have been triggered. Format:
	 * <pre>{
	 * 	String: { // The event namespace (such as "analytics" or "hook") => structured data concerning events of this namespace
	 * 		String: { // The first and most descriptive Ndn_EventHub identifier describing an object being observed by the Ndn_EventHub module => structured data concerning all the events that have been triggered within said object being observed
	 * 			String: [ // An event name => a list of all events of this type that have been triggered for this embedded product
	 * 				Object, // Structured data concerning the event triggered
	 * 				... // Structured data concerning all other events triggered
	 * 			],
	 * 			... // All other event types that have been triggered for this embedded product and their associated data
	 * 			'*': [ // A list of ALL events of ANY type that have been triggered FOR this embedded product
	 * 		},
	 *      String: String, // An alias for an object's Ndn_EventHub identifier => said object's first and most descriptive Ndn_EventHub identifier
	 * 		... // Structured data concerning all other observable object identifiers/aliases and their associated data
	 * 		'*': { // Structured data of ALL events of ANY type that have been triggered FOR ANY embedded product
	 * 			String: [ // An event name => a list of all events of this type that have been triggered for any embedded product
	 * 				Object, // Structured data concerning the event triggered
	 * 				... // Structured data concerning all other events triggered
	 * 			],
	 * 			... // All other event types that have been trigger for any embedded product and their associated data
	 * 			'*': [ // A list of ALL events of ANY type that have been triggered FOR ANY embedded product
	 * 		}
	 * 	},
	 * 	... // Any other event namespaces
	 * }</pre>
	 * @var {Object}
	 */
	var _eventsTriggered = {};
	
	/**
	 * The list of recognized event namespaces that individual widget's may trigger and be recognized
	 * @var {Array<String,Object>}
	 */
	var _eventNamespaces = {
		'hook': Ndn_Hooks,
		'analytics': Ndn_Analytics
	};
	
	/**
	 * @param {String} eventNamespace
	 * @param {String} primaryObservableIdentifier
	 * @param {Array<String>} observableIdentifiers
	 * @param {Object} observableData
	 * @param {String} eventName
	 * @param {Object} reservedIndices
	 * @param {Object} event
	 */
	var _storeWidgetEvent = function(eventNamespace, primaryObservableIdentifier, observableIdentifiers, observableData, eventName, reservedIndices, event) {
	    // Determine whether the specified observable object had its identifiers registered before the call to this function was made
	    var hadRegisteredIdentifiers = !!(_eventsTriggered[eventNamespace] && _eventsTriggered[eventNamespace][primaryObservableIdentifier]);
	    
		// Make sure the needed structure is in place for recording the details of this event
		_eventsTriggered[eventNamespace] = _eventsTriggered[eventNamespace] || {};
		_eventsTriggered[eventNamespace][primaryObservableIdentifier] = _eventsTriggered[eventNamespace][primaryObservableIdentifier] || {};
		_eventsTriggered[eventNamespace][primaryObservableIdentifier][eventName] = _eventsTriggered[eventNamespace][primaryObservableIdentifier][eventName] || [];
		_eventsTriggered[eventNamespace][primaryObservableIdentifier]['*'] = _eventsTriggered[eventNamespace][primaryObservableIdentifier]['*'] || [];
		_eventsTriggered[eventNamespace]['*'] = _eventsTriggered[eventNamespace]['*'] || {};
		_eventsTriggered[eventNamespace]['*'][eventName] = _eventsTriggered[eventNamespace]['*'][eventName] || [];
		_eventsTriggered[eventNamespace]['*']['*'] = _eventsTriggered[eventNamespace]['*']['*'] || [];
		
		// Store the details of this event
		_eventsTriggered[eventNamespace][primaryObservableIdentifier][eventName][reservedIndices.containerElementEventTypeIndex] = event;
		_eventsTriggered[eventNamespace][primaryObservableIdentifier]['*'][reservedIndices.containerElementEventIndex] = event;
		_eventsTriggered[eventNamespace]['*'][eventName][reservedIndices.eventTypeIndex] = event;
		_eventsTriggered[eventNamespace]['*']['*'][reservedIndices.anyContainerElementEventIndex] = event;
		
        // If this observable object has not had its observable object identifier-aliases initialized
        if (!hadRegisteredIdentifiers) {
            // Make sure all observable object identifier aliases map to the primary observable object identifier
            if (observableIdentifiers.length > 1) {
                // Initialize all aliases for this observable object's primary observable identifier
                for (var i = 1; i < observableIdentifiers.length; i++) {
                    var observableIdentifier = observableIdentifiers[i];
                    _eventsTriggered[eventNamespace][observableIdentifier] = primaryObservableIdentifier;
                }
            }
        }
	};

	var _getTime = function() {
		return Date.now ? Date.now() : (new Date().valueOf());
	};
	
	/**
	 * Structured data concerning the event data to always pass with events identifier by the specified primary observable identifiers. Format:
	 * <pre>{
	 *     String: Object, // An observable object's primary observable identifier => structured data to always pass with events triggered by the associated observable object
	 *     ... // All other structured data to always pass with events triggered by said event's associated observable object
	 * }</pre>
	 * @var {Object}
	 */
	var _constantObservableEventData = {};
	
	/**
	 * @param {Ndn_Widget} widget The widget that spawned the provided event
	 * @param {String} fullEventName The full name of the event that is being observed (such as "hook:videoLoad")
	 * @param {Object} [eventData] The data paired with the triggering of the event being observed
	 */
	var _processWidgetEvent = function(widget, fullEventName, eventData) {
		// console.log('Ndn_EventHub, _processWidgetEvent, ', fullEventName);
		
		$.each(_eventNamespaces, function(eventNamespace, eventNamespaceLibrary) {
			if (_isInEventNamespace(fullEventName, eventNamespace)) {
				var eventName = _getEventName(fullEventName, eventNamespace),
			        observableData = (widget.getObservableData() || {}),
				    observableIdentifiers = observableData.identifiers
				    primaryObservableIdentifier = observableIdentifiers[0];
				
                // Reserve indices concerning when this event was triggered
                var reservedIndices = _getReservedIndices(eventNamespace, primaryObservableIdentifier, eventName);
                
                // Make sure that the data to always pass with every event observed for this observable object is stored for delivery
                if (!_constantObservableEventData[primaryObservableIdentifier]) {
                    _constantObservableEventData[primaryObservableIdentifier] = observableData.data || {};
                }
                
                // Create the event object that this module will hand out
				var event = {
					'public': _.extend({
						eventName: eventName,
						data: eventData
					}, _constantObservableEventData[primaryObservableIdentifier]),
					'system': _.extend({
						widget: widget,
						timestamp: _getTime()
					}, reservedIndices)
				};
				
				// If debug mode is enabled, store the widget event's data so it may be retrieved at a later point in time
				if (_storeEventData) {
					_storeWidgetEvent(eventNamespace, primaryObservableIdentifier, observableIdentifiers, observableData, eventName, reservedIndices, event);
				}
				
				// Trigger all combinations of the ways to describe the event that is just now happening (starting with the most specific
				// description of the event getting triggered first and ending with the least specific description of the event)
				{
					var trigger = function(eventDescription) {
						Ndn_EventHub.trigger(eventNamespace + ':' + eventDescription, event, false);
						eventNamespaceLibrary.trigger(eventDescription, event);
					};
					
					$.each(observableIdentifiers, function() {
					    var observableIdentifier = this + '';
                        
                        trigger(observableIdentifier + '/' + eventName + '[' + reservedIndices.containerElementEventTypeIndex + ']');
                        if (_storeEventData) trigger(observableIdentifier + '/' + '*' + '[' + reservedIndices.containerElementEventIndex + ']');
					});
					
					// Trigger the event but replace an observable object identifier with an asterisk "*"
                    trigger('*' + '/' + eventName + '[' + reservedIndices.eventTypeIndex + ']');
                    if (_storeEventData) trigger('*' + '/' + '*[' + reservedIndices.anyContainerElementEventIndex + ']');
                    
                    // Trigger the event without any indices appended to the end of the event identifier
                    {
                        $.each(observableIdentifiers, function() {
                            var observableIdentifier = this + '';
                            trigger(observableIdentifier + '/' + eventName);
                        });
                        
                        // Trigger the event using only the asterisk "*" in place of an observable object identifier
                        trigger('*' + '/' + eventName);
                        if (_storeEventData) trigger('*' + '/' + '*');
                    }
				}
			}
		});
	};
	
	/**
	 * Handle how the events are passed along to the appropriate event namespace listener modules; such as: Ndn_Analaytics, Ndn_Hooks
	 * @param {String} fullEventName
	 * @param {Object} eventData
	 * @param {Boolean} [process] Whether this event should really be processed, defaults to true (this may be set to false in the case where this event has already been processed in order to avoid an infinite loop)
	 */
	var _processEvent = function(fullEventName, eventData, process) {
		// console.log('Ndn_EventHub, _processEvent(', arguments, ');');
		
		if (process !== false) {
			$.each(_eventNamespaces, function(eventNamespace, eventNamespaceLibrary) {
				if (_isInEventNamespace(fullEventName, eventNamespace)) {
					var eventName = _getEventName(fullEventName, eventNamespace);
					
					var event = {
						'public': {
							eventName: eventName,
							data: eventData
						},
						'system': {
							timestamp: _getTime(),
							eventNamespace: eventNamespace,
							eventNamespaceTimelineIndex: _getTimelineEventIndex(eventNamespace),
							timelineEventIndex: _getTimelineEventIndex()
						}
					};
					
					// Trigger all combinations of the ways to describe the event that is just now happening (starting with the most specific
					// description of the event getting triggered first and ending with the least specific description of the event)
					{
						var trigger = function(eventDescription) {
							if (fullEventName != eventNamespace + ':' + eventDescription) {
								Ndn_EventHub.trigger(eventNamespace + ':' + eventDescription, event, false);
							}
							eventNamespaceLibrary.trigger(eventDescription, event);
						};
						
						trigger(eventName);
						
						// TODO: Add trigger for "<eventName>[index]"
						
						if (_storeEventData) trigger('*[' + event.system.eventNamespaceTimelineIndex + ']');
						if (_storeEventData) trigger('*');
					}
				}
			});
		}
	};
	
	/**
	 * The observable object to hijack its .trigger(), .on(), and .once() methods from for this library
	 * @var {Backbone.Model}
	 */
	var _observable = new Backbone.Model();
	
	_observable.on('all', _processEvent);
	
	var Ndn_EventHub = {
		/**
		 * Pre-requisite: All calls to this method should be wrapped in the following: _ndnq.push(['when', 'initialized', function() { ... }]);
		 * 
		 * @param {String} eventIdentifier Description of the event. For example "analytics:#0/videoLoad[1]" describes the analytics event concerning the SECOND "videoLoad" event triggered by the
		 * embedded product with an "embedIndex" of "0" (the first embedded product to be embedded). The "analytics:#0/videoLoad[0]" describes the analytics event concerning the FIRST "videoLoad" 
		 * event triggered by the same embedded product. TODO: Describe this in more detail concerning event descriptions such as "analytics:*\/videoLoad[0]" vs. "analytics:*\/videoLoad".
		 * @return {jQuery.Deferred} Resolves when the described event has its data available
		 */
		fetchEventData: function(eventIdentifier) {
			return $.Deferred(function(deferred) {
				var listenForEvent = function() {
					Ndn_EventHub.once(eventIdentifier, function(event) {
						deferred.resolve(event);
					});
				};
				
				// If the event data to fetch has an index (an event that has occurred at a certain time; for example 0 meaning the first time the event was triggered)
				if (eventIdentifier.indexOf('[') !== -1) {
					var event;
					if (event = Ndn_EventHub.getEventData(eventIdentifier)) {
						// If the event has already been triggered, return the stored event data
						deferred.resolve(event);
					}
					else {
						// Wait for the event to be triggered
						listenForEvent();
					}
				}
				else {
					// Wait for the event to be triggered
					listenForEvent();
				}
			}).promise();
		},
		
		/**
		 * @param {Ndn_Widget} widget The widget to observe
		 */
		observeWidget: function(widget) {
			widget.on('all', function(fullEventName, eventData) {
				_processWidgetEvent(widget, fullEventName, eventData);
			});
		},
		
		/**
		 * @param {Boolean} toggle Whether this module should store its event data
		 */
		toggleEventDataStorage: function(toggle) {
		    _storeEventData = !!toggle;
		},
		
		/**
		 * @param {String} eventIdentifier An identifier describing an event; format: "<eventNamespace>:<observableObjectIdentifier>/<eventName>[<eventIndex>]", for example: "hook:#0/videoLoad[0]"
		 */
		getEventData: function(eventIdentifier) {
		    if (typeof eventIdentifier == 'undefined') {
		        return _eventsTriggered;
		    }
		    
		    // console.log('getEventData(', eventIdentifier, ');');
		    
			var matches;
			if (matches = eventIdentifier.match(/^([^:]+):([^\/]+)\/([^\/]+)\[([0-9]+)\]$/)) {
				var eventNamespace = matches[1],
				    observableIdentifier = matches[2],
					eventName = matches[3],
					eventIndex = matches[4];
								
				// If the event namespace described by the provided event identifier has been registered
				if (_eventsTriggered[eventNamespace]) {
				    // If the observable object identifier is actually an alias to another observable object identifier, use the primary observable object identifier going forward
				    if (typeof _eventsTriggered[eventNamespace][observableIdentifier] == 'string') {
				        observableIdentifier = _eventsTriggered[eventNamespace][observableIdentifier];
				    }
				    
				    // If the observable object identifier described by the provided event identifier has been registered
	                if (_eventsTriggered[eventNamespace][observableIdentifier]) {
	                    var observableObjectEventsData = _eventsTriggered[eventNamespace][observableIdentifier];
	                    
	                    // If the event name has been triggered yet for the specified observable object, return the event data pertaining to the specified event index
	                    if (observableObjectEventsData[eventName]) {
	                        return observableObjectEventsData[eventName][eventIndex];
	                    }
	                }
				}
			}
		},
		
		getProductTimelineData: function(eventNamespace, containerElementId) {
			// console.log('Ndn_EventHub.getProductTimelineData(', arguments, ');');
			
			try {
				return _eventsTriggered[eventNamespace][containerElementId]['*'];
			}
			catch (e) {}
		},

		trigger: function() {
			return Backbone.Model.prototype.trigger.apply(_observable, arguments);
		},
		
		on: function() {
			return Backbone.Model.prototype.on.apply(_observable, arguments);
		},
		
		once: function() {
			return Backbone.Model.prototype.once.apply(_observable, arguments);
		}
	};
	
	_nw2e.Ndn_EventHub = Ndn_EventHub;
	
	return Ndn_EventHub;
});