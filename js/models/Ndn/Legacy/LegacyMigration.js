var _ndnq = _ndnq || [];

define([
	    'jquery',
	    'models/Ndn/App',
	    'models/Ndn/Utils/UrlParser'
	],
	function($, Ndn_App, Ndn_Utils_UrlParser) {
		return {
			/**
			 * Migrate a legacy player embed
			 */
			migrate: function(legacyEmbedUrl) {
				// console.log('Ndn_Legacy_LegacyMigration.migrate(', arguments, ');');
				
				var parsedUrl = Ndn_Utils_UrlParser.parseHref(legacyEmbedUrl);
				
				// If the "parent" query string is provided, which specifies the container element's "id" to be embedded
				var containerElementId = parsedUrl['queryParams']['parent'];
				if (containerElementId) {
					// Migrate the container element into the appropriate configurable <div> element for the NDN Player Suite
					$('#' + containerElementId)
					.removeClass('textwidget')
					.attr('data-config-legacy-embed-url', legacyEmbedUrl);
					
					// Embed the converted dom element using the NDN Player Suite's JavaScript API
					_ndnq.push(['embed', containerElementId]);
				}
			}
		};
	}
);