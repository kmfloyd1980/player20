/**
 * Handles quirks for Triton Loyalty Program
 */
define(['jquery'], function($) {
	return {
		/**
		 * AP-specific legacy config translation
		 */
		translate: function(legacyConfig, resultConfig) {
			return $.Deferred(function(deferred) {
				var result = resultConfig;
				
				// Explicitly define the "memberid" while accounting for an instance where embed code used to pass it "membedid" instead of "memberid"
				legacyConfig['memberid'] = legacyConfig['memberid'] || legacyConfig['membedid'] || '';
				
				// If there is a "memberid" parameter in the legacy config (meaning it also has a "siteid" parameter)
				if (legacyConfig['memberid'] != '' && legacyConfig['memberid'] != 'none') {
					resultConfig['loyaltyProgramId'] = 'ndntritonservice';
					resultConfig['loyaltyProgramData'] = {
						'memberId': legacyConfig['memberid'],
						'siteId': legacyConfig['siteid']
					};
				}
			
				deferred.resolve(resultConfig);
			}).promise();	
		}
	};
});