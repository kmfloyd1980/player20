/**
 * Handles quirks for Associated Press's (AP) one off config parameters passed through to our legacy players
 */
define(['jquery'], function($) {
	/**
	 * Associative array mapping AP partner IDs to tracking group IDs that NDN understands
	 * @var {Object<string,string>}
	 */
	var _apPartnerIdsToTrackingGroup = {
		"1153168": "90703",
		"1296344": "90416",
		"10003188": "90693",
		"TXABI": "90459",
		"PAALM": "90364",
		"GASTA": "90683",
		"TXAMA": "90582",
		"ARLID": "90554",
		"GAATH": "90604",
		"GAAUG": "90612",
		"TXAUS": "90105",
		"MTBOZ": "90721",
		"PABUT": "90433",
		"FLCAP": "90364",
		"TXCOR": "90550",
		"VACUL": "90527",
		"IAWEB": "90364",
		"MIESC": "90364",
		"VADAR": "90528",
		"ALDOT": "90510",
		"PAERI": "90464",
		"IAEST": "90364",
		"FLJAJ": "90049",
		"NCHIC": "90518",
		"TXHOU": "90000",
		"NCCON": "90517",
		"JRC": "90734",
		"WIKEN": "90233",
		"WABRE": "90552",
		"MOCAG": "90589",
		"NDMIN": "90364",
		"FLNAP": "90542",
		"INBNB": "90605",
		"ALOPE": "90514",
		"MNROC": "90379",
		"VARIT": "90535",
		"FLSAR": "90492",
		"INSBT": "90575",
		"MNPAU": "90156",
		"TXSAN": "90551",
		"CAPAS": "90494",
		"TNMEM": "90547",
		"MSCCD": "90135",
		"OHFIN": "90421",
		"OHCAM": "90249",
		"MIHOU": "90364",
		"MIIRO": "90364",
		"VACHA": "90526",
		"NCFAY": "90364",
		"wvwhe": "90364",
		"TNKNN": "90542",
		"OHMAR": "90364",
		"HIMAN": "90364",
		"NCMAR": "90519",
		"MIMAR": "90364",
		"VALYD": "90532",
		"VAWAY": "90538",
		"SCCHA": "90349",
		"NYJAP": "90364",
		"OHEAS": "90249",
		"OHSAL": "90364",
		"ILSPR": "90108",
		"GAGAI": "90079",
		"WVWEI": "90364",
		"PAWIL": "90087",
		"NYALT": "90001",
		"WAKR": "90412",
		"TXWIC": "90553",
		"NCWIN": "90522",
		"WRKO": "90629",
		"WSPATV": "90507",
		"WTKKFM": "90456",
		"PAYOK": "90732",
		"FLMIH": "90045",
		"WABEL": "90472",
		"SCCOL": "90474",
		"CAFRE": "90476",
		"CAMOD": "90479",
		"CASAB": "90482",
		"WATAC": "90483",
		"FLBRA": "90484",
		"ILROR": "90108",
		"MAQUI": "90108",
		"CTNOR": "90108",
		"MABRO": "90108",
		"WSAW": "90447",
		"FLTAM": "90426",
		"NVLAS": "90277",
		"INSID": "90531",
		"WNCNTV": "90777",
		"WCMHTV": "90061",
		"OREUG": "90878",
		"SCFLO": "90784",
		"WJARTV": "90053",
		"WIMIL": "90078",
		"RIJRJ": "90446",
		"WRGBTV": "90028",
		"WWMTTV": "90029",
		"ILALT": "90033",
		"WPECTV": "90039",
		"KTVLTV": "90042",
		"OHLIM": "90043",
		"AAAA": "0000",
		"WASECA": "90667",
		"PAYOR": "90681",
		"GACOV": "90691",
		"GAHIN": "90686",
		"CASCN": "90152",
		"CAMAN": "90690",
		"WIEAU": "90694",
		"MSMCC": "90695",
		"FLNAV": "90696",
		"LAMOR": "90697",
		"LAFRA": "90698",
		"SCAIK": "90699",
		"ARFOR": "90700",
		"MIGHT": "90702"
	};
	
	return {
		/**
		 * @param {String} apPartnerId The AP partner ID to map to a tracking group
		 * @return {String} The tracking group that maps to the provided AP partner ID (defaults to AP's tracking group when the provided AP partner ID is not recognized)
		 */
		getTrackingGroupFromApPartnerId: function(apPartnerId) {
			return _apPartnerIdsToTrackingGroup[apPartnerId] || '90121';
		},
		
		/**
		 * AP-specific legacy config translation
		 * @param {Object} legacyConfig Structured data concerning the query string parameters sent to legacy players
		 * @param {Object} resultConfig Structured data that has already begun its translation of the legacy config into this result config
		 */
		translate: function(legacyConfig, resultConfig) {
			return $.Deferred($.proxy(function(deferred) {
				var result = resultConfig;

				if (legacyConfig['f']) {
					result['trackingGroup'] = this.getTrackingGroupFromApPartnerId((legacyConfig['f'] + '').toUpperCase());
					
					// If the widget ID is set to '0', then default the widget ID to AP's default widget ID
					if (result['widgetId'] == '0') {
						result['widgetId'] = '3989';
					}
				}
				
				if (!result['videoId'] && legacyConfig['g']) {
					return $.ajax({
						type: 'GET',
						url: window.location.protocol + '//dmeserve.newsinc.com/apids/lookup.jsonp?apvid=' + legacyConfig['g'],
						dataType: 'jsonp'
					})
					.then(function(response) {
						result['videoId'] = response;
						deferred.resolve(result);
					});
				}
				else {
					deferred.resolve(result);
				}
			}, this)).promise();
		}
	};
});