define([
        'underscore',
        'jquery',
        'models/Ndn/Utils/UrlParser',
        'models/Ndn/Legacy/Config/Ap',
        'models/Ndn/Legacy/Config/TritonLoyaltyProgram'
	], function(_, $, Ndn_Utils_UrlParser, Ndn_Legacy_Config_Ap, Ndn_Legacy_Config_TritonLoyaltyProgram) {
		return {
			/**
			 * @param {Object} legacyConfig The query string parameters passed to legacy players
			 * @param {String} [widgetType] An override to specify which widget type should really be used in the translated config
			 * @return {Object} Structured data that our new players understand
			 */
			translate: function(legacyEmbedUrl, widgetType) {
				return $.Deferred(function(deferred) {
					var parsedUrl = Ndn_Utils_UrlParser.parseHref(legacyEmbedUrl);
					
					// Get the legacy config's query string parameters and ensure the config params are all lowercase for consistency
					var legacyConfig = parsedUrl['queryParams'];
					for (var paramKey in legacyConfig) {
						if (legacyConfig.hasOwnProperty(paramKey) && paramKey != paramKey.toLowerCase()) {
							legacyConfig[paramKey.toLowerCase()] = legacyConfig[paramKey];
							delete legacyConfig.paramKey;
						}
					}
					
					// Determine the URL path of the legacy embed URL (or set to the empty string to indicate this is not truly coming from a legacy embed URL)
					var legacyUrlPath = parsedUrl['url'].match(/^http:[\/]{2}embed\.newsinc\.com/)
						? parsedUrl['path'].toLowerCase()
						: '';
					
					var result = {
						'widgetId': legacyConfig['wid'],
						'siteSection': legacyConfig['sitesection'] || '',
						'videoId': legacyConfig['vid'] || legacyConfig['vcid'] || legacyConfig['spid'] || '',
						'trackingGroup': legacyConfig['freewheel'] || '',
						'playlistId': legacyConfig['cid'] || ''
					};
					
					// Delete any config settings with empty values since they weren't actually provided
					$.each(result, function(index) {
						if (this == '') {
							delete result[index];
						}
					});
					
					var embedUrlsToWidgetType = {
						'/single/embed.js': 'VideoPlayer/Default',
						'/inform/article/matching/video/embed.js': 'VideoPlayer/Default',
						'/horizontaltitle550/embed.js': 'VideoLauncher/Horizontal',
						'/inline/embed.js': 'VideoPlayer/Inline300',
						'/inlinesound/embed.js': 'VideoPlayer/Inline300',
						'/inline590/embed.js': 'VideoPlayer/Inline590',
						'/inlinesound590/embed.js': 'VideoPlayer/Inline590',
						'/toppicks/embed.js': 'VideoLauncher/Slider'
					};
					
					// Determine the widget's type
					if (typeof widgetType != 'undefined') {
						result['type'] = widgetType;
					}
					else {
						if (typeof embedUrlsToWidgetType[legacyUrlPath] != 'undefined') {
						 	result['type'] = embedUrlsToWidgetType[legacyUrlPath];
						}
						else {
							var typeMap = {
								'slider': 'VideoLauncher/Slider',
								'plist': 'VideoLauncher/Playlist300x250',
								'vertical': 'VideoLauncher/VerticalTower',
								'horizontal': 'VideoLauncher/Horizontal',
								'horizontal400': 'VideoLauncher/Horizontal',
								'horizontal500': 'VideoLauncher/Horizontal'
							};
							
							var legacyProductType = legacyConfig['type'] || legacyConfig['thumbid'];
							if (typeof typeMap[legacyProductType] != 'undefined') {
								result['type'] = typeMap[legacyProductType];
							}
							else if (_.contains(['/thumbnail/embed.js', '/thumbnail/iframe.html'], legacyUrlPath)) {
								result['type'] = 'VideoLauncher/Slider';
							}
						}
					}
					
					// Determine whether the legacy URL indicates that the "rmmSoundOn" setting should be set to true
					if (_.contains(['/inlinesound/embed.js', '/inlinesound590/embed.js', '/thumbnailsound/embed.js'], legacyUrlPath)) {
						result['rmmSoundOn'] = true;
					}
					
					// Add "width" and "height" config values to the config resulting config if present
					(legacyConfig['width'] || legacyConfig['w']) && (result['width'] = (legacyConfig['width'] || legacyConfig['w']));
					(legacyConfig['height'] || legacyConfig['h']) && (result['height'] = (legacyConfig['height'] || legacyConfig['h']));
					
					// Add the "width" config value if data is conveyed via the legacy embed URL
					var embedUrlsToWidgetWidth = {
						'/horizontaltitle550/embed.js': 550
					};
					if (typeof embedUrlsToWidgetWidth[legacyUrlPath] != 'undefined') {
						result['width'] = embedUrlsToWidgetWidth[legacyUrlPath];
					}
					else {
						if (legacyConfig['type'] == 'horizontal400') {
							result['width'] = 425;
						}
						else if (_.contains(['horizontal', 'horizontal500'], legacyConfig['type'])) {
							result['width'] = 550;
						}
					}
					
					// Remove any config settings that may have been set to "0" to indicate default behavior, whereas the NDN Player Suite uses the absence of a config setting to indicate default behavior
					var configSettingsThatShouldNotHaveZeroAsValue = [
						'playlistId',
						'widgetId',
						'videoId',
						'trackingGroup'
					];
					for (var i = 0; i < configSettingsThatShouldNotHaveZeroAsValue.length; i++) {
						var configSettingName = configSettingsThatShouldNotHaveZeroAsValue[i];
						if (result[configSettingName] == '0') {
							delete result[configSettingName];
						}
					}
					
					// If the provided "legacyEmbedUrl" did not come from Perfect Pixel's integration with the NDN Player Suite
					// and the translated config indicates that the product should be a Single player and that either the "width" 
					// or "height" config settings are not specified, then apply the legacy player suite's "width" and "height"
					// defaults. 
					// 
					// *Note: This logic gets bypassed when "legacyEmbedUrl" is provided via Perfect Pixel due to the way it needs
					// to indicate that the Single player is responsive and its CSS should be provided externally.
					if (legacyUrlPath != '/single/iframe.html' && !result['type'] || result['type'] == 'VideoPlayer/Default') {
						if (!result['width']) {
							result['width'] = 600;
						}
						if (!result['height']) {
							result['height'] = 360;
						}
					}
					
					Ndn_Legacy_Config_Ap.translate(legacyConfig, result)
					.then(function(result) { 
						return Ndn_Legacy_Config_TritonLoyaltyProgram.translate(legacyConfig, result); 
					})
					.fail(deferred.reject)
					.done(deferred.resolve);
				}).promise();
			}
		};
	}
);
