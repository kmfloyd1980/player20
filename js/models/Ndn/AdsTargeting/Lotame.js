var _ndnq = _ndnq || [];

/**
 * Ads targeting specifically using the service described at www.lotame.com.
 */
define([
		'jquery',
		'models/Ndn/AdsTargeting/Abstract'
	],
	function($, Ndn_AdsTargeting_Abstract) {
		"use strict";
		
		return Ndn_AdsTargeting_Abstract.extend({
			initialize: function() {
				Ndn_AdsTargeting_Abstract.prototype.initialize.apply(this, arguments);
				
				// TODO: Refactor out "auditudeSegmentsKey"
				// Used within the Ndn_VideoPlayer library only for data passed to Auditude
				this.set({
					'auditudeSegmentsKey': 'lotame'
				});
				
				// Once the Ndn_App library is available
				_ndnq.push(['when', 'initialized', $.proxy(function() {
					var Ndn_App = _nw2e.Ndn_App;
					
					// Create the global closure needed for the jsonp request to Lotame
					var globalClosureName = Ndn_App.makeClosureGlobal($.proxy(function(response) {
						var segments = [],
							index;
						
						var addAudienceIds = function(audiences) {
							for (var i = 0; i < audiences.length; i++) {
								segments.push(audiences[i]['id']);
							}
						};
						
						if (typeof response['Profile']['Audiences']['Audience'] != 'undefined') {
							addAudienceIds(response['Profile']['Audiences']['Audience']);
						}

						if (typeof response['Profile']['Audiences']['ThirdPartyAudience'] != 'undefined') {
							addAudienceIds(response['Profile']['Audiences']['ThirdPartyAudience']);
						}
						
						this.set('segments', segments);
						this.set('isLoaded', true);
					}, this));
					
					// Create the Lotame jsonp request using the global closure created above
					(function() {
						var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
						s.src = document.location.protocol + '//' + 'ad.crwdcntrl.net/5/c=1859/pe=y/callback=' + globalClosureName;
						var s0 = document.getElementsByTagName('script')[0]; s0.parentNode.insertBefore(s, s0);
					})();
				}, this)]);
			}
		});
	}
);
