/**
 * Ads targeting specifically from www.legolas-media.com.
 */
define([
		'models/Ndn/AdsTargeting/Abstract',
		'models/Ndn/Timer'
	],
	function(Ndn_AdsTargeting_Abstract, Ndn_Timer) {
		"use strict";

		return Ndn_AdsTargeting_Abstract.extend({
			initialize: function() {
				Ndn_AdsTargeting_Abstract.prototype.initialize.apply(this, arguments);
				
				this.set({
					'auditudeSegmentsKey': 'lsg'
				});
				
				var cookieData = (function(name) {
					var cookies = document.cookie;
					
					if (cookies.indexOf(name) != -1) {
						var startpos = cookies.indexOf(name) + name.length + 1;
						var endpos = cookies.indexOf(";", startpos) - 1;
						if (endpos == -2) {
							endpos = cookies.length;
						}
						
						return unescape(cookies.substring(startpos, endpos));
					}
					else {
						// The cookie was not found; either it was never set before or it expired
						return false;
					} 
				})('lsg');
				
				if (cookieData) {
					this.set('segments', cookieData.split('s'));
				}
				
				// Indicate that the ads targeting segments have been loaded
				this.set('isLoaded', true);
			}
		});
	}
);
