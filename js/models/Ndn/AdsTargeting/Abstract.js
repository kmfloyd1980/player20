define([
		'backbone',
		'jquery',
		'models/Ndn/Timer'
	],
	function(Backbone, $, Ndn_Timer) {
		return Backbone.Model.extend({
			defaults: {
				/**
				 * @var {Array<string>}
				 */
				segments: [],
				
				/**
				 * @var {Boolean}
				 */
				isLoaded: false,
				
				/**
				 * The timeout timer to indicate that the attempt at getting this ads targeting data has timed out
				 * @var {Ndn_Timer}
				 */
				timeoutTimer: null,
				
				/**
				 * Whether segments data has been received or receiving data has been attempted but has timed out
				 * @var {Boolean}
				 */
				isDeliverable: false,
				
				/**
				 * The key identifier for sending this ads targeting's segments to Auditude
				 * @var {String}
				 */
				auditudeSegmentsKey: ''
			},
			
			isDeliverable: function() {
				return this.get('isDeliverable');
			},
			
			onceIsDeliverable: function(callback) {
				if (this.isDeliverable()) {
					callback();
				}
				else {
					this.once('deliver', callback);
				}
			},
			
			initialize: function() {
				this.set({
					'segments': [],
					'isDeliverable': false,
					'isLoaded': false
				});
				
				this
				.on('deliver', $.proxy(function() {
					this.set('isDeliverable', true);
				}, this))
				.on('change:isLoaded', $.proxy(function() {
					//console.log('is triggering "deliver" for auditudeSegmentsKey ==', this.get('auditudeSegmentsKey'));
					
					this.trigger('deliver');
				}, this));
				
				// Set a timer to timeout and make this type of ad targeting deliverable anyway
				new Ndn_Timer($.proxy(function() {
					this.trigger('deliver');
				}, this), 500)
				.start();
			},
			
			isLoaded: function() {
				return this.get('isLoaded');
			},
			
			getSegments: function() {
				return this.get('segments');
			}
		});
	}
);

