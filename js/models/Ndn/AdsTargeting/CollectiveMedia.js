/**
 * Ads targeting specifically from www.collective-media.com.
 */
define([
		'jquery',
		'models/Ndn/AdsTargeting/Abstract'
	],
	function($, Ndn_AdsTargeting_Abstract) {
		"use strict";

		return Ndn_AdsTargeting_Abstract.extend({
			initialize: function() {
				Ndn_AdsTargeting_Abstract.prototype.initialize.apply(this, arguments);
				
				this.set({
					'auditudeSegmentsKey': 'ampseg'
				});
				
				$.ajax({
					type: 'GET',
					url: 'http://a.collective-media.net/profile/jsonp?net=cm',
					jsonpCallback: 'cb',
					contentType: 'application/json',
					dataType: 'jsonp',
					success: $.proxy(function(response) {
						this.set({
							'segments': response['segments'],
							'isLoaded': true
						});
					}, this),
					error: function(error) {
						// TODO: Handle this scenario.
					}
				});
			}
		});
	}
);
