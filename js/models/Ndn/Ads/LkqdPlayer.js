define([
    'jquery',
    'backbone',
    'models/Ndn/App',
    'models/Ndn/Analytics/Comscore',
    'models/Ndn/Debugger'
], function(
	$,
	Backbone,
	Ndn_App,
	Ndn_Analytics_Comscore,
	Ndn_Debugger
) {
	/**
	 * Whether debug statements should be pushed to the web console
	 * @var {Boolean}
	 */
	var _debug = Ndn_Debugger.inMode('lkqd');
	
	/**
	 * Whether an instance of this video player has been initialized yet (only one may be initialized per page)
	 * @var {Boolean}
	 */
	var _hasInitializedPlayer = false;
	
	/**
	 * @private
	 */
	var _initializeEventListeners = function() {
		var playerInstance = this,
			containerElement = this.get('containerElement'),
			containerId = containerElement.attr('id');
		
		function handlePlayerMessages(event) {
		    if (_debug) console.log('[Ndn_Ads_LkqdPlayer] handlePlayerMessages(', event, ');');
		    
			if (event.data && event.data.lkqd && event.data.from == "lkqd" && event.data.lkqdid == playerInstance.get('lkqdId')) {
				switch (event.data.event) {
					case 'AdLoaded':
						playerInstance.trigger('adLoad', {
							/*
							adId: '',
							adPlatform: '',
							adSystem: '',
							adTitle: '',
							linear: '',
							podPosition: '',
							totalAds: '',
							workflow: '',
							url: '',
							duration: ''
							*/
						});
					break;
					case 'AdStarted':
						playerInstance.trigger('adStart');
					break;
					case 'AdStopped':
						// Determine whether this player has loaded content yet
						var hasLoadedContent = (!document.getElementById(containerId).childNodes) || (document.getElementById(containerId).childNodes.length == 0) || ((document.getElementById(containerId).childNodes.length == 1) && (document.getElementById(containerId).childNodes[0].childNodes.length == 0));
						
						// If the player does not have content within its container element
						if (!hasLoadedContent) {
							/* TODO: Implement this retry-functionality later
							try {
								if (playerInstance.get('errorCount') == 1) {
									// The milliseconds since the last error was encountered
									var timeSinceLastError = Date.now() - playerInstance.get('lastErrorTimestamp');
									
									// If the last error encountered was less than x-milliseconds ago and this player has not tried reloading itself to workaround this error
									if (timeSinceLastError < 2000 && !playerInstance.get('hasRetriedLoadingPlayer')) {
										// Indicate that this player has retried loading itself to workaround an encountered error
										playerInstance.set('hasRetriedLoadingPlayer', true);
										
										// Attempt reloading the player to workaround said encountered error
										document.getElementById(containerId).innerHTML = ''; // remove replay button
										playerInstance.start();
									}
								}
							}
							catch (e) {}
							*/
						}
						else {
							// Indicate that the ad has ended
							playerInstance.trigger('adEnd');
						}
					break;
					case 'AdError':
						playerInstance.set({
							lastErrorTimestamp: Date.now(),
							errorCount: playerInstance.get('errorCount') + 1
						});
						
						playerInstance.trigger('adError', {
							receivedData: {
								errorType: 'adError'
							}
						});
					break;
					case 'NoDevice':
						playerInstance.trigger('adError', {
							receivedData: {
								errorType: 'deviceNotSupported'
							}
						});
					break;
				}
			}
		}
		
		if (window.addEventListener) { window.addEventListener("message", handlePlayerMessages, false); } else { window.attachEvent("onmessage", handlePlayerMessages); }
		
		this.set('isReady', true);
	};
	
	/**
	 * @return {String} The next unique identifier for the container element of this player
	 */
	var _getNextPlayerUid = (function() {
		var nextPlayerIndex = 1;
		return function() {
			return 'ndn_adsPlayer_lkqdPlayer_' + nextPlayerIndex++;
		};
	})();
	
	var Ndn_Ads_LkqdPlayer = Backbone.Model.extend({
		defaults: {
			/**
			 * The ID describing this type of player
			 * @var {Number}
			 */
			playerInstanceId: 4,
			
			/**
			 * The dom element containing this instance of the player
			 * @var {jQueryElement}
			 */
			containerElement: null,
			
			/**
			 * Integer describing when the last error was encountered
			 * @var {Number}
			 */
			lastErrorTimestamp: null,
			
			/**
			 * The number of errors encountered by this player
			 * @var {Number}
			 */
			errorCount: 0,
			
			/**
			 * Whether this player has retried loading itself to workaround an encountered error
			 * @var {Boolean}
			 */
			hasRetriedLoadingPlayer: false,
			
			/**
			 * The ID describing the instance of this player for a particular load attempt
			 * @var {Number}
			 */
			lkqdId: null,
			
			/**
			 * The interval for muting the player (if the player is muted)
			 * @var {Number}
			 */
			muteInterval: null,
			
			/**
			 * Whether this player is muted
			 * @var {Boolean}
			 */
			isMuted: false,
			
			/**
			 * Whether this instance of the video player is ready for playback (all its event listeners have been initialized)
			 * @var {Boolean}
			 */
			isReady: false,
			
			/**
			 * Structured data concerning the parameter data needed for sending data to the ad network
			 * @var {Object}
			 */
			params: null
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdDuration: function() {
			return -1;
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdCurrentTime: function() {
			return 0;
		},
		
		/**
		 * TODO: Refactor the need for this out, as this is redundant here!
		 */
		isPlayingAd: function() {
			return true;
		},
		
		/**
		 * @return {Boolean} Whether this player is muted
		 */
		isMuted: function() {
			return this.get('isMuted') || !!this.get('muteInterval');
		},
		
		/**
		 * @param {Boolean} toggle Whether this player should be muted
		 */
		setMuted: function(toggle) {
			// Define how the player gets muted/unmuted, and how the mute interval gets cleared
			var mutePlayer = $.proxy(function() {
					this.setVolume(0);
				}, this),
				unmutePlayer = $.proxy(function() {
					this.setVolume(0.75);
				}, this),
				clearMuteInterval = $.proxy(function() {
					// If the player is currently muted (has a mute interval that ensures the "mute" state is enforced)
					if (this.isMuted()) {
						// Clear the mute interval that ensures that the "mute" state is enforced
						clearInterval(this.get('muteInterval'));
						this.set('muteInterval', null);
					}
				}, this);
			
			if (toggle) {
				// If the player should be muted and is currently not muted, mute the player
				if (!this.isMuted()) {
					// Ensure that the player is still muted every x-number of milliseconds
					this.set('muteInterval', setInterval(mutePlayer, 500));
				}
				
				// Mute the player
				mutePlayer();
			}
			else {
				// Clear the mute interval (if there is one) that ensures that the "mute" state is enforced
				clearMuteInterval();
				
				// Unmute the player
				unmutePlayer();
			}
			
			// Keep track of whether the player is muted via its mute interval logic
			this.set('isMuted', toggle);
		},
		
		/**
		 * @param {Number} volume TODO: Determine what the unit of measure is for this parameter (0 is used for mute).
		 */
		setVolume: function(volume) {
			if (_debug) console.log('[Ndn_Ads_LkqdPlayer] setVolume(' + volume + ');');
			
			// TODO: Add this code here!
		},
		
		/**
		 * TODO: Improve this implementation so that the width and height can be dynamically changed using width and height of 100% on the <iframe> element.
		 * 
		 * @param {jQueryElement} containerElement The container element to hold this instance of the EASI player
		 * @param {Object} params Structured data concerning data to be sent to the appropriate ad network
		 */
		initialize: function(containerElement, params) {
			if (_debug) console.log('Ndn_Ads_LkqdPlayer(', arguments, ');');
			
			var containerWidth = containerElement.width(),
				containerHeight = containerElement.height();
			
			if (_debug) {
				this.on('all', function(eventName) {
					console.log('[Ndn_Ads_LkqdPlayer] ' + eventName);
				});
			}
			
			// Ensure the provided container element has a unique "id" attribute
			if (!containerElement.attr('id')) {
				containerElement.attr('id', _getNextPlayerUid());
			}
			
			this.set({
				params: params,
				containerElement: containerElement
			});
			
			// Handle comScore
			this.on({
				'adStart': function() {
					var params = this.get('params');
					
					Ndn_Analytics_Comscore.execute({
						c1: '1',
		                c2: '11112732',
		                c3: params.distributorName,
		                c4: params.providerName + ' 09',
		                c5: '09',
		                c6: params.siteSection,
		                c10: params.producerCategory
					});
				}
			});
			
			// Mute the player immediately once an ad has loaded into the player, and unmute the player once the ad completes
			this.on({
				'adLoad': function() {
					this.setMuted(true);
				},
				'adEnd adError adUnavailable': function() {
					this.setMuted(false);
				}
			});
			
			// Initialize this player's event listeners
			_initializeEventListeners.call(this);
		},
		
		/**
		 * Start using this player
		 */
		start: function() {
            if (_debug) console.log('[Ndn_Ads_LkqdPlayer] start();');
            
			var containerElement = this.get('containerElement'),
				containerWidth = containerElement.width(),
				containerHeight = containerElement.height(),
				params = this.get('params'),
				customParamsData = { // TODO: Actually send this data in the ad call!!
					tg: params.adTrackingGroupNumber, // Tracking Group to send to the ad network
					ei: params.embedIndex, // Embed Index
					ci: params.widgetId, // Widget ID
					insid: params.pageViewSessionId // The "insid"
				};
			
			var tagqa = typeof Ndn_App.getSetting('lkqd_tagqa') != 'undefined' ? Ndn_App.getSetting('lkqd_tagqa') : false; //  !false; // '';
			var playerid = containerElement.attr('id'); // '';
			var width = containerWidth; // '';
			var height = containerHeight; // '';
			var admode = 'overlay';
			var companionid = '';
			var pubmacros = '';
			
			if (_debug) console.log('tagqa ==', tagqa);
			
			var lkqdid = new Date().getTime().toString() + Math.round(Math.random() * 1000000000).toString();

            if (_debug) console.log('lkqdid ==', lkqdid);
			
			// Store the ID for this instance of the player
			this.set('lkqdId', lkqdid);
			
			var lkqdjs = document.createElement('script'); 
			lkqdjs.async = true; 
			lkqdjs.type = 'text/javascript'; 

            if (_debug) console.log('About to append <script> element... setting .onload property...');
            
			lkqdjs.onload = function() { 
				window.postMessage({'lkqd': true, 'from': 'pub', 'lkqdid': lkqdid, 'event': 'initAd'}, '*');
				window.postMessage({'lkqd': true, 'from': 'pub', 'lkqdid': lkqdid, 'event': 'startAd'}, '*');
			};

			lkqdjs.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//ad.lkqd.net/serve/pure.js?format=2&apt=auto&ear=100&env=1&lkqdid=' + lkqdid + '&pid=35&sid=576&tagqa=' + tagqa + '&elementid=' + encodeURIComponent(playerid) + '&width=' + width + '&height=' + height + '&mode=' + admode + '&companionid=' + encodeURIComponent(companionid) + '&rnd=' + Math.floor(Math.random() * 100000000) + '&m=' + encodeURIComponent(pubmacros);
			var elem = document.getElementsByTagName('script')[0];
			
            if (_debug) console.log('About to append <script> element, which is:', elem);
			
			elem.parentNode.insertBefore(lkqdjs, elem);
		},
		
		/**
		 * Begin or resume playback for this video player
		 */
		play: function() {
			// TODO: Code this!!
		},
		
		/**
		 * Pause playback for this video player
		 */
		pause: function() {
			// TODO: Code this!!
		},

		/**
		 * @return {Number} The ID describing this type of player
		 */
		getPlayerInstanceTypeId: function() {
			return this.get('playerInstanceId');
		}
	},
	{
		/**
		 * @param {jQueryElement} containerElement The element to place the video player within
		 * @param {Object} params Structured data concerning the parameters generated in relation to the particular video load iteration by the Ndn_VideoPlayer instance
		 * @return {jQuery.Deferred} Resolves with the newly created Ndn_Ads_EasiPlayer instance when the video player could be created; rejects otherwise
		 */
		load: function(containerElement, params) {
            if (_debug) console.log('[Ndn_Ads_LkqdPlayer] load(', containerElement, ', ', params, ');');
            
			return $.Deferred($.proxy(function(deferred) {
				// TODO: Is the limitation of just one player per page still needed? Consider removing the below code that places said limitation.
				
				// If an instance of the Optimatic player has already been initialized to play, then indicate that this command could not be executed
				if (_hasInitializedPlayer) return deferred.reject();
				
				// Indicate that one instance of the player has begun its initialization (so that future initializations will be blocked)
				_hasInitializedPlayer = true;
				
				return deferred.resolve(new Ndn_Ads_LkqdPlayer(containerElement, params));
			}, this)).promise();
		}
	});
	
	return Ndn_Ads_LkqdPlayer;
});