var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];

define([
	'jquery',
	'models/Ndn/Utils/UrlParser'
], function(
	$,
	Ndn_Utils_UrlParser
) {
	/**
	 * The "adUnitPath" needed by the ad studies library to request the ad studies appropriately
	 * @var {String}
	 */
	// TODO: Remove this, as it is no longer needed. This is not static.
	// var _adUnitPath = '/6416049/NDN/AdTestCompany_83111';
	
	var _debug = (function() {
		if (Ndn_Utils_UrlParser.getParam('ndn_debugAdStudies', 'query') == '1') {
			// console.log('debug enabled!');
			
			return function() {
				/*
				var args = Array.prototype.slice.call(arguments, 0);
				console.log('args ==', args);
				var logParams = args.unshift('[Ndn_Ads_AdStudies]');
				console.log('logParams ==', logParams);
				
				console.log.apply(console, logParams);
				*/
				console.log(arguments);
			};
		}
		else {
			return function() {};
		}
	})();
	
	/**
	 * The prefix to be added before each unique "id" attribute for <div> container elements of generated ad studies
	 * @var {String}
	 */
	var _slotIdPrefix = 'ndn-div-gpt-ad-123456789-';
	
	/**
	 * The zero-based index of the next slot to be created that contains an ad study
	 * @var {int}
	 */
	var _nextSlotIndex = 0;
	
	/**
	 * Whether the DFP Ad Studies library has been initialized
	 * @var {Boolean}
	 */
	var _isInitialized = false;
	
	/**
	 * @return {String} The next "id" attribute for the next newly created ad study's <div> container element
	 */
	var _getNextSlotId = function() {
		return _slotIdPrefix + _nextSlotIndex++;
	};
	
	/**
	 * Structured data mapping "id" attributes of dom elements holding googletag.Slot objects to the googletag.Slot object itself. Format:
	 * <pre>{
	 * 	String: googletag.Slot, // The "id" attribute of the dom element holding this googletag.Slot object => the googletag.Slot object
	 * 	... // All other created googletag.Slot objects
	 * }</pre>
	 * @var {Object}
	 */
	var _slots = {};
	
	/**
	 * Structured data mapping "id" attributes of dom elements holding googletag.Slot objects to the jQuery.Deferred object responsible 
	 * for indicating whether the display/refresh of specified slot was successful. Format:
	 * <pre>{
	 * 	String: jQuery.Deferred, // The "id" attribute of the dom element holding this googletag.Slot object => the jQuery.Deferred object responsible for indicating whether the display/refresh of specified slot was successful
	 * 	... // All other jQuery.Deferred objects responsible for indicating whether an ad study was displayed for this slot
	 * }</pre>
	 * @var {Object}
	 */
	var _slotsDeferred = {};
	
	return {
		initialize: function() {
			if (!_isInitialized) {
				(function() {
					var gads = document.createElement('script');
					gads.async = true;
					gads.type = 'text/javascript';
					var useSSL = 'https:' == document.location.protocol;
					gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
					var node = document.getElementsByTagName('script')[0];
					node.parentNode.insertBefore(gads, node);
				})();
				
				// Indicate that the JavaScript library needed for ad studies has been initialized
				_isInitialized = true;
			}
		},
		
		/**
		 * Attempts to display an ad study for the provided dom element. If no ad study is delivered, then the dom element is hidden; otherwise, the dom element is shown with its new ad study content.
		 * @param {DOMNode} divElement Reference to a simple <div></div> element already placed within the page's dom tree (this element should not already have an "id" attribute set)
		 * @param {String} dfpIuValue The "iu" value sent to DFP, which describes what kind of ads should be returned
		 * @param {Array<Number>} [dimensions] Structured data concerning the dimensions of this ad study (defaults to [300,250]). Format:
		 * <pre>[
		 * 	Number, // The width measured in pixels of the ad study to be displayed
		 * 	Number, // The height measured in pixels of the ad study to be displayed
		 * ]</pre>
		 * @return {jQuery.Deferred} Resolves once the ad study is displayed; fails when no ad study is delivered
		 */
		displayAdStudy: function(divElement, dfpIuValue, dimensions) {
			_debug('displayAdStudy(', arguments, ');');
			
			try {
				// If dimensions are not provided, default to 300x250 pixels
				if (!dimensions) dimensions = [300, 250];
				
				// Ensure that the needed setup for displaying ad studies is initialized
				this.initialize();
				
				var slotContainer = $(divElement);
				
				slotContainer.hide();
				
				_debug('Just hid the slot\'s container element, slotContainer ==', slotContainer);
				
				return $.Deferred($.proxy(function(deferred) {
					var slotExists = typeof _slots[slotContainer.attr('id')] != 'undefined';
					
					// The slot ID of the googletag.Slot object to be created or that has already been created
					var slotId = slotExists ? slotContainer.attr('id') : _getNextSlotId();
					
					if (!slotExists) {
						// Assign the appropriate new "id" attribute for the new ad study slot
						slotContainer.attr('id', slotId);
					}
					
					// Initialize the deferred object for this specific slot (or use an existing one)
					_slotsDeferred[slotId] = _slotsDeferred[slotId] || deferred;
					
					// Set the appropriate width and height of the container <div> element
					slotContainer.css({
						width: dimensions[0],
						height: dimensions[1]
					});
						
					// Use the ad study library to finish creating (or to refresh) the ad study within the <div> element provided
				    googletag.cmd.push(function() {
				    	if (!slotExists) {
				    		_debug('Slot does not exist yet, so it is being created...');
				    		
					    	// Empty the <div> element's content
					    	slotContainer.html('');
					    	
					    	// Define the ad study slot and store the reference to it
							_slots[slotId] = 
							googletag.defineSlot(dfpIuValue, dimensions, slotId)
							.addService(googletag.pubads())
							.setCollapseEmptyDiv(true);
							
							googletag.pubads().addEventListener('slotRenderEnded', function(event) {
								_debug('[slotRenderEnded], event ==', event);
								
								// Get the stored deferred object from static storage
								var deferred = _slotsDeferred[slotId];
								
								if (event.isEmpty === false) {
									slotContainer.show();
									
									// Resolve the deferred object if the deferred object was present
									if (deferred) deferred.resolve();
								}
								else {
									slotContainer.hide();
	
									// Reject the deferred object if the deferred object was present
									if (deferred) deferred.reject();
								}
								
								// Remove the deferred object so that it does not block future deferred objects getting created for refreshes of this ad study
								delete _slotsDeferred[slotId];
						    });
							
							googletag.pubads().enableAsyncRendering();
					    	googletag.pubads().enableSingleRequest();
							googletag.enableServices();
							
							_debug('document.getElementById("' + slotId + '") ==', document.getElementById(slotId));
							
							googletag.display(slotId);
							
							_debug('googletag.display(' + slotId + '); was just called...');
				    	}
				    	else {
				    		_debug('Refreshing slot... slotId ==', slotId, '; _slots[slotId] ==', _slots[slotId]);
				    		
							googletag.pubads().refresh([_slots[slotId]]);
							
				    		_debug('googletag.pubads().refresh() was just called...');
				    	}
				    });
				}, this)).promise();
			    
			}
			catch(e) {
				_debug('PROBLEM DETECTED, e ==', e);
			}
		}
	};
});