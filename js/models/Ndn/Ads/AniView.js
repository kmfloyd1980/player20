var _nw2e = _nw2e || [];

define([
    'jquery',
    'backbone',
    'models/Ndn/Utils/UrlParser'
], function(
	$,
	Backbone,
	Ndn_Utils_UrlParser
) {
	// Create a deferred object to resolve itself once an empty response is received from the AniView player
	var _pendingEmptyResponse = $.Deferred(function(deferred) {
		// Make this conclude any pending and future AniView requests 
		_nw2e.concludeAniView = function() {
			// console.log('_nw2e.concludeAniView();');
			deferred.resolve();
		};
	}).promise();

	var _channelId = Ndn_Utils_UrlParser.getParam('ndn_aniviewChannelId', 'query') || '93697',
		_vastRetries = parseInt(Ndn_Utils_UrlParser.getParam('ndn_aniviewVastRetries', 'query')) || 2;
	
	/**
	 * @return {Object} Structured data concerning the config that is used to create the appropriate instance of an ad player
	 */
	var _getConfig = function(containerId, customData) {
		var customDataString = (function() {
			var params = [];
			$.each(customData, function(index, paramData) {
				var paramName = paramData[0],
					paramValue = paramData[1];
				params.push('custom[' + paramName + ']=' + encodeURIComponent(paramValue));
			});
			return params.join('&');
		})();
		
		var result = {
			publisherID: '1', 
			channelID: _channelId,
			GetVASTRetry: _vastRetries,
			pause: false,
			loop: false,
			autoClose: true,
			contentID: '',
			closeButton: true,
			showCloseButon: 5,
			skipText: 'You can skip to video in {timeleft}',
			pauseButton: true,
			soundButton: true,
			fullScreenButton: false,
			passBackUrl: '',
			isSmart: false, 
			playerType: 2,
			clickTracking: '',
			subExternalID: '',
			ref1: customDataString,
			backgroundImageUrl: '',
			backgroundColor: '#FFF',
			position: containerId,
			passBackUrl: _nw2e.appUrl + '/js/models/Ndn/Ads/AniView/conclude.js'
		};
		
		// console.log('AniView Player Config ==', result);
		
		return result;
	};
	
	/**
	 * @return {String} The next unique "id" attribute to identify a new instance of an ad player
	 */
	var _getNextContainerId = (function() {
		var counter = 0;
		
		return function() {
			return 'ndn-aniview-container-' + counter++;
		};
	})();
	
	/**
	 * @return {jQuery.Deferred} Resolves once the JavaScript library needed to create a new instance of an autoplay preroll is loaded on the page
	 */
	var _requireLibrary = (function() {
		var libraryUrl = 'http://eu.ani-view.com/aniview.js', // 'http://cdn2.ani-view.com/aniviewdemo.js',
			deferredLoading = null;
		
		return function() {
			deferredLoading = deferredLoading || $.Deferred(function(deferred) {
				var script = document.createElement('script');
				script.src = libraryUrl;
				script.onload = function() {
					deferred.resolve();
				};
				document.getElementsByTagName('head')[0].appendChild(script);
			}).promise();
			
			return deferredLoading;
		};
	})();
	
	return Backbone.Model.extend({
		defaults: {
			/**
			 * The container <div> to use for this ad
			 * @var {DOMNode}
			 */
			'containerElement': null,
			
			/**
			 * The "id" attribute assigned to the provided <div> container element that uniquely identifies this ad player
			 * @var {String}
			 */
			'containerId': null,
			
			/**
			 * Structured data concerning the parameters to pass to the ad network
			 * @var {Object}
			 */
			'adParams': null,
			
			/**
			 * Whether the first quartile has been triggered via the "view" event
			 * @var {Boolean}
			 */
			'hasTriggeredFirstQuartile': false,
			
			/**
			 * The instance of the aniviewPlayer responsible for displaying the autoplay preroll ad
			 * @var {aniviewPlayer}
			 */
			'playerInstance': null
		},
		
		/**
		 * @param {DOMNode} containerElement The <div> container that will hold this ad player
		 * @param {Object} adParams Structured data that is sent to the ad network. Format:
		 * <pre>[
		 * 	[ // Structured data concerning a parameter and its value to send to the appropriate ad network
		 * 		String, // The ad parameter's name
		 * 		String, // The ad parameter's value
		 * 	],
		 * 	... // All other parameters to send to the ad network
		 * ]</pre>
		 */
		initialize: function(containerElement, adParams) {
			// Generate the unique "id" attribute to use for this instance and store the provided ad parameters
			this.set({
				containerElement: containerElement,
				containerId: _getNextContainerId(),
				adParams: adParams
			});
		},
		
		/**
		 * Begin playback of this AniView ad using the previously provided ad parameters and container element (to this object's constructor)
		 */
		play: function() {
			// Make sure the AniView third-party library has been loaded
			_requireLibrary().done($.proxy(function() {
				// Assign the generated "id" attribute to the provided <div> container element
				$(this.get('containerElement')).attr('id', this.get('containerId')).data('aniview', true);
				
				// Initialize the player instance
				var playerInstance = new aniviewPlayer;
				this.set('playerInstance', playerInstance);
				
				// Initialize this instance's event listeners
				playerInstance.onClick = $.proxy(function() { this.trigger('click'); }, this);
				playerInstance.onPlay = $.proxy(function() { this.trigger('play'); }, this);
				playerInstance.onPause = $.proxy(function() { this.trigger('pause'); }, this);
				playerInstance.onPlay25 = $.proxy(function() { this.trigger('view', {percentile: 25}); }, this);
				playerInstance.onPlay50 = $.proxy(function() { this.trigger('view', {percentile: 50}); }, this);
				playerInstance.onPlay75 = $.proxy(function() { this.trigger('view', {percentile: 75}); }, this);
				playerInstance.onPlay100 = $.proxy(function() { this.trigger('view', {percentile: 100}); }, this);
				playerInstance.onSoundOn = $.proxy(function() { this.trigger('soundOn'); }, this);
				playerInstance.onSoundOff = $.proxy(function() { this.trigger('soundOff'); }, this);
				playerInstance.onClose = $.proxy(function() { this.trigger('close'); }, this);
				
				// Indicate when the AniView player has received an empty response
				_pendingEmptyResponse.done($.proxy(function() {
					this.trigger('emptyResponse');
				}, this));
				
				// Closure to trigger the "adEnd" event
				var triggerAdEnd = $.proxy(function() {
					this.trigger('adEnd');
				}, this);
				
				// When the ad played is 100% complete, or when the ad was skipped via clicking the "X" to close the ad, trigger the "adEnd" event
				this.on({
					'view': function(event) {
						if (event.percentile === 100) {
							triggerAdEnd();
						}
					},
					'close emptyResponse': triggerAdEnd
				});
				
				// Make sure the "view" event with a "0%" percentile is triggered appropriately
				this.on('play', function() {
					// Add the "ndn_aniviewContainer" class for any special CSS styling needs (the "class" attribute gets removed if added before this event is triggered)
					$(this.get('containerElement')).addClass('ndn_aniviewContainer');
					
					if (!this.get('hasTriggeredFirstQuartile')) {
						this.set('hasTriggeredFirstQuartile', true);
						this.trigger('view', {percentile: 0});
					}
				});
				
				// Begin playback of an ad using the initialized player
				playerInstance.play(_getConfig(this.get('containerId'), this.get('adParams')));
			}, this));
		}
	});
});