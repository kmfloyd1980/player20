define([
    'jquery',
    'backbone',
    'models/Ndn/App',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/Analytics/Comscore',
    
    'lib/jquery_plugins/jquery.json'
], function(
	$,
	Backbone,
	Ndn_App,
	Ndn_Utils_UrlParser,
	Ndn_Analytics_Comscore
) {
	/**
	 * Whether debug statements should be pushed to the web console
	 * @var {Boolean}
	 */
	var _debug = Ndn_Utils_UrlParser.getParam('ndn_debug', 'query') == 'easi';
	
	// console.log('_debug ==', _debug);
	
	/**
	 * The SpotX channel ID to use for the Optimatic player
	 * @var {Number}
	 */
	var _spotXChannelId = Ndn_Utils_UrlParser.getParam('ndn_easiPlayerChannelId', 'query') || 86847; // 75776; // 79391;
	
	/**
	 * Whether an instance of this video player has been initialized yet (only one may be initialized per page)
	 * @var {Boolean}
	 * 
	 * @todo Is this still needed? Or does this only apply to the Optimatic player?
	 */
	var _hasInitializedPlayer = false;
	
	/**
	 * @private
	 */
	var _initializeEventListeners = function() {
		var playerInstance = this,
			iframeElement = this.get('iframeElement'),
			iframePlayer = iframeElement.contentWindow.spotx_ad;
		
		iframePlayer.subscribe(function() { playerInstance.trigger('adStart'); }, 'AdStarted');
		iframePlayer.subscribe(function() { playerInstance.trigger('adEnd'); }, 'AdStopped');
		iframePlayer.subscribe(function() { playerInstance.trigger('adError', {
			receivedData: {
				errorType: 'adError'
			}
		}); }, 'AdError');

		iframePlayer.subscribe(function() {
			// TODO: Ensure that the needed "adLoad" event data is getting passed here once the EASI player makes it available
			playerInstance.trigger('adLoad', {
				adId: '',
				adPlatform: '',
				adSystem: '',
				adTitle: '',
				linear: '',
				podPosition: '',
				totalAds: '',
				workflow: '',
				url: '',
				duration: ''
			});
		}, 'AdLoaded');
		
		this.set('isReady', true);
	};
	
	/**
	 * @private
	 * @return {EasiPlayer}
	 */
	var _getIframePlayer = function() {
		return this.get('iframeElement').contentWindow.spotx_ad;
	};
	
	var Ndn_Ads_EasiPlayer = Backbone.Model.extend({
		defaults: {
			/**
			 * The ID describing this type of player
			 * @var {Number}
			 */
			playerInstanceId: 2,
			
			/**
			 * The dom element containing this instance of the EASI player
			 * @var {jQueryElement}
			 */
			containerElement: null,
			
			/**
			 * Whether the ad's playback has begun
			 * @var {Boolean}
			 */
			hasAdStarted: false,
			
			/**
			 * The interval for muting the player (if the player is muted)
			 * @var {Number}
			 */
			muteInterval: null,
			
			/**
			 * Whether this player is muted
			 * @var {Boolean}
			 */
			isMuted: false,
			
			/**
			 * Whether this instance of the video player is ready for playback (all its event listeners have been initialized)
			 * @var {Boolean}
			 */
			isReady: false,
			
			/**
			 * Structured data concerning the parameter data needed for sending data to the ad network
			 * @var {Object}
			 */
			params: null
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdDuration: function() {
			return -1;
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdCurrentTime: function() {
			return 0;
		},
		
		/**
		 * TODO: Refactor the need for this out, as this is redundant here!
		 */
		isPlayingAd: function() {
			return true;
		},
		
		/**
		 * @return {Boolean} Whether this player is muted
		 */
		isMuted: function() {
			return this.get('isMuted') || !!this.get('muteInterval');
		},
		
		/**
		 * @param {Boolean} toggle Whether this player should be muted
		 */
		setMuted: function(toggle) {
			/**
			if (toggle) {
				this.setVolume(0);
			}
			else {
				this.setVolume(0.75);
			}
			this.set('isMuted', toggle);
			/**/
			/**/
			// Define how the player gets muted/unmuted, and how the mute interval gets cleared
			var mutePlayer = $.proxy(function() {
					this.setVolume(0); // 0.01);
				}, this),
				unmutePlayer = $.proxy(function() {
					this.setVolume(0.75);
				}, this),
				clearMuteInterval = $.proxy(function() {
					// If the player is currently muted (has a mute interval that ensures the "mute" state is enforced)
					if (this.isMuted()) {
						// Clear the mute interval that ensures that the "mute" state is enforced
						clearInterval(this.get('muteInterval'));
						this.set('muteInterval', null);
					}
				}, this);
			
			if (toggle) {
				// If the player should be muted and is currently not muted, mute the player
				if (!this.isMuted()) {
					// Ensure that the player is still muted every x-number of milliseconds
					this.set('muteInterval', setInterval(mutePlayer, 500));
				}
				
				// Mute the player
				mutePlayer();
			}
			else {
				// Clear the mute interval (if there is one) that ensures that the "mute" state is enforced
				clearMuteInterval();
				
				// Unmute the player
				unmutePlayer();
			}
			
			// Keep track of whether the player is muted via its mute interval logic
			this.set('isMuted', toggle);
			/**/
		},
		
		/**
		 * @param {Number} volume TODO: Determine what the unit of measure is for this parameter (0 is used for mute).
		 */
		setVolume: function(volume) {
			if (_debug) console.log('[Ndn_Ads_EasiPlayer] setVolume(' + volume + ');');
			
			if (this.hasLoadedIframePlayer()) {
				_getIframePlayer.call(this).setAdVolume(volume);
			}
		},
		
		/**
		 * TODO: Improve this implementation so that the width and height can be dynamically changed using width and height of 100% on the <iframe> element.
		 * 
		 * @param {jQueryElement} containerElement The container element to hold this instance of the EASI player
		 * @param {Object} params Structured data concerning data to be sent to the appropriate ad network
		 */
		initialize: function(containerElement, params) {
			var containerWidth = containerElement.width(),
				containerHeight = containerElement.height();
			
			this.set({
				params: params,
				containerElement: containerElement,
				iframeElement: (function() {
					// Begin creating the iframe to embed within the provided container
					var iframe = document.createElement("iframe");
					iframe.scrolling = 'no';
					iframe.style.overflow = 'hidden';
					iframe.style.width = containerWidth + 'px';
					iframe.style.height = containerHeight + 'px';
					iframe.style.border = '0';
					$(iframe).addClass('ndn_easiPlayer');
					
					// Place the iframe to embed within the provided container
					containerElement.get(0).appendChild(iframe);
					
					return iframe;
				})()
			});
			
			// Handle comScore
			this.on({
				'adStart': function() {
					var params = this.get('params');
					
					Ndn_Analytics_Comscore.execute({
						c1: '1',
		                c2: '11112732',
		                c3: params.distributorName,
		                c4: params.providerName + ' 09',
		                c5: '09',
		                c6: params.siteSection,
		                c10: params.producerCategory
					});
				}
			});
			
			// Mute the player immediately once an ad has loaded into the player, and unmute the player once the ad completes
			this.on({
				'adLoad': function() {
					this.setMuted(true);
				},
				'adEnd adError adUnavailable': function() {
					this.setMuted(false);
				}
			});
			
			// Keep track of the player's state (whether an ad has begun playback or not)
			this.on({
				'adStart': function() {
					this.set('hasAdStarted', true);
				},
				'adEnd': function() {
					this.set('hasAdStarted', false);
				}
			});
			
			// Handle when the iframe element has loaded
			this.get('iframeElement').onload = $.proxy(function() {
				// Control memory leaks in IE
				this.get('iframeElement').onload = null;
				
				var initializePlayer = $.proxy(function() {
					if (this.hasLoadedIframePlayer()) {
						// Trigger this event for analytics because according to the EASI team, this is when the ad call is made
						this.trigger('adRequest', {
							receivedData: {},
							playerInstanceType: 2 // this.getPlayerInstanceTypeId()
						});
						
						// If the iframe's player is ready to apply event listeners to, initialize the appropriate event listeners
						_initializeEventListeners.call(this);
					}
					else {
						// If the iframe's player is not ready to apply event listeners to, check back every x-number of milliseconds
						window.setTimeout($.proxy(function() {
							initializePlayer();
						}, this), 50);
					}
				}, this);
				
				initializePlayer();
			}, this);
		},
		
		/**
		 * Start using this player
		 */
		start: function() {
			var containerElement = this.get('containerElement'),
				containerWidth = containerElement.width(),
				containerHeight = containerElement.height(),
				params = this.get('params'),
				customParamsData = {
					tg: params.adTrackingGroupNumber, // Tracking Group to send to the ad network
					ei: params.embedIndex, // Embed Index
					ci: params.widgetId, // Widget ID
					insid: params.pageViewSessionId // The "insid"
				};
			
			// Write the iframe's HTML
			var iframeDocument = this.get('iframeElement').contentDocument;
			iframeDocument.open();
			iframeDocument.write("<html><head><scr" + "ipt>spotx_channel_id = " + _spotXChannelId + ";spotx_content_container_id='ad';spotx_easi_id='spotx_ad';custom=" + $.toJSON(customParamsData) + ";spotx_ad_format='flash';spotx_content_width=" + containerWidth + ";spotx_content_height=" + containerHeight + ";spotx_content_page_url=\"" + Ndn_App.getEmbedOriginUrl() + "\";</scr" + "ipt><scr" + "ipt src='//search.spotxchange.com/js/spotx.js'></scr" + "ipt></head><body><div id='ad'></div></body></html>");
			iframeDocument.close();
		},
		
		/**
		 * @return {Boolean} Whether this video player has finished loading its iframe element and is ready to begin initializing event listeners
		 */
		hasLoadedIframePlayer: function() {
			return !!this.get('iframeElement').contentWindow && !!this.get('iframeElement').contentWindow.spotx_ad;
		},
		
		/**
		 * Begin or resume playback for this video player
		 */
		play: function() {
			var iframePlayer = _getIframePlayer.call(this);
			
			if (this.get('hasAdStarted')) iframePlayer.resumeAd();
			else iframePlayer.startAd();
		},
		
		/**
		 * Pause playback for this video player
		 */
		pause: function() {
			_getIframePlayer.call(this).pauseAd();
		},

		/**
		 * @return {Number} The ID describing this type of player
		 */
		getPlayerInstanceTypeId: function() {
			return this.get('playerInstanceId');
		}
	},
	{
		/**
		 * @param {jQueryElement} containerElement The element to place the video player within
		 * @param {Object} params Structured data concerning the parameters generated in relation to the particular video load iteration by the Ndn_VideoPlayer instance
		 * @return {jQuery.Deferred} Resolves with the newly created Ndn_Ads_EasiPlayer instance when the video player could be created; rejects otherwise
		 */
		load: function(containerElement, params) {
			return $.Deferred($.proxy(function(deferred) {
				// TODO: Is the limitation of just one player per page still needed? Consider removing the below code that places said limitation.
				
				// If an instance of the Optimatic player has already been initialized to play, then indicate that this command could not be executed
				if (_hasInitializedPlayer) return deferred.reject();
				
				// Indicate that one instance of the player has begun its initialization (so that future initializations will be blocked)
				_hasInitializedPlayer = true;
				
				return deferred.resolve(new Ndn_Ads_EasiPlayer(containerElement, params));
			}, this)).promise();
		}
	});
	
	return Ndn_Ads_EasiPlayer;
});