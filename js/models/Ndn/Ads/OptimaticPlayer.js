define([
    'jquery',
    'backbone',
    'models/Ndn/App',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/Analytics/Comscore',
    'models/Ndn/Utils/UserAgent',
    
 	'text!templates/ndn/ads/optimatic-player.html'
], function(
	$,
	Backbone,
	Ndn_App,
	Ndn_Utils_UrlParser,
	Ndn_Analytics_Comscore,
	Ndn_Utils_UserAgent,
	
	template
) {
	/**
	 * Whether debug statements should be pushed to the web console
	 * @var {Boolean}
	 */
	var _debug = Ndn_Utils_UrlParser.getParam('ndn_debug', 'query') == 'optimatic';
	
	/**
	 * Whether an Optimatic video player has been initialized yet (only one may be initialized per page)
	 * @var {Boolean}
	 */
	var _hasInitializedPlayer = false;
	
	var Ndn_Ads_OptimaticPlayer = Backbone.Model.extend({
		defaults: {
			/**
			 * The interval for muting the player (if the player is muted)
			 * @var {Number}
			 */
			muteInterval: null,
			
			/**
			 * Whether this player is muted
			 * @var {Boolean}
			 */
			isMuted: false
		},
		
		initialize: function() {
			// Handle comScore
			this.on({
				'adStart': function() {
					var params = this.get('params');
					
					Ndn_Analytics_Comscore.execute({
						c1: '1',
		                c2: '11112732',
		                c3: params.distributorName,
		                c4: params.providerName + ' 09',
		                c5: '09',
		                c6: params.siteSection,
		                c10: params.producerCategory
					});
				}
			});
			
			// Mute the player immediately once an ad has loaded into the player, and unmute the player once the ad completes
			this.on({
				'adLoad': function() {
					this.setMuted(true);
				},
				'adEnd adError adUnavailable': function() {
					this.setMuted(false);
				}
			});
			
			this.on('all', function(eventName) {
				if (_debug) console.log('[Ndn_Ads_OptimaticPlayer] ' + eventName, arguments);
			});
		},
		
		/**
		 * @param {Number} volume The percentage to set this player's volume to (1 for full volume, 0 for muted)
		 */
		setVolume: (function() {
			// Only wrap this function in a try-catch block if using IE due to an error that is currently being thrown in IE, whereas try-catch blocks
			// sacrifice performance significantly on non-IE browsers. Reference: http://www.developerknowhow.com/javascript-trycatch-performance-hit/
			if (Ndn_Utils_UserAgent.isIe()) {
				return function(volume) {
					if (_debug) console.log('[Ndn_Ads_OptimaticPlayer] setVolume(' + volume + ');');
					
					// Use try-catch block to fix [PLAYER-2371]; whereas some sites were triggering an error when calling this method in IE
					try {
						alfyad_SetVolume(volume);
					}
					catch(e) {}
				};
			}
			else {
				return function(volume) {
					if (_debug) console.log('[Ndn_Ads_OptimaticPlayer] setVolume(' + volume + ');');
					
					alfyad_SetVolume(volume);
				};
			}
		})(),
		
		/**
		 * @return {Boolean} Whether this player is muted
		 */
		isMuted: function() {
			return this.get('isMuted') || !!this.get('muteInterval');
		},
		
		/**
		 * @param {Boolean} toggle Whether this player should be muted
		 */
		setMuted: function(toggle) {
			// Define how the player gets muted/unmuted, and how the mute interval gets cleared
			var mutePlayer = $.proxy(function() {
					this.setVolume(0); // 0.01);
				}, this),
				unmutePlayer = $.proxy(function() {
					this.setVolume(0.75);
				}, this),
				clearMuteInterval = $.proxy(function() {
					// If the player is currently muted (has a mute interval that ensures the "mute" state is enforced)
					if (this.isMuted()) {
						// Clear the mute interval that ensures that the "mute" state is enforced
						clearInterval(this.get('muteInterval'));
						this.set('muteInterval', null);
					}
				}, this);
			
			if (toggle) {
				// If the player should be muted and is currently not muted, mute the player
				if (!this.isMuted()) {
					// Ensure that the player is still muted every x-number of milliseconds
					this.set('muteInterval', setInterval(mutePlayer, 500));
				}
				
				// Mute the player
				mutePlayer();
			}
			else {
				// Clear the mute interval (if there is one) that ensures that the "mute" state is enforced
				clearMuteInterval();
				
				// Unmute the player
				unmutePlayer();
			}
			
			// Keep track of whether the player is muted via its mute interval logic
			this.set('isMuted', toggle);
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdDuration: function() {
			return -1;
		},
		
		/**
		 * TODO: This must be coded for analytics!!!
		 */
		getAdCurrentTime: function() {
			return 0;
		},
		
		/**
		 * TODO: Refactor the need for this out, as this is redundant here!
		 */
		isPlayingAd: function() {
			return true;
		},
		
		/**
		 * Start using this player
		 */
		start: function() {
			// Trigger this event for analytics
			this.trigger('adRequest', {
				sentData: {
					embedOriginUrl: window.alfyad_options.eUrl
				},
				receivedData: {},
				playerInstanceType: 1
			});
			
			// Insert the Optimatic player's JavaScript library to complete the initialization
			(function() {
				var s0 = document.createElement('script');
				s0.type = 'text/javascript';
				s0.src = 'http://cdn.optimatic.com/shell_min.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(s0, s);
			})();
		}
	},
	
	{
		/**
		 * @param {jQueryElement} containerElement The element to place the video player within
		 * @param {Object} params Structured data concerning the parameters generated in relation to the particular video load iteration by the Ndn_VideoPlayer instance
		 * @return {jQuery.Deferred} Resolves with the newly created video player instance when the video player could be created; rejects otherwise
		 */
		load: function(containerElement, params) {
			if (_debug) console.log('OptimaticPlayer.load(', arguments, ');');
			
			return $.Deferred($.proxy(function(deferred) {
				// If an instance of the Optimatic player has already been initialized to play, then indicate that this command could not be executed
				if (_hasInitializedPlayer) return deferred.reject();
				
				// Indicate that one instance of the player has begun its initialization (so that future initializations will be blocked)
				_hasInitializedPlayer = true;
				
				var player = new Ndn_Ads_OptimaticPlayer();
				
				player.on('all', function(eventName) {
					if (_debug) console.log('----(+)----- NDN_ADS_OPTIMATICPLAYER EVENT HEARD: ' + eventName);
				});
				
				// Store the parameters for this video load iteration
				player.set('params', params);
				
				/**/
				// Set the configuration options for the Optimatic player
				window.alfyad_options = {
					alfyPObj: Ndn_Utils_UrlParser.getParam('ndn_optimaticPObj', 'query') || 'n258d453n756kaptest', // 'n258d453n756kap', // 'q145a541', // 'n258d453n756kap', // TODO: Figure out what this value should be once we push to production!
					
					alfyzone: params.adTrackingGroupNumber, // Tracking Group to send to the ad network
					
					eUrl: Ndn_App.getEmbedOriginUrl(),
					
					on_playerAvailable: function() {
						if (_debug) console.log('on_playerAvailable event was heard! arguments ==', arguments);
						if (_debug) console.log(' >> window.alfyad_options ==', window.alfyad_options);
						
						window.alfyad_CreatePlayer();
					},
					
					handleCompanions: false, // true,
					
					on_noAd: function() {
						if (_debug) console.log('on_noAd event was heard! arguments ==', arguments);
						
						// TODO: Consider removing the "adUnavailable" event altogether
						player.trigger('adUnavailable');
						
						// Trigger the "adError" event with the limited non-precise data that we have
						player.trigger('adError', {
							receivedData: {
								errorType: 'adError'
							}
						});
					},
					
					on_adLoad: function() {
						if (_debug) console.log('on_adLoad event was heard! arguments ==', arguments);
						
						// TODO: Ensure that the needed "adLoad" event data is getting passed here once the EASI player makes it available
						player.trigger('adLoad', {
							adId: '',
							adPlatform: '',
							adSystem: '',
							adTitle: '',
							linear: '',
							podPosition: '',
							totalAds: '',
							workflow: '',
							url: '',
							duration: ''
						});
					},
					
					on_adStart: function() {
						if (_debug) console.log('on_adStart event was heard! arguments ==', arguments);
						
						player.trigger('adStart');
					},
					
					on_adDuration: function(duration) {
						if (_debug) console.log('on_adDuration event was heard! arguments ==', arguments);
						
						player.trigger('adTimeUpdate');
					},
					
					on_adComplete: function() {
						if (_debug) console.log('on_adComplete event was heard! arguments ==', arguments);
						
						try {
							player.trigger('adEnd');
						}
						catch (e) {
							if (_debug) console.log('----(++)----- EXCEPTION FOUND ==' + e);
						}
					},
					
					width: containerElement.width(),
					height: containerElement.height(), // 300, // containerElement.height(),
					default_volume: 0,
					
					// Custom Parameters
					custom1: params.pageViewSessionId, // The "insid"
					custom2: params.embedIndex, // Embed Index
					custom3: params.widgetId, // Widget ID
					
					content_url: ''
				};
				/**/
				
				if (_debug) console.log('window.alfyad_options ==', window.alfyad_options);
				
				containerElement.html(template);
				
				// Resolve the deferred object, passing along the newly instantiated Ndn_Ads_OptimaticPlayer instance
				deferred.resolve(player);
			}, this)).promise();
		}
	});
	
	return Ndn_Ads_OptimaticPlayer;
});