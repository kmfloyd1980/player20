define([], function() {
	/**
	 * @param {Function} callback The closure to execute once this timer reaches the end of the specified interval
	 * @param {int} [interval=0] The number of milliseconds for how often this timer gets executed
	 * @param {int} [numberOfTimesToExecute=1] The number of times to execute the callback provided to this timer; if the value of 0 is provided, then the interval will execute indefinitely
	 */
	return function(callback, interval, numberOfTimesToExecute) {
		/**
		 * Whether the timer is currently active
		 * @var {Boolean}
		 */
	    this.active = false;
	    
	    /**
	     * The callback function to execute once the timer concludes itself
	     * @var {Function}
	     */
	    this.callback = callback;
	    
	    /**
	     * The number of milliseconds for the  
	     * @var {int}
	     */
	    this.interval = interval || 0;
	    
	    /**
	     * @return {Boolean} Whether this timer is currently active
	     */
	    this.isActive = function() {
	    	return this.active;
	    };
	    
	    /**
	     * The number of times to execute before ... TODO: Implement this
	     * @var {int}
	     */
	    this.numberOfTimesToExecute;
	    
	    this.setNumberOfTimesToExecute = function(numberOfTimesToExecute) {
	    	return (typeof numberOfTimesToExecute != 'undefined')
	    		? numberOfTimesToExecute
	    	    : 1;
	    };
	    
	    // Reference to this instance of Ndn_Timer
	    var self = this;
	    
	    /**
	     * Starts the timer
	     * @return this
	     */
	    this.start = function() {
	        if (!this.isActive()) {
	            this.active = true;
	            this.timerObject = setInterval(function() {
	            	// console.log(this.numberOfTimesToExecute ==', this.numberOfTimesToExecute);
	            	if (self.numberOfTimesToExecute-- || numberOfTimesToExecute === 0) {
	            		self.callback();
	            	}
	            	else {
	            		self.stop();
	            	}
	            }, this.interval);
	        }
	        
	        return this;
	    };
	    
	    /**
	     * Executes the callback immediately
	     */
	    this.execute = function() {
	    	self.callback();
	    };

	    /**
	     * Stops the timer
	     * @return this
	     */
	    this.stop = function() {
	        this.active = false;
	        clearInterval(this.timerObject);
	        return this;
	    };
	    
	    /**
	     * Resets the timer
	     * @return this
	     */
	    this.reset = function() {
		    this.numberOfTimesToExecute = this.setNumberOfTimesToExecute(numberOfTimesToExecute);
	    	return this.stop().start();
	    };
	    
	    this.numberOfTimesToExecute = this.setNumberOfTimesToExecute(numberOfTimesToExecute);
	};
});