define([
    'jquery',
    'underscore',
    'backbone',
    'mustache',
    'models/Ndn/App',
    'models/Ndn/Widget',
    'models/Ndn/Utils/DomElement',
    'models/Ndn/Debugger',
    
    'jquery_autoellipsis',
    'lib/jquery_plugins/jquery.drags'
],
function(
    $,
    _,
    Backbone,
    Mustache,
    Ndn_App,
    Ndn_Widget,
    Ndn_Utils_DomElement,
    Ndn_Debugger
) { 
    /**
     * The float container that is currently floating
     * @var {Ndn_FloatContainer}
     */
    var _currentFloatContainer = null;
    
    /**
     * Whether debugging for this module is enabled
     * @var {Boolean}
     */
    var _debug = Ndn_Debugger.inMode('float');
    
    var Ndn_FloatContainer = Backbone.Model.extend({
        defaults: {
            /**
             * The product that may get placed within this modal container
             * @var {Ndn_Widget}
             */
            'product': null,
            
            /**
             * @var {jQueryElement}
             */
            'container': null,
            
            /**
             * Whether this float container is currently floating
             * @var {Boolean}
             */
            'isFloating': false,
            
            /**
             * Whether this float container is enabled
             * @var {Boolean}
             */
            'enabled': true,
            
            /**
             * @var {Integer}
             */
            'placeholderTop': null,
            
            /**
             * @var {Integer}
             */
            'placeholderBottom': null
        },
        
        /**
         * @param {Ndn_Widget} product The product that gets placed within this modal container
         * @param {Object} params Structured data concerning the behavior of this float container. Format:
         * <pre>{
         *  'initialPosition': String, // The initial position of the float container, for example "top left" or "bottom right"
         * }</pre>
         */
        initialize: function(product, params) {
            this.set({
                product: product
            });
            
            this.initializeDom(params.initialPosition);
        },
        
        /**
         * @param {String} initialPosition The initial position of the float container, for example "top left" or "bottom right"
         */
        initializeDom: function(initialPosition) {
            // Assign the appropriate dom element as this float container's top-most container element
            this.set('container', this.get('product').getContainerElement().find('> .ndn_floatContainer'));
            
            // Once this float container's container element has a height, record the appropriate placeholder coordinates
            Ndn_Utils_DomElement.onceHasWidthAndHeight(this.getContainer()).done($.proxy(function() {
                this.setPlaceholderCoordinates();
            }, this));
            
            // Snap this modal container to the top right corner of the browser window
            this.snap(initialPosition);
            
            // Initialize the event listeners specific to this float container
            this.initializeEventListeners();
            
            // If the "floatFillpage" debug mode is enabled, then fill the page with content that allows for the page to enable a vertical scrollbar
            if (Ndn_Debugger.inMode('floatFillpage')) {
                var fillPage = $('<p></p>'); for (var i = 0; i < 300; i++) { fillPage.append('Test<br/>'); } fillPage.appendTo('body');
            }
        },
        
        /**
         * Initialize the event listeners related to this float container
         */
        initializeEventListeners: function() {
            var container = this.getContainer(),
                product = this.get('product'),
                videoPlayer = product.get('videoPlayer');
            
            // Handle the ability to drag the float container and also handles the "snap" logic to the boundaries of the browser's window
            container.drags({
                handle: '.ndn_floatContainer_header > .ndn_videoTitle',
                placementHook: $.proxy(function(coordinates) {
                    // If this container is not currently floating, then do not allow for the container to get dragged
                    if (!this.isFloating()) return;
                    
                    var windowWidth = $(window).innerWidth(),
                        windowHeight = $(window).innerHeight(),
                        windowScrollTop = $(window).scrollTop(),
                        windowScrollLeft = $(window).scrollLeft(),
                        fixedCoordinates = {
                            top: coordinates.top - windowScrollTop,
                            left: coordinates.left - windowScrollLeft
                        },
                        containerWidth = container.outerWidth(),
                        containerHeight = container.outerHeight(),
                        snapToPixels = 25,
                        exceedsTop = fixedCoordinates.top <= 0 + snapToPixels,
                        exceedsBottom = windowHeight <= fixedCoordinates.top + containerHeight + snapToPixels,
                        exceedsLeft = fixedCoordinates.left <= 0 + snapToPixels,
                        exceedsRight = windowWidth <= fixedCoordinates.left + containerWidth + snapToPixels;
                    
                    if (exceedsTop) this.snap('top');
                    else if (exceedsBottom) this.snap('bottom');
                    else {
                        container.css({
                            top: fixedCoordinates.top,
                            bottom: 'auto'
                        });
                    }
                    
                    if (exceedsLeft) this.snap('left');
                    else if (exceedsRight) this.snap('right');
                    else {
                        container.css({
                            left: fixedCoordinates.left,
                            right: 'auto'
                        });
                    }
                    
                    // Make sure that while dragging this float container around, text is not inadvertently selected
                    this.deselectText();
                    
                    return false;
                }, this)
            });
            
            // Update the video title within the floating container
            product.on('hook:videoLoad', function(event) {
                container.find('> .ndn_floatContainer_header .ndn_videoTitle').html(event.videoData.videoTitle);
            });
            
            // Allow the "close" button to pause and then "unfloat" this float container's contents back to where it originated within the current webpage
            container.on('click', '.ndn_floatContainer_close', $.proxy(function() {
                if (this.isFloating()) {
                    videoPlayer.pause();
                    this.unfloat();
                }
            }, this));
            
            /*
            // When the player is paused by clicking on the video player
            videoPlayer.on('playerElementClick', $.proxy(function(event) {
                console.log('playerElementClick: videoPlayer.isPlaying() ==', videoPlayer.isPlaying());
                
                if (this.isFloating() && videoPlayer.isPlaying()) {
                    // Allow for any additional logic that plays out to occur before the code to react to the user pausing the video player is executed
                    setTimeout($.proxy(function() {
                        videoPlayer.pause();
                        this.unfloat();
                    }, this), 20);
                }
            }, this));
            */
            
            // Fade the "close" button in x-seconds after video playback begins
            videoPlayer.once('videoStart', $.proxy(function() {
                setTimeout(function() {
                    container.find('.ndn_floatContainer_close').fadeIn('slow');
                }, 3000);
            }, this));
            
            // Synchronizes the float behavior of this float container
            var synchronizeFloatBehavior = $.proxy(function() {
                if (this.isEnabled() && this.isPlaceholderInitialized()) {
                    if (this.isFloating() && this.isPlaceholderInViewVertically()) {
                        // If this float container's product is currently floating and the placeholder container comes into view,
                        // unfloat the product and replace the placeholder with the product
                        this.unfloat();
                    }
                    else if (!this.isFloating() && !this.isPlaceholderInViewVertically() && product.get('videoPlayer').isPlaying()) {
                        // If this float container's product is not currently floating and the product's container element goes out of view,
                        // float the product in this float container
                        this.float();
                    }
                }
            }, this);
            
            // Synchronize this float container's float behavior when the browser window scrolls or resizes 
            // or when the following events are triggered by the associated product's video player
            $(window).on('resize scroll', synchronizeFloatBehavior);
            product.get('videoPlayer').on('videoStart videoResume adStart adResume', synchronizeFloatBehavior);
        },
        
        /**
         * @return {Boolean} Whether this float container is enabled
         */
        isEnabled: function() {
            return this.get('enabled');
        },
        
        /**
         * Disables this float container so that it no longer activates automatically and unfloats the float container's contents
         */
        disable: function() {
            this.set('enabled', false);
            this.unfloat();
            
            // Set this product's video player to its click-to-play state
            this.get('product').get('videoPlayer')
            .setDefaultPlayOnLoadValue(false)
            .load();
        },
        
        /**
         * @return {Boolean} Whether the current placeholder coordinates are initialized (which happens when the float container's content's element has a width and height)
         */
        isPlaceholderInitialized: function() {
            return this.get('placeholderTop') !== null;
        },

        /**
         * @return {Boolean} Whether this float container's content's placeholder is in view within the browser vertically (horizontally is not considered)
         */
        isPlaceholderInViewVertically: function() {
            var elementTop = this.get('placeholderTop'),
                elementBottom = this.get('placeholderBottom'),
                windowTop = $(window).scrollTop(),
                windowBottom = windowTop + $(window).height();
            
            // Return true when the element's top edge is above the window's top edge and the element's bottom edge is below the window's bottom edge,
            // or when the element's top edge is between the window's top and bottom edge, 
            // or when the element's bottom edge is between the window's top and bottom edge
            return (elementTop < windowTop && elementBottom > windowBottom) ||
                (elementTop >= windowTop && elementTop <= windowBottom) ||
                (elementBottom >= windowTop && elementBottom <= windowBottom);
        },
        
        /**
         * @return {jQueryElement} The top-level container element of this modal container
         */
        getContainer: function() {
            return this.get('container');
        },
        
        /**
         * @return {Boolean} Whether this float container is currently floating
         */
        isFloating: function() {
            return this.get('isFloating');
        },
        
        /**
         * Record the top and bottom coordinates (placeholders) of the float container's content when it is not floating
         */
        setPlaceholderCoordinates: function() {
            var container = this.getContainer();
            this.set({
                'placeholderTop': container.offset().top,
                'placeholderBottom': container.offset().top + container.outerHeight()
            });
        },
        
        /**
         * Floats the video player associated with this float container
         */
        float: function() {
            // If this float container is already floating or if floating is disabled, there is nothing left to do
            if (this.isFloating() || !this.isEnabled()) return;
            
            // Make the float container visible
            this.getContainer()
            .removeClass('ndn_floatContainer_disabled')
            .addClass('ndn_floatContainer_enabled');
            
            // Unfloat any other float container that is currently floating, and keep track that this float container is now the sole float container that is currently floating
            Ndn_FloatContainer.registerFloat(this);
            
            // Indicate that this float container is currently floating
            this.set('isFloating', true);
        },
        
        /**
         * Unfloats the video player associated with this float container
         */
        unfloat: function() {
            // If this float container is already not floating, there is nothing left to do
            if (!this.isFloating()) return;
            
            // Hide the float container
            this.getContainer()
            .addClass('ndn_floatContainer_disabled')
            .removeClass('ndn_floatContainer_enabled');
            
            // Unregister this float container as the sole float container that is currently floating
            Ndn_FloatContainer.unregisterFloat(this);
            
            // Indicate that this float container is currently not floating
            this.set('isFloating', false);
        },
        
        /**
         * Deselects any text (used for preventing text getting selected while dragging the float container)
         */
        deselectText: function() {
            var selection = window.getSelection ? window.getSelection() : document.selection;
            if (selection) {
                if (selection.removeAllRanges) selection.removeAllRanges();
                else if (selection.empty) selection.empty();
            }
        },
        
        /**
         * @param {String} edges Space-delimited string indicating which edges to snap this modal container to (for example "top left" or "bottom")
         */
        snap: function(edges) {
            var edgesData = {
                'top': {top: 0, bottom: 'auto'},
                'right': {left: 'auto', right: 0},
                'bottom': {top: 'auto', bottom: 0},
                'left': {left: 0, right: 'auto'}
            };
            
            var container = this.getContainer();
            
            var edgesToSnapTo = edges.split(' ');
            $.each(edgesToSnapTo, function() {
                var edge = this;
                if (typeof edgesData[edge] != 'undefined') {
                    var edgeData = edgesData[edge];
                    container.css(edgeData);
                }
            });
        }
    },
    {
        /**
         * Unfloat and disable any float container that is currently floating and then register this float container as the new sole float container that is currently floating
         * @param {Ndn_FloatContainer} The float container to register as the sole float container that is floating
         */
        registerFloat: function(floatContainer) {
            // If there is a float container already floating, then disable and unfloat it so that it will never float again
            if (_currentFloatContainer) _currentFloatContainer.disable();
            
            // Keep track of which float container is currently floating
            _currentFloatContainer = floatContainer;
        },
        
        /**
         * Unregisters the provided float container as the sole float container that is currently floating, provided that said float container 
         * actually is the sole float container that is currently floating
         * @param {Ndn_FloatContainer} The float container to unregister as the sole float container that is currently floating
         */
        unregisterFloat: function(floatContainer) {
            if (_currentFloatContainer == floatContainer) _currentFloatContainer = null;
        }
    });
    
    return Ndn_FloatContainer;
});