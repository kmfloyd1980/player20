define([
    'jquery',
    'underscore',
    'models/Ndn/App',
    'models/Ndn/Utils/UserAgent',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/Widget/PlayerOptions',
    'models/Ndn/Legacy/Config'
], function(
    $,
    _,
    Ndn_App,
    Ndn_Utils_UserAgent,
    Ndn_Utils_UrlParser,
    Ndn_Widget_PlayerOptions,
    Ndn_Legacy_Config
) {
    /**
     * List of recognized config settings
     * @var {Array<String>} _recognizedConfigSettings
     */
    var _recognizedConfigSettings = [
        'distributorId',
        'widgetId',
        'type',
        'trackingGroup',
        'playlistId',
        'videoId',
        'providerId',
        'providerStoryId',
        'siteSection',
        'acceptsConfigFromUrl',
        'acceptsConfigFromUrlPrefix',
        'loyaltyProgramId',
        'loyaltyProgramData',
        'legacyEmbedUrl',
        'autoPlay',
        'playOnMouseover',
        'playOnInView',
        'continuousPlay',
        'rmmSoundOn',
        'splitVersion',
        'adsEnabled',
        'width',
        'height',
        'aspectRatio',
        'pb'
    ];
    
    /**
     * Structured data concerning the different types of widgets
     * @var {Object} _widgetTypesData
     */
    var _widgetTypesData = {
        'VideoPlayer/Default': {
            typeId: 5
        },
        'VideoPlayer/16x9': {
            typeId: 2
        },
        'VideoPlayer/Inline300': {
            typeId: 4,
            typeIdForAutoPlay: 13
        },
        'VideoPlayer/Inline590': {
            typeId: 11,
            typeIdForAutoPlay: 14
        },
        'VideoLauncher/Slider': {
            typeId: 21
        },
        'VideoLauncher/Playlist300x250': {
            typeId: 18
        },
        'VideoLauncher/Horizontal': {
            typeId: 19
        },
        'VideoLauncher/VerticalTower': {
            typeId: 20
        },
        'VideoPlayer/PTR': {
            typeId: 21
        },
        'VideoPlayer/Studio': {
            typeId: 33  // Because it's the best BSG episode </nerd> // TODO: Should this not be the value "16" per https://newsinc.atlassian.net/wiki/display/DEV/Analytics ?
        }
    };
    
    /**
     * A list of aliases for widget types
     * @var {Object} _widgetTypeAliases
     */
    var _widgetTypeAliases = {
        'videolauncher/slider300x250': 'VideoLauncher/Slider',
        'videoplayer/single': 'VideoPlayer/Default'
    };
    
    /**
     * Map for widget type label (in lowercase) paired with the appropriately cased widget type label. This is used so the "type" config setting may be case-insensitive. Format:
     * <pre>{
     *  {String}: {String}, // Widget type label in all lowercase, ie. "videoplayer/inline300" => widget type label in normal case, ie. "VideoPlayer/Inline300"
     *  ... // All other widget type labels
     * }</pre>
     * @var {Object<String,String>} _widgetTypeLabels
     */
    var _widgetTypeLabels = (function() {
        var result = {};
        _.each(_widgetTypesData, function(data, widgetTypeLabel) {
            result[widgetTypeLabel.toLowerCase()] = widgetTypeLabel;
        });
        return result;
    })();
    
    var _translateTypeAliases = function(type) {
        if (type.toLowerCase && _widgetTypeAliases[type.toLowerCase()]) {
            type = _widgetTypeAliases[type.toLowerCase()];
        }
        
        return type;
    };
    
    return {
        /**
         * @param {Object} providedConfig The config to get a cleaned-up clone of
         * @return {Object} A cleaned-up clone of the provided config
         */
        cleanConfig: function(providedConfig) {
            // console.log('cleanConfig(', providedConfig, ');');
            
            // Make sure the provided config object is not accidentally modified
            var config = _.extend({}, providedConfig);
            
            // Ensure that for all config settings that have string values, those values have no leading or trailing whitespace
            $.each(config, function(settingName, settingValue) {
                if (typeof config[settingName] == 'string') {
                    config[settingName] = $.trim(config[settingName]);
                }
            });
            
            // Ensure that the following config settings, if provided, are interpretted as booleans
            var booleanConfigSettings = ['autoPlay', 'playOnMouseover', 'rmmSoundOn', 'adsEnabled', 'acceptsConfigFromUrl'];
            $.each(booleanConfigSettings, function(index, configSetting) {
                if (typeof config[configSetting] == 'string') {
                    var settingValue = config[configSetting];
                    
                    if (settingValue == '') {
                        // When the config setting is merely present and has no value assigned to it, interpret that as true
                        settingValue = true;
                    }
                    else {
                        // If the config setting's provided value is "true" or "1", then convert the value to the boolean value of true; 
                        // otherwise, convert this value to the boolean value of false
                        if (_.contains(['true', '1'], settingValue.toLowerCase())) {
                            settingValue = true;
                        }
                        else {
                            settingValue = false;
                        }
                    }
                    
                    config[configSetting] = settingValue;
                }
                else if (typeof config[configSetting] != 'undefined') {
                    // If this config setting is provided and is not a string, make sure it is a boolean value
                    config[configSetting] = !!config[configSetting];
                }
            });
            
            // Ensure that the following config settings, if provided, are interpretted as integers
            var integerConfigSettings = ['continuousPlay', 'pb'];
            $.each(integerConfigSettings, function(index, configSetting) {
                if (typeof config[configSetting] != 'undefined') {
                    config[configSetting] = parseInt(config[configSetting]);
                }
            });
            
            // Remove any unrecognized config settings
            $.each(config, function(index) {
                if (!_.contains(_recognizedConfigSettings, index)) {
                    delete config[index];
                }
            });
            
            // If the provided config has the "type" config setting present
            if (config['type']) {
                // We occasionally want to alias player types, like 'Single' to 'Default' or 'Slider300x250' to 'Slider'
                config['type'] = _translateTypeAliases(config['type']);
                
                // Ensure that the "type" config setting is using the correct case that the rest of this application expects
                config['type'] = _widgetTypeLabels[(config['type'] || '').toLowerCase()];
                
                // If the "type" config setting is provided but its value is not recognized, unset/ignore the config setting
                if (config['type'] && typeof _widgetTypeLabels[config['type'].toLowerCase()] == 'undefined') {
                    delete config['type'];
                }
            }
            
            // The "pb" config setting is only allowed for the Single player
            if (config['type'] != 'VideoPlayer/Default') delete config['pb'];
            
            return config;
        },
        
        /**
         * @param {Object} config
         * @param {Object} settings
         * @return {int} An integer that identifies certain scenarios about the provided config settings that is used for analytics, referred to as the "plt" value because it is passed in a parameter named "plt"
         */
        getPltValue: function(config, settings) {
            var widgetTypeData = _widgetTypesData[config['splitVersion'] || config['type']];
            
            var behaviorSettings = settings.settings.behavior;
            
            // If the floating player behavior for the Single player is enabled and this product is the Single player
            if (behaviorSettings.float.enabled && config['type'] == 'VideoPlayer/Default') return 24;
            
            var rmmSettings = behaviorSettings.rmm;
            if (rmmSettings.enabled && rmmSettings.continuousPlay > 0) return 22;
            
            var ptrSettings = behaviorSettings.ptr;
            if ((config['splitVersion'] || config['type']) == 'VideoPlayer/PTR' && ptrSettings.playsInIframe) return 23;
            
            // If this widget's "autoPlay" config setting is set to true and this widget's type has a special widget type id to use 
            // when this is the case, use special widget type id; otherwise, return the widget type's normal widget type id
            if (config.autoPlay && widgetTypeData.typeIdForAutoPlay) {
                return widgetTypeData.typeIdForAutoPlay;
            }
            else if (config.rmmSoundOn != false && widgetTypeData.typeIdForAutoPlay) {
                return widgetTypeData.typeIdForAutoPlay;
            }
            else {
                return widgetTypeData.typeId;
            }
        },
        
        /**
         * @param {Object} providedConfig
         * @param {Object} configDefaults
         * @param {Object} unprocessedSettings The unprocessed player settings returned by player services
         */
        getFinalizedConfig: function(providedConfig, configDefaults, unprocessedSettings) {
            var browserSettings = this.getBrowserSettings(unprocessedSettings);
            
            // Process the "pb" config setting based on the provided config
            var config = this.processPlayerBehaviorSetting(providedConfig);
            
            // Apply the config defaults provided by player services
            config = this.applyConfigDefaults(config, configDefaults);
            
            // Apply the browser settings, which affect the final outcome of the provided config
            config = this.applyBrowserSettings(config, browserSettings);
            
            // Remove the "pb" config setting, and have it get determined based on the config settings that are now set for the embedded product's config
            if (typeof config['pb'] != 'undefined') delete config['pb'];
            config = this.processPlayerBehaviorSetting(config);
            
            return config;
        },
        
        /**
         * @param {Object} providedConfig The config to apply config defaults to
         * @param {Object} configDefaults The config defaults to apply to the provided config
         * @return {Object} Structured data concerning a config that has applied the provided config defaults to the provided config
         */
        applyConfigDefaults: function(providedConfig, configDefaults) {
            var config = _.extend({}, providedConfig);
            
            // Do not allow for "adsEnabled" to be overrideable via the provided config
            delete config['adsEnabled'];
            
            // Get the result of applying the config defaults to the provided config (where the provided config overrides the config defaults)
            config = _.extend({}, configDefaults, config);
            
            // The "playOnMouseover" config setting is only allowed for the Single player
            if (config['playOnMouseover'] && config['type'] != 'VideoPlayer/Default') config['playOnMouseover'] = false;
            
            return config;
        },
        
        /**
         * @param {Object} settings The unprocessed provided settings from player services (these settings are not required to have been processed yet by Ndn_Widget.processSettings())
         * @return {Object} Structured data concerning settings that are determined by the browser. Format:
         * <pre>{
         *  'isAbleToPlayVideoAutomatically': Boolean, // Whether the browser is able to play video (as in a <video> element or Flash embed) automatically (iPhones cannot for example)
         *  'playOnInViewToggleForSinglePlayers': Boolean, // The "playOnInView" config setting to set for products when their "type" config setting is "VideoPlayer/Single" when this toggle is enabled via the "autoplayToggleForSinglePlayersEnabled" property
         *  'playOnInViewToggleForSinglePlayersEnabled': Boolean, // Whether the "autoplayToggleForSinglePlayers" property is respected
         * }</pre>
         */
        getBrowserSettings: function(settings) {
            return {
                'isAbleToPlayVideoAutomatically': Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically(),
                'playOnInViewToggleForSinglePlayers': Ndn_Widget_PlayerOptions.getPlayOnInViewToggleForSinglePlayers(),
                'playOnInViewToggleForSinglePlayersEnabled': settings.settings.behavior.provided.playOnInViewToggle.enabled
            };
        },
        
        /**
         * @param {Object} providedConfig The config to apply the provided browser settings to
         * @param {Object} browserSettings Structured data concerning settings that are determined by the browser
         */
        applyBrowserSettings: function(config, browserSettings) {
            // TODO: Refactor this so that the "splitVersion" is determined by the player services settings.settings.behavior.provided.ptr value instead of the default config values (NEEDS RESTRUCTURING/REFACTORING)
            // TODO: Refactor the below code out of the front-end logic (and should be handled in player services response)
            // Don't use the "ptr" split version if it should not apply to the current situation
            if (config['splitVersion'] == 'VideoPlayer/PTR' && (!_.contains(['VideoLauncher/Slider', 'VideoLauncher/Playlist300x250'], config['type']) || !browserSettings.isAbleToPlayVideoAutomatically)) {
                config['splitVersion'] = '';
            }
            
            // If the "playOnInView" config setting is toggled via data stored by the browser, then update the config setting appropriately
            if (browserSettings.playOnInViewToggleForSinglePlayersEnabled && config['type'] == 'VideoPlayer/Default') {
                config['playOnInView'] = browserSettings.playOnInViewToggleForSinglePlayers;
            }
            
            // If the current browser is unable to play videos automatically, modify the config so that it indicates that "autoPlay" is turned off
            if (!browserSettings.isAbleToPlayVideoAutomatically) {
                config['autoPlay'] = false;
                config['playOnInView'] = false;
            }
            
            return config;
        },
        
        /**
         * @param {jQueryElement} element The dom element wrapped in jQuery that specifies a widget's config for embed
         * @return {Object} Structured data concerning a widget's config for embed
         */
        getConfigFromElement: function(element) {
            // console.log('getConfigFromElement(', element, ');');
            
            var result = {},
                regEx = new RegExp(this.hasUpdatedEmbedCode(element) ? "^data-(.*)$" : "^data-config-(.*)$");
            
            $.each(element[0].attributes, function(index, attribute) {
                var matches;
                if (matches = attribute.name.replace(/\s/g, '').toLowerCase().match(regEx)) {
                    var configSettingId = matches[1].replace(/-(\w)/g, function(match, p1) {
                        return p1.toUpperCase();
                    });
                    
                    result[configSettingId] = attribute.value;
                }
            });
            
            return result;
        },
        
        /**
         * @return {String} The default prefix to namespace config settings that are receivable from the URL params
         */
        getDefaultUrlVarPrefixForConfigSettings: function() {
            return 'ndn.';
        },
        
        /**
         * @param {String} paramsPrefix The prefix of config parameters accepted via the current landing page URL
         * @return {Boolean} Whether the current URL is intended for the legacy player suite, but the legacy player suite's code responsible for producing a landing page has been replaced with the new NDN Player Suite
         */
        isOnForcedMigrationRedirectedLandingPage: function(paramsPrefix) {
            return !Ndn_Utils_UrlParser.getUrlVar('legacyEmbedUrl') && Ndn_Utils_UrlParser.getUrlVar('freewheel') && !Ndn_Utils_UrlParser.getUrlVar(paramsPrefix + 'trackingGroup');
        },
        
        /**
         * @param {String} url The URL to a landing page that hosts an NDN Player Suite product, which receives its config settings from this URL
         * @return {Object} Structured data concerning an embedded product's config settings
         */
        getConfigFromLandingPageUrl: function(url) {
            // console.log('Ndn_Widget_Config.getConfigFromLandingPageUrl(', url, ');');
            
            var paramsPrefix = Ndn_App.getAppUrlInfo(url) ? '' : Ndn_Widget_Config.getDefaultUrlVarPrefixForConfigSettings(),
                params = Ndn_Utils_UrlParser.parseHref(url).queryParams;

            var result = {};
            
            $.each(params, function(paramName, paramValue) {
                if (paramName.substr(0, paramsPrefix.length) == paramsPrefix) {
                    result[paramName.substr(paramsPrefix.length)] = paramValue;
                }
            });
            
            return result;
        },
        
        /**
         * @param {DOMNode} containerElement
         */
        processConfigFromElement: function(containerElement, config) {
            return this.process($.extend({}, this.getConfigFromElement(containerElement), config || {}));
        },
        
        /**
         * @param {Object} config
         * @param {Boolean} [useUpdatedEmbedCode] Whether to use the updated embed code (with data-* instead of data-config-* attributes); defaults to false
         */
        getEmbedCode: function(config, useUpdatedEmbedCode, containerId) {
            if (typeof useUpdatedEmbedCode == 'undefined') useUpdatedEmbedCode = false;
            
            var result = '<div class="ndn_embed"',
                attributePrefix = useUpdatedEmbedCode ? 'data-' : 'data-config-';
            
            // If an "id" attribute is provided for this configurable <div> element, add it to the resulting html
            if (containerId) result += ' id="' + containerId + '"';
            
            $.each(config, function(configSettingName, configSettingValue) {
                var attributeName = attributePrefix + configSettingName.replace(/[A-Z]/g, function(match) {
                    return '-' + match.toLowerCase();
                });
                
                result += ' ' + attributeName + '="' + configSettingValue + '"';
            });
            
            result += '></div>';
            
            return result;
        },
        
        /**
         * @param {Object} config
         * @return {jQuery.Deferred} Resolves once the provided config is fully processed
         */
        process: function(config) {
            // console.log('process(', config, ');');
            
            config = this.cleanConfig(config);
            
            // console.log('config ==', config);
            
            // If this widget accepts config settings from the URL's query string
            if (config['acceptsConfigFromUrl']) {
                var paramsPrefix = config.hasOwnProperty('acceptsConfigFromUrlPrefix')
                    ? config['acceptsConfigFromUrlPrefix']
                    : this.getDefaultUrlVarPrefixForConfigSettings();
                
                // If the current URL is intended for the legacy player suite, but the legacy player suite's code responsible 
                // for producing a landing page has been replaced with the new NDN Player Suite
                if (this.isOnForcedMigrationRedirectedLandingPage(paramsPrefix)) {
                    // If the "legacyEmbedUrl" config setting has not already been set by the configurable <div> element
                    if (!config['legacyEmbedUrl']) {
                        config['legacyEmbedUrl'] = window.location.href + '';
                    }
                }
                else {
                    // console.log('paramsPrefix ==', paramsPrefix);
                    // console.log('config ==', config);
                    // console.log('Ndn_Utils_UrlParser.getUrlVarsWithPrefix(paramsPrefix) ==', Ndn_Utils_UrlParser.getUrlVarsWithPrefix(paramsPrefix));
                    
                    config = this.cleanConfig(_.extend({}, Ndn_Utils_UrlParser.getUrlVarsWithPrefix(paramsPrefix), config));
                }
            }
            
            return $.Deferred(function(deferred) {
                // If the "legacyEmbedUrl" config setting is present, then this is a legacy embed and needs to be migrated
                if (config['legacyEmbedUrl']) {
                    Ndn_Legacy_Config.translate(config.legacyEmbedUrl, config['type'])
                    .done(function(translatedConfig) {
                        // Override the translated config with any other config values that were passed explicitly
                        // delete config.legacyEmbedUrl;
                        config = _.extend(translatedConfig, config);
                        
                        deferred.resolve(config);
                    });
                }
                else {
                    deferred.resolve(config);
                }
            }).promise()
            .always(function(config) {
                // Make sure the "type" property is defined
                if (typeof config['type'] == 'undefined') {
                    config['type'] = '';
                }
                
                // Remove any whitespace from the provided "type" config setting
                config['type'] = (config['type'] + '').replace(' ', '');
                
                // If the "type" config setting is not provided, then default to the Single player
                if (config['type'] == '') {
                    config['type'] = 'VideoPlayer/Default';
                }
            });
        },
        
        /**
         * Processes the "pb" (player behavior) config setting by using the provided "pb" property of the "config" parameter to correctly fill in (and potentially override)
         * the other related config settings. 
         * 
         * If no "pb" property is present in the provided "config" parameter, then the "pb" setting is determined and added to the provided config using
         * the config's current settings.
         * @param {Object} providedConfig Structured data concerning the config to use for an embedded product
         * @return {Object} Newly created object of structured data concerning the provided config but with its "pb" setting processed
         */
        processPlayerBehaviorSetting: function(providedConfig) {
            var recognizedPlayerBehaviorIds = {
                '0': {
                    autoPlay: false,
                    playOnMouseover: false
                },
                '1': {
                    autoPlay: true,
                    playOnMouseover: false
                },
                '3': {
                    autoPlay: false,
                    playOnMouseover: true
                }
            };   
            
            // Make a shallow copy of the config object (so that the provided parameter is not accidentally modified)
            var config = $.extend({}, providedConfig);
            
            var playerBehaviorIdToCheck = config['pb'];
            if (typeof recognizedPlayerBehaviorIds[playerBehaviorIdToCheck] != 'undefined') {
                config = $.extend({}, config, recognizedPlayerBehaviorIds[playerBehaviorIdToCheck]);
                config['pb'] = playerBehaviorIdToCheck;
            }
            else {
                $.each(recognizedPlayerBehaviorIds, function(playerBehaviorId, playerBehaviorConfig) {
                    var matchesPlayerBehavior = true;
                    $.each(playerBehaviorConfig, function(setting, settingValue) {
                        if (config[setting] !== settingValue) {
                            matchesPlayerBehavior = false;
                        }
                    });
                    
                    // If a start behavior is detected, then set that to the "pb" config setting, and exit the loop
                    if (matchesPlayerBehavior) {
                        config['pb'] = playerBehaviorId;
                        return false;
                    }
                });
            }
            
            return config;
        },
        
        /**
         * @param {jQueryElement} element The embedded product container element to check whether it is using the updated embed code
         * @return {Boolean} Whether the provided container element is using the updated embed code, which means that "data-*" attributes are used instead of the redundant "data-config-*" attributes
         */
        hasUpdatedEmbedCode: function(element) {
            if (!Ndn_App.getSetting('updatedEmbedCode')) return false;
            
            var result = true;
            
            $.each(element[0].attributes, function(index, attribute) {
                var matches;
                if (matches = attribute.name.replace(/\s/g, '').toLowerCase().match(/^data-config-(.*)$/)) {
                    var configSettingId = matches[1].replace(/-(\w)/g, function(match, p1) {
                        return p1.toUpperCase();
                    });
                    
                    // If this is not the "data-config-id" attribute, then this is not using the updated embed code
                    if (configSettingId != 'id') {
                        result = false;
                    }
                }
            });
            
            return result;
        },
        
        /**
         * @param {Object} config
         * @param {Boolean} [hasUpdatedEmbedCode] Whether the embed code is updated to just use "data-*" instead of "data-config-*"; defaults to false
         */
        processDefaultDimensions: function(config, hasUpdatedEmbedCode) {
            if (typeof hasUpdatedEmbedCode == 'undefined') hasUpdatedEmbedCode = false;
            
            var result = _.extend({}, config);
            
            // If this is the Single player, does not have an "aspectRatio" config setting value provided, and has updated embed code (using data-* attributes instead of data-config-*)
            if (result['type'] == 'VideoPlayer/Default' && !result['aspectRatio'] && hasUpdatedEmbedCode) {
                // Only add a default value for the "aspectRatio" when either the "width" or "height" config settings are not present
                if (!result['width'] || !result['height']) {
                    result['aspectRatio'] = '16:9';
                }
            }
            
            var aspectRatio = result['aspectRatio'] + '';
            
            // If the "aspectRatio" config setting is provided and it includes the ":" character as expected
            if (aspectRatio.match(/:/g)) {
                var tokens = aspectRatio.split(':'),
                    widthRatio = tokens[0],
                    heightRatio = tokens[1];
                
                if (result['width']) {
                    // If "width" is provided, set "height" to the appropriate formula
                    result['height'] = heightRatio + '/' + widthRatio + 'w';
                }
                else {
                    if (result['height']) {
                        // If "width" is not provided but "height" is, set the "width" to the appropriate formula
                        result['width'] = widthRatio + '/' + heightRatio + 'h';
                    }
                    else {
                        // If neither the "width" or "height" config settings are provided, set "width" to "100%" and "height" to the appropriate formula
                        result['width'] = '100%';
                        result['height'] = heightRatio + '/' + widthRatio + 'w';
                    }
                }
            }
            
            return result;
        }
    };
});