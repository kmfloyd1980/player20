define([
	 	'jquery',
	 	'models/Ndn/Widget',
	 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
	 	'views/Ndn/Widget/VideoPlayer/DefaultView',
	 	'models/Ndn/FloatContainer',
	 	'models/Ndn/Widget/PlayerOptions',
	 	
	 	'text!templates/ndn/widget/video-player/default/default.html',
	 	
	 	'models/Ndn/VideoPlayer'
 	], function(
 		$,
 		Ndn_Widget,
 		Ndn_Widget_VideoPlayer_AbstractModel,
 		Ndn_Widget_VideoPlayer_DefaultView,
        Ndn_FloatContainer,
        Ndn_Widget_PlayerOptions,
 		
 		defaultTemplate,
 		
 		Ndn_VideoPlayer
 	) {
		return Ndn_Widget_VideoPlayer_AbstractModel.extend({
			defaults: {
				/**
				 * The view associated with this model
				 * @var {Ndn_AbstractView}
				 */
				'widgetView': Ndn_Widget_VideoPlayer_DefaultView,

				/**
				 * An associative array mapping template aliases to their respective template content
				 * @var {Array<String,String>}
				 */
				'templates': {
					'default': defaultTemplate
				},
				
				/**
				 * The maximum number of playlist items that may be associated with this widget
				 * @var {int}
				 */
				'playlistItemsLimit': 40
			},
			
			initialize: function(config) {
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
				
				// If once this product's video player is initialized it is realized that the floating behavior for this product's video player is enabled
				this.on('containerInitialized', function() {
				    var behaviorSettings = this.getCachedSettings().settings.behavior,
				        floatSettings = behaviorSettings.float;
				    
				    if (floatSettings.enabled) {
				        // Create the float container for this product
				        new Ndn_FloatContainer(this, {
				            initialPosition: floatSettings.initialPosition
				        });
				    }
				    
				    if (behaviorSettings.playOnInViewToggle.enabled) {
				        // Create the player options to appear just below this product on the page
			            new Ndn_Widget_PlayerOptions(this);
				    }
				});
			},
			
			initializeVideoPlayer: function() {
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
				
				var videoPlayer = this.get('videoPlayer'),
					parentWidget = this.getParentWidget();
				
				videoPlayer.on('embed', function() {
					videoPlayer
					.setDefaultPlayOnLoadValue(parentWidget.get('config')['autoPlay'])
					.load();
				});
			},
			
			/*
			* Returns: Boolean - if the player has autoPlay enabled
			*/
			isAutoplayEnabled: function(){							
				return this.get('videoPlayer').get('widget').get('config')['autoPlay'];
			}
		});
	}
);