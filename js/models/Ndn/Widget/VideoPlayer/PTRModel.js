define([
 	'jquery',
 	'underscore',
 	'models/Ndn/Widget',
 	'models/Ndn/Debugger',
 	'models/Ndn/Utils/UserAgent',
 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
 	'models/Ndn/Widget/VideoLauncher/SliderModel',
 	'views/Ndn/Widget/VideoPlayer/PTRView',
 	
 	'text!templates/ndn/widget/video-launcher/slider/default.html',
	'text!templates/ndn/widget/video-launcher/slider/selectableVideos.html',
 	'text!templates/ndn/widget/video-launcher/slider/thumbnails.html'
], function(
	$,
	_,
	Ndn_Widget,
	Ndn_Debugger,
	Ndn_Utils_UserAgent,
	Ndn_Widget_VideoPlayer_AbstractModel,
	Ndn_Widget_VideoLauncher_SliderModel,
	Ndn_Widget_VideoPlayer_PTRView,
	
	defaultTemplate,
	selectableVideosTemplate,
	thumbnailsTemplate
) {
	return Ndn_Widget_VideoPlayer_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoPlayer_PTRView,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate,
				'selectableVideos': selectableVideosTemplate,
				'thumbnails': thumbnailsTemplate
			}, 
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 15,

			/**
			 * The length of the clip to play, measured in seconds, when in preview mode (0 denotes the entire video)
			 * @var {int}
			 */
			'videoLengthToPlay': 30,
			
			/**
			 * Whether the widget is currently being interacted with (the user's mouse is over the widget's selectable videos window for x-number of seconds)
			 * @var {Boolean}
			 */
			'userHasInteracted': false,

			/**
			 * Whether the period of time where this product was using a video player has concluded
			 * @var {Boolean}
			 */
			'hasConcludedVideoPlayer': false,
			
			/**
			 * The timeout ID for the interval that is waiting to update this product's "userHasInteracted" property to true. This timeout is activated by user engagement with the product.
			 * @var {int}
			 */
			'userInteractionTimer': null,
			
			/**
			 * The width (measured in pixels) that had been added to the width of this slider's video player (used when ensuring the optimal video player width)
			 * @var {int}
			 */
			'addedVideoPlayerWidth': 0,
			
			/**
			 * The optimal width of the video player to use while it displays ads
			 * @var {int}
			 */
			'optimalWidth': 315,
			
			/**
			 * Whether the aspect ratio of the top container is enabled
			 * @var {Boolean}
			 */
			'aspectRatioEnabled': true,
			
			/**
			 * Whether the previous video that was played was muted
			 * @var {Boolean}
			 */
			'previousVideoMuted': false,
			
			/**
			 * The showcase thumbnail carousel, which displays the large clickable thumbnail/image (the "slideshow carousel")
			 * @var {jQuery.bxSlider}
			 */
			'carousel': null,
			
			/**
			 * The carousel that displays a set of thumbnails at a time
			 * @var {jQuery.bxSlider}
			 */
			'thumbnailCarousel': null
		},

		initialize: function(config, parentWidget) {
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
		},
		
		/**
		 * @return {Boolean} Whether this product should play the PTR experience (or otherwise fallback to playing the default RMM experience)
		 */
		shouldPlayPtrExperience: function() {
		    return this.isInView(this.get('view').$el) || (Ndn_Widget.isEmbeddedByIframe() && this.getCachedSettings().settings.behavior.ptr.playsInIframe);
		},
		
		initializeVideoPlayer: function() {
            // Determine whether PTR experience is enabled for this product
            this.setIsPtr(this.shouldPlayPtrExperience());
            
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
			
			var videoPlayer = this.get('videoPlayer');
			
			videoPlayer.setPlaylistItemsLimit(this.get('playlistItemsLimit'));
			videoPlayer.toggleInfoPanel(false);
			
			videoPlayer.on('embed', $.proxy(function() {
			    if (Ndn_Debugger.inMode('ptr')) console.log('Embed event heard... this.shouldPlayPtrExperience() ==', this.shouldPlayPtrExperience());
			    
				if (this.isPtr()) {
	                if (Ndn_Debugger.inMode('ptr')) console.log('this.isPtr() ==', this.isPtr());
					
					videoPlayer
					.setDefaultPlayOnLoadValue(this.isAbleToPlayRmm())
					.load();
				}
				else {
					this.initializeVideoPlayerForRmm(true);
				}
			}, this));
		},

		shouldBeMuted: function() {
			if (this.get('userHasInteracted')) {
				return false;
			}

			return true;
		},

		/**
		 * @return {Boolean} Whether thumbnails are displayed
		 */
		getThumbnailsEnabled: function() {
			return Ndn_Widget_VideoLauncher_SliderModel.prototype.getThumbnailsEnabled.apply(this, arguments);
		},
		
		/**
		 * @return {Boolean} Whether the top carousel is enabled
		 */
		isShowcaseCarouselEnabled: function() {
			return Ndn_Widget_VideoLauncher_SliderModel.prototype.isShowcaseCarouselEnabled.apply(this, arguments);
		},

		/**
		 * @return {jQuery.Deferred} Resolves with an array of videos to be displayed in the slider
		 */
		getVideos: function() {
			return $.Deferred($.proxy(function(deferred) {
				var config = this.get('config');
				var videos = _.first(this.get('videoPlayer').getActivePlaylistData()['items'], 6);

				videos = this.getVideoDataForLaunchers(videos);

				deferred.resolve(videos);
			}, this)).promise();
		},
		
		getCurrentPlaylistLength: function() {
			return this.get('videoPlayer').get('playlistData').items.length;
		},
		
		/**
		 * @return {Boolean} Whether video playback should end completely upon the first "videoEnd" event being encountered
		 */
		shouldPlaybackEndUponVideoEnd: function() {
		    return !this.get('userHasInteracted') || !this.shouldPlayPtrExperience() || this.get('previousVideoMuted');
		},
		
		mutePlayer: function() {
			this.get('videoPlayer').setMuted(true);
		},
		
		unmutePlayer: function() {
			this.get('videoPlayer').setMuted(false);
		},
		
		isAspectRatioOfTopContainerEnabled: function() {
			return Ndn_Widget_VideoLauncher_SliderModel.prototype.isAspectRatioOfTopContainerEnabled.apply(this, arguments);
		},
		
		toggleAspectRatioOfTopContainer: function(toggle) {
			return Ndn_Widget_VideoLauncher_SliderModel.prototype.toggleAspectRatioOfTopContainer.apply(this, arguments);
		},

        onThumbnailClick: function() {
            if (!this.get('hasConcludedVideoPlayer')) {
				if (this.get('videoPlayer').isShowingAd()) {
					this.get('videoPlayer').setMuted(true);
				} else {
					this.get('videoPlayer').pause();
				}
            }
        },

		setUserInteractionTimer: function() {
			// If a user interaction timer is already set, unset it so a new one can replace it
			if (this.get('userInteractionTimer')) {
				this.unsetUserInteractionTimer();
			}
			
			// After x-seconds of not having this user interaction timer get unset (due to removed user interaction), update the "userHasInteracted" property to indicate 
			// that this product has been interacted with and unset the user interaction timer as it is no longer needed
			this.set('userInteractionTimer', setTimeout($.proxy(function() {
				this.set('userHasInteracted', true);
				this.unsetUserInteractionTimer();
			}, this), 2000));
		},
		
		/**
		 * Unset the user interaction timer
		 */
		unsetUserInteractionTimer: function() {
			clearTimeout(this.get('userInteractionTimer'));
			this.set('userInteractionTimer', null);
		},

		destroyPlayer: function() {
			var currentTime  = this.get('videoPlayer').get('player').getCurrentTime();
			// this.get('videoPlayer').setMuted(false);
			this.get('videoPlayer').get('player').destroy();

			// The video player is sometimes destroyed before the analytics are sent.  This should allow them to function properly.
			var dummyPlayer = {
				getCurrentTime: function() {
					return currentTime;
				},

				isPlaying: function() {
					return false;
				},

				pause: function() {
					return;
				}
			};

			this.get('videoPlayer').set('player', dummyPlayer);
		}
	});
});
