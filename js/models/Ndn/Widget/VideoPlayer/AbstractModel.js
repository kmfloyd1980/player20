define([
		'jquery',
		'backbone',
		'models/Ndn/Widget',
		'models/Ndn/VideoPlayer',
		'models/Ndn/Utils/UserAgent'
	], function(
		$,
		Backbone,
		Ndn_Widget,
		Ndn_VideoPlayer,
		Ndn_Utils_UserAgent
	) {
		/**
		 * The playlist id of the "Must See" playlist that gets played after the first playlist finishes playing
		 * @var {String}
		 */
		var _mustSeePlaylistId = '14126';
		
		return Ndn_Widget.extend({
			defaults: {
				/**
				 * The video player
				 * @var {?Ndn_VideoPlayer}
				 */
				'videoPlayer': null
			},
			
			initialize: function() {
				Ndn_Widget.prototype.initialize.apply(this, arguments);
			},
			
			initializeVideoPlayer: function() {
				// Initialize the video player
				this.set('videoPlayer', new Ndn_VideoPlayer(this.getParentWidget()));
				
				var videoPlayer = this.get('videoPlayer'),
					parentWidget = this.getParentWidget();

				// Set the playlist items limit according to the split version model's "playlistItemsLimit" property
				videoPlayer.setPlaylistItemsLimit(this.get('playlistItemsLimit'));
				
				// Incorporate the supported hook events for this widget
				videoPlayer.on({
					'pause': function() {
						parentWidget.trigger('hook:videoPause', {
							currentTime: videoPlayer.getCurrentTime()
						});
					},
					
					'hook:rmmStartRequest': function() {
						parentWidget.trigger('hook:rmmStartRequest', {});
					}
				});
				
				// If the "providerId" and "providerStoryId" config settings were provided within the configurable <div> element, but no video data was returned
				// by player services to be played by the player, collapse the configurable <div> element so that it is no longer visible
				videoPlayer.addDeliberator('loadAttempt', function(deferred) {
					parentWidget.getSettings().done(function(settings) {
						var widgetConfig = parentWidget.get('config');
						if (widgetConfig['providerId'] && widgetConfig['providerStoryId'] && !settings.videosData) {
							// Hide this widget's container element completely from the page
							parentWidget.getContainerElement().css({
								'display': 'none',
								'width': '0px',
								'height': '0px'
							});
							
							// Do not allow the load that is being attempted
							deferred.reject();
						}
						else {
							deferred.resolve();
						}
					});
				});
				
				videoPlayer.on({
					// Execute this widget's loyalty program (if it has one) upon the conclusion of an advertisement
					'adEnd': $.proxy(function() {
						if (this.get('loyaltyProgram')) {
							this.get('loyaltyProgram').execute();
						}
					}, this.getParentWidget())
				});

				// Handle which playlist loads next
				if (parentWidget.get('config')['videoId']) {
					videoPlayer.on('isolatedVideoEnd', $.proxy(function() {
						// Once the next video is requested to begin playing, set the next playlist id for the video player to the "Must See" playlist
						videoPlayer.once('videoStartRequest', function() {
							this.setNextPlaylistId(_mustSeePlaylistId);
						});
					}, this));
				}
				else {
					videoPlayer.setNextPlaylistId(_mustSeePlaylistId);
				}
				this.get('videoPlayer').toggleInfoPanel(false);
				this.trigger('videoPlayerInitialized');
			},

			/**
			 * Initializes what to do with this widget's video player when RMM is playable for this type of widget
			 */
			initializeVideoPlayerForRmm: function(forceRmm) {
				// console.log('initializeVideoPlayerForRmms()...');
				forceRmm = forceRmm || false;
				
				// console.log(forceRmm);
				var videoPlayer = this.get('videoPlayer');
				
				// If the video player is clicked during an RMM bumper, set the default "play on load" value appropriately 
				// and begin playing the first video like normal
				videoPlayer.on('click', $.proxy(function() {
					// If the RMM bumper is showing, begin "continuous play mode" by playing the first video loaded into the video player
					if (videoPlayer.isShowingRmm() && videoPlayer.isShowingVideo()) {
						// TODO: Refactor out the "setDefaultPlayOnLoadValue()" code
						videoPlayer
						.setDefaultPlayOnLoadValue(this.getParentWidget().get('config')['autoPlay'])
						.play(0);
					}
				}, this));
				
				// TODO: Refactor this method so that "forceRmm" becomes "loadRmm" and the value this.getParentWidget().get('config')['autoPlay'] gets passed to this parameter
				if (this.getParentWidget().get('config')['autoPlay'] && !forceRmm) {
					// console.log('loadRmm');
					// If the current widget is configured to automatically begin playing a video, do so
					videoPlayer
					.setDefaultPlayOnLoadValue(true)
					.load();
				}
				else {
					if (this.isAbleToPlayRmm()) {
						// console.log('isAbleToPlayRMM');
						// Play an RMM in the video player; once the RMM has concluded, enable the "auto-advance" feature/mode
						videoPlayer
						.once('rmmContinuousPlayEnd', function() {
							// Load the first video and ensure that videos do not automatically play
							videoPlayer
							.setDefaultPlayOnLoadValue(false)
							.load();
						})
						.playRmm();
					}
					else {
						// Load the first video and ensure that videos do not automatically play
						videoPlayer
						.setDefaultPlayOnLoadValue(false)
						.load();
					}
				}
			},

			/**
			 * Embeds the initial widget's containing element onto the page along with its video player
			 */
			embed: function() {
				this.initializeVideoPlayer();
				
				return $.Deferred($.proxy(function(deferred) {
					// Initialize the view's root container template
					this.get('view').render(this.getTemplate('default'), {
						'appUrl': Ndn_Widget.getAppUrl(),
						'playerContainerElementId': this.getPlayerContainerElementId(),
						'width': this.get('config')['width'],
						'height': this.get('config')['height']
					});
					
	                this.trigger('containerInitialized');
					
					this.get('videoPlayer').embed(this.getPlayerContainerElementId())
					.done($.proxy(function() {
					    // Indicate that this product's video player has been embedded
					    this.trigger('videoPlayerEmbedded');
					    
						deferred.resolve();
					}, this));
				}, this)).promise();
			},
			
			destruct: function() {
				this.get('videoPlayer').destroy();
				Ndn_Widget.prototype.destruct.apply(this, arguments);
			},

			hideSelectableVideos: function() {
				this.getContainerElement().find('.ndn_selectableVideosContainer').hide();
			},
			
			toggleSelectableVideos: function() {
				var selectableVideosContainer = this.getContainerElement().find('.ndn_selectableVideosContainer');
				if (selectableVideosContainer.is(':visible')) {
					selectableVideosContainer.slideUp();
				}
				else {
					selectableVideosContainer.slideDown();
				}
			}
		});
	}
);
