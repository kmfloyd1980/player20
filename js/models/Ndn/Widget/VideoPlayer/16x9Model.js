define([
 	'jquery',
 	'mustache',
 	'models/Ndn/PlayerServices',
 	'models/Ndn/PlayerServices/Cache',
 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
 	'views/Ndn/Widget/VideoPlayer/16x9View',

 	'text!templates/ndn/widget/video-player/16x9/default.html',
 	'text!templates/ndn/widget/video-player/16x9/playlistTabs.html',
 	'text!templates/ndn/widget/video-player/16x9/playlistThumbnailsPage.html'
], function(
	$,
	Mustache,
	Ndn_PlayerServices,
	Ndn_PlayerServices_Cache,
	Ndn_Widget_VideoPlayer_AbstractModel,
	Ndn_Widget_VideoPlayer_16x9View,
	
	defaultTemplate,
	playlistTabsTemplate,
	playlistThumbnailsPageTemplate
) {
		return Ndn_Widget_VideoPlayer_AbstractModel.extend({
			defaults: {
				'widgetView': Ndn_Widget_VideoPlayer_16x9View,
				
				/**
				 * An associative array mapping template aliases to their respective template content
				 * @var {Array<String,String>}
				 */
				'templates': {
					'default': defaultTemplate,
					'playlistTabs': playlistTabsTemplate,
					'playlistThumbnailsPage': playlistThumbnailsPageTemplate
				},
				
				'playlists': [],
				
				/**
				 * The database ID of the playlist currently being displayed
				 * @var {int}
				 */
				'selectedPlaylistId': null,
				
				/**
				 * Structured data concerning what page each of the playlist tabs are currently displaying. Format:
				 * <pre>{
				 * 	Number: Number, // The playlist ID => the video index of the first video of the currently selected page being displayed for the specified playlist ID
				 * 	... // Any other playlists that are selectable and contain pages of thumbnails
				 * }</pre>
				 * @var {Object}
				 */
				'selectedPageData': 0,
				
				/**
				 * Structured data concerning cached HTML representing thumbnail pages. Format:
				 * <pre>{
				 * 	Number: { // The playlist ID => Structured data concerning HTML representing thumbnail pages for the specified playlist
				 * 		Number: { // The thumbnails per page => structured data concerning the associated HTML
				 * 			'thumbnailCount': Number, // The number of thumbnails that were used when generating the associated HTML
				 * 			'html': String, // The associated HTML
				 * 		},
				 * 		... // Any other thumbnails per page paired with its structured data concerning the associated HTML
				 * 	},
				 * 	... // Any other cached HTML associated with a playlist
				 * }</pre>
				 * @var {Object}
				 */
				'cachedThumbpagePagesHtml': null,
				
				/**
				 * Whether this product is currently displaying a companion ad
				 * @var {Boolean}
				 */
				'isShowingCompanionAd': false,
				
				/**
				 * Whether this product is currently displaying an ad study
				 * @var {Boolean}
				 */
				'isShowingAdStudy': false,

				/**
				 * Whether this product utilizes ad studies, which override the placements of companion ads
				 * @var {Boolean}
				 */
				'usesAdStudies': true,
				
				/**
				 * The maximum number of playlist items that may be associated with this widget
				 * @var {int}
				 */
				'playlistItemsLimit': 36
			},
			
			initialize: function() {
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
				
				// Initialize the structured data needed for storing the appropriate page number for each of the selectable playlist tabs
				this.set('selectedPageData', {});
				
				this.on({
					'change:playlists': $.proxy(function() {
						this.trigger('playlistsUpdate');
					}, this)
				});
				
				// When the player data is received from player services, update this widget with the appropriate playlists
				this.getSettings().done($.proxy(function(response) {
					// Store the playlists
					this.set('playlists', response['associatedPlaylists']);
					
					// Indicate that the playlists have been updated
					this.trigger('playlistsUpdate');
					
					// Select the first playlist that was received to begin with
					this.selectPlaylist(this.get('playlists')[0].id);
				}, this));
			},
			
			getThumbnailPageNumber: function(videoIndex) {
				var thumbsPerPage = this.getThumbnailsPerPage();
				for (var videoIndexIterator = thumbsPerPage - 1, pageNumber = 1; videoIndexIterator <= videoIndex; videoIndexIterator += thumbsPerPage) {
					pageNumber++;
				}
				
				return pageNumber;
			},
			
			/**
			 * Stores the appropriate video index in order to re-determine what the selected page is for the currently selected playlist tab
			 * @param {int} pageNumber The page number (starts at 1) of the currently selected page
			 */
			storeSelectedPage: function(pageNumber) {
				var pageData = this.get('selectedPageData');
				pageData[this.get('selectedPlaylistId')] = (pageNumber - 1) * this.getThumbnailsPerPage();
			},
			
			/**
			 * @return {int} The page number (starts at 1) of the thumbnail page currently selected for the currently selected playlist tab
			 */
			getCurrentThumbnailPage: function() {
				return this.getThumbnailPageNumber(this.get('selectedPageData')[this.get('selectedPlaylistId')]);
			},
			
			getSelectedPlaylistId: function() {
				return this.get('selectedPlaylistId');
			},
			
			getSelectedPlaylistData: function() {
				// console.log('Ndn_Widget_VideoPlayer_16x9Model.getCurrentPlaylistData();');
				
				return $.Deferred($.proxy(function(deferred) {
					Ndn_PlayerServices.getPlaylistData(this.getSelectedPlaylistId(), this).done(function(playlistData) {
						deferred.resolve(playlistData);
					});
				}, this)).promise();
			},
			
			selectPlaylist: function(playlistId) {
				this.set('selectedPlaylistId', playlistId);
				
				// Indicate that a new playlist has been selected
				this.trigger('playlistSelect');
			},
			
			setCachedThumbnailPagesHtmlData: function(htmlData, playlistId, thumbsPerPage) {
				if (!this.get('cachedThumbnailPagesHtmlData')) {
					this.set('cachedThumbnailPagesHtmlData', {});
				}
				
				var cache = this.get('cachedThumbnailPagesHtmlData');
				
				cache[playlistId] = cache[playlistId] || {};
				
				cache[playlistId][thumbsPerPage] = htmlData;
			},
			
			getCachedThumbnailPagesHtmlData: function(playlistId, thumbsPerPage) {
				if (!this.get('cachedThumbnailPagesHtmlData')) {
					this.set('cachedThumbnailPagesHtmlData', {});
				}
				
				var cache = this.get('cachedThumbnailPagesHtmlData');
				
				cache[playlistId] = cache[playlistId] || {};
				
				return cache[playlistId][thumbsPerPage];
			},
			
			/**
			 * @return {int} The number of thumbnails to display per page of thumbnails
			 */
			getThumbnailsPerPage: function() {
				return this.isShowingCompanionAdOrAdStudy() ? 8 : 12;
			},
			
			/**
			 * Pre-requisite: The playlist data for the specified playlist must have already been fetched from the database (and exist in the cache accessible by the Ndn_PlayerServices_Cache module).
			 * 
			 * @param {Number} playlistId The playlist ID of the playlist to generate HTML for
			 * @param {Number} thumbsPerPage The number of thumbnails to display per page in the generated HTML
			 * @return {String} HTML that displays the specified playlist's thumbnails using the specified number of thumbnails per page
			 */
			getThumbnailPagesHtmlData: function(playlistId, thumbsPerPage) {
				// console.log('Ndn_Widget_VideoPlayer_16x9Model.getThumbnailPagesHtmlData(', arguments, ');');
				
				// If the needed thumbnail page HTML has already been generated and cache, return the cached HTML
				var cachedHtmlData = this.getCachedThumbnailPagesHtmlData(playlistId, thumbsPerPage);
				if (cachedHtmlData) {
					return cachedHtmlData;
				}
				
				// Get the playlist data used to generate the requested HTML, if no playlist ID is specified (possibly null due to video data not tied to a playlist), 
				// then just use the video items already loaded into the video player
				var playlistData = playlistId
					? Ndn_PlayerServices_Cache.getPlaylistData(playlistId, this.get('playlistItemsLimit'))
					: this.get('videoPlayer').getActivePlaylistData();
				
				var playlistItems = playlistData ? playlistData.items : [],
					thumbnailPagesData = [],
					pagePlaylistItems,
					getVideoIndex = (function() {
						var videoIndex = 0;
						return function() {
							return videoIndex++;
						};
					})();
				
				// Assign the appropriate playlist items to the appropriate pages
				for (var playlistItemIndex = 0, pageNumber = 1; playlistItemIndex < playlistItems.length; playlistItemIndex += thumbsPerPage) {
					// Determine the number of thumbnails that should be displayed on the current page
					var thumbsPerThisPage = playlistItemIndex + thumbsPerPage <= playlistItems.length
						? thumbsPerPage
						: Math.min(playlistItems.length % thumbsPerPage, thumbsPerPage);
					
					thumbnailPagesData.push({
						pageNumber: pageNumber++,
						playlistItems: playlistItems.slice(playlistItemIndex, playlistItemIndex + thumbsPerThisPage),
						videoIndex: getVideoIndex
					});
				}
				
				var result = {
					thumbnailCount: playlistItems.length,
					html: Mustache.to_html(playlistThumbnailsPageTemplate, {
						thumbnailPagesData: thumbnailPagesData
					})
				};
				
				this.setCachedThumbnailPagesHtmlData(result, playlistId, thumbsPerPage);
				
				return result;
			},
			
			/**
			 * @return {Boolean} Whether this product is currently showing a companion ad or an ad study
			 */
			isShowingCompanionAdOrAdStudy: function() {
				return this.get('isShowingCompanionAd') || this.get('isShowingAdStudy');
			},
			
			initializeVideoPlayer: function() {
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
				
				var videoPlayer = this.get('videoPlayer'),
					parentWidget = this.getParentWidget();
				
				videoPlayer.on('embed', function() {
					videoPlayer
					.setDefaultPlayOnLoadValue(parentWidget.get('config')['autoPlay'])
					.load();
				});
			}
		});
	}
);