define([
 	'jquery',
 	'models/Ndn/Widget',
 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
 	'views/Ndn/Widget/VideoPlayer/StudioView',
 	
 	'text!templates/ndn/widget/video-player/studio/default.html'
], function(
	$,
	Ndn_Widget,
	Ndn_Widget_VideoPlayer_AbstractModel,
	Ndn_Widget_VideoPlayer_StudioView,
	
	defaultTemplate
) {
	return Ndn_Widget_VideoPlayer_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoPlayer_StudioView,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate
			},

			/**
			 * Whether this product utilizes ad studies, which override the placements of companion ads
			 * @var {Boolean}
			 */
			'usesAdStudies': true,
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 40,
			
			/**
			 * Whether a companion ad is currently being displayed
			 * @var {Boolean}
			 */
			'isShowingCompanionAd': false,
			
			/**
			 * Top trending videos data
			 * @var {Array<Object>}
			 */
			'topTrendingVideosData': null
		},
		
		initialize: function(config) {
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
		},
		
		initializeVideoPlayer: function() {
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
			
			var videoPlayer = this.get('videoPlayer'),
				parentWidget = this.getParentWidget();
			
			videoPlayer.on('embed', function() {
				videoPlayer
				.setDefaultPlayOnLoadValue(parentWidget.get('config')['autoPlay'])
				.load();
			});
		}
	});
});