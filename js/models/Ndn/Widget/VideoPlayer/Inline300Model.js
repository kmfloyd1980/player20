define([
	 	'jquery',
	 	'models/Ndn/Timer',
	 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
	 	'views/Ndn/Widget/VideoPlayer/Inline300View',
	 	'models/Ndn/Utils/UserAgent',
	 	'models/Ndn/Debugger',
	 	
	 	'text!templates/ndn/widget/video-player/inline300/default.html',
	 	'text!templates/ndn/widget/video-player/inline300/selectableVideos.html'
 	], function(
 		$,
 		Ndn_Timer,
 		Ndn_Widget_VideoPlayer_AbstractModel,
 		Ndn_Widget_VideoPlayer_Inline300View,
 		Ndn_Utils_UserAgent,
 		Ndn_Debugger,
 		
 		defaultTemplate,
 		selectedVideosTemplate
 	) {
		return Ndn_Widget_VideoPlayer_AbstractModel.extend({
			defaults: {
				/**
				 * The view associated with this model
				 * @var {Ndn_AbstractView}
				 */
				'widgetView': Ndn_Widget_VideoPlayer_Inline300View,

				/**
				 * An associative array mapping template aliases to their respective template content
				 * @var {Array<String,String>}
				 */
				'templates': {
					'default': defaultTemplate,
					'selectableVideos': selectedVideosTemplate
				},
				
				/**
				 * Whether the widget is currently being interacted with (the user's mouse is over the widget's selectable videos window)
				 * @var {Boolean}
				 */
				'isInteractingWithUser': false,
				
				/**
				 * An integer reference to the interval used to track the countdown timer until the widget re-engages its scroll synchronization that scrolls to the
				 * currently active video within the scroll window of selectable videos. If no countdown is active, this value is set to false.
				 * @var {int}
				 */
				'queueScrollSyncCountdown': false,
				
				/**
				 * The timer that causes this widget model to trigger the "autoAdvance" event every 4 seconds when "auto-advance" mode is enabled
				 * @var {Ndn_Timer}
				 */
				'autoAdvanceTimer': null,
				
				/**
				 * The maximum number of playlist items that may be associated with this widget
				 * @var {int}
				 */
				'playlistItemsLimit': 12
			},
			
			initialize: function() {
				this.set({
					'autoAdvanceTimer': new Ndn_Timer($.proxy(function() {
						this.trigger('autoAdvance');
					}, this), 7000, 0)
				});
				
				this.on('autoAdvance', $.proxy(function() {
					// Get the next video to load for the "auto-advance" feature; if the end of the playlist is reached, load the first video of the playlist
					var videosPerPage = 3;
					var videoIndexToLoad = Math.floor(this.get('videoPlayer').get('currentVideoIndex') / videosPerPage) * videosPerPage + videosPerPage;
					
					if (!this.get('videoPlayer').getVideoData(videoIndexToLoad)) {
						videoIndexToLoad = 0;
					}
					
					// console.log('[[>>>]] videoIndexToLoad ==', videoIndexToLoad);
					
					this.get('videoPlayer').resetContinuousPlayCounter();
					this.get('videoPlayer').load(videoIndexToLoad);
				}, this));
				
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
			},
			
			initializeVideoPlayer: function() {
				Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
				
				var videoPlayer = this.get('videoPlayer');
				
				videoPlayer.toggleInfoPanel(false);
				
				// Once it is detected that the video player has begun playing a video, disable the "auto-advance" feature
				videoPlayer.once('videoTimeUpdate', $.proxy(function() {
					this.disableAutoAdvance();
				}, this));
				
				videoPlayer.on('embed', $.proxy(function() {
					if (this.isAbleToPlayRmm()) {
						// Once the RMM has finished, enable the "auto-advance" feature
						videoPlayer.once('rmmContinuousPlayEnd', $.proxy(function() {
							if (Ndn_Debugger.inMode('rmmCp')) console.log('rmmContinuousPlayEnd heard, about to enableAutoAdvance...');
							
							// Enable the "auto-advance" feature
							this.enableAutoAdvance();
						}, this));

						this.initializeVideoPlayerForRmm();
					}
					else {
						this.enableAutoAdvance();
					}
				}, this));
			},
			
			/**
			 * @return {Boolean}
			 */
			isAutoAdvanceEnabled: function() {	
				return this.get('autoAdvanceTimer').isActive();
			},
			
			enableAutoAdvance: function() {
				this.get('videoPlayer').setDefaultPlayOnLoadValue(false);
				// this.get('autoAdvanceTimer').execute();
				this.get('autoAdvanceTimer').reset();
			},
			
			disableAutoAdvance: function() {
				// console.log('Ndn_Widget_Inline300Model.disableAutoAdvance();');
				
				this.get('videoPlayer').setDefaultPlayOnLoadValue(this.get('config')['autoPlay']);
				this.get('autoAdvanceTimer').stop();
			}
		});
	}
);