define([
 	'jquery',
 	'models/Ndn/Widget',
 	'models/Ndn/Widget/VideoPlayer/AbstractModel',
 	'views/Ndn/Widget/VideoPlayer/Inline590View',
 	
 	'text!templates/ndn/widget/video-player/inline590/default.html',
 	'text!templates/ndn/widget/video-player/inline590/selectableVideos.html'
], function(
	$,
	Ndn_Widget, 
	Ndn_Widget_VideoPlayer_AbstractModel,
	Ndn_Widget_VideoPlayer_Inline590View,
	
	defaultTemplate,
	selectedVideosTemplate
) {
	return Ndn_Widget_VideoPlayer_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoPlayer_Inline590View,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate,
				'selectableVideos': selectedVideosTemplate
			},
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 40
		},

		initialize: function(config) {
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initialize.apply(this, arguments);
		},
		
		initializeVideoPlayer: function() {
			Ndn_Widget_VideoPlayer_AbstractModel.prototype.initializeVideoPlayer.apply(this, arguments);
			
			this.get('videoPlayer').on('embed', $.proxy(function() {
				this.initializeVideoPlayerForRmm();
			}, this));
		}
	});
});
