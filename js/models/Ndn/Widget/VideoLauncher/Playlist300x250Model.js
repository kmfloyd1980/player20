define([
 	'jquery',
 	'underscore',
 	'models/Ndn/Widget',
 	'models/Ndn/Widget/VideoLauncher/AbstractModel',
 	'models/Ndn/VideoPlayer',
 	'views/Ndn/Widget/VideoLauncher/Playlist300x250View',

 	'text!templates/ndn/widget/video-launcher/playlist300x250/default.html',
 	'text!templates/ndn/widget/video-launcher/playlist300x250/selectableVideos.html',
 	'text!templates/ndn/widget/video-launcher/playlist300x250/playlistSelect.html'
], function(
	$,
	_,
	Ndn_Widget,
	Ndn_Widget_VideoLauncher_AbstractModel,
	Ndn_VideoPlayer,
	Ndn_Widget_VideoLauncher_Playlist300x250View,
	
	defaultTemplate,
	selectableVideosTemplate,
	playlistSelectTemplate
) {
	return Ndn_Widget_VideoLauncher_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoLauncher_Playlist300x250View,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate,
				'selectableVideos': selectableVideosTemplate,
				'playlistSelect': playlistSelectTemplate
			},
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 15,

			/**
			 * Whether the widget is currently being interacted with (the user's mouse is over the widget's selectable videos window)
			 * @var {Boolean}
			 */
			'userHasInteracted': false,
            
            /**
             * Whether the aspect ratio of the top container is enabled
             * @var {Boolean}
             */
            'aspectRatioEnabled': true,

			/**
			 * Whether the widget is currently displaying the slider (The final state that the player can arrive at)
			 * @var {Boolean}
			 */
			'displayingSlider': false,

			/**
			 * The length of time a user must mouseover the PTR slider before sound turns on (in milliseconds);
			 * @var {int}
			 */
			'mouseoverTime': 2000
		},

		initialize: function(config) {
			Ndn_Widget_VideoLauncher_AbstractModel.prototype.initialize.apply(this, arguments);
		},

		mutePlayer: function() {
			this.get('videoPlayer').setMuted(true);
		},

		unmutePlayer: function() {
			this.get('videoPlayer').setMuted(false);
		},
        
        isAspectRatioOfTopContainerEnabled: function() {
            return this.get('aspectRatioEnabled');
        },
        
        toggleAspectRatioOfTopContainer: function(toggle) {
            this.set('aspectRatioEnabled', toggle);
        },

		/**
		 * @return {String} The "id" attribute of the container dom element that is responsible for holding this widget's player
		 */
		getPlayerContainerElementId: function() {
			return this.get('widgetUid') + '-player';
		},

		getPlaylistsList: function() {
			return _.map(this.get('videoPlayer').get('playerData').associatedPlaylists, function(playlist) {
				if (this.get('videoPlayer').get('playlistId') == playlist.id) {
					playlist.currentPlaylist = true;
				}

				return _.pick(playlist, 'id', 'label', 'currentPlaylist');
			}.bind(this));
		},

		getVideos: function() {
			var config = this.get('config');
			var videos = _.first(this.get('videoPlayer').getActivePlaylistData()['items'], 12);

			videos = this.getVideoDataForLaunchers(videos);

			return this.chunkPlaylist(videos, 4, 12);
		},

		chunkPlaylist: function(playlist, chunkSize, totalSize) {
			playlist = _.first(playlist, totalSize);
			var chunkedPlaylist = new Array((totalSize/chunkSize));

			// Initialize the buckets.
			for (var i = 0; i < chunkedPlaylist.length; i++) {
				chunkedPlaylist[i] = new Array();
			};

			_.each(playlist, function(item, index) {
				chunkedPlaylist[Math.floor(index/chunkSize)].push(item);
			});

			return _.filter(chunkedPlaylist, function(item) {
				return item.length;
			});
		},
		
		embed: function() {
			this.initializeVideoPlayer();
			
			return $.Deferred($.proxy(function(deferred) {
				// Initialize the view's root container template
				this.get('view').render(this.getTemplate('default'), {
					'playerContainerElementId': this.getPlayerContainerElementId()
				});
				
				this.trigger('containerInitialized');
				
				this.get('videoPlayer').embed(this.getPlayerContainerElementId());
			}, this)).promise();
		}
	});
});