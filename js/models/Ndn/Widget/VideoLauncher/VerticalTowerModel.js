define([
 	'jquery',
 	'underscore',
	'models/Ndn/Widget',
 	'models/Ndn/Utils/UrlParser',
 	'models/Ndn/Widget/VideoLauncher/AbstractModel',
 	'views/Ndn/Widget/VideoLauncher/VerticalTowerView',
 	
 	'text!templates/ndn/widget/video-launcher/vertical/default.html'
], function(
	$, 
	_,
	Ndn_Widget,
	Ndn_Utils_UrlParser,
	Ndn_Widget_VideoLauncher_AbstractModel,
	Ndn_Widget_VideoLauncher_VerticalTowerView,
	
	defaultTemplate
) {
	return Ndn_Widget_VideoLauncher_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoLauncher_VerticalTowerView,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate
			}, 
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 15
		},

		initialize: function(config) {
			Ndn_Widget_VideoLauncher_AbstractModel.prototype.initialize.apply(this, arguments);
		},

		embed: function() {
			return $.Deferred($.proxy(function(deferred) {
				this.getSettings()
				.done($.proxy(function(response) {
					this.set({
						'playlistData': response.playlists[_.first(_.keys(response.playlists))],
						'playerData': response
					});

					var launcherPlaylist = this.getVideoDataForLaunchers(this.get('playlistData').items);

					var launcherSize = 'ndn_horizontalLarge';
					if (this.get('config')['width'] && this.get('config')['width'] <= 425) {
						launcherSize = 'ndn_horizontalSmall';
					}

					// Render this widget's view
					this.get('view').render(this.getTemplate('default'), {
						'playlistItems': launcherPlaylist,
						'playerContainerElementId': this.getPlayerContainerElementId(),
						'launcherSize': launcherSize,
						'appUrl': Ndn_Widget.getAppUrl()
					});
				}, this));
			}, this)).promise();
		}
	});
});
