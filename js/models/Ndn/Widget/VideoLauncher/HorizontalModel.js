define([
 	'jquery',
 	'underscore',
 	'models/Ndn/Widget',
	'models/Ndn/Widget/VideoLauncher/AbstractModel',
 	'models/Ndn/PlayerServices',
 	'models/Ndn/VideoPlayer',
 	'models/Ndn/Utils/UrlParser',
 	'views/Ndn/Widget/VideoLauncher/HorizontalView',
 	
 	'text!templates/ndn/widget/video-launcher/horizontal/default.html'
], function(
	$,
	_,
	Ndn_Widget,
	Ndn_Widget_VideoLauncher_AbstractModel,
	Ndn_PlayerServices,
	Ndn_VideoPlayer,
	Ndn_Utils_UrlParser,
	Ndn_Widget_VideoLauncher_HorizontalView,
	
	defaultTemplate
) {
	var _fallbackPlaylistId = '14126';
	
	return Ndn_Widget_VideoLauncher_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoLauncher_HorizontalView,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate
			}, 
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 12,
			
			/**
			 * This product's thumbnail carousel
			 * @var {Object}
			 */
			'carousel': null,
			
			/**
			 * The maximum number of slides to use for this product's thumbnail carousel
			 * @var {int}
			 */
			'carouselMaxSlides': null,
			
			/**
			 * TODO: Document what this is used for
			 * @var {int}
			 */
			'carouselNumberOfSlides': null
		},

		initialize: function(config) {
			Ndn_Widget_VideoLauncher_AbstractModel.prototype.initialize.apply(this, arguments);
		},
		
		embed: function() {
			var config = this.get('config'),
				playlistItemsLimit = this.get('playlistItemsLimit');
			
			return $.Deferred($.proxy(function(deferred) {
				this.getSettings()
				.done($.proxy(function(response) {
					this.set({
						'playlistData': response.playlists[_.first(_.keys(response.playlists))],
						'playerData': response
					});
		
					var launcherPlaylist = _.first(this.get('playlistData').items, 12);

					// If the current playlist doesn't contain 12 videos, grab videos from default playlist until you've got 12.
					if (launcherPlaylist.length < playlistItemsLimit) {
						Ndn_PlayerServices.getPlaylistData(this, _fallbackPlaylistId)
						.done(function(response) {
							additionalPlaylist = _.first(response.playlists[_.first(_.keys(response.playlists))].items, 12 - launcherPlaylist.length);

							launcherPlaylist = launcherPlaylist.concat(additionalPlaylist);

							this.renderPlaylist(launcherPlaylist, config);
						}.bind(this));
					} else {
						this.renderPlaylist(launcherPlaylist, config);
					}
				}, this));
			}, this)).promise();
		},

		renderPlaylist: function(launcherPlaylist, config) {
			// Indicate how many slides this launcher is supposed to have using a class name that will be applied to this widget's container element
			var containerElement = this.getContainerElement(),
				launcherSize = containerElement.width() <= 425
					? 'ndn_horizontalSmall'
					: 'ndn_horizontalLarge';
			
			// This logic is left in for backwards-compatibility where the "width" is still being provided manually in this widget's config
			if (config['width']) {
				launcherSize = config['width'] <= 425
					? 'ndn_horizontalSmall'
					: 'ndn_horizontalLarge';
			}

			launcherPlaylist = this.getVideoDataForLaunchers(launcherPlaylist);

			// Render this widget's view
			this.get('view').render(this.getTemplate('default'), {
				'playlistItems': launcherPlaylist,
				'playerContainerElementId': this.getPlayerContainerElementId(),
				'launcherSize': launcherSize,
				'appUrl': Ndn_Widget.getAppUrl()
			});
		},

		/**
		 * @return {String} The "id" attribute of the container dom element that is responsible for holding this widget's player
		 */
		getPlayerContainerElementId: function() {
			return this.get('widgetUid') + '-player';
		}
	});
});
