define([
 	'jquery',
 	'underscore',
 	'models/Ndn/App',
 	'models/Ndn/Widget',
 	'models/Ndn/Widget/VideoLauncher/AbstractModel',
 	'models/Ndn/PlayerServices',
 	'models/Ndn/VideoPlayer',
 	'models/Ndn/Utils/UserAgent',
 	'views/Ndn/Widget/VideoLauncher/SliderView',
 	
 	'text!templates/ndn/widget/video-launcher/slider/default.html',
 	'text!templates/ndn/widget/video-launcher/slider/selectableVideos.html',
 	'text!templates/ndn/widget/video-launcher/slider/thumbnails.html',
 	
 	'jquery_responsive_containers'
], function(
	$,
	_,
	Ndn_App,
	Ndn_Widget, 
	Ndn_Widget_VideoLauncher_AbstractModel, 
	Ndn_PlayerServices, 
	Ndn_VideoPlayer,
	Ndn_Utils_UserAgent,
	Ndn_Widget_VideoLauncher_SliderView,
	
	defaultTemplate,
	selectableVideosTemplate,
	thumbnailsTemplate
) {
	return Ndn_Widget_VideoLauncher_AbstractModel.extend({
		defaults: {
			/**
			 * The view associated with this model
			 * @var {Ndn_AbstractView}
			 */
			'widgetView': Ndn_Widget_VideoLauncher_SliderView,

			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate,
				'selectableVideos': selectableVideosTemplate,
				'thumbnails': thumbnailsTemplate
			}, 
			
			/**
			 * The maximum number of playlist items that may be associated with this widget
			 * @var {int}
			 */
			'playlistItemsLimit': 15,
			
			/**
			 * Whether the period of time where this product was using a video player has concluded
			 * @var {Boolean}
			 */
			'hasConcludedVideoPlayer': false,

			/**
			 * Whether the widget is currently being interacted with (the user's mouse is over the widget's selectable videos window)
			 * @var {Boolean}
			 */
			'userHasInteracted': false,
			
			/**
			 * The width (measured in pixels) that had been added to the width of this slider's video player (used when ensuring the optimal video player width)
			 * @var {int}
			 */
			'addedVideoPlayerWidth': 0,
			
			/**
			 * The optimal width of the video player to use while it displays ads
			 * @var {int}
			 */
			'optimalWidth': 315,
			
			/**
			 * Whether the aspect ratio of the top container is enabled
			 * @var {Boolean}
			 */
			'aspectRatioEnabled': true,
			
			/**
			 * The showcase thumbnail carousel, which displays the large clickable thumbnail/image (the "slideshow carousel")
			 * @var {jQuery.bxSlider}
			 */
			'carousel': null,
			
			/**
			 * The carousel that displays a set of thumbnails at a time
			 * @var {jQuery.bxSlider}
			 */
			'thumbnailCarousel': null
		},

		initialize: function(config) {
			Ndn_Widget_VideoLauncher_AbstractModel.prototype.initialize.apply(this, arguments);
		},

		embed: function() {
			this.initializeVideoPlayer();
			
			return $.Deferred($.proxy(function(deferred) {
				// Initialize the view's root container template
				this.get('view').render(this.getTemplate('default'), {
					'playerContainerElementId': this.getPlayerContainerElementId(),
					'width': this.get('config')['width'],
					'height': this.get('config')['height'],
					'siteSection': this.get('config')['siteSection']
				});
				
				this.trigger('containerInitialized');
				
				this.get('videoPlayer').embed(this.getPlayerContainerElementId(), this.get('config'));
				
				deferred.resolve();
			}, this)).promise();
		},

		getVideos: function() {
            return $.Deferred($.proxy(function(deferred) {
                this.getParentWidget().getSettings().done($.proxy(function(settings) {
                    var config = this.get('config');
                    var playlist = settings.playlists[_.first(_.keys(settings.playlists))] || [];
                    var videos = _.first(playlist.items, 6);

                    videos = this.getVideoDataForLaunchers(videos);

                    deferred.resolve(videos);
                }, this));
            }, this)).promise();
		},
		
		unmutePlayer: function() {
			this.get('videoPlayer').setMuted(false);
		},
		
		isAspectRatioOfTopContainerEnabled: function() {
			return this.get('aspectRatioEnabled');
		},
		
		toggleAspectRatioOfTopContainer: function(toggle) {
			this.set('aspectRatioEnabled', toggle);
		},
		
		/**
		 * @return {String} The "id" attribute of the container dom element that is responsible for holding this widget's player
		 */
		getPlayerContainerElementId: function() {
			return this.get('widgetUid') + '-player';
		},

		getCurrentPlaylistLength: function() {
			return this.get('videoPlayer').get('playlistData').items.length;
		},

		/**
		 * Whether thumbnails are displayed; Checks if the parent container is being set to 300x250. If so then do not show the thumbnails. 
		 * We specifically tell partners to wrap the embed div in a parent div with style="width:300px;height:250px;" to hide the thumbnials. 		
		 * @return {Boolean}
		 */
		getThumbnailsEnabled: function() {
			var parentContainer = this.getContainerElement().parent(),
				parentContainerHeight = parentContainer.height(),
				parentContainerWidth = parentContainer.width(),
				productDisplayRatio = 5/6,
				moreVideosLabelHeight = 26,
				thumbnailDisplayRatio = 9 /16;
			
			return !(parentContainerWidth == 300 && parentContainerHeight == 250);
			
			// return parentContainerHeight >= ((productDisplayRatio * parentContainerWidth) + moreVideosLabelHeight + ((parentContainerWidth - 8) / 3) * thumbnailDisplayRatio); // use this if the above is not catching all of the possible slider implementations. Keeping it simple for right now for debug and support.
		},

		/**
		 * @return {Boolean} Whether the showcase carousel is enabled
		 */
		isShowcaseCarouselEnabled: function() {
			return !!this.get('carousel');
		}
	});
});
