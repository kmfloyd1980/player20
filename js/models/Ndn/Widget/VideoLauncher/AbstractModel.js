define([
	'jquery',
	'underscore',
	'models/Ndn/Widget',
	'models/Ndn/VideoPlayer',
	'models/Ndn/Utils/UserAgent'
], function(
	$, 
	_, 
	Ndn_Widget,
	Ndn_VideoPlayer,
	Ndn_Utils_UserAgent
) {
	return Ndn_Widget.extend({
		defaults: {
			/**
			 * The video player
			 * @var {?Ndn_VideoPlayer}
			 */
			'videoPlayer': null
		},
		
		initialize: function() {
			Ndn_Widget.prototype.initialize.apply(this, arguments);
		},
		
		/**
		 * Initialize this product's video player
		 */
		initializeVideoPlayer: function() {
			this.set('videoPlayer', new Ndn_VideoPlayer(this.getParentWidget()));

			this.get('videoPlayer').setPlaylistItemsLimit(this.get('playlistItemsLimit'));

			if (this.isAbleToPlayRmm()) {
				this.get('videoPlayer')
				.on('rmmContinuousPlayEnd', $.proxy(function() {
					this.get('view').onTeaserFinished();
				}, this));
			} else {
				this.get('videoPlayer')
				.on('embed', $.proxy(function() {
					this.get('view').$el.find('.ndn_videoPlayerWrapper').hide();
				}, this))
				.on('videoPlaylistLoad', $.proxy(function() {
					this.get('view').onTeaserFinished(false);
				}, this));
			}
			
			this.get('videoPlayer').on({
				'embed': $.proxy(function() {
					this.get('videoPlayer').loadPlaylist(this.getParentWidget().get('config')['playlistId'])
					.done($.proxy(function() {
						this.getParentWidget().onceConfigIsComplete($.proxy(function() {
							if (this.isAbleToPlayRmm()) {
								this.get('videoPlayer').playRmm();
							}
							else {
								// Load the first video and ensure that videos do not automatically play
								this.get('videoPlayer')
								.setDefaultPlayOnLoadValue(false)
								.load();
							}
						}, this));
					}, this));
				}, this),
				
				// When this launcher's video player's RMM 6-second bumper video is clicked, the RMM bumper's full video is launched in a new window
				'click': $.proxy(function() {
					if (this.get('videoPlayer').isShowingRmm() && this.get('videoPlayer').isShowingVideo()) {
						// Launch the RMM bumper's full video in a new window
						this.get('view').openNdnWindow(this.getLandingPageUrl({
							'videoId': _.first(this.get('videoPlayer').getActivePlaylistData()['items'])['videoId']
						}));
						
						// Continue playing the rest of the RMM bumper
						this.get('videoPlayer').play();
						
						// Conclude the teaser video
						// this.onTeaserFinished();
					}
				}, this)
			});

            this.trigger('videoPlayerInitialized');
		},

		/**
		 * Embeds the initial widget's containing element onto the page along with its video player
		 * TODO: This never has any deferred.resolve(). Add it on the this.get('videoPlayer').embed().done() call?
		 * 
		 * TODO: Should this be refactored out for one of the video launchers?
		 */
		embed: function() {
			return $.Deferred($.proxy(function(deferred) {
				// Initialize the view's root container template
				this.get('view').render(this.getTemplate('default'), {
					'playerContainerElementId': this.getPlayerContainerElementId()
				});
				
				this.get('videoPlayer').embed(this.getPlayerContainerElementId());
			}, this)).promise();
		}
	});
});
