define([
    'jquery',
    'backbone',
    'models/Ndn/Utils/BrowserStorage',
    'models/Ndn/Utils/UserAgent'
], function(
    $,
    Backbone,
    Ndn_Utils_BrowserStorage,
    Ndn_Utils_UserAgent
) {
    /**
     * List of registered components
     * @var {Array<Ndn_Widget_PlayerOptions>}
     */
    var _registeredComponents = [];
    
    var Ndn_Widget_PlayerOptions = Backbone.Model.extend({
        defaults: {
            /**
             * The product associated with these options
             * @var {Ndn_Widget}
             */
            'product': null,
            
            /**
             * 
             * @var {jQueryElement}
             */
            'container': null,
            
            /**
             * Whether autoplay is enabled for all players
             * @var {Boolean}
             */
            'autoplay': null
        },
        
        initialize: function(product) {
            this.set({
                product: product
            });
            
            this.initializeDom();
            this.synchronizeInterface();
            this.initializeEventListeners();
            
            Ndn_Widget_PlayerOptions.registerComponent(this);
            
            // Once "widgetEmbedEnd" is encountered for the associated product, append the player options container element to just underneath the product's container element
            product.on('widgetEmbedEnd', $.proxy(function() {
                this.get('container').insertAfter(product.getContainerElement());
            }, this));
        },
        
        initializeDom: function() {
            var productContainer = this.get('product').getContainerElement();
            
            var container = 
            $('<div class="ndn_playerOptions"><b>Autoplay:</b> <a href="#" data-autoplay="on">On</a> | <a href="#" data-autoplay="off">Off</a></div>')
            .css('width', productContainer.width());
    
            this.set('container', container);
        },
        
        initializeEventListeners: function() {
            this.get('container')
            .on('click', 'a', function(event) {
                if (!$(this).hasClass('ndn_selected')) {
                    Ndn_Widget_PlayerOptions.setIsPlayOnInViewAlwaysOnForSinglePlayers($(this).data('autoplay') == 'on')
                }
                
                // Synchronize the interface of all registered instances of the Ndn_Widget_PlayerOptions component
                Ndn_Widget_PlayerOptions.synchronizeAllInterfaces();
                
                event.preventDefault();
                return false;
            });
        },
        
        synchronizeInterface: function() {
            var container = this.get('container'),
                isAutoplayOn = Ndn_Widget_PlayerOptions.getPlayOnInViewToggleForSinglePlayers();
            
            // Only allow the option that is not currently set to be selectable
            container.find('a').each(function() {
                $(this).toggleClass('ndn_selected', ($(this).data('autoplay') == 'on' && isAutoplayOn) || ($(this).data('autoplay') != 'on' && !isAutoplayOn));
            });
        }
    },
    {
        /**
         * @param {Ndn_Widget_PlayerOption} A used instance to register statically so that whenever one component's interface is synchronized, all the other ones are too
         */
        registerComponent: function(component) {
            _registeredComponents.push(component);
        },
        
        /**
         * Synchronize the interface of all registered components so that none of them display conflicting information on the page
         */
        synchronizeAllInterfaces: function() {
            $.each(_registeredComponents, function() {
                this.synchronizeInterface();
            });
        },
        
        /**
         * @return {Boolean} The value of the "autoPlay" config setting for all "VideoPlayer/Single" products (for when respected, see Ndn_Widget_Config.getBrowserSettings())
         */
        getPlayOnInViewToggleForSinglePlayers: function() {
            return Ndn_Utils_UserAgent.isDesktop() && Ndn_Utils_BrowserStorage.get('playOnInViewForSinglePlayers') != 'false';
        },
        
        /**
         * @param {Boolean} toggle
         */
        setIsPlayOnInViewAlwaysOnForSinglePlayers: function(toggle) {
            Ndn_Utils_BrowserStorage.set('playOnInViewForSinglePlayers', toggle ? 'true' : 'false');
        }
    });
    
    return Ndn_Widget_PlayerOptions;
});