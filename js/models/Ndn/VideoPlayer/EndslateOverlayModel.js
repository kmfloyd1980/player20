define([
 	'jquery',
 	'underscore',
 	'mustache',
 	'models/Ndn/VideoPlayer/OverlayAbstractModel',
 	'models/Ndn/PlayerServices',
 	'views/Ndn/VideoPlayer/EndslateOverlayView'
 ], function(
	$,
	_,
	Mustache,
	Ndn_VideoPlayer_OverlayAbstractModel,
	Ndn_PlayerServices,
	Ndn_VideoPlayer_EndslateOverlayView
) {
	/**
	 * Clears the countdown's interval and resets the countdown
	 * @private
	 */
	var _removeCountdown = function() {
		if (this.get('countdownInterval') !== null) {
			clearInterval(this.get('countdownInterval'));
			this.set({
				countdownInterval: null,
				countdownValue: 5
			});
		}
	};

 	return Ndn_VideoPlayer_OverlayAbstractModel.extend({
 		defaults: {
 			/**
 			 * The widget that this end slate currently belongs to
 			 * @var {Ndn_Widget}
 			 */
 			'widget': null,
 			
 			/**
 			 * The video player that this overlay belongs to
 			 * @var {Ndn_VideoPlayer}
 			 */
 			'videoPlayer': null,
 			
 			/**
 			 * This end slate's type; recognized types: "endOfPlaylist", "isolatedVideoEnd", "videoNotAvailable", "playlistPause", "mediaBlocked"
 			 * @var {String}
 			 */
 			'type': null,
 			
 			/**
 			 * The interval created for the countdown to playlist transition
 			 * @var {int}
 			 */
 			'countdownInterval': null,
 			
 			/**
 			 * The number of seconds remaining in the countdown
 			 * @var {int}
 			 */
 			'countdownValue': 5,
 			
 			/**
 			 * The view associated with this end slate model
 			 * @var {Ndn_AbstractView}
 			 */
 			'view': null
 		},

 		/**
 		 * @param {Ndn_VideoPlayer} videoPlayer The video player associated with this endslate
 		 */
 		initialize: function(videoPlayer) {
 			videoPlayer.onceEmbedded($.proxy(function() {
 	 			this.set({
 	 				widget: videoPlayer.get('widget'),
 	 				videoPlayer: videoPlayer
 	 			});
 	 			
 	 			this.set('view', new Ndn_VideoPlayer_EndslateOverlayView({
 	 				model: this
 	 			}));
 	 			
 	 			this.get('videoPlayer')
 	 			.on('videoUnavailable', $.proxy(function() {
 	 				this.get('view').show('videoNotAvailable');
 	 			}, this))
 	 			.on('playlistEnd', $.proxy(function() {
 	 				this.get('view').show('endOfPlaylist');
 	 			}, this))
 	 			.on('isolatedVideoEnd', $.proxy(function() {
 	 				this.get('view').show('isolatedVideoEnd');
 	 			}, this))
				.on('continuousPlayEnd', $.proxy(function() {
					if (videoPlayer.get('widget').get('config')['continuousPlay'] != 0) {
						this.get('view').show('playlistPause');
					}
				}, this));
 	 			
 	 			// If a video is selected by the user via the current widget, remove the countdown to transition to a new playlist
 	 			this.get('widget').on('videoSelect', $.proxy(function() {
 	 				_removeCountdown.call(this);
 	 			}, this));
 			}, this));
 		},
 		
 		/**
 		 * @param {Object} nextVideoData Structured data concerning the data of the next video to be played
 		 * @return {Object} Structured data needed for rendering this end slate's html
 		 */
 		getTemplateData: function(nextVideoData) {
			var currentVideoData = this.get('videoPlayer').getCurrentVideoData();
		
			var result = {
				upNextTitle: nextVideoData.videoTitle,
				upNextThumbnail: nextVideoData.stillImageUrl,				
				showUnavailableAlert: this.get('type') == 'videoNotAvailable',
				showCountdown: this.displaysCountdown(),
				mediaIsBlocked: this.get('type') == 'mediaBlocked',
				countdownValue: this.get('countdownValue')
			};
			
			// If current video data is available, provide it
			if (currentVideoData && currentVideoData != null) {
				result = _.extend(result, {
					justWatchedThumbnail: currentVideoData.stillImageUrl,
					backgroundImage: currentVideoData.stillImageUrl
				});
			}
 			
 			return result;
 		},
 		
 		/**
 		 * @return {Boolean} Whether this end slate transitions into a new playlist
 		 */
 		transitionsToNewPlaylist: function() {
 			return _.contains(['endOfPlaylist', 'videoNotAvailable', 'isolatedVideoEnd'], this.get('type'));
 		},
 		
 		/**
 		 * @return {Boolean} Whether this end slate displays a countdown timer
 		 */
 		displaysCountdown: function() {
 			// If this end slate is the result of the "videoId" video ending and it does not have continuous play enabled,
 			if (this.get('type') == 'isolatedVideoEnd' && !this.get('widget').get('config')['continuousPlay']) {
 				return false;
 			}
 			
 			return _.contains(['endOfPlaylist', 'videoNotAvailable', 'isolatedVideoEnd'], this.get('type'));
 		},
 		
 		/**
 		 * @return {jQuery.Deferred} A jQuery.Deferred object that gets resolved with structured data concerning the video that is scheduled to play after the 
 		 * completion of the video currently playing
 		 */
 		getNextVideoData: function() {
 			// console.log('Ndn_VideoPlayer_EndslateOverlayView.getNextVideoData();');
 			
 			return $.Deferred($.proxy(function(deferred) {
 				// If the type is videoNotAvailable then load the current video data instead of the next video and set that as the result. 
 				if (this.get('type') === 'videoNotAvailable'){
 					var result = this.get('videoPlayer').getCurrentVideoData(); 	 					
 				}
 				else{
	 				var result = this.get('videoPlayer').getNextVideoData();	
 				}	 			
 	 			
 	 			if (result) {
 	 				return deferred.resolve(result);
 	 			}
 	 			else {
 	 				// Get the video data for the first video of the next playlist
 	 				var nextPlaylistId = this.get('videoPlayer').getNextPlaylistId();
 	 				Ndn_PlayerServices.getPlaylistData(nextPlaylistId, this.get('widget'))
 					.fail(deferred.reject)
 					.done($.proxy(function(playlistData) {
 						deferred.resolve(playlistData.items[0]);
 					}, this));
 	 			}
 			}, this)).promise();
 		},
 		
 		/**
 		 * Begins the countdown to when the video player loads its next queued playlist
 		 */
 		countdownToNextPlaylist: function() {
 			this.set('countdownInterval', setInterval(function() {
 				if (this.get('countdownValue') <= 0) {
 					this.trigger('endslateFinished');
 				}
 				this.decrementCountdownValue();
 			}.bind(this), 1000));

 			this.on('endslateFinished', function () {
 	 			_removeCountdown.call(this);
 				this.playNextVideo();
 			}.bind(this));
 		},
 		
 		/**
 		 * Plays the next queued video
 		 */
 		playNextVideo: function() {
 			if (this.transitionsToNewPlaylist()) {
 				// If the widget config has autoPlay set to false load the player with the next video instead of automatically playing it.
 				if (this.get('type') === 'videoNotAvailable'){ 						 				
		 			this.get('videoPlayer').load(0, this.get('videoPlayer').getNextPlaylistId(), this.get('videoPlayer').get('widget').get('config')['autoPlay']);
				}
				// Else play the next video 
				else{					
					this.get('videoPlayer').play(0, this.get('videoPlayer').getNextPlaylistId());
				} 				
 			} 
 			else if (this.get('type') == 'playlistPause') {
 				this.get('videoPlayer').play(this.get('videoPlayer').get('currentVideoIndex') + 1);
 			} 
 			else {
 				this.get('videoPlayer').play();
 			}
 		},

 		/**
 		 * Decrements the end slate's countdown timer by 1 second
 		 */
 		decrementCountdownValue: function() {
 			this.set('countdownValue', Math.max(0, this.get('countdownValue') - 1));
 		}, 		
 		
 		/**
 		 * TODO: Refactor out the use of this method, this should be restricted to being called within this model
 		 * Removes the end slate's countdown
 		 * @deprecated This method's logic should only be called from within the Ndn_VideoPlayer_EndslateOverlayModel class
 		 */
 		cancelCountdown: function() {
 			_removeCountdown.call(this);
 		}, 		

 		/**
 		 * @return {String} Identifier for this endslate's type
 		 */
 		getEndslateType: function() {
 			return this.get('type');
 		}
 	});
 });