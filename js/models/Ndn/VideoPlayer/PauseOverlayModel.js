define([
	 	'jquery',
	 	'underscore',
	 	'backbone',
	 	'models/Ndn/Widget',
	 	'models/Ndn/VideoPlayer/OverlayAbstractModel',
	 	'views/Ndn/VideoPlayer/PauseOverlayView',
	 	
	 	'jquery_resize'
 	], function(
 		$,
 		_,
 		Backbone,
 		Ndn_Widget,
 		Ndn_VideoPlayer_OverlayAbstractModel,
 		Ndn_VideoPlayer_PauseOverlayView
 	) {
		return Ndn_VideoPlayer_OverlayAbstractModel.extend({
			defaults: {
				/**
				 * The video player associated with this overlay
				 * @var {Ndn_VideoPlayer}
				 */
				'videoPlayer': null,
				
				/**
				 * @var {Ndn_AbstractView}
				 */
				'view': null,
				
				/**
				 * Whether this pause overlay is currently showing
				 * @var {Boolean}
				 */
				'isShowing': false,
				
				/**
				 * Minimum width that determines if the carousel has 5 slides or 3 slides if less than this number
				 * @var {Integer}
				 */
				'fiveSlideWidth': 1022,
				
				/**
				 * Minimum width that determines if the carousel has 5 slides or 3 slides if less than this number
				 * @var {Integer}
				 */
				'fourSlideWidth': 750
			},
			
			/**
			 * @param {Ndn_VideoPlayer} videoPlayer The video player that this overlay is associated with
			 */
			initialize: function(videoPlayer) {
				this.set('videoPlayer', videoPlayer);
				this.set('view', new Ndn_VideoPlayer_PauseOverlayView({
					model: this
				}));
				
				this.get('videoPlayer').onceEmbedded($.proxy(function() {
					// Append this model's overlay view
					this.get('videoPlayer').getContainerElement().append(this.get('view').$el);
					
					this.hide();
					this.get('videoPlayer')
					.on('pause', $.proxy(function() {
						this.show();
					}, this))
					.on('adStart videoStart play', $.proxy(function() {
						this.hide();
					}, this))
					.on('exitingfullscreen', $.proxy(function() {
						if(this.get('videoPlayer').isPlaying()){
							this.hide();	
						}						
					}, this));	
				}, this));
				
				this.get('videoPlayer').toggleInfoPanel(false);				
				},
			
	 		getTemplateData: function() {
	 			// console.log('getTemplateData(); this.get("videoPlayer") ==', this.get("videoPlayer"));
	 			
	 			var widgetConfig = this.get('videoPlayer').get('widget').get('config'),
	 				currentVideoData = this.get('videoPlayer').getCurrentVideoData(),
	 				videoBaseUrl = Ndn_Widget.getAppUrl();	 			

				var result = {
					'videoBaseUrl': videoBaseUrl		
				};
				
				if (currentVideoData) {
					result = _.extend(result, {
						'currentTitle': currentVideoData['videoTitle'],	
						'currentThumb': currentVideoData['stillFrameUrl'],
						'providerName': currentVideoData['provider']['name'],
						'currentDescription': currentVideoData['description'],
						'producerLogo': currentVideoData['provider']['logoUrl'],
						'singleVideoFeed': encodeURIComponent('http://social.newsinc.com/media/json/69017/'+currentVideoData['videoId']+'/singleVideoOG.html?videoId='+currentVideoData['videoId'] + '&type=VideoPlayer/16x9&trackingGroup=69017'),
						'singleTwitterVideoFeed': encodeURIComponent('https://social.newsinc.com/media/json/69017/'+currentVideoData['videoId']+'/singleVideoOG.html?videoId='+currentVideoData['videoId'] + '&type=VideoPlayer/Single&widgetId=2&trackingGroup=69017'),
						'videoLink': videoBaseUrl + "/?type=VideoPlayer/16x9&autoPlay=" + widgetConfig['autoPlay'] + "&continuousPlay=" + widgetConfig['continuousPlay'] + "&widgetId=" + widgetConfig['widgetId'] + "&trackingGroup=69016&playlistId=" + widgetConfig['playlistId'] + "&width=600&height=500&vcid=" + currentVideoData['videoId'],
						'emailLink': "mailto:?subject=" + currentVideoData['videoTitle'] + "&body=Check out this video:%0A%0A" + currentVideoData['videoTitle'] + "%0A" + encodeURIComponent('https://social.newsinc.com/media/json/69017/'+currentVideoData['videoId']+'/singleVideoOG.html?videoId='+currentVideoData['videoId'] + '&type=VideoPlayer/16x9&widgetId=2&trackingGroup=69017')
					});
				}								
				return result;		
	 		},

            getCarouselData: function() {
            	var playlistItems = _.map(this.get('videoPlayer').getActivePlaylistData().items, function(item, key) {
            		item.videoIndex = key;            		  

            		return item;
            	});
            	
				// Below is a stupid hack to handle a bug where if a videoId is passed in along with a playlist then the pause carousel would show two of the same thumbnails or only one. And no one knows why.
				playlistItems.length = playlistItems.length -1;
				
				var playlistLength = playlistItems.length;
				if (playlistLength == 0) {
					this.get('videoPlayer').getContainerElement().find('.ndn_pauseCarousel').hide();
				}				
				// ugg finally the hack is over, continue showing the playlist if there is one to show.

                return {'playlistItems': playlistItems};
            },
            	 		
	 		/**
	 		 * @return {Boolean} Whether this pause overlay is currently showing
	 		 */
	 		isShowing: function() {
	 			return this.get('isShowing');
	 		},

	 		/**
	  		 * @return {Boolean} Can the player show social buttons?
	  		 */
	 		canShowSocialButton: function(){
	 			var canShowSocial = this.get('videoPlayer').get('playerData').settings.share.socialNetworks.enabled;
				return canShowSocial;	 			
	 		} 	
		});
	}
);
