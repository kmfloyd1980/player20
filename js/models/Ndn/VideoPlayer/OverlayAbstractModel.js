define([
 	'jquery',
 	'backbone'
], function(
	$,
	Backbone
) {
	return Backbone.Model.extend({
		initialize: function() {
			Backbone.Model.prototype.initialize.apply(this, arguments);
			
			// Indicate that this overlay is not currently showing
			this.set('isShowing', false);
		},
		
		hide: function() {
			if (typeof this.get('view').hide == 'function') {
				this.get('view').hide();
			}
			else {
				this.get('view').$el.hide();
			}

			// Indicate that this overlay is not currently showing
			this.set('isShowing', false);
		},
		
		show: function() {
			if (typeof this.get('view').show == 'function') {
				this.get('view').show();
			}
			else {
				this.get('view').$el.prependTo(this.get('videoPlayer').getContainerElement()).show();
			}
			
			// Indicate that this overlay is currently showing
			this.set('isShowing', true);
		},
		
		/**
		 * @return {Boolean} Whether this overlay is currently showing
		 */
		isShowing: function() {
			return !!this.get('isShowing');
		}
	});
});