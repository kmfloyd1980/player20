define([
 	'jquery',
 	'models/Ndn/App',
 	'models/Ndn/Utils/UrlParser', 	
 	'models/Ndn/Utils/UserAgent',
 	'models/Ndn/VideoPlayer/OverlayAbstractModel', 	
 	'views/Ndn/VideoPlayer/EnforceVisibilityOverlayView',
 	
 	'jquery_resize'
], function(
	$,
	Ndn_App,
	Ndn_Utils_UrlParser,
	Ndn_Utils_UserAgent,
	Ndn_VideoPlayer_OverlayAbstractModel,
	Ndn_VideoPlayer_EnforceVisibilityOverlayView
) {
	/**
	 * Whether the current URL allows for visibility requirements to be enforced
	 * @var {Boolean}
	 */
	var _onEnforceableUrl = (function() {
		// return ((window.location.hostname != window.parent.location.hostname) ? document.referrer.hostname : document.location.hostname) != 'https://tweetdeck.twitter.com';
		
		var embedOriginUrl = Ndn_App.getEmbedOriginUrl(),
			embedOriginUrlInfo = Ndn_Utils_UrlParser.parseHref(embedOriginUrl);
		
		var url = embedOriginUrlInfo['protocol'] + '//' + embedOriginUrlInfo['domain'];
		
		// console.log('_onEnforceableUrl(), url ==', url);
		
		return url != 'https://tweetdeck.twitter.com';
	})();
	
	/**
	 * @private
	 */
	var _meetsVisibilityRequirements = function() {
		if (!_onEnforceableUrl) return true;
		
		var containerElement = this.get('videoPlayer').getContainerElement();
		
		// If the container element is not visible (and not currently loading offscreen)
		if (!containerElement.is(':visible') && !containerElement.hasClass('ndn_loadingOffScreen')) return false;
		
		// If the Container is not on mobile then use the 300x168 min dimensions 
		if (!Ndn_Utils_UserAgent.isMobile()) return containerElement.width() >= this.get('minimumWidth') && containerElement.height() >= this.get('minimumHeight');
		
		// If the container is on mobile then use the 285x160 min dimensionse 
		if (Ndn_Utils_UserAgent.isMobile()) return containerElement.width() >= this.get('minimumMobileWidth') && containerElement.height() >= this.get('minimumMobileHeight');			
	};
	
	/**
	 * @private
	 */
	var _enforceVisibilityRequirements = function() {
		// Ensure that the product's container element's dimensions are up-to-date
		this.get('videoPlayer').get('widget').synchronizeContainerDimensions();
		
		var meetsVisibilityRequirements = _meetsVisibilityRequirements.call(this),
			hasChangedStatus = meetsVisibilityRequirements !== this.get('meetsVisibilityRequirements');
		
		// Indicate whether this video player meets its visibility requirements
		this.set('meetsVisibilityRequirements', meetsVisibilityRequirements);
		
		if (hasChangedStatus) {
			if (meetsVisibilityRequirements) {				
				_handleAppeasement.call(this);
			}
			else {
				_handleViolation.call(this);
			}
		}
	};
	
	/**
	 * @private
	 */
	var _handleViolation = function() {
		this.show();
		this.set('wasPlayingBeforeViolation', this.get('videoPlayer').isPlaying());
		this.get('videoPlayer').pause();
		
		this.trigger('visibilityViolation');
			
		// Check every half-second whether the visibility violation has been dealt with or not
		if (this.get('checkInterval') !== null) {
			this.set('checkInterval', setInterval($.proxy(function() {
				_enforceVisibilityRequirements.call(this);
			}, this), 500));
		}
	};

	/**
	 * @private
	 */
	var _handleAppeasement = function() {
		this.hide();
		
		// If the video player was playing prior to violating its visibility, have the player begin playing again
		if (this.get('wasPlayingBeforeViolation')) {
			this.get('videoPlayer').play();
		}
		
		this.trigger('visibilityAppeasement');
		
		// Clear the interval that checks every so often whether the visibility violation has been dealt with
		clearInterval(this.get('checkInterval'));
		this.set('checkInterval', null);
	};
	
	return Ndn_VideoPlayer_OverlayAbstractModel.extend({
		defaults: {
			/**
			 * The video player associated with this overlay
			 * @var {Ndn_VideoPlayer}
			 */
			'videoPlayer': null,

			/**
			 * The minimum width that the specified video player must have
			 * @var {int}
			 */
			'minimumWidth': 300,
			
			/**
			 * The minimum height that the specified video player must have
			 * @var {int}
			 */
			'minimumHeight': 168,
			
			/**
			 * The minimum responsive width that the specified video player must have for mobile (some partner pages are < 300px wide). 
			 * @var {int}
			 */
			'minimumMobileWidth': 250,
			
			/**
			 * The minimum height that the specified video player must have if set to be responsive (300x168.75 = 9/16)
			 * @var {int}
			 */
			'minimumMobileHeight': 141,
			
			/**
			 * Whether the video player meets visibility requirements
			 * @var {?Boolean}
			 */
			'meetsVisibilityRequirements': null,
			
			/**
			 * Whether the video player was playing just before its visibililty violation
			 * @var {Boolean}
			 */
			'wasPlayingBeforeViolation': false,
			
			/**
			 * An interval (created using setInterval() JavaScript function) that checks every half-second if the visibility-violation has been appeased yet
			 * @var {int}
			 */
			'checkInterval': null,
			
			/**
			 * @var {Ndn_AbstractView}
			 */
			'view': null
		},
		
		/**
		 * @param {Ndn_VideoPlayer} videoPlayer The video player that this overlay is associated with
		 */
		initialize: function(videoPlayer) {
			this.set({
				videoPlayer: videoPlayer,
				view: new Ndn_VideoPlayer_EnforceVisibilityOverlayView({
					model: this
				})
			});
			
			// Hide the overlay by default
			this.hide();
			
			videoPlayer
			.onceEmbedded($.proxy(function() {
				// Append the overlay to the player once the player gets embedded
				this.get('videoPlayer').getContainerElement().append(this.get('view').$el);
				
				// Enforce visibility requirements as the video player's container element is resized
				videoPlayer.getContainerElement().resize($.proxy(function() {
					_enforceVisibilityRequirements.call(this);
				}, this));
			}, this));
			
			videoPlayer.addDeliberator('loadAttempt', $.proxy(function(deferred) {
				_enforceVisibilityRequirements.call(this);
				if (this.get('meetsVisibilityRequirements')) {
					deferred.resolve();
				}
				else {
					this.once('visibilityAppeasement', deferred.resolve);
				}
			}, this));
		}
	});
});