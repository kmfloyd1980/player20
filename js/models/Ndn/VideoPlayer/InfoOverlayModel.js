define([
 	'jquery',
 	'models/Ndn/VideoPlayer/OverlayAbstractModel',
 	'views/Ndn/VideoPlayer/InfoOverlayView',
 	'models/Ndn/Widget'
 ], function(
	$,
	Ndn_VideoPlayer_OverlayAbstractModel,
	Ndn_VideoPlayer_InfoOverlayView,
	Ndn_Widget
) {
 	return Ndn_VideoPlayer_OverlayAbstractModel.extend({
 		defaults: {
			/**
			 * The video player associated with this overlay
			 * @var {Ndn_VideoPlayer}
			 */
	 		'videoPlayer': null,
			
			/**
			 * @var {Ndn_AbstractView}
			 */
			'view': null,
			
			/**
			 * Whether this overlay allows user interaction (via mouseover or touch for mobile devices) to toggle whether it is displayed
			 * @var {Boolean}
			 */
			'toggleableDisplay': true
		},
		
		/**
		 * @param {Ndn_VideoPlayer} videoPlayer The video player that this overlay is associated with
		 */
 		initialize: function(videoPlayer) {
 			this.set('videoPlayer', videoPlayer);
 			
			this.set('view', new Ndn_VideoPlayer_InfoOverlayView({
				model: this
			}));
			
			var overlayModel = this,
				overlayView = this.get('view');
			
			videoPlayer.on({
				'videoLoad': $.proxy(function(event) {
					this.get('view').updateHtml();
					
					this.get('videoPlayer').onceEmbedded($.proxy(function() {
						this.show();
					}, this));
				}, this),
				'play': function() {
					overlayModel
					.setToggleableDisplay(true)
					.setVisibility(false);
				},
				'videoPause': function() {
					// Show this overlay without the ability to toggle it to disappear via user interaction
					overlayModel
					.setToggleableDisplay(false)
					.setVisibility(true, true);
				}
			});
			
			var overlayContainer = this.get('view').$el; // videoPlayer.getContainerElement().find('.ndn_infoOverlayContainer');
			
			videoPlayer.onceEmbedded(function() {
				var videoPlayerContainerElement = videoPlayer.getContainerElement();
				
				videoPlayerContainerElement.on({
					'mouseenter touchstart': function() {
						if (overlayModel.get('toggleableDisplay')) overlayModel.setVisibility(true);
					},
					'mouseleave touchend': function() {
						if (overlayModel.get('toggleableDisplay')) overlayModel.setVisibility(false);
					}
				});
			});
  		},
  		
  		/**
  		 * @param {Boolean} visible Whether the overlay should be visible
  		 * @param {Boolean} [instantly] Whether the transition to the specified visibility state should happen instantly; defaults to false
  		 * @return this
  		 */
  		setVisibility: function(visible, instantly) {
  			if (typeof instantly == 'undefined') instantly = false;
  			
			var overlayContainer = this.get('view').$el,
				usingHtmlPlayer = this.get('videoPlayer').getPlaybackMode() != 'flash';
  			
  			if (usingHtmlPlayer) {
  				if (visible) {
  	  				overlayContainer
  					.stop(true, true)
  					.css('display', 'block')
  					.css('visibility', 'visible');
  					
  					// delay is needed to match the showing of the control bar in the player
  					overlayContainer
  					.delay(0)
  					.fadeIn(900);
  				}
  				else {
					// delay of 4700ms need to match the control bar hiding in the player
					overlayContainer
					.stop(true, true)
					.delay(4700)
					.fadeOut(900);
  				}
  			}
  			else {
  				if (visible) {
					overlayContainer
					.stop(true, true)
					.fadeIn();	
  				}
  				else {
					overlayContainer
					.stop(true, true)
					.delay(1000)
					.fadeOut(800);
  				}
  			}
  			
  			// If specified, make the transition to the specified visibility state instantly 
  			if (instantly) {
  				overlayContainer.stop(true, true);
  			}
  			
  			return this;
  		},
  		
  		/**
  		 * @param {Boolean} toggle Whether this overlay's visibility is toggleable via interaction with the user interface
  		 * @return this
  		 */
  		setToggleableDisplay: function(toggle) {
  			this.set('toggleableDisplay', toggle);
  			return this;
  		},
  		
  		/**
  		 * @return this
  		 */
  		show: function(event) {
  			// show in all players except for Inline300
  			if (this.get('videoPlayer').get('widget').get('config')['type'] != 'VideoPlayer/Inline300') {
	  			Ndn_VideoPlayer_OverlayAbstractModel.prototype.show.apply(this, arguments);
  			}
  			
  			return this;
  		},
  		
  		/**
  		 * @return {Boolean} Can the player show social buttons?
  		 */
 		canShowSocialButton: function(){
 			var canShowSocial = this.get('videoPlayer').get('playerData').settings.share.socialNetworks.enabled;
			return canShowSocial;	 			
 		} 		
   	});
 });