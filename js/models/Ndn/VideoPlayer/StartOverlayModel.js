define([
 	'jquery',
 	'underscore',
 	'models/Ndn/VideoPlayer/OverlayAbstractModel',
 	'views/Ndn/VideoPlayer/StartOverlayView',
 	'models/Ndn/Utils/UserAgent',
 	
 	'lib/jquery_plugins/jquery.backstretch.min',
	'jquery_autoellipsis'
], function(
	$,
	_,
	Ndn_VideoPlayer_OverlayAbstractModel,
	Ndn_VideoPlayer_StartOverlayView,
	Ndn_Utils_UserAgent
) {
	return Ndn_VideoPlayer_OverlayAbstractModel.extend({	
		
		defaults: {
			/**
			 * The video player associated with this overlay
			 * @var {Ndn_VideoPlayer}
			 */
			'videoPlayer': null,
			
			/**
			 * @var {Ndn_VideoPlayer_StartOverlayView}
			 */
			'view': null
		},
		
		/**
		 * @param {Ndn_VideoPlayer} videoPlayer The video player that this overlay is associated with
		 */
		initialize: function(videoPlayer) {
			this.set('videoPlayer', videoPlayer);
			
			this.set('view', new Ndn_VideoPlayer_StartOverlayView({
				model: this
			}));

			// Once the video player is embedded on the page, append this overlay to the dom container that the video player was embedded to
			this.get('videoPlayer').onceEmbedded($.proxy(function() {
				this.get('videoPlayer').getContainerElement().append(this.get('view').$el.hide());
			}, this));
			
			// Hides this overlay
			var hide = $.proxy(function() {
				this.hide();
			}, this);
			
			this.get('videoPlayer').on({
				// When a video finishes loading, show this overlay
				'videoLoad': $.proxy(function(event) {
					// console.log('StartOverlayModel.. videoLoad heard!');
					
					this.get('videoPlayer').onceEmbedded($.proxy(function() {
						// console.log('StartOverlayModel, videoPlayer is onceEmbedded...');
						
						if (!event.playOnLoad) { //  || !Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically() // && Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically()) { // 
							this.show();
							
							// Once the "videoTimeUpdate" is heard, make sure that this overlay is hidden
							this.get('videoPlayer').once('videoTimeUpdate', hide);
							if (this.get('videoPlayer').get('widget').get('config')['adsEnabled']) {
								this.get('videoPlayer').once('adTimeUpdate', hide);
							}
						}
					}, this));
				}, this),
				
				// Once the video player associated with this overlay starts video or ad playback, hide this overlay
				'play': hide,
				'videoStartRequest': hide,
				'adStart': hide,
				'videoStart': hide
			});
		},
		
		show: function() {
			// Update the info panel
			var panelContainer = this.get('view').$el.find('.ndn_pauseVideoInfo');
			var videoData = this.get('videoPlayer').getCurrentVideoData();
			
			panelContainer.find('.ndn_videoTitle').html(videoData['videoTitle']);
			panelContainer.find('.ndn_videoProviderName').html(videoData['provider']['name']);
			panelContainer.find('.ndn_videoDescription').html(videoData['description']);
			panelContainer.find('img').attr('src', videoData['provider']['logoUrl']);				
			panelContainer.show();
			
			// Remove any previously backstretched images, then backstretch the currently loaded video's still image
			this.get('view').$el.find('.backstretch').remove();
			this.get('view').$el.backstretch(this.get('videoPlayer').getCurrentVideoData()['stillImageUrl']);
			
			// Prepend this overlay's container element to the video player's container element, and show it
			this.get('view').$el.prependTo(this.get('videoPlayer').getContainerElement()).show();
		}
	});
});
	