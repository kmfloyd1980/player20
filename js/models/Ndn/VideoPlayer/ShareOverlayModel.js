define([
 	'jquery',
 	'models/Ndn/VideoPlayer/OverlayAbstractModel',
 	'views/Ndn/VideoPlayer/ShareOverlayView',
 	'models/Ndn/App',
    'models/Ndn/Widget',
    'models/Ndn/Widget/Config',
 	'models/Ndn/PlayerServices',
 	'mustache',
 	
 	'text!templates/ndn/shared/share.html'
 ], function(
	$,
	Ndn_VideoPlayer_OverlayAbstractModel,
	Ndn_VideoPlayer_ShareOverlayView,
	Ndn_App,
	Ndn_Widget,
	Ndn_Widget_Config,
	Ndn_PlayerServices,
	Mustache,
	
	shareTemplate
) {
 	return Ndn_VideoPlayer_OverlayAbstractModel.extend({
 		defaults: {
			/**
			 * The video player associated with this overlay
			 * @var {Ndn_VideoPlayer}
			 */
	 		'videoPlayer': null,
			
			/**
			 * @var {Ndn_AbstractView}
			 */
			'view': null
		},

		/**
		 * @param {Ndn_VideoPlayer} videoPlayer The video player that this overlay is associated with
		 */
 		initialize: function(videoPlayer) {
			this.set('videoPlayer', videoPlayer);
			this.set('view', new Ndn_VideoPlayer_ShareOverlayView({
				model: this
			}));

			// Once the video player is embedded on the page, append this overlay to the dom container that the video player was embedded to
			this.get('videoPlayer').onceEmbedded($.proxy(function() {
				this.get('videoPlayer').getContainerElement().append(this.get('view').$el.hide());
			}, this));
			
			this.get('videoPlayer')
			.on('videoLoad', $.proxy(function() {
				this.get('view').updateHtml();
			}, this))
			.on('share', $.proxy(function(event) {
				this.get('videoPlayer').pause();
				
				// If this "share" event was triggered by the Facebook or Twitter icons on the player's control bar, launch the appropriate URL for sharing and pause the player
				if (event.data.provider == 'facebook' || event.data.provider == 'twitter') {
					
					this.get('videoPlayer').getContainerElement().find('.ndn_pauseOverlayContainer').hide();
					var currentVideoData = this.get('videoPlayer').getCurrentVideoData(),
						encodedShareUrl = encodeURIComponent(this.getShareableUrl()),
						encodedTwitterShareUrl = encodeURIComponent('https://social.newsinc.com/media/json/69017/' + currentVideoData['videoId'] + '/singleVideoOG.html?type=VideoPlayer/Single&widgetId=2&trackingGroup=69017&videoId=' + currentVideoData['videoId']);
					
					if (event.data.provider == 'facebook') {
						shareUrl = 'http://api.addthis.com/oexchange/0.8/forward/' + event.data.provider + '/offer?url=' + encodedShareUrl + '&pubid=ra-518bbde23f7ed2a9&ct=1';
						shareUrl += '&title=' + currentVideoData.description;
					}
					if (event.data.provider == 'twitter') {
						shareUrl = 'http://api.addthis.com/oexchange/0.8/forward/' + event.data.provider + '/offer?url=' + encodedTwitterShareUrl + '&pubid=ra-518bbde23f7ed2a9&ct=1';
					}					
					window.open(shareUrl, '', '');				
					
				}
				
				// If the share icon was clicked, now we are hiding the pause overlay instead of showing it. The player will still be paused when the share overlay is present but will be transparent with the video paused beneath it.
				if(event.data.provider == 'share'){				
					// Hiding the pause overlay when we click on share now
					this.get('videoPlayer').getContainerElement().find('.ndn_pauseOverlayContainer').hide();
					
					this.show();
				}
				
				else {
					this.show();
				}
			}, this));
  		},
  		
  		/**
  		 * @return {String} The embed code to be used when a user would like to embed the current video player's content
  		 */
  		getEmbedCode: function() {
  			var widget = this.get('videoPlayer').get('widget'),
  				widgetConfig = widget.get('config'),
  				currentVideoData = this.get('videoPlayer').getCurrentVideoData(),
  				playlistId = this.get('videoPlayer').getCurrentPlaylistId();
  			
  			return '<iframe width="590" height="332" src="http://launch.newsinc.com/?type=VideoPlayer/Single&widgetId=1&trackingGroup=69016' +
				(widgetConfig['siteSection'] ? '&siteSection=' + widgetConfig['siteSection'] : '') +
  				'&videoId=' + currentVideoData['videoId'] + 
  				(playlistId ? '&playlistId=' + playlistId : '') + 
  				'" frameborder="no" scrolling="no" noresize marginwidth="0" marginheight="0"></iframe>';
  		},
  		
  		/**
  		 * @return {String} The direct link that may be shared with others in order to view this video again directly
  		 */
  		getDirectLink: function() {
  			var widget = this.get('videoPlayer').get('widget'),
				widgetConfig = widget.get('config'),
				directLinkData = this.get('videoPlayer').get('playerData').settings.share.directLink,
				currentVideoData = this.get('videoPlayer').getCurrentVideoData(),
				playlistId = this.get('videoPlayer').get('playlistId'),
				isAppLandingPage = !!Ndn_App.getAppUrlInfo(directLinkData.landingPageUrl),
				configSettingPrefix = !isAppLandingPage
					? Ndn_Widget_Config.getDefaultUrlVarPrefixForConfigSettings()
					: '';
  			
  			var legacySupport = '&freewheel=' + widgetConfig['trackingGroup'] +
  				'&sitesection=' + widgetConfig['siteSection'] +
  				'&vid=' + currentVideoData['videoId'];
  			
  			var directLinkPageUrl = directLinkData.landingPageUrl;
  			if (directLinkPageUrl.indexOf('?') === -1) {
  				directLinkPageUrl += '?';
  			}
  			
  			return directLinkPageUrl + 
  				configSettingPrefix + 'trackingGroup=' + widget.get('config')['trackingGroup'] + 
  				(widgetConfig['siteSection'] ? '&' + configSettingPrefix + 'siteSection=' + widgetConfig['siteSection'] : '') + 
  				'&' + configSettingPrefix + 'videoId=' + currentVideoData['videoId'] +
  				(!isAppLandingPage ? legacySupport : '');
  		},
 		
 		/**
  		 * @return {Boolean} Can the player show the email button?
  		 */
 		canShowEmailButton: function(){
 			var canShowEmail = this.get('videoPlayer').get('playerData').settings.share.email.enabled;	
			return canShowEmail;	 			
 		},

 		/**
  		 * @return {Boolean} Can the player show social buttons?
  		 */
 		canShowSocialButton: function(){
 			var canShowSocial = this.get('videoPlayer').get('playerData').settings.share.socialNetworks.enabled;
			return canShowSocial;	 			
 		},
 		
 		/**
  		 * @return {Boolean} Can the player show the embed button?
  		 */
 		canShowEmbedButton: function(){
 			var canShowEmbed = this.get('videoPlayer').getCurrentVideoData().settings.share.embedCode.enabled;	 
			return canShowEmbed;	 			
 		},

 		/**
 		 * @param {String} [videoId] The database ID of the video to create a shareable URL; defaults to the currently loaded video's ID if not provided
 		 * @return {String} The shareable URL pertaining to the specified video
 		 */
 		getShareableUrl: function(videoId) {
 			if (typeof videoId == 'undefined') videoId = (this.get('videoPlayer').getCurrentVideoData() || {}).videoId;
 			
 			return 'http://social.newsinc.com/media/json/69017/' + videoId + '/singleVideoOG.html?type=VideoPlayer/16x9&trackingGroup=69017&videoId=' + videoId;
 		}
 	});
 });
