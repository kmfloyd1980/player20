define([
    'jquery',
    'backbone',
    'models/Ndn/Analytics/Comscore'
],
function(
    $,
    Backbone,
    Ndn_Analytics_Comscore
) {
    return Backbone.Model.extend({
        defaults: {
            /**
             * The video player associated with this behavior
             * @var {Ndn_VideoPlayer}
             */
            'videoPlayer': null
        },
        
        initialize: function(videoPlayer) {
            this.set('videoPlayer', videoPlayer);
            
            var widget = videoPlayer.get('widget'),
                widgetView = widget.get('view');
            
            var stepIntoNextRmm = function() {
                // Keep track of the number of continuous plays
                videoPlayer.incrementContinuousPlayCounter();
                
                // If the last video of the currently loaded playlist data just finished playing, trigger the appropriate event
                if (videoPlayer.stepIntoNextVideo()) {
                    // If the number of continuous plays is equal (or greater than, because it might have been skipped if we just reached the 
                    // end of the playlist) to the number of continuous plays allowed
                    if (videoPlayer.getContinuousPlays() >= videoPlayer.getContinuousPlaysLimit() + 1) {
                        // Reset the continuous plays counter
                        videoPlayer.resetContinuousPlayCounter();

                        // Indicate that the series of continuous plays has ended
                        videoPlayer.concludeRmmContinuousPlay();
                    }
                    else {
                        // Act as though another RMM is about to get played even though only the ad and not the 6-second bumper video will not play
                        videoPlayer.playRmm(); // videoPlayer.playRmm(videoPlayer.getCurrentVideoIndex() + 1);
                    }
                }
            };
            
            // Handle continuous play
            videoPlayer.on({
                'videoEnd': stepIntoNextRmm,
                'videoStart': function() {
                    // If this it the beginning of a 6-second bumper that is not the first 6-second bumper, do not play it and continue on to the next RMM ad experience
                    if (videoPlayer.getVideoLoadIndex() > 0) {
                        // Wait a moment before stepping into the next RMM ad experience...
                        setTimeout(function() {
                            stepIntoNextRmm();
                        }, 1000);
                    }
                    else {
                        // If this is the 6-second bumper video that is supposed to be visible, make sure that the video player is visible
                        widgetView.setVideoPlayerOnScreen(true);
                    }
                },
                'adEnd': function() {
                    // If this is not the first video loaded (the 6-second bumper video that should actually show),
                    // then hide the video player immediately after an ad ends
                    if (videoPlayer.getVideoLoadIndex() > 0) {
                        widgetView.setVideoPlayerOnScreen(false);
                    }
                }
            });
            
            // Only execute the Comscore Video Metrix ad call and video start call only once each
            videoPlayer.once({
                'adStart': function() {
                    Ndn_Analytics_Comscore.executeAdCall(videoPlayer.getParams());
                },
                'videoStart': function() {
                    Ndn_Analytics_Comscore.executeVideoStartCall(videoPlayer.getParams());
                }
            });
        }
    });
});