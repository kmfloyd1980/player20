var _nw2e = _nw2e || [];
var _ndnq = _ndnq || [];

/**
 * @module Ndn
 */
define([
	 	'jquery',
	 	'underscore',
	 	'backbone',
	 	'require',
	 	'models/Ndn/Utils/UrlParser',
	 	'models/Ndn/Analytics',
	 	'models/Ndn/AdsTargeting',
	 	'models/Ndn/Hooks',
	 	'models/Ndn/EventHub',
	 	'models/Ndn/Debugger', // This is also added here so that functionality testing may be achieved
	 	'models/Ndn/Analytics/ChartbeatStrategy',

	 	'models/Ndn/ChromeExtLayer'
 	], function(
 		$,
 		_,
 		Backbone,
 		require,
 		Ndn_Utils_UrlParser,
 		Ndn_Analytics,
 		Ndn_AdsTargeting,
 		Ndn_Hooks,
 		Ndn_EventHub,
 		Ndn_Debugger,
 		Ndn_Analytics_ChartbeatStrategy
 	) {
		// Prevent Ndn_App from being defined multiple times (in addition to preventing the 
		// global _nw2e variable from being processed multiple times)
		if (_nw2e.Ndn_App) {
			return _nw2e.Ndn_App;
		}

		// Ensure Backbone uses our jQuery version and not a globally-scoped version
		Backbone.$ = $;
		
		// Expose access to NDN Player Suite's internal JavaScript libraries for unit testing
		_nw2e.jQuery = $;
		_nw2e.require = require;
		
		/**
		 * The domain name of this application's URL
		 * @var {String}
		 */
		var _appDomain;
		
		/**
		 * @var {Ndn_AdsTargeting}
		 */
		var _adsTargeting = new Ndn_AdsTargeting();
		
		/**
		 * Whether the NDN Player Suite application has been initialized
		 * @var {Boolean}
		 */
		var _isAppInitialized = false;
		
		/**
		 * Structured data concerning all the widgets that have begun their embed process. Format:
		 * <pre>[
		 * 	{Ndn_Widget}, // An instance of Ndn_Widget that has had its embedding process initialized
		 * 	... // All other instances of Ndn_Widget that have had their embedding process initialized
		 * ]</pre>
		 * @var {Array<Ndn_Widget>}
		 */
		var _widgets = [];
		
		/**
		 * List of application settings toggleable through URL query and/or hash parameters
		 * @var {Array<String,Boolean>}
		 */
		var _appSettings;
		
		/**
		 * The build number for this version of the NDN Player Suite or the empty string ("") for unbuilt local versions
		 * @var {String}
		 */
		var _appBuild = '';
		
		/**
		 * The environment of this current version of the NDN Player Suite; for example: "production", "staging", "qa4", "qa3", "qa", "dev2", "dev", "local2", or empty string ("") for unbuilt local versions
		 * @var {String}
		 */
		var _appEnvironment = '';

		// Determine what the application version's environment and build number are
		(function() {
			var matches = (_nw2e.appUrl + '').match(/^((https?:)?\/\/(([^-]+)?[-]?launch\.newsinc\.com))\/?(\d+$)?/);
			
			if (matches) {
				_appBuild = ((matches[5] + '').toLowerCase() == 'launch.newsinc.com')
					? 'production'
					: (matches[5] || '');
				
				_appEnvironment = matches[4] || '';
			}
		})();
		
		/**
		 * The base singleton class for the NDN Player Suite
		 * @class Ndn_App
		 */
		var Ndn_App = {
			/**
			 * @return {String} The environment of this current version of the NDN Player Suite, examples: "production", "qa", "qa2", "staging", "dev2", "local", or "" for local unbuilt versions of this application
			 */
			getEnv: function() {
				return _appEnvironment;
			},
			
			/**
			 * @return {String} The build number for this version of the NDN Player Suite or the empty string ("") for unbuilt local versions
			 */
			getBuild: function() {
				return _appBuild;
			},
			
			/*
			getEnvPrefix: function(envType) {
			    if (typeof this.getEnvOverrides()[envType] != 'undefined') {
			        return this.getEnvOverrides()[envType]
			            ? this.getEnvOverrides()[envType] + '-'
	                    : '';
			    }
			    
			    return '';
			},
			
			getEnvOverrides: function() {
				var result = {
					perfectPixel: Ndn_Utils_UrlParser.getUrlParam('ndn_ppEnv') == 'prod' ? '' : Ndn_Utils_UrlParser.getUrlParam('ndn_ppEnv') || Ndn_App.getSetting('perfectPixelEnv'),
					playerServices: Ndn_Utils_UrlParser.getUrlParam('ndn_playerServicesEnv') == 'prod' ? '' : Ndn_Utils_UrlParser.getUrlParam('ndn_playerServicesEnv') || Ndn_App.getSetting('playerServicesEnv'),
					analytics: Ndn_Utils_UrlParser.getUrlParam('ndn_analyticsEnv') == 'prod' ? '' : Ndn_Utils_UrlParser.getUrlParam('ndn_analyticsEnv') || Ndn_App.getSetting('analyticsEnv')
				};
				
				$.each(result, function(envName, envValue) {
				    result[envName] = (result[envName] || '').replace(/[^A-Za-z0-9-_]/g, '');
				});
				
				return result;
			},
            */
            
			getEnvOverrides: function() {
				return {
					perfectPixel: Ndn_Utils_UrlParser.getUrlVar('ndn_ppEnv'),
					playerServices: Ndn_Utils_UrlParser.getUrlVar('ndn_playerServicesEnv'),
					analytics: Ndn_Utils_UrlParser.getUrlVar('ndn_analyticsEnv')
				};
			},
			
			/**
			 * @param {String} appSettingsString The string to parse app settings for
			 * @return {Object} Structured data concerning the app settings
			 */
			parseAppSettingsString: (function() {
			    var booleanSettingNames = [
                   'forceHtmlPlayer',
                   'playOnInViewToggle',
                   'disableAddThis',
                   'operationCwal',
                   'launchModalEmbeds',
                   'launchModalEmbedsV2',
                   'forceMobileAutoplayAdPlayerForRmm',
                   'lkqd_tagqa',
                   'updatedEmbedCode',
                   'chartbeatAnalytics',
                   'googleAnalytics',
                   'forceComscoreVme',
                   'forceIframes',
                   'forceIframesUsingSrc',
                   'vast3'
			    ];
			    
			    var stringSettingNames = [
			        'chartbeatAnalyticsUid',
                    'adPlayerTypeForRmm',
                    'rmmContinuousPlay',
                    'rmmContinuousPlayPercentage'
                ];
			    
			    var environmentSettingNames = [
                    'perfectPixelEnv',
                    'playerServicesEnv',
                    'analyticsEnv'
			    ];
			    
			    // Add the environment-specific setting names to the list of string setting names
			    stringSettingNames = stringSettingNames.concat(environmentSettingNames);
			    
			    return function(appSettingsString) {
                    var result = {};
                    
                    var settings = Ndn_Utils_UrlParser.parseParams(appSettingsString);
                    
                    $.each(settings, function(settingName, settingValue) {
                        if (_.contains(booleanSettingNames, settingName)) {
                            result[settingName] = typeof settingValue == 'undefined' || !(settingValue == '0' || settingValue.toLowerCase() == 'false');
                        }
                        else if (_.contains(stringSettingNames, settingName)) {
                            result[settingName] = settingValue;
                            
                            // For environment values, make sure these are safe values (only alpha-numeric with possible hyphens and underscores)
                            if (_.contains(environmentSettingNames, settingName)) {
                                result[settingName] = settingValue.replace(/[^A-Za-z0-9-_]/g, '');
                            }
                        }
                    });
                    
                    return result;
			    };
            })(),
            
            getDefaultSettings: function() {
                return this.parseAppSettingsString(_nw2e.appSettings || '');
            },
            
            getOverrideSettings: function() {
                return this.parseAppSettingsString(_nw2e.appOverrideSettings || Ndn_Utils_UrlParser.getUrlParam('ndn_appSettings') || '');
            },
			
			getSettings: function() {
			    // If the application settings have not been initialized yet, do so
                if (!_appSettings) {
                    _appSettings = _.extend(
                        {},
                        this.getDefaultSettings(),
                        this.getOverrideSettings()
                    );
                    
                    if (Ndn_Debugger.inMode('appSettings')) console.log('_appSettings ==', _appSettings);
                }
                
                return _appSettings;
			},
				
			/**
			 * @param {String} settingName The name of an application setting
			 * @return {Boolean} Whether the specified application setting is enabled
			 */
			getSetting: function(settingName) {
			    var settings = this.getSettings();
				return settings[settingName];
			},
			
			/**
			 * @return {String} Any suffix to include to the end of URLs to other pages within the same application
			 */
			getAppUrlSuffix: function() {
				var queryStringParams = ['lessc', 'ndn.custom.localStorage', 'env'];
				
				var foundParams = [];
				var urlVars = Ndn_Utils_UrlParser.getUrlVars();
				for (var paramName in urlVars) {
					if (urlVars.hasOwnProperty(paramName)) {
						foundParams.push(paramName + '=' + encodeURIComponent(urlVars[paramName]));
					}
				}
				
				if (foundParams.length) {
					return '&' + foundParams.join('&');
				}
				else {
					return '';
				}
			},
			
			/**
			 * @return {Array<Ndn_Widget>} List of registered embedded (or about to be embedded) products for the current page 
			 */
			getRegisteredEmbeds: function() {
				return _widgets;
			},
			
			/**
			 * @param {Ndn_Widget} widget The widget to be integrated
			 */
			integrateWidget: function(widget) {
				Ndn_EventHub.observeWidget(widget);
				
				widget.triggerEvent('widgetEmbedStart', {
					initialContainerElementHtml: widget.get('initialContainerElementHtml'),
					initialConfig: widget.get('initialConfig')
				}, 'hook,analytics');
				
				// Keep a record of all widgets that have begun being embedded with the NDN Player Suite
				_widgets.push(widget);
			},
			
			/**
			 * @param {Ndn_Widget} widgetToRemove The widget to be removed from this application's list of integrated widgets
			 */
			unintegrateWidget: function(widgetToRemove) {
				$.each(_widgets, function(index, widget) {
					if (widget.getContainerElement().attr('id') == widgetToRemove.getContainerElement().attr('id')) {
						_widgets.splice(index, 1);
					}
				});
			},
			
			/**
			 * @param {String} hookId The hook identifier
			 * @param {Function(Object)} hookCallback The handler function for when the hook occurs
			 */
			addHook: function(hookId, hookCallback) {
				Ndn_Hooks.getPublicApiAccess().on(hookId, hookCallback);
			},
			
			/**
			 * @return {String} The URL of the page responsible for embedding widgets
			 */
			getEmbedOriginUrl: function() {
				var result = window.location.href.toString();
				
				// If the current browser window is located on, for example, the landing-page.html file,
				// then look to the "embedOriginUrl=" value from the current page's query string for the
				// real URL of where the embed process originating. Otherwise, if the current page is an
				// application page as well as in an iframe, return the URL of the window's referrer. 
				// Lastly, if the current page is an application page but is neither in an iframe nor in 
				// a popup window, then use the current page's URL as the embed origin URL.
				if (Ndn_App.isOnAppPage()) {
					result = Ndn_Utils_UrlParser.getUrlVar('embedOriginUrl')
						|| (Ndn_App.isEmbeddedByIframe() && document.referrer)
						|| window.location.href.toString();
				}
				
				return result;
			},
			
			/**
			 * @return {Ndn_AdsTargeting} The instance responsible for managing all ads targeting
			 */
			getAdsTargeting: function() {
				return _adsTargeting;
			},
			
			/**
			 * Makes the provided closure globally accessible within JavaScript's current window
			 * @param {Function} closure The function to make globally accessible within JavaScript's current window
			 * @return {String} The full name of the closure that is now globally accessible within JavaScript's current window
			 */
			makeClosureGlobal: (function() {
				_nw2e['closures'] = _nw2e['closures'] || [];
				
				var closureIndex = 0;
				
				return function(closure) {
					var name = 'c' + (closureIndex++);
					
					_nw2e['closures'][name] = closure;
					
					return '_nw2e.closures.' + name;
				};
			})(),
			
			/**
			 * @return {Boolean} Whether the codebase has been minified and namespaced using our build process
			 */
			isBuilt: function() {
				// Use the global namespace variable created by the build process as an indicator
				return typeof(Ndn_Require) !== 'undefined';
			},
			
			/**
			 * @return {Boolean} Whether the current page is being served through the browser using SSL
			 */
			isOnSsl: function() {
				return window.location.protocol == "https:";
			},
			
			/**
			 * @return {String} The application's base URL
			 */
			getAppUrl: function() {
				return _nw2e.appUrl;
			},
			
			/**
			 * @return {String} The domain name of this application's URL
			 */
			getAppDomain: function() {
				if (!_appDomain) {
					_appDomain = this.isBuilt()
						? this.getAppUrl().match(/\/\/(.*)\//)[1]
						: window.location.host + '';
				}
				
				return _appDomain;
			},
			
			/**
			 * @param {String} [appUrl] The application URL; defaults to the current application URL
			 * @param {Boolean} [isBuilt] Whether the embed code's needed script tag should be generated for a built environment; defaults to whether the current environment is built
			 */
			getEmbedCodeScriptTag: function(appUrl, isBuilt) {
			    if (typeof appUrl == 'undefined') appUrl = this.getAppUrl();
			    if (typeof isBuilt == 'undefined') isBuilt = this.isBuilt();
			    
                var requireJsHtml = '<script type="text/javascript" src="' + appUrl + '/js/require.js"></script>';
                return (!isBuilt ? requireJsHtml : '') + '<script type="text/javascript" src="' + appUrl + '/js/embed.js" id="_nw2e-js"></script>';
			},
			
            /**
			 * @return {Boolean} Whether the current window is the result of a product being launched from NDN Player Suite launcher product
			 */
			isInLaunchedBrowserWindow: function() {
			    return this.isOnAppPage() && Ndn_Utils_UrlParser.getUrlVar('embedOriginUrl');
			},
			
			/**
			 * @return {Boolean} Whether this widget is embedded solely by using an <iframe /> element
			 */
			isEmbeddedByIframe: _.memoize(function() {
				// The following logic depends on the expectation that the "/share.html" webpage is the only "application page" that is 
				// ever expected to be linked to from another webpage (with one exception), and that "/share.html" will never be enclosed 
				// within an <iframe>. The only exception to this, is when the NDN Player Suite launches a 16:9 player, which also passes 
				// the "embedOriginUrl" parameter in the URL's query string.
				return _nw2e.isInIframe || (this.isOnAppPage() && 
					!Ndn_Utils_UrlParser.getUrlVar('embedOriginUrl') && 
					document.referrer && 
					!window.location.pathname.toString().match(/^\/(\d+\/)?share\.html$/));
			}),
			
			/**
			 * @return {Boolean} Whether the current page is a page within the current application, ie. "/embed.html?..."
			 */
			isOnAppPage: function() {
				return !!(window.location.host + '').match(/^([^0-9-]+)?([0-9]*)?[-]?launch\.newsinc\.com$/); // window.location.host == this.getAppDomain();
			},

			/**
			 * @param {String} url A URL that may or may not point to an asset used by the NDN Player Suite application
			 * @return {Object?} Structured data concerning the protocol, environment, environment lane, and environment build number (or null if the provided URL is not an application URL). Format:
			 * <pre>{
			 * 	'protocol': String, // The protocol; example values: 'https:', 'http:', or ''
			 * 	'env': String, // The environment; example values: 'staging', 'qa', 'dev', 'local', or '' (for production). This value does not include the environment lane.
			 * 	'envLane': String, // The environment lane; example values: '' (for production or staging since they have no lanes or "qa-launch.newsinc.com", etc.), '2' (for "dev2", "qa2", "local2", etc.)
			 * 	'envName': String, // The environment name; example values: "dev2", "dev", "qa4", "qa", "local2", "", "staging"
			 * 	'buildNumber': String, // The environment build number; for example "0" for "http://local-launch.newsinc.com/0" or "26" for "http://launch.newsinc.com/26"
			 * 	'path': String, // The URL's path relative to the application's base application URL
			 * 	'subdomain': String, // The URL's subdomain; for example "qa2-launch.newsinc.com" for "http://qa2-launch.newsinc.com/27/js/embed.js"
			 * 	'baseUrl': String, // The environment's base application URL; for example "http://local-launch.newsinc.com" for "http://local-launch.newsinc.com/js/embed.js", or "http://launch.newsinc.com/46" for "http://launch.newsinc.com/46/css/NdnEmbed.css"
			 * }</pre>
			 */
			getAppUrlInfo: function(appUrl) {
				var matches = (appUrl + '').match(/^((https?:)?\/\/(([^0-9-]+)?([0-9]*)?[-]?launch\.newsinc\.com))\/?(\d+)?(\/.*)?$/);
				
				if (!matches) return null;
				
				var result = {
					protocol: matches[2] || '',
					env: matches[4] || '',
					envLane: matches[5] || '',
					buildNumber: matches[6] || '',
					path: matches[7] || ''
				};
				
				result.envName = result.env + result.envLane;
				result.subdomain = (result.envName ? result.envName + '-' : '') + 'launch.newsinc.com';
				result.baseUrl = result.protocol + '//' + result.subdomain + (result.buildNumber ? '/' + result.buildNumber : '');
				
				return result;
			},
			
			/**
			 * Embeds widgets on all container dom elements with the current page's <body></body> tags with the class of "ndn_embed" and the appropriate "data-config-*" attributes
			 */
			embedWidgetsFromContainersOnPage: function() {
			    var elementsToEmbed = $('.ndn_embed:not(.ndn_embedding,.ndn_embedded)'),
			        numberOfElementsToEmbed = elementsToEmbed.length;
			    
			    elementsToEmbed.each(function() {
					var embedContainer = this;
					require(['models/Ndn/Widget'], function(Ndn_Widget) {
						Ndn_Widget.embed(embedContainer);
					});
				});
			},
			
			/**
			 * @param {Function} callback The closure to execute once the NDN Player Suite application is initialized
			 */
			onceAppIsInitialized: function(callback) {
				if (_isAppInitialized) {
					callback();
				}
				else {
					Ndn_EventHub.once('initialized', callback);
				}
			},
			
			/**
			 * @return {jQuery.Deferred} Resolved parameters format:
			 * <pre>[
			 * 	{ // Structured data concerning IDs used for analytics
			 * 		'uniqueUserId': {String}, // The unique user ID
			 * 		'pageViewSessionId': {String}, // The page view session ID
			 * 	}
			 * ]</pre>
			 */
			getAnalyticsIds: function() {
				return Ndn_Analytics.getIds();
			},

			/**
			 * @return {String} Unique identifier specifying this page view's session (using version 4 UUID)
			 */
			getPageViewSessionId: function() {
				return Ndn_Analytics.getPageViewSessionId();
			},

			/**
			 * @return {String} Unique identifier specifying this page view's session (using version 4 UUID)
			 */
			getUniqueUserId: function() {
				return Ndn_Analytics.getUniqueUserId();
			}
		};
		
		// Handle the legacy player's support of the "ndnEndOfVideo()" function
		Ndn_App.addHook('*/videoEnd', function() {
			if (typeof ndnEndOfVideo == 'function') {
				ndnEndOfVideo();
			}
		});
		
		// Track the page view; this only occurs once ever for each page load due to the fact that Ndn_App only gets defined once as well in the code
		_nw2e.onReady(function() {
			Ndn_EventHub.trigger('analytics:pageView');
		});

		// Handle the implementation of the "_nw2e" global variable
		(function() {
			// Override the _nw2e variable so that new pushes to its array are processed by Ndn_Widget instead
			_nw2e.push = function(data) {
				// console.log('_nw2e.push(', arguments, ');');
				
				if (typeof data[0] != 'undefined') {
					if (data[0] == 'hook') {
						Ndn_App.addHook(data[1], data[2]);
					}
					else if (data[0] == 'embed') {
						// Load the Ndn_Widget module needed to embed the widget data provided
						require(['models/Ndn/Widget'], function(Ndn_Widget) {
							if (data[1]) {
								Ndn_Widget.embed(data[1], data[2], ['jsapi']);
							}
							else {
								_nw2e.onReady(function() {
									Ndn_App.embedWidgetsFromContainersOnPage();
								});
								
								// Once the application is initialized, indicate to the entire application that it is initialized (as opposed to waiting for dom ready or window onload events the default way)
								Ndn_App.onceAppIsInitialized(function() {
									_nw2e.onReady();
								});
							}
						});
					}
					else if (data[0] == 'when') {
						if (data[1] == 'initialized') {
							Ndn_App.onceAppIsInitialized(data[2]);
						}
					}
					else if (data[0] == 'command') {
						if (data[1].match(/\/videoPause$/)) {
							// Determine the container element ID from the "<container-element-id>/videoPause" command
							var containerElementId = data[1].split('/')[0];
							
							// Find the widget that has begun being embedded and is identifiable using the specified container element ID
							$.each(_widgets, function(index, widget) {
								if (widget.getContainerElement().attr('id') == containerElementId) {
									// Once the widget's config has been finalized by receiving its config defaults from player services
									widget.onceConfigIsComplete(function() {
										// If this widget has a video player, execute its "pause" method
										if (widget.get('videoPlayer')) {
											widget.get('videoPlayer').pause();
										}
									});
									
									// Only apply this command to the first widget that matches the specified container element ID
									return false;
								}
							});
						}
						else if (data[1] == 'forceReady') {
							_nw2e.onReady();
						}
					}
					else {
						throw "Unrecognized _nw2e instruction: " + data[0];
					}
				}
				else {
					// Load the Ndn_Widget module needed to embed the widget data provided
					require(['models/Ndn/Widget'], function(Ndn_Widget) {
						Ndn_Widget.embed(data.containerElementId, data.config);
					});
				}
			};

			// Process any of the already pushed embed data or hook event handlers
			var embedData;
			while (embedData = _nw2e.shift()) {
				_nw2e.push(embedData);
			}
			
			// TODO: Refactor the "_nw2e" global variable out for a lighter footprint
			// IMPORTANT NOTE: PPix/2.0 embed and NDN Studio are using this variable however! Make sure they are no longer depending on "_nw2e" before removing this completely!
			// 
			// Channel off anything that was added using the "_ndnq" global, documented, and supported variable into using the _nw2e global variable instead
			_ndnq.push = _nw2e.push;
			while (embedData = _ndnq.shift()) {
				_ndnq.push(embedData);
			}
		})();
		
		// Remember that the Ndn_App library has been loaded and save a reference to it in the global "_nw2e" variable
		_nw2e.Ndn_App = Ndn_App;
		
		Ndn_EventHub.once('initialized', function() {
			_isAppInitialized = true;
		});
		
		// Indicate that the NDN Player Suite application has been initialized
		Ndn_EventHub.trigger('initialized');
		
		return Ndn_App;
	}
);
