var _ndnq = _ndnq || [];
var _nw2e = _nw2e || [];

/**
 * An instance of this class can observe widgets and proxy their events to be listened to via access to the Ndn_Widget.addHook() method.
 */
define([
		'jquery',
		'underscore',
		'backbone',
		'models/Ndn/Utils/UrlParser'
	],
	function(
		$,
		_,
		Backbone,
		Ndn_Utils_UrlParser
	) {
		// Do not instantiate this class multiple times
		if (_nw2e.Ndn_Debugger) {
			return _nw2e.Ndn_Debugger;
		}
		
		/**
		 * Structured data concerning which modes are enabled. Format:
		 * <pre>{
		 * 	String: true, // The enabled mode's identifier => true
		 * 	... // All other enabled mode identifiers
		 * }</pre>
		 * @var {Object}
		 */
		var _enabledModes;
		
		/**
		 * List of responses that were requested via _nw2e.Ndn_Debugger.requestEventData()
		 * @var <Array<String,Object>
		 */
		var _requestResponses = {};
		
		var _concludeRequestIdentifier = function(requestIdentifier) {
			// Add an invisible element to the dom to indicate to the unit testing driver that the requested data has received a response
			$(document.createElement('div'))
			.attr({
				'id': requestIdentifier,
				'style': 'display: none !important;'
			})
			.appendTo('body');
		};
		
		_nw2e.Ndn_Debugger = {
			/**
			 * TODO: Finish documenting this method.
			 * 
			 * Pre-requisite: All calls to this method should be wrapped in the following: _ndnq.push(['when', 'initialized', function() { ... }]);
			 * 
			 * @param {String} requestIdentifier
			 * @param {String} eventIdentifier See Ndn_Debugger.fetchEventData() method.
			 */
			requestEventData: function(requestIdentifier, eventIdentifier) {
				if (typeof _requestResponses[requestIdentifier] != 'undefined') {
					// Indicate to the browser that the requested data is ready for withdraw
					_concludeRequestIdentifier(requestIdentifier);
				}
				else {
					// _nw2e.Ndn_EventHub should already be present due to the pre-requisite that all calls to this method should be wrapped in specific code described in this method's description
					_nw2e.Ndn_EventHub.fetchEventData(eventIdentifier).done(function(event) {
						// Always cache the event's data mapped to the event request identifier that was used to request this event's data
						_requestResponses[requestIdentifier] = event;
						
						// Indicate to the browser that the requested data is ready for withdraw
						_concludeRequestIdentifier(requestIdentifier);
					});
				}
			},
			
			/**
			 * @param {String} requestIdentifier The identifier that describes the request that was made using _nw2e.Ndn_Debugger.requestHookData() method
			 * @return {unknown_type} The
			 */
			getResponse: function(requestIdentifier) {
				// Remove the dom element as it is no longer needed to communicate back to unit testing driver that the request has a response
				$('#' + requestIdentifier).remove();
				
				return _requestResponses[requestIdentifier];
			},
			
			/**
			 * @return {Boolean} Whether debug mode is enabled
			 */
			isEnabled: function() {
				return "ndn_debug" in Ndn_Utils_UrlParser.getQueryParams() || "ndn_debug" in Ndn_Utils_UrlParser.getHashParams();
			},
			
			/**
			 * @return {Object}
			 */
			getEnabledModes: function() {
				if (!_enabledModes) {
					_enabledModes = {};
					
					if (this.isEnabled()) {
						var modes = (Ndn_Utils_UrlParser.getUrlParam('ndn_debug') || '').split(',');
						$.each(modes, function() {
						    var mode = this + '';
						    if (mode != '') {
	                            _enabledModes[this + ''] = true;
						    }
						});
					}
				}
				
				return _enabledModes;
			},
			
			/**
			 * @return {Array<String>} List of enabled debug modes
			 */
			getEnabledModesList: function() {
                var result = [];
                if (this.isEnabled()) {
                    $.each(this.getEnabledModes(), function(modeLabel) {
                        result.push(modeLabel);
                    });
			    }
			    return result;
			},
			
			/**
			 * @param {String} modeIdentifier The identifier of a specific debug mode that can be enabled by listing this mode in the query string like so: "?ndn_debug=<modeIdentifier>"
			 * @return {Boolean} Whether the specified mode is currently enabled
			 */
			inMode: function(modeIdentifier) {
				return !!this.getEnabledModes()[modeIdentifier];
			}
		};
		
		return _nw2e.Ndn_Debugger;
	}
);
