var _nw2e = _nw2e || [];

define(['underscore'], function(_) {
	// Avoid this library from getting initialized twice
	if (_nw2e.Ndn_Utils_UrlParser) {
		return _nw2e.Ndn_Utils_UrlParser;
	}
	else {
		_nw2e.Ndn_Utils_UrlParser = {};
	}
	
	/**
	 * An associative array mapping the query string parameter identifiers with their respective values. For example, a query string 
	 * such as "?wid=4&freewheel=69365&cid=5409#ndn_debug=true&somethingElse=1" would be indicated by a JSON object like so:
	 * <pre>{
	 * 	'query': {
	 * 		'wid': '4',
	 * 		'freewheel': '69365',
	 * 		'cid': '5409'
	 * 	},
	 * 	'hash': {
	 * 		'ndn_debug': 'true',
	 * 		'somethingElse': '1'
	 * 	}
	 * }</pre>
	 * @var {?Object.<string, string>}
	 */
	var _params = {
		'query': null,
		'hash': null
	};
	
	return _.extend(_nw2e.Ndn_Utils_UrlParser, {
		/**
		 * @param {String} paramsString String of parameters formatted like so: "wid=4&freewheel=69365&cid=5409"
		 * @return {Object<String,String>} Structured data concerning the parameters found in the provided string; all parameter values are decoded using JavaScript's "decodeURIComponent" function
		 */
		parseParams: function(paramsString) {
			var result = {};
			
			var decode = function(value) {
				var result = value;
				
				try {
					result = decodeURIComponent(result);
				}
				catch (e) {
					result = result.replace(/%([^0-9A-Fa-f]|$)/g, function(m0, m1) {
						return '%25' + m1;
					});
					result = decodeURIComponent(result);
				}
				
				return result;
			};
			
			var keyValuePairs = (paramsString + '').split('&');
			_.each(keyValuePairs, function(keyValuePair) {
				var pairData = keyValuePair.split('=');
				
				result[pairData[0]] = pairData[1] ? decode(pairData[1]) : pairData[1];
			});
			
			return result;
		},
		
		getUrlParam: function(paramName, useCache) {
			return this.getQueryParam(paramName, useCache) || this.getHashParam(paramName, useCache);
		},
		
		getQueryParam: function(paramName, useCache) {
			return this.getParam(paramName, 'query', useCache);
		},
		
		getQueryParams: function(useCache) {
			return this.getParams('query', useCache);
		},

		getHashParam: function(paramName, useCache) {
			return this.getParam(paramName, 'hash', useCache);
		},
		
		getHashParams: function(useCache) {
			return this.getParams('hash', useCache);
		},
		
		/**
		 * @param {String} paramName The parameter name
		 * @param {String} paramType The type of parameter to return; recognized values: "query", "hash"
		 * @param {boolean} [useCache=true] Whether the previous cached result (if there is one) should be returned
		 * @return {String} The specified parameter's associated value (or undefined if no such parameter exists)
		 */
		getParam: function(paramName, paramType, useCache) {
			if (typeof useCache == 'undefined') useCache = true;
			
			// Update the cache appropriately
			_params[paramType] = this.getParams(paramType, useCache);
			
			return (_params[paramType] || {})[paramName];
		},
		
		/**
		 * @param {String} paramType The type of parameters to return; recognized values: "query", "hash"
		 * @param {boolean} [useCache=true] Whether the previous cached result (if there is one) should be returned
		 * @return {!Object} An associative array of query string parameter names mapping to their associated value
		 */
		getParams: function(paramType, useCache) {
			if (typeof useCache == 'undefined') useCache = true;
			
			if (!useCache || _params[paramType] === null) {
				// Make sure you aren't modifying the actual "window.location" reference
				var href = window.location + '';
				
				_params[paramType] = this.parseHref(href)[paramType + 'Params'];
			}
			
			return _params[paramType];
	    },
	    
	    /**
	     * @param {String} prefix The prefix that query string params must have in order to be returned in the result
	     * @return {Object<String,String>} An associative array of query string parameter names (with provided prefix stripped off) mapping to their associated value, but only of query string parameters whose name begins with the provided prefix
	     */
	    getUrlVarsWithPrefix: function(prefix) {
	    	var result = {};
	    	
	    	_.each(this.getUrlVars(), function(value, index) {
	    		if (index.substring(0, prefix.length) == prefix) {
	    			result[index.substring(prefix.length)] = value;
	    		}
	    	});
	    	
	    	return result;
	    },
	    
	    /**
	     * @param {String} href The "href" value
	     * @return {Object} Structured data concerning the provided "href" value. Format:
	     * <pre>{
	     * 	'protocol': {String}, // The protocol of the provided "href" value, for example "http:" or "https:"
	     * 	'domain': {String}, // The domain name of the provided "href" value, for example "www.facebook.com" or "launch.newsinc.com"
	     * 	'url': {String}, // The URL to the webpage from the provided "href" value without the query string parameters // TODO: Remove the hash value as well (and make sure nothing is expecting it to be there)
	     * 	'path': {String}, // The path of the URL provided without the query string parameters or hash value
	     * 	'queryParams': {Object<String,String>} // The query string parameters mapped to their associated values
	     * 	'hashParams': {Object<String,String>} // The hash string parameters mapped to their associated values
	     * }</pre>
	     */
	    parseHref: function(href) {
			return {
				'protocol': (function() {
					var match = href.match(/^[^:]*:/g);
					return match ? match[0] : null;
				})(),
				'domain': (function() {
					try {
						var match = href.match(/^[^:]*:\/\/([^\/]*)/g);
						return match ? match[0].split('//')[1] : null;
					} catch (e) {}
				})(),
				'url': href.replace(/\?.*$/, ''),
				'path': href.replace(/^[^:]*:[\/]{2}[^\/]*/, '').replace(/\?.*$/, '').replace(/#.*$/, ''),
				'queryParams': this.parseParams(href.replace(/#.*$/, '').replace(/^[^?]*[?]?/, '')),
				'hashParams': this.parseParams(href.replace(/^[^#]*[#]?/, ''))
			};
	    },
	    
	    /**
	     * @param {String} [url] The URL to find its query string for (if not provided, the current window's URL is used by default)
	     * @return {String} The substring of the provided URL (or the current window's URL by default) that follows after the URL's first "?" character
	     */
	    getUrlQueryString: function(url) {
	    	if (typeof url == 'undefined') url = window.location.href.toString();
	    	
	    	// If the URL contains a "?" character
	    	if (url.indexOf('?') !== -1) {
	    		return url.replace(/^[^?]*\?/, '');
	    	}
	    	else {
	    		return '';
	    	}
	    },

		/**
		 * @deprecated Use this.getQueryParam() instead
		 */
		getUrlVar: function(paramName, useCache) {
			return this.getQueryParam(paramName, useCache);
		},
		
		/**
		 * @deprecated Use this.getQueryParams() instead
		 */
		getUrlVars: function(useCache) {
			return this.getQueryParams(useCache);
		}
	});
});