define(['jquery'], function($) {
	/**
	 * Structured data concerning the active closure intervals that are repeatedly evaluated against until their associated logic closure returns true. Format:
	 * <pre>{
	 * 	String: { // The string uniquely associated with a condition closure passed to the Ndn_Utils_DomElement.onceMeetsCondition() method => structured data concerning dom elements that have this type of condition being repeatedly evaluated against
	 * 		Number: jQuery.Deferred, // The interval ID (assigned via the setInterval() function) of the active closure interval => the jQuery.Deferred object that resolves once the dom element (that this closure is checking) returns true
	 * 		... // All other active closure intervals
	 * 	},
	 * 	... // All other strings uniquely associated with a condition closure passed to the Ndn_Utils_DomElement.onceMeetsCondition() method
	 * }</pre>
	 * @var {Object}
	 */
	var _activeDomElementIntervals = {};
	
	return {
		/**
		 * @param {DOMNode|jQueryElement} domElement The dom element to use when evaluating against the provided condition logic
		 * @param {Function} condition The closure to use in order to evaluate whether the provided dom element meets specified conditions. The first parameter of the closure is passed the jQueryElement it is to evaluate; the closure must return true in order to resolve its associated deferred object.
		 * @param {String} conditionId String uniquely associated with the condition closure passed so that the condition logic provided does not conflict with other condition closures passed. *Important note: This string is used to store data paired with the provided dom element using jQuery's .data() method, so avoid naming conflicts with other plugins/libraries.
		 * @param {Number} [intervalLength] How often the provided condition closure should be evaluated (measured in milliseconds); defaults to 500
		 * @return {jQuery.Deferred} Resolves when the provided closure returns true when evaluating against the provided dom element
		 */
		onceMeetsCondition: function(domElement, condition, conditionId, intervalLength) {
			if (typeof intervalLength == 'undefined') intervalLength = 500;
			
			// Get the jQueryElement object of the provided dom node
			var element = $(domElement);
			
			return $.Deferred(function(deferred) {
				if (condition(element)) {
					// If the provided closure evaluates to true when passed the provided dom element, resolve the deferred object
					return deferred.resolve();
				}
				else {
					// If this element has an active interval already for the specified conditional logic
					if (element.data(conditionId)) {
						// Use the remembered deferred object (of the previous call to this method using the provided element and condition) to notify the current deferred object when it has been resolved
						_activeDomElementIntervals[conditionId][element.data(conditionId)].done(function() {
							deferred.resolve();
						});
					}
					else {
						// Get the interval ID for the new interval evaluates against the provided dom element with the provided conditional logic
						var intervalId = setInterval(function() {
							// If the provided dom element evalutes to true when passed to the provided condition logic
							if (condition(element)) {
								// Clear/remove the active interval for this condition
								clearInterval(element.data(conditionId));
								
								// Disassociate the interval ID with its currently associated dom element and condition logic
								element.removeData(conditionId);
								
								// Resolve the appropriate deferred object
								deferred.resolve();
								
								// Remove the reference to this remembered deferred object as it is no longer needed
								delete _activeDomElementIntervals[conditionId][intervalId];
							}
						}, intervalLength);
						
						// Remember this deferred object for the case when this element and conditional logic is passed to this method again and we need it to notify the new method call of this deferred object's status/progress
						_activeDomElementIntervals[conditionId] = _activeDomElementIntervals[conditionId] || {};
						_activeDomElementIntervals[conditionId][intervalId] = deferred;
						
						// Associate the interval ID to this element for the specified conditional logic
						element.data(conditionId, intervalId);
					}
				}
			}).promise();
		},
		
		/**
		 * @param {DOMNode|jQueryElement} domElement The dom element to use when waiting for said element to have a width and a height greater than zero
		 * @return {jQuery.Deferred} Resolves when it detects the provided dom element has a width and height greater than zero
		 */
		onceHasWidthAndHeight: function(domElement) {
			return this.onceMeetsCondition(domElement, function(element) {
				return element.width() && element.height();
			}, 'ndnWidthAndHeightInterval');
		}
	};
});