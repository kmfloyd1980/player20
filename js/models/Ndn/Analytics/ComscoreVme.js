define([
    'jquery',
    'backbone'
], function(
    $,
    Backbone
) {
    return Backbone.Model.extend({
        defaults: {
            /**
             * @var {Ndn_VideoPlayer} The video player to make Comscore VME analytics calls for
             */
            videoPlayer: null,
            
            /**
             * @var {integer} The most recently encountered ad ID reportedly received by this instance's associated video player 
             */
            mostRecentAdId: null,
            
            /**
             * @var {Boolean} Whether the VME tag no longer needs to be placed for the remainder of ads within the current series of ad(s)
             */
            isAdSeriesDone: false,
            
            /**
             * @var {Boolean} Whether the most recently loaded ad series is for UI ad(s)
             */
            isUiAdSeries: false
        },
        
        initialize: function(videoPlayer) {
            this.set('videoPlayer', videoPlayer);
            
            var self = this;
            videoPlayer.on({
                'videoLoad': function(event) {
                    self.set({
                        'isAdSeriesDone': false,
                        'isUiAdSeries': videoPlayer.getParams()['isUiAdSeries']
                    });
                },
                'adLoad': function(event) {
                    if (!self.isAdSeriesDone()) {
                        self.set('mostRecentAdId', event.adId);
                    }
                },
                'adStart': function(event) {
                    if (!self.isAdSeriesDone()) {
                        self.placeVmeTag();
                    }
                }
            });
        },
        
        /**
         * @return {Boolean} Whether the VME tag no longer needs to be placed for the remainder of ads within the current series of ad(s)
         */
        isAdSeriesDone: function() {
            return this.get('isAdSeriesDone');
        },
        
        getVmeTag: function() {
            var videoPlayer = this.get('videoPlayer'),
                product = videoPlayer.get('widget'),
                productConfig = product.get('config');
            
            // Determine the "src" attribute for the <script> tag
            return 'http://b.voicefive.com/c2/18416366/rs.js' +
                '#c1=3' +
                '&c3=18416366_VME' +
                '&c4=' + product.getPltValue() + '_' + this.get('mostRecentAdId') +
                '&c5=' + productConfig['siteSection'] +
                '&c6=' +
                '&c10=1' +
                '&c11=' + productConfig['trackingGroup'] +
                '&c12=&c13=&c16=gen&ax_vme=1&';
        },
        
        placeVmeTag: function() {
            // Determine the "src" attribute for the <script> tag
            var scriptTagUrl = this.getVmeTag();
            
            // Add the <script> tag to the page
            var scriptElement = document.createElement('script'); scriptElement.type = 'text/javascript';
            scriptElement.src = scriptTagUrl;
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(scriptElement, s);

            // If currently in a non-UI ad series, indicate that the one VME tag for the current ad series has been placed (no other VME tags will need to be placed within this ad series)
            if (!this.get('isUiAdSeries')) {
                this.set('isAdSeriesDone', true);
            }
        }
    });
});