define([
	'jquery',
	'underscore',
	'require',
	'backbone',
	'models/Ndn/Utils/UrlParser',
	'models/Ndn/Utils/FlashDetect',
	'models/Ndn/Utils/UserAgent',
	// 'models/Ndn/Utils/DataEncoder',
	
	'lib/jquery_plugins/jquery.json'
],
function(
	$,
	_,
	require,
	Backbone,
	Ndn_Utils_UrlParser,
	Ndn_Utils_FlashDetect,
	Ndn_Utils_UserAgent,
	Ndn_Utils_DataEncoder
) {
    var _analyticsEndpointProtocolVersion = 1;
    
    var _getTime = function() {
        return Date.now ? Date.now() : (new Date().valueOf());
    };
    
    var _getScreenWidth = function() {
        return !!screen ? screen.width || null : null
    };
    
    var _getScreenHeight = function() {
        return !!screen ? screen.height || null : null
    };
    
    var _getViewportWidth = function() {
        return $(window).width();
    };
    
    var _getViewportHeight = function() {
        return $(window).height();
    };
    
    var _furl = encodeURIComponent(window.location.href.split('?')[0]);
    
    // Get the 'analyticsServerPrefix' url param.  This is for testing purposes.
    var _getAnalyticsServerPrefix = function() {
        var prefix = Ndn_Utils_UrlParser.getUrlVar('ndn_analyticsEnv') || Ndn_Utils_UrlParser.getUrlVar('analyticsServerPrefix') || '';
        prefix = prefix.replace(/[^a-zA-Z]/g, '').slice(0,10);
        return (prefix.length > 0) ? prefix + '-' : prefix;
    };
    
    /**
     * This static variable is instantiated once this class' .initialize(Ndn_Analytics) method is called
     * @var {Ndn_Analytics}
     * @static
     */
    var _analytics;
    
    var _getAnalyticsTimelineEventIndex = (function() {
        var index = 0;
        
        return function() {
            return index++;
        };
    })();
    
    /**
     * @param {int} [eventTimestamp] The timestamp when this event initially occured
     * @return {int} The delta (measured in milliseconds) from the first analytics call made during the current page view session
     */
    var _getAnalyticsTimelineEventTimestamp = (function() {
        var originTimestamp;
        
        return function(eventTimestamp) {
            eventTimestamp = eventTimestamp || _getTime();
            
            // If the origin timestamp has not yet been created, create it and return 0 since this is the first event requesting a timestamp
            if (typeof originTimestamp == 'undefined') {
                originTimestamp = eventTimestamp;
                return 0;
            }
            else {
                return eventTimestamp - originTimestamp;
            }
        };
    })();
    
    /**
     * @param {String} endpointId The endpoint ID being called for analytics
     * @return {Number} The next index to use to describe the analytics call occurring with the specified endpoint ID
     */
    var _getAnalyticsEndpointIdIndex = (function() {
        var indices = {};
        
        return function(endpointId) {
            // If the next index for the specified endpoint does not yet exist, create it and set it to 0
            if (typeof indices[endpointId] == 'undefined') indices[endpointId] = 0;
            
            return indices[endpointId]++;
        };
    })();
    
    /**
     * @param {String} endpointId The identifier that helps describe the URL of the endpoint to send the provided data to (such as "vv" for "c.newsinc.com/1/vv")
     * @param {Object} data The data to send to the specified endpoint
     * @param {Number} [eventTimestamp] The timestamp when the event occured
     */
    var _reportData = function(endpointId, data, eventTimestamp) {
        // var endpointUrl = window.location.protocol + '//' + _getAnalyticsServerPrefix() + 'c.newsinc.com/' + _analyticsEndpointProtocolVersion + '/' + endpointId;
        var endpointUrl = window.location.protocol + '//' + _getAnalyticsServerPrefix() + 'c.newsinc.com/' + endpointId;
        
        _analytics.getIds().done(function() {
            var extraData = {
                'uut': _analytics.getUniqueUserId() || '',
                'insid': _analytics.getPageViewSessionId() || '',
                'atei': _getAnalyticsTimelineEventIndex(),
                'atets': _getAnalyticsTimelineEventTimestamp(eventTimestamp)
            };
            
            // Keep this data at the top of the data sent, but also use this data to override any other data that may accidentally try to override this data
            data = _.extend(extraData, data, {'_': _getTime()}, extraData);
            
            // Allow for the analytics calls to be exposed
            _ndnq.push(['when', 'initialized', function() {
                var eventData = {
                    endpointId: endpointId,
                    endpointUrl: endpointUrl,
                    eventTimestamp: eventTimestamp,
                    eventData: data
                };
                
                _nw2e.Ndn_EventHub.trigger('analyticsCall:*', eventData);
                _nw2e.Ndn_EventHub.trigger('analyticsCall:*[' + _getAnalyticsEndpointIdIndex('*') + ']', eventData);
                _nw2e.Ndn_EventHub.trigger('analyticsCall:' + endpointId + '[' + _getAnalyticsEndpointIdIndex(endpointId) + ']', eventData);
                _nw2e.Ndn_EventHub.trigger('analyticsCall:' + endpointId, eventData);
            }]);
            
            // Make a GET call to the appropriate 204 analytics endpoint instead of using a POST call (as problems seem to be arising from making a POST instead of GET call)
            var imageElement = new Image;
            imageElement.src = endpointUrl + '?' + $.param(data);
            
            /*
            // For IE8/IE9, explicitly use the XDomainRequest object
            if (_.contains([8, 9], Ndn_Utils_UserAgent.getIeVersion())) {
                // Use Microsoft XDR
                var xdr = new XDomainRequest();
                xdr.open('POST', endpointUrl);
                
                // These event handlers and wrapped timeout declaration are needed in order to not abort the request prematurely;
                // reference: http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
                xdr.onprogress = function() {};
                xdr.ontimeout = function() {};
                xdr.onerror = function() {};
                setTimeout(function() {
                    // Send the data
                    xdr.send($.param(data));
                }, 0);
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: endpointUrl,
                    data: data
                });
            }
            */
        });
    };
    
    /**
     * @param {Object} jsonObject The JSON object to encode in JSON for sending through an analytics call
     * @return {String} URL-encoded JSON string describing the provided JSON object
     */
    var _encodeJson = function(jsonObject) {
        return encodeURIComponent($.toJSON(jsonObject));
    };
    
    /**
     * @param {String} url A url from which to extract query params.
     * @return {Object} An object with the query params of the given url as attributes.
     */
    var _parseUrlParamsToObject = function(url) {
        var data = {};
        var params = url.split('?')[1];
        
        if (params) {
            _.each(params.split('#')[0].split('&'), function(item) {
                var exp = item.split('=');
                if (exp.length < 2) return;
                data[exp[0]] = exp[1];
            });
        }
        
        return data;
    };
    
    return {
        initialize: function(Ndn_Analytics) {
            // Assign this static variable, which is used by the static method _reportData()
            _analytics = Ndn_Analytics;
            
            Ndn_Analytics.on({
                'pageView': function(event) {
                    var data = {
                        'vw': _getViewportWidth(),
                        'vh': _getViewportHeight(),
                        'sw': _getScreenWidth(),
                        'sh': _getScreenHeight(),
                        'furl': _furl,
                        'ua': Ndn_Utils_UserAgent.getBrowser(),
                        'embedCount': $('.ndn_embed').size()
                    };
                    
                    require(['models/Ndn/App'], function(Ndn_App) {
                        data = _.extend(data, {
                            'eo': Ndn_App.getEmbedOriginUrl(),
                            'iframe': Ndn_App.isEmbeddedByIframe() ? 1 : 0,
                                    'fe': Ndn_Utils_FlashDetect.hasFlashEnabled() ? 1 : 0,
                                            'fv': Ndn_Utils_FlashDetect.getFlashVersion(),
                                            'env': Ndn_App.getEnv(),
                                            'bn': Ndn_App.getBuild(),
                                            'ref': !Ndn_App.isEmbeddedByIframe() ? document.referrer : ''
                        });
                        
                        _reportData('pl', data, event.system.timestamp);
                    });
                },
                
                '*/widgetEmbedStart': function(event) {					
                    var _getTime = function() {
                        return Date.now ? Date.now() : (new Date().valueOf());
                    };
                    
                    var widget = event.system.widget,
                        widgetConfig = widget.get('config'),
                        placedBy = widget.getPlacedByAnalyticsString(),
                        data = {
                            'ei': widget.get('embedIndex'),
                            'wid': widgetConfig['widgetId'] || '',
                            'ssid': widgetConfig['siteSection'] || '',
                            'anid': widgetConfig['trackingGroup'] || '',
                            'furl': _furl,
                            'vw': _getViewportWidth(),
                            'vh': _getViewportHeight(),
                            'sw': _getScreenWidth(),
                            'sh': _getScreenHeight(),
                            'fe': Ndn_Utils_FlashDetect.hasFlashEnabled() ? 1 : 0,
                            'fv': Ndn_Utils_FlashDetect.getFlashVersion(),
                            'placedby': placedBy,
                            'v': widget.isInView() ? 1 : 0,
                            'pcnfg': _encodeJson(widget.get('initialConfig')),
                            'cde': widget.get('initialContainerElementHtml')
                        };
                    
                    // Make the quantcast call to record this embedded product
                    $('<div style="display:none;"><img src="//pixel.quantserve.com/pixel/p-573scDfDoUH6o.gif?labels=' + (widgetConfig['trackingGroup'] || '') + ',' + _getTime() + '" border="0" height="1" width="1" alt="Quantcast"/></div>').appendTo('body');
                    
                    // Legacy analytics call
                    Ndn_Analytics.getIds().done(function() {
                        $.ajax({
                            url: window.location.protocol + '//analytics.newsinc.com/AnalyticsProvider/jsonp/analytics/PageViewJSONP',
                            type: 'GET',
                            contentType: 'application/json',
                            dataType: 'jsonp',
                            data: {
                                'uut': Ndn_Analytics.getUniqueUserId(),
                                'insid': Ndn_Analytics.getPageViewSessionId(),
                                'embedIndex': widget.get('embedIndex'),
                                'wid': widgetConfig['widgetId'] || '',
                                'ssid': widgetConfig['siteSection'] || '',
                                'anid': widgetConfig['trackingGroup'] || '',
                                'purl': null,
                                'placedby': placedBy,
                                'furl': _furl,
                                '_': _getTime()
                            }
                        });
                    });
                    
                    require(['models/Ndn/App'], function(Ndn_App) {
                        _reportData('wl', _.extend(data, {
                            'iframe': Ndn_App.isEmbeddedByIframe() ? 1 : 0
                        }), event.system.timestamp);
                    });
                },
                
                '*/configComplete': function(event) {
                    var widget = event.system.widget,
                        widgetConfig = widget.get('config');
                    
                    _reportData('cc', {
                        'ei': widget.get('embedIndex'),
                        'wid': widgetConfig['widgetId'] || '',
                        'ssid': widgetConfig['siteSection'] || '',
                        'anid': widgetConfig['trackingGroup'] || '',
                        'ads': widgetConfig['adsEnabled'] ? 1 : 0,
                        'furl': _furl,
                        'vw': _getViewportWidth(),
                        'vh': _getViewportHeight(),
                        'sw': _getScreenWidth(),
                        'sh': _getScreenHeight(),
                        'v': widget.isInView() ? 1 : 0,
                        'cnfg': _encodeJson(widget.get('config')),
                        'plt': widget.getWidgetTypeId() || ''
                    }, event.system.timestamp);
                },
                
                '*/videoLoad': function(event) {
                    var eventData = event['public'].data,
                        videoLoadParams = eventData.params || {},
                        widget = event.system.widget,
                        widgetConfig = widget.get('config');
                    
                    _reportData('vl', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'furl': _furl,
                        'wid': widgetConfig['widgetId'] || '',
                        'ssid': widgetConfig['siteSection'] || '',
                        'anid': widgetConfig['trackingGroup'] || '',
                        'vid': eventData.videoData['videoId'],
                        'playlistId': eventData.playlistId,
                        'sound': eventData.isSoundEnabled ? 1 : 0,
                        'pb': eventData.playerBehaviorId,
                        'pw': eventData.playerWidth,
                        'ph': eventData.playerHeight,
                        'v': widget.isInView() ? 1 : 0,
                        'fp': videoLoadParams.foldPositionId,
                        'wgt': videoLoadParams.wgtValue
                    }, event.system.timestamp);
                },
                
                '*/videoStartRequest': function(event) {
                    var eventData = event['public'].data,
                        widget = event.system.widget;
                    
                    _reportData('vsr', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'src': eventData.source
                    }, event.system.timestamp);
                },
                
                
                '*/adRequest': function(event) {
                    require(['models/Ndn/App'], function(Ndn_App) {
                        var eventData = event['public'].data,
                            requestData = _parseUrlParamsToObject(eventData.receivedData.requestURL || ''),
                            widget = event.system.widget,
                            widgetConfig = widget.get('config');
                        
                        var analyticsData = {
                            'ei': widget.get('embedIndex'),
                            'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                            'adtag': eventData.adTag,
                            'cszs': requestData.ciu_szs,
                            'cmsid': requestData.cmsid,
                            'corr': requestData.correlator,
                            'iu': requestData.iu,
                            'out': requestData.output,
                            'playlistId': requestData.playlistId,
                            'scor': requestData.scor,
                            'sz': requestData.sz,
                            'ups': requestData.unviewed_position_start,
                            'eo': requestData.url || (eventData.sentData && eventData.sentData.embedOriginUrl),
                            'lin': typeof eventData.receivedData.linear != 'undefined' ? (eventData.receivedData.linear ? 1 : 0) : eventData.receivedData.linear,
                            'vid': requestData.vid,
                            'vpos': eventData.receivedData.podPosition,
                            'pit': eventData.playerInstanceType || 0,
                            'aeo': Ndn_App.getEmbedOriginUrl(),
                            'anid': widgetConfig['trackingGroup'],
                            'wid': widgetConfig['widgetId']
                        };
                        
                        if (widget.get('settings').settings.dfp.isSafeViralOverride) {
                            analyticsData['ordpid'] = widget.get('settings').defaultWidgetConfig.trackingGroup;
                        }
                        
                        _reportData('ac', analyticsData, event.system.timestamp);
                    });
                },
                
                '*/adLoad': function(event) {
                    var eventData = event['public'].data,
                        widget = event.system.widget;
                    
                    // console.log('adLoad heard! event ==', event);
                    
                    _reportData('acr', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'adid': eventData.adId,
                        'adPlatform': eventData.adPlatform,
                        'adSystem': eventData.adSystem,
                        'title': eventData.adTitle,
                        'linear': eventData.linear,
                        'podPosition': eventData.podPosition,
                        'totalAds': eventData.totalAds,
                        'workflow': eventData.workflow,
                        'url': eventData.url,
                        'dur': eventData.duration
                    }, event.system.timestamp);
                },
                
                '*/adError': function(event) {
                    var eventData = event['public'].data,
                        widget = event.system.widget;
                    
                    _reportData('ae', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'vec': eventData.receivedData.vastErrorCode,
                        'et': eventData.receivedData.errorType,
                        'ec': eventData.receivedData.errorCode,
                        'msg': eventData.message
                    }, event.system.timestamp);
                },
                
                '*/adMute */adUnmute */adPause */adResume': function(event) {
                    var eventData = event['public'],
                        widget = event.system.widget;
                    
                    if (eventData.data[eventData.eventName] > 1) { return; }
                    
                    _reportData('acc', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'ev': eventData.eventName
                    }, event.system.timestamp);
                },
                
                '*/adView': function(event) {
                    var eventData = event['public'].data,
                        widget = event.system.widget,
                        widgetConfig = widget.get('config');
                    
                    _reportData('av', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'furl': _furl,
                        'ssid': widgetConfig['siteSection'],
                        'anid': widgetConfig['trackingGroup'],
                        'wid': widgetConfig['widgetId'],
                        'vid': eventData.videoData.videoId,
                        'percentPlayed': eventData.percentPlayed,
                        'pt': eventData.videoPlayerType,
                        'cvi': eventData.currentVideoIndex
                    }, event.system.timestamp);
                },
                
                '*/videoView': function(event) {
                    var eventData = event['public'].data,
                        widget = event.system.widget,
                        placedBy = widget.getPlacedByAnalyticsString(),
                        widgetConfig = widget.get('config');
                    
                    // Only track video views via the legacy analytics video view call if the current widget's 
                    // split version is not "VideoPlayer/PTR" and this video view is not related to an RMM
                    if (!_.contains(['VideoPlayer/PTR'], widget.getSplitVersionId()) && !eventData.isShowingRmm) {
                        Ndn_Analytics.getIds().done(function() {
                            var imageElement = new Image,
                                data = {
                                    'uniqueUserId': Ndn_Analytics.getUniqueUserId(),
                                    'uut': Ndn_Analytics.getUniqueUserId(),
                                    'insid': Ndn_Analytics.getPageViewSessionId(),
                                    'embedIndex': widget.get('embedIndex'),
                                    'sitesection': widgetConfig['siteSection'] || '',
                                    'event': 'progprogres',
                                    'freewheel': widgetConfig['trackingGroup'] || '',
                                    'plt': widget.getWidgetTypeId(),
                                    'videoId': eventData.videoData['videoId'],
                                    'playlistId': eventData.playlistId,
                                    'placedby': placedBy,
                                    'percentPlayed': eventData.percentPlayed,
                                    'timePlayed': eventData.currentTime,
                                    'd': _getTime()
                                };
                            
                            // Ping our server with below data
                            imageElement.src = window.location.protocol + '//ps2.newsinc.com/players/report.xml?' + $.param(data);
                        });
                    }
                    
                    _reportData('vv', {
                        'ei': widget.get('embedIndex'),
                        'vli': widget.getSplitVersionModel().get('videoPlayer').getVideoLoadIndex(),
                        'furl': _furl,
                        'placedby': placedBy,
                        'ssid': widgetConfig['siteSection'] || '',
                        'anid': widgetConfig['trackingGroup'] || '',
                        'wid': widgetConfig['widgetId'] || '',
                        'vid': eventData.videoData['videoId'],
                        'playlistId': eventData.playlistId,
                        'percentPlayed': eventData.percentPlayed,
                        'timePlayed': eventData.currentTime,
                        'cp': eventData.isInContinuousPlay ? 1 : 0,
                        'plorder': eventData.currentVideoIndex,
                        'vw': _getViewportWidth(),
                        'vh': _getViewportHeight(),
                        'sw': _getScreenWidth(),
                        'sh': _getScreenHeight(),
                        'fe': Ndn_Utils_FlashDetect.hasFlashEnabled() ? 1 : 0,
                        'fv': Ndn_Utils_FlashDetect.getFlashVersion(),
                        'v': widget.isInView() ? 1 : 0,
                        'vt': eventData.videoPlayerType,
                        'pw': eventData.playerWidth,
                        'ph': eventData.playerHeight,
                        'wgt': eventData.wgtValue
                    }, event.system.timestamp);
                }
            });
        },
        
        /**
         * @return {Number} The next analytics timeline event index to be used
         */
        getNextAtei: function() {
            return _getAnalyticsTimelineEventIndex();
        }
    };
});
