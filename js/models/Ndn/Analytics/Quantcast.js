define([
		'jquery'
	],
	function($) {
		/**
		 * The Quantcast Account ID
		 * @var {String}
		 */
		var _accountId = 'p-573scDfDoUH6o';
		
		return {
			initialize: function(Ndn_Analytics) {
				// TODO: NOTE THAT THIS IS DOING NOTHING AND HAS BEEN MOVED TO Ndn_Analytics_Ndn MODULE!!!!
				return;
				
				Ndn_Analytics.on({	
					'*/widgetEmbedStart': function(event) {
						var widget = event.system.widget;
						
						var _getTime = function() {
							return Date.now ? Date.now() : (new Date().valueOf());
						};
						
						$('<div style="display:none;"><img src="//pixel.quantserve.com/pixel/' + _accountId + '.gif?labels=' + (widget.get('config')['trackingGroup'] || '') + ',' + _getTime() + '" border="0" height="1" width="1" alt="Quantcast"/></div>').appendTo('body');
					}
				});
			}
		};
	}
);