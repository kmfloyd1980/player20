define([
    'jquery',
    'backbone',
    'models/Ndn/Debugger'
], function(
    $,
    Backbone,
    Ndn_Debugger
) {
    /**
     * Whether debugging is enabled for this module
     * @var {Boolean}
     */
    var _debug = Ndn_Debugger.inMode('chartbeat');
    
    /**
     * Unique 2-3 character name of strategy
     * @var {String}
     */
    var _strategyName = 'ndn';
    
    var _getTime = function() {
        return Date.now ? Date.now() : (new Date().valueOf());
    };
    
    /**
     * Ensures that Chartbeat analytics is initialized by adding the appropriate "chartbeat_video.js" file to the page
     * if it has not already been added to the page using this method
     * @param {String} uidConfigSetting The "uid" config setting for the global "_sf_async_config" object for Chartbeat's config
     */
    var _initializeChartbeatAnalytics = (function() {
        var initialized = false;
        
        return function(uidConfigSetting) {
            if (!initialized) {
                // Allow Chartbeat to see the Chartbeat strategy created for NDN Player Suite's products
                (window['_cbv_strategies'] = window['_cbv_strategies'] || []).push(Ndn_Analytics_ChartbeatStrategy);
                
                // Ensure that the global "_sf_async_config" variable is set and has its "domain" property set to the current webpage's current top-level domain,
                // but does not override the "domain" property if it is already present
                window._sf_async_config = window._sf_async_config || {};
                window._sf_async_config.domain = window._sf_async_config.domain || location.hostname.split('.').slice(-2).join('.');
                
                // Also, if a "uid" config setting is provided, ensure that the global "_sf_async_config" variable has its "uid" config setting present
                if (uidConfigSetting) {
                    window._sf_async_config.uid = window._sf_async_config.uid || uidConfigSetting;
                }
                
                // Add the needed JavaScript file to the page for Chartbeat video analytics to work
                var e = document.createElement('script');
                e.setAttribute('language', 'javascript');
                e.setAttribute('type', 'text/javascript');
                e.setAttribute('src', '//static.chartbeat.com/js/chartbeat_video.js');
                document.body.appendChild(e);
                
                if (Ndn_Debugger.inMode('chartbeat')) {
                    console.log('[Ndn_Analytics_ChartbeatStrategy] Chartbeat video analytics just initialized with _sf_async_config ==', window._sf_async_config);
                }
                
                // Indicate that Chartbeat analytics have been initialized
                initialized = true;
            }
        };
    })();
    
    var Ndn_Analytics_ChartbeatStrategy = Backbone.Model.extend({
        defaults: {
            /**
             * @var {Boolean}
             */
            isStrategyInstance: true,

            /**
             * @var {Ndn_Widget}
             */
            embeddedProduct: null,

            /**
             * @var {Ndn_VideoPlayer}
             */
            videoPlayer: null,

            /**
             * Structured data concerning the params loaded into the video player for the currently loaded video
             * @var {Object}
             */
            params: null,

            /**
             * The timestamp of when the most recent "videoLoad" event occurred
             * @var {Number}
             */
            videoLoadTimestamp: null,
            
            /**
             * The video player's current state
             * @var {Ndn_Analytics_ChartbeatStrategy.VideoState}
             */
            videoState: null
        },
        
        initialize: function(embeddedProduct) {
            if (_debug) console.log('Ndn_Analytics_ChartbeatStrategy(', arguments, ');');
            
            var isOriginalStrategyInstance = Ndn_Analytics_ChartbeatStrategy.isNdnEmbeddedProduct(embeddedProduct);
            
            if (!isOriginalStrategyInstance) {
                var adapter = embeddedProduct;
                this.get = function(propertyName) {
                    return adapter.get(propertyName);
                };
                
                return;
            }
            else {
                this.set('embeddedProduct', embeddedProduct);
            }
            
            // Keep reference to this instance
            var self = this;
            
            // Once the embedded product has its video player initialized, associate this Chartbeat strategy with the appropriate NDN Player Suite video player
            this.get('embeddedProduct').getSplitVersionModel().on('videoPlayerInitialized', function() {
                self.set('videoPlayer', this.get('videoPlayer'));
                
                self.get('videoPlayer').on({
                    'videoLoad': function(eventData) {
                        // Keep track of when the most recent "videoLoad" event occurred
                        self.set('videoLoadTimestamp', _getTime());
                        
                        // Keep track of the parameters currently loaded for the current video
                        self.set('params', eventData.params);
                    }
                });
                
                // Keep the current "content type" state updated
                self.get('videoPlayer').on({
                    'videoStart': function() {
                        self.set('contentTypeState', Ndn_Analytics_ChartbeatStrategy.ContentType.CONTENT);
                    },
                    'adStart': function() {
                        self.set('contentTypeState', Ndn_Analytics_ChartbeatStrategy.ContentType.AD);
                    }
                });
                
                // Keep the current video state updated
                self.get('videoPlayer').on({
                    'adStart videoStart play': function() {
                        self.set('videoState', Ndn_Analytics_ChartbeatStrategy.VideoState.PLAYED);
                    },
                    'pause': function() {
                        self.set('videoState', Ndn_Analytics_ChartbeatStrategy.VideoState.STOPPED);
                    },
                    'videoEnd adEnd': function() {
                        self.set('videoState', Ndn_Analytics_ChartbeatStrategy.VideoState.COMPLETED);
                    }
                });
            });
            
            // If debugging is enabled, spit out information pertaining to this Chartbeat adapter every x-number of seconds
            if (_debug) { // && isOriginalStrategyInstance) {
                setInterval($.proxy(function() {
                    var debugParams = ['[Chartbeat Adapter]', this.get('embeddedProduct').getContainerElement().attr('id')];
                    
                    if (this.isReady()) {
                        var methodsToDebug = [
                            'isReady',
                            'getState',
                            'getTitle',
                            'getVideoPath',
                            'getContentType',
                            'getAdPosition',
                            'getTotalDuration',
                            'getCurrentPlayTime',
                            'getBitrate',
                            'getThumbnailPath',
                            'getPlayerType',
                            'getAutoplayType',
                            'getStrategyName',
                            'getViewStartTime',
                            'getSections',
                            'getAuthors'
                        ];
                        $.each(methodsToDebug, function() {
                            var methodName = this + '';
                            var methodResult = self[methodName]();
                            debugParams.push("\n\t." + methodName + '() ==', methodResult);
                        });
                    }
                    else {
                        debugParams.push("\n\t.isReady() ==", this.isReady());
                    }
                    
                    console.log.apply(console, debugParams);
                }, this), 2000);
            }
        },
        
        /**
         * Indicates if the video strategy is ready for pinging.
         * Note: Pings should only be sent after this reads true.
         * This method is REQUIRED
         * @return {boolean} The ready state of the strategy.
         */
        isReady: function() {
            return !!this.get('videoPlayer');
        },
        
        /**
         * Gets the human readable video title.
         * This method is REQUIRED
         * @return {string} The video title.
         */
        getTitle: function() {
            if (!this.isReady()) return '';
            
            return (this.get('videoPlayer').getCurrentVideoData() || {}).videoTitle;
        },
        
        /**
         * Per Chartbeat's interface documentation, this method should return "the playable video path if available"; 
         * however, returning a unique identifier such as the video's ID is also acceptable per the email thread 
         * titled "Chartbeat Video Documentation for Tomorrow's Call"
         * This method is REQUIRED
         * @return {string} The video ID of the current video playing
         */
        getVideoPath: function() {
            if (!this.isReady()) return '';
            
            return (this.get('videoPlayer').getCurrentVideoData() || {}).videoId;
        },
        
        /**
         * Gets the type of video playing.
         * This method is OPTIONAL
         * If you choose to exclude this method,
         * the default should be "ct", or leave it out entirely
         * @return {Ndn_Analytics_ChartbeatStrategy.ContentType} The type of content (ad or ct).
         */
        getContentType: function() {
            if (!this.isReady()) return;
            
            return this.get('contentTypeState') || Ndn_Analytics_ChartbeatStrategy.ContentType.CONTENT;
        },
        
        /**
         * Gets the ad position.
         * This method is OPTIONAL
         * If you choose to exclude this method,
         * the default should be "a1", or leave it out entirely
         * @return {Ndn_Analytics_ChartbeatStrategy.AdPosition|string} The ad position
         * from a1 (pre-roll), a2 (mid-roll), a3 (post-roll),
         * a4 (overlay), or a5 (special).
         */
        getAdPosition: function() {
            // Hardcoded to return that the ad currently playing (if there is one playing) is a pre-roll
            return Ndn_Analytics_ChartbeatStrategy.AdPosition.PREROLL;
        },
        
        /**
         * Gets the total duration of the video.
         * This method is REQUIRED
         * @return {number|undefined} The total duration time in milliseconds or undefined if currently unavailable
         */
        getTotalDuration: function() {
            if (!this.isReady()) return;
            
            var videoData = this.get('videoPlayer').getCurrentVideoData();
            return videoData && videoData['mediaContent'][0]['duration'] * 1000;
        },
        
        /**
         * Gets the current state of the video.
         * This method is REQUIRED
         * @return {string} The current video state (Ndn_Analytics_ChartbeatStrategy.VideoState)
         */
        getState: function() {
            // console.log('this ==', this);
            
            return this.get('videoState') || Ndn_Analytics_ChartbeatStrategy.VideoState.UNPLAYED;
        },
        
        /**
         * Gets the current play time of the video.
         * This method is REQUIRED
         * @return {number} The current play time in milliseconds.
         */
        getCurrentPlayTime: function() {
            try {
                return this.get('videoPlayer').getCurrentTime() * 1000;
            }
            catch (e) {}
            
            return 0;
        },
        
        /**
         * Gets the current bitrate of the video.
         * This method is OPTIONAL
         * If you choose to exclude this method,
         * the default should be "0", or leave it out entirely
         * @return {number} The current bitrate in kbps.
         */
        getBitrate: function() {
            // Unavailable at this time.
        },
        
        /**
         * Gets the thumbnail of the video.
         * This method is REQUIRED
         * @return {string} The [absolute] path to the thumbnail.
         */
        getThumbnailPath: function() {
            if (!this.isReady()) return '';
            
            return (this.get('videoPlayer').getCurrentVideoData() || {}).thumbnailUrl;
        },
        
        /**
         * Gets the video player type.
         * Used for embeded, popup, homepage, or whatever makes sense
         * in the context of the different players
         * This method is OPTIONAL
         * If you choose to exclude this method,
         * the default should be "", or leave it out entirely
         * @return {string} The name of the strategy
         */
        getPlayerType: function() {
            if (!this.isReady()) return '';
            
            var videoPlayer = this.get('videoPlayer'),
                widget = videoPlayer.get('widget'),
                widgetConfig = widget.get('config');
            
            var result = widgetConfig['splitVersion'] || widgetConfig['type'];
            
            return result == 'VideoPlayer/Default'
                ? 'VideoPlayer/Single'
                : result;
        },
        
        /**
         * Gets the autoplay type for this video.
         * This method is REQUIRED
         * @return {Ndn_Analytics_ChartbeatStrategy.AutoplayType|string|undefined} The autoplay type
         * for this video. Use 'unkn' for unknown/indeterminate, 'man' for manual play,
         * 'auto' for autoplay, and 'cont' for continuous (a video autoplaying
         * unprompted after another video has finished).
         */
        getAutoplayType: function() {
            if (!this.isReady()) return Ndn_Analytics_ChartbeatStrategy.AutoplayType.UNKNOWN;
            
            var videoPlayer = this.get('videoPlayer'),
                widget = videoPlayer.get('widget'),
                widgetConfig = widget.get('config');
            
            // if (_debug) console.log('this.get("params") ==', this.get('params'));
            
            if (!this.get('params')) {
                return Ndn_Analytics_ChartbeatStrategy.AutoplayType.UNKNOWN;
            }
            
            if (this.get('params').truViewCounter > 0) return Ndn_Analytics_ChartbeatStrategy.AutoplayType.CONTINUOUS;
            
            if (videoPlayer.isShowingRmm() || widgetConfig['autoPlay']) return Ndn_Analytics_ChartbeatStrategy.AutoplayType.AUTOPLAY;
            
            if (this.get('params').truViewCounter == 0 && !videoPlayer.isShowingRmm()) {
                return Ndn_Analytics_ChartbeatStrategy.AutoplayType.MANUAL;
            }
            
            return Ndn_Analytics_ChartbeatStrategy.AutoplayType.UNKNOWN;
        },
        
        /**
         * Short name of the strategy for tracking purposes
         * This should be unique for each player platform type
         * This method is REQUIRED
         * @return {string} Unique 2-3 char name of strategy
         */
        getStrategyName: function() {
            return _strategyName;
        },
        
        /**
         * Gets the time since start of viewing.
         * This method is REQUIRED
         * @return {number} The time since viewing started in milliseconds.
         */
        getViewStartTime: function() {
            // If no video load timestamp has been initialized yet, then no time has passed since the viewing started since the viewing hasn't started yet
            if (!this.get('videoLoadTimestamp')) return 0;
            
            return _getTime() - this.get('videoLoadTimestamp');
        },
        
        /**
         * Gets the optional section for this video.
         * This method is OPTIONAL
         * If you choose to exclude this method,
         * the default should be "", or leave it out entirely
         * @return {Array|undefined} The video section list. If
         * undefined then it will use the pages section when pinging.
         */
        getSections: function() {
            // Omitted.
        },
        
        /**
         * Gets the optional author for this video.
         * This method is OPTIONAL
         * If you choose to exclude this method, the default should be "", or leave it out entirely
         * @return {Array|undefined} The video author list. If undefined then it will use the pages author when pinging.
         */
        getAuthors: function() {
            if (!this.isReady()) return;
            
            var videoData = this.get('videoPlayer').getCurrentVideoData();
            if (videoData) return [videoData.provider.name];
        },
        
        isNdnPlayerSuiteProductChartbeatAdapter: function() {
            return true;
        }
    },
    {
        /**
         * Enum for the content type.
         * @enum {string}
         */
        ContentType: {
            AD: 'ad',
            CONTENT: 'ct'
        },
        
        /**
         * Enum for the ad position.
         * @enum {string}
         */
        AdPosition: {
            PREROLL: 'a1',
            MIDROLL: 'a2',
            POSTROLL: 'a3',
            OVERLAY: 'a4',
            SPECIAL: 'a5'
        },
        
        /**
         * Enum for the video state.
         * @enum {string}
         */
        VideoState: {
            UNPLAYED: 's1',
            PLAYED: 's2',
            STOPPED: 's3',
            COMPLETED: 's4'
        },
        
        /**
         * Enum for autoplay type.
         * @enum {string}
         */
        AutoplayType: {
            UNKNOWN: 'unkn',
            MANUAL: 'man',
            AUTOPLAY: 'auto',
            CONTINUOUS: 'cont'
        },
        
        isNdnEmbeddedProduct: function(adapter) {
            return typeof adapter == 'object' && typeof adapter.isNdnPlayerSuiteProduct == 'function' && adapter.isNdnPlayerSuiteProduct();
        },
        
        isNdnEmbeddedProductChartbeatAdapter: function(adapter) {
            return typeof adapter == 'object' && typeof adapter.isNdnPlayerSuiteProductChartbeatAdapter == 'function' && adapter.isNdnPlayerSuiteProductChartbeatAdapter();
        },
        
        /**
         * Verifies that the given adapter belongs to this strategy. Used for a greedy search of the matching strategy for a given element or object.
         * This method is REQUIRED
         * @param {Object} configurableDivElement A pointer to the player being tracked.
         * @return {boolean|undefined}
         */
        verify: function(adapter) {
            if (_debug) console.log('Ndn_Analytics_ChartbeatStrategy.verify(', arguments, ');');
            
            return this.isNdnEmbeddedProduct(adapter) || this.isNdnEmbeddedProductChartbeatAdapter(adapter);
        },
        
        /**
         * @param {Ndn_Widget} product
         * @param {String} uidConfigSetting The "uid" config setting to pass into the window._sf_async_config object for Chartbeat initialization
         */
        enableForNdnProduct: function(product, uidConfigSetting) {
            // Ensure that Chartbeat analytics is initialized
            _initializeChartbeatAnalytics(uidConfigSetting);

            // Allow Chartbeat to see the Chartbeat adapter created for the provided NDN Player Suite products
            (window['_cbv'] = window['_cbv'] || []).push(new Ndn_Analytics_ChartbeatStrategy(product));
        }
    });
    
    return Ndn_Analytics_ChartbeatStrategy;
});