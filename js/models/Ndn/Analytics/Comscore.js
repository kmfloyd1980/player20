/**
 * This must be defined globally for the comScore code to work
 */
// var _comscore = _comscore || [];

var _nw2e = _nw2e || [];

define([
    'jquery',
    'require'
], function(
	$,
	require
) {
	// TODO: Ensure that the _nw2e.requireConfig is actually already present in all cases -- THIS MAY ALREADY BE THE CASE.
	var requireConfig = {paths:{}};
	requireConfig.paths['comscoreLibrary'] = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon"; // The ".js" is not needed when using RequireJS
	var requireComscoreLibrary = _nw2e.requireConfig(requireConfig);
	
	// var _comscoreLibraryUrl = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon"; // The ".js" is not needed when using RequireJS
	
	return {
		/**
		 * @param {Object} Structured data concerning the comScore's publisher tag parameters. For example:
		 * <pre>{
		 * 	c1: 2,
		 * 	c2: "11112732", // Pre-populated unique ID for each client assigned by comScore ("11112732" is unique to NDN and should be used for 
		 * 	c4: String, // (Optional) The manually populated full page URL including query string variables
		 * 	c5: String, // (Optional) Alphanumeric value used for client specific custom classification
		 * 	c6: String, // (Optional) Alphanumeric value for customized aggregation to reflect sections or site centric advertising packages
		 * 	c15: String, // (Optional) Alphanumeric value for Publisher's own segment for the machine the content asset is being served to
		 * }</pre>
		 */
		execute: function(parameters) {
			// console.log('Ndn_Analytics_Comscore.execute(', arguments, ');');
			
			requireComscoreLibrary(['comscoreLibrary'], function() {
				COMSCORE.beacon(parameters);
			});
		},
		
        /**
         * @param {Object} params Structured data concerning the data needed for this Comscore call. Format:
         * <pre>{
         *    'distributorName': String, // The distributor's name
         *    'providerName': String, // The content provider's name
         *    'siteSection': String, // The "siteSection" config setting of the name
         * }</pre>
         */
        executeAdCall: function(params) {
            this.execute({
                c1: '1',
                c2: '11112732',
                c3: params.distributorName,
                c4: params.providerName + ' 09',
                c5: '09',
                c6: params.siteSection,
                c10: params.producerCategory
            });
        },
        
        /**
         * @param {Object} params Structured data concerning the data needed for this Comscore call. Format:
         * <pre>{
         *    'distributorName': String, // The distributor's name
         *    'providerName': String, // The content provider's name
         *    'siteSection': String, // The "siteSection" config setting of the name
         * }</pre>
         */
        executeVideoStartCall: function(params) {
            this.execute({
                c1: '1',
                c2: '11112732',
                c3: params.distributorName,
                c4: params.providerName + ' ' + params.mediaAnalyticsC5Value,
                c5: params.mediaAnalyticsC5Value,
                c6: params.siteSection,
                c10: params.producerCategory
            });
        }
	};
});