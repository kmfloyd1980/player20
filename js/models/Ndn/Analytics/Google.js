var _gaq = _gaq || [];

define(['jquery'], function($) {
	/**
	 * List of domains that should be tracked using Google Analytics, for example:
	 * <pre>[
	 * 	'chicagotribune.com',
	 * 	'local.newsinc.com'
	 * ]</pre>
	 * @var {Array<string>}
	 */
	var _trackedDomains = [
	    'local.newsinc.com',
		'dev.assets.newsinc.com'
	];
	
	/**
	 * The Google Account ID
	 * @var {String}
	 */
	var _accountId = 'UA-35606174-6';
	
	/**
	 * Whether the current webpage should be tracked using Google Analytics
	 * @var {Boolean}
	 */
	var _pageShouldBeTracked = (function() {
		var result = false;
		$.each(_trackedDomains, function() {
			var check = new RegExp('([.]|^)' + this + '$');
			if (check.test(window.location.host)) {
				result = true;
				return false;
			}
		});
		
		return result;
	})();
	
	return {
		initialize: function(Ndn_Analytics) {
			Ndn_Analytics.on({
				'pageView': function() {
					if (_pageShouldBeTracked) {
						_gaq.push(['ndn._setAccount', _accountId]);
						_gaq.push(['ndn._setAllowLinker', true]);
						_gaq.push(['ndn._trackPageview']);
						
						(function() {
							var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
							ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
						})();
					}
				},
				
				'*/widgetEmbedStart': function(event) {
					var widget = event.system.widget;
					
					if (_pageShouldBeTracked) {
						_gaq.push(['ndn._setCustomVar', 1, 'WID', widget.get('config')['widgetId'], 3]);
						_gaq.push(['ndn._trackEvent', 'Embed', 'Widget Initiated']);
					}
				},
				
				'*/widgetEmbedEnd': function(event) {
					var widget = event.system.widget;
					
					if (_pageShouldBeTracked) {
						_gaq.push(['ndn._setCustomVar', 2, 'PartnerID', widget.get('config')['trackingGroup'], 3]);
						_gaq.push(['ndn._setCustomVar', 3, 'Player Behavior', widget.get('config')['autoPlay'] ? 'AS' : 'CTP', 3]);
						_gaq.push(['ndn._setCustomVar', 4, 'SiteSection', widget.get('config')['siteSection'], 3]);
						_gaq.push(['ndn._setCustomVar', 5, 'PlayerProduct', widget.getWidgetTypeId() + '', 3]);
						_gaq.push(['ndn._trackEvent', 'Embed', 'Widget Loaded']);
					}
				}
			});
		}
	};
});