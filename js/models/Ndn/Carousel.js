define(['jquery'], function($) {
    return {
        loadSlidePlaceholderImages: function(containerElement, slideIndex, itemsPerSlide, nestedListSelector) {
            // console.log('Ndn_Carousel.loadSlidePlaceholderImages(', arguments, ');');
            
            var firstItemIndexInSlide = slideIndex * itemsPerSlide,
                lastItemIndexInSlider = firstItemIndexInSlide + itemsPerSlide - 1,
                itemElementsSelector = nestedListSelector
                    ? 'li:not(".ndn_carousel-clone") ' + nestedListSelector + ' li'
                    : 'li.ndn_selectableVideo:not(".ndn_carousel-clone")',
                itemElementsToLoadImagesFor = containerElement.find(itemElementsSelector).filter(':eq(' + firstItemIndexInSlide + '), :gt(' + firstItemIndexInSlide + '):lt(' + lastItemIndexInSlider + ')');
            
            itemElementsToLoadImagesFor.each(function() {
                var transferableAttributes = ['src', 'alt', 'title', 'class'];
                $(this).find('.ndn_imagePlaceholder').each(function() {
                    var placeholderElement = $(this);
                    
                    // Create the image element's HTML that will replace the placeholder element
                    var imageElementHtml = '<img ';
                    $.each(transferableAttributes, function() {
                        var attributeName = this + '';
                        
                        if (placeholderElement.attr('data-' + attributeName)) {
                            imageElementHtml += attributeName + '="' + placeholderElement.attr('data-' + attributeName) + '" ';
                        }
                    });
                    imageElementHtml += '/>';
                    
                    $(this).replaceWith(imageElementHtml);
                });
            });
        }
    };
});