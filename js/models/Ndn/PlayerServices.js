define([
	 	'jquery',
	 	'underscore',
	 	'require',
	 	'models/Ndn/PlayerServices/Cache',
	 	'models/Ndn/Utils/UrlParser',
	 	'models/Ndn/App'
 	], function($, _, require, Ndn_PlayerServices_Cache, Ndn_Utils_UrlParser, Ndn_App) {
		var _getPlayerServicesBaseUrl = (function() {
			var defaultEnv = '';
			var environment = (Ndn_Utils_UrlParser.getUrlVar('ndn_playerServicesEnv') ? Ndn_Utils_UrlParser.getUrlVar('ndn_playerServicesEnv') : defaultEnv).replace(/[^A-Za-z0-9-_]/g, ''),
				result = '//' + (environment ? environment + '-' : '') + 'lps.newsinc.com';
			
			return function() {
				return result;
			};
		})();
	
		/**
		 * @param {Ndn_Widget} widget The widget requesting a response from player services
		 * @param {int} widgetId The widget's "widgetId" config setting
		 * @param {int} trackingGroup The widget's "trackingGroup" config setting
		 * @param {int} [playlistId] The widget's "playlistId" config setting
		 * @param {int} [videoId] The widget's "videoId" config setting
		 * @param {int} [limit] The maximum number of playlist items to return in the ajax call
		 * @param {int} [providerId] The widget's "providerId" config setting
		 * @param {String} [providerStoryId] The widget's "providerStoryId" config setting
		 * @return {String} The ajax URL used to fetch the player data formatted like so: http://lps.newsinc.com/player/show/{trackingGroup}/{widgetId}/{playlistId}/{videoId}/3/{limit}
		 */
		var _getPlayerDataAjaxUrl = function(widget, widgetId, trackingGroup, playlistId, videoId, limit, providerId, providerStoryId) {
			var urlSuffix = '&' +
				'insid=' + (Ndn_App.getPageViewSessionId() || '') + '&' +
				'uut=' + (Ndn_App.getUniqueUserId() || '') + '&' +
				'embedIndex=' + widget.get('embedIndex') + '&' +
				'ver=' + Ndn_App.getBuild() + '&' +
				'benv=' + Ndn_App.getEnv() + '&' +
				'embedOriginUrl=' + encodeURIComponent(Ndn_App.getEmbedOriginUrl());
			
			if (providerId && providerStoryId) {
				return _getPlayerServicesBaseUrl() + '/player/show/' +
					providerId + '/' + providerStoryId + '?' +
					'trackingGroupId=' + (trackingGroup ? trackingGroup : '0') + '&' +
					'widgetId=' + (widgetId ? widgetId : '0') + '&' + 
					'playlistId=' + (playlistId ? playlistId : '0') + 
					urlSuffix;
			}
			else {
				if (limit && limit <= 40) {
					// Limit the playlist items limit to either 1, 15, or 40
					if (limit != 1) {
						limit = limit > 15 ? 40 : 15;
					}
				}
				else {
					// Otherwise don't limit the number of playlist items returned in the response
					limit = null;
				}
				
				return _getPlayerServicesBaseUrl() + '/player/show/' +
					(trackingGroup || '0') + '/' +
					(widgetId || '0') + '/' +
					(playlistId || '0') + '/' +
					(videoId || '0') + '/3' +
					(limit ? '/' + limit : '') + '?d=' + new Date().getTime() +
					urlSuffix;
			}
		};
		
		/**
		 * @param {String} assetType The "AssetMimeType" property from the player services response (or the following other recognized strings: "producerLogo", "stillFrame", or "thumbnail") that describes what a specific asset is used for 
		 * @return {String} The base URL used to create the complete URL of an asset of the specified type
		 */
		var _getAssetBaseUrl = function(assetType) {
			var assetBaseTypeUrls = {
				'producerLogo':['http://assets.newsinc.com', 'https://assets-s.newsinc.com'],
				'stillFrame':['http://content-img.newsinc.com', 'https://content-img-s.newsinc.com'],
				'thumbnail':['http://content-img.newsinc.com', 'https://content-img-s.newsinc.com'],
				'video/mp4':['http://content-mp4.newsinc.com', 'https://content-s.newsinc.com'],
				'application/x-mpeg':['http://ndn_mp4-vh.akamaihd.net', 'https://ndn_mp4-vh.akamaihd.net'],
				'application/x-mpegURL':['http://ndnmedia-vh.akamaihd.net', 'https://ndnmedia-vh.akamaihd.net'],
				'video/f4m':['http://ndnmedia-vh.akamaihd.net', 'https://ndnmedia-vh.akamaihd.net'],
				'video/x-flv':['rtmp://cp98516.edgefcs.net', 'rtmp://cp98516.edgefcs.net'],
				'video/progressive':['http://ndnmedia-a.akamaihd.net', 'https://ndnmedia-a.akamaihd.net']
			};
			
			// Use the provided asset type and whether the browser is currently using SSL to determine the base URL
			if (typeof assetBaseTypeUrls[assetType] != 'undefined') {
				return assetBaseTypeUrls[assetType][window.location.protocol == 'http:' ? 0 : 1];
			}
		};
		
		/**
		 * @param {Object} videoData The video data provided from our legacy player services (lt.ndnps.newsinc.com)
		 * @return {Object} Structured data that the current video player can understand. Format:
		 * <pre>{ // Structured data concerning a video
		 * 	'videoId': {int}, // This video's database id
		 * 	'videoTitle': {String}, // This video's title
		 * 	'description': {String}, // This video's description
		 * 	'antiKeywords': {String}, // A comma-delimited list of keywords used for anti-targeting when ads are served with this content (disclaimer: there is no filtering on the data we recieve here)
		 * 	'publishedOn': {String}, // The date when this content was published
		 * 	'mediaContent': [ // A list of content files that may be used to serve this video's content
		 * 		... // TODO: Document this
		 * 	],
		 * 	'producer': { ... } // @deprecated Use the "provider" property described below instead
		 * 	'provider': { // Structured data concerning the content provider partner that provided this content 
		 * 		'id': {int}, // The provider's database id
		 * 		'name': {String}, // The provider's name
		 * 		'category': {String}, // The provider's category
		 * 		'logoUrl': {String}, // The URL to this provider's logo
		 * 	},
		 * 	'settings': { // Structured data concerning this video's settings
		 * 		... // TODO: Document this
		 * 	},
		 * }</pre>
		 */
		var _playerServicesVideoDataConversion = function(videoData) {
			var result = {
				'videoId': videoData['ContentID'],
				'videoTitle': videoData['Name'],
				'description': videoData['Description'],
				'antiKeywords': videoData['AKW'],
				'publishedOn': videoData['PubDate'],
				'mediaContent': [],
				
				// TODO: Refactor this out
				// @deprecated Use 'provider' property instead
				'producer': {
					'trackingGroup': videoData['ContentPartnerID'],
					'name': videoData['ProducerName'],
					'category': videoData['ProducerCategory'],
					'logoUrl': _getAssetBaseUrl('producerLogo')+videoData['ProducerLogo']
				},

				'provider': {
					'id': videoData['CPID'],
					'trackingGroup': videoData['ContentPartnerID'],
					'name': videoData['ProducerName'], 
					'category': videoData['ProducerCategory'],
					'logoUrl': _getAssetBaseUrl('producerLogo')+videoData['ProducerLogo']
				},
				'settings': {
					'share': {
						'embedCode': {
							'enabled': !!videoData['EmbedCodeAccessible']
						}
					}
				}
			};
			
			// Create a temporary unsorted array for src assets
			var tmpSrcAssets = [];
			for (var i = 0; i < videoData['Assets'].length; i++) {
				var assetData = videoData['Assets'][i];
				
				// @TODO: A new PS response specifically for 2.0 is being worked up; so this will change when that rolls around
				if (assetData['AssetType'] == 'src') {
					tmpSrcAssets.push(assetData);
				}
				else if (assetData['AssetType'] == 'stillFrame') {
					result['stillImageUrl'] = _getAssetBaseUrl('stillFrame') + assetData['AssetLocation'];
				}
				else if (assetData['AssetType'] == 'thumbnail') {
					result['thumbnailUrl'] = _getAssetBaseUrl('thumbnail') + assetData['AssetLocation'];
				}
			}
			
			// Sort/map/push video assets
			var sortedSrcAssets = _.sortBy(tmpSrcAssets, function(item){
				return item.AssetSortOrder;
			});
			
			/**
			 * @param {Object} item Structured data concerning a described asset within the player services response
			 */
			var getAssetUrl = (function() {
				/**
				 * The asset path to the PDL video provided
				 * @var {String}
				 */
				var pdlAssetPath;
				for (var i = 0; i < sortedSrcAssets.length; i++) {	
					if (sortedSrcAssets[i].AssetMimeType == 'video/progressive') {
						pdlAssetPath = sortedSrcAssets[i].AssetLocation;
					}
				}
				
				return function(item) {
					if (item['AssetMimeType'] == 'video/f4m' && Ndn_App.isOnSsl()) {
						// Until the SSL domain is set up for "video/f4m" assets, use the URL to the PDL video instead of HDS when on SSL
						return _getAssetBaseUrl('video/progressive') + pdlAssetPath;
					}
					else {
						return _getAssetBaseUrl(item['AssetMimeType']) + item['AssetLocation'];
					}
				};
			})();
			
			result['mediaContent'] = _.map(sortedSrcAssets, function(item) {
				return {
					'type': item['AssetMimeType'] != 'video/progressive' ? item['AssetMimeType'] : 'video/mp4',
					'medium': 'video',
					'duration': item['Duration'],
					'url': getAssetUrl(item)
				};
			});

			// Append pretty duration based on first src item
			var prettyVideoDuration = parseInt(result['mediaContent'][0]['duration'],10);//parse into something we can use
				prettyVideoDuration = prettyVideoDuration < 0 ? 0 : prettyVideoDuration; //if duration is 0 show 0. 
			var minutes = Math.floor(prettyVideoDuration / 60); //convert duration in minutes
			var hours = minutes > 59 ? hours = Math.floor(minutes / 60): hours; //if an hour or longer convert min to hours
			var seconds = prettyVideoDuration % 60; // returns remainder which is how many seconds there are +- hours			
				seconds = seconds < 10 ? "0"+seconds : seconds;	//if seconds are less than 10 then add in a preceeding zero		
			
			
			if (hours > 0 && hours != "undefined"){	// if more than an hour
				minutes = minutes % 60;
				minutes = minutes < 10 ? "0"+minutes : minutes;			
				prettyVideoDuration = hours+":"+minutes+":"+seconds;//add hour to prettyVideoDuration								
			}
			else{
				prettyVideoDuration = minutes+":"+seconds;//else just show min:sec
			}
			
			result['displayDuration'] = prettyVideoDuration;
			return result;
		};
		
		/**
		 * @param {Object} playlistData Structured data concerning a video playlist. Format:
		 * <pre>[
		 * 	{ // Structured data concerning an available playlist
		 * 		'Contents': [ // A list of structured data concerning the videos belonging to this playlist
		 * 			{ // Structured data concerning a video belonging to this playlist
		 * 				'Assets': [ // A list of assets related to this video
		 * 					{ // Structured data concerning an asset related to this video
		 * 						'AssetLocation': string, // The absolute URL to this asset
		 * 						'AssetType': string, // An identifier that describes what kind of asset this is and how it relates to this video
		 * 						'Duration': float, // The duration (maybe described in seconds?) of this asset only if this asset's "AssetType" is "src", null otherwise
		 * 					},
		 * 					... // Any other assets related to this video
		 * 				],
		 * 				'ContentID': int, // The database id of this video
		 * 				'ContentOrder': int, // TODO: Implement this as the order of the content video
		 * 				'ContentPartnerID': int, // The partner id of this video's content provider // *IMPORTANT NOTE:* I believe this is in fact a "partner id" (internal to our database) not a "tracking group" or "CPID"!!!
		 * 				'Description': string, // This video's description
		 * 				'Keyword': null, // Comma-separated list of keywords that may be tied to the video (or null) -- seldom used.
		 * 				'Name': string, // This video's name
		 * 				'PLaylistID': int, // The current playlist's id // IMPORTANT NOTE: Notice that it is in fact "PLaylistID" and not "PlaylistID"!
		 * 				'ProducerCategory': string, // The producer's category // TODO: What is this used for? // TODO: Find out what this is for from Ben
		 * 				'ProducerLogo': string, // The absolute URL for the producer's logo
		 * 				'ProducerName': string, // The producer's name, ie. "AP"
		 * 				'ProducerNameAlt': string, // The producer's alternate name, ie. "ap"
		 * 				'PubDate': string, // The date this video was published, ie. "/Date(1363203180000-0400)/"
		 * 				'VideoGroupName': null, // TODO: Is "null" the only possible value for this property? // 
		 * 			},
		 * 			... // Any other videos belonging to this playlist
		 * 		],
		 * 		'PlaylistOrder': int, // The playlist's order
		 * 		'Title': string, // The playlist's title
		 * 		'playlistid': int, // The playlist's id
		 * 	},
		 * 	... // Any other available playlists
		 * ]</pre>
		 * @return {!Object} Structured data concerning a video player's associated playlist data. Format:
		 * <pre>{
		 * 	'playlistId': {number},
		 * 	'items': [
		 * 		// TODO: Document this 
		 * 	] 
		 * }</pre>
		 * @param {int} playlistItemsLimit The maximum number of playlist items allowed to be returned
		 */
		var _playerServicesPlaylistsDataConversion = function(receivedPlaylistsData, playlistItemsLimit) {
			// console.log('_playerServicesPlaylistsDataConversion(', arguments, ');');
			
			var result = [];

			_.each(receivedPlaylistsData, function(receivedPlaylistData) {
				if (receivedPlaylistData['playlistid'] !== 9999999) {
					var playlistData = {
						'playlistId': receivedPlaylistData['playlistid'],
						'title': receivedPlaylistData['Title'],
						'items': []
					};
					
					var playlistItemsData = _.sortBy(receivedPlaylistData['Contents'], function(videoData) {
						return videoData.ContentOrder;
					});
					
					_.each(playlistItemsData, function(videoData) {
						playlistData['items'].push(_playerServicesVideoDataConversion(videoData));
					});

					// TODO: Temporary work-around due to the exchange between the QA and production player services, where QA limits the playlist items and production does not
					playlistData.items = _.first(playlistData.items, playlistItemsLimit);

					result.push(playlistData);
				}
			});
			
			return result;
		};
		
		/**
		 * @param {!Object} playerData Sample expected format:
		 * <pre>{
		 * 	"AllowFullScreen": 1,
		 * 	"ContinuousPlay": 6,
		 * 	"DeliverySettings": [
		 * 		{
		 * 			"Name": "comscoreOn", // Analytics (not ads targeting). Comscore Monthly: In the top 10 of video views (newsinc).
		 * 			"Value": "true"
		 * 		}, {
		 *          "Name": "videoStartTime", // How far into the video we start into the video
		 *          "Value": "3"
		 *      }, {
		 *          "Name": "videoSegmentLength", // The length of the video segment that is the "teaser", "user engagement", "video preview"
		 *          "Value": "6"
		 *      }, {
		 *          "Name": "fadeDownDuration", // After the {{slateAfterVideoHold}} situation, fades down into a black screen
		 *          "Value": "1"
		 *      }, {
		 *          "Name": "slateAfterVideoHold", // After the {{videoSegmentLength}}-second video, it holds as a black screen
		 *          "Value": "1"
		 *      }, {
		 *          "Name": "deliverGA", // Analytics (Google Analytics)
		 *          "Value": "false"
		 *      }, {
		 *          "Name": "EnableConviva", // Analytics 
		 *          "Value": "false"
		 *      }, {
		 *          "Name": "EnableAMA", // Analytics, Akamai Media Analytics
		 *          "Value": "false"
		 *      }, {
		 *          "Name": "GoogleExtURL", // aka. "descriptionUrl" Sent to Auditude in user data under variable name "descriptionUrl=", and means that the "external_url" (the URL of page embedding our widget) gets sent to Auditude and is double-encoded (@see players_core)
		 *          "Value": "true"
		 *      }, {
		 *          "Name": "EnableLegalos", // Ads Targeting
		 *          "Value": "false"
		 *      }, {
		 *          "Name": "EnableCollective", // Ads Targeting
		 *          "Value": "false"
         *      }, {
         *          "Name": "muteInterval", // The mute interval setting to control how often we should repeatedly mute the player so that ad creatives do not unmute it
         *          "Value": {Number} // "0" indicates that no mute interval should be used, whereas any number greater than 0 indicates the frequency of how often a muted player should be muted, measured in number of milliseconds 
         *      }
		 *  ],
		 *  "DisableAds": {boolean}, // Whether ads are disabled
		 *  "DisableSingleEmbed": false, // Whether the "Copy Embed" button is specified as disabled by "wid"
		 *  "DisableSocialNetworking": false, // Whether *email* sharing is disabled (TODO: Verify)
		 *  "DisableVideoPlayOnLoad": true, // Whether autoplay is disabled
		 *  "DistributorName": "NDN Basic Accounts", // The name of the distributor (TODO: Which distributor?) -- may be ignored.
		 *  "DistributorNameAlt": "ndnbasicaccounts", // Not used.
		 *  "Height": 320, // Height of the player (may be used in single embed), could use as fallback
		 *  "LandingURL": "http:\/\/landing.newsinc.com\/shared\/video.html", // @deprecated Not used 
		 *  "LandingUrl_Hulu": "http:\/\/www.newsinc.com", // @deprecated Not used.
		 *  "LauncherID": 2, // TODO: This is possibly the same as "WidgetID" described below.
		 *  "LogoURL169": "http:\/\/assets.newsinc.com\/partnerlogo169.jpg", // 'logoUrls': { '16x9': 'sgsgdasgdsg', '4x3': 'asdgiosdg' }
		 *  "LogoURL43": "http:\/\/assets.newsinc.com\/partnerlogo.jpg",
		 *  "LogoURLDisplay": "no logo", // TODO: Get feedback from Ben on what the possible return values for this are
		 *  "Playlists": {Array.<Object>}, // @see above function
		 *	"ProducerCategory": {String}, // TODO: Find out what this means, example usage: "News Distribution Network, Inc." // Should be moved to the individual video's data // May not be useful anymore...
		 *	"SiteSectionName": {String}, // The site section to track against, ie. "ndn" // sub ("subsection"), sec ("section"), oth ("other"), concatenated by "_" -- "ndn" is probably just the default "sitesection"
		 *	"TrackingGroup": 10557, // Distribution Partner Id ("dpid")
		 *	"WidgetID": 2, // "wid=" value in the query string
		 *	"Width": 425, // The width of the player (may be used in single embed), could use as fallback
		 * 	"ZoneID": 50974 // This is the "aud_zone_id" Auditude data variable
		 * }</pre>
		 * @return {!Object} The translated/converted data that our video player model will need to understand. Format:
		 * <pre>{
		 * 	'playlists': { // An associative array of playlist ids mapping to the respective playlist's structured data
		 * 		{number}: { // A playlist's database id => the playlist's structured data
		 * 			... // @see The return value of the _playerServicesPlaylistsDataConversion() function
		 * 		},
		 * 		... // Any other playlists
		 * 	},
		 * 	'associatedPlaylists': [
		 * 		{ // Structured data describing a playlist that is associated with this video player's widget
		 * 			'id': {int}, // The playlist's database id
		 * 			'label': {String} // The playlist's label/title
		 * 		},
		 * 		... // Any other playlists associated with this video player's widget
		 * 	],
		 * 	'defaultWidgetConfig': { // Structured data concerning the default widget config settings
		 * 		'siteSection': {String}, // The site section used for analytics/tracking
		 * 		'autoPlay': {Boolean}, // Whether the player automatically begins playing without the user engaging the widget
		 * 	},
		 * 	'settings': {
         *      'analytics': { // Structured data concerning analytics enabled for this product
         *          'chartbeat': {
         *            'enabled': {Boolean}, // Whether Chartbeat analytics are enabled
         *          },
         *          'google': {
         *            'enabled': {Boolean}, // Whether Google analytics is enabled
         *          },
         *      },
         *      'behavior': {
         *          'provided': { // Structured data concerning the data provided by player services, which is translated later into reliable settings data after Ndn_Widget.processSettings() method is invoked on this data
         *              'ptr': {
         *                  'enabled': Boolean, // Whether the PTR behavior is enabled
         *                  'previewLength': Integer, // The length of the initial preview for when PTR is enabled
         *                  'playsInIframe': Boolean, // Whether the PTR behavior will always be executed when embedded within an iframe
         *              },
         *              'rmm': {
         *                  'continuousPlay': {Integer}, // RMM continuous play. Indicates how many RMM 6-second bumper videos will continuously play, which will only be respected only if the RMM continuous play percentage (see rmmContinuousPlayPercentage property below) enables this functionality altogether.
         *                  'continuousPlayPercentage': {Integer}, // RMM continuous play percentage. The percentage of likeliness that the RMM continuous play behavior will be enabled at all. For example "40" will indicate that it has a 40% chance of being enabled.
         *              },
         *              'float': {
         *                  'enabled': Boolean, // Whether the floating behavior (that allows this product to get placed in a float container when it goes out of the browser's view) is enabled for this product
         *                  'initialPosition': String, // The initial position of the floating container on a page when visible; recognized values: "" (when not enabled), "top right", "top left", "bottom left", "bottom right"
         *              }
         *          }
         *      },
         *      'rmm': {
         *          'provided': {
         *              // @deprecated Use result.settings.behavior.provided.rmm instead, which is documented above
         *          }
         *      },
		 *      'muteInterval': {Number}, // The frequency of how often a muted player should be muted measured in number of milliseconds; a value of 0 here indicates that there should be no mute interval to mute an already muted player
		 * 		'launcher': { // Structured data concerning how this product launches additional players
		 * 			'behavior': {String}, // An identifier describing how this product should launch additional players; recognized values: 'popup', 'studio', 'modal'
		 * 			'landingPageUrl': {String}, // The URL of the landing page that this product launches additional players onto (this is only used if the specified launcher behavior includes opening a new URL)
		 * 		},
		 * 		'share': { // Structured data concerning how the video player's current content may be shared
		 * 			'enabled': {Boolean}, // Whether the ability to share anything is enabled
		 * 			'email': {
		 * 				'enabled': {Boolean}, // Whether sharing a video's content via email is enabled
		 * 			},
		 * 			'socialNetworks': {
		 * 				'enabled': {Boolean}, // Whether sharing a video's content via social networks is enabled
		 * 			},
		 * 			'directLink': {
		 * 				'landingPageUrl': {String}, // The URL to the landing page that this widget may share content to for this widget's "Direct Link", which is copiable from the share window
		 * 			}
		 * 		},
		 * 		'ads': { // Structured data concerning how ads are provided for this widget
		 * 			'enabled': {Boolean}, // Whether ads are enabled for this widget
		 * 			'server': {String}, // The label of the server responsible for serving ads
		 * 		},
		 * 		'auditude': { // Structured data concerning the settings needed to interact with Auditude appropriately
		 * 			'trackingGroupAuditudeId': {String}, // Auditude's database's id that identifies the tracking group associated with this video player's widget
		 * 			'enableGoogleAds': {Boolean}, // Whether Google Ads is used with Auditude, which determines whether the 'descriptionUrl' parameter is sent when making an Auditude call
		 * 		}
		 * 	},
		 * 	'distributor': { // Structured data concerning this widget's distributor
		 * 		'name': {String}, // The distributor's name
		 * 	},
		 * 	
		 * 	'zoneId': {String}, // @deprecated (Use 'settings.auditude.trackingGroupAuditudeId' instead)
		 * 	'siteSection': {String}, // @deprecated (Use 'defaultWidgetConfig.siteSection' instead)
		 * }</pre>
		 * @param {int} playlistItemsLimit The maximum number of playlist items that are allowed
		 */
		var _playerServicesPlayerDataConversion = function(playerData, playlistItemsLimit) {
			
			var directLinkLandingPageUrl = (!playerData['LandingURL'] || (playerData['LandingURL'] == 'http://landing.newsinc.com/shared/video.html'))
				? 'http://launch.newsinc.com/share.html'
				: playerData['LandingURL'];
			
			var launcherBehavior = (function() {
				var result = playerData['LandingPageAllowed'] ? 'studio' : 'popup';
				
				// Only allow the "modal" behavior to override the "popup" behavior (not the "studio" behavior),
				// and only if this product is not currently embedded within an iframe
				if (result != 'studio' && !Ndn_App.isEmbeddedByIframe()) {
					$.each(playerData['DeliverySettings'], function() {
						if (this['Name'] == 'launchModal' && this['Value'] == 'true' || Ndn_App.getSetting('launchModalEmbeds')) {
							result = 'modal';
							return false;
						}
					});
				}
				
				return result;
			})();
			
			var launcherLandingPageUrl = launcherBehavior == 'studio'
				? directLinkLandingPageUrl
				: 'http://' + Ndn_App.getAppDomain() + '/';
			
			/**
			 * @param {String} deliverySettingName The name of the "DeliverySetting" property returned by the player services response
			 * @return {String} The value of the specified delivery setting
			 */
			var getDeliverySetting = function(deliverySettingName) {
			    var result;
                $.each(playerData['DeliverySettings'], function() {
                    if (this['Name'] == deliverySettingName) {
                        result = this['Value'];
                        return false;
                    }
                });
                return result;
			};
			
			/**
			 * @param {String} deliverySettingName The name of the "DeliverySetting" property returned by the player services response
			 * @param {Integer} defaultValue The value to default to in the event that the delivery setting specified was not provided in the player services response
			 */
			var getDeliverySettingInteger = function(deliverySettingName, defaultValue) {
			    var result = getDeliverySetting(deliverySettingName);
			    
			    if (typeof result != 'undefined') return parseInt(result);
			    else return defaultValue;
			};
            
            /**
             * @param {String} deliverySettingName The name of the "DeliverySetting" property returned by the player services response
             * @param {Boolean} defaultValue The value to default to in the event that the delivery setting specified was not provided in the player services response
             * @return {Boolean} The boolean value of the specified delivery setting
             */
            var getDeliverySettingBoolean = function(deliverySettingName, defaultValue) {
                var result = getDeliverySetting(deliverySettingName);
                
                if (typeof result == 'string') return result.toLowerCase() == 'true' || result.toLowerCase() == '1';
                else return defaultValue;
            };
			
			var providedRmmSettings = {
                'continuousPlay': parseInt(Ndn_App.getSetting('rmmContinuousPlay')) || (getDeliverySetting('rmm_cp') && parseInt(getDeliverySetting('rmm_cp'))) || 0,
                'continuousPlayPercentage': parseInt(Ndn_App.getSetting('rmmContinuousPlayPercentage')) || getDeliverySetting('rmm_cp_percent') && parseInt(getDeliverySetting('rmm_cp_percent'))
            };
			
			var result = {
				'playlists': {},
				'associatedPlaylists': [],
				'defaultWidgetConfig': {
					'widgetId': playerData.WidgetID,
					'trackingGroup': playerData.TrackingGroup,
					'type': 'VideoPlayer/Default',
					'siteSection': playerData.SiteSectionName,
					'autoPlay': !playerData.DisableVideoPlayOnLoad,
					'continuousPlay': playerData.ContinuousPlay,
					'videoId': '',
					'playlistId': '', // This gets populated later within this method
					'adsEnabled': !playerData['DisableAds'],
					'rmmSoundOn': false,
					'playOnMouseover': !!playerData['PlayOnMouseover'] || false,
                    'playOnInView': false,
					'splitVersion': playerData['Ptr'] ? 'VideoPlayer/PTR' : '' // Additional logic is handled in the Ndn_Widget_Config.applyConfigDefaults() method // TODO: Move mentioned logic into the player services response
				},
				'ptr': playerData['Ptr'] || 0, // TODO: Remove this since it has been refactored to be "splitVersion" of the "defaultWidgetConfig" values returned
				'launchToStudio': {
					'enabled': playerData['LandingPageAllowed'],
					'url'    : playerData['LandingURL']
				},
				'settings': {
	                'analytics': {
	                    'chartbeat': {
	                        'enabled': Ndn_App.getSetting('chartbeatAnalytics') || getDeliverySetting('analytics_chartbeat') == 'true',
                            'uid': parseInt(Ndn_App.getSetting('chartbeatAnalyticsUid') || getDeliverySetting('analytics_chartbeat_uid'))
	                    },
	                    'google': {
	                        'enabled': Ndn_App.getSetting('googleAnalytics') || getDeliverySetting('deliverGA') == 'true'
	                    },
	                    'comscore': {
	                        'vme': {
	                            'enabled': Ndn_App.getSetting('forceComscoreVme') || getDeliverySetting('comscore_vme') == 'true'
	                        }
	                    }
	                },
	                'behavior': {
	                    'provided': {
	                        'ptr': {
	                            'enabled': !!playerData['Ptr'],
	                            'previewLength': getDeliverySettingInteger('ptr_preview_length', 30),
	                            'playsInIframe': getDeliverySetting('ptr_iframe') == 'true'
	                        },
	                        'rmm': providedRmmSettings,
	                        'float': {
	                            'enabled': !!getDeliverySetting('float_player'),
	                            'initialPosition': getDeliverySetting('float_player')
	                        },
	                        'playOnInViewToggle': {
	                            'enabled': Ndn_App.getSetting('playOnInViewToggle') || getDeliverySettingBoolean('playOnInViewToggle', false)
	                        }
	                    }
	                },
	                'rmm': {
                        'provided': providedRmmSettings
	                },
				    'muteInterval': (function() {
				        var result;
				        $.each(playerData['DeliverySettings'], function() {
	                        if (this['Name'] == 'muteInterval') {
	                            result = parseInt(this['Value']);
	                            return false;
	                        }
	                    });
				        return result;
				    })(),
					'launcher': {
						'behavior': launcherBehavior,
						'landingPageUrl': launcherLandingPageUrl
					},
					'share': {
						'enabled': !playerData['DisableSocialNetworking'] && !playerData['DisableSingleEmbed'],
						
						'email': {
							'enabled': !playerData['DisableSocialNetworking']
						},
						'socialNetworks': {
							'enabled': !playerData['DisableSingleEmbed']
						},
						'directLink': {
							'landingPageUrl': directLinkLandingPageUrl
						}
					},
					'ads': {
						'enabled': !playerData['DisableAds'],
						'server': playerData['AdServer']
					},
					'dfp': (function() {
						var result;
						
						function getAdTrackingGroupNumber(adTrackingGroupId) {
							var matches;
							if (matches = adTrackingGroupId.match(/_(\d+)$/)) {
								return matches[1];
							}
						}
						
						var isSafeViralOverride = false;
						
						if (playerData.Dfp) {
							if (playerData.Dfp.Substituted) {
							    // Indicate that this is a Safe Viral override
							    isSafeViralOverride = true;
							    
								result = {
									'accountNumber': playerData.Dfp.Substituted.Account,
									'adOwnershipGroupId': playerData.Dfp.Substituted.OwnershipGroup,
									'adTrackingGroupId': playerData.Dfp.Substituted.TrackingGroup
								};
								
								if (result['adTrackingGroupId'] != playerData.Dfp.Direct.TrackingGroup) {
									result['unused'] = {
										'adTrackingGroupId': playerData.Dfp.Direct.TrackingGroup,
										'adTrackingGroupNumber': getAdTrackingGroupNumber(playerData.Dfp.Direct.TrackingGroup) || '',
										'reason': playerData.Dfp.SubstitutedReason || ''
									};
								}
							}
							else {
								result = {
									'accountNumber': playerData.Dfp.Direct.Account,
									'adOwnershipGroupId': playerData.Dfp.Direct.OwnershipGroup,
									'adTrackingGroupId': playerData.Dfp.Direct.TrackingGroup
								};
							}
						}
						else {
							return {};
						}
						
						result['adTrackingGroupNumber'] = getAdTrackingGroupNumber(result['adTrackingGroupId']) || '';
						
						result['isSafeViralOverride'] = isSafeViralOverride;
						
						return result;
					})(),
					'auditude': {
						'trackingGroupAuditudeId': playerData['ZoneID'],
						'enableGoogleAds': (function() {
							var result;
							$.each(playerData['DeliverySettings'], function() {
								if (this['Name'] == 'GoogleExtURL') {
									result = (this['Value'] == 'true');
									return false;
								}
							});
							return result;
						})()
					}
				},
				'distributor': {
					'name': playerData['DistributorName']
				},
				
				// The below values are deprecated; do not rely on the below values; use the new values instead (see this method's documentation)
				'zoneId': playerData.ZoneID,
				'siteSection': playerData.SiteSectionName

			};
			var playlistId;
			var playlistsData = _playerServicesPlaylistsDataConversion(playerData['Playlists'], playlistItemsLimit);
			_.each(playlistsData, function(playlistData) {
				result['associatedPlaylists'].push({
					'id': playlistData['playlistId'],
					'label': playlistData['title']
				});
				
				// Only add this playlist's data if the playlist has items within it
				if (playlistData['items'].length) {
					// Remember this playlist ID so that video-specific data can be determined later
					playlistId = playlistData['playlistId'];
					
					result['playlists'][playlistId] = playlistData;
				}
			});
			
			// Provide the default playlist ID from player services
			result.defaultWidgetConfig.playlistId = playlistId;
			
			// Add either the 'videoIndex' or 'videosData' associative key to the returned result
			_.each(playerData['Playlists'], function(receivedPlaylistData) {
				// console.log('receivedPlaylistData ==', receivedPlaylistData);
				
				if (receivedPlaylistData['playlistid'] == 9999999) {
					if (receivedPlaylistData['Contents'][0]) {
						// Check if the video data returned from player services is from the playlist data that was also returned, and if so,
						// find the video data's video index within the playlist data returned
						var receivedVideoData = receivedPlaylistData['Contents'][0];
						var playlistItems = result['playlists'][playlistId]['items'];
						
						for (var i = 0; i < playlistItems.length; i++) {
							var videoData = playlistItems[i];
							
							// Find the first video index of the received video data in the queued playlist data
							if (videoData['videoId'] == receivedVideoData['ContentID']) {
								result['videoIndex'] = i;
								break;
							}
						}
						
						// If the video data's video index was not found for the current playlist's data, simply return the video data
						if (typeof result['videoIndex'] == 'undefined') {
							result['videosData'] = result['videosData'] || [];
							result['videosData'].push(_playerServicesVideoDataConversion(receivedVideoData));
						}
					}
				}

				// If this is the playlist object that lists the provider stories data
				if (receivedPlaylistData['playlistid'] === -1) {
					for (var i = 0; i < receivedPlaylistData.Contents.length; i++) {
						var receivedVideoData = receivedPlaylistData.Contents[i];

						result['videosData'] = result['videosData'] || [];
						result['videosData'].push(_playerServicesVideoDataConversion(receivedVideoData));
					}
				}				
			});
			
			return result;
		};

		/**
		 * @param {Ndn_Widget} widget The widget in need of its settings from player services
		 * @param {String} [playlistId] The playlist id to fetch data for
		 * @param {int} [playlistItemsLimit] The maximum number of playlist items needed
		 * @param {int} [videoId] The video id to fetch data for
		 * @param {int} [providerId] The database id of a content provider (this is provided only if trying to receive a video related to a content provider's story)
		 * @param {String} [providerStoryId] The database id of a content provider's story (this is provided only if trying to receive a video related to a content provider's story)
		 * @return See _playerServicesPlayerDataConversion function
		 * @static
		 * @private
		 */
		var _getWidgetSettings = function(widget, playlistId, playlistItemsLimit, videoId, providerId, providerStoryId) {
			return $.Deferred($.proxy(function(deferred) {
				var widgetConfig = widget.get('config');
				
				if (!playlistItemsLimit) {
					playlistItemsLimit = widget.get('playlistItemsLimit');
				}
				
				$.ajax({type: 'GET',
					url: _getPlayerDataAjaxUrl(widget, widgetConfig.widgetId, widgetConfig.trackingGroup, playlistId || widgetConfig.playlistId, videoId || widgetConfig.videoId, playlistItemsLimit, providerId, providerStoryId),
					contentType: 'json',
					dataType: 'jsonp'
				})
				.done(function(response) {
					// console.log('[>>] Raw response: ', response);

					// If no response data is provided, indicate that this player services request has failed
					if (!response || !response.Playlists) {
						return deferred.reject();
					}
					else {
						var convertedResponse = _playerServicesPlayerDataConversion(response, playlistItemsLimit);
						
						// console.log('[>>] Converted response: ', convertedResponse);
						
						// Cache all of the returned playlist data
						$.each(convertedResponse.playlists, function() {
							Ndn_PlayerServices_Cache.cachePlaylistData(this, playlistItemsLimit);
						});
						
						// If isolated videos data was returned, cache it
						if (convertedResponse.videosData) {
							$.each(convertedResponse.videosData, function() {
								Ndn_PlayerServices_Cache.cacheVideoData(this);
							});
						}
						
						// TODO: Uncomment-out the below line of code. This has been removed temporarily for backwards-compatability. Do not rely on the "playlists" property to be present for future code!
						// This data does not pertain to the scope of this function, so remove it from the response
						// delete convertedResponse.playlists;
						
						deferred.resolve(convertedResponse);
					}
				})
				.fail(function() {
					deferred.reject({
						reason: (providerId || providerStoryId)
							? 'storyVideoUnavailable'
							: ''
					});
				});
			}, this)).promise();
		};

		var Ndn_PlayerServices = {
			/**
			 * @param {Ndn_Widget} widget The widget in need of its settings from player services
			 * @return {jQuery.Deferred} A deferred object that resolves with structured data concerning the provided widget's settings. See _playerServicesPlayerDataConversion function for the returned structured data's format.
			 */
			getWidgetSettings: function(widget) {
				var widgetConfig = widget.get('config');
				return _getWidgetSettings.call(this, widget, null, null, null, widgetConfig['providerId'], widgetConfig['providerStoryId']);
			},
			
			/**
			 * @param {int} playlistId The database id of the playlist
			 * @param {Ndn_Widget} widget The widget requesting this playlist data
			 * @param {int} [playlistItemsLimit] The maximum number of playlist items needed, if not provided then the provided widget's "playlistItemsLimit" property is used instead
			 * @return {jQuery.Deferred}
			 */
			getPlaylistData: function(playlistId, widget, playlistItemsLimit) {
				// console.log("Ndn_PlayerServices.getPlaylistData(", arguments, ");");
				
				return $.Deferred($.proxy(function(deferred) {
					if (typeof playlistItemsLimit == 'undefined') {
						playlistItemsLimit = widget.get('playlistItemsLimit');
					}
					
					var cachedResult = Ndn_PlayerServices_Cache.getPlaylistData(playlistId, playlistItemsLimit);
					if (cachedResult) {
						deferred.resolve(cachedResult);
					}
					else {
						_getWidgetSettings.call(this, widget, playlistId)
						.done($.proxy(function() {
							cachedResult = Ndn_PlayerServices_Cache.getPlaylistData(playlistId, playlistItemsLimit);
							if (cachedResult) {
								deferred.resolve(cachedResult);
							}
							else {
								deferred.reject();
							}
						}, this))
						.fail(deferred.reject);
					}
				}, this)).promise();
			},
			
			getVideoData: function(videoId, widget) {
				return $.Deferred($.proxy(function(deferred) {
					var cachedResult = Ndn_PlayerServices_Cache.getVideoData(videoId);
					if (cachedResult) {
						deferred.resolve(cachedResult);
					}
					else {
						_getWidgetSettings.call(this, widget, null, 1, videoId)
						.done($.proxy(function() {
							cachedResult = Ndn_PlayerServices_Cache.getVideoData(videoId);
							if (cachedResult) {
								deferred.resolve(cachedResult);
							}
							else {
								deferred.reject();
							}
						}, this))
						.fail(deferred.reject);
					}
				}, this)).promise();
			}
		};
		
		return Ndn_PlayerServices;
	}
);
