define([
    'jquery',
    'backbone',
    'mustache',
    'models/Ndn/App',
    'models/Ndn/Widget',

 	'text!templates/ndn/modal-embed/default.html',
 	'text!templates/ndn/modal-embed/expandable.html'
],
function(
	$,
	Backbone,
	Mustache,
	Ndn_App,
	Ndn_Widget,
	
	defaultTemplate,
	expandableTemplate
) {
	/**
	 * The zero-based index of the modal embed product that has been placed on the current page
	 * @var {int}
	 */
	var modalEmbedIndex = 0;
	
	return Backbone.Model.extend({
		defaults: {
			/**
			 * An associative array mapping template aliases to their respective template content
			 * @var {Array<String,String>}
			 */
			'templates': {
				'default': defaultTemplate,
				'expandable': expandableTemplate
			},
			
			/**
			 * The product responsible for launching this modal embedded product
			 * @var {Ndn_Widget}
			 */
			'sourceProduct': null,
			
			/**
			 * The product embedded within this modal window
			 * @var {Ndn_Widget}
			 */
			'product': null,
			
			/**
			 * The zero-based index of this modal embed product
			 * @var {int}
			 */
			'modalEmbedIndex': null,
			
			/**
			 * @var {jQueryElement}
			 */
			'container': null
		},
		
		/**
		 * @return {int} The next modal embed index
		 */
		getNextModalEmbedIndex: function() {
			return modalEmbedIndex++;
		},
		
		/**
		 * @param {Ndn_Widget} sourceProduct The product responsible for launching this modal embedded product
		 * @param {Object} productConfig The config settings of the new product to embed within this modal window
		 */
		initialize: function(sourceProduct, productConfig) {
			// If using the new modal behavior, override the player's config with the Inline590 player
			if (Ndn_App.getSetting('launchModalEmbedsV2')) {
				productConfig = $.extend({}, productConfig, {
					type: 'VideoPlayer/Inline590'
				});
			}
			
			this.set({
				sourceProduct: sourceProduct,
				productConfig: productConfig,
				modalEmbedIndex: this.getNextModalEmbedIndex()
			});
			
			if (Ndn_App.getSetting('launchModalEmbedsV2')) {
				this.initializeDom2();
			}
			else {
				this.initializeDom();
			}
		},
		
		getInnerContainer: function() {
			return $('#ndn-modal-embed-' + this.get('modalEmbedIndex'));
		},

		initializeDom: function() {
			var modalWidth = 778,
				modalHeight = 825;
			
			var modalScreen = $('<div class="ndn_modalEmbedScreen"></div>').appendTo('body');
			var container = $('<div class="ndn_modalEmbed"><div id="ndn-modal-embed-' + this.get('modalEmbedIndex') + '" data-config-type="VideoPlayer/16x9" data-config-playlist-id="' + this.get('productConfig')['playlistId'] + '"  data-config-tracking-group="' + this.get('productConfig')['trackingGroup'] + '" data-config-video-id="' + this.get('productConfig')['videoId'] + '" data-config-widget-id="' + this.get('productConfig')['widgetId'] + '" data-config-site-section="' + this.get('productConfig')['siteSection'] + '"></div><div class="ndn_closeModal"></div></div>');
			
			// Cross-domain support for the opacity
			modalScreen.css('opacity', '0.7');
			
			// Calculate the y-offset from the top of the window in order to center the modal window vertically, but make sure that there is at least 20px padding for the top of the modal window
			var yOffset = Math.max(20, Math.floor(($(window).height() - modalHeight) / 2));
			
			container.css({
				top: ($(window).scrollTop() + yOffset) + 'px',
				width: modalWidth + 'px'
			});
			
			// Center the modal element horizontally within the browser's window
			var centerModalElement = $.proxy(function() {
				container.css('left', (Math.floor($(window).width() / 2) - Math.floor(modalWidth / 2)) + 'px');
			}, this);
			
			centerModalElement();
			
			$('body').append(container);
			
			$(window).resize(centerModalElement);
			
			// Begin the embed process for the product within this modal window
			Ndn_Widget.embed(this.getInnerContainer().get(0), this.get('productConfig'), ['fromSlider'])
			.done($.proxy(function(embeddedProduct) {
				embeddedProduct.setIsModal(true);
				this.set('product', embeddedProduct);
			}, this));
			
			var closeModal = $.proxy(function() {
				if (this.get('product')) this.get('product').destruct();
				modalScreen.hide();
				container.hide();
			}, this);
			
			modalScreen.on('click', closeModal);
			container.on('click', '.ndn_closeModal', closeModal);

			this.set('container', container);
		},

		initializeDom2: function() {
			// console.log('initializeDom2();');
			
			var modalWidth = 600;
			
			var modal = this;
			// var container = $('<div class="ndn_modalEmbed"><div id="ndn-modal-embed-' + this.get('modalEmbedIndex') + '"></div><div class="ndn_closeModal"></div></div>');
			
			var container = $(Mustache.to_html(expandableTemplate, {
				modalEmbedIndex: this.get('modalEmbedIndex')
			}));
			
			container.css({
				bottom: '0',
				width: modalWidth + 'px'
			});
			
			// Center the modal element horizontally within the browser's window
			var centerModalElement = $.proxy(function() {
				container.css('left', (Math.floor($(window).width() / 2) - Math.floor(modalWidth / 2)) + 'px');
			}, this);
			
			centerModalElement();
			
			$('body').append(container);
			
			$(window).resize(centerModalElement);
			
			// Begin the embed process for the product within this modal window
			Ndn_Widget.embed(this.getInnerContainer().get(0), this.get('productConfig'))
			.done($.proxy(function(embeddedProduct) {
				// Establish that the embedded product is a embedded within a modal and link this modal to the product embedded within it
				embeddedProduct.setIsModal(true);
				modal.set('product', embeddedProduct);
				
				embeddedProduct.on('widgetEmbedEnd', $.proxy(function() {
					// Hide the selectable videos section of the 16x9 player once the product has been completely embedded
					// embeddedProduct.hideSelectableVideos(); // embeddedProduct.getContainerElement().find('.ndn_selectableVideosContainer').hide();
					
					embeddedProduct.toggleSelectableVideos();
					
					// Initialize the event listeners for the modal's behavior
					container
					.on('click', '.ndn_modalEmbed_close', $.proxy(function() {
						if (this.get('product')) this.get('product').destruct();
						container.hide();
					}, this))
					.on('click', '.ndn_modalEmbed_expand', function() {
						embeddedProduct.toggleSelectableVideos();
					});
				}, this));
			}, this));
			

			this.set('container', container);
		}
	});
});