define([
	 	'jquery',
	 	'underscore',
	 	'backbone',
	 	'models/Ndn/App',
	 	'models/Ndn/Widget',
	 	'models/Ndn/Utils/UserAgent',
	 	'models/Akamai/VideoPlayerModel',
	 	'models/Ndn/PlayerServices',
	 	'models/Ndn/PlayerServices/Cache',
	 	'models/Ndn/VideoPlayer/EnforceVisibilityOverlayModel',
	 	'models/Ndn/VideoPlayer/StartOverlayModel',
	 	'models/Ndn/VideoPlayer/PauseOverlayModel',
	 	'models/Ndn/VideoPlayer/ShareOverlayModel',
	 	'models/Ndn/VideoPlayer/EndslateOverlayModel',
	 	'models/Ndn/VideoPlayer/InfoOverlayModel',
	 	'models/Ndn/Ads/AniView',
	 	'models/Ndn/Ads/OptimaticPlayer',
	 	'models/Ndn/Ads/EasiPlayer',
	 	'models/Ndn/Ads/LkqdPlayer',
	 	'models/Ndn/Utils/UrlParser',
	 	'models/Ndn/Analytics/Ndn',
	 	'models/Ndn/Utils/DomElement',
        'models/Ndn/Analytics/ComscoreVme',
        'models/Ndn/VideoPlayer/Behavior/RotatingRmm',
	 	'models/Ndn/Debugger',
	 	
	 	'lib/amp.premier/config/skin/templates',
	 	
	 	'jquery_responsive_containers'
 	], function(
 		$,
 		_,
 		Backbone,
 		Ndn_App,
 		Ndn_Widget,
 		Ndn_Utils_UserAgent,
 		Akamai_VideoPlayerModel,
 		Ndn_PlayerServices,
 		Ndn_PlayerServices_Cache,
 		Ndn_VideoPlayer_EnforceVisibilityOverlayModel,
 		Ndn_VideoPlayer_StartOverlayModel,
 		Ndn_VideoPlayer_PauseOverlayModel,
 		Ndn_VideoPlayer_ShareOverlayModel,
 		Ndn_VideoPlayer_EndslateOverlayModel,
 		Ndn_VideoPlayer_InfoOverlayModel,
 		Ndn_Ads_AniView,
 		Ndn_Ads_OptimaticPlayer,
 		Ndn_Ads_EasiPlayer,
 		Ndn_Ads_LkqdPlayer,
 		Ndn_Utils_UrlParser,
 		Ndn_Analytics_Ndn,
 		Ndn_Utils_DomElement,
 		Ndn_Analytics_ComscoreVme,
 		Ndn_VideoPlayer_Behavior_RotatingRmm,
 		Ndn_Debugger,
 		
 		ampPremierSkinTemplates
 	) {
		/**
		 * Structured data concerning the available ad players that may be used in place of the Akamai Media Player. Format:
		 * <pre>{
		 * 	String: Object, // The ad player type ID => the ad player class
		 * 	... // All other types of ad players that can replace the Akamai Media Player
		 * }</pre>
		 * @var {Object}
		 */
		var _adPlayers = {
			lkqd: Ndn_Ads_LkqdPlayer,
			easi: Ndn_Ads_EasiPlayer,
			optimatic: Ndn_Ads_OptimaticPlayer
		};
	
 		// List of players that should send a PTR value to DFP.
 		var ptrProducts = ['VideoPlayer/PTR', 'VideoLauncher/Slider', 'VideoLauncher/Playlist300x250'];
 		
 		/**
 		 * @param {String|Number} videoId The video's actual database ID
 		 * @param {Boolean} isRmm Whether the video is being used for RMM purposes
 		 * @return {String} The video's asset ID, which is a hardcoded ID if the video specified is an RMM or the video's actual database ID that is currently loaded otherwise 
 		 */
 		var _getVideoAssetId = function(videoId, isRmm) {
 			return isRmm ? '23408962' : (videoId + '');
 		};
		
		/**
		 * @param {Boolean} isRmm Whether the parameters requested are for the purposes of a video being loaded for RMM
		 * @return {Object} Structured data concerning dynamic parameters are referenced within the Akamai Media Player's config
		 * @private
		 */
		var _getParams = function(isRmm) {
			// console.log('_getParams();');
			
		    var widget = this.get('widget'),
		        widgetConfig = widget.get('config'),
		        currentVideoData = this.getCurrentVideoData(),
		        playerData = this.get('playerData');
			
			var shareScenarioId = (function() {
				try {	
					var emailEnabled = playerData.settings.share.email.enabled,
						socialNetworksEnabled = playerData.settings.share.socialNetworks.enabled,
						embedCodeEnabled = currentVideoData.settings.share.embedCode.enabled;
					
					switch(true) {
						case socialNetworksEnabled && emailEnabled && embedCodeEnabled: 	return 1;
						case socialNetworksEnabled && emailEnabled && !embedCodeEnabled: 	return 2;
						case socialNetworksEnabled && !emailEnabled && !embedCodeEnabled: 	return 3;
						case !socialNetworksEnabled && emailEnabled && embedCodeEnabled: 	return 4;
						case !socialNetworksEnabled && !emailEnabled && embedCodeEnabled: 	return 5;
						case !socialNetworksEnabled && !emailEnabled && !embedCodeEnabled: 	return 6;
						case !socialNetworksEnabled && emailEnabled && !embedCodeEnabled: 	return 7;
					}
				}
				catch (e) {
					return '';
				}
			})();

			var buildNumber = Ndn_App.getBuild();
			var buildEnvironment = Ndn_App.getEnv();
			
			var widgetTypeId = this.get('widget').getWidgetTypeId();	

			var playerBehaviorId = ($.proxy(function() {
			    if (Ndn_Debugger.inMode('pb')) {
			        console.log('[Ndn_VideoPlayer] playerBehaviorId debug:');
                    console.log(' >> this.isRotatingRmm() ==', this.isRotatingRmm());
                    console.log(' >> this.isShowingPtr() ==', this.isShowingPtr());
                    console.log(' >> this.isShowingRmm() ==', this.isShowingRmm());
                    console.log(" >> widgetConfig['playOnMouseover'] ==", widgetConfig['playOnMouseover']);
                    console.log(" >> widgetConfig['autoPlay'] ==", widgetConfig['autoPlay']);
			    }
			    
				if (this.isRotatingRmm()) {
				    return '5';
				}
				else if (this.isShowingPtr() && widget.getCachedSettings().settings.behavior.ptr.playsInIframe) {
				    // If this is a forced PTR player (which was added for the "Altitude" network)
				    return '6';
				}
				else if (this.isShowingPtr()){
					return '4';
				}
				else if (this.isShowingRmm()) {
                    return '2';
                }
				else if (widgetConfig['autoPlay']) {
				    return '1';
				}
				else if (widgetConfig['playOnInView']) {
				    return '7';
				}
				else if (widgetConfig['playOnMouseover']) {
					return '3';
				}
				else {
				    // Indicate the "Click to Play" player behavior
					return '0';
				}
			}, this))();

			// The "wgt" value, which is used for tracking/analytics
			var wgtValue = ($.proxy(function() {
				if (this.isShowingRmm()) {
					if (widgetConfig['rmmSoundOn']) {
						if (widgetConfig['type'] == 'VideoPlayer/Inline300' || widgetConfig['type'] == 'VideoLauncher/Slider') {
							return 9;
						}
						else if (widgetConfig['type'] == 'VideoPlayer/Inline590') {
							return 10;
						}
					}
					else {
						if (_.contains(['VideoPlayer/Inline300', 'VideoPlayer/Inline590', 'VideoLauncher/Slider', 'VideoLauncher/Playlist300x250', 'VideoPlayer/PTR'], widgetConfig['type'])) {
							return 1;
						}
					}
					
					if (_.contains(['VideoLauncher/VerticalTower', 'VideoLauncher/Horizontal'], widgetConfig['type'])) {
						return 8;
					}
				} else if (_.contains(['VideoPlayer/PTR'], this.get('widget').getSplitVersionId())) {
					return 1;
				}
				
				return 0;
			}, this))();
			
			/**
			 * @var {Boolean} Whether the preroll ad(s) to play before video content are UI (user-initiated) ad(s)
			 */
			var isUiAdSeries = wgtValue == 0;

			var siteSectionSegments = (widgetConfig['siteSection'] || '').split('_');
			var siteSectionSecValue = (siteSectionSegments[1] || 'oth'),
				siteSectionSubValue = (siteSectionSegments[2] || 'non'),
				siteSectionPageTypeValue = (siteSectionSegments[3] || ''); // TODO: Determine what the default value should be if the 4th segment of the site section is not provided (right now it is just the empty string "")
			
			var embedOriginUrl = Ndn_Widget.getEmbedOriginUrl();
			var embedPageUrl = embedOriginUrl; // For backwards-compatability
			var encodedEmbedOriginUrl = encodeURIComponent(embedOriginUrl),
				doubleEncodedEmbedOriginUrl = encodeURIComponent(encodeURIComponent(embedOriginUrl)),
				encodedCurrentPageUrl = encodeURIComponent(window.location.href.toString()),
				doubleEncodedCurrentPageUrl = encodeURIComponent(encodedCurrentPageUrl);

			var containerElement = $('#' + this.get('playerContainerElementId'));
			
			var videoPlayerWidth = containerElement.width(),
				videoPlayerHeight = containerElement.height();

			/**
			 * @param {int} videoWidth The width of the current video player measured in pixels
			 * @return {int} A number identifying the video player's width range; "1", "2", and "3" indicate ranges, "0" indicates an error occurred and the video player's width was not adequately detected
			 */
			var videoSizeId = (function(videoWidth) {
				videoWidth = parseInt(videoWidth);
				if (videoWidth <= 300) {
					return 1;
				}
				else if (videoWidth < 315) {
					return 2;
				}
				else if (videoWidth < 350) {
					return 3;
				}
				else if (videoWidth < 400) {
					return 4;
				}
				else {
					return 5;
				}
			})(videoPlayerWidth);
			
			// Get the fold position ID using the video player's container element (instead of the widget's container element)
			var foldPositionId = this.get('widget').getFoldPositionId(this.getContainerElement());
		
			var thumbs = this.get('widget').getSplitVersionModel().getThumbnailsEnabled() ? 1 : 0;

			/**
			 * The anti-keywords of the current video
			 * @var {String}
			 */
			var videoAntiKeywords = (function() {
				try {
					return currentVideoData['antiKeywords'] || '';
				}
				catch (e) {
					return '';
				}
			})();
			
			/**
			 * The database id of content provider partner that provided the current video
			 * @var {int}
			 */
			var providerId = (function() {
            	try {
            		return currentVideoData['provider']['id'];
            	}
            	catch (e) {
            		return '';
            	}
            })();
			
			/**
			 * The tracking group of content provider partner that provided the current video
			 * @var {int}
			 */
			var providerTrackingGroup = (function() {
            	try {
            		return currentVideoData['provider']['trackingGroup'];
            	}
            	catch (e) {
            		return '';
            	}
            })();
			
			/**
			 * Whether our ad calls are informed that sound is enabled on the video player
			 * @var {Boolean}
			 */
			var isSoundEnabled = !this.isMuted();
			
			/**
			 * The next analytics timeline event index available
			 * @var {Integer}
			 */
			var analyticsTimelineEventIndex = this.get('currentAteiValue');
			
			var auditudeUserData = ($.proxy(function() {
				var result = {
					wgt: wgtValue,
					external_url: encodedEmbedOriginUrl,
					dpid: widgetConfig['trackingGroup'],
					sitesection: widgetConfig['siteSection'],
					sec: siteSectionSecValue,
					sub: siteSectionSubValue,
					pt: siteSectionPageTypeValue,
					fp: foldPositionId,
					cp: parseInt(widgetConfig['continuousPlay']),
					pb: playerBehaviorId,
					snd: isSoundEnabled ? '1': '0',
					videosize: videoSizeId
				};

				// Include the player type id (if applicable)
				if (widgetTypeId !== null) {
					result.plt = widgetTypeId;
				}
				
				try {
					// Handle when Auditude has Google Ads enabled
					if (this.get('playerData').settings.auditude.enableGoogleAds) {
						result.descriptionUrl = doubleEncodedCurrentPageUrl; // doubleEncodedEmbedOriginUrl;
					}
				}
				catch (e) {}
				
				return result;
			}, this))();
			
			/**
			 * Specially formatted string to send to Auditude. This formatted string allows for the scenario where keys can point to multiple data,
			 * for example: "...;lotame=value1;lotame=value2;lotame=value3;wgt=0;..."
			 * @var {String}
			 */
			var auditudeUserDataString = ($.proxy(function() {
				var result = '';

				var adsTargetingTypes = [
					// 'collective',
					// 'lotame',
					'legolas'
				];
				$.each(adsTargetingTypes, function() {
					var adsTargeting = Ndn_App.getAdsTargeting().getInstance(this);
					var auditudeSegmentKey = adsTargeting.get('auditudeSegmentsKey');
					
					if (adsTargeting.isLoaded()) {
						$.each(adsTargeting.getSegments(), function() {
							result += auditudeSegmentKey + '=' + this + ';';
						});
					}
				});
				
				$.each(auditudeUserData, function(index, value) {
					result += index + '=' + value + ';';
				});
				
				return result;
			}, this))();

			var auditudeParams = (function() {
				var result = {
					userData: auditudeUserDataString,
					external_url: encodedEmbedOriginUrl
				};
				
				if (auditudeUserData.descriptionUrl) {
					result.descriptionUrl = auditudeUserData.descriptionUrl;
				}
				
				return result;
			})();
			
			/**
			 * The "iu" value passed along in the DFP request call for ads
			 * @var {String}
			 */
			var dfpIuValue = (function() {
				try {
					var dfpInfo = playerData.settings.dfp;
					return dfpInfo.accountNumber + '/' + 
						dfpInfo.adOwnershipGroupId +
						(Ndn_Utils_UserAgent.isMobile() ? '_mob' : '') + '/' + 
						dfpInfo.adTrackingGroupId + '/' + 
						widgetConfig['siteSection'] + '/' + 
						siteSectionSecValue + '/' + 
						siteSectionSubValue;
				}
				catch (e) {}
			})();
			
			/**
			 * The type of video player that is to be used; recognized values: "html5", ""
			 * @var {String}
			 */
			var videoPlayerType = (this.getPlaybackMode() == 'flash' ? 'flash' : 'html5');
			
			var truViewCounter = this.getTruViewCounter();
			
			/**
			 * The embed index of this embedded product
			 * @var {Number}
			 */
			var embedIndex = this.get('widget').get('embedIndex');
			
			/**
			 * The "tracking group" to be received by the ad network
			 * @var {String}
			 */
			var adTrackingGroupNumber = this.get('playerData').settings.dfp.adTrackingGroupNumber;

			/**
			 * URI encoded DFP parameters to be sent to IMA, formatted like so: "wgt=1%26adserver=dfp%26..." (instead of "wgt=1&adserver=dfp&...")
			 * @var {String}
			 */
			var dfpParams = $.proxy(function() {
			    // If the "dfp" settings data is not available, return an empty array
			    if (!this.get('playerData').settings.dfp) return [];
			    
			    // Determine whether the "dpid" parameter is determined by a Safe Viral override
			    var isSafeViralOverride = this.get('playerData').settings.dfp.isSafeViralOverride;
			    
			    // Determine what the original (when non-overridden by Safe Viral) "dpid" value would be
			    var originalDpidValue = this.get('playerData').defaultWidgetConfig.trackingGroup;
			    
				// Comma-delimited list of segment data concerning Lotame's ad targeting
				var lotameAdTargetingSegments = (function() {
					var adsTargeting = Ndn_App.getAdsTargeting().getInstance('lotame');
					return (adsTargeting.isLoaded() ? adsTargeting.getSegments() : []).join(',');
				})();
				
				var params = [
					['wgt', wgtValue],
					['plt', widgetTypeId],
					['pb', playerBehaviorId]
				];
				
				// If this video player's product can have PTR enabled, include the parameters below
				if (_.contains(ptrProducts, widgetConfig['type'])) {
					params = params.concat([['ptr', (widget.getCachedSettings().settings.behavior.provided.ptr.enabled ? '1' : '0')]]);
					params = params.concat([['thmb', thumbs]]);
				}

				params = params.concat([
					['snd', (isSoundEnabled ? '1': '0')],
					['fp', foldPositionId],
					['cp', parseInt(widgetConfig['continuousPlay'])],
					['vs', videoSizeId],
					['pt', siteSectionPageTypeValue],
					['dpid', isSafeViralOverride ? this.get('playerData').settings.dfp.adTrackingGroupNumber : originalDpidValue],
					['sitesection', widgetConfig['siteSection']],
					['vt', videoPlayerType],
					['cpvnum', truViewCounter],
					['plid', widgetConfig['playlistId'] || ''],
					['assetid', _getVideoAssetId(currentVideoData['videoId'], isRmm || this.isShowingPtr())],
					['modal', this.get('widget').isModal() ? '1' : '0'],
					['width', videoPlayerWidth],
					['height', videoPlayerHeight],
					['vli', this.getVideoLoadIndex()],
					['dtype', Ndn_Utils_UserAgent.getDeviceType()],
					['lat', lotameAdTargetingSegments],
					['akw', videoAntiKeywords]
				]);
				
				if (isSafeViralOverride) {
				    params.push(['ordpid', originalDpidValue]);
				}
				
				params = params.concat([
					['wid', widgetConfig['widgetId']],
					['cpid', providerId],
					['insid', Ndn_App.getPageViewSessionId()],
					['embedIndex', embedIndex],
					['ver', buildNumber],
					['benv', buildEnvironment],
					['atei', analyticsTimelineEventIndex],
					['ua', Ndn_Utils_UserAgent.getBrowser().toLowerCase()],
					['ext_url', encodedEmbedOriginUrl],
					['de_url', doubleEncodedEmbedOriginUrl],
					['adserver', 'dfp']
				]);
				
				var result = [];
				$.each(params, function(index, param) {
					var paramKey = param[0],
						paramValue = (param[1] + '').substr(0, 256); // Limit the parameter's value to 256 characters maximum
					
					result.push([paramKey, paramValue]);
				});
				return result;
			}, this)();
			
			var dfpEncodedParams = (function() {
				var result = [];
				$.each(dfpParams, function(index, param) {
					var paramKey = param[0],
						paramValue = param[1];
					
					result.push(paramKey + '=' + paramValue);
				});
				return encodeURIComponent(result.join('&'));
			})();

			/**
			 * A comma-delimited list of dimensions for needed companion ads
			 * @var {String}
			 */
			var dfpCiuSzsValue = (function() {
				if (widgetConfig['type'] == 'VideoPlayer/16x9' || widgetConfig['type'] == 'VideoPlayer/Inline300' || widgetConfig['type'] == 'VideoPlayer/Studio') {
					return '300x250';
				}
				else {
					return '';
				}
			})();
			
			var result = {
				adTrackingGroupNumber: adTrackingGroupNumber,
				'aud_zone_id': (function() {
					try {
						return playerData.settings.auditude.trackingGroupAuditudeId;
					}
					catch (e) {}
				})(),
				buildNumber: buildNumber,
				buildEnvironment: buildEnvironment,
				trackingGroup: widgetConfig['trackingGroup'],
				auditudeUserData: auditudeUserData,
				auditudeParams: auditudeParams,
                adServer: (function() {
                	try {
                		return playerData.settings.ads.server;
                	}
                	catch (e) {}
                })(),
                adsEnabled: widgetConfig['adsEnabled'],
                siteSection: widgetConfig['siteSection'],
                siteSectionSecValue: siteSectionSecValue,
                siteSectionSubValue: siteSectionSubValue,
                distributorName: (function() {
                	try {
                		return playerData.distributor.name;
                	}
                	catch (e) {}
                })(),
                distributorType: '', // 'conventional', // TODO: Unhard code this!
                pageViewSessionId: Ndn_App.getPageViewSessionId(),
                producerCategory: (function() {
                	try {
                		return currentVideoData['provider']['category'];
                	}
                	catch (e) {}
                })(),
                videoId: currentVideoData['videoId'],
                producerId: providerId,
                videoDuration: (function() {
                	try {
                		return currentVideoData['mediaContent'][0]['duration'];
		        	}
		        	catch (e) {}
		        })(),
		        videoPlayerType: videoPlayerType,
                playerBehaviorId: playerBehaviorId,
                isSoundEnabled: isSoundEnabled,
                isUiAdSeries: isUiAdSeries,
                foldPositionId: foldPositionId,
                wgtValue: wgtValue,
                widgetId: widgetConfig['widgetId'] || '',
                widgetTypeId: widgetTypeId,
                videoPlayerWidth: videoPlayerWidth,
                videoPlayerHeight: videoPlayerHeight,
                continuousPlay: parseInt(widgetConfig['continuousPlay']),
                rmmSoundOn: !!widgetConfig['rmmSoundOn'],
                providerName: currentVideoData['provider']['name'],
                shareScenarioId: shareScenarioId,
                playlistId: widgetConfig['playlistId'] || '',
                playlistName: $.proxy(function() {
                	try {
                		return this.getActivePlaylistData().title || '';
                	}
                	catch (e) {}
                }, this)(),
                encodedEmbedPageUrl: encodedEmbedOriginUrl, // @deprecated Use "encodedEmbedOriginUrl" instead
                encodedEmbedOriginUrl: encodedEmbedOriginUrl,
                doubleEncodedEmbedOriginUrl: doubleEncodedEmbedOriginUrl,
                doubleEncodedCurrentPageUrl: doubleEncodedCurrentPageUrl,
                videoSizeId: videoSizeId,
                
                // TODO: Refactor this to be "distributorAuditudeId" since this value is not tied to the provider (or "producer") of the current content about to be played
                producerAuditudeId: (function() {
                	try {
                		return playerData.settings.auditude.trackingGroupAuditudeId;
                	}
                	catch (e) {}
                })(),
                
                mediaAnalyticsC5Value: (this.isShowingRmm() || this.get('widget').getSplitVersionId() == 'VideoPlayer/PTR') ? '02' : '03',
                embedMethod: Ndn_Widget.isEmbeddedByIframe() ? 'iframe' : 'JS',
                embedIndex: embedIndex,
                dfpParams: dfpParams,
                dfpEncodedParams: dfpEncodedParams,
                dfpIuValue: dfpIuValue,
                dfpCiuSzsValue: dfpCiuSzsValue,
                dfpPodPositionValue: this.get('widget').getDfpPodPositionValue(),
                truViewCounter: truViewCounter
			};
			
			if (Ndn_Debugger.inMode('playerParams') && typeof console.dir == 'function') {
			    console.log('[Ndn_VideoPlayer] Parameters:');
			    console.dir(result);
			}
			
			return result;
		};
		
		/**
		 * @private
		 * @param {Boolean} isRmm Whether RMM content is currently being loaded
		 * @see Ndn_VideoPlayer.load()
		 */
		var _load = function(videoIndex, playlistId, playOnLoad, isRmm) {
			if (Ndn_Debugger.isEnabled()) console.log('Ndn_VideoPlayer: _load(', arguments, ');');
			
			return $.Deferred($.proxy(function(deferred) {
				if (typeof playlistId == 'undefined') playlistId = this.get('playlistId');
				if (typeof playOnLoad == 'undefined') playOnLoad = this.get('defaultPlayOnLoadValue');
				
				/**
				 * Handle the loading of the appropriate video data into the video player after loading playlist data into the video player was attempted
				 * @param {Boolean} playlistLoadSuccess Whether playlist data was loaded successfully
				 */
				var _afterPlaylistLoadAttempt = function(playlistLoadSuccess) {
					var nextPlaylistId = playlistLoadSuccess
						? this.get('playlistId')
						: 507;
						
					// console.log('_afterPlaylistLoadAttempt()..');
					
					// If there was no video index specified to be played
					if (typeof videoIndex == 'undefined') {
						// Determine whether the widget's config specifies videos to be played (via either the "videosData" or "videoIndex" properties returned by the widget's settings returned by player services)
						var widgetConfig = this.get('widget').get('config'),
							configSpecifiesVideos = widgetConfig['videoId'] || (widgetConfig['providerId'] && widgetConfig['providerStoryId']);
						
						// If the widget's config specifies videos to be played
						if (configSpecifiesVideos) {
							// If isolated videos data is provided from the player services, then load this into the player's current playlist data, although make sure the active playlist ID is set to null
							// to indicate that these videos are not being played from within a loaded playlist. Additionally, uncache the AMP feed data as it will need to be re-generated.
							if (typeof this.get('playerData')['videosData'] != 'undefined') {
								this.set({
									'playlistData': {
										'items': this.get('playerData')['videosData']
									},
									'playlistId': null,
									'nextPlaylistId': nextPlaylistId,
									'ampFeedData': null
								});
								
								videoIndex = 0;
							}
							else if (typeof this.get('playerData')['videoIndex'] != 'undefined') {
								// If there was a video index provided by the player services call of the current playlist (that may or may not have been set automatically from the player service call as well),
								// then set the video index to the one provided by player services
								videoIndex = this.get('playerData')['videoIndex'];
							} 
							else {
								// Schedule this video player's appropriate playlist to be played next
								this.set({
									'nextPlaylistId': nextPlaylistId
								});
								
								// Indicate that the attempt at loading this widget's video specified by its config, was unsuccessful (the video is thus assumed it is unavailable)
								this.trigger('videoUnavailable');
								return deferred.resolve();
							}
						}
						else {
							videoIndex = 0;
						}
					}
					
					_loadVideo.call(this, videoIndex, playOnLoad, isRmm).done(deferred.resolve);
				};
				
				// Attempt to load the appropriate playlist data into the video player
				this.loadPlaylist(playlistId)
				.fail($.proxy(function() {
					_afterPlaylistLoadAttempt.call(this, false);
				}, this))
				.then($.proxy(function() {
					_afterPlaylistLoadAttempt.call(this, true);
				}, this));
			}, this)).promise();
		};
		
		var _loadVideo = function(videoIndex, playOnLoad, isRmm) {
		    if (Ndn_Debugger.inMode('rmmCp')) console.log('_loadVideo(' + videoIndex + ', ' + playOnLoad + ', ' + isRmm + ');');
			
			if (typeof isRmm == 'undefined') isRmm = false;
			
			return $.Deferred($.proxy(function(deferred) {
				var videoPlayer = this;
				
				// Remove traces of the previously loaded video (if there was one)
				this.unloadVideo();
				
				// Get the index that describes this iteration of loading video content into the Ndn_VideoPlayer instance
				var videoLoadIndex = this.get('videoLoadIndex') + 1;
				
				this.set({
					'isShowingRmm': isRmm,
					'currentVideoIndex': videoIndex,
					'videoLoadIndex': videoLoadIndex
				});
				
				var videoDataToLoad;
				try {
					videoDataToLoad = this.get('playlistData')['items'][videoIndex];
				}
				catch (e) {}
				
				// Wait until we have analytics IDs before we begin loading data/params into the Akamai Media Player, since they are used within the params sent to Akamai Media Player
				Ndn_App.getAnalyticsIds().done($.proxy(function() {
					this.onceEmbedded($.proxy(function() {
						// Attempt to get the current video's data
						var ampFeedVideoData;
						try {
							ampFeedVideoData = this.getAmpFeedVideoData(videoDataToLoad, isRmm);
						}
						catch (e) {}
						
						// If the current video's data is present
						if (typeof ampFeedVideoData != 'undefined') {
							// Once the ads targeting data is deliverable (it was either fetched successfully or timed out)
							Ndn_Widget.getAdsTargeting().onceIsDeliverable($.proxy(function() {
                                // Get the next analytics timeline event index and assign it to this video player instance
                                this.set('currentAteiValue', Ndn_Analytics_Ndn.getNextAtei());
                                
							    // Initialize the params for this video load
							    var getParams = $.proxy(function() {
                                    // Get the appropriate parameters for this video load
                                    var params = _getParams.apply(this, [isRmm]);
                                    params.videoIndex = videoIndex;
                                    
                                    // Remember the parameters that were generated for this video load iteration
                                    this.set('params', params);
                                    
                                    return params;
                                }, this);
								
								// Determine whether a mobile autoplay ad player should be used in place of an RMM
								var useMobileAutoplayAdPlayerForRmm = this.useMobileAutoplayAdPlayerForRmm();

								// Whether ads are enabled via the embedded product's config settings
								var adsEnabledViaConfig = this.get('widget').get('config')['adsEnabled'];
								
								// If an RMM is about to be played but the current device is unable to play video automatically
								if (isRmm && !Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically()) {
									// If ads are enabled and a mobile autoplay ad player should be used in place of an RMM
									if (adsEnabledViaConfig && useMobileAutoplayAdPlayerForRmm) {
                                        if (Ndn_Debugger.inMode('adPlayer')) {
                                            console.log('>> adsEnabledViaConfig && useMobileAutoplayAdPlayerForRmm');
                                            console.log(">> Ndn_App.getSetting('adPlayerTypeForRmm') ==", Ndn_App.getSetting('adPlayerTypeForRmm'));
                                        }
									    
										// If the LKQD player should be used
										if (Ndn_App.getSetting('adPlayerTypeForRmm') == 'lkqd') {
											// Attempt using the appropriate ad player to play RMM ad(s)
											this.attemptUsingAdPlayer(Ndn_App.getSetting('adPlayerTypeForRmm'), videoLoadIndex, getParams())
											.done(function(player) {
												// When either the ad ends or no ad is available, conclude the RMM by triggering the "rmmContinuousPlayEnd" event
												player.on('adEnd adError adUnavailable', function() {
													if (Ndn_Debugger.inMode('adPlayer')) console.log('>> Event: "adEnd adError adUnavailable" was triggered.');

													// Trigger the "rmmContinuousPlayEnd" event since the mobile autoplay ad player has concluded
													videoPlayer.trigger('rmmContinuousPlayEnd');
												});
												
												// Start using the player
												player.start();
											})
											.fail(function() {
												// Trigger the "rmmContinuousPlayEnd" event since no action was taken in place of the RMM
												videoPlayer.trigger('rmmContinuousPlayEnd');
											});
										}
										else {
											// TODO: Merge the AniView player to reuse code like the Optimatic/EASI/LKQD player does
											
											// Create the container for the AniView player
											var prerollAdContainer = $(document.createElement('div')).css({
												'z-index': 200,
												position: 'absolute',
												top: 0,
												left: 0
											}).appendTo(this.getContainerElement());
											
											var prerollAd = new Ndn_Ads_AniView(prerollAdContainer.get(0), getParams().dfpParams);
											
											prerollAd.on('adEnd', $.proxy(function() {
												prerollAdContainer.hide();
												this.trigger('rmmContinuousPlayEnd');
											}, this));
											
											prerollAd.play();
										}
									}
									else {
										// Trigger the "rmmContinuousPlayEnd" event since no action was taken in place of the RMM
										this.trigger('rmmContinuousPlayEnd');
									}
								}
								else {
									var loadIntoAmp = $.proxy(function(adsEnabled, playOnLoad) {
										// If using the AMP player to load ads, indicate the AMP player as the current ad player for this video load iteration
										if (adsEnabled) this.set('adPlayer', this.get('player'));
										
										this.setMuted(this.get('widget').shouldBeMuted(this));
										
										// Set whether the video player should play immediately upon loading the video as well as whether ads are enabled
										this.get('player').setPlayOnLoad(playOnLoad);
										this.get('player').setAdsEnabled(adsEnabled);
										
										// Set the AMPPremier's params appropriately
										this.get('player').setParams(getParams());
										
										this.get('debugInfo')['videosLoaded'].push([
											['setAutoplay', playOnLoad],
											['ads.setEnabled', adsEnabled],
											['setParams', this.getParams()],
											['feed.setData', ampFeedVideoData]
										]);
										
										// Load the appropriate data into the player
										this.get('player').loadVideoData(ampFeedVideoData);
									}, this);
									
									var isSliderProduct = _.contains(['VideoLauncher/Slider', 'VideoLauncher/Playlist300x250'], this.get('widget').get('config')['type']),
										adPlayerTypeForRmm = (function() {
											var adPlayerType = Ndn_App.getSetting('adPlayerTypeForRmm');
											return (_adPlayers.hasOwnProperty(adPlayerType))
												? adPlayerType
												: 'amp';
										})();
									
									if (Ndn_Debugger.inMode('adPlayer')) {
										console.log('adsEnabledViaConfig ==', adsEnabledViaConfig);
										console.log('isRmm ==', isRmm);
										console.log('adPlayerTypeForRmm ==', adPlayerTypeForRmm);
									}
									
									if (adsEnabledViaConfig && isRmm && isSliderProduct && adPlayerTypeForRmm != 'amp') {
										// Attempt using the appropriate ad player to play RMM ad(s)
										this.attemptUsingAdPlayer(adPlayerTypeForRmm, videoLoadIndex, getParams())
										.done(function(player) {
											// When either the ad ends or no ad is available, begin content video playback using the AMP player with ads disabled
											// TODO: Consider listening specifically for the "adUnavailable" event to begin content playback using the AMP player with ads *ENABLED*.
											player.on('adEnd adError adUnavailable', function() {
												if (Ndn_Debugger.inMode('adPlayer')) console.log('>> Event: "adEnd adError adUnavailable" was triggered.');
												
												loadIntoAmp(false, true);
											});
											
											// Start using the player
											player.start();
										})
										.fail(function() {
											// Load into the Akamai Media Player like normal
											loadIntoAmp(adsEnabledViaConfig, playOnLoad);
										});
									}
									else {
										// Load into the Akamai Media Player like normal
										loadIntoAmp(adsEnabledViaConfig, playOnLoad);
									}
									
									var params = this.getParams();
									
									// Indicate that a video has been loaded into the video player
									this.set('hasVideoLoaded', true);
									
									// Increment the "TruView" counter
                                    this.incrementTruViewCounter();
									
									var videoLoadEventData = {
										videoLoadIndex: this.getVideoLoadIndex(),
										videoData: videoDataToLoad,
										playlistId: this.get('playlistId'),
										isSoundEnabled: params.isSoundEnabled,
										playerWidth: this.getContainerElement().width(),
										playerHeight: this.getContainerElement().height(),
										playerBehaviorId: params.playerBehaviorId,
										params: params,
										ampFeedVideoData: ampFeedVideoData,
										widgetConfig: this.get('widget').get('config'),
										playOnLoad: playOnLoad
									};
									
									this.get('widget').trigger('analytics:videoLoad', videoLoadEventData);
									
									this.trigger('videoLoad', {
										videoData: videoDataToLoad,
										playOnLoad: playOnLoad,
										params: params
									});
									
									this.get('widget').trigger('hook:videoLoad', videoLoadEventData);
								}
								
								// Resolve the deferred object now that the video has actually been loaded
								return deferred.resolve();
							}, this));
						}
						else {
							// If no video data was found that could be loaded, indicate that the current video is unavailable
							this.trigger('videoUnavailable');
							return deferred.reject();
						}
					}, this));
				}, this));
			}, this)).promise();
		};
		
		/**
		 * @param {String} eventName The name of the event that observers may listen to in order to prevent or delay the provided closure from executing
		 * @param {Function} closure The logic that observers may prevent or delay from executing (which appears to allow either a Function or jQuery.Deferred object)
		 * @return {jQuery.Deferred}
		 */
		var _deliberate = function(eventName, closure) {
			// Trigger the event
			this.trigger(eventName);
			
			// Get the deliberators and begin executing them
			var deliberators = [];
			$.each(this.get('deliberators')[eventName], function(index, deliberator) {
				deliberators.push($.Deferred(deliberator).promise());
			});
			
			// Once all deliberators resolve, then execute the closure logic provided
			return $.when.apply($, deliberators).done(closure);
		};
		
		/**
		 * Empty all playlist data currently loaded into this video player
		 * @private
		 */
		var _clearPlaylistData = function() {
			this.set({
				playlistData: {
					items: [],
					playlistId: null
				},
				playlistId: null,
				ampFeedData: null
			});
		};
		
		return Backbone.Model.extend({
			defaults: {
				/**
				 * TODO: Document the interface that classes must adhere to when being instantiated for this property.
				 * 
				 * The instance of the ad player currently being used
				 * @var {unknown_type}
				 */
				'adPlayer': null,
				
				/**
				 * The quartile that the current video has played to; allowed values: 0, 25, 50, 75, 100 (or null if no quartile has been set yet)
				 * @var {int}
				 */
				'currentQuartile': null,
			
                /**
                 * The quartile that the current ad has played to; allowed values: 0, 25, 50, 75, 100 (or null if no quartile has been set yet)
                 * @var {int}
                 */
                'currentAdQuartile': null, 

				/**
				 * The index pointing to which video in the playlist data is currently active
				 * @var {?number}
				 */
				'currentVideoIndex': null,

                /**
                 * The number of times each listed event has occurred.
                 * @var {?Object}
                 */
                'currentAdEvents': {
                    'adMute': 0,
                    'adUnmute': 0,
                    'adPause': 0,
                    'adResume': 0
                },
				
				/**
				 * Format:
				 * <pre>{
				 * 	'playlists': {
				 * 		string: { // The playlist id of a loaded playlist => structured data concerning the playlist
				 * 			'playlistId': {int}, // The playlist's database id
				 * 			'items': {Array<Object>} // List of structured data concerning each of the playlist items
				 * 		},
				 * 		... // All other loaded playlists
				 * 	}
				 * }</pre>
				 * @var {?Object}
				 */
				'playerData': null,
				
				/**
				 * The playlist id of the active playlist
				 * @var {?int}
				 */
				'playlistId': null,
				
				/**
				 * The playlist id of the playlist to play after an isolated video has finished playing (a video specified by the "videoId" config setting)
				 * @var {?int}
				 */
				'nextPlaylistId': null,
				
				/**
				 * @var {Akamai_MediaPlayerModel}
				 */
				'player': null,
				
				/**
				 * The video player's active playlist
				 * @var {?Object}
				 */
				'playlistData': null,
				
				/**
				 * The "id" attribute of the container element responsible for holding this video player
				 * @var {String}
				 */
				'playerContainerElementId': null,
				
				/**
				 * Structured data concerning the cached AMPPremier player's formatted feed data
				 * @var {Object}
				 */
				'ampFeedData': null,
				
				/**
				 * The widget that is using this video player
				 * @var {Ndn_Widget}
				 */
				'widget': null,
				
				/**
				 * Whether this video player has been embedded yet
				 * IMPORTANT NOTE: "isEmbedded2" is used instead of "isEmbedded" because apparently "isEmbedded" is a reserved function or something for Backbone apparently
				 * TODO: Investigate an alternative to this property's name being "isEmbedded2"
				 * @var {Boolean}
				 */
				'isEmbedded2': false,
				
				/**
				 * The number of videos that have played continuously in the current set of videos scheduled for continuous play
				 * @var {Integer}
				 */
				'continuousPlays': 0,
				
				/**
				 * The limit to how many continuous plays can happen consecutively
				 * @var {Integer}
				 */
				'continuousPlaysLimit': null,
				
				/**
				 * Whether the video player is currently playing (agnostic of whether content or an advertisement is playing)
				 * @var {Boolean}
				 */
				'isPlaying': false,
				
				/**
				 * Whether the video player is currently showing video content
				 * @var {Boolean}
				 */
				'isShowingVideo': false,
				
				/**
				 * Whether the video player is currently showing ad content
				 * @var {Boolean}
				 */
				'isShowingAd': false,
				
				/**
				 * Whether the top info panel describing the video's title and/or description is displayed
				 * @var {Boolean}
				 */
				'infoPanelEnabled': true,
				
				/**
				 * The default value of the 'playOnLoad' parameter for the load() method
				 * @var {Boolean}
				 */
				'defaultPlayOnLoadValue': false,
				
				/**
				 * Whether the video player has loaded a Rich-Media Monetization (RMM)
				 * @var {Boolean}
				 */
				'isShowingRmm': false,
				
				/**
				 * Whether this video player has video loaded
				 * @var {Boolean}
				 */
				'hasVideoLoaded': false,
				
				/**
				 * The maximum number of playlist items that may be loaded into this video player's current playlist data
				 * @var {?int}
				 */
				'playlistItemsLimit': null,
				
				/**
				 * 
				 * @var {Object}
				 */
				'overlays': {},
				
				/**
				 * Structured data concerning a jury of deliberators that decide whether a certain action may occur. Format:
				 * <pre>{
				 * 	String: [ // The name of the event occuring that allows deliberation to decide whether a certain action is allowed to proceed
				 * 		jQuery.Deferred, // A jQuery.Deferred object that acts as a "jury member" to deliberate whether this action is approved (by using deferred.resolve() method) or denied (using deferred.reject() method)
				 * 		... // All other "jury members"
				 * 	],
				 * 	... // All other events that allow for deliberation
				 * }</pre>
				 * @var {Object<String,Array>}
				 */
				'deliberators': null,
				
				/**
				 * The index of how many videos have played, which determine whether the next ad that plays is considered a "TruView" to DFP
				 * @var {int}
				 */
				'truViewCounter': 0,
				
				/**
				 * Structured data concerning this video player's parameters for its current video load
				 * @var {Object}
				 */
				'params': null,
				
				/**
				 * The index describing the current iteration of when the video player loads a video
				 * @var {Number}
				 */
				'videoLoadIndex': -1,
				
				/**
				 * Whether the video playback during an RMM was ended via calling the player instance's .end() method
				 * @var {Boolean}
				 */
				'endedRmmVideoPlayback': false,
				
				/**
				 * The analytics timeline event index available (for tracking DFP call within the timeline of our internal analytics)
				 * @var {Integer}
				 */
				'currentAteiValue': null,
				
				/**
				 * Debug information concerning instructions this video player has given to the Akamai Media Player library. Format:
				 * <pre>{
				 * 	'ampVersion': {String}, // The version of the current Akamai Media Player build being used (ie. "1.0.0000")
				 * 	'playerConfigDefaults': {Object}, // Structured data concerning the debug player config defaults for the Akamai Media Player library
				 * 	'playerConfig': {Object}, // Structured data concerning the config that this instance of the Akamai Media Player is specifically using
				 * 	'videosLoaded': [ // A series of videos that have been loaded using the Akamai Media Player
				 * 		[ // Structured data concerning a video that was loaded using this video player
				 * 			[ // Structured data concerning an instruction sent to the Akamai Media Player library
				 * 				{String}, // The name of the method called on the Akamai Media Player
				 * 				{unknown_type}, // A parameter sent to the specified method
				 * 				... // Any other parameters sent to the specified method
				 * 			],
				 * 			... // Any other instructions sent to the Akamai Media Player library
				 * 		],
				 * 		... // Any other data concerning a video that was loaded using this video player
				 * 	]
				 * ]</pre>
				 * @var {Array<Array>}
				 */
				'debugInfo': null
			},
			
			/**
			 * Start the video playback of the currently loaded video
			 * TODO: Refactor this.
			 */
			start: function() {
				this.hideOverlays();
				
				if (Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically()) {
					// console.log('About to play like normal! Because we can play video automatically!!');
					
					this.loadVideo(this.getCurrentVideoIndex(), true);
				}
				else {
					// console.log('About to play video for special case of browsers that cannot play videos automatically!');
					
					this.get('player').play();
				}
			},
			
			/**
			 * Increment the "TruView" counter
			 */
			incrementTruViewCounter: function() {
				this.set('truViewCounter', this.get('truViewCounter') + 1);
			},
			
			/**
			 * @return {int} The number of times a video and ad have been loaded into the player after the "TruView" experience happened (when the result is "0"), which is sent to DFP
			 */
			getTruViewCounter: function() {
				return this.get('truViewCounter');
			},
			
			/**
			 * Play a Rich-Media Monetization (RMM)
			 * @param {Integer} videoIndex
			 */
			playRmm: function(videoIndex) {
			    if (Ndn_Debugger.isEnabled()) console.log('Ndn_VideoPlayer.playRmm(', videoIndex, ');');
			    
				return _deliberate.call(this, 'loadAttempt', $.proxy(function() {
					var x;
					_load.call(this, videoIndex, x, true, true);
				}, this));
			},
			
			/**
			 * Hide each of the overlays that may be potentially overlaying on top of this video player
			 */
			hideOverlays: function() {
				$.each(this.get('overlays'), function(overlayName, overlay) {
					if (!_.isNull(overlay)) {
						overlay.hide();
					}
				});
			},
			
			/**
			 * @return {Number} The index describing the current iteration of when the video player loads a video
			 */
			getVideoLoadIndex: function() {
				return this.get('videoLoadIndex');
			},
			
			initializeOverlays: function() {
				this.set('overlays', {
					endslate: new Ndn_VideoPlayer_EndslateOverlayModel(this),
					pause: new Ndn_VideoPlayer_PauseOverlayModel(this),
					start: new Ndn_VideoPlayer_StartOverlayModel(this),
					share: new Ndn_VideoPlayer_ShareOverlayModel(this),
					info: new Ndn_VideoPlayer_InfoOverlayModel(this),
					enforceVisibility: new Ndn_VideoPlayer_EnforceVisibilityOverlayModel(this)
				});
			},
			
			/**
			 * @param {String} adPlayerType Identifier describing the type of ad player; recognized values: "lkqd", "optimatic", "easi"
			 * @param {Number} videoLoadIndex The video load iteration's index when this attempt was first called
			 * @param {Object} params Structured data concerning the parameters for the currently loaded video
			 */
			attemptUsingAdPlayer: function(adPlayerType, videoLoadIndex, params) {
				// If the specified type of ad player is unrecognized, throw an exception
				if (!_adPlayers[adPlayerType]) throw "Unrecognized ad player type: " + adPlayerType;
				
				var videoPlayer = this,
					containerElementClassName = 'ndn_' + adPlayerType + 'PlayerContainer',
					adPlayerClass = _adPlayers[adPlayerType];
				
				return $.Deferred($.proxy(function(deferred) {
					var hasPlayerContainer = this.getContainerElement().find('.' + containerElementClassName).length,
						playerContainerElement = hasPlayerContainer
							? this.getContainerElement().find('.' + containerElementClassName)
							: $('<div class="' + containerElementClassName + ' ndn_videoPlayer_adPlayer"></div>');
					
					// If this video player instance does not have a container for this type of ad player, then add one
					if (!hasPlayerContainer) {
						this.getContainerElement().append(playerContainerElement);
					}
					
					// Wait until the container element for the new ad player has a width and a height
					Ndn_Utils_DomElement.onceHasWidthAndHeight(playerContainerElement).done($.proxy(function() {
						// If the video load index is still the same as when the attempt to use this player was first made
						if (this.get('videoLoadIndex') == videoLoadIndex) {
							adPlayerClass.load(playerContainerElement, params)
							.done(function(player) {
								var surfacePlayerAdEvents = function(player) {
									var eventsToSurface = ['adRequest', 'adLoad', 'adStart', 'adTimeUpdate', 'adEnd', 'adError'];
									$.each(eventsToSurface, function() {
										var eventName = this + '';
										
										player.on(eventName, function(eventData) {
											try {
												videoPlayer.trigger(eventName, eventData);
											}
											catch (e) {
												console.log('Error handling player event "' + eventName + '". Description: ', e);
											}
										});
									});
								};
								
								// Indicate this player as the ad player for this video load iteration
								videoPlayer.set('adPlayer', player);
								
								// Trigger the appropriate events for this instance of Ndn_VideoPlayer concerning this instance of the ad player
								surfacePlayerAdEvents(player);
								
								deferred.resolve(player);
							})
							.fail(function() {
								deferred.reject();
							});
						}
						else {
							// Abort since the Ndn_VideoPlayer instance has moved on to another video load index
							deferred.reject();
						}
					}, this));
				}, this)).promise();
			},
			
			/**
			 * Saves overlays to a hidden dom location just before calling the Akamai Media Player's .destroy() method, which 
			 * if the overlays were not saved, the overlay's dom content is obliterated in some browsers
			 */
			saveOverlays: function() {
				var overlays = this.get('overlays'),
					saveContainer = $(document.createElement('div')).addClass('ndn_videoPlayer_overlaySaveContainer').hide();
				
				// Append the save container to the global DOM
				this.getContainerElement().after(saveContainer);
				
				// Move all of the overlays into this "save container" so that the overlays do not get permanently deleted within IE and perhaps other browsers
				$.each(overlays, $.proxy(function(overlayName, overlay) {
					overlay.get('view').$el.appendTo(saveContainer);
				}, this));
			},
			
			/**
			 * Restores overlays from their saved dom location
			 */
			restoreOverlays: function() {
				var overlays = this.get('overlays'),
					saveContainer = this.getContainerElement().parent().find('.ndn_videoPlayer_overlaySaveContainer');

				// Move all of the overlays from this "save container" back into the appropriate video player container
				$.each(overlays, $.proxy(function(overlayName, overlay) {
					overlay.get('view').$el.appendTo(this.getContainerElement());
				}, this));
			},
			
			initialize: function(widget) {
				var videoPlayer = this;
				
				this.set('widget', widget);
				
				// Keep track of the current state of this video player
				this.on({
					'videoStart': function() {
						this.set({
							'isShowingVideo': true,
							'isShowingAd': false,
							'isPlaying': true
						});
					},
					'adStart': function() {
						this.set({
							'isShowingVideo': false,
							'isShowingAd': true,
							'isPlaying': true
						});
					},
					'adEnd': function() {
						this.set({
							'isShowingAd': false,
							'isPlaying': false
						});
					},
					'videoEnd': function() {
						this.set({
							'isShowingVideo': false,
							'isPlaying': false
						});
					},
					'pause': function() {
						this.set({
							'isPlaying': false
						});
					},
					'play': function() {
						this.set({
							'isPlaying': true
						});
					},
					'videoUnload': function() {
		                this.set({
		                    'isShowingVideo': false,
		                    'isShowingAd': false,
		                    'isPlaying': false
		                });
					}
				});
				
				// Set the continuous plays limit appropriately depending on whether rotating RMM is enabled
				this.set('continuousPlaysLimit', this.isRotatingRmm() ? widget.getCachedSettings().settings.behavior.rmm.continuousPlay : widget.get('config')['continuousPlay']);
				
				// Keep track of the Comscore VME analytics (if VME analytics is enabled for this embedded product)
				if (widget.getCachedSettings().settings.analytics.comscore.vme.enabled) {
                    new Ndn_Analytics_ComscoreVme(this);
			    }
				
				/* For debugging difficult devices without easy debug consoles
				var log = function(message) {
					if (!$('#log222').length) {
						$(document.createElement('div'))
						.attr('id', 'log222')
						.attr('style', "width:100%;height:500px;overflow-y:scroll;border:1px solid Black;")
						.appendTo($('body'));
					}
					
					$('#log222').append('<p>' + message + '</p>');
				};
				*/
				
				if (Ndn_Debugger.inMode('playerEvents') || Ndn_Debugger.inMode('allPlayerEvents')) {
	                var triggerReference = this.trigger;
	                this.trigger = $.proxy(function(eventName) {
                        if (Ndn_Debugger.inMode('allPlayerEvents') || !_.contains(['adTimeUpdate', 'videoTimeUpdate', 'videoVolumeChange'], eventName)) {
                            var args = ['[Ndn_VideoPlayer Event]'];
                            for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);
                            console.log.apply(console, args);
                        }
                        
	                    triggerReference.apply(this, arguments);
	                }, this);
				}
				
				// Initialize the deliberators for this video player
				this.set('deliberators', {
					'loadAttempt': []
				});
				
                $.proxy(function() {
                    var setAdQuartile = $.proxy(function(quartile) {
                        this.set('currentAdQuartile', quartile);
                        this.get('widget').triggerEvent('adView', {
                            percentPlayed: quartile,
                            quartile: quartile,
                            currentTime: quartile == 100
                                ? this.get('adPlayer').getAdDuration()
                                : this.get('adPlayer').getAdCurrentTime(),
                            videoData: this.getCurrentVideoData(),
                            currentVideoIndex: this.get('currentVideoIndex'),
                            playerWidth: this.getContainerElement().width(),
                            playerHeight: this.getContainerElement().height(),
                            videoPlayerType: this.getParams().videoPlayerType // this.get('player').getParams().videoPlayerType
                        }, 'analytics');
                    }, this);

                    this.on({
                        'adStart': function(event) {
                            setAdQuartile(0);
                            this._resetAdConversions();
                        },
                        'adLoad': function(event) {
                            this.get('widget').trigger('analytics:adLoad', event);
                        },
                        'adError': function(event) {
                            this.get('widget').trigger('analytics:adError', event);
                        },
                        'adRequest': function(event) {
                            this.get('widget').trigger('analytics:adRequest', event);
                        },
                        'adTimeUpdate': function(event) {
                            var player = this.get('adPlayer');
                            
                            if (player.isPlayingAd()) {
                                // Update quartile if necessary
                                var quartile = Math.floor((player.getAdCurrentTime() / player.getAdDuration()) * 100);
                                if (_.contains([25,50,75], quartile) && quartile > this.get('currentAdQuartile')) {
                                    setAdQuartile(quartile);
                                }
                            }
                        },
                        'adEnd': function(event) {
                            setAdQuartile(100);
                            this.get('widget').trigger('analytics:adEnd', event);
                            this._resetAdConversions();
                        },
                        'adMute': function(event) {
                            var adEvents = this.get('currentAdEvents');
                            adEvents['adMute']++;
                            this.set('currentAdEvents', adEvents);
                            this.get('widget').trigger('analytics:adMute', adEvents);
                        },
                        'adUnmute': function(event) {
                            var adEvents = this.get('currentAdEvents');
                            adEvents['adUnmute']++;
                            this.set('currentAdEvents', adEvents);
                            this.get('widget').trigger('analytics:adUnmute', adEvents);
                        },
                        'adResume': function(event) {
                            var adEvents = this.get('currentAdEvents');
                            adEvents['adResume']++;
                            this.set('currentAdEvents', adEvents);
                            this.get('widget').trigger('analytics:adResume', adEvents);
                        },
                        'rmmPause': function(event) {
                            var adEvents = this.get('currentAdEvents');
                            adEvents['adPause']++;
                            this.set('currentAdEvents', adEvents);

                            if (this.isShowingAd() == true) {
                                this.get('widget').trigger('analytics:adPause', adEvents);
                            }
                        },
                        'videoPause': function(event) {
                            var adEvents = this.get('currentAdEvents');
                            adEvents['adPause']++;
                            this.set('currentAdEvents', adEvents);

                            if (this.isShowingAd() == true) {
                                this.get('widget').trigger('analytics:adPause', adEvents);
                            }
                        }
                    });
                }, this)();
                
                // Handle when the "playlistData" is updated so that the number of playlist items is correctly limited (if there is a limit)
                this.on('change:playlistData', $.proxy(function() {
                    if (this.get('playlistItemsLimit') !== null && this.get('playlistData')) {
                        this.get('playlistData')['items'] = _.first(this.get('playlistData')['items'], this.get('playlistItemsLimit'));
                    }
                    
                    if (Ndn_Debugger.inMode('playlistData')) console.log('[Ndn_VideoPlayer] playlistData ==', this.get('playlistData'));
                }, this));
                
				// Handle tracking the video views (quartiles)
				$.proxy(function() {
					var setQuartile = $.proxy(function(quartile) {
						// Update the current quartile
						this.set({
							'currentQuartile': quartile
						});
						
                        // Do not trigger any video view analytics for rotating RMM behavior unless it is for the first 6-second bumper video
                        if (this.isRotatingRmm() && this.getVideoLoadIndex() > 0) return;
                        
						this.get('widget').triggerEvent('videoView', {
						    isShowingRmm: this.isShowingRmm(),
						    wgtValue: (this.get('params') || {}).wgtValue,
							percentPlayed: quartile,
							quartile: quartile,
							currentTime: quartile == 100
								? this.get('player').getDuration()
								: this.get('player').getCurrentTime(),
							videoData: this.getCurrentVideoData(),
							playlistId: this.get('playlistId'),
							isInContinuousPlay: this.get('continuousPlays') > 0,
							currentVideoIndex: this.get('currentVideoIndex'),
							playerWidth: this.getContainerElement().width(),
							playerHeight: this.getContainerElement().height(),
							videoPlayerType: this.getParams().videoPlayerType // this.get('player').getParams().videoPlayerType
						}, 'hook,analytics');
					}, this);
					
					this.on({
						'videoStart': $.proxy(function() {
							if (!this.isShowingPtr()) {
								setQuartile(0);
							}
							
							if (!this.isShowingRmm()) {
								// Trigger the hook for the "videoStart" event
								this.get('widget').trigger('hook:videoStart', {
									videoId: this.getCurrentVideoData().videoId
								});
							}
						}, this),
						'videoTimeUpdate': $.proxy(function() {
							var player = this.get('player');
							
							if (!this.isShowingRmm() && !this.isShowingPtr()) {
								// Update quartile if necessary
								var quartile = Math.floor((player.getCurrentTime() / player.getDuration()) * 100);
								if (_.contains([25,50,75], quartile) && quartile > this.get('currentQuartile')) {
									setQuartile(quartile);
								}
							}
							
							// If we need to workaround HTML5 player's lack of support for clipped RMM bumper videos... 
							if (this.isShowingRmm() && this.getPlaybackMode() != 'flash') {
								var rmmStart = 5,
									rmmDuration = 6,
									currentTime = player.getCurrentTime();
								
								if (currentTime < rmmStart) {
									// If the RMM bumper video playback is playing at a "current time" prior to when it should begin, manually set the current time of the playback to the RMM's initial start time
									player.setCurrentTime(rmmStart);
									player.play();
								}
								else if (currentTime > (rmmStart + rmmDuration)) {
                                    // If the RMM bumper video playback has lasted longer than its intended duration, manually end the video playback
								    
								    // Remember whether the video playback has already been ended so that it is not called repeatedly over and over again due to a glitch
								    var isEndedRmmVideoPlaybackState = this.get('endedRmmVideoPlayback');
								    
								    // Remember that video playback is being set to its end state due to this if-block
								    this.set('endedRmmVideoPlayback', true);
								    
								    // If the video playback has not already ended before due to this if-block, then tell the player instance to end its playback
								    if (!isEndedRmmVideoPlaybackState) player.end();
								}
							}
						}, this),
						'videoEnd': $.proxy(function() {
						    // Unset the variable responsible for tracking whether we are in an artificially ended RMM state, triggered by calling the .end() method of the player instance directly
						    this.set('endedRmmVideoPlayback', false);
						    
							if (!this.isShowingPtr()) {
								setQuartile(100);
							}
							
							if (!this.isShowingRmm()) {
								// Trigger the hook for the "videoEnd" event
								this.get('widget').trigger('hook:videoEnd', {
									videoId: this.getCurrentVideoData().videoId
								});
							}
						}, this)
					});
				}, this)();
				
				// Ensure that the control bar is hidden when the RMM's 6-second bumper video is playing; re-enable control bar after the RMM gets unloaded
				(function(videoPlayer) {
					var hideControlsForRmm = function() {
						if (videoPlayer.isShowingRmm()) {
							videoPlayer.disableControls();
						}
					};
					
					var enableControls = function() {
						videoPlayer.enableControls();
					};
					
					videoPlayer.on({
						'videoStart': hideControlsForRmm,
						'rmmUnload': enableControls,
						'rmmContinuousPlayEnd': enableControls
					});
				})(this);
				
				if (Ndn_Debugger.inMode('rmmCp')) {
				    this.on('rmmContinuousPlayEnd', function() {
				        console.log(">> rmmContinuousPlayEnd heard!");
				    });
				}
				    
				// Handle continuous play
				this.on('videoEnd', $.proxy(function() {
					var continuousPlaysLimit = this.get('continuousPlaysLimit');
					
					// Do not execute this logic here if rotating RMM is enabled (logic was moved to the Ndn_VideoPlayer_Behavior_RotatingRmm module)
					if (this.isRotatingRmm()) return;
					
					if (Ndn_Debugger.inMode('rmmCp')) {
					    console.log('videoEnd heard!');
                        console.log('continuousPlaysLimit ==', continuousPlaysLimit);
					    console.log('this.isShowingRmm() ==', this.isShowingRmm());
					}
				    
					var concludeContinuousPlay = $.proxy(function() {
					    if (this.isShowingRmm()) {
					        this.trigger('rmmContinuousPlayEnd');
					    }
					    else {
                            this.trigger('continuousPlayEnd');
					    }
					}, this);
                    
					// If an RMM bumper video just concluded, but rotating RMM ("RMM continuous play") is not enabled, conclude this continuous play series
                    if (this.isShowingRmm() && !this.isRotatingRmm()) {
                        concludeContinuousPlay();
                        return;
                    }
					
					// If the continuous plays limit is set to "0", then make sure to reload the video that just finished playing and trigger the appropriate event
					if (continuousPlaysLimit == 0) {
					    if (!this.isRotatingRmm()) {
	                        this.loadVideo(this.get('currentVideoIndex'), false);
					    }
					    
                        // Indicate that the series of continuous plays has ended
					    concludeContinuousPlay();
					}
					else {
						// Keep track of the number of continuous plays
						this.incrementContinuousPlayCounter();
						
						// If the last video of the currently loaded playlist data just finished playing, trigger the appropriate event
						if (!this.getNextVideoData()) {
							if (this.getPlaylistId()) {
								// Indicate that the end of the video player's loaded playlist has finished playing
								this.trigger('playlistEnd');
							}
							else {
								// Indicate that the isolated video (that was played because of the presence of the "videoId" config setting) has finished playing
								this.trigger('isolatedVideoEnd');
							}
						}
						else {
							// If the number of continuous plays is equal (or greater than, because it might have been skipped if we just reached the 
							// end of the playlist) to the number of continuous plays allowed
							if (this.get('continuousPlays') >= continuousPlaysLimit + 1) {
								// Reset the continuous plays counter
								this.resetContinuousPlayCounter();
								
								// Indicate that the series of continuous plays has ended
		                        concludeContinuousPlay();
							}
							else {
								// Continue to the next video
							    if (!this.isShowingRmm()) {
	                                this.play(this.get('currentVideoIndex') + 1);
							    }
							    else {
		                            this.playRmm(this.get('currentVideoIndex') + 1);
							    }
							}
						}
					}
				}, this));
		        
				// Trigger the expected events when the player instance triggers the "pause" event
				// TODO: Reconcile the two events "rmmPause" and "videoPause" into a single "pause" event, optionally with event data that indicates whether the content being shown is RMM.
				this.on('pause', $.proxy(function() {
					if (this.isShowingRmm()) {
						this.trigger('rmmPause');
					}
					else {
						this.trigger('videoPause');
					}
				}, this));
				
                // If this video player has the rotating RMM functionality enabled, initialize the appropriate module that contains (some of) rotating RMM's logic
                if (this.isRotatingRmm()) new Ndn_VideoPlayer_Behavior_RotatingRmm(this);
				
				this.initializeOverlays();
			},
			
			/**
			 * The product associated with this video player
			 * @return {Ndn_Widget}
			 */
			getProduct: function() {
			    return this.get('widget');
			},
			
			concludeRmmContinuousPlay: function() {
                this.trigger('rmmContinuousPlayEnd');
			},
			
			/**
			 * @return {Object} An associative array mapping all loaded AMP parameters to their respective values
			 */
			getParams: function() {
				return this.get('params') || {}; // this.get('player').getParams();
			},
            
            getContinuousPlays: function() {
                return this.get('continuousPlays');
            },
            
            getContinuousPlaysLimit: function() {
                return this.get('continuousPlaysLimit');
            },
			
			/**
			 * @param {String} requestSource An identifier describing where the request to start a video originated from
			 * @return this
			 */
			trackVideoStartRequest: function(requestSource) {
				this.get('widget').trigger('analytics:videoStartRequest', {
					source: requestSource
				});
				
				return this;
			},
			
			/**
			 * Checks whether there is a next video to step into and if not, it will trigger the appropriate event (either "playlistEnd" or "isolatedVideoEnd")
			 * @return {Boolean} Whether there is a next video to step into
			 */
			stepIntoNextVideo: function() {
			    var hasNextVideo = this.getNextVideoData();
			    if (!hasNextVideo) {
			        if (this.getPlaylistId()) {
                        // Indicate that the end of the video player's loaded playlist has finished playing
			            this.trigger('playlistEnd');
                    }
                    else {
                        // Indicate that the isolated video (that was played because of the presence of the "videoId" config setting) has finished playing
                        this.trigger('isolatedVideoEnd');
                    }
                }
			    
			    return hasNextVideo;
			},
			
	        /**
	         * @return {Boolean} Whether a mobile autoplay ad player should be used in place of running an RMM
	         */
	        useMobileAutoplayAdPlayerForRmm: function() {
	            var config = this.get('widget').getSplitVersionModel().get('config');
	            return (!Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically() || Ndn_App.getSetting('forceMobileAutoplayAdPlayerForRmm')) && _.contains(['VideoLauncher/Slider', 'VideoLauncher/Playlist300x250', 'VideoPlayer/Inline300'], config['type']);
	        },
			
			/**
			 * Toggles whether the info panel is ever shown for the video player
			 * NOTE: This method must be called before the video player gets embedded on the page
			 */
			toggleInfoPanel: function(toggle) {
				if (typeof toggle == 'undefined') toggle = !this.get('infoPanelEnabled');
				
				this.set({
					'infoPanelEnabled': toggle
				});
			},
			
			/**
			 * Load the video at the specified index for the specified playlist; if no playlist is specified, the active playlist is used
			 * @param {int} [videoIndex] The index of the video with the specified playlist to load (if not provided and the current widget's config has a 'videoId' specified, then the video index may be determined automatically by player services; otherwise defaults to 0)
			 * @param {int} [playlistId] The database id of the playlist to load (if not provided, then the currently active playlist is used, and if no playlist is currently active, then a default playlist is received from player services)
			 * @param {Boolean} [playOnLoad] Whether the video immediately begins playing once the video is loaded, defaults to the value specified via the setDefaultPlayOnLoadValue() method
			 */
			load: function(videoIndex, playlistId, playOnLoad) {
				// console.log('Ndn_VideoPlayer.load(', arguments, ');');
				
				return _deliberate.call(this, 'loadAttempt', $.proxy(function() {
					return _load.call(this, videoIndex, playlistId, playOnLoad, false);
				}, this));
			},

			/**
			 * @param {int} [playlistId] The database id of the playlist to load into the video player model and to display thumbnails of
			 * @param {Boolean} [intoCacheOnly=false] Whether the playlist data should be loaded into cache only
			 */
			loadPlaylist: function(playlistId, intoCacheOnly) {
				// console.log('Ndn_VideoPlayer.loadPlaylist(', arguments, ');');
				
				if (typeof intoCacheOnly == 'undefined') intoCacheOnly = false;
				
				return $.Deferred($.proxy(function(deferred) {
					// Make sure that the widget has its settings first from player services
					this.get('widget').getSettings().done($.proxy(function(settings) {
						if (!this.get('playerData')) {
							this.set('playerData', settings);
						}
						
						// If there was no playlist id explicitly provided, use the default "playlistId" config setting from this widget's settings returned in the response
						if (!playlistId) {
							playlistId = this.get('playerData').defaultWidgetConfig.playlistId;
						}
						
						var playlistItemsLimit = this.get('playlistItemsLimit');
						
						// Check the cache for the needed playlist data
						var cachedPlaylistData = Ndn_PlayerServices_Cache.getPlaylistData(playlistId, playlistItemsLimit);
						
						// If cached playlist data was found and this method was executed only for the purpose of loading the playlist 
						// data into the cache, resolve the execution of this method
						if (cachedPlaylistData && intoCacheOnly) {
							return deferred.resolve();
						}
						
						// Load the cached playlist data into the video player
						if (cachedPlaylistData) {
							this.set({
								'playlistData': cachedPlaylistData,
								'playlistId': playlistId,
								'ampFeedData': null
							});
						}
						
						// Retrieve the needed playlist data from player services
						Ndn_PlayerServices.getPlaylistData(playlistId, this.get('widget'), playlistItemsLimit)
						.fail($.proxy(function() {
							// Empty all playlist data associated with this video player since playlist data was unable to be loaded
							_clearPlaylistData.call(this);
							
							return deferred.reject();
						}, this))
						.done($.proxy(function(response) {
							// If only concerned about loading the playlist data into cache, then finish executing this method
							if (intoCacheOnly) {
								return deferred.resolve();
							}
							
							// Load the appropriate playlist data into the video player, set the appropriate playlist id, 
							// and clear the cached playlist data converted specifically for Akamai Media Player
							this.set({
								'playlistData': Ndn_PlayerServices_Cache.getPlaylistData(playlistId, playlistItemsLimit),
								'playlistId': playlistId,
								'ampFeedData': null
							});
							
							return deferred.resolve();
						}, this));
					}, this));
				}, this)).promise().done($.proxy(function() {
					// Indicate that the video player's playlist has been loaded/changed
					this.trigger('videoPlaylistLoad');
				}, this));
			},
			
			/**
			 * @param {number} videoIndex The index of the video to load into the player from the currently loaded playlist
			 * @param {Boolean} [playOnLoad=false] Whether the video begins playing immediately after it is loaded
			 */
			loadVideo: function(videoIndex, playOnLoad) {
				// console.log("Ndn_VideoPlayer.loadVideo(" + videoIndex + ', ' + playOnLoad + ');');			

				if (typeof playOnLoad == 'undefined') var playOnLoad = false;
				
				return _deliberate.call(this, 'loadAttempt', $.proxy(function() {
					_loadVideo.call(this, videoIndex, playOnLoad);
				}, this));				
			},
			
			/**
			 * Remove any traces of the last video loaded (if there was one)
			 */
			unloadVideo: function() {
				if (this.get('hasVideoLoaded')) {
					if (this.isShowingRmm()) {
						this.set('isShowingRmm', false);
						
						this.trigger('rmmUnload');
					}
					
					// Indicate that there is no video currently loaded
					this.set({
						'currentQuartile': null,
						'hasVideoLoaded': false,
						'adPlayer': null
					});
					
					this.trigger('videoUnload');
				}
			},
			
			/**
			 * @return {Object} Structured data concerning this video player's active playlist
			 */
			getActivePlaylistData: function() {
				return this.get('playlistData');
			},
			
			/**
			 * @return {Object} Structured data concerning this video player's active video
			 */
			getActiveVideoData: function() {
				return this.get('playlistData')['items'][this.get('currentVideoIndex')];
			},

			/**
			 * Plays the specified video (or whichever video is currently active in the player if no video is specified)
			 * @param {int} [videoIndex=0]
			 * @param {int} [playlistId]
			 */
			play: function(videoIndex, playlistId) {
				// console.log('Ndn_VideoPlayer.play(' + videoIndex + ', ' + playlistId + ');');
				
				if (typeof videoIndex == 'undefined' && typeof playlistId == 'undefined' && this.get('player').get('isReady')) { // this.getCurrentVideoData()) {
					// If there is already video data loaded, then simply call Akamai Media Player's "play" method
					this.get('player').play();
				}
				else {
					this.load(videoIndex, playlistId, true);
				}
			},
			
			/**
			 * Pause the video player
			 */
			pause: function() {
				this.get('player').pause();
			},
			
			/**
			 * @return jQueryElement The dom container for this video player (provided that it has been embedded already)
			 */
			getContainerElement: function() {
				// console.log('Ndn_VideoPlayer.getContainerElement(); playerContainerElementId ==', this.get('playerContainerElementId'));
				
				return $('#' + this.get('playerContainerElementId'));
			},

			/**
			 * Embeds this video player into the window's dom at the dom element with the specified "id" attribute, but only if this player 
			 * is not already embedded to the specified dom element
			 * @param {String} containerElementId The "id" attribute of the container element responsible for holding this video player
			 * @return {jQuery.Deferred}
			 */
			embed: function(containerElementId) {
				// console.log('Ndn_VideoPlayer.embed(' + containerElementId + ');');
				
				return $.Deferred($.proxy(function(deferred) {
                    // Make sure that the widget has its settings first from player services
                    this.get('widget').getSettings().done($.proxy(function(settings) {
                        if (!this.get('playerData')) {
                            this.set('playerData', settings);
                        }
                            
    					this.set('debugInfo', {
    						'ampVersion': Akamai_VideoPlayerModel.getVersion(),
    						'playerConfigDefaults': Akamai_VideoPlayerModel.getDebugConfig(),
    						'playerConfig': this.getPlayerConfig(true),
    						'videosLoaded': []
    					});
    					
    					// Store the "id" attribute of the container element that this video player is being embedded to
    					this.set('playerContainerElementId', containerElementId);
    					
    					// Create and store the new instance of the Akamai_VideoPlayerModel
    					this.set('player', new Akamai_VideoPlayerModel(containerElementId, this.getPlayerConfig(), this));
    					
    					this.initializeContainerElement();
    					
    					// Indicate that this video player has been embedded on the page
    					this.set('isEmbedded2', true);
    					this.trigger('embed');
    					
    					// console.log('about to resolve this Ndn_VideoPlayer.embed() method call!');
    					
    					deferred.resolve();
                    }, this));
				}, this)).promise();
			},
			
			initializeContainerElement: function() {
				var containerElement = 
				$('#' + this.get('playerContainerElementId'));
				
				containerElement.addClass('ndn_videoPlayer');
				
				// Add the responsive break-points to the video player's container element so that its CSS may be applied responsively
				var responsiveBreakPoints = {
					'min-width': [
						[419, 'largeView']
					],
					'max-width': [						
						[1198, 'view1198'],
						[1022, 'view1022'],
						[999, 'view999'],
						[849, 'view849'],
						[784, 'view784'],
						[766, 'view766'],
						[702, 'view702'],
						[639, 'view639'],
						[574, 'view574'],
						[499, 'view499'],
						[418, 'smallView']
					]
				};
				$.each(responsiveBreakPoints, function(feature, breakPoints) {
					$.each(breakPoints, function() {
						var value = this[0],
							className = 'ndn_videoPlayer_' + this[1];
						
						containerElement.responsiveContainer({
							feature: feature,
							value: value + 'px',
							className: className
						});
					});
				});
				
				// Trigger the "click" event on the video player whenever its container element is clicked
				(function(videoPlayer) {
					var domElement = containerElement.get(0),
						onClick = function() {
							videoPlayer.trigger('click');
						};
						
					if (domElement.addEventListener) {
						domElement.addEventListener('click', onClick, true);
					}
					else if (domElement.attachEvent) {
						domElement.attachEvent('onclick', onClick);
					}
				})(this);
			},
			
			/**
			 * @return {int} The point, measured in seconds, at which the current video is currently pointing to
			 */
			getCurrentTime: function() {
				return this.get('player').getCurrentTime();
			},
			
			/**
			 * @param {int} videoId The database id of the video to load into the video player
			 * @return {?int} The index of the specified video within the active playlist (or null if the specified video is not found within the active playlist)
			 */
			getVideoIndexById: function(videoId) {
				var videosData = this.getVideosData();
				for (var i = 0; i < videosData.length; i++) {
					var videoData = videosData[i];
					
					if (videoData['videoId'] == videoId) {
						return i;
					}
				}
				
				return null;
			},

			/**
			 * @return {String} The database id of the active video within the video player
			 */
			getCurrentVideoId: function() {
				return this.getVideoData(this.get('currentVideoIndex'))['videoId'];
			},
			
			/**
			 * @return {?int} The database id of the playlist currently loaded into the video player (or null if the video player's current playlist data does not specifically correspond to any playlist in our database)
			 */
			getCurrentPlaylistId: function() {
				return this.get('playlistId');
			},
			
			/**
			 * @param {int} videoId The database id of the video to load into the video player
			 * @param {Boolean} [playOnLoad=false] Whether the video begins playing immediately after it is loaded
			 */
			loadVideoById: function(videoId, playOnLoad) {
				Ndn_PlayerServices.getVideoData(videoId, this.get('widget'))
				.done($.proxy(function(videoData) {
					this.set({
						'playlistData': {
							'items': [videoData]
						},
						'playlistId': null,
						'ampFeedData': null
					});
					
					this.loadVideo(0, playOnLoad);
				}, this));
			},
			
			onceReady: function(callback) {
				return this.get('player').onceReady(callback);
			},
			
			showOverlay: function(overlayName) {
				if (typeof this.get('overlays')[overlayName] != 'undefined') {
					this.get('overlays')[overlayName].show();
				}
			},
			
			getOverlay: function(overlayName) {
				if (typeof this.get('overlays')[overlayName] != 'undefined') {
					return this.get('overlays')[overlayName];
				}
			},
			
			/**
			 * @param {Function} callback The closure to execute once this video player has been embedded on the page
			 * @return this
			 */
			onceEmbedded: function(callback) {
				// console.log('onceEmbedded, this.get("isEmbedded2") == ', this.get("isEmbedded2") ? 'true' : 'false');
				
				// this.get('player').onceReady(callback);
				
				if (this.get('isEmbedded2')) {
					callback();
				}
				else {
					this.once('embed', callback);
				}
				
				return this;
			},
			
			/**
			 * Reset the counter responsible for tracking how many videos have been played continuously without user-engagement
			 * TODO: Decouple the "truViewCounter" property from this call?
			 * @return this
			 */
			resetContinuousPlayCounter: function() {
				this.set({
					'continuousPlays': 0,
					'truViewCounter': 0
				});
				
				return this;
			},
			
			/**
			 * Increment the continuous plays counter
			 * @return this
			 */
			incrementContinuousPlayCounter: function() {
			    this.set('continuousPlays', this.get('continuousPlays') + 1);
			    return this;
			},
			
			/**
			 * @return {String} The playback mode of the video player; either "flash" or "html"
			 */
			getPlaybackMode: function() {
				return (Akamai_VideoPlayerModel.getPlaybackMode() == 'flash' && !this.isForcedHtml5Player()) ? 'flash' : 'html';
			},
			
			/**
			 * @return {Boolean} Whether this video player is (or will be) forcibly an HTML5 player (either because the Safari browser was detected or because the "forceHtmlPlayer" app setting is enabled)
			 */
			isForcedHtml5Player: function() {
				return Ndn_App.getSetting('forceHtmlPlayer'); // || Ndn_Utils_UserAgent.mayHavePowerSaverEnabled();
			},
			
			/**
			 * @return {Boolean} Whether rotating RMM (where RMM continuous play is greater than 0) is enabled
			 */
			isRotatingRmm: function() {
			    var widget = this.get('widget'),
			        rmmSettings = widget.getCachedSettings().settings.behavior.rmm;
			    
			    return rmmSettings = rmmSettings.enabled && rmmSettings.continuousPlay > 0;
			},
			
			/**
			 * @param {Boolean} [debug] Whether the debug player config should be returned; defaults to false
			 * @return {Object} Structured data concerning the config needed to construct an AMPPremier Akamai Media Player
			 */
			getPlayerConfig: function(debug) {
				if (typeof debug == 'undefined') debug = false;
				
				var widget = this.get('widget'),
				    widgetConfig = widget.get('config');
				
				var result = {
                    // controls: {},
                    share: {
                        // enabled: true,
                        facebook: true,
                        twitter: true
                    },
    		        feed: {
    		        	enabled: true
    		        }
				};
				
				// If this video player has RMM continuous play enabled, disable AMP's Comscore Video Metrix Multi-Platform analytics (this will be enabled outside of AMP)
				if (this.isRotatingRmm()) {
				    result = _.extend(result, {
				       comscore: {
				           enabled: false
				       },
				       comscorestreamsense: {
				           enabled: false
				       }
				    });
				}
				
				// Allow the ability to override the mode of the player
				if (this.isForcedHtml5Player()) {
					result.mode = 'html';
				}
				
				// If the smaller controls should be used for the flash player, do so in the resulting player config
				if (_.contains(['VideoPlayer/Inline300', 'VideoPlayer/PTR'], widgetConfig['type'])) {
					var templateParams;
					if (debug) {
						templateParams = Akamai_VideoPlayerModel.getDebugConfigParams();
					}
					
					result.flash = {
						xml: ampPremierSkinTemplates.getTemplate('ndn_assets_small', templateParams)
					};
				}
				
				// If the info panel is disabled, indicate such in the config override
				if (!this.get('infoPanelEnabled')) {
					result.info = {enabled: false};
				}
				
				return result;
			},
			
			/**
			 * @return {!Array<Object>} An array of structured data concerning each of the videos in the currently loaded playlist
			 */
			getVideosData: function() {
				var playlistData = this.get('playlistData');
				if (playlistData && typeof playlistData['items'] != 'undefined') {
					return playlistData['items'];
				}
				else {
					return [];
				}
			},
			
			/**
			 * @param {int} videoIndex The index of the video within this video player's currently loaded playlist
			 * @return {?Object} Structured data concerning the video within the currently loaded playlist at the specified index (or null if no such video is found)
			 */
			getVideoData: function(videoIndex) {
				var videosData = this.getVideosData();
				
				if (typeof videosData[videoIndex] != 'undefined') {
					return videosData[videoIndex];
				}
				else {
					return null;
				}
			},
			
			/**
			 * @return {int} The video index of the video currently loaded into the video player
			 */
			getCurrentVideoIndex: function() {
				return this.get('currentVideoIndex');
			},
			
			/**
			 * @return {?Object} Structured data concerning the current video within the currently loaded playlist (or null if no such video is found)
			 */
			getCurrentVideoData: function() {
				return this.getVideoData(this.get('currentVideoIndex'));
			},
			
			/**
			 * @return {?Object} Structured data concerning the video immediately after to the current video within the currently loaded playlist (or null if no such video is found)
			 */
			getNextVideoData: function() {
				return this.getVideoData(this.get('currentVideoIndex') + 1);
			},
			
			/**
			 * @return {?Object} Structured data concerning the video immediately prior to the current video within the currently loaded playlist (or null if no such video is found)
			 */
			getPreviousVideoData: function() {
				return this.getVideoData(this.get('currentVideoIndex') - 1);
			},
			
			/**
			 * @param {Object} videoData Structured data concerning a video within a playlist
			 * @param {Boolean} [isRmm=false] Whether this AMP feed video data is for an RMM
			 * @return {Object} Structured data concerning a video within a playlist, which is structured to how the AMPPremier class expects it
			 */
			getAmpFeedVideoData: function(videoData, isRmm) {
				// console.log('getAmpFeedVideoData(' + videoData + ', ' + widgetConfig + ', ' + isRmm + ');');
				
				if (typeof isRmm == 'undefined') isRmm = false;
				var isPtr = (this.get('widget').getSplitVersionId() == 'VideoPlayer/PTR');
				
				// Determine whether this video is a "bumper" video, meaning its length is only a short clip that is x-seconds long
				var isBumperVideo = isRmm || Ndn_App.getSetting('operationCwal');
				
				var widgetConfig = this.get('widget').get('config');
				
				var bumperVideoStartPosition = 5, // The time (in seconds) when a bumper video clip of a video's content begins
					bumperVideoDuration = 6; // The duration (in seconds) of a bumper video
				
				// TODO: Convert our existing publish date data into the expected format here. Note: this value cannot be the empty string as it will apparently break everything.
				// TODO: Why is this needed in the below config multiple times? Can it be needed in just one location?
				var pubDate = "Wed, 03 Aug 2011 10:00:21 -0400";
				
				var result = {
                    "@attributes" : {
                        "version" : "2.0"
                    },
                    "channel" : {
                        "category" : "",
                        "pubDate" : pubDate, 
                        "language" : "en",
                        "ttl" : "10",
                        "item" : {
	                        // TODO: Look into whether the below values need to be updated dynamically and figure out what they are for
                        	"title": "NDN - AMP Premier",
	                        "link": "http://akamai.com/player.html",
	                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc augue sapien, dapibus vitae imperdiet vitae, ullamcorper eget tortor. In lorem eros, porttitor quis commodo et, egestas a odio. Nunc eu augue sit amet dolor malesuada condimentum in eu velit. Donec commodo bibendum egestas. In viverra condimentum dapibus. Praesent molestie, dolor a laoreet cursus, massa sem varius tellus, at consectetur mauris ligula at dolor. Etiam semper augue et risus mattis iaculis tincidunt odio sodales. Vivamus eget elit quis metus pulvinar condimentum. Donec et vestibulum dui. Proin ultricies ligula sed felis pretium at adipiscing tortor bibendum. Morbi et ligula non massa facilisis lacinia. Duis at ligula mauris. Nullam ipsum tortor, lobortis eget adipiscing eu, sagittis eu sapien. Nunc dictum porttitor lobortis. Suspendisse cursus mollis lorem sed ultrices",
	                        "category": "",
	                        "pubDate": pubDate,
	                        /*
                            "title" : videoData['videoTitle'],
                            "link" : "http://akamai.com/player.html",
                            "description" : videoData['description'], // "AMP Premier Player",
                            "category" : "",
                            "pubDate" : "Wed, 03 Aug 2011 10:00:21 -0400",
                            */
	                        
	                        "guid": _getVideoAssetId(videoData['videoId'], isRmm || isPtr),
	                        
                            "ndn-provider" : {
                                "id": videoData['provider']['trackingGroup'],
                                "name": videoData['provider']['name'],
                                "image": videoData['provider']['logoUrl']
                            },
                            "media-group" : {
                            	"media-content" : (function(mediaContentItems) {
                            		var result = [];
                            		
                            		for (var i = 0; i < mediaContentItems.length; i++) {
                            			var mediaContentItem = mediaContentItems[i];
                            			
                            			var mediaContentAttributes = {
                                           "@attributes": {
                                        	   "url": (function() {
                                        		   var result = mediaContentItem['url'].replace(' ', ''); // + (isRmm ? '?start=5&end=11' : '');
                                        		   
                                        		   // If this video's URL uses the "rtmp://" protocol and has a ".flv" file extension, strip off the 
                                        		   // ".flv" file extension for the sake of the Akamai Media Player
                                        		   var regexMatches = /^(rtmp:.*)\.flv$/ig.exec(result);
                                        		   if (regexMatches) {
                                        			   result = regexMatches[1];
                                        		   }
                                        		   
                                        		   // If the video's URL has ".f4m?" in its URL, then add "&hdcore=1" to the end of the URL per Frank's instructions in [PLAYER-2582]
                                        		   if (/\.f4m\?/.exec(result)) {
                                        		       result += '&hdcore=1';
                                        		   }
                                        		   
                                        		   return result;
                                        	   })(),                           						
                                        	   "medium": mediaContentItem['medium'],
                                        	   "type": mediaContentItem['type'],
                                        	   'duration': isBumperVideo ? bumperVideoDuration : mediaContentItem['duration'],

                                        	   // TODO: Can these two hardcoded values below be removed?
                                        	   // TODO: Make these values dynamic (if needed)
       		                                   "width": 640,
       		                                   "height": 360
                                           }
                            			};

                            			// Add mime-type labels if applicable
                            			var map = {
                            				'video/mp4': "PDL",
                            				'video/flv': 'RMTP',
                            				'video/x-flv': 'RMTP',
                            				'application/x-mpeg': 'HLS',
                            				'application/x-mpegURL': 'HLS',
                            				'video/f4m': 'HDS'
                            			};
                            			if (typeof map[mediaContentItem['type']] != 'undefined') {
                            				mediaContentAttributes['media-category'] = {
                                         	   "@attributes": {
                                        		   "schema": "http:\/\/mrss.akamai.com\/user_agent_hint",
                                        		   "label": map[mediaContentItem['type']]
                                        	   }
                                           };
                            			}
                            			
                            			// TODO: Make sure this logic works so it determines which one is truly "isDefault"; maybe it checks if it has *.mp4 extension?
                            			if (i == 0) {
                            				mediaContentAttributes['@attributes']['isDefault'] = 'true';
                            			}
                            			
                            			result.push(mediaContentAttributes);
                            		}
                            		
                            		return result;
                            	})(videoData['mediaContent']),
                            	"media-title" : videoData['videoTitle'],
                                "media-description": videoData['description'],
                                "media-thumbnail" : {
                                    "@attributes" : {
                                        "url": videoData['stillImageUrl'],
                                        "width": widgetConfig['width'] || "604",
                                        "height": widgetConfig['height'] || "341"
                                    }
                                }
                            }
                		}
                    }
                };
				
				var playerData = this.get('playerData'),
					displaySocialIcons = playerData.settings.share.enabled && 
						(playerData.settings.share.socialNetworks.enabled && videoData.settings.share.embedCode.enabled);
				
				// If social icons should be displayed on the control bar
				if (displaySocialIcons) {
					result.channel.item['media-group']['media-embed'] = {
                        "@attributes" : {
                            "url" : "http://player.js",
                            "width" : widgetConfig['width'] || "604",
                            "height" : widgetConfig['height'] || "341"
                        },
                        "media-param" : {
                            "@attributes" : {
                                "name" : "type"
                            },
                            "#text" : "text/javascript"
                        }
                    };
				}
				
				if (isBumperVideo) {
					result.channel.item['media-group']['akamai-subclip'] = {
						"akamai-start": bumperVideoStartPosition + '',
						"akamai-end": (bumperVideoStartPosition + bumperVideoDuration) + ''
					};
				}
				
				return result;
			},
			
			/**
			 * @param {Boolean} [useCache=true] Whether the cached result should be returned when this method was called last time (unless the cache has been cleared)
			 * @return {Array<Object>} Structured data concerning the videos for this playlist in the format that the AMPPremier library expects video data in
			 */
			getAmpFeedData: function(useCache) {
				if (typeof useCache == 'undefined') var useCache = true;
				
				// If the cache should be used and there is feed data cached, return the cached feed data
				if (useCache && this.get('ampFeedData')) {
					return this.get('ampFeedData');
				}
				
				// Create the needed AMP feed data 
				var result = [];
				var playlistData = this.get('playlistData');
				var playlistItems = playlistData['items'];
				for (var i = 0; i < playlistItems.length; i++) {
					var videoData = playlistItems[i];
					result.push(this.getAmpFeedVideoData(videoData));
				}
				
				// Cache the result
				this.set('ampFeedData', result);
				
				return result;
			},
			
			/**
			 * @return {String} The currently loaded playlist database id, or null if no playlist from the database is explicitly loaded
			 */
			getPlaylistId: function() {
				return this.get('playlistId');
			},
			
			/**
			 * @param {Boolean} toggle The default "playOnLoad" parameter value for the load() method
			 * @return this
			 */
			setDefaultPlayOnLoadValue: function(toggle) {
				// console.log('Ndn_VideoPlayer.setDefaultPlayOnLoadValue(' + toggle + ');');
				
				this.set('defaultPlayOnLoadValue', toggle);
				
				return this;
			},
			
			/**
			 * @param {Boolean} toggle Whether the video player should be muted. If the value of true is passed to this method, the video player's volume 
			 * is set to 1% (and "muted") and an interval enforces that this state remains as-is every x-number of milliseconds. If the value of false is
			 * passed to this method, the video player is "unmuted" and its volume is set to 75%, while removing any interval enforcing the "muted" state 
			 * of the video player.
			 */
			setMuted: function(toggle) {
                if (this.get('player').setMuted) {
                    this.get('player').setMuted(toggle);
                }
			},

			/**
             * @param {Boolean} [checkPlayerInstanceVolume] Whether to check actual player instance volume
             * @return {Boolean} Whether the video player is muted
			 */
			isMuted: function(checkPlayerInstanceVolume) {
				return this.get('player').isMuted(checkPlayerInstanceVolume);
			},

			/**
			 * @param {float} volume Value to set the player's volume to (between 0 and 1)
			 */
			setVolume: function(volume) {
				this.get('player').setVolume(volume);
			},

			/**
			 * @return {float} The current volume of the player
			 */
			getVolume: function() {				
				return this.get('player').getVolume();
			},

			/**
			 * Enables the control bar on the video player
			 */
			enableControls: function() {			
				this.get('player').enableControls();
			},
			
			/**
			 * Removes the control bar from the video player so that it no longer visible
			 */
			disableControls: function() {
				this.get('player').disableControls();
			},

			/**
			 * @param {String} playlistId The playlist id of the playlist to load into the video player once the currently loaded playlist has finished playing
			 */
			setNextPlaylistId: function(playlistId) {
				this.set({
					'nextPlaylistId': playlistId
				});
			},
			
			/**
			 * @return {String} The playlist id of the playlist to load into the video player once the currently loaded playlist has finished playing
			 */
			getNextPlaylistId: function() {
				return this.get('nextPlaylistId'); // ? this.get('nextPlaylistId') : 14126;
			},
			
			/**
			 * @param {int} limit The maximum number of playlist items that may be loaded into this video player's current playlist data
			 */
			setPlaylistItemsLimit: function(limit) {
				this.set('playlistItemsLimit', limit);
			},
			
			/**
			 * @return {Boolean} Whether advertisements are enabled for this video player
			 */
			hasAdsEnabled: function() {
				return !!this.get('widget').get('config')['adsEnabled'];
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently showing video content
			 */
			isShowingVideo: function() {
				return this.get('isShowingVideo');
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently playing (agnostic of whether content or an advertisement is playing)
			 */
			isPlaying: function() {
				return this.get('isPlaying');
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently showing an advertisement
			 */
			isShowingAd: function() {
				return this.get('isShowingAd');
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently showing video content
			 */
			isShowingVideo: function() {
				return this.get('isShowingVideo');
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently playing an advertisement
			 */
			isPlayingAd: function() {
				return this.isPlaying() && this.isShowingAd();
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently playing video content
			 */
			isPlayingVideo: function() {
				return this.isPlaying() && this.isShowingVideo();
			},
			
			/**
			 * NOTE: This operates slightly differently than the .isShowingAd() and .isShowingVideo() methods; whereas, it is not determined by listening for events in this instance of Ndn_VideoPlayer
			 * 
			 * @return {Boolean} Whether the video player is currently showing a Rich-Media Monetization (RMM), which is true when either an RMM ad or RMM bumper is showing
			 */
			isShowingRmm: function() {
				return this.get('isShowingRmm');
			},
			
			/**
			 * @return {Boolean} Whether the video player is currently within the "extended preview" mode (PTR)
			 */
			isShowingPtr: function() {
			    return this.get('widget').getSplitVersionModel().isPtr();
			},
			
            /**
             * Resets the ad conversion counts to 0.
             */
            _resetAdConversions: function() {
                var adEvents = _.extend({}, this.get('currentAdEvents'));
                _.each(adEvents, function(value, key, obj) { obj[key] = 0; });
                this.set('currentAdEvents', adEvents);
            },

			/**
			 * @param {String} eventName The name of the event that allows for deliberation
			 * @param {jQuery.Deferred} deliberator The deliberator that approves or denies of the specified event to proceed as normal
			 */
			addDeliberator: function(eventName, deliberator) {
				if (this.get('deliberators')[eventName]) {
					this.get('deliberators')[eventName].push(deliberator);
				}
			},
			
			getDebugInfo: function() {
				var result = this.get('debugInfo');
				
				// Add this value back in when the debug info is being requested (the below value may or may not have been defined yet, which indicates whether any Akamai Media Player code has even been executed yet)
				result['playerConfigDefaults'] = Akamai_VideoPlayerModel.getDebugConfig();
				
				return result;
			}
		});
	}
);
