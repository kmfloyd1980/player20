var _nw2e = _nw2e || [];
var _ndnq = _ndnq || [];

/**
 * Communication layer between Ndn_Widget and window.postMessage() for Chrome extension
 */
define([
    'underscore',
    'jquery',
    'require',
    'backbone',
    'models/Ndn/Debugger',
    'models/Ndn/Utils/UrlParser'
], function(
    _,
    $,
    require,
    Backbone,
    Ndn_Debugger,
    Ndn_Utils_UrlParser
) {
	// If the current browser does not allow for window.postMessage() etc. then don't continue executing any code for this module
	if (!(window.addEventListener && window.postMessage && top && top.postMessage)) return;

	// TODO: Remove the need for this pre-processing hack
	var _processConfig = function(config) {
		var result = _.extend({}, config);
		
		// HACK: Changing the reported "type" config setting to get reported as "VideoPlayer/Single" although the NDN Player Suite understands it as "VideoPlayer/Default"
		if (result.type && (result.type + '').toLowerCase() == 'videoplayer/default') {
			result.type = 'VideoPlayer/Single';
		}
		
		return result;
	};

	var _getTime = function() {
		return Date.now ? Date.now() : (new Date().valueOf());
	};
	
	/**
	 * Whether communication with the NDN Player Suite Chrome Extension has been initialized
	 * @var {Boolean}
	 */
	var _isChromeExtCommunicationInitialized = false;
	
	/**
	 * The origin needed to send messages (via the .postMessage() method) to the NDN Player Suite Chrome Extension
	 * @var {String}
	 */
	var _chromeExtensionOrigin;
	
	var _observer = new Backbone.Model();
	
	var _createCloneableEventData = function(eventData) {
		var result = {
			'public': _.clone(_.extend({}, {
				containerElementId: eventData['public'].containerElement.id
			}, _.clone(eventData['public']))),
			'system': _.clone(_.extend({}, _.clone(eventData['system'])))
		};
		
		// Remove these two items as they are not clone-friendly
		delete result['public'].containerElement;
		delete result.system.widget;
		
		return result;
	};
	
	var Ndn_ChromeExtLayer = {
		initialize: function(chromeExtensionOrigin) {
			if (!_isChromeExtCommunicationInitialized) {
				// Remember the Chrome Extension's target origin used for responding to received messages
				_chromeExtensionOrigin = chromeExtensionOrigin;
				
				this.initializeEventListeners();
				
				// Indicate that the communication with the Chrome Extension has been initialized
				_isChromeExtCommunicationInitialized = true;
			}
		},
		
		initializeEventListeners: function() {
			// Whenever an embedded product is destructed, unregister said embedded product with the Chrome Extension
			_ndnq.push(['hook', '*/destruct', function(event) {
				Ndn_ChromeExtLayer.sendMessage('widgetDestruct', {
					'containerElementId': event.data.containerElementId
				});
			}]);
			
			/**
			 * The "id" attribute of this product's <iframe> container element (or null if the products within this frame are not within an iframe)
			 * @var {String}
			 */
			var iframeContainerElementId;
			
			// Keep track of the last embedded product that detected the "mousedown" event within it as well as the timestamp of when this event occurred
			var lastClickedContainerId;
			var lastClickedTimestamp;
			$('body').on('mousedown', '.ndn_embedded', function(e) {
				// console.log('******* (*) mousedown, e ==', e);
				lastClickedContainerId = $(e.currentTarget).attr('id');
				lastClickedTimestamp = _getTime();
			});
			
			// Load the Ndn_App and Ndn_Widget modules
			require(['models/Ndn/App', 'models/Ndn/Widget', 'models/Ndn/Hooks'], function(Ndn_App, Ndn_Widget, Ndn_Hooks) {
				Ndn_ChromeExtLayer.on({
					'getEmbedData': function(messageData) {
						var sendEmbedData = function(widget) {
							if (Ndn_App.isEmbeddedByIframe() && (messageData._nw2eContainerId || messageData.containerId)) {
								iframeContainerElementId = (messageData._nw2eContainerId || messageData.containerId);
							}
							
							widget.getSettings().done(function(settings) {
								Ndn_ChromeExtLayer.sendMessage('widgetEmbed', {
									widget: {
										container: {
											width: widget.getContainerElement().width(),
											height: widget.getContainerElement().height()
										},
										isIframe: Ndn_App.isEmbeddedByIframe(),
										containerId: messageData._nw2eContainerId || messageData.containerId || widget.getContainerElement().attr('id'),
										initialConfig: _processConfig(widget.get('initialConfig')),
										config: _processConfig(widget.get('config')),
										appUrl: Ndn_Widget.getAppUrl(),
										appDomain: Ndn_Widget.getAppDomain(),
										envOverrides: Ndn_App.getEnvOverrides(),
										appSettings: {
		                                    'default': Ndn_App.getDefaultSettings(),
		                                    'overrides': Ndn_App.getOverrideSettings(),
		                                    'used': Ndn_App.getSettings()
		                                },
		                                isStoringEventTimelineData: Ndn_Debugger.isEnabled(),
		                                debugModes: Ndn_Debugger.getEnabledModesList(),
										settings: settings
									}
								});
							});
						};
						
						// Send window.postMessage() for each widget in the future that begins its embed process
						Ndn_Hooks.on('*/widgetEmbedEnd', function(event) {
							sendEmbedData(event.system.widget);
						});
						
						// Send embed data for each product that has already begun its embed process
						$.each(Ndn_App.getRegisteredEmbeds(), function() {
							var product = this;
							this.onceConfigIsComplete(function() {
								sendEmbedData(product);
							});
						});
					},
					
					'getProductTimelineData?': function(messageData) {
					    // console.log('>>> getProductTimelineData? received! messageData ==', messageData);
					    // console.log('Ndn_App.isEmbeddedByIframe() ==', Ndn_App.isEmbeddedByIframe());
					    // console.log("(_nw2e.iframeId || Ndn_Utils_UrlParser.getUrlParam('ndn_iframeId') ==", (_nw2e.iframeId || Ndn_Utils_UrlParser.getUrlParam('ndn_iframeId')));
					    
						var containerElementId = messageData._nw2eContainerId || messageData.containerId;
						
						require(['models/Ndn/EventHub', 'models/Ndn/App', 'models/Ndn/Utils/UrlParser'], function(Ndn_EventHub, Ndn_App, Ndn_Utils_UrlParser) {
						    // If this embedded product is within an iframe and it is not supposed to refer to this data request received, then do nothing with this data request
						    if (Ndn_App.isEmbeddedByIframe() && (_nw2e.iframeId || Ndn_Utils_UrlParser.getUrlParam('ndn_iframeId')) != containerElementId) {
						        // console.log('!! containerElementId ==' + containerElementId);
						        return;
						    }
						    
						    var internalContainerId = Ndn_App.isEmbeddedByIframe()
                                ? 'landing-page-embed-container'
                                : containerElementId;
						    
						    // console.log('internalContainerId ==', internalContainerId);
						    
							// Ensure that the timeline data includes only cloneable elements, which can be transferred using the window.postMessage() method
							var timelineEventsData = Ndn_EventHub.getProductTimelineData('analytics', internalContainerId) || [];
							var resultingTimelineEventsData = [];
							
							// console.log('timelineEventsData ==', timelineEventsData);
							
							$.each(timelineEventsData, function(eventIndex, eventData) {
								resultingTimelineEventsData[eventIndex] = _createCloneableEventData(eventData);
							});
							
							/**
							 * Structured data concerning the event data received from the NDN Player Suite. Format:
							 * <pre>{
							 * 	String: Object, // The event identifier => the event's data
							 * 	... // All other event data received
							 * }</pre>
							 * @var {Object}
							 */
							var eventDataReceived = {};
							
							/**
							 * Structured data mapping the event's timestamp to the event's identifier. Format:
							 * <pre>{
							 * 	Number: String, // A received event's timestamp => the event's identifier (for example, "analytics:landing-page-container-element/videoLoad[0]")
							 * 	... // All other received events
							 * }</pre>
							 * @var {Object}
							 */
							var eventTimestamps = {};
							
							/**
							 * List of timestamps received that correspond with event timestamps
							 * @var {Array}
							 */
							var timestampsReceived = [];
							
							// Record when each of the events in the timeline occurred as well as each event's identifier
							$.each(resultingTimelineEventsData, function() {
								var eventData = this,
									timestamp = eventData.system.timestamp,
									eventIdentifier = 'analytics:' + eventData['public'].containerElementId + '/' + eventData['public'].eventName + '[' + eventData.system.containerElementEventTypeIndex + ']';
								
								timestampsReceived.push(timestamp);
								eventTimestamps[timestamp] = eventIdentifier;
								eventDataReceived[eventIdentifier] = eventData;
							});
							
							// Get the timeline events sorted by when they occurred
							resultingTimelineEventsData = (function() {
								var result = [];
								
								// Sort the timestamps received
								timestampsReceived.sort(function(a, b) { return a - b; });
								
								$.each(timestampsReceived, function() {
									var eventIdentifier = eventTimestamps[this];
									result.push({
										eventIdentifier: eventIdentifier,
										eventData: eventDataReceived[eventIdentifier]
									});
								});
								
								return result;
							})();
							
							var dataToSendSoon = {
                                events: resultingTimelineEventsData,
                                containerId: containerElementId
                            };
							
							// console.log('dataToSendSoon ==', dataToSendSoon);
							
							Ndn_ChromeExtLayer.sendMessage('getProductTimelineData', {
								events: resultingTimelineEventsData,
								containerId: containerElementId
							});
						});
					},
					
					'getLastClickedContainerData': function(messageData) {
						// console.log('HEARD: getLastClickedContainerData, messageData ==', messageData);
						
						var payload = {
							containerId: iframeContainerElementId || lastClickedContainerId,
							timestamp: lastClickedTimestamp,
							_nw2eMessageId: messageData._nw2eMessageId
						};
						
						// console.log('SENDING (>>> * >>>) PAYLOAD: ', payload);
						
						Ndn_ChromeExtLayer.sendMessage('reply', payload);
					},
					
					'requestDebugInfo': function(messageData) {
						$.each(Ndn_App.getRegisteredEmbeds(), function() {
							if (this.getContainerElement().attr('id') == messageData.containerId || Ndn_App.isEmbeddedByIframe()) {
								Ndn_ChromeExtLayer.sendMessage('widgetDebugInfo', {
									'containerId': messageData.containerId,
									'debugInfo': this.getDebugInfo()
								});
							}
						});
					},
					
					'legacyMigrationAddedEmbedJs?': function(messageData) {
						Ndn_ChromeExtLayer.sendMessage('legacyMigrationAddedEmbedJs', {
							value: _nw2e.legacyMigrationAddedEmbedJs
						});
					},
					
					'pauseOthers': function(messageData) {
						$.each(Ndn_App.getRegisteredEmbeds(), function() {
							if (this.getContainerElement().attr('id') != messageData.containerId) {
								_ndnq.push(['command', this.getContainerElement().attr('id') + '/videoPause']);
							}
						});
					},
					
					'pause': function(messageData) {
						$.each(Ndn_App.getRegisteredEmbeds(), function() {
							if (this.getContainerElement().attr('id') == messageData.containerId || Ndn_App.isEmbeddedByIframe()) {
								_ndnq.push(['command', this.getContainerElement().attr('id') + '/videoPause']);
							}
						});
					},
					
					'all': function() {
						// console.log('Ndn_ChromeExtLayer: MESSAGE RECEIVED, ', arguments);
					}
				});
			});
		},
		
		processMessage: function(message) {
			if (message && message.data && message.data._nw2e && message.data._nw2eOrigin) {
				Ndn_ChromeExtLayer.initialize(message.data._nw2eOrigin);
				// console.log('processMessage(' + message.data._nw2e + ')........');
				_observer.trigger(message.data._nw2e, message.data);
			}
		},
		
		on: function() {
			_observer.on.apply(_observer, arguments);
		},
		
		once: function() {
			_observer.once.apply(_observer, arguments);
		},
		
		sendMessage: function(subject, data) {
			var messageData = $.extend({
				_nw2e: subject,
				_nw2eOrigin: window.location.protocol + '//' + window.location.hostname
			}, data);
			
			// Get rid of functions that cannot be passed using window.postMessage()
			messageData = JSON.parse(JSON.stringify(messageData));
			
			top.postMessage(messageData, _chromeExtensionOrigin);
		},
		
		reply: function(messageId, data) {
			this.sendMessage('reply', $.extend({
				_nw2eMessageId: messageId
			}, data));
		}
	};
	
	// Once the application has been initialized, begin listening for messages that may come from the NDN Player Suite Chrome Extension
	_ndnq.push(['when', 'initialized', function() {
		window.addEventListener("message", function(message) {
			Ndn_ChromeExtLayer.processMessage(message);
		});
	}]);
});