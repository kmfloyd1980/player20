/**
 * An instance of this class can observe widgets and proxy their events to be listened to via access to the Ndn_Widget.addHook() method.
 */
define(['backbone'], function(Backbone) {
	/**
	 * The observable object to hijack its .trigger() and .on() methods from for this library
	 * @var {Backbone.Model}
	 */
	var _observable = new Backbone.Model();
	
	/**
	 * 
	 * @var {Backbone.Model}
	 */
	var _publicApiAccess = new Backbone.Model();
	
	var Ndn_Hooks = {
		/**
		 * @return {Backbone.Model} The object responsible for triggering events with event data strictly limited to NDN Player Suite's public JS API
		 */
		getPublicApiAccess: function() {
			return _publicApiAccess;
		},

		trigger: function() {
			return Backbone.Model.prototype.trigger.apply(_observable, arguments);
		},
		
		on: function() {
			return Backbone.Model.prototype.on.apply(_observable, arguments);
		},
		
		once: function() {
			return Backbone.Model.prototype.once.apply(_observable, arguments);
		}
	};
	
	// Only reveal the allowed hook data to be accessible to NDN Player Suite's public JS API 
	Ndn_Hooks.on('all', function(eventName, eventData) {
		Ndn_Hooks.getPublicApiAccess().trigger(eventName, eventData['public']);
	});
	
	return Ndn_Hooks;
});