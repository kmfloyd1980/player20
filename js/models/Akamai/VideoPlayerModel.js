var _nw2e = _nw2e || [];

define([
    'jquery',
    'underscore',
    'backbone',
    'models/Ndn/App',
    'models/Ndn/Widget',
    'models/Ndn/Analytics',
    'models/Ndn/Utils/UserAgent',
    'models/Ndn/Debugger',
    
    'lib/amp.premier/config/skin/templates',
    
    'amp_lib'
], function(
	$,
	_,
	Backbone,
    Ndn_App,
    Ndn_Widget,
	Ndn_Analytics,
	Ndn_Utils_UserAgent,
	Ndn_Debugger,
	
	ampPremierSkinTemplates
) {
	// Force swfobject to presume that the dom ready event has already occurred
	swfobject.callDomLoadFunctions();
	
	// Store a reference to the version we want to use and then replace any overwritten variables (from the initialization 
	// of our Akamai Media Player) so that other initializations of the Akamai Media Player load appropriately
	_nw2e.videoPlayer = AKAMAI_MEDIA_PLAYER.noConflict();
	
	/**
	 * @param {Object} params Parameters used for creating AMPPremier's config object (optional parameters may be used to simulate how this config 
	 * should be rendered for Akamai Media Player without our wrapper library so we can test differences between our products and Akamai's build pages, 
	 * for example: http://projects.mediadev.edgesuite.net/customers/akamai/mdt-html5-core/ndn/0.1.0047/samples/index.html). Format:
	 * <pre>{
	 * 	'adServer': {String}, // The ad server to use; recognized values: "dfp", "Auditude"
	 * 	'adOverlayText': {String}, // The text to be added before the number of seconds counting down until the end of the current ad being shown
	 * 	'akamaiJsLibFolderUrl': {String}, // Optional, URL to the folder containing the Akamai Media Player's JavaScript files
	 * 	'akamaiResourcesFolderUrl': {String}, // Optional, URL to the folder containing the Akamai Media Player's resource files
	 * 	'windowLocation': {String}, // Optional, URL to the current page (this can be overridden for creating a debug config)
	 * }</pre>
	 * @private
	 */
	var _getConfig = function(params) {
		// If the following optional parameters were not provided, set their default values
		params.akamaiJsLibFolderUrl = params.akamaiJsLibFolderUrl || Ndn_Widget.getAppUrl() + "/js/lib/amp.premier";
		params.akamaiResourcesFolderUrl = params.akamaiResourcesFolderUrl || Ndn_Widget.getAppUrl() + "/resources";
		params.windowLocation = params.windowLocation || (window.location.href + '');
		
		// Determine what the URL to the AkamaiAdvancedStreamingPlugin is
		params.aaspUrl = window.location.protocol == 'http:'
			? 'http://players.edgesuite.net/flash/plugins/osmf/advanced-streaming-plugin/v3.4/osmf2.0/AkamaiAdvancedStreamingPlugin.swf'
			: params.akamaiResourcesFolderUrl + '/plugins/AkamaiAdvancedStreamingPlugin.swf';
		
		// This data is used a couple of times in the config below
		var hintingRules = "%7B%22flashTablets%22%3A%7B%22label%22%3A%22Android%202%20%26%203%20or%20Kindle%20Fire%201%22%2C%22regexp%22%3A%22Android%20%5B23%5D%7CSilk/1%22%7D%2C%22html5Phones%22%3A%7B%22label%22%3A%22iPhone%22%2C%22regexp%22%3A%22iPhone%22%7D%2C%22html5Tablets%22%3A%7B%22label%22%3A%22HTML5%20Tablets%22%2C%22regexp%22%3A%22iPad%7CAndroid%204%7CSilk/2%22%7D%2C%22desktop%22%3A%7B%22label%22%3A%22Desktop%22%2C%22regexp%22%3A%22%5E%28%28%3F%21iPad%7CiPhone%7CAndroid%7CBlackBerry%7CPlayBook%7CSilk%29.%29*%24%22%7D%2C%22android_4_gets_m3u8%22%3A%7B%22regexp%22%3A%22Android%204%22%7D%7D";
		
		return {
		    "resources": [],
	        "domain": "newsinc.com",
	        "target": "_top",
	        "params": {
	        	'auditude_userDataString': '',
	        	'distributorName': '',
	        	producerCategory: '',
	        	producerAuditudeId: '',
	        	windowLocation: params.windowLocation,
	        	
	        	/**/
	            "tracking_group": 9999,
	            "sitesection": "ndn",
	            "sec": "oth",
	            "sub": "non",
	            "plt": "11",
	            "wgt": 1,
	            "fp": 0,
	            "aud_zone_id": 50974,
	            "aud_default_asset": "default_ndn",
	            "partner_type": "conventional",
	            "autoplay": 1,
	            "continuous_play": 1,
	            "sound": 1,
	            "ad_server": "none",
	            "trans_time": 0,
	            "size": "10x10",
	            "player_type": 11,
	            "launcher_type": 0,
	            "ads_enabled": 0,
	            "wid": 0,
	            "share": 1,
	            "email": 0,
	            "device_mode": 1,
	            "external_url": "http://newsinc.com",
	            "override_playlist": 0,
	            "user_token": "ZZZZZZZ",
	            "embed_type": 0,
	            "ndn_session_id": "",
	            "rmm": 0,
	            "wid_type": 0,
	            "desc_url_type": "http://newsinc.com"
	            /**/
	        },
	        "fullscreen": {
	            "native": true
	        },
	        "controls": {
	            "autoHide": 5,
	            "mode": "auto"
	        },
	        "info": {
	            "title": "#{media.title}",
	            "description": "#{media.description}",
	            "provider": {
	                "name": "#{media.metadata['ndn-provider'].name}",
	                "image": "#{media.metadata['ndn-provider'].image}"
	            }
	        },
	        "flash": {
	            "swf": params.akamaiJsLibFolderUrl + "/AkamaiPremierPlayer.swf",
	            "debug": params.akamaiJsLibFolderUrl + "/AkamaiPremierPlayer-debug.swf",
	            "vars": {
	                core_ads_enabled: false,
            		ad_server_timeout: 30, // The timeout for the ad server to return an ad to load into the player
            		ad_media_server_timeout: 30, // The timeout for ad media to load into the player
	            	display_scene_markers: false,
	            	context_menu_mode: "short",
	                overlay_ad_delay: 3,
	                volume: 1,
	                auto_replay: false,
	                fullscreen_enabled: true,
	                share_enabled: true,
	                auto_play: false,
	                auto_play_override: true,
	                hinting_rules: hintingRules,
	                externalTarget: "_top",
	                domain: "newsinc.com",
	                embedDomain: "newsinc.com",
	                show_play_button_overlay: false,
	                auto_play_list: false,
	                core_ads_enabled: true,
	                next_video_timer: -1
	            },
	            /**/
	            "static": {
	            	enabled: true,
	            	swf: params.akamaiJsLibFolderUrl + "/AkamaiPremierPlayer-static.swf",
	            	debug: params.akamaiJsLibFolderUrl + "/AkamaiPremierPlayer-static-debug.swf",
	            	vars: {
	            		ad_server_timeout: 30, // The timeout for the ad server to return an ad to load into the player
	            		ad_media_server_timeout: 30, // The timeout for ad media to load into the player
		            	context_menu_mode: "short",
		                overlay_ad_delay: 3,
		                volume: 1,
		                auto_replay: false,
		                fullscreen_enabled: true,
		                share_enabled: true,
		                auto_play: false,
		                hinting_rules: hintingRules,
		                externalTarget: "_top",
		                domain: "newsinc.com",
		                embedDomain: "newsinc.com",
		                show_play_button_overlay: false,
		                auto_play_list: false,
		                core_ads_enabled: true,
		                next_video_timer: -1
		            },
		            "params":{
			             "wmode": (Ndn_Utils_UserAgent.isIe() || Ndn_Utils_UserAgent.isFirefox() || (Ndn_Utils_UserAgent.isSafari() && Ndn_Utils_UserAgent.isWindows())) ? 'transparent' : 'direct'
		            },
		            "xml": ampPremierSkinTemplates.getTemplate('ndn_assets', params)
	            },
	            /**/
	            "params":{
		             "wmode": (Ndn_Utils_UserAgent.isIe() || Ndn_Utils_UserAgent.isFirefox() || (Ndn_Utils_UserAgent.isSafari() && Ndn_Utils_UserAgent.isWindows())) ? 'transparent' : 'direct'
	            },
	            "xml": ampPremierSkinTemplates.getTemplate('ndn_assets', params)
	        },
	        "share": {
	            "facebook": true,
	            "twitter": true,
	            "enabled": true
	        },
	        "comscore": {
	            "url": Ndn_Widget.isOnSsl()
	            	? "https://sb.scorecardresearch.com/b"
	            	: "http://b.scorecardresearch.com/b",
	            "data": {
	                "c1": '1',
                    "c2": "11112732",
	                "c3": "#{distributorName}",
	                "c4": "#{media.metadata['ndn-provider'].name}",
	                "c5": "",
	                "c6": "#{siteSection}",
	                "c10": "#{producerCategory}"
	            },
	            events: {
	            	ads: [
                    	{
                    		type: "started", 
                    		data: {
                    			c4: "#{media.metadata['ndn-provider'].name} 09",
                    			c5: '09'
                    		}
                    	}
                    ],
                    video: [
                    	{
                    		type: "started", 
                    		data: {
                    			c4: "#{media.metadata['ndn-provider'].name} #{mediaAnalyticsC5Value}",
                    			c5: "#{mediaAnalyticsC5Value}"
                    		}
                    	}
                    ]
	            }
	        },
	        comscorestreamsense: {
	            enabled: true,
                resources: [
                    {
                       type: 'text/javascript',
                       src: params.akamaiResourcesFolderUrl + '/js/streamsense.4.1412.05.min.js'
                    }
                ],
	        	data: {
	        		clientId: "11112732",
	        		publisherSecret: "c3798e9f708f2d61a9cf8232b0503d5a",
	        		metadata: {
    	        		ci: "#{media.guid}",
    	        		c3: "#{distributorName}",
    	        		c4: "#{media.metadata['ndn-provider'].name}",
    	        		c6: "#{siteSection}"
        		    }
	        	}
	        },
	        ima: {
	            version: 3,
                resources: [
                    {
                       type: 'text/javascript',
                       src: window.location.protocol + '//s0.2mdn.net/instream/html5/ima3.js'
                    }
                ],
                vpaidAllowed: true,
                vpaidMode: "enabled",
	            companion: {
	            	width: '300',
	            	height: '250'
	            },
	            // displaySceneMarkers: true,
	            adTagUrl: window.location.protocol + '//pubads.g.doubleclick.net/gampad/ads' +
	            	'?ad_rule=1' +
	            	'&sz=750x425' +
	            	'&iu=#{dfpIuValue}' + 
	            	'&ciu_szs=#{dfpCiuSzsValue}' + 
	            	'&vid=#{media.guid}' + 
	            	'&cmsid=1169' +
	            	'&vpos=preroll' + 
	            	'&scor=#{now}' + 
	            	'&vad_type=linear' + 
	            	'&description_url=#{encodedEmbedOriginUrl}' + 
	            	'&playlistId=#{playlistId}' +
	            	'&widgetId=#{widgetId}' +
	            	'&trackingGroup=#{trackingGroup}' +
	            	'&impl=s' + 
	            	'&gdfp_req=1' + 
	            	'&env=vp' + 
	            	'&output=xml_vast' + (Ndn_App.getSetting('vast3') ? '3' : '2') +
	            	'&unviewed_position_start=1' + 
	            	'&url=#{encodedEmbedOriginUrl}' + // encodeURIComponent(window.location.href + '') + 
	            	'&correlator=#{now}' + 
	            	'&cust_params=#{dfpEncodedParams}',
	            enabled: params.adServer == 'dfp',
	            ppid: Ndn_Analytics.getPpidValue(),
	            disableCompanionAds: true // Disables the companion ads from breaking out of the player and onto partner sites
	        },
            auditude: {
                enabled: params.adServer == 'Auditude',
                resources: [
                    {
                       type: 'text/javascript',
                       src: 'http://adunit.cdn.auditude.com/player/js/lib/plugin/1.3/aud.html5player.js' // TODO: Needs SSL-support!
                    }
                ],
	            defaultId: "ndn_default",
                videoId: "#{media.guid}",
                domain: "auditude.com",
                zoneId: "#{aud_zone_id}",
                version: "ndn-1.0",
                params: "#{auditudeParams}",
	            "userData": {
	            	"videotype": "#{player.mode}"
	            }
            },
	        "autoplay": false,
	        "volumepanel": {},
	        "volume": 0.75, // 1, // 0-1, ie. 0.5 for half volume (doesn't work in iOS HTML5)
	        "rules": {
	        	"PDL": {
	                "regexp": "Android 4"
	            },
	            "HLS":{
		            "regexp": "Mac OS X 10_8_5"
	            }
	        },
	        "adchoices": {
	            "target": "http://assets.newsinc.com/info/adchoices.html"
	        },
	        "playoverlay": {
	            "enabled": false
	        },
	        "locales": {
	        	"en": {
	        		"MSG_NEXT_VIDEO": params.adOverlayText,
	        		"MSG_SECONDS": "seconds"
	        	}
	        },
	        "mode":"auto" // failsafe to handle AMP's lookin' at the mode query param on a partner page. Honestly, who doesn't namespace?
	    };
	};

	/**
	 * Whether the AMPPremier library has loaded its base config
	 * @var {Boolean}
	 */
	var _hasLoadedConfig = false;
	
	/**
	 * Structured data concerning the config to be used for Akamai Media Player debugging purposes only
	 * @var {Object}
	 */
	var _debugConfig;
	
	/**
	 * @param {Boolean} toggle
	 * @private
	 */
	var _setMuted = function(toggle) {
		// console.log('_setMuted(' + toggle + ');');
		
		// Define how the Akamai Media Player gets muted/unmuted, and how the mute interval gets cleared
		var mutePlayer = $.proxy(function() {
				try {
					if (this.get('isReady')) {
						_delegateMethod(this, 'setVolume', 0.01);
						_delegateMethod(this, 'setMuted', true);
					}
				}
				catch (e) {}
			}, this),
			unmutePlayer = $.proxy(function() {
				try {
					if (this.get('isReady')) {
						_delegateMethod(this, 'setMuted', false);
						_delegateMethod(this, 'setVolume', 0.75);
					}
				}
				catch (e) {}
			}, this),
			clearMuteInterval = $.proxy(function() {
				// If the video player is currently muted (has a mute interval that ensures the "mute" state is enforced)
				if (this.isMuted()) {
					// If a mute interval that ensures that the "mute" state is enforced is set, remove it
					if (this.get('muteInterval')) {
					    clearInterval(this.get('muteInterval'));
	                    this.set('muteInterval', null);
	                    
	                    if (Ndn_Debugger.inMode('muteInterval')) console.log('[Akamai_VideoPlayerModel] Mute interval unset.');
					}
				}
			}, this);
		
		if (toggle) {
			// If the video player should be muted and is currently not muted, mute the player
			if (!this.isMuted()) {
			    // Get the frequency of how often to apply the mute to the player in milliseconds
			    var muteInterval = (function(videoPlayer) {
			        var settingsData = videoPlayer.get('widget').getCachedSettings();
			        
			        if (settingsData && typeof settingsData.settings.muteInterval != 'undefined') {
			            return settingsData.settings.muteInterval;
			        }
			    })(this.get('videoPlayer'));
			    
			    // If the mute interval is defined and is greater than 0 milliseconds, apply the mute interval appropriately
			    if (muteInterval) {
	                // Ensure that the player is still muted every x-number of milliseconds
	                this.set('muteInterval', setInterval(mutePlayer, muteInterval));
	                
                    if (Ndn_Debugger.inMode('muteInterval')) console.log('[Akamai_VideoPlayerModel] Mute interval of ' + muteInterval + ' milliseconds was just set.');
			    }
			}
			
			// Mute the Akamai Media Player
			mutePlayer();
		}
		else {
			// Clear the mute interval (if there is one) that ensures that the "mute" state is enforced
			clearMuteInterval();
			
			// Unmute the Akamai Media Player
			unmutePlayer();
		}
		
		// Keep track of whether the player is muted via its mute interval logic
		this.set('isMuted', toggle);
	};
	
	/**
	 * @param {String} eventName
	 * @param {Object?} eventData
	 * @private
	 */
	var _trigger = function(eventName, eventData) {
	    if (Ndn_Debugger.inMode('attemptedEvents')) console.log('[Akamai_VideoPlayerModel ATTEMTPED EVENT] ' + eventName);
	    
		var logBadEvent = function(eventName) {
			if (Ndn_Debugger.inMode('unexpectedEvents')) console.log('[Akamai_VideoPlayerModel UNEXPECTED EVENT] ' + eventName);
		};
		
		var videoPlayer = this.get('videoPlayer');
		
		if (eventName == 'adCompanion') {
			// Simplify all the craziness that could get returned for an "adCompanion" event
			eventData = {
				content: (function() {
					try {
						if (eventData.data && eventData.data[0]) {
							return eventData.data[0].data || eventData.data[0].content;
						}
						else if (eventData.data && eventData.data.metadata && eventData.data.metadata[0]) {
							return eventData.data.metadata[0].data || eventData.data.metadata[0].content;
						}
					}
					catch (e) {}
					
					return '';
				})()
			};
			
			// If no companion ad content was returned, then do not trigger this "adCompanion" event
			if (!eventData.content) {
				return;
			}
		}
		else if (eventName == 'videoStart') {
			if (videoPlayer.isShowingVideo()) {
				// If "videoStart" is being triggered, but the video player already believes it is showing video content,
				// ignore this event as it must have been erroneously triggered by the Akamai Media Player
				return logBadEvent(eventName);
			}
			else {
				// Keep track that the video player is now showing (and playing) video content, which consequently means it is not showing an ad
				this.set({
					'isShowingVideo': true,
					'isShowingAd': false,
					'isPlaying': true
				});
			}
		}
		else if (eventName == 'adStart') {
			if (videoPlayer.isShowingAd()) {
				// If "adStart" is being triggered, but the video player already believes it is showing an ad,
				// ignore this event as it must have been erroneously triggered by the Akamai Media Player
				return logBadEvent(eventName);
			}
			else {
				// Keep track that the video player is now showing (and playing) an ad, which consequently means it is not showing video content
				this.set({
					'isShowingVideo': false,
					'isShowingAd': true,
					'isPlaying': true
				});
			}
		}
		else if (eventName == 'adEnd') {
			if (!videoPlayer.isShowingAd()) {
				// If "adEnd" is being triggered, but the video player does not believe it is showing an ad,
				// ignore this event as it must have been erroneously triggered by the Akamai Media Player
				return logBadEvent(eventName);
			}
			else {
				// Keep track that the video player is no longer showing an ad
				this.set('isShowingAd', false);
				
				// Also indicate that playback is no longer being played
				this.set('isPlaying', false);
			}
		}
		else if (eventName == 'videoEnd') {
			if (!videoPlayer.isShowingVideo()) {
				// If "videoEnd" is being triggered, but the video player does not believe it is showing video content,
				// ignore this event as it must have been erroneously triggered by the Akamai Media Player
				return logBadEvent(eventName);
			}
			else {
				// Keep track that the video player is no longer showing video content
				this.set('isShowingVideo', false);
				
				// Also indicate that playback is no longer being played
				this.set('isPlaying', false);
			}
		}
		else if (eventName == 'videoTimeUpdate') {
			// If the rogue RTMP behavior is happening where the video content plays in the background
			if (!videoPlayer.isShowingVideo() || !videoPlayer.isPlaying()) {
				logBadEvent(eventName);
				
				// Ignore this erroneously triggered event
				return;
			}
			else if (this.getCurrentTime() > this.getDuration()) {
				logBadEvent(eventName);
				
				// If the video playback's current time exceeds the video playback's entire duration, then
				// something is misbehaving; force the video playback to end
				_delegateMethod(this, 'end');
				
				// Ignore this event erroneously triggered by the Akamai Media Player
				return;
			}
		}
		else if (eventName == 'pause') {
			if ((!videoPlayer.isShowingVideo() && !videoPlayer.isShowingAd()) || !videoPlayer.isPlaying()) {
				// Ignore this erroneously triggered event
				return logBadEvent(eventName);
			}
			else {
				this.set('isPlaying', false);
			}
		}
		else if (eventName == 'play') {
			if (!videoPlayer.isShowingVideo() && !videoPlayer.isShowingAd()) {
				// Ignore this erroneously triggered event
				return logBadEvent(eventName);
			}
			else {
				this.set('isPlaying', true);
			}
		}
		
		// console.log('[EVENT] ', eventName);
		
		// Trigger these events for the parenting instance of the Ndn_VideoPlayer library
		videoPlayer.trigger(eventName, eventData);
		
		this.trigger(eventName, eventData);
	};
	
	/**
	 * Processes and translates events triggered by the Akamai Media Player into events triggered by this instance of the Akamai_VideoPlayerModel library
	 * @private
	 */
	var _initializeEventProcessing = function() {
		// Try to stay consistent with the naming convention below until we can find a better way to identify each one of these layers
		var player = this,
			videoPlayer = this.get('videoPlayer'),
			playerInstance = player.get('playerInstance'),
			inFlashPlaybackMode = videoPlayer.getPlaybackMode() == 'flash';
		
		var trigger = $.proxy(function(eventName, eventData) {
			_trigger.call(this, eventName, eventData);
		}, this);
		
		// Wait for the AMP player to be ready before initializing any event listeners (other than the "ready" event listener)
        playerInstance.addEventListener('ready', $.proxy(function(event) {

            // Process the "videoLoadComplete" event
            // TODO: Consider renaming "videoLoadComplete" to a more appropriate event name; what does Akamai Media Player's "canplay" event even mean?
            // TODO: Verify that Akamai Media Player still passes along its "canplay" event
            playerInstance.addEventListener('canplay', function(event) { trigger('videoLoadComplete'); });
            
            // Process the "play" and "videoStart" events (the "play" event is processed again after the below code)
            playerInstance.addEventListener('started', function(event) {
                trigger('play', event);
                trigger('videoStart', event);
            });

            // TODO: Consider refactoring out the "rmmStartRequest" and "videoStartRequest" events out
            // Process the "playRequest", "rmmStartRequest", and "videoStartRequest" events
            playerInstance.addEventListener('playrequest', $.proxy(function(event) {
                trigger('playRequest');
                
                if (videoPlayer.isShowingRmm()) {
                    trigger('rmmStartRequest');
                }
                else {
                    trigger('videoStartRequest');
                }
            }, this));

            playerInstance.addEventListener('playstatechange', $.proxy(function(event) {
                if (event && event.data && event.data.newState == 'playing') {
                    if (this.get('videoPlayer').isShowingAd()) {
                        trigger('adResume', event);
                    }

                    trigger('play', event);
                    trigger('videoResume', event);
                }
            }, this));
            
            // Handle the "playerElementClick" event
            playerInstance.addEventListener('mediaPlayerElementEvent', $.proxy(function(event) {
                if (event.data && event.data.element && _.contains(['playPauseBtn', 'videoWindow'], event.data.element) && event.data.type == 'click') {
                    trigger('playerElementClick', {
                        element: event.data.element
                    });
                }
            }, this));
            
            // Keep the player's volume up-to-date
            playerInstance.addEventListener('volumechange', $.proxy(function(event) {
                if (event && event.data && typeof event.data.currentVolume != 'undefined') {
                    this.set('volume', event.data.currentVolume);
                }
            }, this));
            
            //Process the "fullscreenchange events
            playerInstance.addEventListener('fullscreenchange', function(event) {
                if(event.data){
                    trigger('enteringfullscreen', event);
                }
                else{
                    trigger('exitingfullscreen', event);                    
                }
                
            });

            // Process the "volumechange" events.
            playerInstance.addEventListener('volumechange', $.proxy(function(event) {
                if (!this.isMuted()) {
                    this.set('volume', event.data.currentVolume);
                }

                if (this.get('videoPlayer').isPlayingAd()) {
                    if (_.isNumber(event.data.currentVolume)) {
                        if (event.data.currentVolume == 0 && this.previous('volume') > 0) {
                            trigger('adMute', event);
                        } else if (event.data.currentVolume > 0 && this.previous('volume') == 0) {
                            trigger('adUnmute', event);
                        }
                    } else if (_.isNumber(event.data)) {
                        if (event.data == 0) {
                            trigger('adMute', event);
                        } else {
                            trigger('adUnmute', event);
                        }
                    }
                }
            }, this));
            
            // Process the "videoLoadStart", "videoTimeUpdate", "videoEnd", "videoVolumeChange", and "error" events
            playerInstance.addEventListener('loadstart', function() { trigger('videoLoadStart'); });
            playerInstance.addEventListener('timeupdate', function(event) { trigger('videoTimeUpdate', event); });
            playerInstance.addEventListener('ended', function(event) { trigger('videoEnd', event); });
            playerInstance.addEventListener('volumechange', function(event) { trigger('videoVolumeChange'); });
            playerInstance.addEventListener('error', function(event) { trigger('error', event); });
            
            /**
             * @trigger "adRequest" Format:
             * <pre>{
             *  'adTag': String, // The ad tag URL requesting an ad response
             *  'receivedData': Object, // Data received from the SDK responsible for making the ad request
             * }</pre>
             */
            if (inFlashPlaybackMode) {
                playerInstance.addEventListener('adComponentAdsRequest', function(event) {
                    var eventData = event.data;
                    
                    trigger('adRequest', {
                        adTag: eventData.requestURL,
                        receivedData: eventData
                    });
                });
            }
            else {
                // TODO: Add logic to trigger the "adRequest" event for the video player when in HTML5 playback mode
                playerInstance.ads.addEventListener('request', function(event) {
                    trigger('adRequest', {
                        receivedData: {} // There is no received data from the SDK regarding this event
                    });
                });
            }
            
            /**
             * @trigger "adLoad" Format:
             * <pre>{
             *  'adId': String, // The ad ID of the advertisement received
             *  'adPlatform': String, // The name of the ad platform
             *  'adSystem': String, // The name of the ad system
             *  'adTitle': String, // The ad's title
             *  'duration': unknown_type,
             *  'linear': unknown_type,
             *  'totalAds': unknown_type,
             *  'workflow': unknown_type,
             *  'url': String, // The ad's request URL
             * }</pre>
             */
            (function() {
                // The event listener for both the Flash and HTML5 playback modes
                var listener = function(event) {
                    try {
                        var eventData = event.data;
                        
                        if (eventData) {
                            var adId = (eventData.metadata && eventData.metadata.wrapperAdIds && eventData.metadata.wrapperAdIds[0]) || eventData.id;
                            
                            trigger('adLoad', {
                                adId: adId,
                                adPlatform: eventData.adPlatform,
                                adSystem: eventData.adSystem,
                                adTitle: eventData.title,
                                duration: eventData.duration,
                                linear: eventData.linear ? 1 : 0,
                                podPosition: eventData.podPosition,
                                totalAds: eventData.totalAds,
                                workflow: eventData.workflow,
                                url: eventData.requestURL
                            });
                        }
                    }
                    catch (e) {}
                };
                
                // Add the event listener appropriately to both the Flash and HTML5 playback modes of the AMP player
                if (inFlashPlaybackMode) playerInstance.addEventListener('adComponentMediaLoaded', listener);
                else playerInstance.ads.addEventListener('loaded', listener);
            })();
            
            // Process the "volumeButtonClick" event
            playerInstance.addEventListener('mediaPlayerElementEvent', function(event) {
                if (event.data.element == 'volumeBtn' && event.data.type == 'click') {
                    trigger('volumeButtonClick');
                }
            });
            
            // Process the "pause" event
            playerInstance.addEventListener('pause', function(event) { trigger('pause', event); });
            playerInstance.addEventListener('playstatechange', function(event) {
                if (event && event.data && event.data.newState == 'paused' && !event.player.getEnded()) {
                    trigger('pause', event);
                }
            });
            
            // Process the "click" event, which is triggered when the player is clicked
            playerInstance.addEventListener('mediaPlayerElementEvent', function(event) {
                if (event.data.element == 'videoWindow' && event.data.type == 'click') {
                    // This constitutes a click on the player rectangle
                    trigger('click', event);
                }
            });
            
            // Process the "share" event
            if (playerInstance.share != null) { playerInstance.share.addEventListener('share', function(event) { trigger('share', event); }); }
            
            // TODO: Process the "visibilitychange" event?
            if (playerInstance.captioning != null) {
                playerInstance.captioning.addEventListener('visibilitychange', function(event) {
                    // TODO: Implement this once captioning is available using our feed data
                });
            }
            
            // Process the "adStart", "adEnd", "adTimeUpdate", and "adCompanion" events
            // TODO: Watchout for the "adTimeUpdate" event as it may be unexpectedly (or expectedly) being triggered 
            // after calling the .setCurrentTime() method...
            playerInstance.ads.addEventListener('started', function(event) { trigger('adStart'); });
            playerInstance.ads.addEventListener('ended', function(event) { trigger('adEnd'); });
            playerInstance.ads.addEventListener('timeupdate', function(event) { trigger('adTimeUpdate'); });
            playerInstance.ads.addEventListener('companion', function(event) { trigger('adCompanion', event); });
            
            
            // Handle "adError" event
            playerInstance.ads.addEventListener('error', function(event) {
                try {
                    var eventData = event.data.metadata.error;
                    if (eventData) {
                        trigger('adError', {
                            message: eventData.reason,
                            receivedData: eventData
                        });
                    }
                }
                catch (e) {}
            });
            playerInstance.ads.addEventListener('log', function(event) {
                try {
                    var eventData = event.data.metadata.metadata.error;
                    if (eventData) {
                        trigger('adError', {
                            message: eventData.message,
                            receivedData: eventData
                        });
                    }
                }
                catch (e) {}
            });
        
            // Indicate that the player is finally ready and its event listeners have been initialized
            trigger('playerReady', event);    
        }, this));
	};
	
	/**
	 * Call a provided JavaScript closure with provided parameters (only works for up to 19 parameters provided)
	 * @param {Function} method The method to call
	 * @param {Array<unknown_type>} methodParams The list of parameters to send to the method to be called
	 * @private
	 */
	function _callMethod(methodParent, methodName, methodParams) {
		var a = methodParent, b = methodParams, c = methodName;
		switch(b.length){case 0:return a[c]();case 1:return a[c](b[0]);case 2:return a[c](b[0],b[1]);case 3:return a[c](b[0],b[1],b[2]);case 4:return a[c](b[0],b[1],b[2],b[3]);case 5: return a[c](b[0],b[1],b[2],b[3],b[4]);case 6:return a[c](b[0],b[1],b[2],b[3],b[4],b[5]);case 7:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6]);case 8:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7]);case 9:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8]);case 10:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9]);case 11:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10]);case 12:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11]);case 13:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12]);case 14:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13]);case 15:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13],b[14]);case 16:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13],b[14],b[15]);case 17:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13],b[14],b[15],b[16]);case 18:return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13],b[14],b[15],b[16],b[17]);case 19: return a[c](b[0],b[1],b[2],b[3],b[4],b[5],b[6],b[7],b[8],b[9],b[10],b[11],b[12],b[13],b[14],b[15],b[16],b[17],b[18]);}
	}
	
	/**
	 * Use this function to make any calls directly to the Akamai Media Player instance.
	 * 
	 * For example, to call the "ads.setEnabled" method with the parameter of "true", call this function like so 
	 * while inside an Akamai_VideoPlayerModel method:
	 * <pre>_delegateMethod(this, 'ads.setEnabled', true);</pre>
	 * 
	 * This function works with multiple arguments. For example, if you wanted to call "amp.setMuted(true, true);" where "amp" is the variable
	 * holding the Akamai Media Player instance, you would call this method using the following code:
	 * <pre>_delegateMethod(this, 'setMuted', true, true);</pre>
	 * 
	 * @param {Akamai_VideoPlayerModel} videoPlayerProxy
	 * @param {String} methodName The name of the method to call directly on the Akamai Media Player instance (ie. "setVolume")
	 * @return {unknown_type} The result of whatever method was just called via this function
	 * @private
	 */
	var _delegateMethod = function(videoPlayerProxy, methodFullName) {
		var playerInstance = videoPlayerProxy.get('playerInstance'),
			methodParams = Array.prototype.slice.call(arguments, 2);
		
		if (Ndn_Debugger.inMode('delegateMethods')) console.log('---> Akamai_VideoPlayerModel, _delegateMethod(', methodFullName, ', ', methodParams, ');');
		
		// Get the player instance's method being referred to by the "methodName" parameter
		var methodParent = playerInstance,
			methodName;
		
		// If the Akamai Media Player has not been instantiated yet, ignore this call
		// if (!playerInstance) return;
		
		try {
			var functionTokens = methodFullName.split('.');
			for (var i = 0; i < functionTokens.length - 1; i++) {
				var functionToken = functionTokens[i];
				
				if (methodParent !== null && typeof methodParent == 'object' && typeof methodParent[functionToken] != 'undefined') {
					methodParent = methodParent[functionToken];
				}
				else {
					throw "Method parent could not be found.";
				}
			}
			
			if (!(methodParent !== null && typeof methodParent == 'object')) {
				throw "Method parent could not be found.";
			}
			else {
				methodName = functionTokens[functionTokens.length - 1];
			}
		}
		catch (e) {
			console.log('ERROR TRYING TO FIND DELEGATE METHOD: ', e);
		}
		
		// If the method specified was actually a function available on the instance of the Akamai Media Player
		if (methodParent && typeof methodParent[methodName] == 'function') {
			return _callMethod(methodParent, methodName, methodParams);
		}
	};
	
	/**
	 * Ensure that the third-party player is instantiated and up-to-date
	 * @private
	 */
	var _syncThirdPartyPlayer = function() {
		// console.log('_syncThirdPartyPlayer();');
		
		return _initializeThirdPartyPlayer.call(this).done($.proxy(function() {
			// Once the third-party player has been instantiated, make sure it is up-to-date with the latest video data (if the video data has changed)
			if (this.get('videoDataHasChanged')) {
				_updateThirdPartyPlayer.call(this);
			}
		}, this));
	};
	
	/**
	 * Initialize an instance of the Akamai Media Player for this instance of an Akamai_VideoPlayerModel (or make sure this has already happened)
	 * @return {jQuery.Deferred}
	 * @private
	 */
	var _initializeThirdPartyPlayer = function() {
		// console.log('_initializeThirdPartyPlayer();');
		
		return $.Deferred($.proxy(function(deferred) {
			// If there is no third-party player instantiated yet
			if (!this.get('playerInstance')) {
				this.get('videoPlayer').get('widget').getSettings()
				.done($.proxy(function(widgetSettings) {
					// Make sure that AMPPremier has its config loaded and with the appropriate ad server
					Akamai_VideoPlayerModel.loadConfig({
						adServer: widgetSettings.settings.ads.server,
						adOverlayText: this.get('params').videoPlayerWidth > 350 ? 'Your content starts in ' : 'Starts in '
					});
					
					// TODO: Don't reference the "videoPlayer" property here, this model should already have that information internally
					// Create a new Akamai Media Player instance
					this.set('playerInstance', Akamai_VideoPlayerModel.getAmpPremier().create(this.get('videoPlayer').get('playerContainerElementId'), this.get('playerConfig')));
					
					// Initialize the processing of the new Akamai Media Player instance's events
					_initializeEventProcessing.call(this);
					
					this.onceReady(function() {
						deferred.resolve();
					});
				}, this));
			}
			else {
				// If a third-party player has already been instantiated, then this function's business is done
				deferred.resolve();
			}
		}, this)).promise();
	};
	
	/**
	 * Update the Akamai Media Player instance with the stored state of the player within this model (only if the video data has changed)
	 * @private
	 */
	var _updateThirdPartyPlayer = function() {
		// console.log('_updateThirdPartyPlayer();');
		
		if (this.get('videoDataHasChanged')) {
			// Update the volume/mute-state
			_setMuted.call(this, this.get('isMuted'));
			if (!this.get('isMuted')) {
				_delegateMethod(this, 'setVolume', this.get('volume'));
			}
			
			// Update everything else
			_delegateMethod(this, 'setAutoplay', this.get('playOnLoad'));
			_delegateMethod(this, 'ads.setEnabled', this.get('adsEnabled')); // This was removed when the Ndn_VideoPlayer.getPlayerConfig() method started disabling IMA there.
			_delegateMethod(this, 'setParams', this.get('params'));
			_delegateMethod(this, 'feed.setData', this.get('videoData'));
			
			// Indicate that the "videoData" property is no longer out-of-sync with what is loaded into the video player's Akamai Media Player instance
			this.set('videoDataHasChanged', false);
		}
	};
	
	/**
	 * Destructs this video player's instance of the Akamai Media Player and resets any of this proxy 
	 * model's properties related to the state of said instance
	 * @param {Boolean} [saveState] Whether the state of the video player should be saved and re-instated whenever the third-party player is re-initialized (defaults to false)
	 * @private
	 */
	var _destructThirdPartyPlayer = function(saveState) {
		// console.log('_destructThirdPartyPlayer();');
		
		if (typeof saveState == 'undefined') saveState = false;
		
		// If there is a player instance to destruct
		var playerInstance = this.get('playerInstance');
		if (playerInstance) {
			var savedState = {
				'currentTime': this.getCurrentTime()
			};
			
			// Reset the following properties to their default values
			var resetProperties = [
				'isReady',
				'isShowingAd',
				'isShowingVideo',
				'isMuted',
				'videoDataHasChanged'
				/*
				'volume',
				'adsEnabled',
				'playOnLoad',
				'params',
				'videoData'
				*/
			];
			$.each(resetProperties, $.proxy(function(index, propertyName) {
				if (saveState) {
					savedState[propertyName] = this.get(propertyName);
				}
				
				// Reset the property to its default value
				this.set(propertyName, this.defaults[propertyName]);
			}, this));
			
			// Make sure the video player
			this.setMuted(false);
			
			this.set('savedState', savedState);
			
			// Save the video player's overlays
			this.get('videoPlayer').saveOverlays();
			
			// Finally destruct the actual Akamai Media Player
			playerInstance.destroy();
			this.set('playerInstance', null);
			
			this.set('timesDestructed', this.get('timesDestructed') + 1);
			
			// TODO: Consider refactoring this logic so that it doesn't expose this functionality to a public method
			this.get('videoPlayer').initializeContainerElement();
			
			// Restore the overlays back to the video player
			this.get('videoPlayer').restoreOverlays();
		}
	};
	
	var Akamai_VideoPlayerModel = Backbone.Model.extend({
		defaults: {
			/**
			 * The "id" attribute of this video player's intended container dom element
			 * @var {String}
			 */
			'containerElementId': null,
			
			/**
			 * Structured data concerning the config needed to instantiate this instance of an Akamai Media Player
			 * @var {Object}
			 */
			'playerConfig': null,
			
			/**
			 * The instance of the parenting Ndn_VideoPlayer 
			 * @var {Ndn_VideoPlayer}
			 */
			'videoPlayer': null,
			
			/**
			 * The actual Akamai Media Player instance
			 * @var {AMPPremier}
			 */
			'playerInstance': null,

			/**
			 * An interval set for continuously enforcing the Akamai Media Player to be muted (or null if no interval is set)
			 * @var {int}
			 */
			'muteInterval': null,
			
			/**
			 * The video player's volume
			 * @var {float}
			 */
			'volume': 0.75,
			
			/**
			 * Whether the instance of the third-party video player library has been instantiated 
			 * @var {Boolean}
			 */
			'isReady': false,

			/**
			 * Whether the video player is currently showing an ad
			 * @var {Boolean}
			 */
			'isShowingAd': false,
			
			/**
			 * Whether the video player is currently showing video content
			 * @var {Boolean}
			 */
			'isShowingVideo': false,
			
			/**
			 * Whether the video player is currently playing, which is different than "showing", either an ad or video content
			 * @var {Boolean}
			 */
			'isPlaying': false,
			
			/**
			 * Whether the video player is currently muted
			 * @var {Boolean}
			 */
			'isMuted': false,
			
			/**
			 * Whether the video player begins playing immediately after calling this.loadVideoData();
			 * @var {Boolean}
			 */
			'playOnLoad': false,
			
			/**
			 * Whether ads are enabled on this video player
			 * @var {Boolean}
			 */
			'adsEnabled': true,
			
			/**
			 * The parameters sent to the Akamai Media Player via its .setParams() method
			 * @var {Object}
			 */
			'params': null,
			
			/**
			 * TODO: Document this property
			 * @var {Object}
			 */
			'videoData': null,
			
			/**
			 * Whether the video data loaded into this video player has changed
			 * @var {Boolean}
			 */
			'videoDataHasChanged': false,
			
			/**
			 * TODO: Finish documenting this property.
			 * 
			 * Structured data from the video player instance that was saved just before the instance was destructed, which allows for said 
			 * data to be retrieved without the presence of the video player instance. Format:
			 * <pre>{
			 * 	'currentTime': Number, // The current time of the video player instance just before it was destructed
			 * 	'isReady': Boolean, 
			 * 	'isShowingAd': Boolean,
			 * 	'isShowingVideo': Boolean,
			 * 	'isMuted': Boolean,
			 * 	'videoDataHasChanged': Boolean,
			 * }</pre>
			 * @var {Object}
			 */
			'savedState': null,
			
			/**
			 * The number of times this model's Akamai Media Player has been destructed
			 * @var {int}
			 */
			'timesDestructed': 0
		},
		
		initialize: function(containerElementId, playerConfig, videoPlayer) {
			// console.log('Akamai_VideoPlayerModel.initialize(', arguments, ');');
			
			this.set({
				'containerElementId': containerElementId,
				'playerConfig': playerConfig,
				'videoPlayer': videoPlayer
			});
			
			/**
			this.on('all', function(eventName, eventData) {
				console.log('VideoPlayerModel event: ' + eventName, eventData);
			});
			/**/
			
			// Initialize the default saved state of the player
			this.set('savedState', {});
			
			// If the volume button is clicked while the player is being repeatedly muted, then unmute the player
			this.on('volumeButtonClick', $.proxy(function() {
				if (this.isMuted()) {
					this.setMuted(false);
				}
			}, this));
			
			// Handle when the Akamai Media Player instance becomes ready, by indicating its ready status
			this.on('playerReady', $.proxy(function() {
				this.set('isReady', true);
			}, this));
		},
		
		destroy: function() {
			_destructThirdPartyPlayer.call(this);
		},
		
		/**
		 * @param {Function} callback The closure to execute once the third-party video player has been instantiated and embedded on the page
		 * @return this
		 */
		onceReady: function(callback) {
			if (this.get('isReady')) {
				callback();
			}
			else {
				this.once('playerReady', callback);
			}
			
			return this;
		},
		
		/**
		 * Load video data into this proxy model in preparation of playing it
		 */
		loadVideoData: function(videoData) {
			// console.log("loadVideoData(", videoData, ');');
			// console.log('Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically() ==', Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically());
			// console.log('thumbnail ==', videoData.channel.item['media-group']['media-thumbnail']['@attributes'].url);
			
			// console.log('this.get("playOnLoad") ==', this.get('playOnLoad'));
			
			this.set('videoData', videoData);
			this.set('videoDataHasChanged', true);
			
			// If the video player instance is about to be needed (it is about to be played or it is on a mobile device, which is needed for 1-click play)
			if (this.get('playOnLoad')) {
				// TODO: Add this logic inside the _syncThirdPartyPlayer() method?
				_trigger.call(this, 'videoStartRequest');
				
				// Make sure the video player is initialized and up-to-date
				_syncThirdPartyPlayer.call(this);
			}
			else if (!Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically()) {
				// console.log('Ndn_Utils_UserAgent.isAbleToPlayVideoAutomatically() is FALSE!');
				
				// Make sure the video player is initialized and up-to-date
				_syncThirdPartyPlayer.call(this).done($.proxy(function() {
					if (this.get('playOnLoad')) {
						// console.log('About to call this.play() explicitly so that maybe the video begins playing automatically!');
						
						this.play();
					}
				}, this));
			}
			else if (this.get('playerInstance')) {
				// Deallocate resources for "Click To Play" state if the video player instance is using the Flash playback mode
				if (this.get('videoPlayer').getPlaybackMode() == 'flash') {
					_destructThirdPartyPlayer.call(this);
				}
			}
		},
		
		getCurrentTime: function() {
			if (this.get('playerInstance')) {
				return _delegateMethod(this, 'getCurrentTime');
			}
			else {
				return this.get('savedState').currentTime;
			}
		},
		
		setCurrentTime: function(time) {
			return _delegateMethod(this, 'setCurrentTime', time);
		},

        /**
         * Get the duration of the current ad.
         * @returns number
         */
        getAdDuration: function() {
            try {
                return this.get('playerInstance').ads.adVO.metadata.duration;
            } catch (er) {
                return null;
            }
        },

        /**
         * Get the duration of the current ad.
         * @returns number
         */
        getAdCurrentTime: function() {
            try {
                return this.get('playerInstance').ads.adVO.metadata.currentTime;
            } catch (er) {
                return null;
            }
        },
		
		/**
		 * Toggle whether the video player should play immediately upon loading the video
		 */
		setPlayOnLoad: function(toggle) {
			this.set('playOnLoad', toggle);
		},
		
		getPlayOnLoad: function() {
			return this.get('playOnLoad');
		},

		/**
		 * Toggle whether the video player should play ads before playing video content
		 */
		setAdsEnabled: function(toggle) {
			this.set('adsEnabled', toggle);
		},
		
		setParams: function(params) {
			this.set('params', params);
		},
		
		getParams: function() {
			return this.get('params');
		},

		/**
		 * @param {Boolean} toggle Whether the video player should be muted. If the value of true is passed to this method, the video player's volume 
		 * is set to 1% (and "muted") and an interval enforces that this state remains as-is every x-number of milliseconds. If the value of false is
		 * passed to this method, the video player is "unmuted" and its volume is set to 75%, while removing any interval enforcing the "muted" state 
		 * of the video player.
		 */
		setMuted: function(toggle) {
            if (Ndn_Debugger.inMode('mute')) console.log('Akamai_VideoPlayerModel.setMuted(', arguments, ');');
			
			_setMuted.call(this, toggle);
		},
		
		/**
		 * @param {Boolean} [checkPlayerInstanceVolume] Whether to check actual player instance volume
		 * @return {Boolean} Whether the video player is muted
		 */
		isMuted: function(checkPlayerInstanceVolume) {
			checkPlayerInstanceVolume = checkPlayerInstanceVolume || false;

			if (checkPlayerInstanceVolume) {
				return this.getVolume() <= 0.01;
			}

			return this.get('isMuted');
		},

		/**
		 * @param {float} volume Value to set the player's volume to (between 0 and 1)
		 */
		setVolume: function(volume) {
			this.set('volume', volume);
			if (this.get('isReady')) _delegateMethod(this, 'setVolume', volume);
		},
		
		getVolume: function() {
			return this.get('volume');
		},
		
		getDuration: function() {
			return _delegateMethod(this, 'getDuration');
		},
		
		/**
		 * Begins or resumes ad or content video playback
		 */
		play: function() {
			// console.log('VideoPlayerModel.play(); about to trigger "videoStartRequest"');
			
			_trigger.call(this, 'videoStartRequest');
			
			_syncThirdPartyPlayer.call(this).done($.proxy(function() {
				// console.log('about to call play...');
				
				_delegateMethod(this, 'play');
			}, this));
		},

		/**
		 * Ends video playback (if playback is currently playing)
		 */
		end: function() {
			if (this.isPlaying()) {
				_delegateMethod(this, 'end');
			}
		},
		
		/**
		 * Pauses the video player
		 */
		pause: function() {
			if (this.isPlaying()) {
				_delegateMethod(this, 'pause');
			}
		},
		
		enableControls: function() {
			return _delegateMethod(this, 'controls.setMode', 'auto');
		},
		
		disableControls: function() {
			return _delegateMethod(this, 'controls.setMode', 'none');
		},
		
		/**
		 * @return {Boolean} Whether the video player is currently showing video content
		 */
		isShowingVideo: function() {
			return this.get('isShowingVideo');
		},

		/**
		 * @return {Boolean} Whether the video player is currently showing an ad
		 */
		isShowingAd: function() {
			return this.get('isShowingAd');
		},
		
		isPlaying: function() {
			return this.get('isPlaying');
		},
		
		isPlayingVideo: function() {
			return this.get('isPlaying') && this.get('isShowingVideo');
		},
		
		isPlayingAd: function() {
			return this.get('isPlaying') && this.get('isShowingAd');
		}
	},
	{
		/**
		 * @return {String?} The version number of the current Akamai Media Player build being used (for example, "1.0.0000")
		 */
		getVersion: function() {
			var versionString = _nw2e.videoPlayer.version || Akamai_VideoPlayerModel.getAmpPremier().VERSION,
				matches;
			
			if ((matches = versionString.match(/v([0-9.]+)$/)) && matches[1]) {
				return matches[1];
			}
			else {
				return null;
			}
		},
		
		/**
		 * @return {Object} The namespace JSON object that has all the AMPPremier related variables associated with it as properties
		 */
		getAmpPremier: function() {
			return _nw2e.videoPlayer.akamai.amp.AMP;
		},
		
		/**
		 * Loads the config for AMPPremier if it has not been loaded previously
		 * @param {Object} params Parameters to modify how the config to be loaded into AMPPremier is generated
		 */
		loadConfig: function(params) {
			if (!_hasLoadedConfig) {
				// Store the "test config" used for debugging
				_debugConfig = this.getTestConfig(params);
				
				Akamai_VideoPlayerModel.getAmpPremier().loadDefaults(Akamai_VideoPlayerModel.getConfig(params));
				_hasLoadedConfig = true;
			}
		},
		
		/**
		 * @return {String} The version of the video player; either "Flash" or "html"
		 */
		getPlaybackMode: function() {
			return Akamai_VideoPlayerModel.getAmpPremier().getPlaybackMode();
		},
		
		/**
		 * @param {Object} params Parameters to modify how the config is generated
		 */
		getConfig: function(params) {
			return _getConfig(_.extend({}, params));
		},
		
		/**
		 * @return {Object} Structured data concerning the parameters to use for generating the video player configs used for debugging
		 */
		getDebugConfigParams: function() {
			return {
				'akamaiJsLibFolderUrl': '../amp.premier',
				'akamaiResourcesFolderUrl': '../resources'
			};
		},

		/**
		 * @param {Object} params Parameters to modify how the config is generated
		 */
		getTestConfig: function(params) {
			return _getConfig(_.extend({}, params, Akamai_VideoPlayerModel.getDebugConfigParams()));
		},
		
		/**
		 * @return {?Object} Structured data concerning the config to be used for Akamai Media Player debugging purposes only
		 */
		getDebugConfig: function() {
			return _debugConfig;
		}
	});
	
	return Akamai_VideoPlayerModel;
});
