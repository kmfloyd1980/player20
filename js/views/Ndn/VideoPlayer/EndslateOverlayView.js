define([
	'jquery',
	'backbone',
	'mustache',
	'models/Ndn/VideoPlayer/EndslateOverlayModel',
	'models/Ndn/Widget',
 	
 	'text!templates/ndn/shared/endslate.html',

	'jquery_autoellipsis',
 	'lib/jquery_plugins/jquery.backstretch.min'
], function(
	$,
	Backbone,
	Mustache,
	Ndn_VideoPlayer_EndslateOverlayModel,
	Ndn_Widget,
	
	template
) {
	return Backbone.View.extend({
		events: {
			'click .ndn_endslateClickSurface': 'onSurfaceClick'
		},

		/**
		 * @return {jQueryElement} A jQuery wrapped DOMElement that is the container element for the current widget this view belongs to
		 */
		getWidgetContainerElement: function() {
			return this.model.get('widget').get('view').$el;
		},
		
		/**
		 * @param {Ndn_VideoPlayer_EndslateOverlayModel} model The model associated with this view
		 */
		initialize: function() {
			// Create the container element for this view
			this.setElement(
				$(document.createElement('div'))
				.addClass('ndn_endslateOverlay ndn_playerOverlay')
				.prependTo(this.model.get('videoPlayer').getContainerElement())
				.hide()
				.get(0)
			);
			
			this.model
			.on('endslateFinished', function() {
				this.trigger('endslateFinished');
				this.hide();
			}.bind(this))
			.on('change:countdownValue', function() {
				this.$el.find('.ndn_startsInCountdown').html(this.model.get('countdownValue'));
			}.bind(this));
		},
		
		hide: function() {
			this.$el.hide();
		},
		
		show: function(endslateType) {
			this.model.set('type', endslateType);
			
 			this.model.getNextVideoData()
 			.done($.proxy(function(nextVideoData) {
 				var templateData = this.model.getTemplateData(nextVideoData);
 				 				
 				// Generate this endslate's html
 				this.$el.html(Mustache.to_html(template, templateData));
 				
 				// Make sure that this overlay is visible (in order for ellipsis and other plugins to work)
 				this.$el.appendTo(this.model.get('videoPlayer').getContainerElement()).show();
 				

 				// Backstretch the endslate's background image, and then remove the "z-index" CSS property that 
 				// gets assigned to the element automatically through the backstretch plugin
 				
 				// do a null check first. 				
 				if(templateData['backgroundImage'] != undefined){
	 				this.$el
	 				.backstretch(templateData['backgroundImage'])
	 				.css('z-index', '');
	 				
	 				// Warning: Hardcoded value of "110" here; any CSS changes may conflict or cause problems with this value being assigned in JavaScript
	 				// Reset the backstretch image's "z-index" CSS property to be just behind this container's "z-index" value
	 				this.$el.find('.backstretch').css('z-index', 110);
	 			}
 				

 				
 				if (this.$el.height() < 300) this.$el.find('.ndn_upNextTitle').ellipsis();
 				if (this.$el.height() < 250) this.$el.find('.ndn_justWatchedContainer').hide(); 				 				
 				
 	 			if (this.model.displaysCountdown()) { 	 				
 	 				this.model.countdownToNextPlaylist();
 	 			}
 				
 				// Hide the infoOverlay / do not set to display none as that will make it appear 				
 				this.model.get('videoPlayer').getContainerElement().find('.ndn_infoOverlayContainer').css('visibility', 'hidden');

 	 			// Show this overlay
 				this.$el
 				.prependTo(this.model.get('videoPlayer').getContainerElement())
 				.show();
 			}, this));
		},

		onSurfaceClick: function() {
			this.model.get('videoPlayer').trackVideoStartRequest('endslateNextVideoClick');
			this.trigger('endslateFinished');
			this.model.playNextVideo();							
			this.hide();
		}
	});
});
