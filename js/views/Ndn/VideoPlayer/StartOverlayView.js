define([
	'jquery',
	'mustache',
	'backbone',
	'models/Ndn/Utils/FastClick',
	'models/Ndn/Utils/UserAgent',
	
	'text!templates/ndn/video-player/start-overlay.html'
], function(
	$,
	Mustache,
	Backbone,
	Ndn_Utils_FastClick,
	Ndn_Utils_UserAgent,
	
	template
) {
	return Backbone.View.extend({
	    
		events: {
            'mouseover': 'onMouseover',
			'click .ndn_startOverlay': 'onOverlayClick',
			'click .ndn_toggleDescription': 'toggleDescription'
		},
		
		initialize: function() {
			Backbone.View.prototype.initialize.call(this, arguments);
			
			this.$el = $(Mustache.to_html(template, {}));
			
			// Compensate for mobile devices that may have trouble clicking the play button
			new Ndn_Utils_FastClick(this.$el.find('.ndn_fastClick_touchandclickevent').get(0));
			
			this.description = this.$el.find('.ndn_videoDescription');
			
			// When the browser's "scroll" or "resize" events are heard, evaluate whether this 
			// product's "playOnInView" config setting should trigger playback for this overlay's
			// video player
			if (this.model.get('videoPlayer').get('widget').get('config')['playOnInView']) {
	            $(window).on('resize scroll', $.proxy(function() {
	                if (this.$el.is(':visible') && this.isInViewVertically()) {
	                    this.model.get('videoPlayer')
	                    .trackVideoStartRequest('playOnInView')
	                    .resetContinuousPlayCounter()
	                    .start();
	                }
	            }, this));
			}
		},
		
        /**
         * @return {Boolean} Whether this overlay's video player is in view within the browser vertically (horizontally is not considered)
         */
        isInViewVertically: function() {
            var elementTop = this.$el.offset().top,
                elementBottom = elementTop + this.$el.height(),
                windowTop = $(window).scrollTop(),
                windowBottom = windowTop + $(window).height();
            
            // Return true when the element's top edge is above the window's top edge and the element's bottom edge is below the window's bottom edge,
            // or when the element's top edge is between the window's top and bottom edge, 
            // or when the element's bottom edge is between the window's top and bottom edge
            return (elementTop < windowTop && elementBottom > windowBottom) ||
                (elementTop >= windowTop && elementTop <= windowBottom) ||
                (elementBottom >= windowTop && elementBottom <= windowBottom);
        },
		
		/**
		 * Hides this overlay and then starts playing the video player
		 */
		onOverlayClick: function() {
			//console.log('Ndn_VideoPlayer_StartOverlayView.start();');
			
			this.model.get('videoPlayer')
			.trackVideoStartRequest('startOverlayClick')
			.resetContinuousPlayCounter()
			.start();
		},
		
		show: function() {
			this.$el.appendTo(this.model.get('videoPlayer').getContainerElement()).show();
		},
		
		hide: function() {
			// this.model.get('videoPlayer').getOverlay('info').hide();
			this.$el.hide();
		},
		
		/**
		 * Handles when a new video has been selected to be loaded into the video player
		 */			
		toggleDescription: function() {
			this.descriptionButton = this.$el.find('.ndn_toggleDescription');
			// toggleClass wouldn't work due to the font-awesome using the pseudo selector :before to style the element. I personally think there is a better way to handle this but so far this is the only solution at the moment.
			if (this.descriptionButton.hasClass('ndn_icon_info-circled')){
				this.descriptionButton.removeClass('ndn_icon_info-circled');
				this.descriptionButton.addClass('ndn_icon_cancel-circled');
				this.description.slideToggle('500', "swing");
			}
			else{
				this.descriptionButton.removeClass('ndn_icon_cancel-circled');
				this.descriptionButton.addClass('ndn_icon_info-circled');
				this.description.slideToggle('500', "swing");
			}
		},
		
        /**
         * Handles when this overlay is moused-over
         */
        onMouseover: function(){
            var videoPlayer = this.model.get('videoPlayer'),
                product = videoPlayer.get('widget'),
                productConfig = product.get('config');
            
            if (!productConfig['autoPlay'] && productConfig['playOnMouseover']) {
                videoPlayer
                .trackVideoStartRequest('mouseOverToPlay')
                .resetContinuousPlayCounter()
                .start();           
            }           
        }
	});
});