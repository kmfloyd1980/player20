define([
	'jquery',
	'backbone',
	'mustache',
	
 	'text!templates/ndn/video-player/share-overlay.html',

	'jquery_autoellipsis',
	'bootstrap_tabs'
], function(
	$,
	Backbone,
	Mustache,
	
	template

) {
	return Backbone.View.extend({
		events: {

			'click .ndn_closeShare': 'onCloseClick',
			'mouseenter .ndn_shareCopyVideoLinkText': 'selectDirectLink',			
			'mouseenter .ndn_shareCopyEmbedCodeText': 'selectEmbedCode',	
			'click .ndn_shareEmbed': 'showEmbedLink',
			'click .ndn_backToShare': 'showEmbedLink'
		},
		
		initialize: function() {
			Backbone.View.prototype.initialize.call(this, arguments);
			
			this.$el = $(document.createElement('div')).addClass('ndn_shareOverlay ndn_playerOverlay');
		},
		
		/**
		 * Triggered once the "X" button has been clicked in order to close this share overlay
		 */
		onCloseClick: function() {
			this.model.get('videoPlayer').get('overlays')['info'].show();
			this.model.get('videoPlayer').get('overlays')['pause'].hide();
			this.model.get('videoPlayer').get('overlays')['share'].hide();
			this.model.get('videoPlayer').play();
			this.$el.find('.ndn_infoOverlayContainer').css('visibility','visible');
			
			return false;
		},
		
		/**
		 * Updates the overlay's container element's HTML appropriately
		 */
		updateHtml: function() {
 			var currentVideo = this.model.get('videoPlayer').getCurrentVideoData(),
				shareVideoUrl = 'http://social.newsinc.com/media/json/69017/' + currentVideo['videoId'] + '/singleVideoOG.html?type=VideoPlayer/16x9&trackingGroup=69017&videoId=' + currentVideo['videoId'];
				twitterShareVideoUrl = 'https://social.newsinc.com/media/json/69017/' + currentVideo['videoId'] + '/singleVideoOG.html?type=VideoPlayer/Default&widgetId=2&trackingGroup=69017&videoId=' + currentVideo['videoId'];
 			
			// Generate the appropriate HTML for this overlay's container element
			this.$el.html(Mustache.to_html(template, {
				'shareTitle': currentVideo['videoTitle'],
				'shareDescription': currentVideo['description'],
				'shareThumb': currentVideo['thumbnailUrl'],
				'shareVideoId': currentVideo['videoId'],
				'addThisPubId': 'ra-518bbde23f7ed2a9',
				'singleVideoFeed': shareVideoUrl,
				'singleTwitterVideoFeed': twitterShareVideoUrl,
				'singleVideoFeedEncoded': encodeURIComponent(shareVideoUrl),
				'singleTwitterVideoFeedEncoded': encodeURIComponent(twitterShareVideoUrl),
				'embedCode': this.model.getEmbedCode(),
				'videoLink': this.model.getDirectLink(),
				'emailLink': "mailto:?subject=" + currentVideo['videoTitle'] + "&body=Check out this video:%0A%0A" + currentVideo['videoTitle'] + "%0A" + encodeURIComponent('https://social.newsinc.com/media/json/69017/'+currentVideo['videoId']+'/singleVideoOG.html?videoId='+currentVideo['videoId'] + '&type=VideoPlayer/16x9&widgetId=2&trackingGroup=69017')
	 		}));
						
	
			// Determine whether the email button should be displayed
			if (!this.model.canShowEmailButton()) {

				//need to hide the icon first, and then the list item
				this.model.get('videoPlayer').getContainerElement().find('.ndn_icon_mail-alt').hide();
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmail').css('margin', '0');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareFacebook').css('margin-left', '9%');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareTwitter').css('margin-left', '9%');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmbed').css('margin-left', '9%');

			}
			if (!this.model.canShowEmbedButton()) {

				//need to hide the icon first, and then the list item
				this.model.get('videoPlayer').getContainerElement().find('.ndn_icon_code').hide();
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmbed').css('margin', '0');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareFacebook').css('margin-left', '9%');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareTwitter').css('margin-left', '9%');
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmbed').css('margin-left', '0%');
			}			
			
			if (!this.model.canShowSocialButton()) {
	
				//need to hide the icon first, and then the list item
				this.model.get('videoPlayer').getContainerElement().find('.ndn_icon_twitter').hide();
				this.model.get('videoPlayer').getContainerElement().find('.ndn_icon_facebook').hide();
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmail').hide();
				this.model.get('videoPlayer').getContainerElement().find('.ndn_icon_code').hide();
			}		
			if (!this.model.canShowSocialButton() || !this.model.canShowEmailButton()){
				this.model.get('videoPlayer').getContainerElement().find('.ndn_shareEmbed').css('margin-left', '10%');
			}				
						
			this.$el.find('.ndn_infoOverlayContainer').hide();
		},

		/**
		 * Select all the text within the textbox for displaying the copiable direct link
		 *
		 */
		selectDirectLink: function() {
			this.$el.find('.ndn_shareCopyVideoLinkText').select();
			
			return false;
		},

		/**
		 * Select all the text within the textbox for displaying the copiable embed code
		 * 
		 */
		 selectEmbedCode: function() {
			this.$el.find('.ndn_shareCopyEmbedCodeText').select();
			
			return false;
		},
				
		showEmbedLink: function(){
			this.$el.find('.ndn_shareBox').fadeToggle("fast", 'linear');			
			this.$el.find('.ndn_embedBox').fadeToggle("fast", 'linear');
			
			return false;
		},
		
				
		/**
		 * @return {jQueryElement} A jQuery wrapped DOMElement that is the container element for the current widget this view belongs to
		 */
		getWidgetContainerElement: function() {
			var widget = this.model.get('videoPlayer').get('widget').getParentWidget();

			return widget.getSplitVersionModel().get('view').$el;
		},
		/**
		 * Resumes play within the associated video player
		 */
		resumePlay: function(event) {
			// Make sure share overlay is hidden if playback is resumed. 
			this.$el.find('.ndn_shareOverlay').hide();			
			this.$el.find('.ndn_infoOverlayContainer').show();
			
			this.model.get('videoPlayer').play();
			
			return false;
		}		
	});
});
