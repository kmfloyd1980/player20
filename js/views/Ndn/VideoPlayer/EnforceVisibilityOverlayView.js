define([
	'jquery',
	'backbone',
	'mustache',
	'models/Ndn/Widget',
	'models/Ndn/Utils/UserAgent',
	
 	'text!templates/ndn/video-player/enforce-visibility-overlay.html'
], function(
	$,
	Backbone,
	Mustache,
	Ndn_Widget,
	Ndn_Utils_UserAgent,
	
	template
) {
	return Backbone.View.extend({
		initialize: function() {
			Backbone.View.prototype.initialize.call(this, arguments);		
						
			// if on mobile use smaller min width & height due to some mobile layouts being < 300px wide. If not on mobile then use standard 300x170 min dimensions. 					
			if(!Ndn_Utils_UserAgent.isMobile()){
				this.$el = $(Mustache.to_html(template, {   
					minimumDimensions: this.model.get('minimumWidth') + 'x' + this.model.get('minimumHeight')
				}));	
			}
			else{
				this.$el = $(Mustache.to_html(template, {  
					minimumDimensions: this.model.get('minimumMobileWidth') + 'x' + this.model.get('minimumMobileHeight')
				}));	
			}
		}
			
	});
});