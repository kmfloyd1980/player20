define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'models/Ndn/Utils/FastClick',
	'models/Ndn/Utils/UserAgent',

	'text!templates/ndn/video-player/pause-overlay/overlay.html',
    'text!templates/ndn/video-player/pause-overlay/carousel.html',
	
	'jquery_imagesloaded',
	'jquery_autoellipsis',
	'jquery_bxslider'	
], function(
	$,
	_,
	Backbone,
	Mustache,
	Ndn_Utils_FastClick,
	Ndn_Utils_UserAgent,
	
	overlayTemplate,
    carouselTemplate
) {
	return Backbone.View.extend({
		events: {
			'click .ndn_pauseOverlay_selectableVideo': 'onVideoSelect',
			'click .ndn_pauseOverlay': 'resumePlay',
			'click .ndn_startPlay': 'resumePlay',
			'click .ndn_sliderNext': 'onSliderNextClick',
			'click .ndn_sliderPrev': 'onSliderPrevClick',
			'click .ndn_toggleDescription': 'toggleDescription',
			'click .ndn_toggleShare': 'openShareOverlay'
		},
		
		stopPropagation: function(event) {
			event.stopPropagation();
		},

		carousel: null,
		slides: 3,
		
		/**
		 * @return {jQueryElement} A jQuery wrapped DOMElement that is the container element for the current widget this view belongs to
		 */
		getWidgetContainerElement: function() {
			var widget = this.model.get('videoPlayer').get('widget').getParentWidget();

			return widget.getSplitVersionModel().get('view').$el;
		},
		
		initialize: function() {
			Backbone.View.prototype.initialize.call(this, arguments);
			
			this.$el = $(document.createElement('div')).addClass('ndn_pauseOverlayContainer ndn_playerOverlay');
			
			this.model.get('videoPlayer')
			.on('videoLoad', $.proxy(function() {
				var isVisible = this.$el.is(":visible");
				if (!isVisible) {
					// Force this overlay to be visible briefly so that the ellipsis plugin may work correctly
					this.$el.appendTo(this.model.get('videoPlayer').getContainerElement()).show();
				}
				
				// Ensure that base html has been generated
				if (!this.$el.html()) this.generateHtml();				
				
				this.synchronizeVideoData();
				
				// Restore the previous state of this overlay's visibility
				this.$el.toggle(isVisible);
			}, this));
		},
		
		generateHtml: function() {
			// Generate the base html used for this overlay's container element
			this.$el.html(Mustache.to_html(overlayTemplate, this.model.getTemplateData()));
			
			// Compensate for mobile devices that may have trouble clicking the play button
			new Ndn_Utils_FastClick(this.$el.find('.ndn_fastClick_touchandclickevent').get(0));
			
            this.generateCarousel();
		},
		
		/**
		 * @return {Boolean} Whether this pause overlay should display a thumbnail carousel
		 */
		usesCarousel: function() {
        	// Only generate a thumbnail carousel for the Single player and when the browser being used is not IE11
			return this.model.get('videoPlayer').get("widget").get("config").type == "VideoPlayer/Default"
        		&& Ndn_Utils_UserAgent.getIeVersion() != 11;
		},
		
        generateCarousel: function() {
            if (this.usesCarousel()) {

            	this.$el.find('.ndn_pauseCarousel').html(
                    Mustache.to_html(carouselTemplate, this.model.getCarouselData())
                );
                // determines if the carousel should have 3 or 5 slides depending on the width of the player. 
                var containerElement = this.model.get('videoPlayer').getContainerElement();
                var hasFiveSlidesCarousel = containerElement.width() >= this.model.get('fiveSlideWidth');
                var hasFourSlidesCarousel = (containerElement.width() >= this.model.get('fourSlideWidth')) && (containerElement.width() < this.model.get('fiveSlideWidth'));
                // should it have 5, 4, or 3 as the maxSlides?
                
                if (!hasFiveSlidesCarousel){
	            	var maxSlidesVar = (hasFourSlidesCarousel) ?  4 : 3;    
                }
                else{
	                var maxSlidesVar = (hasFiveSlidesCarousel) ?  5 : 3;
                }			
				
				var slideWidthVar = (hasFiveSlidesCarousel) ?  200 : 140;
				
				//build the carousel
                this.carousel = this.$el.find('.ndn_sliderItems').bxSlider({
    				pager: false,
    				controls: false,
    				slideWidth: slideWidthVar,
    				slideMargin: 5,
    				minSlides: 3,
    				maxSlides: maxSlidesVar,
    				onSliderLoad: function() {
    					this.$el.find('.ndn_sliderNext, .ndn_sliderPrev')
    					.on('touchstart', function(event) {
    						event.target.css('background-color', '#0099CC'); 
    					})
    					.on('touchend', function(event) {
    						event.target.css('background-color', '#000000');
    					});
    					
    					//this.$el.find('.ndn_sliderThumbnailTitle').ellipsis();
    				}.bind(this),
    				onSlideBefore: function($el, oldSlide, newSlide) {
    					this.loadImagesForSlide(newSlide);
    				}.bind(this)
    			});
            }
        },
				
		synchronizeVideoData: function() {			
			var templateData = this.model.getTemplateData(),
				videoInfoElement = this.$el.find('.ndn_pauseVideoInfo');
			
			videoInfoElement.find('.ndn_videoTitle').html(templateData.currentTitle);
			videoInfoElement.find('.ndn_videoDescription').html(templateData.currentDescription);			
			videoInfoElement.find('.ndn_videoProviderName').html(templateData.providerName);			
			videoInfoElement.find('.ndn_pauseVideoLogo').attr('src', templateData.producerLogo);
			
			if (!this.model.canShowSocialButton()){
		 		this.$el.find('.ndn_icon_share').hide();
	 		} 		

			// Shorten descriptions in the UI using an ellipsis plugin
			// Need to clear out the data value for the title and descriptions. Add elements to this if ellipsis fail to work when loading data over Ajax. 
			//videoInfoElement.find('.ndn_videoTitle').data('jqae', null);
			//videoInfoElement.find('.ndn_videoTitle').ellipsis({updateOnWindowResize: true});
		},
		
			
		/**
		 * Resumes play within the associated video player
		 */
		resumePlay: function(event) {
			// Make sure the pause overlay is hidden if playback is resumed. 						
			this.hide();			
			this.model.get('videoPlayer').play();
			this.$el.find('.ndn_infoOverlayContainer').css('visibility','visible');						
			return false;
		},	
		
		/**
		 * Handles when a new video has been selected to be loaded into the video player
		 */
		onVideoSelect: _.debounce(function(event) {
			this.model.get('videoPlayer').resetContinuousPlayCounter();
			
			// It is important to hide this overlay before calling the play() method on the video player, 
			// due to workaround for Akamai Media Player that plays video/ads at unsolicited times
			this.hide();
			
			this.model.get('videoPlayer')
			.trackVideoStartRequest('pauseOverlayCarouselThumbnail')
			.play($(event.currentTarget).data('video-index'));
			
			return false;
		}, 500, true),
		
		/**
		 * Show this overlay
		 */
		show: function() {
			
			this.$el.prependTo(this.model.get('videoPlayer').getContainerElement()).show();
			
			if (this.usesCarousel()) {
				this.carousel.reloadSlider();
				this.loadImagesForSlide(0);
			}
		},
		
		/**
		 * Hide this overlay
		 */
		hide: function() {
			this.$el.find('.ndn_infoOverlayContainer').show();
			this.$el.find('.ndn_infoOverlayContainer').css('visibility','visible');
			
			this.$el.hide();
		},

		/**
		 * Load the proper images for the pause overlay slider
		 */
		loadImagesForSlide: function(index) {
			// This next line is super convoluted because the bxSlider has a threshold at 83.33333% at which it decides how much of a partially visible slide counts as a valid slide.
			this.slides = ((this.$el.find('.ndn_sliderWrapper').width() % 120) >= 108) ? Math.ceil(this.$el.find('.ndn_sliderWrapper').width() / 120) : Math.floor(this.$el.find('.ndn_sliderWrapper').width() / 120);
			var current = (index * this.slides);
			
			this.$el.find('li.ndn_pauseOverlay_selectableVideo:not(".ndn_carousel-clone")')
			.filter(':eq(' + current + '), :gt(' + current + '):lt(' + this.slides + ')')
			.each(function(index, element) {
				// Find the correct element again and with any clones of said element to compensate for how the third-party carousel library works
				var selectableVideoId = $(element).data('videoId'),
					selectableVideoElement = this.$el.find('.ndn_pauseOverlay_selectableVideo[data-video-id="' + selectableVideoId + '"]');
				
				// For each <img /> element within the selectable video's container element that has a "data-src" attribute,
				// set the value of the "data-src" attribute to the <img /> element's "src" attribute to load its image.
				// Afterwards, remove the "data-src" attribute, as it is no longer needed.
				selectableVideoElement.find('img[data-src]').each(function() {
					$(this)
					.attr('src', $(this).data('src'))
					.removeAttr('data-src');
				});
			}.bind(this));
		},
				
		onSliderPrevClick: function(event) {
			this.carousel.goToPrevSlide();
			return false;
		},

		onSliderNextClick: function(event) {
			this.carousel.goToNextSlide();
			return false;
		},

		toggleDescription: function() {			
			this.descriptionButton = this.$el.find('.ndn_toggleDescription');		
			this.description = this.$el.find('.ndn_videoDescription');
							
			// toggleClass wouldn't work due to the font-awesome using the pseudo selector :before to style the element.
			if (this.descriptionButton.hasClass('ndn_icon_info-circled')){
				this.descriptionButton.removeClass('ndn_icon_info-circled');
				this.descriptionButton.addClass('ndn_icon_cancel-circled');		
				this.description.slideToggle('500', "swing");

			}
			else{
				this.descriptionButton.removeClass('ndn_icon_cancel-circled');
				this.descriptionButton.addClass('ndn_icon_info-circled');
				this.description.slideToggle('500', "swing");
			}						
		},
		
		openShareOverlay: function(event) {
			this.$el.hide();
			this.model.get('videoPlayer').get('overlays')['share'].show();
			
			event.stopPropagation();
			event.preventDefault();
			return false;
		}
	});
});
