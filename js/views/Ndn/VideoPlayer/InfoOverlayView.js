define([
	'jquery',
	'mustache',
	'backbone',
	'models/Ndn/Utils/FastClick',
	
	'text!templates/ndn/video-player/info-overlay.html',
	
	'jquery_autoellipsis'
], function(
	$,
	Mustache,
	Backbone,
	Ndn_Utils_FastClick,
	
	template
) {
	return Backbone.View.extend({
		events: {
			'click .ndn_toggleInfoDescription': 'toggleDescription',
			'click .ndn_toggleShare': 'openShareOverlay'
		},
		
		initialize: function() {
			Backbone.View.prototype.initialize.call(this, arguments);
			
			this.setElement($('<div class="ndn_infoOverlayContainer ndn_playerOverlay">').get(0));
		},
  		
		/**
		 * Handles when a user clicks on the "More Info" button when the video is playing. Shows or hides the description.
		 */			
		toggleDescription: function() {
			this.descriptionButton = this.$el.find('.ndn_toggleInfoDescription');			
			this.description = this.$el.find('.ndn_videoInfoDescription');
			
			// toggleClass wouldn't work due to the font-awesome using the pseudo selector :before to style the element. I personally think there is a better way to handle this but so far this is the only solution at the moment.
			if (this.descriptionButton.hasClass('ndn_icon_info-circled')){
				this.descriptionButton.removeClass('ndn_icon_info-circled');
				this.descriptionButton.addClass('ndn_icon_cancel-circled');
				this.description.slideToggle('500', "swing");
			}
			else{
				this.descriptionButton.removeClass('ndn_icon_cancel-circled');
				this.descriptionButton.addClass('ndn_icon_info-circled');
				this.description.slideToggle('500', "swing");
			}						
		},
		
		openShareOverlay: function(event) {			
			this.model.get('videoPlayer').pause();			
			this.model.get('videoPlayer').get('overlays')['info'].hide();
			this.model.get('videoPlayer').get('overlays')['pause'].hide();
			this.model.get('videoPlayer').get('overlays')['share'].show();
			
			return false;
		},
		
		updateHtml: function() {			
 			var currentVideo = this.model.get('videoPlayer').getCurrentVideoData();
			
			// Generate the appropriate HTML for this overlay's container element
 			this.$el.html(Mustache.to_html(template, {
				'videoTitle': currentVideo['videoTitle'],
				'videoDescription': currentVideo['description'],
				'videoProviderLogo': currentVideo['provider']['logoUrl'],
				'videoProviderName': currentVideo['provider']['name']
	 		}));
	 		
	 		// Hide Share icon if you cant show social icons.
			if (!this.model.canShowSocialButton()){
		 		this.$el.find('.ndn_icon_share').hide();
	 		} 		
	 					
			// videoInfoElement.find('.ndn_videoTitle').ellipsis({updateOnWindowResize: true});						
			// Apply an ellipsis to shorten the video's description appropriately
			// this.$el.find('.ndn_videoTitle').ellipsis({updateOnWindowResize: true});
		}
	});
});