var _ndnq = _ndnq || [];

define([
	'jquery',
	'underscore',
	'backbone',
	'mustache',
	'models/Ndn/App',
    'models/Ndn/Widget',
    'models/Ndn/Widget/Config',
	'models/Ndn/ModalEmbed',
	'models/Ndn/Utils/UserAgent',
	'models/Ndn/Carousel',
	
	'jquery_autoellipsis',
	'jquery_responsive_containers'
], function(
	$,
	_,
	Backbone,
	Mustache,
	Ndn_App,
    Ndn_Widget,
    Ndn_Widget_Config,
	Ndn_ModalEmbed,
	Ndn_Utils_UserAgent,
	Ndn_Carousel
) {
	return Backbone.View.extend({
		/**
		 * TODO: Refactor whatever this is, out.
		 */
		slides: 1,
		
		/**
		 * TODO: Refactor this out. Ensure that Ndn_AbstractView.prototype.initialize.apply(this, arguments); is being called appropriately for when this method is removed.
		 */
		initialize: function() {
			// Handle the CSS hover logic using the "ndn_hover" class and JavaScript to artificially linger the 
			// "hover" state on mobile devices when a "ndn_sliderNavButton" element on "mouseenter" and "mouseleave" events
			// TODO: Consider using native "touchstart" and "touchend" events instead. Are these events on all mobile devices?
			this.$el
			.on('mouseenter', '.ndn_sliderNavButton', function() {
				$(this).addClass('ndn_hover');

				if (Ndn_Utils_UserAgent.isMobile() || Ndn_Utils_UserAgent.isOnIos()) {
					setTimeout(function() {
						$(this).removeClass('ndn_hover');
					}.bind(this), 700);
					
				}
			})
			.on('mouseleave', '.ndn_sliderNavButton', function() {
				$(this).removeClass('ndn_hover')
			});
		},

		/**
		 * TODO: Document this function.
		 */
		loadImagesForSlide: function(slideIndex, nestedListSelector) {
			Ndn_Carousel.loadSlidePlaceholderImages(this.$el, slideIndex, this.slides, nestedListSelector);
		},
		
		/**
		 * @param {function} template
		 * @param {Object} params
		 * @param {?String|Boolean} [selector] jQuery selector relative to the view's container element for the container element 
		 * to update. If no selector is provided the view's root container element is used. Additionally, if the value of TRUE is 
		 * provided, then the rendered html is returned instead of applied to any element.
		 */
		render: function(template, params, selector) {
			// console.log('Ndn_AbstractView.render(); where params ==');
			// console.log(params);
			
			var html = Mustache.to_html(template, params); // template(params);
			
			var result;
			
			if (selector === true) {
				result = html;
			}
			else {
				var containerElement = (selector ? this.$el.find(selector) : this.$el);
				containerElement.html(html);
			}
			
			this.trigger('render', {
				template: template,
				params: params,
				selector: selector
			});
			
			return result;
		},
		
		/**
		 * Display companion advertisement content to a container element
		 * @param {jQueryElement} containerElement The container element to hold the companion ad content
		 * @param {String} content The companion ad's content in HTML
		 */
		displayCompanionAd: function(containerElement, content) {
			// console.log('displayCompanionAd(', arguments, ');');
			
			// If the <html> tag is inside the content, then create an iframe and place the content within it
			if (content.indexOf('<html') !== -1) {
				var iframe, 
					iframeDocument;
				
				containerElement.html(
					iframe =
					$(document.createElement('iframe'))
					.css({
						width: '300px',
						height: '250px',
						overflow: 'hidden'
					})
					.attr('scrolling', 'no')
				);
				
				try {
					iframeDocument = iframe.get(0).contentWindow.document;
					iframeDocument.open();
					iframeDocument.write(content);
					iframeDocument.close();
				}
				catch(error) {
					// console.log('ERROR ==', error);
					
					this.trigger('analytics:error', {
						type: 'AdCompanionError',
						error: error
					});
				}
			}
			else {
				// If there is no <html> tag inside the content, just simply add the content to the appropriate <div> element
				containerElement.html(content);
			}
		},

		openNdnWindow: function(url) {
			var launcherSettings = this.model.getParentWidget().get('settings').settings.launcher;
			
			if (launcherSettings.behavior == 'studio') {
				window.open(url, "_blank");
			}
			else if (launcherSettings.behavior == 'modal' && !Ndn_Utils_UserAgent.isMobile()) {
				new Ndn_ModalEmbed(this.model, Ndn_Widget_Config.getConfigFromLandingPageUrl(url));
				return false;
			}
			else {
				window.open(url, "ndnPlayer", "scrollbars,resizable,width=768,height=825", !1);
			}
		}
	});
});
