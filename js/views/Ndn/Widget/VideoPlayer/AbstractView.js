define([
	'jquery',
	'underscore',
	'mustache',
	'views/Ndn/AbstractView',
	'models/Ndn/Utils/UserAgent',
	
	'text!templates/ndn/video-player/pause-overlay/overlay.html'
	
], function(
	$,
	_,
	Mustache,
	Ndn_AbstractView,
	Ndn_Utils_UserAgent,
	
	overlayTemplate
) {
	return Ndn_AbstractView.extend({
		events: {
			'click .ndn_selectableVideo': 'onVideoSelect',
			'click .ndn_playlistSelectorMenu a': 'onPlaylistSelect'
		},
		
		showingPlaylistPause: false,

		initialize: function() {
			// console.log('>> Ndn_Widget_VideoPlayer_AbstractView.initialize(); containerElementId == ' + this.model.getContainerElement().attr('id'));

			Ndn_AbstractView.prototype.initialize.apply(this, arguments);

			// Once this widget's video player is initialized and accessible, attach event listeners to the video player
			this.model.on('videoPlayerInitialized', $.proxy(function() {
				
				this.model.get('videoPlayer').on({
					// When a new video playlist is loaded into the video player, update the selectable videos accordingly
					'videoPlaylistLoad': $.proxy(function() {
						this.updateSelectableVideos();
					}, this),
					
					// When a new video video is loaded into the video player, update the selected video accordingly
					'videoLoad': $.proxy(function() {
						this.updateSelectedVideo();
					}, this),
					
					//when the video is paused hide the info panel
					'videoPause': $.proxy(function(){
						this.$el.find('.ndn_infoOverlayContainer').css('visibility','hidden');		
					}, this),
					
					//when the video is playing show the info panel
					'videoStartRequest': $.proxy(function(){
						this.$el.find('.ndn_infoOverlayContainer').css('visibility','visible');		
					}, this),
					
					'adStart': $.proxy(function() {						
						this.$el.find('.ndn_infoOverlayContainer').css('visibility','hidden');		
					}, this),
					
					'adEnd': $.proxy(function() {						
						this.$el.find('.ndn_infoOverlayContainer').css('visibility','visible');		
					}, this)
				});
			}, this));
		},

		/**
		 * Hides all the overlays that can possibly overlay the widget's video player
		 */
		hidePlayerOverlays: function() {
			this.$el.find('.ndn_playerOverlay').hide();
		},
				
		generateHtml: function() {
			// Generate the base html used for this overlay's container element
			this.$el.html(Mustache.to_html(overlayTemplate, this.model.getTemplateData()));
            
		},

		/**
		 * Handles when a new video has been selected to be loaded into the video player
		 */
		onVideoSelect: _.debounce(function(event) {
			// console.log('Ndn_Widget_VideoPlayer_AbstractView.onVideoSelect();');
			
			// Indicate that a video has been selected by the user to be played
			this.model.trigger('videoSelect');

			this.hidePlayerOverlays();

			var videoIndex = $(event.currentTarget).data('video-index');

			// Indicate which video is currently selected within the list of selectable videos
			var selectableVideos = this.$el.find('.ndn_selectableVideos .ndn_selectableVideo');
			selectableVideos.removeClass('ndn_activeVideo');
			selectableVideos.filter(':eq(' + videoIndex + ')').addClass('ndn_activeVideo');

			this.model.get('videoPlayer').resetContinuousPlayCounter();
			this.model.get('videoPlayer').play(videoIndex);

			return false;
		}, 1000, true),

		/**
		 * Handles when a new playlist is selected to be loaded into the video player
		 */
		onPlaylistSelect: function(event) {
			// console.log('onPlaylistSelect();');

			this.hidePlayerOverlays();

			// Load the playlist into the video player and then load the first video within said playlist
			this.model.get('videoPlayer').resetContinuousPlayCounter();
			this.model.get('videoPlayer').load(0, $(event.target).data('playlist-id'), this.model.get('config')['autoPlay']);
			return false;
		},

		/**
		 * Update this widget's active playlist's thumbnails (if this model has a template identified as "selectableVideos")
		 */
		updateSelectableVideos: function() {
			var selectableVideosTemplate = this.model.getTemplate('selectableVideos');
			if (selectableVideosTemplate) {
				this.render(selectableVideosTemplate, {
					'playlistItems': this.model.get('videoPlayer').getActivePlaylistData()
						? this.model.get('videoPlayer').getActivePlaylistData().items
						: [],
					'videoIndex': (function() {
						var videoIndex = 0;
						return function() {
							return videoIndex++;
						};
					})()
				}, '.ndn_selectableVideos');
			}
		},

		/**
		 * Updates the user interface indicator of which selectable video is currently active
		 */
		updateSelectedVideo: function() {
			//console.log('Ndn_Widget_VideoPlayer_AbstractView.updateSelectedVideo();');
			
			// TODO: Refactor out the shared ".ndn_pauseOverlay_selectableVideo" class selector. This should be encapsulated within the PauseOverlayView/Model!
			var videoPlayer = this.model.get('videoPlayer'),
				currentVideoData = videoPlayer.getCurrentVideoData(),
				selectableVideoElements = this.$el.find('.ndn_selectableVideo,.ndn_pauseOverlay_selectableVideo'),
				adsEnabled = this.model.get('config')['adsEnabled'];
			
			// Reset all selectable video indicators of whether any selectable videos are "Up Next" or "Now Playing"
			selectableVideoElements.removeClass('ndn_activeVideo ndn_activeVideoNotPlaying');
			
			if (currentVideoData !== null) {
				var currentVideoId = currentVideoData['videoId'],
					currentVideoElements = selectableVideoElements.filter('[data-video-id="' + currentVideoId + '"]');
				
				/**
				 * @return {Boolean} Whether the video currently loaded into this product's video player is the same as it was when this method (this.updateSelectedVideo) was originally called
				 */
				var hasSameVideoLoaded = function() {
					var currentVideoData = videoPlayer.getCurrentVideoData();
					return currentVideoData !== null && currentVideoData['videoId'] == currentVideoId;
				};
				
				if (videoPlayer.isShowingRmm()) {
					// Indicate which video thumbnail(s) are "Up Next"
					currentVideoElements.addClass('ndn_activeVideoNotPlaying');
				}
				else {
					if (adsEnabled) {
						// Indicate which video thumbnail(s) are "Up Next"
						currentVideoElements.addClass('ndn_activeVideoNotPlaying');
						
						// Once the ad has ended, if the video player still has the same video loaded, then indicate that the appropriate 
						// video thumbnail(s) are no longer "Up Next" and are now "Now Playing"
						videoPlayer.once('adEnd', function() {
							if (hasSameVideoLoaded()) {
								currentVideoElements
								.removeClass('ndn_activeVideoNotPlaying')
								.addClass('ndn_activeVideo');
							}
						});
					}
					else {
						// Indicate which video thumbnail(s) are "Now Playing"
						currentVideoElements.addClass('ndn_activeVideo');
					}
				}
			}
		}		
		
	});
});
