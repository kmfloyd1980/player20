define([
	'jquery',
	'views/Ndn/Widget/VideoPlayer/AbstractView'
], function($, Ndn_Widget_VideoPlayer_AbstractView) {
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		initialize: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);
		}
	});
});