define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoPlayer/AbstractView',
	'models/Ndn/Utils/UserAgent',

	'jquery_autoellipsis'
], function(
	$,
	_,
	Ndn_Widget_VideoPlayer_AbstractView,
	Ndn_Utils_UserAgent
) {
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		events: {
			'click .ndn_selectableVideo': 'onVideoSelect',
			'mouseenter .ndn_selectableVideos': 'onUserInteractionStart',
			'touchstart .ndn_selectableVideos': 'onUserInteractionStart',
			'mouseleave .ndn_selectableVideos': 'onUserInteractionEnd',
			'touchend .ndn_selectableVideos': 'onUserInteractionEnd'
		},
		
		/**
		 * Temporarily deactivates the scroll synchronization of the selectable videos while the user is interacting with the player 
		 * by either mousing over or touching (via tablet device) the scrollable pane of selectable videos
		 */
		onUserInteractionStart: function() {
			this.model.set({
				'isInteractingWithUser': true
			});
			
			this.dequeueScrollSync();
		},

		/**
		 * Reactivates the scroll synchronization of the selectable videos while the user is interacting with the player 
		 * by either mousing over or touching (via tablet device) the scrollable pane of selectable videos
		 */
		onUserInteractionEnd: function() {
			this.model.set({
				'isInteractingWithUser': false
			});
			
			this.queueScrollSync();
		},
		
		initialize: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);
			
			this.model.on('videoPlayerInitialized', $.proxy(function() {
				// When a video gets loaded into the video player, update the current video details
				this.model.get('videoPlayer')
				.on('videoLoad', $.proxy(function(eventData) {
					var currentVideoData = eventData.videoData;
					
					this.$el.find('.ndn_videoPlayerHeader .ndn_videoTitle').html(currentVideoData['videoTitle']);
					this.$el.find('.ndn_videoPlayerHeader .ndn_videoProvider').html(currentVideoData['provider']['name']);
					this.$el.find('.ndn_videoPlayerHeader .ndn_videoProviderLogo').html(
						$(document.createElement('img'))
						.attr({
							'src': currentVideoData['provider']['logoUrl'],
							'alt': currentVideoData['provider']['name']
						})
					);
					
					//console.log('html ==', this.$el.find('.ndn_videoPlayerHeader').html());
					
					if (!this.isInteractingWithUser()) {
						this.syncScrollPosition();
					}
				}, this));
				
				// Handle companion ads
				this.model.get('videoPlayer')
				.on('adCompanion', $.proxy(function(event) {
					// console.log('adCompanion, event ==', event);
					
					// Show the companion when we get one
					this.showCompanionAd(event.content);
				}, this))
				// Hide the companion again at the end of a video
				.on('adEnd', $.proxy(this.hideCompanionAd, this));
				
				this.model.get('videoPlayer')
				.on('embed', $.proxy(function() {
					// When the video player throws the event that indicates that playing an ad or video has been requested, make sure to disable the "auto-advance" feature
					this.model.get('videoPlayer').on('videoStartRequest', $.proxy(function() {
						this.model.disableAutoAdvance();
					}, this));
				}, this));
			}, this));
		},
		
		updateSelectableVideos: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.updateSelectableVideos.apply(this, []);
			
			// Shorten video descriptions as needed
			this.$el.find('.ndn_selectableVideos .ndn_videoDescription').ellipsis();
		},
		
		/**
		 * Shows a companion ad
		 * @param {String} content The HTML content of the companion ad to be displayed
		 */
		showCompanionAd: function(content) {
			// Hide the selectable video thumbnails
			this.$el.find('.ndn_selectableVideos').hide();
			
			var adCompanionContainer = this.$el.find('.ndn_adCompanion');
			this.displayCompanionAd(adCompanionContainer, content);
			adCompanionContainer.addClass('ndn_active');
		},
		
		/**
		 * Hides the companion ad
		 */
		hideCompanionAd: function() {
			this.$el.find('.ndn_adCompanion')
			.removeClass('ndn_active')
			.html('');

			// Show the selectable video thumbnails
			this.$el.find('.ndn_selectableVideos').show();
		},
		
		/**
		 * Dequeues any queued request to sync the widget's scroll position to show the currently selected video
		 */
		dequeueScrollSync: function() {
			if (this.model.get('queueScrollSyncCountdown') !== false) {
				clearInterval(this.model.get('queueScrollSyncCountdown'));
				this.model.set({
					'queueScrollSyncCountdown': false
				});
			}
		},
		
		/**
		 * Initialize a timer that once timed-out will re-engage the widget's scroll synchronization that will scroll to show the 
		 * currently selected video in the middle of the selectable videos scroll window
		 */
		queueScrollSync: function() {
			/**
			 * The number of seconds from the time this method is called until this widget re-engages its scroll sync so that the
			 * currently selected video is scrolled to appropriately
			 * @var {int}
			 */
			var countdownTime = 3;
			
			// If a scroll sync is queued up, dequeue it so we can queue it again
			this.dequeueScrollSync();

			// Keep track of the countdown for the next X-number of seconds
			this.model.set({
				'queueScrollSyncCountdown': setInterval($.proxy(function() {
					// Clear the interval and call the "onNoUserInteraction" handler
					clearInterval(this.model.get('queueScrollSyncCountdown'));
					this.model.set({
						'queueScrollSyncCountdown': false
					});
					
					this.syncScrollPosition();
				}, this), countdownTime * 1000)
			});
		},
		
		/**
		 * @return {Boolean} Whether the user is considered "interacting" with the widget
		 */
		isInteractingWithUser: function() {
			return this.model.get('isInteractingWithUser');
		},
		
		/**
		 * Synchronizes the scroll position of the selectable videos window pane to show the currently active video in the middle of the window pane,
		 * which is the default behavior, or if this widget is not in "auto-advance" mode, the set of three videos are shown
		 */
		syncScrollPosition: function() {
			var selectableVideoHeight = 90;
			
			var scrollTopValue = this.model.isAutoAdvanceEnabled()
				? (Math.floor(this.model.get('videoPlayer').get('currentVideoIndex') / 3) * (selectableVideoHeight * 3))
				: (Math.max(this.model.get('videoPlayer').get('currentVideoIndex') - 1, 0) * selectableVideoHeight);
			
			var selectableVideos = this.$el.find('.ndn_selectableVideos');
			
			// If the last video is selected, where the scrollTop value exceeds the scrollable height, set the scrollTop value to a scrollable height
			if (scrollTopValue + selectableVideos.height() == selectableVideos.get(0).scrollHeight + selectableVideoHeight) {
				scrollTopValue -= selectableVideoHeight;
			}
			
			if (selectableVideos.get(0).scrollTop != scrollTopValue) {
				selectableVideos.animate({
					scrollTop: scrollTopValue
				}, {
					duration: 1000,
					done: $.proxy(function() {
						this.syncScrollPosition();
					}, this)
				});
			}
		},
		
		onVideoSelect: _.debounce(function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.onVideoSelect.apply(this, arguments);
			
			this.syncScrollPosition();
			
			return false;
		}, 500, true)
	});
});
