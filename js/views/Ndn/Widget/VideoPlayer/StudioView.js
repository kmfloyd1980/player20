define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoPlayer/AbstractView'
], function(
	$,
	_,
	Ndn_Widget_VideoPlayer_AbstractView
) {
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		initialize: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);
			
			this.model.on('videoPlayerInitialized', $.proxy(function() {
				// Handle companion ads (just bubble this up as a hook event for now)
				this.model.get('videoPlayer')
				.on('adCompanion', $.proxy(function(event) {
					this.model.trigger('hook:adCompanion', {content: event.content});
				}, this));
			}, this));
		},
		
		/**
		 * Synchronize the side panel to display appropriately based on whether there is a companion ad currently displayed
		 */
		syncSidePanel: function() {
			var isShowingCompanionAd = this.$el.find('.ndn_sidePanel').hasClass('ndn_hasCompanionAd');

			this.$el.find('.ndn_selectableVideos').css('height', this.getSelectableVideosContainerHeight(isShowingCompanionAd));				
			this.$el.find('.ndn_sidePanel').toggleClass('ndn_hasCompanionAd', isShowingCompanionAd);
		},
		
		/**
		 * @param {Boolean} isShowingCompanionAd Whether a companion ad is being displayed
		 * @return {int} The height (measured in pixels) concerning how tall the selectable videos container element should be
		 */
		getSelectableVideosContainerHeight: function(isShowingCompanionAd) {
			var result = this.$el.find('.ndn_playerEngagement').height();
			
			// Remove height reserved for selectable videos' header
			result -= 34;
			
			// Reserve height if there is a companion ad being displayed
			if (isShowingCompanionAd) {
				result -= 260;
			}
			
			return result;
		},
		
		/**
		 * Shows a companion ad
		 * @param {String} content The HTML content of the companion ad to be displayed
		 */
		showCompanionAd: function(content) {
			this.model.set('isShowingCompanionAd', true);
			
			this.syncSidePanel();
			
			var adCompanionContainer = this.$el.find('.ndn_adCompanion');
			this.displayCompanionAd(adCompanionContainer, content);
			adCompanionContainer.addClass('ndn_active');
		},
		
		/**
		 * Hides the companion ad
		 */
		hideCompanionAd: function() {
			this.model.set('isShowingCompanionAd', false);
			
			this.$el.find('.ndn_adCompanion')
			.removeClass('ndn_active')
			.html('');
			
			this.syncSidePanel();
		},

		/**
		 * Handles when a new video has been selected to be loaded into the video player
		 */
		onVideoSelect: _.debounce(function(event) {
			// Indicate that a video has been selected by the user to be played
			this.model.trigger('videoSelect');

			this.hidePlayerOverlays();

			var videoId = $(event.currentTarget).data('video-id');

			// Indicate which video is currently selected within the list of selectable videos
			var selectableVideos = this.$el.find('.ndn_selectableVideos .ndn_selectableVideo');
			selectableVideos.removeClass('ndn_activeVideo');
			
			// selectableVideos.filter(':eq(' + videoIndex + ')').addClass('ndn_activeVideo');

			this.model.get('videoPlayer').resetContinuousPlayCounter();
			this.model.get('videoPlayer').loadVideoById(videoId, true);
			
			return false;
		}, 1000, true)
	});
});
