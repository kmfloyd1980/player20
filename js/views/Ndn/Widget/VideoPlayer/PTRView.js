define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoPlayer/AbstractView',
	'views/Ndn/Widget/VideoLauncher/AbstractSliderView',
	'models/Ndn/Utils/UserAgent',
  	'models/Ndn/Widget',
  	'models/Ndn/Debugger',

	'jquery_imagesloaded',
	'jquery_bxslider',
	'jquery_autoellipsis'
], function(
	$,
	_,
	Ndn_Widget_VideoPlayer_AbstractView,
	Ndn_Widget_VideoLauncher_AbstractSliderView,
	Ndn_Utils_UserAgent,
	Ndn_Widget,
	Ndn_Debugger
) {
	// TODO: Merge the PTR model/view into the Slider model/view and make the Ndn_VideoPlayer library accept a player behavior ("pb" config setting) value
	// that describes the "extended preview" mode of the video player (where "pb" is set to 4)
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		events: {
			'mouseenter .ndn_videoPlayerWrapper': 'onUserInteractionStart',
			'touchstart .ndn_videoPlayerWrapper': 'onUserInteractionStart',
			'mouseleave .ndn_videoPlayerWrapper': 'onUserInteractionEnd',
			'touchend .ndn_videoPlayerWrapper': 'onUserInteractionEnd',
			'click .ndn_sliderContainer .ndn_selectableVideo a': 'onThumbnailClick',
			'click .ndn_sliderNext': 'onNextClick',
			'click .ndn_sliderPrev': 'onPrevClick',
			'click .ndn_playlistThumbnails li a': 'onThumbnailClick'
		},

		initialize: function(event) {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);
			
			// Apply the optimal width to the video player when it plays ads
			// this.initializeOptimalVideoPlayerWidthBehavior(); // Removed from Sprint #17
			
			// Hide the video player whenever it is waiting for its ad/content to load
			this.hideVideoPlayerWhileLoading();
			
			// Initialize the maintenance of the top container's aspect ratio
			this.initializeAspectRatioOfTopContainer();
			
			// TODO: Take the process of merging this code with the VideoLauncher/Slider even further by removing the need for this model/view 
			// and making the "extended preview" mode a config setting for embedded products
			// 
			// Add this class so that our VideoLauncher/Slider CSS is not unnecessarily repeated
			this.$el.addClass('ndn_widget_VideoLauncher-Slider');

			this.model.on('change:userHasInteracted', $.proxy(function() {
			    if (this.model.isPtr()) {
	                // If the user interacts with the player during an rmm bumper.
	                if (this.model.get('userHasInteracted') && !this.model.get('hasConcludedVideoPlayer')) {
	                    this.model.unmutePlayer();
	                    this.disablePreviewMode();
	                }
			    }
			}, this));
			
			this.once('render', function(e) {
				this.renderThumbnailPlaceholders();
				if (!this.model.getThumbnailsEnabled()) {
					this.$el.find('.ndn_playlistThumbnails').hide();
				}
			});
			
			this.model.on('videoPlayerInitialized', $.proxy(function() {
			    // Initialize the preview length for the PTR experience
			    var settings = this.model.getCachedSettings();
		        this.model.set('videoLengthToPlay', settings.settings.behavior.ptr.previewLength);
		        
		        if (Ndn_Debugger.inMode('ptr')) console.log('[Ndn_Widget_VideoPlayer_PTRView] previewLength ==', this.model.get('videoLengthToPlay'));
		        
                this.model.get('videoPlayer').on({
                    'adStart': $.proxy(function() {
                        this.clearCurrentVideoThumbnail();
                        this.setCurrentVideoThumbnail(this.model.get('videoPlayer').getCurrentVideoId());
                        this.goToThumbnailSlide(this.model.get('videoPlayer').getCurrentVideoId());
                    }, this),
                    'adEnd': $.proxy(function() {
                        // If the user has interacted with this product after an ad has played, disable preview mode to play the entire video
                        if (this.model.get('userHasInteracted')) {
                            this.disablePreviewMode();
                        }
                    }, this),
                    'videoTimeUpdate': $.proxy(function(e) {
                        // If preview mode is enabled and the video has progressed further than the duration of the preview mode, conclude the video player's preview mode
                        if (this.isPreviewModeEnabled() && e.data.currentTime > this.model.get('videoLengthToPlay') ) {
                            this.concludePreviewMode();
                        }
                    }, this),
                    'videoUnload': $.proxy(function() {
                        // TODO: This should be refactored so that "videoUnload" does not have to be listened to
                        // TODO: Fix this so that an extra "videoLoad" event does not unnecessarily occur -- is causing analytics to be misleading, making an unneeded "/vl" and "/acr" analytics calls
                        if (Ndn_Debugger.inMode('ptr')) console.log("!!this.model.get('videoPlayer').isMuted(true) ==", !!this.model.get('videoPlayer').isMuted(true));
                        
                        this.model.set('previousVideoMuted', !!this.model.get('videoPlayer').isMuted(true));
                    }, this),
                    'videoEnd': $.proxy(function() {
                        this.clearCurrentVideoThumbnail();
                        
                        if (Ndn_Debugger.inMode('ptr')) console.log('[Ndn_Widget_VideoPlayer_PTRView] shouldPlaybackEndUponVideoEnd ==', this.model.shouldPlaybackEndUponVideoEnd());
    
                        if (this.model.shouldPlaybackEndUponVideoEnd()) {
                            this.concludePreviewMode();
                        }
                    }, this),
                    'rmmContinuousPlayEnd': $.proxy(function() {
                        this.concludePreviewMode();
                    }, this),
                    'videoStart': $.proxy(function() {
                        this.setCurrentVideoThumbnail(this.model.get('videoPlayer').getCurrentVideoId());
                        this.goToThumbnailSlide(this.model.get('videoPlayer').getCurrentVideoId());
                    }, this)
                })
                .once({
                    'videoLoad': $.proxy(function() {
                        if (this.model.getThumbnailsEnabled()) {
                            this.renderThumbnails();
                        }
                    }, this)
                });
		    }, this));
		},
		
		/**
		 * @return {Boolean} Whether the preview mode of the video player is enabled
		 */
		isPreviewModeEnabled: function() {
			return this.model.get('videoLengthToPlay') > 0;
		},
		
		/**
		 * Disables the preview mode of the player so that the video player plays loaded videos in their entirety
		 */
		disablePreviewMode: function() {
			this.model.set('videoLengthToPlay', 0);
		},
		
		/**
		 * Concludes the video player's preview mode
		 * @param {Boolean} [fadeOut] Whether the video player should fade out before transitioning to the slider; defaults to true
		 */
		concludePreviewMode: function(fadeOut) {
			fadeOut = _.isUndefined(fadeOut) ? true : fadeOut;
			
			this.model.set('hasConcludedVideoPlayer', true);

			this.updateSelectableVideos().done($.proxy(function() {
				this.setCurrentVideoThumbnail(this.$el.find('[data-video-index="0"]').data('videoId'));
				
				var sliderContainer = this.$el.find('.ndn_sliderContainer');
				
				var self = this;
				
				if (fadeOut) {
					sliderContainer.find('.ndn_videoPlayer')
					.fadeTo(1000, 0.01)
					.fadeOut(1000, function() {
						self.transitionToSlider();
					});				
				}
				else {
					self.transitionToSlider();
				}

				this.model.destroyPlayer();
			}, this));
		},

		transitionToSlider: function() {
			// Once the showcase carousel is forcibly initialized, transition to and display the showcase carousel
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initializeShowcaseCarousel.apply(this, [true]).done($.proxy(function() {
				var carousel = this.model.get('carousel'),
					sliderContainer = this.$el.find('.ndn_sliderContainer'),
					videoPlayerWrapperElement = sliderContainer.find('.ndn_videoPlayerWrapper');
				
				carousel.stopAuto();
				carousel.goToSlide(0);
				carousel.startAuto();
				
				videoPlayerWrapperElement.hide();
				videoPlayerWrapperElement.detach();
			}, this));
		},

		onUserInteractionStart: function() {
			this.model.setUserInteractionTimer();
		},

		onUserInteractionEnd: function() {
			this.model.unsetUserInteractionTimer();
		},

		onPrevClick: function(){
			this.model.get('carousel').stopAuto();
			this.model.get('carousel').goToPrevSlide();
			this.model.get('carousel').startAuto();

			return false;
		},

		onNextClick: function(){
			this.model.get('carousel').stopAuto();
			this.model.get('carousel').goToNextSlide();
			this.model.get('carousel').startAuto();

			return false;
		},
		
		/**
		 * Displays the indicator on a video thumbnail that identifies which video is currently being displayed within the showcase carousel 
		 * @param {String} videoId The video ID of the video currently being displayed in the showcase carousel
		 */
		setCurrentVideoThumbnail: function(videoId) {
			this.$el.find('.ndn_playlistThumbnails [data-video-id="' + videoId + '"] .ndn_currentVideoThumbnail').fadeIn(400);
		},
		
		/**
		 * Removes the indicator that identifies which video thumbnail is currently being displayed in the showcase carousel
		 */
		clearCurrentVideoThumbnail: function() {
			this.$el.find('.ndn_currentVideoThumbnail').fadeOut(400);
		},
		
		goToThumbnailSlide: function(videoId) {
		    // Only go to the specified thumbnail slide if thumbnails are enabled
			if (this.model.getThumbnailsEnabled()) { // this.model.get('thumbnailCarousel')) {
	            var index = this.$el.find('.ndn_playlistThumbnails [data-video-id="' + videoId + '"]').data('videoIndex');
	            this.model.get('thumbnailCarousel').goToSlide(Math.floor(index/3));
			}
		},
		
		/**
		 * Handles when a video thumbnail or still image is clicked on
		 */
		onThumbnailClick: function(event) {
			// Launch the product specified by the clicked <a> element
			this.openNdnWindow($(event.currentTarget).attr('href'));
			
			// Indicate to the model that a thumbnail has been clicked
            this.model.onThumbnailClick();
            
            // Stop the default behavior of this event as wells as its further propagation
			event.preventDefault();
			event.stopPropagation();
            return false;
		},
		
		hideVideoPlayerWhileLoading: function() {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.hideVideoPlayerWhileLoading.apply(this, arguments);
		},
		
		containedWithinOverflowXHiddenContainer: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.containedWithinOverflowXHiddenContainer.apply(this, arguments);
		},
		
		initializeOptimalVideoPlayerWidthBehavior: function() {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initializeOptimalVideoPlayerWidthBehavior.apply(this, arguments);
		},

		renderThumbnailPlaceholders: function() {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.renderThumbnailPlaceholders.apply(this, arguments);
		},

		renderThumbnails: function() {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.renderThumbnails.apply(this, arguments);
		},

		centerThumbnails: function() {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.centerThumbnails.apply(this, arguments);
		},

		updateSelectableVideos: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.updateSelectableVideos.apply(this, arguments);
		},
		
		setVideoPlayerOnScreen: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.setVideoPlayerOnScreen.apply(this, arguments);
		},
		
		ensureOptimalVideoPlayerWrapperWidth: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.ensureOptimalVideoPlayerWrapperWidth.apply(this, arguments);
		},
		
		ensureOptimalVideoPlayerWidth: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.ensureOptimalVideoPlayerWidth.apply(this, arguments);
		},
		
		restoreOriginalVideoPlayerWidth: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.restoreOriginalVideoPlayerWidth.apply(this, arguments);
		},
		
		restoreOriginalVideoPlayerWrapperWidth: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.restoreOriginalVideoPlayerWrapperWidth.apply(this, arguments);
		},
		
		initializeAspectRatioOfTopContainer: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initializeAspectRatioOfTopContainer.apply(this, arguments);
		},
		
		initializeShowcaseCarousel: function() {
			return Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initializeShowcaseCarousel.apply(this, arguments);
		}
	});
});
