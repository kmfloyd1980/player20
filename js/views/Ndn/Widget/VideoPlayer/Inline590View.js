define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoPlayer/AbstractView',
	'models/Ndn/Utils/UserAgent',

	'jquery_autoellipsis',
	'jquery_imagesloaded',
	'jquery_bxslider'
], function(
	$,
	_,
	Ndn_Widget_VideoPlayer_AbstractView,
	Ndn_Utils_UserAgent
) {
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		events: {
			'click .ndn_selectableVideo': 'onVideoSelect',
			'click .ndn_sliderNext': 'onNextClick',
			'click .ndn_sliderPrev': 'onPrevClick',
			'click .ndn_closeShare': 'hideShare',
			'click .ndn_shareCopyEmbedCodeText': 'selectAllEmbed',
			'click .ndn_shareCopyVideoLinkText': 'selectAllVideo'
		},

		carousel: null,
		slides: 4,
		
		initialize: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);

			this.model.on('videoPlayerInitialized', $.proxy(function() {
				this.model.get('videoPlayer').on('videoLoad', function() {
					this.carousel.goToSlide(Math.floor(this.model.get('videoPlayer').get('currentVideoIndex')/4));
				}.bind(this));
			}, this));
		},
		
		onPrevClick: function() {
			this.carousel.goToPrevSlide();
		},

		onNextClick: function() {
			this.carousel.goToNextSlide();
		},
		
		onVideoSelect: _.debounce(function(event) {			
			this.hidePlayerOverlays();

			var videoId = $(event.currentTarget).data('video-id');

			if (videoId == this.model.get('videoPlayer').getCurrentVideoId()) {
				this.model.get('videoPlayer').resetContinuousPlayCounter();
				this.model.get('videoPlayer').play();
			} else {
				Ndn_Widget_VideoPlayer_AbstractView.prototype.onVideoSelect.apply(this, arguments);
			}
			
			return false;
		}, 1000, true),

		/**
		 * Update this widget's active playlist's thumbnails
		 */
		updateSelectableVideos: function() {
			var items = this.model.get('videoPlayer').getActivePlaylistData()
					? this.model.get('videoPlayer').getActivePlaylistData().items
					: [];
			var videoIndex = 0;

			if (_.isNull(this.model.get('videoPlayer').get('playlistId'))) {
				var pl = this.model.get('videoPlayer').get('playerData').playlists;
				var additionalItems = pl[_.first(_.keys(pl))].items;
				items = items.concat(additionalItems);
				videoIndex = -1;
			}

			this.render(this.model.getTemplate('selectableVideos'), {
				'playlistItems': items,
				'videoIndex': (function() {

					return function() {

						return Math.max(videoIndex++, 0);
					};

				})()
			}, '.ndn_selectableVideos');
			
			var sliderConfig = {
				pager: false,
				controls: false,
				slideWidth: 125,
				slideMargin: 10,
				minSlides: 4,
				maxSlides: 4,
				onSliderLoad: function() {
					// TODO: Find out why IE11 never gets to this point in the code
					this.loadImagesForSlide(0);
				}.bind(this),
				onSlideBefore: function($el, oldSlide, newSlide) {
					this.loadImagesForSlide(newSlide);
				}.bind(this)
			};
			
			if (_.isNull(this.carousel)) {
				this.carousel = this.$el.find('.ndn_selectableVideos').bxSlider(sliderConfig);
				
				// Hack for IE11, clicking on the previous button and then immediately clicking on the next button 1 second later
				// TODO: Find out why the elements with the "ndn_carousel-clone" class are still showing until these buttons are 
				// clicked when running IE11
				if (Ndn_Utils_UserAgent.getIeVersion() == 11) {
					this.$el.find('.ndn_sliderPrev').trigger("click");
					setTimeout($.proxy(function() {
						this.$el.find('.ndn_sliderNext').trigger("click");
					}, this), 1000);
				}
			} else {
				this.carousel.reloadSlider(sliderConfig);
			}

			this.$el.find('.ndn_sliderThumbnailTitle p').ellipsis();
		}
	});
});
