define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoPlayer/AbstractView',
	'models/Ndn/PlayerServices',
	'models/Ndn/PlayerServices/Cache',
	'models/Ndn/Widget',
	'models/Ndn/Ads/AdStudies',
	'models/Ndn/Utils/UserAgent',

	'jquery_autoellipsis',
	'lib/jquery_plugins/jquery.simplePagination',
	'lib/jquery_plugins/jquery.bootpag'
], function(
	$,
	_,
	Ndn_Widget_VideoPlayer_AbstractView,
	Ndn_PlayerServices,
	Ndn_PlayerServices_Cache,
	Ndn_Widget,
	Ndn_Ads_AdStudies,
	Ndn_Utils_UserAgent
) {
	return Ndn_Widget_VideoPlayer_AbstractView.extend({
		events: {
			'click .ndn_selectableVideo': 'onVideoSelect',
			'click .ndn_playlistTabs li a': 'onPlaylistSelect',
			'click a.ndn_accordion-toggle': 'onMobileAccordionClick'
		},
		
		initialize: function() {
			Ndn_Widget_VideoPlayer_AbstractView.prototype.initialize.apply(this, arguments);
			
			this.model.on({
				'playlistsUpdate': $.proxy(function() {
					this.updatePlaylistTabs();
				}, this),
				'playlistSelect': $.proxy(function() {
					this.updateSelectableVideos();
				}, this)
			});
			
			this.model.on('videoPlayerInitialized', $.proxy(function() {
				var self = this,
					videoPlayer = this.model.get('videoPlayer');
					adsEnabled = videoPlayer.get('widget').get('config')['adsEnabled'];
				
				videoPlayer.on({
					'embed': function() {
						// Update the user interface with the currently loaded playlists
						if (self.model.get('playlists').length) {
							self.updatePlaylistTabs();
						}
					},
					'videoUnavailable': function() {
						self.model.set('playlists', []);
					}
				});
				
				if (adsEnabled) {
					// Handle companion ads
					videoPlayer.on({
						'adCompanion': function(event) {
							// Show the companion when we get one
							self.showCompanion(event.content);
						},
						'adEnd': function() {
							// Hide the companion again at the end of an ad
							self.hideCompanion();
						}
					});
					
					// Handle ad studies
					videoPlayer.on({
						'videoStart': function() {
							self.showAdStudy();
						},
						'videoEnd': function() {
							self.hideAdStudy();
						}
					});
				}
			}, this));
		},
		
		updatePlaylistTabs: function() {
			this.render(this.model.getTemplate('playlistTabs'), {
				'playlists': this.model.get('playlists')
			}, '.ndn_playlistTabs');
			
			// Make sure the appropriate playlist tab is selected when a new video loads into the player
			this.updatePlaylistSelectedTab(this.model.get('videoPlayer').get('playlistId'));
		},
		
		onMobileAccordionClick: function(event) {
			// Easy: slideToggle the list of playlists!
			this.$el.find('.ndn_accordion .ndn_collapse').slideToggle();
			return false;
		},
		
		onPlaylistSelect: function(event) {
			var playlistId = $(event.currentTarget).data('playlistId');
			
			this.model.selectPlaylist(playlistId);
			
			// Return false so the anchor stuff does nothing
			return false;
		},
		
		/**
		 * Handles when a new video has been selected to be loaded into the video player.
		 * Custom implementation here to make sure active playlist is updated if needed
		 * before handling the click
		 */
		onVideoSelect: _.debounce(function(event) {
			// Indicate that a video has been selected by the user to be played
			this.model.trigger('videoSelect');
			
			// Get the playlist id of the playlist of the video that was just clicked
			var playlistId = this.$el.find('.ndn_playlistTabs li.ndn_active a').data('playlistId') 
				|| this.model.get('videoPlayer').get('playlistId');
			
			// Update the video playlist, then call the parent when that's done
			var selectArgs = arguments;
			this.model.get('videoPlayer').loadPlaylist(playlistId)
			.done($.proxy(function(){
				Ndn_Widget_VideoPlayer_AbstractView.prototype.onVideoSelect.apply(this, selectArgs);
			}, this));
		}, 500, true),
		
		/**
		 * Display a companion ad with the provided content (only if an ad study is not already being displayed)
		 * @param {String} content The HTML that serves as the content of the companion ad to display
		 * @return {jQuery.Deferred} Resolves when the companion ad with the provided content is displayed or if it is detected that an ad study is already being displayed
		 */
		showCompanion: function(content) {
			return $.Deferred($.proxy(function(deferred) {
				// Only allow a companion ad to be displayed if an ad study is not already being displayed
				if (!this.model.get('isShowingAdStudy')) {
					var dom = this.getDomElements(),
						adCompanionContainer = dom.companion,
						pagesContainer = dom.thumbnailPages;

					// Indicate in the dom that a companion ad is being displayed
					pagesContainer.addClass('ndn_withCompanion');
					
					// Display the companion ad using the provided content
					this.displayCompanionAd(adCompanionContainer, content);
					adCompanionContainer.show();
					
					// Remember the correct state of whether a companion ad is being displayed
					this.model.set('isShowingCompanionAd', true);
					
					// Update the selectable videos so that the correct number of selectable videos are displayed per page
					this.updateSelectableVideos().done(function() {
						deferred.resolve();
					});
				}
				else {
					// Resolve when a companion ad should not be displayed (because an ad study is already being displayed)
					deferred.resolve();
				}
			}, this)).promise();
		},
		
		/**
		 * Hide any visible companion ad
		 */
		hideCompanion: function() {
			return $.Deferred($.proxy(function(deferred) {
				// If a companion ad is currently being displayed
				if (this.model.get('isShowingCompanionAd')) {
					var dom = this.getDomElements(),
						pagesContainer = dom.thumbnailPages,
						adCompanionContainer = dom.companion;
					
					// Indicate in the dom that a companion ad is no longer being displayed
					pagesContainer.removeClass('ndn_withCompanion');
					adCompanionContainer.html('').hide();
					
					// Remember the correct state of whether a companion ad is being displayed
					this.model.set('isShowingCompanionAd', false);
					
					// Update the selectable videos so that the correct number of selectable videos are displayed per page
					this.updateSelectableVideos().done(function() {
						deferred.resolve();
					});
				}
				else {
					// Resolve when no companion ad is being displayed to begin with
					deferred.resolve();
				}
			}, this)).promise();
		},
		
		/**
		 * Attempt to display a new ad study
		 * @return {jQuery.Deferred} Resolves when a new ad study is successfully displayed; fails when an ad study is not returned
		 */
		showAdStudy: function() {
			// console.log('Ndn_Widget_VideoPlayer_16x9View.showAdStudy();');
			
			return $.Deferred($.proxy(function(deferred) {
				var dom = this.getDomElements(),
					adStudyContainer = dom.adStudy,
					pagesContainer = dom.thumbnailPages;
				
				// Hide the ad study first and wait for the ad study response until it is shown again
				this.hideAdStudy();
				
				// Attempt to display an ad study in the provided container element with the provided DFP "iu" value
				Ndn_Ads_AdStudies.displayAdStudy(adStudyContainer.get(0), this.model.get('videoPlayer').getParams().dfpIuValue)
				.done($.proxy(function() {
					// console.log('((>>)) Showing Ad Study!!');
					
					// Indicate in the dom that an ad study is being displayed
					pagesContainer.addClass('ndn_withAdStudy');
					
					// Remember the correct state of whether an ad study is being displayed
					this.model.set('isShowingAdStudy', true);
					
					// Hide any companion ad (if one is currently being displayed), and then update the selectable videos so that the correct number of selectable videos are displayed per page
					this.hideCompanion().done($.proxy(function() {
						this.updateSelectableVideos().done(function() {
							deferred.resolve();
						});
					}, this));
				}, this))
				.fail(function() {
					// console.log('((>>)) NOT Showing Ad Study!!');
				});
			}, this)).promise();
		},
		
		/**
		 * Hide any displayed ad study
		 * @return {jQuery.Deferred} Resolves once an ad study is no longer being displayed
		 */
		hideAdStudy: function() {
			return $.Deferred($.proxy(function(deferred) {
				// If there is an ad study currently being displayed
				if (this.model.get('isShowingAdStudy')) {
					var dom = this.getDomElements(),
						pagesContainer = dom.thumbnailPages,
						adStudyContainer = dom.adStudy;
					
					// Indicate in the dom that an ad study is no longer being displayed
					pagesContainer.removeClass('ndn_withAdStudy');
					adStudyContainer.html('').hide();
					
					// Remember the correct state of whether an ad study is being displayed
					this.model.set('isShowingAdStudy', false);
					
					// Update the selectable videos so that the correct number of selectable videos are displayed per page
					this.updateSelectableVideos().done(function() {
						deferred.resolve();
					});
				}
				else {
					// Resolve when there is no ad study currently being displayed
					deferred.resolve();
				}
			}, this)).promise();
		},
		
		getDomElements: function() {
			return {
				'thumbnailPages': this.$el.find('.ndn_playlistThumbnailsPages'),
				'paginator': this.$el.find('.ndn_playlistThumbnailsPagination'),
				'companion': this.$el.find('.ndn_companion'),
				'adStudy': this.$el.find('.ndn_adStudy')
			};
		},
		
		/**
		 * Update this widget's active playlist's thumbnails
		 * @return {jQuery.Deferred} Resolves once the selectable videos HTML has been updated
		 */
		updateSelectableVideos: function() {
			return $.Deferred($.proxy(function(deferred) {
				this.model.getSelectedPlaylistData().done($.proxy(function(playlistData) {
					var elements = this.getDomElements(),
						pagesContainer = elements.thumbnailPages,
						paginationContainer = elements.paginator,
						selectedPlaylistId = playlistData ? playlistData.playlistId : null,
						thumbsPerPage = this.model.getThumbnailsPerPage(),
						htmlData = this.model.getThumbnailPagesHtmlData(selectedPlaylistId, thumbsPerPage);
					
					// Update the thumbnail pages dom element container with the appropriate HTML
					elements.thumbnailPages.html(htmlData.html);
					
					// Update the paginator appropriately
					elements.paginator
					.pagination('destroy')
					.pagination({
				        items: htmlData.thumbnailCount,
						itemsOnPage: thumbsPerPage,
				        cssStyle: 'pagination-centered',
				        onPageClick: $.proxy(function(pageNumber, event) {
							// Keep track of what the video index is of the first video on the currently selected page of thumbnails
							this.model.storeSelectedPage(pageNumber);
							
							// Show the appropriate thumbnail page and use ellipsis as needed
							var pagesContainer = this.getDomElements().thumbnailPages;
							pagesContainer.find('> .ndn_playlistThumbnailsPage').hide();
							pagesContainer.find('> .ndn_playlistThumbnailsPage[data-page-number="' + pageNumber + '"]').show();
							pagesContainer.find('.ndn_videoTitle:visible, .ndn_videoMeta:visible').ellipsis({updateOnWindowResize: true});
							
							return false;
				        }, this),
				        prevText: '&laquo;',
				        nextText: '&raquo;'
				    });
					
					// Set the appropriate current selected playlist tab and thumbnail page
					this.updatePlaylistSelectedTab(selectedPlaylistId);
					elements.paginator.pagination('selectPage', this.model.getCurrentThumbnailPage());
					
					// Ensure the "Now Playing" thumb is still highlighted
					this.updateSelectedVideo();
					
					// Show the pagination component for the pages of thumbnails only if there are multiple pages available
					elements.paginator.toggle(elements.paginator.pagination('getPagesCount') > 1);
					
					deferred.resolve();
				}, this));
			}, this)).promise();
		},
		
		/**
		 * @param {int} activePlaylistId The playlist id associated with the playlist tab that is currently active 
		 */
		updatePlaylistSelectedTab: function(activePlaylistId) {
			// Find the tab corresponding to this playlist and ensure it's active
			var playlistTabsContainer = this.$el.find('.ndn_playlistTabs');
			var $activeTab = playlistTabsContainer.find('li.ndn_active');
			if ($activeTab.children('a').data('playlistId') !== activePlaylistId) {
				$activeTab.removeClass('ndn_active');
				playlistTabsContainer.find('a[data-playlist-id=' + activePlaylistId + ']').parents('li').addClass('ndn_active');
			}
		}
	});
});
