define([
		'jquery',
		'underscore',
		'views/Ndn/Widget/VideoLauncher/AbstractSliderView',
		'models/Ndn/Utils/UserAgent',
		'models/Ndn/Widget',
		
		'jquery_autoellipsis',
		'jquery_imagesloaded',
		'jquery_bxslider'
	], function($, _, Ndn_Widget_VideoLauncher_AbstractSliderView, Ndn_Utils_UserAgent, Ndn_Widget) {
		return Ndn_Widget_VideoLauncher_AbstractSliderView.extend({
			events: {
				'mouseenter': 'onUserInteractionStart',
				'touchstart': 'onUserInteractionStart',
				'mouseleave': 'onUserInteractionEnd',
				'touchend': 'onUserInteractionEnd',
				'click .ndn_sliderContainer li a': 'onThumbnailClick',
				'click .ndn_sliderNext': 'onNextClick',
				'click .ndn_sliderPrev': 'onPrevClick',
				'change .ndn_playlistSelectContainer select': 'onSelectChange'
			},

			carousel: null,

			slides: 4,

			initialize: function(event) {
				Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initialize.apply(this, arguments);
				
				// TODO: Refactor this out:
				this.mouseoverInterval = [];
			},

			onUserInteractionStart: function() {
				this.setMouseoverInterval(setTimeout($.proxy(function() {
					this.model.set({
						'userHasInteracted': true
					});

					this.unsetMouseoverInterval();
				}, this), this.model.get('mouseoverTime')));
			},

			onUserInteractionEnd: function() {
				this.unsetMouseoverInterval();
			},

			setMouseoverInterval: function(interval) {
				var newInterval = {};

				// Associate the interval with this instances model to accomodate multiple players on one page.
				newInterval[this.model.cid] = interval;

				this.mouseoverInterval = _.extend(this.mouseoverInterval, newInterval);
			},

			unsetMouseoverInterval: function() {
				clearTimeout(this.mouseoverInterval[this.model.cid]);
				delete this.mouseoverInterval[this.model.cid];
			},
	
			/**
			 * @param {Boolean} [fadeOut] Whether the video player should fade out before transitioning to the slider; defaults to true
			 */
			onTeaserFinished: function(fadeOut) {
				fadeOut = _.isUndefined(fadeOut) ? true : fadeOut;
				
				this.updateSelectableVideos();
				this.updatePlaylistSelect();
				
				if (fadeOut) {
					this.$el.find('.ndn_videoPlayer').fadeTo(1000, 0.01).fadeOut(1000, function() {
						this.transitionToSlider();
					}.bind(this));				
				} else {
					this.transitionToSlider();
				}
			},

			transitionToSlider: function() {
				this.$el.find('.ndn_sliderPlaylistPane li:nth-child(odd)').addClass('ndn_odd');
				this.$el.find('.ndn_videoPlayerWrapper').remove();

				this.$el.find('.ndn_playlistSelectContainer').show();
				this.$el.find('.ndn_sliderControls').show();

				this.carousel.stopAuto();
				this.carousel.goToSlide(0);
				this.carousel.startAuto();
			},

			updatePlaylistSelect: function() {
				this.render(this.model.getTemplate('playlistSelect'), {
					'playlists': this.model.getPlaylistsList(),
					'single': (this.model.getPlaylistsList().length <= 1)
				}, '.ndn_playlistSelectContainer');
			},
	
			onPrevClick: function(){
				this.carousel.stopAuto();
				this.carousel.goToPrevSlide();
				this.carousel.startAuto();
			},
	
			onNextClick: function(){
				this.carousel.stopAuto();
				this.carousel.goToNextSlide();
				this.carousel.startAuto();
			},
	
			onThumbnailClick: function(event) {
				var link = event.currentTarget;
				if ($(link).attr('href') !== undefined) {
					this.openNdnWindow($(link).attr('href'));
				}
	
				event.preventDefault();
			},
	
			onSelectChange: function(event) {
				this.model.get('videoPlayer').loadPlaylist(event.target.value).done(function() {
					this.updateSelectableVideos();
				}.bind(this));
			},
			
			/**
			 * Update this widget's active playlist's thumbnails
			 */
			updateSelectableVideos: function() {
				var videos = this.model.getVideos();

				this.render(this.model.getTemplate('selectableVideos'), {
					'appUrl': Ndn_Widget.getAppUrl(),
					'urlSuffix': Ndn_Widget.getAppUrlSuffix(),
					'playlistItems': videos,
					'playlists': [this.model.get('videoPlayer').get('playlistId')],
					'siteSection': this.model.get('config')['siteSection'],
					'videoIndex': (function() {
						var videoIndex = 0;
						return function() {
							return videoIndex++;
						};
					})()
				}, '.ndn_selectableVideos');
				
				var defaultSliderConfig = {
					pager: false,
					controls: false,
					slideWidth: 300,
					auto: true,
					autoHover: true,
					pause: 7000,
					onSliderLoad: function() {
						this.loadImagesForSlide(0, '.ndn_sliderPlaylistPane');
					}.bind(this),
					onSlideBefore: function($el, oldSlide, newSlide) {
						this.loadImagesForSlide(newSlide, '.ndn_sliderPlaylistPane');
					}.bind(this)
				};

				if (_.isNull(this.carousel)) {
					this.carousel = this.$el.find('.ndn_selectableVideos').bxSlider(defaultSliderConfig);
				} else {
					if (videos.length == 1) {
						this.carousel.reloadSlider(_.extend(defaultSliderConfig, {auto: false}));
					} else {
						this.carousel.reloadSlider(defaultSliderConfig);
					}
				}

				if (videos.length == 1) {
					this.$el.find('.ndn_sliderControls').hide();
				} else {
					this.$el.find('.ndn_sliderControls').show();
				}

				this.$el.find('.ndn_sliderThumbnailOverlay p').ellipsis();
				this.$el.find('.ndn_sliderPlaylistPane li:nth-child(odd)').addClass('ndn_odd');
			}
		});
	}
);