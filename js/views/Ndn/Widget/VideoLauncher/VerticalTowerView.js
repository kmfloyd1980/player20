define([
	'jquery',
	'views/Ndn/Widget/VideoLauncher/AbstractView',
	'models/Ndn/Utils/UserAgent',
	'jquery_imagesloaded',
	'jquery_bxslider'
], function($, Ndn_Widget_VideoLauncher_AbstractView, Ndn_Utils_UserAgent) {

	return Ndn_Widget_VideoLauncher_AbstractView.extend({
		events: {
			'click .ndn_verticalTowerWrapper a': 'onThumbnailClick',
			'click .ndn_verticalTowerNavPrev': 'onPrevClick',
			'click .ndn_verticalTowerNavNext': 'onNextClick'
		},

		carousel: null,
		slides: 5,

		initialize: function() {
			this.on('render', function() {
				this.initCarousel();
			}.bind(this));
		},

		onPrevClick: function(){
			this.carousel.goToPrevSlide();
		},

		onNextClick: function(){
			this.carousel.goToNextSlide();
		},

		onThumbnailClick: function(event) {
			var link = event.currentTarget;
			if ($(link).attr('href') !== undefined) {
				this.openNdnWindow($(link).attr('href'));
			}

			event.preventDefault();
		},

		initCarousel: function() {
			this.carousel = this.$el.find('ul').bxSlider({
				controls: false,
				maxSlides: 5,
				minSlides: 5,
				mode: 'vertical',
				pager: false,
				slideMargin: 3,
				onSliderLoad: function() {
					this.loadImagesForSlide(0);
				}.bind(this),
				onSlideBefore: function($el, oldSlide, newSlide) {
					this.loadImagesForSlide(newSlide);
				}.bind(this)
			});
		}
	});
});