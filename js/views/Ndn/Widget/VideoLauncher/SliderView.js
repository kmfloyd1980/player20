define([
	'jquery',
	'underscore',
	'views/Ndn/Widget/VideoLauncher/AbstractSliderView',
	'models/Ndn/Utils/UserAgent',
	'models/Ndn/Widget',
	'jquery_imagesloaded',
	'jquery_bxslider',
	'jquery_autoellipsis'
], function(
	$,
	_,
	Ndn_Widget_VideoLauncher_AbstractSliderView,
	Ndn_Utils_UserAgent, 
	Ndn_Widget
) {
	return Ndn_Widget_VideoLauncher_AbstractSliderView.extend({
		events: {
			'mouseenter': 'onUserInteractionStart',
			'touchstart': 'onUserInteractionStart',
			'mouseleave': 'onUserInteractionEnd',
			'touchend': 'onUserInteractionEnd',
			'click .ndn_sliderContainer li a': 'onThumbnailClick',
			'click .ndn_sliderContainer .ndn_sliderControls': 'onThumbnailClick',
			'click .ndn_sliderNext': 'onNextClick',
			'click .ndn_sliderPrev': 'onPrevClick',
            'click .ndn_playlistThumbnails li a': 'onThumbnailClick'
		},

		initialize: function(event) {
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initialize.apply(this, arguments);
			
			// Apply the optimal width to the video player when it plays ads
			// this.initializeOptimalVideoPlayerWidthBehavior(); // Removed from Sprint #17
			
			// Hide the video player whenever it is waiting for its ad/content to load
			this.hideVideoPlayerWhileLoading();

			// Initialize the maintenance of the top container's aspect ratio
			this.initializeAspectRatioOfTopContainer();
			
			var self = this;
			this.once('render', function(e) {
				this.model.get('videoPlayer').on({
					'videoPlaylistLoad': function() {
						self.synchronizeInterface();
					}
				});
			});
		},
		
		/**
		 * Updates the interface's thumbnails/carousel
		 */
		synchronizeInterface: function() {
			this.renderThumbnailPlaceholders();
            if (!this.model.getThumbnailsEnabled()) {
            	this.$el.find('.ndn_playlistThumbnails').hide();
            }
            else {
            	this.renderThumbnails();
            }

            this.updateSelectableVideos();
            this.showCarousel();
		},
		
		/**
		 * @param {Boolean} [fadeOut] Whether the video player should fade out before transitioning to the slider; defaults to true
		 */
		onTeaserFinished: function(fadeOut) {
			fadeOut = _.isUndefined(fadeOut) ? true : fadeOut;
			
			if (fadeOut) {
				this.$el.find('.ndn_videoPlayer')
				.fadeTo(1000, 0.01)
				.fadeOut(1000, $.proxy(function() {
					this.transitionToSlider();
				}, this));
			} else {
				this.transitionToSlider();
			}
			
			// Indicate that the period of time this product uses its video player has concluded
			this.model.set('hasConcludedVideoPlayer', true);
		},
		
		/**
		 * Display the large thumbnail carousel (not the small thumbnail carousel that may be included at the bottom)
		 */
		showCarousel: function() {
			// Only show the showcase carousel if it is already enabled/initialized
			if (this.model.isShowcaseCarouselEnabled()) {
				this.$el.find('.ndn_sliderControls').toggle(this.model.getCurrentPlaylistLength() > 1);
				
				// Make sure all the <li> elements are the correct width (as they may have been resized due to the expansion of the video player's width briefly)
				this.$el.find('.ndn_selectableVideos > li').css('width', this.model.getContainerElement().width() + 'px');
				
				this.model.get('carousel').stopAuto();
				this.model.get('carousel').goToSlide(0);
				this.model.get('carousel').startAuto();
				
				this.$el.find('.ndn_sliderThumbnailOverlay p').ellipsis();
				this.$el.find('.ndn_sliderVideoTitle').ellipsis();
			}
		},
		
		/**
		 * @param {Boolean} showSliderControls Whether the slider controls should be displayed once the slider is transitioned into
		 */
		transitionToSlider: function() {
			// Once the showcase carousel is forcibly initialized, transition to and display the showcase carousel
			Ndn_Widget_VideoLauncher_AbstractSliderView.prototype.initializeShowcaseCarousel.apply(this, [true]).done($.proxy(function() {
	            this.$el.find('.ndn_videoPlayerWrapper').hide();
	            
	            try {
				    this.$el.find('.ndn_videoPlayerWrapper').detach();
	            }
	            catch (e) {}
	            
	            this.showCarousel();
			}, this));
		},

		onPrevClick: function(){
			this.model.get('carousel').stopAuto();
			this.model.get('carousel').goToPrevSlide();
			this.model.get('carousel').startAuto();

			return false;
		},

		onNextClick: function(){
			this.model.get('carousel').stopAuto();
			this.model.get('carousel').goToNextSlide();
			this.model.get('carousel').startAuto();

			return false;
		},

		onUserInteractionStart: function() {
			this.model.set({
				'userHasInteracted': true
			});
		},

		setCurrentVideoThumbnail: function(videoId) {
			this.$el.find('.ndn_playlistThumbnails [data-video-id="' + videoId + '"] .ndn_currentVideoThumbnail').fadeIn(400);
		},

		clearCurrentVideoThumbnail: function() {
			this.$el.find('.ndn_currentVideoThumbnail').fadeOut(400);
		},

		goToThumbnailSlide: function(videoId) {
			var index = this.$el.find('.ndn_playlistThumbnails [data-video-id="' + videoId + '"]').data('videoIndex');	
			this.model.get('thumbnailCarousel').goToSlide(Math.floor(index/3));
		},

		onThumbnailClick: function(event) {
			var destination = '';
			if ($(event.currentTarget).attr('href') !== undefined) {
				destination = $(event.currentTarget).attr('href');
			} else {
				var link = $(_.first(this.$el.find('[data-video-index="'+this.model.get('carousel').getCurrentSlide()+'"]')));
				destination = link.find('a').attr('href');
			}

			this.openNdnWindow(destination);
			event.preventDefault();
            return false;
		}
	});
});
