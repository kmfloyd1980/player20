define([
	'jquery',
	'views/Ndn/Widget/VideoLauncher/AbstractView',
	'models/Ndn/Utils/UserAgent',
	
	'jquery_imagesloaded',
	'jquery_bxslider',
	'jquery_autoellipsis'
], function($, Ndn_AbstractView, Ndn_Utils_UserAgent) {
	return Ndn_AbstractView.extend({
		/**
		 * TODO: Refactor this property out.
		 */
		slides: 4,
		
		events: {
			'click .ndn_sliderItems li a': 'onThumbnailClick',
			'click .ndn_sliderNext': 'onNextClick',
			'click .ndn_sliderPrev': 'onPrevClick'
		},
		
		initialize: function() {
			Ndn_AbstractView.prototype.initialize.apply(this, arguments);
			
			this.on('render', $.proxy(function(event) {
				// TODO: Comment why this class is being added
				this.$el.addClass(event.params.launcherSize);
				
				// Determine the maximum number of slides to use for this product's thumbnail carousel
				this.model.set('carouselMaxSlides', event.params.launcherSize == "ndn_horizontalSmall" ? 3 : 4);
				
				// TODO: Refactor this out.
				// For backwards-compatability:
				this.slides = this.model.get('carouselMaxSlides');
				
				this.model.set('carousel', this.$el.find('.ndn_sliderItems').bxSlider({
					auto: true,
					autoHover: true,
					pause: 7000,
					pager: false,
					controls: false,
					slideWidth: 120,
					slideMargin: 6,
					minSlides: 3,
					maxSlides: this.model.get('carouselMaxSlides'),
					onSliderLoad: function() {
						this.loadImagesForSlide(0);
					}.bind(this),
					onSlideBefore: function($el, oldSlide, newSlide) {
						this.loadImagesForSlide(newSlide);
					}.bind(this)
				}));
				
				this.$el.find('p').ellipsis();
			}, this));
		},
		
		onPrevClick: function() {
			var carousel = this.model.get('carousel');
			
			carousel.stopAuto();
			carousel.goToPrevSlide();
			carousel.startAuto();
		},

		onNextClick: function() {
			var carousel = this.model.get('carousel');
			
			carousel.stopAuto();
			carousel.goToNextSlide();
			carousel.startAuto();
		},

		onThumbnailClick: function(event) {
			var link = event.currentTarget;
			if ($(link).attr('href') !== undefined) {
				this.openNdnWindow($(link).attr('href'));
			}

			event.preventDefault();
		}
	});
});