define([
	'jquery',
	'underscore',
	'models/Ndn/App',
	'models/Ndn/Utils/UserAgent',
	'models/Ndn/Utils/UrlParser',
	'models/Ndn/Widget',
	'views/Ndn/Widget/VideoLauncher/AbstractView',
	'models/Ndn/Utils/AspectRatio',
	
	'jquery_bxslider'
], function(
	$,
	_,
	Ndn_App,
	Ndn_Utils_UserAgent,
	Ndn_Utils_UrlParser,
	Ndn_Widget,
	Ndn_Widget_VideoLauncher_AbstractView,
	Ndn_Utils_AspectRatio
) {
	return Ndn_Widget_VideoLauncher_AbstractView.extend({
		/**
		 * Initialize the behavior for this product's video player so that it will expand to the optimal width when showing its ad
		 */
		initializeOptimalVideoPlayerWidthBehavior: function() {
			// Once this product's video player has been initialized
			this.model.on('videoPlayerInitialized', $.proxy(function() {
				// Only if this product's container does NOT have a parent container that hides its overflow (disregarding the <body> and <html> elements),
				// if this product is not embedded using iframe embed code, and if the current device is not mobile
				if (!this.containedWithinOverflowXHiddenContainer() && !Ndn_App.isEmbeddedByIframe() && !Ndn_Utils_UserAgent.isMobile()) {
					var widget = this.model,
						videoPlayer = widget.get('videoPlayer'),
						widgetView = widget.get('view');
					
					// Expand the video player to its optimal width when playing an ad
					videoPlayer.on({
						'loadAttempt': function() {
							widgetView.ensureOptimalVideoPlayerWidth();
						},
						'adStart': function() {
							// Ensure the video player wrapper element is sized to at least the optimal width
							widgetView.ensureOptimalVideoPlayerWrapperWidth();
						},
						'videoStart videoUnload': function() {
							// Remove any changes to the video player's width
							widgetView.restoreOriginalVideoPlayerWidth();
							widgetView.restoreOriginalVideoPlayerWrapperWidth();
						}
					});
				}
			}, this));
		},
        
        initializeAspectRatioOfTopContainer: function() {
            var widgetView = this,
                widgetModel = this.model;
            
            widgetModel.on('containerInitialized', function() {
                Ndn_Utils_AspectRatio.maintain(widgetView.$el.find('.ndn_sliderTopContainer'), '6:5', function(element) {
                    // If maintaining the aspect ratio is enabled or if the height has not yet been calculated
                    return widgetModel.isAspectRatioOfTopContainerEnabled() || !element.css('height');
                });
            });
        },
		
		hideVideoPlayerWhileLoading: function() {
			// If the current device being used is not a mobile device
			if (!Ndn_Utils_UserAgent.isMobile()) {
				// Once this product's video player has been initialized
				this.model.on('videoPlayerInitialized', $.proxy(function() {
					var widget = this.model,
						videoPlayer = widget.get('videoPlayer'),
						widgetView = widget.get('view');
					
					videoPlayer.on({
						'videoLoad': function() {
							widgetView.setVideoPlayerOnScreen(false);
						},
						'adStart videoUnload': function() {
							widgetView.setVideoPlayerOnScreen(true);
						},
						'videoStart': function() {
						    // Only if this is not currently doing the rotating RMM functionality
						    if (!videoPlayer.isRotatingRmm()) {
	                            widgetView.setVideoPlayerOnScreen(true);
						    }
						}
					});
				}, this));
			}
		},
		
		/**
		 * @return {Boolean} Whether the product's container element is contained within elements that have either "overflow-x" or "overflow" CSS properties set to hidden
		 */
		containedWithinOverflowXHiddenContainer: function() {
			var productContainer = this.model.getContainerElement(),
				result = false;
			
			var widget = this.model;
			
			$($.merge($.merge([], productContainer), productContainer.parents())).each(function() {
				// If this parent container is either the <html> or <body> tag, stop searching through the parent container elements
				if (_.contains(['html', 'body'], $(this).prop('tagName').toLowerCase())) return false;
				
				if (_.contains([$(this).css('overflow'), $(this).css('overflow-x')], 'hidden')) {
					result = true;
				} 
			});
			
			return result;
		},
		
		/**
		 * TODO: Please document why this method is needed.
		 * 
		 * Renders placeholders for the bottom thumbnails carousel
		 */
       	renderThumbnailPlaceholders: function() {
			var videos = [{},{},{}],
				index = 0;

			this.render(this.model.getTemplate('thumbnails'), {
				'playlistVideos': videos,
				'videoIndex': function() {
					return index++;
				}
			}, '.ndn_playlistThumbnails');

			this.$el.find('.ndn_playlistThumbnails img').hide();
			this.$el.find('.ndn_playlistThumbnails h4').css('text-indent', '-9999px');
		},
		
		/**
		 * Renders the bottom thumbnails carousel using the appropriate video data
		 */
		renderThumbnails: function() {
            this.model.getVideos().done($.proxy(function(videos) {
                var index = 0;

                this.render(this.model.getTemplate('thumbnails'), {
                    'playlistVideos': videos,
                    'videoIndex': function() {
                        return index++;
                    }
                }, '.ndn_playlistThumbnails');

                this.model.set('thumbnailCarousel', this.$el.find('.ndn_playlistThumbnailsList').bxSlider({
                    pager: false,
                    controls: false,
                    maxSlides: 3,
                    minSlides: 3,
                    slideWidth: 114,
                    slideMargin: 4,
                    auto: false,
                    onSliderLoad: function() {
                        this.loadImagesForSlide(0);
                        this.centerThumbnails();
                    }.bind(this),
                    onSlideBefore: function($el, oldSlide, newSlide) {
                        this.loadImagesForSlide(newSlide);
                    }.bind(this)
                }));
                
                this.$el.find('.ndn_playlistThumbnails .ndn_titleOverlay p').ellipsis({live: true});
            }, this));
		},
		
		/**
		 * TODO: Please document what this function is needed for and why.
		 */
		centerThumbnails: function() {
			this.$el.find('.ndn_playlistThumbnailsList li, .ndn_sliderItems li').each(function() {
				var $img = $(this).find('img:not(.ndn_providerLogo)'),
					newMargin = $img.width() - $img.closest('li').width();

				newMargin = (0 - Math.max(newMargin, 0)) / 2;
				$img.css('margin-left', newMargin + 'px');
			});
		},
		
		/**
		 * @param {Boolean} [force] Whether the showcase carousel (the top/main one) should be forcibly enabled; defaults to false
		 * @return {jQuery.Deferred} Resolves once the showcase carousel has been initialized or once it has been determined that the showcase carousel should not be initialized
		 */
		initializeShowcaseCarousel: function(force) {
			if (typeof force == 'undefined') force = false;
			
			return $.Deferred($.proxy(function(deferred) {
				var onDesktopMacFirefox = Ndn_Utils_UserAgent.isFirefox() && navigator.userAgent.match(/\bMacintosh\b/i);
				
				if (!onDesktopMacFirefox || force) {
		            if (!this.model.isShowcaseCarouselEnabled()) {
		                this.model.set('carousel', this.$el.find('.ndn_selectableVideos').bxSlider({
		                    pager: false,
		                    controls: false,
		                    slideWidth: 350,
		                    auto: true,
		                    autoHover: true,
		                    pause: 7000,
		                    onSliderLoad: $.proxy(function() {
		            			this.loadImagesForSlide(0);
		                        this.centerThumbnails();
		                        this.$el.find('.ndn_sliderItems .ndn_sliderVideoTitle').ellipsis();
		                        
		                        // Indicate that the carousel is in fact enabled, which CSS uses to determine whether the carousel "Previous" and "Next" buttons should be visible
		                        this.$el.find('.ndn_sliderContainer').addClass('ndn_showcaseCarouselEnabled');
		                        
		                        deferred.resolve();
		                    }, this),
		                    onSlideBefore: $.proxy(function($el, oldSlide, newSlide) {
		                    	this.loadImagesForSlide(newSlide);
		                        this.centerThumbnails();
		                        
		                        // TODO: Change this behavior so that it will do this even when the showcase carousel is being displayed before an RMM loads
		                        // by replacing this.model.get('hasConcludedVideoPlayer') with a true indicator of whether or not the showcase carousel is
		                        // being displayed. Keep in mind the scenario where Desktop Mac/FF does not have the carousel enabled, but the first showcase
		                        // thumbnail/image is still visible.
		                        if (this.model.get('hasConcludedVideoPlayer') && this.model.getThumbnailsEnabled()) {
		                            this.clearCurrentVideoThumbnail();
		                            this.setCurrentVideoThumbnail($el.data('videoId'));
		                            
		                            if (newSlide % 3 == 0) {
		                                if (newSlide > oldSlide || (newSlide == 0 && oldSlide == this.model.get('thumbnailCarousel').getSlideCount() - 1)) {
		                                    this.model.get('thumbnailCarousel').goToNextSlide();	
		                                }					
		                            } else if ((newSlide + 1) % 3 == 0) {
		                                if (newSlide < oldSlide || (oldSlide == 0 && newSlide == this.model.get('thumbnailCarousel').getSlideCount() - 1)) {
		                                    this.model.get('thumbnailCarousel').goToPrevSlide();	
		                                }
		                            }
		                        }
		                    }, this)
		                }));
		            }
		            else {
		                this.model.get('carousel').reloadSlider();
		                this.loadImagesForSlide(0);
		                
                        deferred.resolve();
		            }
				}
				else {
                    deferred.resolve();
				}
			}, this)).promise();
		},
		
		/**
		 * Update this widget's active playlist's thumbnails
		 * @return {jQuery.Deferred} Resolves after the selectable videos have been updated
		 */
        updateSelectableVideos: function() {        	
        	return $.Deferred($.proxy(function(deferred) {
    			this.model.getParentWidget().getSettings().done($.proxy(function() {
    				this.model.getVideos().done($.proxy(function(videos) {
                        this.render(this.model.getTemplate('selectableVideos'), {
                            'appUrl': Ndn_Widget.getAppUrl(),
                            'urlSuffix': Ndn_Widget.getAppUrlSuffix(),
                            'playlistItems': videos,
                            'videoIndex': (function() {
                                var videoIndex = 0;
                                return function() {
                                    return videoIndex++;
                                };
                            })()
                        }, '.ndn_selectableVideos');
                        
                        this.initializeShowcaseCarousel().done($.proxy(function() {
                        	// Make sure all the <li> elements are the correct width (as they may have been resized due to the expansion of the video player's width briefly)
            				this.$el.find('.ndn_selectableVideos > li').css('width', this.model.getContainerElement().width() + 'px');
            				
            				// Make sure that at least the first image is loaded for the showcase carousel
            				this.loadImagesForSlide(0);
                        	
                            deferred.resolve();
                        }, this));
                    }, this));
    			}, this));
        	}, this)).promise().done($.proxy(function() {
        		this.model.trigger('updatedSelectabledVideos');
        	}, this));
		},
		
		/**
		 * @param {Boolean} toggle Whether the video player's wrapper element should be displayed on the screen
		 */
		setVideoPlayerOnScreen: function(toggle) {
			this.$el.find('.ndn_videoPlayerWrapper').toggleClass('ndn_videoPlayerWrapper_loading', !toggle);
		},
		
		/**
		 * Ensure the video player's wrapper container is at least the optimal width specified by this product's model
		 * @return {jQuery.Deferred} Resolves once the video player's wrapper container has been expanded to at least its optimal width
		 */
		ensureOptimalVideoPlayerWrapperWidth: function() {
			var optimalWidth = this.model.get('optimalWidth');
			
			return $.Deferred($.proxy(function(deferred) {
				var videoPlayerContainer = this.$el.find('.ndn_responsiveContainer');
				
				if (videoPlayerContainer.width() < optimalWidth) {
					var addedWidth = optimalWidth - videoPlayerContainer.width();
					
					this.model.set('addedVideoPlayerWidth', addedWidth);
					
					// Expand the appropriate container to the optimal width
					videoPlayerContainer.animate({
						width: optimalWidth + 'px',
						left: '-=' + addedWidth
					}, 200, function() {
						deferred.resolve();
					});
				}
				else {
					deferred.resolve();
				}
			}, this)).promise();
		},
		
		/**
		 * @param {Boolean} [animate] Whether to animate the transition to restore the original video player's width; defaults to true
		 */
		restoreOriginalVideoPlayerWrapperWidth: function(animate) {
			if (typeof animate == 'undefined') animate = true;
			
			return $.Deferred($.proxy(function(deferred) {
				this.restoreOriginalVideoPlayerWidth();
				
				var widget = this.model,
					addedWidth = widget.get('addedVideoPlayerWidth'),
					videoPlayerContainer = this.$el.find('.ndn_responsiveContainer'),
					targetCss = {
						width: '-=' + addedWidth,
						left: 0
					};
				
				// Set the added width to 0 now that we have the added width assigned to a variable, which is about to get subtracted
				widget.set('addedVideoPlayerWidth', 0);
				
				if (animate) {
					videoPlayerContainer.animate(targetCss, 500, function() {
						deferred.resolve();
					});
				}
				else {
					videoPlayerContainer.css(targetCss);
					deferred.resolve();
				}
			}, this)).promise();
		},
		
		/**
		 * Modifies the video player's immediate container element (not the wrapper container element that contains this element) so that it is set to
		 * at least its optimal width. The width of the video player's container element is what is used to send its width in its ad call request.
		 */
		ensureOptimalVideoPlayerWidth: function() {
			this.model.toggleAspectRatioOfTopContainer(false);
			
			var videoPlayerElement = this.model.get('videoPlayer').getContainerElement(),
				optimalWidth = this.model.get('optimalWidth');
			
			if (videoPlayerElement.width() < optimalWidth) {
				videoPlayerElement.css('width', optimalWidth + 'px');
			}
		},
		
		/**
		 * Restores the video player's immediate container element (not the wrapper container element that contains this element) so that it is restored
		 * to its original width by removing its "style" attribute
		 */
		restoreOriginalVideoPlayerWidth: function() {
			this.model.get('videoPlayer').getContainerElement().css('width', '');
		}
    });
});
