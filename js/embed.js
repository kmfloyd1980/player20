var _nw2e = _nw2e || [];

(function() {
	// If "console" is not defined, make sure any calls to console.log() in the future do not break our code in browsers that do not support it
	if (typeof console == 'undefined') console = {log: function() {}};
	
	// If the _nw2e.appUrl has not been initialized yet, initialize it
	_nw2e.appUrl = _nw2e.appUrl || '';

	// Keep reference to this require.config method
	_nw2e.requireConfig = ((typeof Ndn_Require != 'undefined' && Ndn_Require.require) || require).config;

	// Create _nw2e.onReady() method that allows for waiting on the ready event for non-blocking async JS
	//
	// Since SWFObject library waits for the window's "onload" event instead of the DOM ready event before it does anything in the Firefox browser,
	// wait until the window's "onload" event for Firefox before making the ajax call to fetch analytics IDs from the server; otherwise, only wait 
	// for the DOM ready event to fire for all the other browsers that are working correctly in association with SWFObject.
	(function() {
		/**
		 * @param {Function} [closure] The closure to execute once the page is ready to execute any of the NDN Player Suite JS. If no 
		 * parameters are provided, the "ready" state is triggered, which executes any queued closures that were passed to this method
		 */
		_nw2e.onReady = _nw2e.onReady || (function() {
			var closures = [],
				isReady = false;
			
			return function(closure) {
				var closureIterator;
				
				if (typeof closure != 'undefined') {
					if (isReady) {
						while (closureIterator = closures.shift()) {
							closureIterator();
						}
						
						closure();
					}
					else {
						closures.push(closure);
					}
				}
				else {
					while (closureIterator = closures.shift()) {
						closureIterator();
					}
					
					isReady = true;
				}
			};
		})();
	})();
	
	require.config({
		baseUrl: _nw2e.appUrl + '/js',
		// waitSeconds: 0, // Do not timeout for requiring dependencies
	    // optimizeAllPluginResources: true,
		
		paths: {
			amp_lib: 'lib/amp.premier/amp.premier',
			jquery: 'lib/jquery',
			underscore: 'lib/underscore',
			backbone: 'lib/backbone',
			mustache: 'lib/mustache',
			toolbox: 'lib/toolbox',	
			bootstrap_tabs: 'lib/bootstrap_tabs',
			jquery_bxslider: 'lib/jquery_plugins/jquery.bxslider',
			jquery_imagesloaded: 'lib/jquery_plugins/jquery.imagesLoaded.min',
			jquery_autoellipsis: 'lib/jquery_plugins/jquery.autoellipsis',
			jquery_bootpag: 'lib/jquery_plugins/jquery.bootpag',
			jquery_simplePagination: 'lib/jquery_plugins/jquery.simplePagination',
			jquery_resize: 'lib/jquery_plugins/jquery.ba-resize',
			jquery_responsive_containers: 'lib/jquery_plugins/jquery.responsive-containers',
			jquery_scrollTo: 'lib/jquery_plugins/jquery.scrollTo.min',
			respond_polyfill: 'lib/respond',
			text: 'lib/require_plugins/text'
		}
	});
})();

require([
	'jquery',
	'underscore',
	'models/Ndn/App',
	'models/Ndn/Utils/UserAgent',
	
	'text'
], function(
	$,
	_,
	Ndn_App,
	Ndn_Utils_UserAgent
) {
	_nw2e.onReady(function() {		
		Ndn_App.embedWidgetsFromContainersOnPage();
	});
	
	// For non-Firefox browser, don't wait on the iframe's "onload" event since it seems to work without this workaround
	if (Ndn_Utils_UserAgent.getBrowser().toLowerCase() != 'firefox') {
		// Trigger that any queued functions are ready to be executed once the DOM is ready
		$(function() {
			_nw2e.onReady();
		});
	}
	else {
		// On domReady load in the iframe. This fixes the firefox bug (until a new one arises) :(
		$(function(){
			//Create the iframe needed to trigger execution of closures passed to the _nw2e.onReady() method
			(function() {
				var iframe = document.createElement('iframe');
				(iframe.frameElement || iframe).style.cssText = "width: 0; height: 0; border: 0";
				iframe.src = "javascript:false";
			
				var where = document.getElementsByTagName('script')[0];
				where.parentNode.insertBefore(iframe, where);
			
				var doc = iframe.contentWindow.document;
				doc.open().write('<body onload="parent.window._nw2e.onReady();">');
				doc.close();
			}());
		});
		
		// Trigger the "ready" state of the player once the window "onload" event is thrown
		// This is commented out for now in case there is an edge case where this is needed for our player to load. 
		// TODO: Further test this and see if it really can be removed. 
		
		/**/
		 $(window).load(function() {
			_nw2e.onReady();
		});
		/**/
	}
});

