var _ndnq = _ndnq || [];
_ndnq.buildDetails = _ndnq.buildDetails || {record: function() {}};
_ndnq.buildDetails.record("<%= build.environment %>", "<%= build.version %>", {
	compress: "<%= build.isCompressingBuild %>",
	optimize: "<%= build.optimizeMethod %>",
	version: "<%= build.playerSuiteVersion %>"
});