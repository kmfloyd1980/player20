# NDN Player 2.0

***

## Installation
* Install *node* and *npm*
* Install grunt-cli globally

  * sudo npm -g install grunt-cli

* Install player2.0 package 

  * `npm install`

* Run default grunt task

  * `grunt`

## Build generated files
There are certain files that are dynamically generated at build time (ie. landing-page.html).
Running the default grunt task will create these files.
