//  Custom function for videogallery page
//  updated: apr - '11
//  videoGallery - Singleton
//  JQuery framework
//

infuse("delayload");
infuse.require("/hive/javascripts/video/html5VideoTracking.js");

(function initializeSliderPVPGalleryExtensions() {
  i$('slider', 'timedscroll');
  i$.ns('i$.plugins.slider.handlers.slider_slidestart').pvpgalleryslider = function() {
    this.findByRole('pvpexposable').removeState('pvpexposed');
  };
  i$.ns('i$.plugins.slider.handlers.timedscroll').pvpgalleryslider = function() {
    
    var scrollTop = window.scrollY || document.documentElement.scrollTop || document.body.scrollTop,
        scrollBottom = scrollTop + (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight),
        elementTopOffset = this.offset().top,
        elementBottomOffset = elementTopOffset + this.outerHeight();
        
    if (scrollBottom > elementBottomOffset) {
      this
        .findByState('active')
        .findByRole('pvpexposable')
        .notByState('pvpexposed')
        .first()
        .loadByHandler()
        .addState('pvpexposed');
    }
  };
})();

jQuery(document).ready(function() {
  jQuery(window).scroll(function () { 
    setThumbnailHover();
  });
});

var videoGallery = (function() {
  // private
	
	return { //public

	  convertMiliTime: function(number) {
	    // converts miliseconds to min : sec
  		number = Math.abs( number/1000 );
  		var val = new Array( 5 );
  		val[ 0 ] = Math.floor( number / 86400 / 7 );//weeks  
  		val[ 1 ] = Math.floor( number / 86400 % 7 );//days  
  		val[ 2 ] = Math.floor( number / 3600 % 24 );//hours  
  		val[ 3 ] = Math.floor( number / 60 % 60 );//mins  
  		val[ 4 ] = Math.floor( number % 60 );//asecs  
  		var stopage = false;
  		var cutIndex = -1;
  		for (var i = 0; i < val.length; i++) {
  			if ( val[ i ] < 10 ) {
  				val[ i ] = "0" + val[ i ];
  			}
  			if ( val[ i ] == "00" && i < ( val.length - 2 ) && !stopage ) {
  				cutIndex = i;
  			} else {
  				stopage = true;
  			}
  		}
  		val.splice( 0, cutIndex + 1 );
  		return val.join(":");
    },
    getUpNext: function(type,id,caller) {
      jQuery.ajax({
        url: "/get-video-up-next.html?type=" + type + "&id=" + id,
        error: function(data) {
          caller.removeLoader("up-next-container");
          var upNext = jQuery('#up-next-container');
          upNext.find('.play-order-data').html("<span>There was an issue loading the associated playlist.</span>");
        },
        success: function(data) {
          caller.setLoader("up-next-container");
          var upNext = jQuery('#up-next-container');
          upNext.find('.play-order-data').html(data);
          
          if (isEmbedded) return false;
          
          window.updateUpNext();
        }
      });
	  },
	  setVideogallery: function (videoObj,adURl) {
	    this.getUpNext(this.getPlaylistType(),this.getPlaylistId(),this);
	    return 0;
	  },
	  loadSlider: function(type,id) {
	    this.getUpNext(this.getPlaylistType(),this.getPlaylistId(),this);
	    return 0;
	  },
    setLoader: function(id) {
      jQuery('#'+id).toggleClass('galleryLoader');
      return 0;
    },
    removeLoader: function(id) {
      jQuery('#'+id).removeClass('galleryLoader');
      return 0;
    },
    alignThumbnails: function() {
      var count = 0;
      var heightValues = [];
      var staticHeight = 300; 
      var videoCategoryList = jQuery('.gallery-item-column');
      
      videoCategoryList.each(function(index, item) {
        var height = jQuery(item).height();
        if (height > staticHeight) { staticHeight = height; }
        if (index % 4 == 3) {
          heightValues.push(staticHeight);
          staticHeight = 300;
        }
      });
      
      videoCategoryList.each(function(index, item) {
        jQuery(item).height(heightValues[count]);
        if (index % 4 == 3) { count++; }
      });
    },
    getPound: function() {
       return document.location.toString().split('#');
  	},
  	isPound: function() {
       var loc = document.location.toString().split('#');
       var valid = false;
       //checks for metrics values from navigation before returning true or false
       var exp = /&lid=(\w*)&lpos=(\w*)/;
       if ((loc.length > 1) && !loc[1].match(exp)) { valid = true; }
       return valid;
  	},
  	getPlaylistType: function(){
  	  if(this.isPound()){
  	    var type = this.getPound();
  	    var t = type[1].split('-')[0];
  	    if (t === "") {
  	      return "gl";
  	    }
  	    return type[1].split('-')[0];
  	  }

  	  return "gl"; 
  	},
  	getPlaylistId: function(){
  	  if(this.isPound()){ 
  	    var id = this.getPound();
  	    
  	    if (id[1] === "") {
  	      return 0;
  	    }
  	    return id[1].split('-')[1];
  	  }

  	  return 0; 
  	},
  	
  	startSearch: function(query) {
  	  jQuery('#search-slider .gallery-slider-item-hold').html('<div style="height: 400px;"></div>');
  	  
  	  var reference = this;

    	if(jQuery('#search-holder').hasClass('hide')) {  
    	  jQuery('#search-holder').removeClass('hide');
    	  jQuery('#category-holder').addClass('hide');
    	  jQuery('#featured-videos-title').addClass('hide');
      } else {
         jQuery('.search-video-results .gallery-slider-item-hold').html("<div style='height: 400px;' ></div>");
         this.setLoader("search_slider_view");
         i$.unregister('slider', document.getElementById('search-data-slider'));
      }

      jQuery.ajax({
        url: "/get-video-gallery-search.xml?query=" + query,
        success: function(data) {
          reference.removeLoader("search_slider_view");
          var slider = jQuery('#search-slider');
          slider.find('.gallery-slider-item-hold').html(data);
          /*if (slider.find('[data-role~=slider_group]').length < 2) {
              i$.element.addRole(slider.find('[data-role~=slider_next]'), 'slider_edge');
          } else {
            i$.element.removeRole(slider.find('[data-role~=slider_next]'), 'slider_edge');
          }*/
          setThumbnailHover();
        },
        error: function(data) {
          reference.removeLoader("search_slider_view");
        }
      });
    },
    hideSearchResults: function() { 
      jQuery('#search-holder').addClass('hide');
      jQuery('#category-holder').removeClass('hide');
      jQuery('#featured-videos-title').removeClass('hide');
    	jQuery('#search-video-results .gallery-slider-item-hold').html("<div style='height: 400px;' ></div>");
      this.setLoader("search_slider_view");
      i$.unregister('slider', document.getElementById('search-data-slider'));
    }
	};
})();


//needs to eb global bc auto event call from Brightcove 

var vastInlineAd = false;
var currentListItem;
//var checkEvent = true;
//var adDuration = 0;

function onTemplateLoaded(experienceID) { 
  jQuery('#gallery-video-email').click(function(e) {
    onEmailClick();
  });
  
  player = brightcove.getExperience(experienceID);

  // Get a reference to the Ad Module API
  adModule = player.getModule(APIModules.ADVERTISING);

  // Turn off inital bandwidth detection to try to speed up load of player.
   video    = player.getModule(APIModules.VIDEO_PLAYER);
   content  = player.getModule(APIModules.CONTENT);
   exp      = player.getModule(APIModules.EXPERIENCE);
   menu     = player.getModule(APIModules.MENU);
   
   video.enableInitialBandwidthDetection(true);
   video.setDefaultBufferTime(10);
   video.addEventListener(BCMediaEvent.COMPLETE, onVideoComplete);
   
   //Enabled External Ads
   adModule.enableExternalAds(false);
   
   // Add a callback function for the external Ads
   adModule.addEventListener(BCAdvertisingEvent.AD_START, onAdStart);
   //adModule.addEventListener(BCAdvertisingEvent.AD_PROGRESS, onAdProgress);
   //adModule.addEventListener(BCAdvertisingEvent.AD_COMPLETE, onAdComplete);
}

function onAdProgress(evt) {
  var p = Math.round(evt.position);
  var c = (adDuration - p).toString();
  
  if (c === "0") {
    jQuery('#countdown-holder').remove();
    return false;
  } 
  
  if (checkEvent) {
    jQuery('<div id="countdown-holder">ADVERTISEMENT: THE VIDEO WILL BEGIN IN <span id="countdown-time">' + c + ' </span> SECONDS.</div>').insertBefore('#video-gallery-player-canvas');
    checkEvent = false;
  } else {
   jQuery('#countdown-time').html(c);
  }
}

function onAdComplete(evt) {
  jQuery('#countdown-holder').remove();
}

function loadCube(evt) {
  var adUrl = jQuery("#premiumVideo-player-container").attr("cubead"); 
  adUrl = adUrl.replace(/pfadx/, "adi");  
	var a = adUrl.split(';');
	if (a.length > 0 && !vastInlineAd) {
	   jQuery('#companion_Ad').html('<iframe src="' + adUrl + '" id="ifr_companion" width="300" height="250" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>' + '</iframe>');
	} 
}

function onVideoComplete(evt) {
  var videoList = new Array();
  var videoIDList = new Array();
  var videoListLength;
  var i = 0;
  var index = 1;
  var locID = document.location.href.split("videogallery/")[1].split("/")[0];
  
  jQuery('.play-order-data .item').each (function (index, value) { 
    var currentRef = value.href.split("videogallery/")[1].split("/")[0];
    videoList.push(value.href);
    videoIDList.push(currentRef);
  });
  
  videoListLength = videoList.length;
  if (locID === "" && (videoListLength > 1)) {
    locID = videoIDList[0];
  }
  
  if (locID !== "") {
    for (i; i < videoListLength; i++) {
      var linkID = videoList[i].split("/")[4];

      if (linkID === locID) {
        if (i < (videoListLength - 1)) {
          index = i + 1;
        } else {
          index = 0;
        }
        break;
      } else {
        if (videoListLength > 1) {
          index = 1;
        } else {
          index = 0;
        }
      }
    }
  }
  if (videoListLength > 1) {
    if (document.location.href.indexOf(".trb") !== -1 || document.location.href.indexOf(".tribdev.com") !== -1) {
      var currentPath = videoList[index].split("videogallery")[1];
      currentListItem = document.location.protocol + "//" + document.location.host + "/videogallery" + currentPath; 
    } else {
      currentListItem = videoList[index]; 
    }
    
    var redirect = setTimeout("gotoNextVideo()", 250);
    
    setInterval( function () {
      //count down
    }, 1000);
  }
} 

function updateUpNext() {
  var videoList = new Array();
  var videoListLength;
  var i = 0;
  var index = 1;
  var locID = document.location.href.split("videogallery/")[1].split("/")[0];
  var videoIDList = new Array();
  var elements = jQuery('.play-order-data .item');
  elements.each (function (index, value) { 
  	var currentRef = value.href.split('videogallery/')[1].split('/')[0];
  	videoList.push(value.href);
  	videoIDList.push(currentRef);
  });
  
  videoListLength = videoList.length;
  
  if (locID === "" && (videoListLength > 1)) {
    locID = videoIDList[0];
  }
  
  if (locID !== "") {
    for (i; i < videoListLength; i++) {
      var linkID = videoList[i].split('videogallery/')[1].split('/')[0];
      var index;
      var nextItem;
      var elm;
      
      if (i >= (videoListLength - 1)) {
        index = 0;
      } else {
        index = i + 1;
      }
      
      nextItem = "#item_" + videoList[index].split('videogallery/')[1].split('/')[0];
      
      if (linkID === locID) {
        elm = jQuery(nextItem);
        var elmPos = (elm.outerHeight(true) - 3) * index;
        var scroller = jQuery("#up-next-scroller");
        
        scroller.scrollTop(elmPos);
        elm.addClass("highlight");
        elm.append("<div class='next'>NEXT</div>");
        break;
        
      } else {        
        jQuery(nextItem).removeClass("highlight");
      }
    }
  }
}

function gotoNextVideo() {
  if (window.history && window.history.pushState) {
    window.history.pushState(currentListItem, '');
  }
  window.location.replace(currentListItem);
}

function setThumbnailHover() {
  jQuery('.category-item-block').unbind('mouseenter mouseleave');
	jQuery('.category-item-block').hover( function(event) {
		jQuery(this).find('.category-item-thumb').stop().animate({"opacity": ".7"}, 100);
	  jQuery(this).find('.play-button-over').fadeIn(100);
	},
	function() {
		jQuery(this).find('.category-item-thumb').stop().animate({"opacity": "1"}, 200);
		jQuery(this).find('.play-button-over').css({'display':'none'});
	});
};

function bcsyncroadblock(adXML) {	
  var newXML   = adXML.replace(/>(\s)*?</g, '><');
  var vastXML   = getXMLDoc(newXML);
  var adImg     = new Image();
  var companionList = vastXML.getElementsByTagName('Companion');
  var htmlAd;
  var iframeAd;
  var staticAd;
  var elm       = document.getElementById("companion_Ad");
  var adLink;
  var adType;
  var adURL;
  var companionAd;
  var currentAd;
  var html;
  var link;
  
  vastInlineAd = false;
  
  for (var i = 0; i < companionList.length; i++) {
    var w = companionList[i].getAttribute('width');
    var h = companionList[i].getAttribute('height');
    if ((w == 300) && (h == 250)) {
      companionAd = companionList[i];
      break;
    }	
  }
  
  if (companionAd) {
  	var htmlAd    = companionAd.getElementsByTagName('HTMLResource')[0];
    var iframeAd  = companionAd.getElementsByTagName('IFrameResource')[0];
    var staticAd  = companionAd.getElementsByTagName('StaticResource')[0];
  } else {
     return false;
  }
        
  currentAd = staticAd || htmlAd || iframeAd;
  
  adLink = vastXML.getElementsByTagName('CompanionClickThrough')[0];
  
  if (adLink) {
    adLink = adLink.firstChild.nodeValue;
  }
  
  if (currentAd === staticAd) {
    adType = "static";
  } else if (currentAd === iframeAd) {
    adType = "iframe";
  } else if (currentAd === htmlAd) {
    adType = "html";
  } else {
    return false;
  }
  
  elm.innerHTML = "";
  adURL = currentAd.firstChild.nodeValue;
  adURL = adURL.replace(/pfadx/, "adi"); //move this into the brightcove loadAd function
  adURL = jQuery.trim(adURL);
  
  if (adURL === "") return false;
  
  if (adType === "static") {
    if (adURL.match(/[*+]?\.swf/i)) {
      html = '<a style="display:block; width:300px; height:250px;" target="_blank" href="' + adLink +  '"><object width="300" height="250"><param name="wmode" value="transparent"><param name="movie" value= "' + adURL + '"><embed src="' + adURL + '" width="300" height="250"></embed></object></a>';
      elm.innerHTML = html;
    } else {
      adImg.onload = function() { 
        if (!adImg.complete && (typeof adImg.naturalWidth !== "undefined" && adImg.naturalWidth === 0)) {
          html = '<iframe src="' + adURL + '" allowtransparency="true" id="ifr_companion" width="300" height="250" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>' + '</iframe>';
          elm.innerHTML = html;
          return false;
        }
        
        link = document.createElement('a');
        link.setAttribute('href', adLink);
        link.setAttribute('target', '_blank');
        link.setAttribute('id', 'pvp_companion_image_link');
        link.appendChild(adImg);
        
        elm.appendChild(link);
      }
      
      adImg.onerror = function () {

        html = '<iframe src="' + adURL + '" allowtransparency="true" id="ifr_companion" width="300" height="250" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>' + '</iframe>';
        elm.innerHTML = html;
        return false;
      }
      
      adImg.src = adURL;
    }
  } else if (adType === "iframe") {
    if (adURL.match(/<iframe/ig)) {
      html = adURL;
    } else {
      html = '<iframe src="' + adURL + '" allowtransparency="true" id="ifr_companion" width="300" height="250" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>' + '</iframe>'; 
    }
    elm.innerHTML = html;
  } else if (adType === "html") {
    if (adURL.indexOf("<noscript") === -1) {
      if (adURL.match(/<iframe/ig)) {
        html = adURL;
      } else if (adURL.match(/^<a href=/i)) { 
        html = adURL;
      } else {
        html = '<iframe src="' + adURL + '" allowtransparency="true" id="ifr_companion" width="300" height="250" marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scrolling=no>' + '</iframe>'; 
      }
      elm.innerHTML = html;
    } else {
      elm.innerHTML = adURL;
      if (elm.childNodes.length > 2) {
        var str = elm.childNodes[2].textContent;
        elm.innerHTML = str;
      } else {
        elm.innerHTML = "";
      }
    }
  } else {
    html = adURL;
    elm.innerHTML = html;
  }
  
  vastInlineAd = true;
}

// Loads adXML as XML.
function getXMLDoc(pXML){
     var adXML;
     if (window.ActiveXObject) { //parses the XML for IE browsers
        adXML = new ActiveXObject("Microsoft.XMLDOM");
        adXML.async = false; adXML.loadXML(pXML);
     } else //parses the XML for Mozilla browsers
     if (window.XMLHttpRequest) {
         adXML = (new DOMParser()).parseFromString(pXML, "text/xml");
     }
     return adXML;
 }

function onEmailClick(e) { 
  menu.showMenuPage("Email");
}
function onAboutClick(e) {
  showMenuPage("Link");
}
function onAdStart(e) {
  //adDuration = adModule.getCurrentAdProperties().duration;
}

// Share Tooltip
jQuery(function () {
  jQuery('.video-share-link').each(function () {
    var distance = 0;
    var time = 250;
    var hideDelay = 100;
    var hideDelayTimer = null;
    var beingShown = false;
    var shown = false;
    var trigger = jQuery('.trigger', this);
    var infoPop = jQuery('.video-share-info', this).css('opacity', 0);

    jQuery([trigger.get(0), infoPop.get(0)]).mouseover(function () {
      if (hideDelayTimer) clearTimeout(hideDelayTimer);

      if (beingShown || shown) {
        return;
      } else {
        beingShown = true;

        infoPop.css({
          top: 30,
          right: 0,
          display: 'block'
        })

        .animate({
          top: '-=' + distance + 'px',
          opacity: 1
        }, time, 'swing', function() {
          beingShown = false;
          shown = true;
        });
      }
    }).mouseout(function () {
      if (hideDelayTimer) clearTimeout(hideDelayTimer);
      
      hideDelayTimer = setTimeout(function () {
        hideDelayTimer = null;
        infoPop.animate({
          top: '-=' + distance + 'px',
          opacity: 0
        }, time, 'swing', function () {
          shown = false;
          infoPop.css('display', 'none');
        });
      }, hideDelay);
    });
  });
});
