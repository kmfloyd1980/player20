// Omniture account code
var s_account="hearstseattlepi"

// Exit links list 3rd-party partner domains; Omniture then understands user hasn't left site despite domain change
var s_exitlinks = "javascript:,seattlepi.com,";
s_exitlinks += "bleacherreport.com,wunderground.com,affiliate.zap2it.com,";
s_exitlinks += "seattlepi.kaango.com,coupons.com,hotjobs.yahoo.com,64.246.64.33,";
s_exitlinks += "hosted.ap.org,businesswire.com,uclick.com,seattlepi.lawinfo.com,mediawebsite.net,";
s_exitlinks += "newhomesource.com,seattle.urbancondoliving.com,landsofamerica.com";

// Google Analytics account code
var ga_account="UA-15987431-1";

// Jump Time account code
var jt_account="seattlepi";

// iCrossing account code
var ix_account = "c10014.ic-live.com/pixel-js/c10014-pixel.js";

// Domain names of 1st-party cookie servers
var s_cookieServer="metrics.seattlepi.com";
var s_cookieServerSecure="smetrics.seattlepi.com";

var s_mainDomain="http://www.seattlepi.com/";
