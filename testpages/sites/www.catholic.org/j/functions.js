function cookieHandler() {
    this.setCookie = function (name, value, seconds) {
        if( typeof( seconds ) != 'undefined' ) {
            var date = new Date();
            date.setTime( date.getTime() + ( seconds * 1000 ) );
            var expires = "; expires=" + date.toUTCString();
        } else {
            var expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    this.getCookie = function (name) {
        name = name + "=";
        var carray = document.cookie.split(';');

        for( var i=0; i < carray.length; i++ ) {
            var c = carray[i];
            while( c.charAt(0)==' ' ) {
                c = c.substring( 1, c.length );
            }

            if( c.indexOf(name) == 0 ) {
                return c.substring( name.length, c.length );
            }
        }
        return null;
    }
 
    this.deleteCookie = function (name) {
        this.setCookie(name, "", -1);
    }
}

function getWikiContent(term,wiki_location) {

	$(wiki_location).append($('<div id="wikiInfo"></div><em>from Wikipedia</em>'));

        $.getJSON('http://en.wikipedia.org/w/api.php?action=parse&page=' + term + '&prop=text&format=json&callback=?', function(json) {
                $('#wikiInfo').html(json.parse.text['*']);
                $("#wikiInfo").find('.dablink').remove();
                $("#wikiInfo").find('.editsection').remove();
                $("#wikiInfo").find('span#External_links').parent().next('ul').remove();
                $("#wikiInfo").find('span#External_links').parent().remove();
                $("#wikiInfo").find('a').contents().unwrap();
                $("#wikiInfo").find('table').remove();
        });
}


$(document).ready(function()
{

    // advent dropdowns
    $("ul.dropdown li").hover(function(){

        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');

    }, function(){

        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');

    });
    
    $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");
    // end advent dropdown




    var mycookie = new cookieHandler();

    if( mycookie.getCookie('col-social') ) {
        // do nothing
    } else {
        $('#interaction-wrap').show();
        $('.social-close').click(function () {
            $('#interaction-wrap').fadeOut("slow", function(){
                mycookie.setCookie('col-social',true,'864');
                // Animation complete
            });
        });

    }


	/* Menu Drop Downs */
	var img1 = document.createElement('img');
	img1.src = "/m/screen/bg-main-nav-drop.png";	
	
	var img2 = document.createElement('img');
	img2.src = "/m/screen/bg-main-nav-drop-alt.png";	
	
	var img3 = document.createElement('img');
	img3.src = "/m/widgets/loading.gif";
	
	$(['news', 'catholic-life', 'local-editions']).each(function()
	{
		var tab = this;
		
		$('.nav-'+tab).parents('li:eq(0)').hover(
			
			function ()
			{
				$('#main-nav > li.on').mouseout();
				
			 $(this).addClass('on');
				
				var $anchor = $(this).find('a.nav-'+tab);
				var $tabContainer = $('.tab-'+tab);
				
				if ($tabContainer.size())
				{
					$tabContainer.show();
				}
				
				else
				{
					var $dropDownContainer = $('<div class="tab-container tab-'+tab+'"><img src="/m/widgets/loading.gif" style="position:absolute; top:25%; left:46%;" /></div>');
					$anchor.after($dropDownContainer);
					$.get('/includes/tab-'+tab+'/', {}, function(data)
					{
						$dropDownContainer.html(data);
						
						$('#main-nav > li:last-child .actions a:last').blur(function() { $('#main-nav > li.on').mouseout(); });
					});
				}
			},
			
			function ()
			{
				if ($.browser.msie) {
					$('.tab-'+tab).hide();
				}
				else
				{
					$('.tab-'+tab).fadeOut(200);
				}
				$(this).removeClass('on');
			}
		);
		
		$('.nav-'+tab).focus(function () { $(this).parents('li:eq(0)').mouseover(); });
		$('#menu-logo li:last-child a').focus(function() { $('#main-nav > li.on').mouseout(); });
		
		$('.nav-'+tab).click(function()
		{
			return false;
		});

	});
	
	/* Carousel/Slideshow Helpers */
	function getPrevious($data)
	{
		if ($data.find('li.on').prev().size())
		{
			return $data.find('li.on').prev().find('a');
		}
		
		if ($data.find('li:last-child').hasClass('direction'))
		{
			return $data.find('li:last-child').prev().find('a');
		}
		
		return $data.find('li:last-child').find('a');
	}
	
	function getNext($data)
	{
		if ($data.find('li.on').next().size() && !$data.find('li.on').next().hasClass('direction'))
		{
			return $data.find('li.on').next().find('a');
		}

		return $data.find('li:first-child').find('a');
	}
	
	function setView($data, numInView)
	{
		if (numInView == undefined)
		{
			numInView = 10;
		}
		
		var on = $data.find('.on').prevAll('li').size();
		var min = (Math.floor(on/numInView)*numInView);
		var max = (Math.floor(on/numInView)*numInView)+(numInView-1);

		$data.find('li').hide();
		$data.find('li:eq('+min+')').css('display', '');
		$data.find('li:gt('+min+')').css('display', '');
		$data.find('li:gt('+max+')').css('display', 'none');
		$data.find('.direction').css('display', '');
	}
	
	
	/* Homepage Tabset */
	$('div#main').wrapInner('<div id="main-js-wrap"></div>')
	$('.roll li a').each(function(i)
	{
		var anchorNumber = i+1;
		var $anchor = $(this);
		$anchor.click(function ()
		{
			$anchor.parents('ul').find('.on').removeClass('on');
			$anchor.parents('li').addClass('on');
			
			var $main = $('#main');
			var $main_js_wrap = $('div#main-js-wrap');
			
			$('div#main').css({height: $main_js_wrap.height(), position:'relative'});
			$('div#main').append('<img id="ajax-loader" src="/m/widgets/loading.gif" style="position:absolute; top:190px; left:49%;" />');
			$('div#main-js-wrap').fadeOut();
			$('div#main-js-wrap').load('homepage-tabs/tab'+anchorNumber, {}, function()
			{
				$('img#ajax-loader').remove();
				$('div#main').css({height: 'auto'});
				$('div#main-js-wrap').fadeIn();
			});
			
			return false;
		});
	})
	
	/* Tabset */
        var $popular = $('.most-popular-tabs');
        var $tabs = $('<ul class="tabs"></ul>');
        $popular.prepend($tabs);
        $popular.find('h3').each(function(i)
        {
                var $h3 = $(this);
                var $tab = $('<li><a href="#" onclick="return false;" class="'+$h3.attr('class')+'">'+$h3.html()+'</a></li>');
                $tabs.append($tab);
                $h3.addClass('move');
                var $tabContent = $h3.next();
                $tabContent.addClass('social-tab-container');
                $tab.click(function()
                {
                        $('.tabs li').removeClass('on');
                        $popular.find('div.social-tab-container').hide();
                        $tab.toggleClass('on');
                        $tabContent.toggle();
                });

                if (i == 0) $tab.addClass('on').addClass('first');
                if (i > 0) $tabContent.toggle();
        });


	/* Accordian */
	$('ul.accordian h3').wrapInner('<a href="#"></a>');
	$('ul.accordian h3 a').click(function()
	{
		var $anchor = $(this);
		var $li = $anchor.parents('li');
		
		$li.siblings().addClass('expand');
		$li.removeClass('expand');
		
		return false;
	});
	
	/* Roster */
	$('ul.roster:not(.accordian) h3').wrapInner('<a href="#"></a>');
	$('ul.roster:not(.accordian) h3 a').click(function()
	{
		var $anchor = $(this);
		var $li = $anchor.parents('li:eq(0)');
		
		if (!$li.hasClass('expand'))
		{
			$li.addClass('expand');
			$li.removeClass('collapse');
			$li.find('ul:eq(0)').hide();
		}
		
		else
		{
			$li.removeClass('expand');
			$li.addClass('collapse');
			$li.find('ul:eq(0)').show();
		}
		
		return false;
	});
	$('ul.roster:not(.accordian) li.expand > ul').hide();
	$('ul.roster:not(.accordian) > li:first-child').removeClass('expand');
	$('ul.roster:not(.accordian) > li:first-child').addClass('collapse');
	$('ul.roster:not(.accordian) > li:first-child ul:eq(0)').show();
	

});


        /* Split Dates */
        function splitDate(input, output)
        {
	    $('#'+input).each(function()
	    {
		var date = $(this).val();
		var pieces = date.split('index.html');
		var outputs = ['mm', 'dd', 'yy'];
		
		for (var i=0; len=pieces.length,i<len; i++)
		{
			$('#'+output+'_'+outputs[i]).val(pieces[i]);
		}
	    });
        }



        /* Video Carousel */
        function mycarousel_initCallback(carousel) {
            jQuery('.jcarousel-control a').bind('click', function() {
                carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
                return false;
            });

            jQuery('.jcarousel-scroll select').bind('change', function() {
                carousel.options.scroll = jQuery.jcarousel.intval(this.options[this.selectedIndex].value);
                return false;
            });

            jQuery('#mycarousel-next').bind('click', function() {
                carousel.next();
                return false;
            });

            jQuery('#mycarousel-prev').bind('click', function() {
                carousel.prev();
                return false;
            });

            // Disable autoscrolling if the user clicks the prev or next button.
            carousel.buttonNext.bind('click', function() {
                carousel.startAuto(0);
            });

            carousel.buttonPrev.bind('click', function() {
                carousel.startAuto(0);
            });

            // Pause autoscrolling if the user moves with the cursor over the clip.
            carousel.clip.hover(function() {
                carousel.stopAuto();
            }, function() {
                carousel.startAuto();
            });
        }

        /* Slider Carousel */
        function mycarousel_lg_initCallback(carousel) {
            jQuery('.jcarousel-control a').bind('click', function() {
                carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
                return false;
            });

            jQuery('.jcarousel-scroll select').bind('change', function() {
                carousel.options.scroll = jQuery.jcarousel.intval(this.options[this.selectedIndex].value);
                return false;
            });

            jQuery('#mycarousel-next').bind('click', function() {
                carousel.next();
                return false;
            });

            jQuery('#mycarousel-prev').bind('click', function() {
                carousel.prev();
                return false;
            });

            // Disable autoscrolling if the user clicks the prev or next button.
            carousel.buttonNext.bind('click', function() {
                carousel.startAuto(0);
            });

            carousel.buttonPrev.bind('click', function() {
                carousel.startAuto(0);
            });

            // Pause autoscrolling if the user moves with the cursor over the clip.
            carousel.clip.hover(function() {
                carousel.stopAuto();
            }, function() {
                carousel.startAuto();
            });
        }


// DropDown Menus
$(function(){
    $("ul.dropdown li").hover(function(){
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
    }, function(){
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
   /* $("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; "); */
});


// Video tabs
$(document).ready(function() {

        $('.vtabs a').click(function(){
                switch_tabs($(this));
        });

        switch_tabs($('.defaultvtab'));

});
function switch_tabs(obj)
{
        $('.vtab-content').hide();
        $('.vtabs a').removeClass("selected");
        var id = obj.attr("rel");

        $('#'+id).show();
        obj.addClass("selected");
}


// Advent/Christmas menu tabs
$(document).ready(function() {

        $('.acm-tabs a').click(function(){
                switch_acmtabs($(this));
        });

        switch_acmtabs($('.defaultacm-tab'));

});
function switch_acmtabs(obj)
{
        $('.acm-tab-content').hide();
        $('.acm-tabs a').removeClass("selected");
        var id = obj.attr("rel");

        $('#'+id).show();
        obj.addClass("selected");
}

// Back to Top
$("#backToTop").click(function(){
	$('html, body').animate({ scrollTop: 0 }, 'slow');
});

