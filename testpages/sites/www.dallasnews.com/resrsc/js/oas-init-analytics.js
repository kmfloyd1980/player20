ad_mgr = {};
function OASgetBeloUserId()
{
	var beloUserId = "";
	var beloHash= OASgetHASHFromBeloCookie("img2","|",0);
	if (beloHash != null)
	{
		if (beloHash.length > 1)
		{
			beloUserId = beloHash;
		}
	}
	return beloUserId;
}
function OASgetHASHFromBeloCookie(cName,delim,cIndex)
{
	var lc = OASgetCookie(cName);
	if (lc!=null)
	{
		var parts,result;
		if (cName!=null && cName!="undefined")
		{
			parts = lc.split(delim);
			if (parts.length > 0)
			{
				result = parts[cIndex];
			}
			else
			{
				return null;
			}
		}
	}
	else
	{
		return null;
	}
	return result;
}
function OASgetCookie(name) 
{
	var dc = document.cookie;
	var cname = name + "=";
	var clen = dc.length;
	var cbegin = 0;
	
	while (cbegin < clen) {
		var vbegin = cbegin + cname.length;
		
		if (dc.substring(cbegin, vbegin) == cname) {
			var vend = dc.indexOf (";", vbegin);
			if (vend == -1) vend = clen;
			
			return unescape(dc.substring(vbegin, vend));
		}
	
		cbegin = dc.indexOf(" ", cbegin) + 1;
		
		if (cbegin== 0) break;
	}
	return "";
}
function OASGetAgeRange(year)
{
	d = new Date();
	age = d.getFullYear() - year;
	range = '';
	if( age >= 13 && age <= 17)
	{
		range = '13-17';
	}
	else if( age >= 18 && age <= 20)
	{
		range = '18-20';
	}
	else if( age >= 21 && age <= 24)
	{
		range = '21-24';
	}
	else if( age >= 25 && age <= 29)
	{
		range = '25-29';
	}
	else if( age >= 30 && age <= 34)
	{
		range = '30-34';
	}
	else if( age >= 35 && age <= 39)
	{
		range = '35-39';
	}
	else if( age >= 40 && age <= 44)
	{
		range = '40-44';
	}
	else if( age >= 45 && age <= 49)
	{
		range = '45-49';
	}
	else if( age >= 50 && age <= 54)
	{
		range = '50-54';
	}
	else if( age >= 55 && age <= 59)
	{
		range = '55-59';
	}
	else if( age >= 60 && age <= 64)
	{
		range = '60-64';
	}
	else if( age >= 65 && age <= 100)
	{
		range = '65-100';
	}
	return range;
}
function OASgetDomain()
{
	var domainName = document.domain;
	var domainNameArr = domainName.split(".");
	if (domainNameArr.length >= 3)
	{
		domainName = "";
		for (var i = 1 ; i < domainNameArr.length ; i++)
		{
			domainName += "." + domainNameArr[i]
		}
	}
	return domainName;
}
function OASsetCookie(name, value, expiration)
{
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + expiration * 24 * 60 * 60 * 1000);
	var strCookie = name + "=" + escape(value) + "; expires=" + expirationDate.toGMTString() + "; path=/; domain=" + OASgetDomain();
	document.cookie = strCookie;
}
function OASprocessRegResult(regResp)
{
	if (regResp.GetUserDataByIMG.Status == "Success")
	{
		OASsetCookie("OASImgKeyV1", OASgetBeloUserId(), 7);
		OASsetCookie("OASImgZipCode", regResp.GetUserDataByIMG.Zip_Code, 7);
		OASsetCookie("OASImgBirthYear", regResp.GetUserDataByIMG.Birth_Year, 7);
		OASsetCookie("OASImgGender", regResp.GetUserDataByIMG.Gender, 7);
		OASsetCookie("OASImgEmail", regResp.GetUserDataByIMG.Email_Address, 7);
		OASsetDemoInfo()
	}
	else
	{
		OASclearUserInfo();	
	}
}
function OASclearCookie(cookieName)
{
	OASsetCookie(cookieName, 0, -10);
}	
function OASclearUserInfo()
{
	OASclearCookie("OASImgKey");
	OASclearCookie("OASImgKeyV1");
	OASclearCookie("OASImgZipCode");
	OASclearCookie("OASImgBirthYear");
	OASclearCookie("OASImgGender");
	OASclearCookie("OASImgEmail");
	
	ad_mgr.user_zip="";
	ad_mgr.user_age="";
	ad_mgr.user_gender="";
}
function getEmailAddress()
{
	return(OASgetCookie("OASImgEmail"));
}

/*  set demographic info in the ad call */

function OASsetDemoInfo()
{
	try
	{
		if (OASgetCookie("img2"))
		{
			if (OASgetCookie("OASImgZipCode").length > 0)
			{
				ad_mgr.user_zip = OASgetCookie("OASImgZipCode");
			}
			
			if (OASgetCookie("OASImgBirthYear").length > 0)
			{
				ad_mgr.user_age = OASGetAgeRange(OASgetCookie("OASImgBirthYear"));
			}
			
			if (OASgetCookie("OASImgBirthYear").length > 0)
			{
				switch (OASgetCookie("OASImgGender").toLowerCase())
				{
					case "m":
						ad_mgr.user_gender = "male";
						break;
					case "f":
						ad_mgr.user_gender = "female";
						break;
				}
			}
		}
	}
	catch(e)
	{
	}
}
function OASsetUserInfo()
{
	try
	{
		OASRegServer = "https://reg" + OASgetDomain();
		if (belo.cgm.util.isLoggedInForBelo())
		{
			var OASImgKey = OASgetCookie("OASImgKeyV1");
			var regImgKey = OASgetBeloUserId();
			if (OASImgKey != regImgKey)
			{
				OASclearUserInfo();
				document.writeln('<sc' + 'ript type="text/javascript" src="' + OASRegServer + '/SSO/GetUserDatabyIMG2.do?cookie_id=' + regImgKey + '&format=json&callback=OASprocessRegResult"></sc' + 'ript>');
			}
			else
			{
				OASsetDemoInfo();
			}
		}
		else
		{
			OASclearUserInfo();
		}
	}
	catch (e)
	{
		OASclearUserInfo();
	}
}	
OASsetUserInfo();

/* analytics tracking ... */

function getOasAnalyticsQuery(oas_query)
{
	var oas_orig_query = oas_query;
	var oas_taxonomy = "";
	var age = "";
	var gender = "";
	try
	{
		if (typeof (ad_mgr) != 'undefined')
		{
			if (typeof (ad_mgr.user_age) != 'undefined' && ad_mgr.user_age.length > 0)
			{
				age = ad_mgr.user_age;
			}
			if (typeof (ad_mgr.user_gender) != 'undefined' && ad_mgr.user_gender.length > 0)
			{
				gender = ad_mgr.user_gender;
			}
		}
		if (age.length > 0)
		{
			oas_taxonomy += (oas_taxonomy.length == 0 ? 'Age=' + age : '&Age=' + age);
		}
		
		if (gender.length > 0)
		{
			oas_taxonomy += (oas_taxonomy.length == 0 ? 'Gender=' + gender : '&Gender=' + gender);
		}
		
		if (oas_taxonomy.length > 0)
		{
			oas_taxonomy = 'XE' + '&' + oas_taxonomy + OAS_rdl + "&if_nt_CookieAccept=" + OAS_CA + "&XE";
		}
		
		oas_query += (oas_query.length == 0 ? oas_taxonomy : '&' + oas_taxonomy);

		return (oas_query);
	}
	catch(e)
	{
		return (oas_orig_query);
	}
}
