function DMNToggleAccordionHeader(div,hdr)
{
	$("#" + div + " h3 .DMNAccModuleHeader").removeClass("on");
	$(hdr).addClass("on");
}

function getSearchParamValue( sKey )
{
 var sString = document.location.search;
 if(sString && sString.indexOf(sKey))
 {
    sString = sString.substring(1);
    var sArray= sString.split('&');
    if(sArray && sArray.length > 0)
    {
       for(var s=0; s< sArray.length; s++)
       {
          var keyVal=sArray[s].split('=');
          if(keyVal[0] == sKey)
          {
             return keyVal[1];
          }
       }
       return "";
    }
    return "";
 }
 return "";
}

function doSearchHomesFindIt(thisForm) {
	var geo_area_text = thisForm.geo_area_text.value;
	var state = thisForm.state.value;
	var property_type = thisForm.property_type.value;
	var min_price = thisForm.min_price.value;
	var max_price = thisForm.max_price.value;
	var min_bed = thisForm.min_bed.value;
	var min_bath = thisForm.min_bath.value;
	if (thisForm.search_filter) {
		var search_filter = thisForm.search_filter.value;
	}
	
	//OAS_sitepage += geo_area_text;
	
	var sbURL = "http://dallasnews.sawbuck.com/search?q="
	var qs = "";
	if (min_bed.length > 0)
	{
		if (qs.length > 0) qs += "+";
		qs += min_bed;
	}
	if (min_bath.length > 0)
	{
		if (qs.length > 0) qs += "+";
		qs += min_bath;
	}
	if (qs.length > 0) qs += "+";
	qs += "$" + min_price + "-" + max_price;
	
	if (property_type.length > 0)
	{
		if (qs.length > 0) qs += "+";
		qs += property_type;
	}
	if (qs.length > 0) qs += "+";
	qs += "in";
	if (geo_area_text.length > 0)
	{
		qs += "+" + geo_area_text;
	}
	if (state.length > 0)
	{
		if (qs.length > 0) qs += "+";
		qs += state;
	}
	
	//If this is an MLS Search, only pass in the mls number
	if (geo_area_text.length > 0 && geo_area_text.length != 5 && !isNaN(geo_area_text))
	{
		qs = geo_area_text;
	}
	
	qs = escape(qs);
	
	if (search_filter && search_filter.length > 0)
	{
		qs += "&page=" + search_filter;
	}
	
	sbURL += qs;
	
	document.location.replace(sbURL);
}
