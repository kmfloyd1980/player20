window.domainArray = window.location.hostname.split('.');
window.regServer = 'https://reg.' + window.domainArray[1] + '.com';
window.regDevServer = 'https://regdev.' + window.domainArray[1] + '.com';
window.regStageServer = 'https://regstage.' + window.domainArray[1] + '.com';
window.regEceServer = 'https://regstage.' + window.domainArray[1] + '.com';
window.siteServer = 'http://www.' + window.domainArray[1] + '.com';
window.siteDevServer = 'http://dev.' + window.domainArray[1] + '.com';
window.siteStageServer = 'http://stage.' + window.domainArray[1] + '.com';
window.siteEceServer = 'http://ece.' + window.domainArray[1] + '.com';
if (window.domainArray[0].toLowerCase() == 'dev') {window.regServer = window.regDevServer;window.siteServer = window.siteDevServer;}
if (window.domainArray[0].toLowerCase() == 'stage') {window.regServer = window.regStageServer;window.siteServer = window.siteStageServer;}
if (window.domainArray[0].toLowerCase() == 'ece') {window.regServer = window.regEceServer;window.siteServer = window.siteEceServer;}

window.regCookieName = 'img2';
window.subCookieName = 'pimg2';
window.cgmCookieName = 'AT';
window.defaultAvatar = 'src="' + window.siteServer + '/resources/images/avatarDefault.gif"';
window.cgmArticleIds = [];
window.cgmPageToolCommentCount = [];
window.cgmPageToolRecommendCount = [];

//SubscriberServicesUrl:"https://www.dmnsubscriber.com/",

function loadLoginHeader()
{
	var getAvatar = false;
	var regData = {
		LoginUrl:regServer + "/registration/signin.do",
		LogoutUrl:regServer + "/LogOut.do",
		RegisterUrl:regServer + "/registration/signin.do",
		MemberCenterUrl:regServer + "/registration/membercenter.do",
		SpecialOffersUrl:regServer + "/registration/LoadSpecialOffers.do",
		SubscriberServicesUrl:regServer + "/registration/SiteToSSFwd.do",
		AvatarUrl:defaultAvatar,
		CurrentPageUrl:escape(document.location.href)};
	var loginHeaderTemplate = "loginHeaderNotLoggedInTmpl";
	
	if (belo.cgm.util.isLoggedInForBelo())
	{
		if (belo.cgm.util.isLoggedInForCGM())
		{
			regData.ScreenName = belo.cgm.util.getScreenNameFromCookie();
			loginHeaderTemplate = "loginHeaderLoggedInWithProfileTmpl";
			getAvatar = true;
		}
		else
		{
			loginHeaderTemplate = "loginHeaderLoggedInNoProfileTmpl";
		}
	}

	$("#loginHeader").empty();
	$("#" + loginHeaderTemplate).tmpl(regData).appendTo("#loginHeader");
	
	if (getAvatar && typeof(PluckSDK) != 'undefined') belo.cgm.util.getAvatarFromPluck();
}
function loadCommentsLoginHeader()
{
	var regData = {
		LoginUrl:regServer + "/registration/signin.do",
		LogoutUrl:regServer + "/LogOut.do",
		RegisterUrl:regServer + "/registration/signin.do",
		MemberCenterUrl:regServer + "/registration/membercenter.do",
		SpecialOffersUrl:regServer + "/registration/LoadSpecialOffers.do",
		SubscriberServicesUrl:"https://www.dmnsubscriber.com/",
		AvatarUrl:defaultAvatar,
		CurrentPageUrl:escape(document.location.href)};
	var loginHeaderTemplate = "commentsLoginHeaderNotLoggedIn";
	
	if (belo.cgm.util.isLoggedInForBelo())
	{
		if (belo.cgm.util.isLoggedInForCGM())
		{
			regData.ScreenName = belo.cgm.util.getScreenNameFromCookie();
			loginHeaderTemplate = "commentsLoginHeaderLoggedInWithProfileTmpl";
			getAvatar = true;
		}
		else
		{
			loginHeaderTemplate = "commentsLoginHeaderLoggedInNoProfileTmpl";
		}
	}

	$("#slcgmlogincontainer").empty();
	$("#" + loginHeaderTemplate).tmpl(regData).appendTo("#slcgmlogincontainer");
}

function subscribeForward()
{
	var email = getEmailAddress();
	if (email.length > 0)
	{
		subscribeGoTo(window.regServer + '/registration/offers/messageForYou.do?UID=' + email);
	}
	else
	{
		var fw = "http://" + document.location.hostname + document.location.pathname
		document.location.href=regServer + "/registration/signin.do?fw=" + fw; 
	}
}
function subscribeGoTo(url)
{
	var href = url + "&fw=" + escape("http://" + document.location.hostname + document.location.pathname);
	document.location.href = href;
}

/* belo namespace */
var belo = function()
{
	var _namespace = "belo";
	return { };
}();

/* SiteLife CGM namespace */
belo.cgm = function()
{
	var _namespace = "belo.cgm";
	return {};
}();

belo.cgm.util = function()
{
	var _namespace = "belo.cgm.util";
	return {
		addScript: function(id,type,src)
		{
			var script = document.createElement( 'script' );
			script.type = type;
			script.id = id;
			var scr = src;
			var tt = document.createTextNode(scr);
			script.appendChild(tt);
			document.getElementsByTagName('head').item(0).appendChild(script);
		},
		isLoggedInForBelo: function ()
		{
			var beloHash= this.getHASHFromBeloCookie(regCookieName,"|",0);
			if (beloHash != null)
			{
				if (beloHash.length > 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		},
		isSubscribedToBelo: function ()
		{
			var beloHash= this.getHASHFromBeloCookie(subCookieName,"|",0);
			if (beloHash != null)
			{
				if (beloHash.length > 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		},
		getHASHFromBeloCookie: function(cName,delim,cIndex)
		{
			var lc = belo.cgm.util.getCookie(cName);
			if (lc!=null)
			{
				var parts,result;
				if (cName!=null && cName!="undefined")
				{
					parts = lc.split(delim);
					if (parts.length > 0)
					{
						result = parts[cIndex];
					}
					else
					{
						return null;
					}
				}
			}
			else
			{
				return null;
			}
			return result;
		},
		getCookie: function(name)
		{
			var dc = document.cookie;
			var prefix = name + "=";
			var begin = dc.toLowerCase().indexOf("; " + prefix.toLowerCase());
			if (begin == -1){
				begin = dc.toLowerCase().indexOf(prefix.toLowerCase());
				if (begin != 0) {
					return null; //begin should be at index 0 - fail if not
				}
			} else {
				begin += (name.length-1);
			}
			var end = dc.indexOf(";", begin);
			if (end == -1) {
				end = dc.length;
			}
			var result = unescape(dc.substring(begin + prefix.length, end));
			return result;
		},
		isLoggedInForCGM: function ()
		{
			var displayName=this.getScreenNameFromCookie();
			if (displayName!=null)
			{
				return true;
			}
			return false;
		},
		getScreenNameFromCookie: function()
		{
			return(this.getNameFromCGMCookie(cgmCookieName,"a"));

		},
		getNameFromCGMCookie: function(cName,prefix)
		{

			var lc = belo.cgm.util.getCookie(cName);
			if (lc!=null)
			{
				var begin,end,result;
				if (cName!=null && cName!="undefined")
				{
					prefix+= "=";
					begin = lc.toLowerCase().indexOf(prefix.toLowerCase());
					if (begin == -1)
					{
						begin = lc.toLowerCase().indexOf(prefix.toLowerCase());
						if (begin != 0)
						{
							return null;
						}
					}
					end = lc.indexOf("&", begin);
					if (end == -1)
					{
						end = lc.length;
					}
					result = unescape(lc.substring(begin + prefix.length, end));
				}
				else
				{
					return null;
				}
				return result;
			}
			else
			{
				return null
			}
		},
		setCookie: function (cName,cookieVal,cookieTtl,cookiePath)
		{
		    var ttl=new Date();
		    ttl.setDate(ttl.getDate() + cookieTtl);
		  	document.cookie = cName + "=" + cookieVal
		  		+ ((cookieTtl==null) ? "" : ";expires=" + ttl.toGMTString()
		  		+ ";path=" + cookiePath + ";domain=" + document.domain + ";");
		},
		getAvatarFromPluck: function()
		{
			var avatarUrl = belo.cgm.util.getNameFromCGMCookie('UV','avatar');
			if (avatarUrl != null)
			{
				$(".UtilityAvatarImg").attr("src",avatarUrl);
			}
			else
			{
				var userReq = new PluckSDK.UserRequest();
			    PluckSDK.SendRequests([userReq], belo.cgm.util.getAvatarFromPluckCallback);
			}
		},
		getAvatarFromPluckCallback: function(responses)
		{
			try
			{
				if (responses[0].ResponseStatus.StatusCode == "OK")
				{
					$(".UtilityAvatarImg").attr("src",responses[0].User.AvatarPhotoUrl);
					belo.cgm.util.setCookie('UV','avatar=' + responses[0].User.AvatarPhotoUrl,1,'/');
				}
			}
			catch(e){}
		},
		renderMetaData: function(articleArray)
		{
			var i;
			var requests = [];

			for (i = 0 ; i < articleArray.length ; i++)
			{
				var articleRequest = new PluckSDK.ArticleRequest();
				articleRequest.ArticleKey = new PluckSDK.ExternalResourceKey();
				articleRequest.ArticleKey.Key = articleArray[i].articleId;
				requests.push(articleRequest);
			}

			PluckSDK.SendRequests(requests, function(responses)
				{
					try
					{
						for (i = 0 ; i < responses.length ; i++)
						{
							var recommendString = '<a href=\'#\' onClick=\'javascript:belo.cgm.util.recommendArticle("' + articleArray[i].articleId + '","' + articleArray[i].divId + '","' + articleArray[i].articleTitle + '");\'>Recommend</a> <img src="' + window.siteServer + '/resources/images/cgm/assets/thumb_up.gif"/>';
							if (responses[i].ResponseStatus.StatusCode == "OK")	
							{
								if (responses[i].Article.Recommendations.CurrentUserHasRecommended)
								{
									recommendString = 'Recommended <img src="' + window.siteServer + '/resources/images/cgm/assets/thumb_up_disabled.gif"/>';
								}
							}							
							else
							{
								responses[i].Article = {};
								responses[i].Article.Comments = {};
								responses[i].Article.Comments.NumberOfComments = 0;
								responses[i].Article.Recommendations = {};
								responses[i].Article.Recommendations.NumberOfRecommendations = 0;
							}
							responses[i].Article.RecommendString = recommendString;
							$("#" + articleArray[i].divId).empty();
							$("#storyCommentCountTmpl").tmpl(responses[i].Article).appendTo("#" + articleArray[i].divId);
						}
					}
					catch(e){};
				});
		},
		renderCommentCount: function(articleArray)
		{
			var i;
			var requests = [];

			for (i = 0 ; i < articleArray.length ; i++)
			{
				var articleRequest = new PluckSDK.ArticleRequest();
				articleRequest.ArticleKey = new PluckSDK.ExternalResourceKey();
				articleRequest.ArticleKey.Key = articleArray[i].articleId;
				requests.push(articleRequest);
			}

			PluckSDK.SendRequests(requests, function(responses)
				{
					try
					{
						for (i = 0 ; i < responses.length ; i++)
						{
							if (responses[i].ResponseStatus.StatusCode != "OK")	
							{
								responses[i].Article = {};
								responses[i].Article.Comments = {};
								responses[i].Article.Comments.NumberOfComments = 0;
								
							}
							$("#" + articleArray[i].divId).empty();
							$("#storyCommentCountPageToolTmpl").tmpl(responses[i].Article).appendTo("#" + articleArray[i].divId);
						}
					}
					catch(e){};
				});
		},
		renderRecommendCount: function(articleArray)
		{
			var i;
			var requests = [];

			for (i = 0 ; i < articleArray.length ; i++)
			{
				var articleRequest = new PluckSDK.ArticleRequest();
				articleRequest.ArticleKey = new PluckSDK.ExternalResourceKey();
				articleRequest.ArticleKey.Key = articleArray[i].articleId;
				requests.push(articleRequest);
			}

			PluckSDK.SendRequests(requests, function(responses)
				{
					try
					{
						for (i = 0 ; i < responses.length ; i++)
						{
							var recommendString = '<a href=\'#\' onClick=\'javascript:belo.cgm.util.recommendArticle("' + articleArray[i].articleId + '","' + articleArray[i].divId + '","' + articleArray[i].articleTitle + '");\'>Recommend</a> <img src="' + window.siteServer + '/resources/images/cgm/assets/thumb_up.gif" align="absmiddle" style="position: relative; top: -3px;"/>';
							if (responses[i].ResponseStatus.StatusCode == "OK")	
							{
								if (responses[i].Article.Recommendations.CurrentUserHasRecommended)
								{
									recommendString = 'Recommended <img src="' + window.siteServer + '/resources/images/cgm/assets/thumb_up_disabled.gif" align="absmiddle" style="position: relative; top: -3px;"/>';
								}
							}							
							else
							{
								responses[i].Article = {};
								responses[i].Article.Comments = {};
								responses[i].Article.Comments.NumberOfComments = 0;
								responses[i].Article.Recommendations = {};
								responses[i].Article.Recommendations.NumberOfRecommendations = 0;
								
							}
							responses[i].Article.RecommendString = recommendString;
							$("#" + articleArray[i].divId).empty();
							$("#storyRecommendCountPageToolTmpl").tmpl(responses[i].Article).appendTo("#" + articleArray[i].divId);
						}
					}
					catch(e){};
				});
		},
		recommendArticle: function(articleId,divId,title) 
		{
			var articleKey = new PluckSDK.ExternalResourceKey( {Key:articleId} );
			var action = new PluckSDK.RecommendActionRequest();
			action.RecommendedKey = articleKey;
			action.Title = title;
			var requests = [];
			requests.push(action);
			PluckSDK.SendRequests(requests, function(responses){belo.cgm.util.renderMetaData(articleId,divId);});
			return false;
		},
		loadPhotoGallery: function(galleryId,galleryDivId,imagesDivId,numberOfPhotos,imageWidth)
		{
			var photoGallery = {"galleryId":galleryId};
			$("#" + galleryDivId).empty();
			$("#" + imagesDivId).empty();
			$("#recentPhotosGalleryTmpl").tmpl(photoGallery).appendTo("#" + galleryDivId);

			var requests = []
			var photoRequest = new PluckSDK.PhotosPageRequest();
			photoRequest.GalleryKey = new PluckSDK.GalleryKey();
			photoRequest.GalleryKey.Key = galleryId;
			photoRequest.ItemsPerPage = numberOfPhotos;
			photoRequest.OneBasedOnPage = 1;
			requests.push(photoRequest);
			PluckSDK.SendRequests(requests, function(responses){
				try
				{
					var outString = "";
					if (responses[0].ResponseStatus.StatusCode == "OK")
					{
						for (var i = 0 ; i < responses[0].Items.length ; i++)
						{
							responses[0].Items[i].ImageWidth = imageWidth;
							responses[0].Items[i].Image.P4Persona = 'src="' + responses[0].Items[i].Image.P4Persona + '"';	
							$("#recentPhotosGalleryImageTmpl").tmpl(responses[0].Items[i]).appendTo("#" + imagesDivId);
							if (i < responses[0].Items.length - 1)
							{
								$("#recentPhotosGalleryFillerTmpl").tmpl(responses[0].Items[i]).appendTo("#" + imagesDivId);
							}
						}
					}
				}
				catch(e){}
			});
		},
		discoverContent: function(activity,age,number,section,divId)
		{
			var discoRequest = new PluckSDK.DiscoverContentActionRequest();

			discoRequest.Activity = activity;
			
			discoRequest.Type = PluckSDK.ContentType.Article;
			
			discoRequest.Age = age;
			discoRequest.MaximumNumberOfDiscoveries = number;
			
			discoRequest.Sections = []
			discoRequest.Sections.push( new PluckSDK.DiscoverySection( {Name:section} ) );
			
			discoRequest.FilterBySiteOfOrigin = true;
			PluckSDK.SendRequests([discoRequest], function(responses){
				var tpl;
				if (responses[0].ResponseStatus.StatusCode == "OK")
				{
					if (responses[0].Activity == "Commented") tpl = "mostCommentedTmpl";
					if (responses[0].Activity == "Recommended") tpl = "mostRecommendedTmpl";
					$("#" + divId).empty();
					for (var i = 0 ; i < responses[0].DiscoveredContent.length ; i++)
					{
						while(responses[0].DiscoveredContent[i].Title.indexOf("\\'") >= 0)
						{
							responses[0].DiscoveredContent[i].Title = responses[0].DiscoveredContent[i].Title.replace("\\'", "'");
						}
						$("#" + tpl).tmpl(responses[0].DiscoveredContent[i]).appendTo("#" + divId);
					}
				}
			});
		}
	}; // close return
}();


/* apt functions */
function APTgetBeloUserId()
{
	var beloUserId = "";
	var beloHash= APTgetHASHFromBeloCookie(regCookieName,"|",0);
	if (beloHash != null)
	{
		if (beloHash.length > 1)
		{
			beloUserId = beloHash;
		}
	}
	return beloUserId;
}
function APTgetHASHFromBeloCookie(cName,delim,cIndex)
{
	var lc = APTgetCookie(cName);
	if (lc!=null)
	{
		var parts,result;
		if (cName!=null && cName!="undefined")
		{
			parts = lc.split(delim);
			if (parts.length > 0)
			{
				result = parts[cIndex];
			}
			else
			{
				return null;
			}
		}
	}
	else
	{
		return null;
	}
	return result;
}
function APTgetCookie(name) 
{
	var dc = document.cookie;
	var cname = name + "=";
	var clen = dc.length;
	var cbegin = 0;
	
	while (cbegin < clen) {
		var vbegin = cbegin + cname.length;
		
		if (dc.substring(cbegin, vbegin) == cname) {
			var vend = dc.indexOf (";", vbegin);
			if (vend == -1) vend = clen;
			
			return unescape(dc.substring(vbegin, vend));
		}
	
		cbegin = dc.indexOf(" ", cbegin) + 1;
		
		if (cbegin== 0) break;
	}
	return "";
}
function APTGetAgeRange(year)
{
	d = new Date();
	age = d.getFullYear() - year;
	range = '';
	if( age >= 13 && age <= 17)
	{
		range = '13-17';
	}
	else if( age >= 18 && age <= 20)
	{
		range = '18-20';
	}
	else if( age >= 21 && age <= 24)
	{
		range = '21-24';
	}
	else if( age >= 25 && age <= 29)
	{
		range = '25-29';
	}
	else if( age >= 30 && age <= 34)
	{
		range = '30-34';
	}
	else if( age >= 35 && age <= 39)
	{
		range = '35-39';
	}
	else if( age >= 40 && age <= 44)
	{
		range = '40-44';
	}
	else if( age >= 45 && age <= 49)
	{
		range = '45-49';
	}
	else if( age >= 50 && age <= 54)
	{
		range = '50-54';
	}
	else if( age >= 55 && age <= 59)
	{
		range = '55-59';
	}
	else if( age >= 60 && age <= 64)
	{
		range = '60-64';
	}
	else if( age >= 65 && age <= 100)
	{
		range = '65-100';
	}
	return range;
}
function APTgetDomain()
{
	var domainName = document.domain;
	var domainNameArr = domainName.split(".");
	if (domainNameArr.length >= 3)
	{
		domainName = "";
		for (var i = 1 ; i < domainNameArr.length ; i++)
		{
			domainName += "." + domainNameArr[i]
		}
	}
	return domainName;
}
function APTsetCookie(name, value, expiration)
{
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + expiration * 24 * 60 * 60 * 1000);
	var strCookie = name + "=" + escape(value) + "; expires=" + expirationDate.toGMTString() + "; path=/; domain=" + APTgetDomain();
	document.cookie = strCookie;
}
function APTprocessRegResult(regResp)
{
	if (regResp.GetUserDataByIMG.Status == "Success")
	{
		APTsetCookie("aptImgKeyV1", APTgetBeloUserId(), 7);
		APTsetCookie("aptImgZipCode", regResp.GetUserDataByIMG.Zip_Code, 7);
		APTsetCookie("aptImgBirthYear", regResp.GetUserDataByIMG.Birth_Year, 7);
		APTsetCookie("aptImgGender", regResp.GetUserDataByIMG.Gender, 7);
		APTsetCookie("aptImgEmail", regResp.GetUserDataByIMG.Email_Address, 7);
	}
	else
	{
		APTclearUserInfo();	
	}
}
function APTsetUserInfo()
{
	if (belo.cgm.util.isLoggedInForBelo())
	{
		var aptImgKey = APTgetCookie("aptImgKeyV1");
		var regImgKey = APTgetBeloUserId();
		if (aptImgKey != regImgKey)
		{
			APTclearUserInfo();
			document.writeln('<sc' + 'ript type="text/javascript" src="' + regServer + '/SSO/GetUserDatabyIMG2.do?cookie_id=' + regImgKey + '&format=json&callback=APTprocessRegResult"></sc' + 'ript>');
		}
		else
		{
			// do nothing
		}
	}
	else
	{
		APTclearUserInfo();
	}
}	
function APTclearCookie(cookieName)
{
	APTsetCookie(cookieName, 0, -10);
}	
function APTclearUserInfo()
{
	APTclearCookie("aptImgKey");
	APTclearCookie("aptImgKeyV1");
	APTclearCookie("aptImgZipCode");
	APTclearCookie("aptImgBirthYear");
	APTclearCookie("aptImgGender");
	APTclearCookie("aptImgEmail");
}
function getEmailAddress()
{
	return(APTgetCookie("aptImgEmail"));
}

/*  set demographic info in the ad call */

function APTsetDemoInfo()
{
	try
	{
		if (APTgetCookie("img2"))
		{
			if (APTgetCookie("aptImgZipCode").length > 0)
			{
				yld_mgr.user_zip = APTgetCookie("aptImgZipCode");
			}
			
			if (APTgetCookie("aptImgBirthYear").length > 0)
			{
				yld_mgr.user_age = APTGetAgeRange(APTgetCookie("aptImgBirthYear"));
			}
			
			if (APTgetCookie("aptImgBirthYear").length > 0)
			{
				switch (APTgetCookie("aptImgGender").toLowerCase())
				{
					case "m":
						yld_mgr.user_gender = "male";
						break;
					case "f":
						yld_mgr.user_gender = "female";
						break;
				}
			}
		}
	}
	catch(e)
	{
	}
}
function APTisNonSub()
{
	if (APTgetCookie('AHCNonSub') == '1') return true;
	return false;
}
APTsetUserInfo();
if (belo.cgm.util.isSubscribedToBelo()) APTclearCookie('AHCNonSub'); 

