window.isIE = true;
window.isKHTML = false;
window.isMac = false;

if (navigator.appName.indexOf('Microsoft') == -1)
{
	window.isIE = false;
}

if(navigator.userAgent.indexOf('Safari')  > -1)
{
   window.isMac = true;   
}
// non-Safari KHTML browser 
else if (navigator.userAgent.indexOf('KHTML') > -1)
{
	window.isKHTML = true;
}


function setCookie(name, value, expiration)
{
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + expiration * 24 * 60 * 60 * 1000);
	
	document.cookie = name + "=" + escape(value) + "; expires=" + expirationDate.toGMTString();
}

function getCookie(name) {
	var dc = document.cookie;
	var cname = name + "=";
	var clen = dc.length;
	var cbegin = 0;
	
	while (cbegin < clen) {
		var vbegin = cbegin + cname.length;
		
		if (dc.substring(cbegin, vbegin) == cname) {
			var vend = dc.indexOf (";", vbegin);
			if (vend == -1) vend = clen;
			
			return unescape(dc.substring(vbegin, vend));
		}
	
		cbegin = dc.indexOf(" ", cbegin) + 1;
		
		if (cbegin== 0) break;
	}
	return null;
}


function clearCookie(cookieName)
{
	setCookie(cookieName, 0, -10);
}	

// video loading for iframe
function loadVideo(container, url, cell)
{
	var  tableContainer = document.getElementById(container);
	var videoTable = tableContainer.getElementsByTagName("table");			
	var videoTableRows = videoTable[0].getElementsByTagName("tr");
	for (var x=0; x < videoTableRows.length; x++)
	{
		videoTableRows[x].className = "";
	}
	
	// highlight selected row
	var row = cell.parentNode;
	while(row.tagName.toLowerCase() != "tr") 
	{					
		row = row.parentNode;
	} 
	
	row.className = "currentStory";
	
	// load video 
	var videoIframe = document.getElementById("main-video");			
	videoIframe.src = url;
}

/**
 * updateHeight()
 * This method adjusts the height of the center content column to be at 
 * least as high as the left column.
 * The height adjustment is achieved using a spacer gif (with the id fillerImg)
 * that will be resized if the left column is longer than the center column.
 */
 
function updateHeight()
{
	var leftCol = document.getElementById("secondarycontainer");
	if(leftCol == null) return;
	var storyContentDiv = document.getElementById("storycontent");
	var fillerImg = document.getElementById("fillerImg");		
	var leftColHeight = leftCol.offsetHeight;
	var fillerImgHeight = leftColHeight - storyContentDiv.offsetHeight;
	if(fillerImgHeight > 0)
	{
		fillerImg.style.height = fillerImgHeight + 30 + "px";
	}
	
}

function initialize()
{
	highlightNavigation();
	hide_marketplace_tabs();
	show_marketplace_tab();
	showTab(getTabCookie());
	if(window.contentId != null)
	{
		displaySubNavigation();
	}
}
