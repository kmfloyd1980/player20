window.fbAsyncInit = function() {
    FB.Event.subscribe('comment.create',
        function (response) {
            FBtrackEvent(22,response.href,26,"Social:Comment:Add");
        });
};
function FBtrackEvent(strEvarId,strEvarValue,strEvent,strProductString)
{
	var eVar = {};
	eVar[strEvarId] = strEvarValue;
	var eventID = "event" + strEvent;
	var s_code;
	if (typeof om_trackProducts != 'undefined')
	{
		s_code = om_trackProducts("","",eventID,strProductString,"","","",eVar);
	}
	else
	{
		if (typeof trackProducts != 'undefined')
		{
			s_code = trackProducts("","",eventID,strProductString,"","","",eVar);
		}
	}
}