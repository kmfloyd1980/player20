var userCustomRedirs = [{"url":"/news/community-news/","cookieName":"HomeCommunity"}];
for (var i = 0 ; i < userCustomRedirs.length ; i++)
{
	var ucr = userCustomRedirs[i];
	if (document.location.pathname == ucr.url)
	{
		if (getCookie(ucr.cookieName))
		{
			if (getCookie(ucr.cookieName).length > 0)
			{
				document.location.replace(getCookie(ucr.cookieName) + "?redir");
			}
		}
	}
}
function ACHViewDefaultCommunity()
{
	var currPage = document.location.pathname;
	document.communities_form.communities_form_dd.value = currPage;
	if (getCookie("HomeCommunity") == document.location.pathname)
	{
		document.communities_form.communities_form_ck.checked = true;
	}
}
function AHCMakeDefaultCommunity (ckEl,ddEl)
{
	if (ckEl.checked)
	{
		if (ddEl.value.length > 0)
		{
			AHCSetCookie("HomeCommunity",ddEl.value,365)
		}
		else
		{
			AHCClearCookie("HomeCommunity")
		}
	}
	else
	{
		AHCClearCookie("HomeCommunity")
	}
	return (true);
}
function AHCGo(el)
{
	if (el.value.length > 0) document.location.href = el.value;
}
function AHCSetCookie(name, value, expiration)
{
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + expiration * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + "; expires=" + expirationDate.toGMTString() + "; path=/";
}
function AHCClearCookie(cookieName)
{
	AHCSetCookie(cookieName, 0, -10);
} 