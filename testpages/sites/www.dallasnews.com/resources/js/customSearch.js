
var intRegex = /^\d+$/;
var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
var nFactsRegex = /nfacts_.*/;

function getElementById(elementId) {
	if (document.getElementById) { // DOM3 = IE5, NS6
		return document.getElementById(elementId);
	} else if (document.layers) { // Netscape 4
		return document.elementId;
	} else { // IE 4
		return document.all.elementId;
	}
}
function isEmpty(form, nFact) {
	var value = $('#'+nFact).val();
	return value == null || value == "";
}

function isNumeric(form, nFact) {
	var IsNumber = false;
	var value = $('#'+nFact).val();
	return intRegex.test(value) || floatRegex.test(value);

}

function validateAndSubmit(form) {
	form.search_type.value='recipe-search-advanced';
	var valid = true;
	var queryString ='';
	for ( var i = 0; i <form.elements.length; i++) {	
		var nFact = form.elements[i];
		if ( nFactsRegex.test(nFact.id) && !validate(nFact.id)) valid = false;
	}
	
	if (valid) {
		getElementById('error-message').style.display = 'none';
		form.submit();
	} 	else {
		getElementById('error-message').style.display = 'block';
}
			
}

function validate(nFact) {
	
	var form = getElementById("recipeSearch");
	var valid = isEmpty(form, nFact) || isNumeric(form, nFact);
	if (!valid)
		getElementById(nFact).style.border = '1px solid red';
	else	
		getElementById(nFact).style.border = '1px solid #A9A9A9';		

	return valid;
}



/*
 * Clear children elements
 */
function clearElement(target) {
	while (target != null && target.childNodes.length > 0) {
		target.removeChild(target.childNodes[0]);
	}
}

/*
 * Create option and add it to parent
 */
function createOption(text, value, parent) {	
	parent.options[parent.options.length] = new Option(text, value);
}


function clearForm() {
	
	var form = getElementById("recipeSearch");
	
	getElementById('error-message').style.display = 'none';
	for ( var i = 0; i <form.elements.length; i++) {		
		var nFact = form.elements[i].id;
		if ( nFactsRegex.test(nFact)) 
			getElementById(nFact).style.border = '1px solid #A9A9A9';				
	}	
}



function changeTopicsBak2(event) {	
	if (recipeEvents[event.selectedIndex - 1]) {
		populateTopics(recipeEvents[event.selectedIndex - 1].topics);
	}
}

/*
 * Add topics to topic list
 */
function changeTopics(event) {
	
	var topicElement = getElementById("topic");
	clearElement(topicElement);
	if (recipeEvents[event.selectedIndex - 1]) {
		var topics = recipeEvents[event.selectedIndex - 1].topics;
		createOption('', '', topicElement);
		for ( var i = 0; i < topics.length; i++) {
			createOption(topics[i], topics[i], topicElement);
		}
	}

}

function populateTopics(topics) {
	var topicElement = getElementById("topic");
  	clearElement(topicElement);  	 	
	createOption('', '', topicElement);
	for ( var i = 0; i < topics.length; i++) {
		createOption(topics[i], topics[i], topicElement);
	}
  		
}


function showBasicSearch () {
	  document.forms.recipeSearch.reset();
	  clearForm();
	  $('.recipe-basic-search-buttons').show(); 
	  $('#advanced-search').hide();
}

function showAdvancedSearch () {
	  $('.recipe-basic-search-buttons').hide(); 
	  	$('#advanced-search').show();
}

function doLargePic( _image ){
	 
    var popWin = window.open('','prevWin','width=509,height=333');
    var msg= "<HTML>";
    msg += "<HEAD>";
    msg += "<TITLE>Image Viewer</TITLE></HEAD>";
    msg += "<BODY marginheight='0' marginwidth='0' leftmargin='0' topmargin='0' bgcolor='#cccccc'>";
    msg += "<table cellpadding='2' cellspacing='0'><tr height='330'>";
    msg += "<td width='510' valign='middle' align='center'><table cellpadding='0' cellspacing='0' border='1' bordercolor='#000000' bgcolor='#000000'><tr><td><img name='image01' src='" + _image + "' border='0' vspace='0' hspace='0' width='400'></td></tr></table></td></tr></table><scrip" + "t";
    msg += " language='javascript'>self.focus();</scrip" + "t>";
    msg += "</BODY>";
    msg += "</HTML>";
    popWin.document.write(msg);
    popWin.document.close();
    
    return;
}


