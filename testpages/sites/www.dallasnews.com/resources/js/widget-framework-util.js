// some general purpose javascript functions

function getElementById(elementId) {
  if (document.getElementById) { // DOM3 = IE5, NS6
    return document.getElementById(elementId);
  }
  else if (document.layers) { // Netscape 4
    return document.elementId;
  }
  else { // IE 4
    return document.all.elementId;
  }
}

function initializeMap(divId, latitudes, longitudes, labels, addresses) {
  var count = latitudes.length;
  var i;

  if (GBrowserIsCompatible() && count > 0) {
    var map = new GMap2(document.getElementById(divId));
    var center = new GLatLng(latitudes[0], longitudes[0]);

    if (count == 1) {
      map.setCenter(center, 12);
    } else {
      map.setCenter(center, 1);
    }

    var topRight = new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(5, 5));
    var topLeft = new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(5, 5));

    map.addControl(new GMapTypeControl(), topLeft);
    map.addControl(new GSmallMapControl(), topRight);

    for (i = 0; i < count; i++) {
      var marker = createGMapMarker(map, latitudes[i], longitudes[i], labels[i], addresses[i]);
      map.addOverlay(marker);
    }
  }
}

function createGMapMarker(map, latitude, longitude, label, address) {
  var location = new GLatLng(latitude, longitude);
  var marker = new GMarker(location);

  GEvent.addListener(marker, 'click', function() {
    var info = "";
    if (label != null && label != "") {
      info += "<b>" + label + "</b>";
    }
    if (address != null && address != "") {
      info += "<br/>Address: " + address;
    }

    if (info != "") {
      map.openInfoWindowHtml(location, info);
    }
  });
  return marker;
}
function showWidgetOnClick(elementId){
  $('#widget-div-main-'+elementId).click(function(e){
    if(configWidgetShowingPopupId != null){
      if(configWidgetShowingPopupId != 'widget-div-popup-'+elementId)  {
        $('#'+configWidgetShowingPopupId).fadeOut(1500);
      }
    }
    if(configWidgetSelectedMainDivId != null){
      if(configWidgetSelectedMainDivId != 'widget-div-main-'+elementId)  {
        $('#'+configWidgetSelectedMainDivId).css({background:'darkgray'});
      }
    }

    configWidgetShowingPopupId = 'widget-div-popup-'+elementId;
    configWidgetSelectedMainDivId = 'widget-div-main-'+elementId;
    var height = $('#widget-div-popup-'+elementId).height();
    var width = $('#widget-div-popup-'+elementId).width();
    leftVal=e.pageX+"px";
    topVal=e.pageY+"px";
    $('#widget-div-main-'+elementId).css({background:'cadetblue'});
    $('#widget-div-popup-'+elementId).css({left:leftVal,top:topVal}).show();
  });
}
function hideWidget(wigetId){
  configWidgetShowingPopupId = null;
  configWidgetSelectedMainDivId = null;
  $('#widget-div-popup-'+wigetId).fadeOut(1500);
  $('#widget-div-main-'+wigetId).css({background:'darkgray'});
}