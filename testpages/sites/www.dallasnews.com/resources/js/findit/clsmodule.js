// Autos
function sendForm(sentForm)
{
	var zipHold=eval("document.getElementById('"+sentForm+"').zc.value");
	if (zipHold.length<5 || isNaN(zipHold))
	{
		alert("Please enter a valid ZIP code")
		return false;
	}
	switch(sentForm)
	{
		case "usedForm":
			var d = document.getElementById('usedForm');
			if(location.href.indexOf("research.cars.com")!=-1)
			{
				d.action='http://www.cars.com/go/search/search.jsp;jsessionid=4UUQBFZZV1EIXLAZGKME2UY';
			}
			else
			{
				d.action='http://www.cars.com/go/search/search.jsp;jsessionid=4UUQBFZZV1EIXLAZGKME2UY';
			}
			document.getElementById('usedForm').method='get';
			break;
		case "newForm":
			var d = document.getElementById('newForm');
			d.mdnm.value=d.newModelSelect.options[d.newModelSelect.selectedIndex].text;
			d.mdid.value=d.newModelSelect.options[d.newModelSelect.selectedIndex].value;
			d.mknm.value=d.newMakeSelect.options[d.newMakeSelect.selectedIndex].text;
			d.action='http://www.cars.com/go/search/search.jsp'
			d.method='get';
			break;
	}
	return true;
}

function submitForm(sentForm)
{
	if (!sendForm(sentForm))
	{
		return false;
	}
	return true;
}

function submitUsedForm()
{
	if(submitForm('usedForm'))
		document.getElementById('usedForm').submit();
	else
		return false;
}

function submitNewForm()
{
	if(submitForm('newForm'))
		document.getElementById('newForm').submit();
	else
		return false;
}

// homes
function openCityList(html,winname,ht,wd)
{
	 // convert all characters to lowercase to simplify testing
	var agt=navigator.userAgent.toLowerCase();

	// *** BROWSER VERSION ***
	// Note: On IE5, these return 4, so use is_ie5up to detect IE5.
	var is_major = parseInt(navigator.appVersion);
	var is_minor = parseFloat(navigator.appVersion);

	// Note: Opera and WebTV spoof Navigator.  We do strict client detection.
	// If you want to allow spoofing, take out the tests for opera and webtv.
	var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
		&& (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
		&& (agt.indexOf('webtv')==-1));
	var is_nav3 = (is_nav && (is_major == 3));
	var is_nav4up = (is_nav && (is_major >= 4));
	var is_navonly = (is_nav && ((agt.indexOf(";nav") != -1) || (agt.indexOf("; nav") != -1)) );
	var is_ie   = (agt.indexOf("msie") != -1);
	var is_ie3  = (is_ie && (is_major < 4));
	var is_ie4  = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")==-1) );
	var is_ie4up  = (is_ie  && (is_major >= 4));
	var is_ie5  = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
	var is_ie5up  = (is_ie  && !is_ie3 && !is_ie4);

	// KNOWN BUG: On AOL4, returns false if IE3 is embedded browser
	// or if this is the first browser window opened.  Thus the
	// variables is_aol, is_aol3, and is_aol4 aren't 100% reliable.
	var is_aol   = (agt.indexOf("aol") != -1);
	var is_aol3  = (is_aol && is_ie3);
	var is_aol4  = (is_aol && is_ie4);

	var attr = "height=" + ht + ",width=" + wd + ",scrollbars=no,resizable=no,menubar=no,locationbar=no,toolbar=no,statusbar=no";

	if (is_nav3 || is_nav4up)
	{
		win1 = window.open(html,winname,attr)
		win1.focus()
	}
	  else if (is_ie4up)
	{
		win1 = window.open(html,winname,attr)
		win1.focus()
  	}
	else
	{
		win1 = window.open(html,winname,attr);
	}
	if (!win1.opener)
	{
	 	win1.opener = self;
	}
}

function validate_required(field,alerttxt)
{
	with (field)
	{
		if (value==null||value==0)
	  	{
			alert(alerttxt);return false;
		}
		else
		{
			return true
		}
	}
}
function validate_form(thisform)
{
	with (thisform)
	{
		if (validate_required(geo_area_text,"Please type in a city, neighborhood or zip")==false)
	  	{
			geo_area_text.focus();
			return false;
		}
	}
}
