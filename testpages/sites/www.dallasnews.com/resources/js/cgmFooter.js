if (window.isIE)
{	
	if ($("#loginHeader")[0]) $(window).load(function(){loadLoginHeader();});
	if ($("#slcgmlogincontainer")[0]) $(window).load(function(){loadCommentsLoginHeader();});
	if (cgmArticleIds.length > 0) $(window).load(belo.cgm.util.renderMetaData(cgmArticleIds));
	if (cgmPageToolCommentCount.length > 0) $(window).load(belo.cgm.util.renderCommentCount(cgmPageToolCommentCount));
	if (cgmPageToolRecommendCount.length > 0) $(window).load(belo.cgm.util.renderRecommendCount(cgmPageToolRecommendCount));
}
else
{
	if ($("#loginHeader")[0]) $(document).ready(function(){loadLoginHeader();});
	if ($("#slcgmlogincontainer")[0]) $(document).ready(function(){loadCommentsLoginHeader();});
	if (cgmArticleIds.length > 0) $(document).ready(belo.cgm.util.renderMetaData(cgmArticleIds));
	if (cgmPageToolCommentCount.length > 0) $(document).ready(belo.cgm.util.renderCommentCount(cgmPageToolCommentCount));
	if (cgmPageToolRecommendCount.length > 0) $(document).ready(belo.cgm.util.renderRecommendCount(cgmPageToolRecommendCount));
}