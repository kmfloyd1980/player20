/*
 * Returns a new XMLHttpRequest object, or false if this browser
 * doesn't support it
 */
function newXMLHttpRequest() {

  var xmlreq = null;
  if (window.XMLHttpRequest) {
    // Create XMLHttpRequest object in non-Microsoft browsers
    xmlreq = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    // Create XMLHttpRequest via MS ActiveX
    try {
      // Try to create XMLHttpRequest in later versions
      // of Internet Explorer
      xmlreq = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e1) {
      // Failed to create required ActiveXObject
      try {
        // Try version supported by older versions
        // of Internet Explorer
        xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {
        // Unable to create an XMLHttpRequest with ActiveX
      }
    }
  }
  return xmlreq;
}

/*
 * Get XML node value
 * @param root - parent node
 * @param tagName - node name
 */
function getElementValue(xmlDoc, tagName) {
	var ele = xmlDoc.getElementsByTagName(tagName);
	if (ele != null && ele.length > 0 && ele[0].firstChild != null) {
		return ele[0].firstChild.nodeValue;
	} else {
		return "";
	}			
}	

function getReadyStateHandler(req, responseXmlHandler) {
	// Return an anonymous function that listens to the 
	// XMLHttpRequest instance
	return function () {

		// If the request's status is "complete"
		if (req.readyState == 4) {
			// Check that a successful server response was received
			if (req.status == 200) {				
				if (req.responseXML != null && req.responseXML.childNodes.length > 0) {
					// Pass the XML payload of the response to the handler function
					responseXmlHandler(req.responseXML);
				} else {
					responseXmlHandler(req.responseText);
				}								
			} else {
				// An HTTP problem has occurred
				alert("HTTP error: "+req.status);
			}
		}
	}
}

/*
 * Create named element to handle special case for IE
 * @param type - element tag
 * @param name - name property
 */
function createNamedElement(type, name) {
  var element;
  try { //for IE
    element = document.createElement("<" + type+ " name='" + name + "'>");
  } catch (e) { }
  if (!element || !element.name) { // for FF 
    element = document.createElement(type)
    element.name = name;
  }
  return element;
}
