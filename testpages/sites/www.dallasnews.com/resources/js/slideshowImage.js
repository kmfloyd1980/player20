function switchImage(ssId,ssPictureId) {
	var pictures = (eval("ss_" + ssId + "_pictures")),
		prevPicture = null,
		currPicture = null,
		currPosition = 0,
		nextPicture = null,
		htmlTags = /<\/?[^>]+?>/g,
		brTag = /\s*<br ?\/?>\s*/g;
	for (var picture in pictures.items) {
		if (currPicture != null) {
			nextPicture = pictures.items[picture];
			break;
		}
		currPosition += 1;
		if (parseInt(pictures.items[picture].id) == parseInt(ssPictureId)) {
			currPicture = pictures.items[picture];
		} else {
			prevPicture = pictures.items[picture];
		}
	}
	if (currPicture) {
		if ($("#ss" + ssId)[0]) {
			// slideshow index
			$("#ss" + ssId + " .ssBodyContentImageTd").html('<img alt="' + currPicture.caption.replace(brTag,' ').replace(htmlTags,'') + '" src="' + currPicture.src + '" id="ssBodyImg">');
			$("#ss" + ssId + " #ssBodyContentCaption").html(currPicture.caption);
			$("#ss" + ssId + " #ssBodyContentCredit").html(currPicture.credit);

			var length = parseInt(pictures.length);
			$("#ss" + ssId + " #ssBodyTopCounts").html(currPosition + "/" + length);

			$("#ss" + ssId + " #ssBodyTopPrev").html( prevPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + prevPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-arrowleft.png"></a>' : "&nbsp;");
			$("#ss" + ssId + " #ssBodyTopNext").html( nextPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + nextPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-arrowright.png"></a>' : "&nbsp;");
		}
		if ($("#ssStory" + ssId)[0]) {
			// story lead display
			$("#ssStory" + ssId + " #ssStoryContentImgTd").html('<img alt="' + currPicture.caption.replace(brTag,' ').replace(htmlTags,'') + '" src="' + currPicture.src + '" id="ssStoryImg">');
			$("#ssStory" + ssId + " #ssStoryContentCaption").html(currPicture.caption);
			$("#ssStory" + ssId + " #ssStoryContentCredit").html(currPicture.credit);

			var length = parseInt(pictures.length);
			$("#ssStory" + ssId + " #ssStoryCountCur").html(currPosition);
			$("#ssStory" + ssId + " #ssStoryCountTot").html(length);

			$("#ssStory" + ssId + " #ssStoryCountPrev").html( prevPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + prevPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-left.png"></a>' : "&nbsp;");
			$("#ssStory" + ssId + " #ssStoryCountNext").html( nextPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + nextPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-right.png"></a>' : "&nbsp;");
		}
		if ($("#ssIndex" + ssId)[0]) {
			// story lead display
			$("#ssIndex" + ssId + " #ssIndexImageSrc").attr('src',currPicture.src);
			$("#ssIndex" + ssId + " #ssIndexContentCaption").html(currPicture.caption);
			$("#ssIndex" + ssId + " #ssIndexContentCredit").html(currPicture.credit);

			var length = parseInt(pictures.length);
			$("#ssIndex" + ssId + " #ssIndexCountCur").html(currPosition);
			$("#ssIndex" + ssId + " #ssIndexCountTot").html(length);

			$("#ssIndex" + ssId + " #ssIndexCountPrev").html( prevPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + prevPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-left.png"></a>' : "&nbsp;");
			$("#ssIndex" + ssId + " #ssIndexCountNext").html( nextPicture ? '<a href="#" onClick="switchImage(' + ssId + ',' + nextPicture.id + ');return false;"><img src="/resources/images/slideshow/ss-right.png"></a>' : "&nbsp;");
		}
	}
}