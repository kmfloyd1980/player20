function DMNValidateSearchForm(objSearchForm)
{
	
	var publicationId = objSearchForm.publicationId.value;
	var includeSectionId = objSearchForm.includeSectionId.value;
	var searchNumber = objSearchForm.DMNSearchDD.selectedIndex;
	var searchUrl = objSearchForm.DMNSearchDD.value;
 	var searchText = objSearchForm.DMNSearchText.value;
 	var words = searchText.split(" ");
 	searchText = words.join("+");

	switch (searchNumber)
	{
		case 0:
		{
			var queryString = "&engine=solr&search_type=site-search-basic&articleTypeFacets=news,bcVideo,gallery,recipe,cars,shopping&pageLength=10&dateFacets=1,7,30&searchString=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 1:
		{
			var queryString = "&searchString=" + searchText + "&search_type=web&pageLength=10&pageNumber=1&engine=web";
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 2:
		{

			var queryString = "?s=" + searchText + "&advid=0&loc=0";
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 3:
		{
			var queryString = "?REGION=dm&s_hidethis=no&search_mode=basic&action=search&search_text=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}		
		case 4:
		{
			var queryString = "?p_product=DMEC&p_theme=histpaper&p_action=search&p_text_base=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 5:
		{
			var queryString = "?REGION=dmob&s_hidethis=no&search_mode=basic&action=search&search_text=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 6:
		{
			var queryString = "?REGION=dmnbb&s_hidethis=no&search_mode=basic&action=search&search_text=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 7:
		{
			var queryString = "&engine=solr&search_type=recipe-search&articleTypes=recipe&pageLength=10&searchString=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 8:
		{
			var queryString = "&searchString=" + searchText + "&search_type=smedia&pageLength=10&pageNumber=1&engine=smedia";
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 9:
		{
			var queryString = "?keyword=" + searchText;
			document.location.href = searchUrl + queryString;
			return false;
		}
		case 10:
		{
			var queryString = "&searchString=" + searchText  + "&search_type=event&pageLength=10&pageNumber=1&engine=event";;
			document.location.href = searchUrl + queryString;
			return false;
		}
	} 	
}
