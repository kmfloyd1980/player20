<?php

/**
 * Compiles "*.less" files; example usage shown below:
 * 
 * Include the following in your <head></head> tags of a webpage:
 * <link href="../_build/lessc.php?path=/less/NdnEmbed.less" rel="stylesheet" type="text/css" />
 * 
 * The above will include the compiled CSS for the *.less file found within this repository at "/less/NdnEmbed.less".
 * 
 * IMPORTANT NOTE:
 * If you are on a Mac and this is not working, make sure that you have executed the following on the command line from the application's base url:
 * chmod -R +x ./_build/Less.app/Contents/Resources/engines/bin
 */

$binPath = __DIR__ . '/Less.app/Contents/Resources/engines/bin';

// The path to the 'lessc' executable
$lessc = __DIR__ . '/Less.app/Contents/Resources/engines/bin/lessc';

// The application's base folder path
$appBasePath = dirname(__DIR__);

// The filepath to the source *.less file to be compiled
$source = $appBasePath . '/' . trim(isset($_GET['path']) ? $_GET['path'] : '', '/');

header('Content-type: text/css');

/**
echo "Lessc: $lessc<br/>";
echo "Source: $source"; exit;
/**/

$tempTarget = __DIR__ . '/' . md5(time());

exec("chmod -R +x $binPath");
$result = `PATH="\$PATH:$binPath";$lessc --line-numbers=comments $source 2>&1`;

// $result = `echo 'hello'`;

// $result = file_get_contents($tempTarget);

//unlink($tempTarget);

echo $result;

