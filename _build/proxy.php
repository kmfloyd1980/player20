<?php

/*
echo "This is the file: " . $_GET['url'] . '<br/>';
echo 'strpos == ' . strpos($_GET['url'], '%');

exit;
*/

$imageUrl = $_GET['url'];

$ch = curl_init();
$source = $imageUrl;
curl_setopt($ch, CURLOPT_URL, $source);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec($ch);
curl_close($ch);

$fileExtension = pathinfo($imageUrl, PATHINFO_EXTENSION);

$mimeTypes = array(
	'jpeg' => 'image/jpeg',
	'jpg' => 'image/jpeg',
	'gif' => 'image/gif',
	'png' => 'image/png',
);

if (isset($mimeTypes[$fileExtension])) {
	header('Content-type: ' . $mimeTypes[$fileExtension]);
}

echo $data;

exit;

/*
if (basename($_GET['pic']) == $_GET['pic']) {
	$pic = $image_folder.$_GET['pic'];
	if (file_exists($pic) && is_readable($pic)) {
		// get the filename extension
		$ext = substr($pic, -3);
		// set the MIME type
		switch ($ext) {
			case 'jpg':
				$mime = 'image/jpeg';
				break;
			case 'gif':
				$mime = 'image/gif';
				break;
			case 'png':
				$mime = 'image/png';
				break;
			default:
				$mime = false;
		}
		// if a valid MIME type exists, display the image
		// by sending appropriate headers and streaming the file
		if ($mime) {
			header('Content-type: '.$mime);
			header('Content-length: '.filesize($pic));
			$file = @ fopen($pic, 'rb');
			if ($file) {
				fpassthru($file);
				exit;
			}
		}
	}
}
*/

/*
$destination = "/asubfolder/afile.zip";
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);
*/