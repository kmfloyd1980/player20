<?php
	function baseUrl($url) {
		if (isset($_GET['build'])) {
			return 'http://qa.launch.newsinc.com/' . $_GET['build'] . $url;
		}
		else {
			return '/5/js/embed.js' . (isset($_GET['appUrl']) ? "?appUrl={$_GET['appUrl']}" : ''); // '' . $url . '.php?baseUrl=';
			// return 'http://dev.launch.newsinc.com/js/embed.js';
		}
	}
?>
<!doctype html>
<html>
<head>
	<script type="text/javascript" src="<?= baseUrl('/js/embed.js'); ?>"></script>
	
    <script type="text/javascript">
    	var _nw2e = _nw2e || [];
    	_nw2e.push(['hook', 'ndn-widget-embed-2/videoEnd', function() {
    		alert('videoEnd detected!!! (ndn-widget-embed-2)');
    	}]);
    	_nw2e.push(['hook', '*/videoPause', function(event) {
        	console.log(event);
        	alert('Video was paused (at ' + event.data.currentTime + ' seconds) for the player ' +
                'embedded within the <div> element with id="' + event.containerElement.id + '"!!');
    	}]);
    </script>
</head>
<body>
	<style>
		body {
			padding: 10px;
		}
	
    	.mainContainer {
    		/* border: 2px solid #333; */
    		
    		border-style: dashed;
    		border-color: #ddd;
    		border-width: 0 0 1px 0;
    		
    		padding: 10px;
    		width: 90%;
    		clear: both;
    	}
    	
    	.mainContainer > strong > code {
    		color: #000;
    		border: 0;
    		background: #ddd;
    	}
    	
    	.mainContainer > strong {
    		color: #333;
    		display: block;
    		padding: 8px 4px 16px 4px;
			font-family: Helvetica, Arial;
			font-size: 12px;
    	}
    	
    	.ndn_embedContainer.ndn_widget_VideoPlayer-16x9 {
    		width: 600px;
    		height: 610px;
    	}
    	
    	.ndn_embedContainer.ndn_widget_VideoPlayer-Default {
    		width: 640px;
    		height: 341px;
    	}
    </style>
    <?php
		$types = array(
			'VideoPlayer/Single',
			'VideoPlayer/Inline300',
			'VideoPlayer/Inline590',
			'VideoLauncher/Horizontal',
			'VideoLauncher/Horizontal',
			'VideoLauncher/Playlist300x250',
			'VideoLauncher/Slider300x250',
			'VideoLauncher/VerticalTower',
			// 'VideoPlayer/16x9',
			/**/
		);

		$widgetId = 3233;
		
		/*
		$configData = array(
			'widgetId' => '3233',
			'playlistId' => '10205',
			'autoPlay' => '',
			'trackingGroup' => '34567',
		);
		*/ 
		?><div style="font-family:Helvetica,Arial;font-size:14px;padding:0 0 0 14px;"><h3>NDN Player Suite, Embed Examples</h3></div><?php

		/**/
		foreach ($types as $type) {
			if (!$type) continue;
			?>
			    <div class="mainContainer">
			    	<strong>Using <code>&lt;div data-config-widget-id="<?= $widgetId ?>" data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
			    	<div class="ndn_embed" data-config-widget-id="<?= $widgetId++ ?>" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id2="10205" data-config-video-id2="25050005000000" data-config-auto-play="" data-config-lessc="dynamic"></div>
				    <div style="clear:both;"></div>
			    </div>
			<?php
		}
		/**/
    ?>
    
    <?php /**/ 
    	// $type = 'VideoPlayer/Single';
    	// $type = 'VideoLauncher/Horizontal'; 
    	$type = 'VideoLauncher/VerticalTower';
	    // $type = 'VideoPlayer/Inline590';
	/**/
	?>
	<div style="margin: 0 auto;">
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" data-config-widget-id="3233" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="10205" data-config-auto-play="" data-config-ads-enabled=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" data-config-widget-id="3233" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="994" data-config-auto-play="" data-config-ads-enabled=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" data-config-widget-id="4" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="" data-config-auto-play=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    
	    <?php $type = 'VideoLauncher/Horizontal'; ?>
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" style="width:420px;" data-config-widget-id="3233" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="10205" data-config-auto-play="" data-config-ads-enabled=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" style="width:425px;" data-config-widget-id="3233" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="10205" data-config-auto-play="" data-config-ads-enabled=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    <div class="mainContainer">
	    	<strong>Using <code>&lt;div data-config-widget-type="<?= $type ?>" ...&gt;&lt;/div&gt;</code></strong>
	    	<div class="ndn_embed" style="width:550px;" data-config-widget-id="3233" data-config-type="<?= $type ?>" data-config-tracking-group="34567" data-config-playlist-id="10205" data-config-auto-play=""></div>
		    <div style="clear:both;"></div>
	    </div>
	    
	    <?php /* Iframe Examples * ?>
	    <div class="mainContainer">
	    	<iframe style="width:300px;height:180px;" src="http://local.launch.newsinc.com/?widgetId=3233&type=VideoPlayer/Single&trackingGroup=34567&playlistId=10205&autoPlay=&adsEnabled="></iframe>
	    </div>
	    <div class="mainContainer">
	    	<iframe style="width:125px;height:545px;" src="http://local.launch.newsinc.com/?widgetId=3233&type=VideoLauncher/VerticalTower&trackingGroup=34567&playlistId=10205&autoPlay=&adsEnabled="></iframe>
	    </div>
	    <?php /**/ ?>
	    
	    <iframe style="width:550px;height:140px;" src="http://local.launch.newsinc.com/?widgetId=3237&type=VideoLauncher/Horizontal&trackingGroup=34567"></iframe>
	</div>
</body>
</html>
    