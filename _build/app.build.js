({
    appDir: "../",
    baseUrl: "js",
    dir: "../../../Builds/media-widgets",
    optimizeAllPluginResources: true,
    findNestedDependencies: true,
    paths: {
	    addThis: "empty:"
    },
    modules: [
              
          {
        	  name: "main"
          }
          /*
          {
        	  name: "players_core/fullplayerwvars"
          },
          {
        	  name: "players_core/playlist16x9_player"
          },
          {
        	  name: "players_core/single"
          },
          {
        	  name: "players_core"
          }
          */
    ]
})