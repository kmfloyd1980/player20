define([
    'jquery',
    'models/Ndn/ChromeExt'
], function(
    $,
    Ndn_ChromeExt
) {
    var Ndn_ChromeExt_SandboxAmp = (function() {
    	/**
    	 * @return {jQuery.Deferred} Resolves with the URL where the Akamai Media Player sandbox application is located
    	 */
    	var _getSandboxUrl = function() {
    	    return $.Deferred(function(deferred) {
    	        Ndn_ChromeExt.getExtensionOptions().done(function(options) {
    	            console.log('options ==', options);
    	            
    	            deferred.resolve('http://' + (options.sandbox_env == 'dev' ? 'localhost' : 'developers.newsinc.com') + '/sandbox/amp/');
    	        });
    	    }).promise();
    	};
    	
    	return {
    		sandbox: function(containerElementId) {
    		    _getSandboxUrl().done(function(ampSandboxUrl) {
    		        console.log('ampSandboxUrl ==', ampSandboxUrl);
    		        
    		        Ndn_ChromeExt.sendRealRequestToContentScript({requestDebugInfo: containerElementId})
                    .done(function(debugInfo) {
                        debugInfo = debugInfo || {};
                        
                        $.ajax({
                            url: ampSandboxUrl,
                            type: 'POST',
                            contentType: 'application/x-www-form-urlencoded',
                            dataType: 'json',
                            data: {
                                extensionVersion: Ndn_ChromeExt.getVersion(),
                                ampVersion: debugInfo.ampVersion || '',
                                playerConfig: debugInfo.playerConfig ? JSON.stringify(debugInfo.playerConfig) : '',
                                playerConfigDefaults: debugInfo.playerConfigDefaults ? JSON.stringify(debugInfo.playerConfigDefaults) : '',
                                videosLoaded: (function() {
                                    var result;
                                    
                                    if (typeof debugInfo.videosLoaded != 'undefined') {
                                        result = [];
                                        
                                        for (var i = 0; i < debugInfo.videosLoaded.length; i++) {
                                            result[i] = result[i] || [];
                                            
                                            for (var j = 0; j < debugInfo.videosLoaded[i].length; j++) {
                                                result[i][j] = result[i][j] || [];
                                                result[i][j][0] = debugInfo.videosLoaded[i][j][0];
        
                                                // Encode the parameters of load instructions into JSON strings
                                                for (var k = 1; k < debugInfo.videosLoaded[i][j].length; k++) {
                                                    result[i][j][k] = JSON.stringify(debugInfo.videosLoaded[i][j][k]);
                                                }
                                            }
                                        }
                                    }
                                    
                                    return result;
                                })()
                            }
                        })
                        .done(function(response) {
                            console.log('The response from that ajax call ==', response);
                            
                            Ndn_ChromeExt.openUrlInAdjacentTab(ampSandboxUrl + response.responseId);
                        });
                    });
    		    });
    		}
    	};
    })();
    
    return Ndn_ChromeExt_SandboxAmp;
});