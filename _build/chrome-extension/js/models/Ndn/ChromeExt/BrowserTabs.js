define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/ChromeExt/BrowserTab'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_ChromeExt_BrowserTab
) {
    var Ndn_ChromeExt_BrowserTabs = Ndn_Utils_Base.extend({
    	constructor: function() {
    		/**
    		 * Structured data concerning each tab of the Chrome browser that has interacted with the Chrome Extension. Format:
    		 * <pre>{
    		 * 	int: Ndn_ChromeExt_BrowserTab, // The tab ID => the associated browser tab object
    		 * 	... // All other tabs
    		 * }</pre>
    		 * @var {Object}
    		 */
    		this._tabs = {};
    	},
    	
    	getTab: function(tabId) {
    		this._tabs[tabId] = this._tabs[tabId] || (new Ndn_ChromeExt_BrowserTab(tabId));
    		return this._tabs[tabId];
    	},
    	
    	/**
    	 * @return {Object} Structured data concerning this browser tab
    	 */
    	getData: function() {
    		// TODO: Code this?
    	},
    	
    	registerEmbed: function(tabId, embedData) {
    	    this.getTab(tabId).registerEmbed(embedData);
    	},
    	
    	unregisterEmbed: function(tabId, containerElementId) {
    	    this.getTab(tabId).unregisterEmbed(containerElementId);
    	},
    	
    	getDebugLogs: function() {
    		var result = {
    			'__debugLog': debugLog
    		};
    		
    		$.each(this._tabs, function(tabId, tab) {
    			result[tabId] = tab.getData().debugLog;
    		});
    		
    		return result;
    	}
    });
    
    return Ndn_ChromeExt_BrowserTabs;
});