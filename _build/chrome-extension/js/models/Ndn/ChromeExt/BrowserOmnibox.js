define([
    'models/Ndn/ChromeExt',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt/SandboxProduct'
], function(
    Ndn_ChromeExt,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt_SandboxProduct
) {
    var Ndn_ChromeExt_BrowserOmnibox = Ndn_Utils_Base.extend({
    	constructor: function() {
    		this.initializeEventListeners();
    	},
    	
    	initializeEventListeners: function() {
    		// When the user's input to the browser's omnibox changes
    		chrome.omnibox.onInputChanged.addListener(function(text, suggest) {
    			suggest([
    		         {content: 'guide', description: 'Implementation Guide'}
    		    ]);
    		});
    		
    		// When the user submits input from the browser's omnibox
    		chrome.omnibox.onInputEntered.addListener(function(text) {
    			var matches;
    			if (matches = text.match(/^sandbox\s*(.*)\s*$/i)) {
    			    Ndn_ChromeExt_SandboxProduct.getSandboxUrl({
                        embedCode: matches[1]
                    }).done(function(url) {
    			        Ndn_ChromeExt.openUrlInCurrentTab(url);
    			    });
    			}
    			else if (matches = text.match(/^launch\s*(.*)\s*$/i)) {
    				try {
    					Ndn_ChromeExt.openUrlInCurrentTab(Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrlFromJsEmbedCode(matches[1]));
    				}
    				catch (e) {
    					alert("The embed code you provided is not valid: " + e);
    				}
    			}
    			else if (text == 'guide') {
    				Ndn_ChromeExt.openUrlInCurrentTab("http://assets.newsinc.com/ndn-player-suite/implementation/index.html");
    			}
    		});
    	}
    });
    
    return Ndn_ChromeExt_BrowserOmnibox;
});