var embeds = embeds || {};

define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt
) {
    var Ndn_ChromeExt_BrowserTab = Ndn_Utils_Base.extend({
    	/**
    	 * @param {int} tabId
    	 */
    	constructor: function(tabId) {
    		/**
    		 * The browser tab's ID
    		 * @var {int}
    		 */
    		this._tabId = tabId;
    		
    		/**
    		 * Structured data concerning this browser tab. Format:
    		 * <pre>{
    		 * 	'debugLog': {String}, // Html formatted log statements used for debugging
    		 * 	'hasHardcodedBuildNumber': {Boolean}, // Whether this tab has detected the use of an NDN Player Suite <script> element with a hardcoded build number
    		 * 	'legacyMigrationAddedEmbedJs': {Boolean}, // Whether this tab has detected the use of NDN Player Suite forced migration that adds its own embed.js file (this can misleadingly trigger the "hasHardcodedBuildNumber" property to be true)
    		 * }</pre>
    		 * @var {Object}
    		 */
    		this._data = null;
    		
    		this.initialize();
    	},
    	
    	/**
    	 * Reset the stored data of this tab to its original state
    	 */
    	initialize: function() {
    	    this._data = {
                debugLog: '',
                hasHardcodedBuildNumber: false,
                legacyMigrationAddedEmbedJs: false,
                
                // TODO: Refactor this; this data is getting reset once the "complete" event is thrown for a page loading, however, before that event is thrown the loading of the page sets these values already and should not be reset in this manner.
                topWindowOrigin: this._data ? this._data.topWindowOrigin || '' : '',
                topWindowHref: this._data ? this._data.topWindowHref || '' : '',
                mainWindowOrigin: this._data ? this._data.mainWindowOrigin || '' : ''
            };
            
            // this.log('this._data == ' + JSON.stringify(this._data));
            
            // Reset any structured data regarding embedded products for a browser tab with this browser tab ID
            embeds[this._tabId] = {}; // TODO: Refactor out this GLOBAL variable!
            
            // Initiate the detection of embedded products
            this.detectEmbeds();
    	},
    	
    	/**
    	 * Send a message to the content script to detect embeds on the page
    	 */
    	detectEmbeds: function() {
    	    // console.log('[NDN Player Suite Chrome Extension] Ndn_ChromeExt_BrowserTab.detectEmbeds();');
    	    
    	    Ndn_ChromeExt.echoClosure($.proxy(function() {
                chrome.tabs.sendMessage(this._tabId, {getEmbeds: true}, function() {});
    	    }, this));
    	},
    	
    	/**
    	 * @param {Object} data Structured data to override this browser tab's existing data with
    	 * ---
    	 * @param {String} propertyName
    	 * @param {unknown_type} propertyValue
    	 */
    	update: function() {
    	    // console.log('Ndn_ChromeExt_BrowserTab.update(', arguments, '); Tab ID: ', this._tabId);
    		// this.log('BrowserTab.update(' + JSON.stringify(arguments) + ');');
    		
    		if (arguments.length == 2) {
    			var propertyName = arguments[0],
    				propertyValue = arguments[1];
    			this._data[propertyName] = propertyValue;
    		}
    		else {
    			this._data = $.extend({}, this._data, arguments[0]);
    		}
    		
    		// console.log('>> Result: ', this._data, '; Tab ID: ', this._tabId);
    	},
    	
    	/**
    	 * @param {String} str Text to log, which is associated with this browser tab
    	 */
    	log: function(str) {
    		this._data.debugLog += '<div>' + str + '</div>';
    	},
    
    	/**
    	 * @param {Object} embedData Structured data concerning an embedded product. Format:
    	 * <pre>{
    	 * 	'container': {
    	 * 		'width': {int}, // The container element's width measured in pixels
    	 * 		'height': {int} // The container element's height measured in pixels
    	 * 	},
    	 * 	'isIframe': {Boolean}, // Whether this embedded product is contained within an <iframe> element
    	 * 	'containerId': {String}, // The container element's "id" attribute
    	 * 	'initialConfig': {Object}, // Structured data concerning this embedded product's provided config settings
    	 * 	'config': {Object}, // Structured data concerning the embedded product's resulting config settings
    	 * 	'appUrl': {String}, // The application's URL
    	 * 	'appDomain': {String}, // The application's domain name
    	 * 	'debugInfo': { // Structured data concerning data that may be used for debugging
    	 * 		'amp': {
    	 * 			'version': {String}, // The version number of this NDN AMP Premier's build (ie. "1.0.0000")
    	 * 			'configDefaults': {Object}, // Structured data concerning the config defaults loaded into the Akamai Media Player library singleton instance
    	 * 			'configInit': {Object}, // Structured data concerning the config passed into the Akamai Media Player on top of its "config defaults"
    	 * 			'loadedVideos': {Array<Array>}, // Structured data concerning the videos loaded into this widget's instance of its Akamai Media Player video player
    	 * 		}
    	 * 	}
    	 * }</pre>
    	 */
    	registerEmbed: function(embedData) {
    		// this.log('Embed Data == ' + JSON.stringify(embedData));
    		
    		// TODO: Refactor out the use of this GLOBAL variable "embeds"!
    		embeds[this._tabId] = embeds[this._tabId] || {};
    		embeds[this._tabId][embedData.containerId] = embedData;
    	},
    	
    	/**
    	 * @param {String} containerElementId The "id" attribute of the configurable <div> element of the embedded product to unregister for this browser tab
    	 */
    	unregisterEmbed: function(containerElementId) {
    		embeds[this._tabId] = embeds[this._tabId] || {};
    		delete embeds[this._tabId][containerElementId];
    	},
    	
    	/**
    	 * @return {Object} Structured data concerning the embedded products within the webpage of this browser tab
    	 */
    	getEmbeddedProductsData: function() {
    		// TODO: REFACTOR OUT THE "embeds" GLOBAL VARIABLE!
    		return embeds[this._tabId] || {};
    	},
    	
    	/**
    	 * @return {int} The number of embeds registered for this browser tab
    	 */
    	getNumberOfEmbeds: function() {
    		var numberOfEmbeds = 0;
    		$.each(this.getEmbeddedProductsData(), function() {
    			numberOfEmbeds++;
    		});
    		return numberOfEmbeds;
    	},
    	
    	/**
    	 * @return {Array<String>} List of player components identifiers that have custom overridden environments. Recognized player component identifiers: "analytics", "playerServices", "perfectPixel"
    	 */
    	getOverriddenEnvs: function() {
    		var overriddenEnvs = {};
    		$.each(this.getEmbeddedProductsData(), function(containerId, productData) {
    			$.each(productData.envOverrides || {}, function(componentId, envName) {
    				if (envName) {
    					overriddenEnvs[componentId] = true;
    				}
    			});
    		});
    		
    		var result = [];
    		$.each(overriddenEnvs, function(componentId) {
    			result.push(componentId);
    		});
    		return result;
    	},
    
    	/**
    	 * @return {int} The number of embeds registered for this browser tab that are from non-production code
    	 */
    	hasNonProductionEmbeds: function() {
    		var nonProductionEmbedsCounter = 0;
    		$.each(this.getEmbeddedProductsData(), function(containerId, productData) {
    			// If the application URL for this embedded product did not come from production, increment the number of non-production embeds detected
    			if (Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(productData.appUrl).env != '') {
    				nonProductionEmbedsCounter++;
    			}
    		});
    		return nonProductionEmbedsCounter > 0;
    	},
    	
    	hasNonProductionEnvOverrides: function() {
    		var envOverridesCounter = 0;
    		$.each(this.getEmbeddedProductsData(), function(containerId, productData) {
    			// If the application URL for this embedded product did not come from production, increment the number of non-production embeds detected
    			if (Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(productData.appUrl).env != '') {
    				envOverridesCounter++;
    			}
    		});
    		return envOverridesCounter > 0;
    	},
    	
    	hasOverriddenAppSettings: function() {
    	    // console.log('(1234) >>>>>>> hasOverriddenAppSettings(); JUST CALLED!!');
    	    
            var result = false;
            
            try {
                // console.log('(1234) this.getEmbeddedProductsData() ==', this.getEmbeddedProductsData());
                
                $.each(this.getEmbeddedProductsData(), function(containerId, productData) {
                    // console.log('(1234) productData ==', productData);
                    
                    if (productData.appSettings) {
                        $.each(productData.appSettings['used'], function(settingName, settingValue) {
                            if (productData.appSettings['default'][settingName] !== settingValue) {
                                result = true;
                                return false;
                            }
                        });
                    }
                });
            }
            catch (e) {
                console.log('(1234) hasOverriddenAppSettings(), EXCEPTION FOUND: e ==', e);
            }
            
            // console.log('(1234) result ==', result);
            
            return result;
    	},
    	
    	/**
    	 * @return {Object} Structured data concerning this browser tab
    	 */
    	getData: function() {
            // console.log(' *** >>>>>>> Ndn_ChromeExt_BrowserTab.getData(); JUST CALLED!!');
            
            try {
                var result = $.extend({
                    tabId: this._tabId,
                    embeddedProductsData: this.getEmbeddedProductsData(),
                    overriddenEnvs: this.getOverriddenEnvs(),
                    hasOverriddenAppSettings: this.hasOverriddenAppSettings()
                }, this._data);
            }
            catch (e) {
                console.log("EXCEPTION FOUND: e ==", e);
            }
    		
    		// console.log('getData(); result ==', result);
    		
    		return result;
    	}
    });
    
    return Ndn_ChromeExt_BrowserTab;
});