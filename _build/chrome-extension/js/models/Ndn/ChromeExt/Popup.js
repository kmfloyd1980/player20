define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt',
    'models/Ndn/ChromeExt/Popup/EmbeddedProducts',
    'models/Ndn/ChromeExt/SandboxProduct'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt,
    Ndn_ChromeExt_Popup_EmbeddedProducts,
    Ndn_ChromeExt_SandboxProduct
) {
    var Ndn_ChromeExt_Popup = (function() {
    	/**
    	 * Whether the user is currently holding the "Alt" key down on their keyboard
    	 * @var {Boolean}
    	 */
    	var _isPressingAltKey = false;
    	
    	return Ndn_Utils_Base.extend({
    		constructor: function() {
    			// console.log('embeds ==', chrome.extension.getBackgroundPage().getEmbeds());
    			
    			/**
    			 * The browser tab ID of the browser tab this popup is associated with
    			 * @var {int}
    			 */
    			this._tabId = chrome.extension.getBackgroundPage().selectedTabId;
    			
    			/**
    			 * @var {Ndn_ChromeExt_Popup_EmbeddedProducts}
    			 */
    			this._embeddedProducts;
    			
    			/**
    			 * This connects communication between background.js and popup.js
    			 * TODO: Document this further.
    			 * @var {chrome.extension.connect}
    			 */
    			// this._port = chrome.runtime.connect({name: 'Popup_' + this.getTabId()});
    			
    			/**
    			 * Structured data concerning this Chrome Extension's options
    			 * @var {Object}
    			 */
    			this._extensionOptions;
    			
    			Ndn_ChromeExt.getExtensionOptions().done($.proxy(function(options) {
    				this._extensionOptions = options;
    				
    				this._embeddedProducts = new Ndn_ChromeExt_Popup_EmbeddedProducts(this);
    
    				this.initializeDom();
    				this.initializeEventListeners();
    
    				// Ensure that the embedded products on the page are detected and then displayed. Synchronize the 
    				// interface immediately, but also at the specified attempt intervals listed below.
    				var attemptIntervals = [1, 2, 4, 8],
    					self = this;
    				this.synchronizeInterface();
    				$.each(attemptIntervals, function(index, seconds) {
    					setTimeout(function() {
    						self.synchronizeInterface();
    					}, seconds * 1000);
    				});
    			}, this));
    		},
    		
    		getExtensionOptions: function() {
    			return this._extensionOptions;
    		},
    		
    		/**
    		 * @return {int} The browser tab ID of the browser tab this popup is associated with
    		 */
    		getTabId: function() {
    			return this._tabId;
    		},
    		
    		initializeDom: function() {
    			var html = this._dom = {
    				container: $('body'),
    				createEmbedCodeContainer: null
    			};
    			
    			html.createEmbedCodeLink =
    			$('.createEmbedCodeLink');
    			
    			html.createEmbedCodeContainer =
    			$('.createEmbedCodeContainer').hide();
    			
    			html.createEmbedCodeContainer.find('legend a').bind('click', function() {
    				html.createEmbedCodeContainer.hide();
    				return false;
    			});
    			
    			// Hide these two containers by default
    			$('#enterEmbedCodeContainer').hide();
    			$('.createEmbedCodeContainer').hide();
    			
    			$('.ndn_versionNumber').html('v' + Ndn_ChromeExt.getVersion());
    			
    			this.synchronizeEmbedCodeCreator();
    		},
    		
    		synchronizeInterface: function() {
    		    // console.log('---> >> Ndn_ChromeExt_Popup.synchronizeInterface();');
    		    
    			var self = this;
    			
    			Ndn_ChromeExt.sendRequestToContentScript({getEmbeds: true});
    			Ndn_ChromeExt.sendRequestToContentScript({detectHardcodedBuildNumbers: true});
    			
    			Ndn_ChromeExt.getCurrentTabData().done(function(tabData) {
    			    // console.log('tabData ==', tabData);
    			    
    				var warningsHtml = '';
    				var showHardcodedBuildNumberWarning = tabData.hasHardcodedBuildNumber && !tabData.legacyMigrationAddedEmbedJs;
    				if (showHardcodedBuildNumberWarning) {
    					warningsHtml += '<div class="warning message"><b class="warning-label">Warning:</b> This page may not be using the latest version of the NDN Player Suite. Either a <span class="code">&lt;script&gt;</span> or <span class="code">&lt;iframe&gt;</span> element was detected with a hardcoded build number, which must be removed in order to receive the latest version automatically.</div>';
    				}
    				
    				// warningsHtml += '<div class="warning message">' + JSON.stringify(tabData) + '</div>';
    				
    				if (tabData.hasOverriddenAppSettings) {
                        warningsHtml += '<div class="warning message"><b class="warning-label">Warning:</b> Default application settings have been overridden for one or more application builds detected on this page.</div>';
    				}
    				
    				if (tabData.overriddenEnvs.length) {
    					var overriddenComponentsHtml = (function() {
    						var componentLabels = {
    							playerServices: 'Player Services',
    							analytics: 'Analytics',
    							perfectPixel: 'Perfect Pixel'
    						};
    						var result = [];
    						$.each(tabData.overriddenEnvs, function(i, componentId) {
    							result.push('<span class="component-label">' + componentLabels[componentId] + '</span>');
    						});
    						return result.join(', ');
    					})();
    					warningsHtml += '<div class="warning message"><b class="warning-label">Warning:</b> Embedded product(s) on this page currently override the production environment for the following components: ' + overriddenComponentsHtml + '.</div>';
    				}
    				
    				self._dom.container.find('.messages').html(warningsHtml);
    				
    				// console.log('tabData ==', tabData);
    				
    				// $('#debugMessages').html(tabData.debugLog);
    			});
    			
    			this._embeddedProducts.synchronizeInterface();
    		},
    	
    		/**
    		 * Synchronize the embed code creator part of the interface
    		 */
    		synchronizeEmbedCodeCreator: function() {
    			// Get the <option> element that holds information pertaining to how this option is configurable using data-* attributes
    			var optionElement = $('#typeConfigSetting :selected');
    			
    			var config = Ndn_Utils_PlayerSuiteApp.getConfigFromElement(optionElement, false);
    			config.type = $('#typeConfigSetting').val();
    			
    			var hideSettingsList = (optionElement.attr('data-hide-settings') || '').split(',');
    			
    			// Show only the config settings available for the selected "type" config setting
    			$('.createEmbedCodeContainer .formItem').hide();
    			$.each(config, function(configSettingName, defaultValue) {
    				var containerElement = $('.createEmbedCodeContainer .' + configSettingName + 'ConfigSettingContainer');
    				
    				// If this config setting is not supposed to be explicitly hidden from the user, reveal it to the user
    				if ($.inArray(configSettingName, hideSettingsList) == -1) {
    					containerElement.show();
    				}
    				containerElement.find('input').val(defaultValue);
    			});
    		},
    		
    		initializeEventListeners: function() {
    			var html = this._dom;
    	
    			// Event listeners to track whether the user is holding down the "Alt" key on the keyboard
    			$(document).keydown(function(e) {
    				if (e.keyCode == 18) {
    					_isPressingAltKey = true;
    					
    					$('.verb').html('Sandbox');
    				}
    			})
    			.keyup(function(e) {
    				if (e.keyCode == 18) {
    					_isPressingAltKey = false;
    					
    					$('.verb').html('Launch');
    				}
    			});
    			
    			// Not the best way to trigger this, but hey.
    			$('.launchAllProducts').on('click', {}, function() {
    				if (_isPressingAltKey) {
    					_mostRecentlyCreatedSandboxHtml = [];
    					$('.embeddedProducts > .embeddedProduct').trigger('getSandboxHtml');
    					
    					Ndn_ChromeExt_SandboxProduct.sandbox({
    					    embedCode: _mostRecentlyCreatedSandboxHtml.join("\n\n")
    					});
    				}
    				else {
    					// Launch each product in its own tab
    					$($('.launchProductContainer > a.active').get().reverse()).trigger('click');
    				}
    			});
    			
    			html.createEmbedCodeLink.on('click', function(event) {
    				if (!html.createEmbedCodeContainer.is(':visible')) {
    					html.createEmbedCodeContainer.show();
    				}
    				else {
    					// Get the <option> element that holds information pertaining to how this option is configurable using data-* attributes
    					var optionElement = $('#typeConfigSetting :selected');
    					
    					// Get the config from the form fields
    					var config = (function() {
    						var config = Ndn_Utils_PlayerSuiteApp.getConfigFromElement(optionElement, false);
    						var result = {};
    						$.each(config, function(index, value) {
    							result[index] = value;
    						});
    						
    						return result;
    					})();
    					
    					config['type'] = $("#typeConfigSetting").val();
    					
    					$.each(config, function(configSettingName, defaultValue) {
    						if (configSettingName != 'type') {
    							var containerElement = $('.createEmbedCodeContainer .' + configSettingName + 'ConfigSettingContainer');
    							config[configSettingName] = containerElement.find('input').val();
    						}
    					});
    					
    					var result = Ndn_Utils_PlayerSuiteApp.getJsEmbedCodeFromConfig(config, 'local-launch.newsinc.com', {
    						container: {
    							width: config.width || '',
    							height: config.height || ''
    						}
    					});
    					
    					// console.log('resulting from createEmbedCode() ==', result);
    					
    					$('#enterEmbedCode').val(result);
    					$('#enterEmbedCodeContainer').show();
    					$('#enterEmbedCode').select().focus();
    				}
    			});
    			
    			$('.embeddedProducts').on('click', '.embeddedProduct .description', function(event) {
    				$(event.currentTarget).toggleClass('active');
    				$(event.currentTarget).parent().find('.options').toggle();
    				
    				event.preventDefault();
    			});
    	
    			// Event listener for the extensions features
    			$('.extensionFeatures .launchEmbedCodeLink').on('click', function(event) {
    				if (!$('#enterEmbedCodeContainer').is(':visible')) {
    					$('#enterEmbedCodeContainer').show();
    					$('#enterEmbedCodeContainer').find('.error').hide();
    				}
    				else {
    					try {
    						/*
    						// Renaming "style" attribute to "data-style" to workaround Chrome Extension's Content Security Policy directive: "style-src 'self'"
    						var embedCode = $('#enterEmbedCode').val().replace(/style=/g, "data-style="),
    							configurableDiv = $(embedCode),
    							config = Ndn_Utils_PlayerSuiteApp.getConfigFromElement(configurableDiv, true, true),
    							url = Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl(config);
    						*/
    	
    						// Hide any previous error messages
    						$('#enterEmbedCodeContainer').find('.error').hide();
    						
    						Ndn_ChromeExt.openUrlInAdjacentTab(Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrlFromJsEmbedCode($('#enterEmbedCode').val()));
    					}
    					catch(e) {
    						$('#enterEmbedCodeContainer').find('.error')
    						.html(e + '')
    						.show();
    					}
    				}
    				
    				event.preventDefault();
    			});
    			
    			// Event listener for the extensions features
                $('.extensionFeatures .viewBuilds').on('click', function(event) {
                    Ndn_ChromeExt.openUrlInAdjacentTab('http://developers.newsinc.com/sandbox/ndn-player-suite/build-details/?internalUseOnly&env=prod');
                    
                    event.preventDefault();
                });
    			
    			$('#typeConfigSetting').on('change', this.synchronizeEmbedCodeCreator);
    			
    
    			// console.log('chrome.runtime.connect in POPUP.JS <<<<<<<<<<<<');
    			
    			/* TODO: ADD THIS BACK IN???
    			var port = this._port;
    			port.onMessage.addListener(function(msg) {
    				console.log('popup.js RECEIVED MESSAGE FROM background.js: msg ==', msg);
    			});
    			port.postMessage(this.getTabId());
    			*/
    			
    			
    			// console.log('chrome.runtime.connect END in POPUP.JS <<<<<<<<<<<<');
    		}
    	},
    	{
    		detectEmbeds: function() {
    			Ndn_ChromeExt.sendRequestToContentScript({getEmbeds: true});
    		},
    		
    		/**
    		 * @return {Boolean} Whether the user is currently holding the "Alt" key pressed down on their keyboard
    		 */
    		isPressingAltKey: function() {
    			return _isPressingAltKey;
    		}
    	});
    })();
    
    return Ndn_ChromeExt_Popup;
});