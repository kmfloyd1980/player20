define([
    'jquery',
    'models/Ndn/ChromeExt'
], function(
    $,
    Ndn_ChromeExt
) {
    return {
	    /**
         * @param {Object} data Structured data concerning what kind of sandbox scenario should be created. Format:
         * <pre>{
         *    embedCode: {String}, // The HTML embed code to sandbox for a specific environment
         *    envName: {String}, // Optional. The name of the environment to launch this product to (defaults to "", which means production); sample values: "dev", "dev2", "qa4"
         *    buildNumber: {String}, // Optional. The environment build number, such as "102" for "dev2-launch.newsinc.com/102" or "" for "dev2-launch.newsinc.com" (defaults to "")
         *    protocol: {String}, // Optional. The protocol of the environment to launch to; recognized values: "http:", "https:", or "" (for "//dev2-launch.newsinc.com" for example); (defaults to "http:")
         * }</pre>
         * @return {jQuery.Deferred} Resolves with the URL to visit in order to view the specified sandbox scenario
         */
        getSandboxUrl: function(data) {
            return $.Deferred(function(deferred) {
                Ndn_ChromeExt.getExtensionOptions().done(function(options) {
                    var sandboxBaseUrl = 'http://' + (options.sandbox_env == 'dev' ? 'localhost-sb' : 'developers.newsinc-sb.com') + '/sandbox/ndn-player-suite/',
                        embedCode = data.embedCode || '',
                        envName = data.envName || '',
                        buildNumber = data.buildNumber || '',
                        protocol = data.protocol || 'http:',
                        forwardedGetParams = data.forwardedGetParams || '';
                    
                    var numberOfDefinedParams = (function() {
                        var result = 0;
                        $.each(data, function() {
                            result++;
                        });
                        return result;
                    })();
                    
                    var params = (embedCode || numberOfDefinedParams > 1) 
                        ? '?' + $.param({
                            includejs: 1,
                            env: envName,
                            build: buildNumber,
                            protocol: protocol,
                            forwardedGetParams: encodeURIComponent(forwardedGetParams),
                            embedCode: embedCode
                        })
                        : '';
                    
                    deferred.resolve(sandboxBaseUrl + params);
                });
            }).promise();
        },
		
		/**
		 * @param {String} embedCode The HTML embed code to sandbox for a specific environment
		 * @param {String} [envName] The name of the environment to launch this product to (defaults to "", which means production); sample values: "dev", "dev2", "qa4"
		 * @param {String} [buildNumber] The environment build number, such as "102" for "dev2-launch.newsinc.com/102" or "" for "dev2-launch.newsinc.com" (defaults to "")
		 * @param {String} [protocol] The protocol of the environment to launch to; recognized values: "http:", "https:", or "" (for "//dev2-launch.newsinc.com" for example); (defaults to "http:")
		 */
		sandbox: function(data) {
            this.getSandboxUrl(data).done(function(url) {
    			Ndn_ChromeExt.openUrlInAdjacentTab(url);
            });
		}
	};
});