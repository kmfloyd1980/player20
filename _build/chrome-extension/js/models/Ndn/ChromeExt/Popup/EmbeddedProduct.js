define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/LaunchProduct',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/GetEmbedCode',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/ShowConfig',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/Features',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/Environments',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/AppSettings',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/DebugModes',
    'models/Ndn/ChromeExt/SandboxAmp'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt,
    Ndn_ChromeExt_Popup_EmbeddedProduct_LaunchProduct,
    Ndn_ChromeExt_Popup_EmbeddedProduct_GetEmbedCode,
    Ndn_ChromeExt_Popup_EmbeddedProduct_ShowConfig,
    Ndn_ChromeExt_Popup_EmbeddedProduct_Features,
    Ndn_ChromeExt_Popup_EmbeddedProduct_Environments,
    Ndn_ChromeExt_Popup_EmbeddedProduct_AppSettings,
    Ndn_ChromeExt_Popup_EmbeddedProduct_DebugModes,
    Ndn_ChromeExt_SandboxAmp
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct = Ndn_Utils_Base.extend({
    	constructor: function(productData, popup) {
    	    console.log('Ndn_ChromeExt_Popup_EmbeddedProduct(', arguments, ');');
    	    
    		/**
    		 * @var {Ndn_ChromeExt_Popup}
    		 */
    		this._popup = popup;
    		
    		/**
    		 * Structured data concerning the important dom elements that make up this embedded product's interface
    		 * @var {Object}
    		 */
    		this._dom = {};
    		
    		/**
    		 * Structured data concerning this embedded product
    		 * @var {Object}
    		 */
    		this._productData = productData;
    	},
    	
    	getPopup: function() {
    		return this._popup;
    	},
    	
    	getProductDescriptionLabel: function() {
    		return this._productData.containerId + (this._productData.isIframe ? ' <i>(iframe)</i>' : '') + 
    			"<div class=\"configType\">" + (this._productData.initialConfig.type || '<i>Unknown</i>') + "</div>";
    	},
    	
    	getData: function() {
    		return this._productData;
    	},
    	
    	initializeDom: function() {
    		var html =
    		this._dom = {
    			container: null,
    			optionsContainer: null,
    			embedCodeContainer: null
    		};
    		
    		var productData = this._productData;
    		
    		
    		
    		html.container =
    		$(document.createElement('div'))
    		.attr('data-container-id', productData.containerId)
    		.addClass('embeddedProduct')
    		.html('<div class="description">' + this.getProductDescriptionLabel() + "</div>");
    	
    		html.container
    		.append(
    			html.optionsContainer =
    			$(document.createElement('div'))
    			.addClass('options')
    			.append(new Ndn_ChromeExt_Popup_EmbeddedProduct_LaunchProduct(this))
                .append(new Ndn_ChromeExt_Popup_EmbeddedProduct_Environments(this))
    			.append(new Ndn_ChromeExt_Popup_EmbeddedProduct_ShowConfig(this))
                .append(new Ndn_ChromeExt_Popup_EmbeddedProduct_AppSettings(this))
                .append(new Ndn_ChromeExt_Popup_EmbeddedProduct_DebugModes(this))
                .append(new Ndn_ChromeExt_Popup_EmbeddedProduct_GetEmbedCode(this))
    			.append(new Ndn_ChromeExt_Popup_EmbeddedProduct_Features(this))
    			.hide()
    		);
    	},
    	
    	initializeEventListeners: function() {
    		// console.log('Ndn_ChromeExt_Popup_EmbeddedProduct.initializeEventListeners();');
    		
    		var html = this._dom,
    			productData = this._productData,
    			self = this;
    		
    		html.container.on('getSandboxHtml', {}, function() {
    			// TODO: Refactor out the usage of this GLOBAL variable ("_mostRecentlyCreatedSandboxHtml"), bad bad bad.
    			_mostRecentlyCreatedSandboxHtml.push(self.getSandboxHtml());
    		});
    	},
    	
    	/**
    	 * @return {String} The HTML describing this embedded product's embed code
    	 */
    	getSandboxHtml: function() {
    		var productData = this._productData;
    		
    		return "<h5><em>" + productData.containerId + "</em> (" + productData.initialConfig.type + ")</h5>" + 
    			"\n" + Ndn_Utils_PlayerSuiteApp.getEmbedCode(productData.isIframe ? 'iframe' : 'js', Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(productData.appUrl).envName, productData) + "\n<br />";
    	},
    	
    	synchronizeInterface: function() {
    		// console.log('Ndn_ChromeExt_Popup_EmbeddedProduct.synchronizeInterface();');
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct;
});