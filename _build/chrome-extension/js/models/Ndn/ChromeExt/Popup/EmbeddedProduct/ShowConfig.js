define([
    'jquery',
    'models/Ndn/Utils/Base',
    
    'lib/jquery/plugins/jquery.jsonPresenter'
], function(
    $,
    Ndn_Utils_Base
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_ShowConfig = Ndn_Utils_Base.extend({
        constructor: function(embeddedProduct) {
            /**
             * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
             */
            this._embeddedProduct = embeddedProduct;
        },
        
        initializeDom: function() {
            var configSettingsToHideForNonDevelopers = [
                'acceptsConfigFromUrl',
                'acceptsConfigFromUrlPrefix'
            ];
            
            var html =
                this._dom = {
                    container: null
                },
                productData = this._embeddedProduct.getData();
            
            // console.log('productData ==', productData);
            
            this._dom.container =
            $(document.createElement('div'))
            .addClass('showConfigContainer')
            .append('Show config: <a href="#" data-type="provided">provided</a><a href="#" data-type="used">used</a><a href="#" data-type="settings">settings</a>')
            .append(
                html.providedConfigContainer =
                $(document.createElement('div'))
                .addClass('providedConfigContainer')
                .append('<p class="header">Provided Config:</p>')
                .hide()
            )
            .append(
                html.usedConfigContainer =
                $(document.createElement('div'))
                .addClass('usedConfigContainer')
                .append('<p class="header">Used Config:</p>')
                .hide()
            )
            .append(
                html.settingsContainer =
                $(document.createElement('div'))
                .addClass('usedConfigContainer')
                .append('<p class="header">Settings:</p>')
                .append(
                    html.jsonContainer = 
                    $('<div class="json">')
                    /*.on('click', function() {
                        this
                    })
                    */
                )
                .hide()
            );
            
            var role = this._embeddedProduct.getPopup().getExtensionOptions().role;
    
            var configSettingsToListAtTop = ['type', 'splitVersion'];
            
            var listConfigSettings = function(containerElement, omitConfigSettings) {
                omitConfigSettings = omitConfigSettings || {};
                
                return function(configSetting, configValue) {
                    // If this config setting is not supposed to be listed, do not display it
                    if ($.inArray(configSetting, omitConfigSettings) !== -1) return;
                    
                    // If this config setting should be hidden from any role except the "Developer" role, do not display it
                    if (role != 'dev' && $.inArray(configSetting, configSettingsToHideForNonDevelopers) !== -1) return;
                    
                    containerElement.append(
                        $(document.createElement('p'))
                        .html('<b>' + configSetting + ':</b> ' + configValue)
                    );
                };
            };
            
            var countProperties = function(obj) {
                var result = 0;
                $.each(obj, function() {
                    result++;
                });
                return result;
            }
            
            var displayConfigSettings = function(configData, containerElement) {
                // List the config settings to be placed on top appropriately
                var configSettingsOnTop = {};
                $.each(configSettingsToListAtTop, function() {
                    var configSetting = this;
                    if (typeof configData[configSetting] != 'undefined') {
                        configSettingsOnTop[configSetting] = configData[configSetting];
                    }
                });
                
                // If there are config settings to list at the top, do so
                if (countProperties(configSettingsOnTop)) {
                    var topConfigSettingsContainer = $('<div class="topConfigSettingsContainer">');
                    containerElement.append(topConfigSettingsContainer);
                    $.each(configSettingsOnTop, listConfigSettings(topConfigSettingsContainer));
                    
                    /*
                    // If there are remaining config properties to be listed, add a divide in the interface to set the top config settings apart
                    if (countProperties(configSettingsOnTop) < countProperties(configData)) {
                        containerElement.append('<div class="ndn_smallDivide"></div>');
                    }
                    */
                }
    
                // List the remaining config settings
                $.each(configData, listConfigSettings(containerElement, configSettingsToListAtTop));
            };
            
            // Generate the "provided" config table
            displayConfigSettings(productData.initialConfig, html.providedConfigContainer);
    
            // Generate the "used" config table
            displayConfigSettings(productData.config, html.usedConfigContainer);
        },
        
        initializeEventListeners: function() {
            // console.log('ShowConfig.initializeEventListeners();');
            
            var html = this._dom,
                productData = this._embeddedProduct.getData();
    
            html.container
            .on('click', 'a[data-type]', function(event) {
                if ($(this).attr('data-type') == 'provided') {
                    html.providedConfigContainer.toggle();
                    $(this).toggleClass('active');
                }
                else if ($(this).attr('data-type') == 'used') {
                    html.usedConfigContainer.toggle();
                    $(this).toggleClass('active');
                }
                else if ($(this).attr('data-type') == 'settings') {
                    html.settingsContainer.toggle();
                    $(this).toggleClass('active');
                    if (!html.jsonContainer.html()) {
                        html.jsonContainer.jsonPresenter({json: productData.settings.settings})
                    }
                }
                
                event.preventDefault();
            });
        }
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_ShowConfig;
});