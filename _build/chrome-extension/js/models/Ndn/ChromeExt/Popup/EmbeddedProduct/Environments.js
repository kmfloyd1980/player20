define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_Environments = Ndn_Utils_Base.extend({
    	constructor: function(embeddedProduct) {
    		/**
    		 * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
    		 */
    		this._embeddedProduct = embeddedProduct;
    		
    		/**
    		 * Whether the overrides should be displayed
    		 * @var {Boolean}
    		 */
    		this._showOverrides = false;
    		
    		/**
    		 * Whether all components should have their environments displayed
    		 * @var {Boolean}
    		 */
    		this._showAll = false;
    	},
    	
    	initializeDom: function() {
    		var html =
    			this._dom = {
    				container: null
    			},
    			productData = this._embeddedProduct.getData();
    		
    		this._dom.container =
    		$(document.createElement('div'))
    		.addClass('showConfigContainer environmentsContainer')
    		// .append('<b class="containerLabel">Environment Overrides:</b> <a href="#" data-type="overrides">show overrides</a><a href="#" data-type="all">show all</a>')
    		.append('<b class="containerLabel">Environments:</b> <a href="#" data-type="all">show overrides</a>')
    		.append(
    			html.envsContainer =
    			$(document.createElement('div'))
    			.addClass('envsContainer')
    			.append('<p class="header"></p>')
    			.hide()
    		);
    		
    		var self = this,
    			embedData = this._embeddedProduct.getData(),
    			embedEnvsData = embedData.envOverrides || {},
    			currentEnvName = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(embedData.appUrl + '').envName,
    			envsData = [
    	            // ['Player Suite', currentEnvName],
    	            ['Analytics', embedEnvsData.analytics],
    	            ['Player Services', embedEnvsData.playerServices],
    	            ['Perfect Pixel', embedEnvsData.perfectPixel]
    	        ];
    		
    		var hasEnvOverride = false;
    		$.each(envsData, function(index) {
    			var componentLabel = this[0],
    				componentEnvName = this[1],
    				isOverride = !!componentEnvName,
    				componentEnvDisplayLabel = isOverride ? componentEnvName : 'production';
    			
    			// If an override is detected, store that there exists at least this one environment override
    			if (isOverride) hasEnvOverride = true;
    			
    			html.envsContainer.append(
    				$('<p class="componentEnvName"><b>' + componentLabel + ':</b> <span class="envDisplayLabel">' + componentEnvDisplayLabel + '</span></p>')
    				.toggleClass('isOverride', isOverride)
    				.toggleClass('isProduction', !isOverride)
    				// .toggle(isOverride)
    			);
    		});
    		
    		var role = this._embeddedProduct.getPopup().getExtensionOptions().role;
    
    		// html.container.find('> .noOverrides').toggle(!hasEnvOverride);
    		// html.container.find('> .option').toggle(hasEnvOverride);
    		
    		if (!hasEnvOverride) {
    			html.container.hide();
    		}
    	},
    	
    	initializeEventListeners: function() {
    		// console.log('ShowConfig.initializeEventListeners();');
    		
    		var html = this._dom,
    			productData = this._embeddedProduct.getData(),
    			self = this;
    
    		html.container
    		.on('click', 'a[data-type]', function(event) {
    			if ($(this).attr('data-type') == 'overrides') {
    				if (self._showOverrides) {
    					self._showOverrides =
    					self._showAll = false;
    				}
    				else {
    					self._showOverrides = true;
    				}
    				
    				/*
    				$(this).toggleClass('active');
    				html.envsContainer.toggle($(this).hasClass('active'));
    				*/
    			}
    			else if ($(this).attr('data-type') == 'all') {
    				if (self._showAll) {
    					self._showAll = false;
    				}
    				else {
    					self._showOverrides =
    					self._showAll = true;
    				}
    				
    				/*
    				$(this).toggleClass('active');
    				$(this).parent().find('a[data-type="overrides"]').toggleClass('active', $(this).hasClass('active'));
    				html.envsContainer.toggle($(this).hasClass('active'));
    				
    				if ($(this).hasClass('active')) {
    					html.envsContainer.find('> p').show();
    				}
    				else {
    					html.envsContainer.find('> p:not(.isOverride)').hide();
    				}
    				*/
    			}
    			
    			self.synchronizeInterface();
    			
    			return false;
    		});
    	},
    	
    	synchronizeInterface: function() {
    		console.log('Environments, synchronizeInterface(); this._showAll ==', this._showAll, 'this._showOverrides ==', this._showOverrides);
    		
    		var html = this._dom;
    		
    		html.container.find('p.header').html(this._showAll ? 'Environments Used:' : 'Environments Overridden:');
    
    		html.container.find('a[data-type="overrides"]').toggleClass('active', this._showOverrides);
    		html.container.find('a[data-type="all"]').toggleClass('active', this._showAll);
    		
    		// html.envsContainer.find('> p.componentEnvName:not(.isOverride)').toggle(this._showAll);
    		
    		// html.envsContainer.toggle(this._showOverrides || this._showAll);
    		html.envsContainer.toggle(this._showAll);
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_Environments;
});