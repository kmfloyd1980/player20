define([
    'jquery',
    'underscore',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt'
], function(
    $,
    _,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_LaunchProduct = Ndn_Utils_Base.extend({
    	constructor: function(embeddedProduct) {
    		// console.log('Ndn_ChromeExt_Popup_EmbeddedProduct_LaunchProduct.constructor(', arguments, ');');
    		
    		/**
    		 * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
    		 */
    		this._embeddedProduct = embeddedProduct;
    	},
    	
    	initializeDom: function() {
    		var html =
    			this._dom = {
    				container: null
    			};
    		
    		this._dom.container =
    		$(document.createElement('div'))
    		.addClass('launchProductContainer')
    		.append('<span class="verb">Launch</span> product: ');
    		
    		// console.log('this._dom ==', this._dom);
    		
    		// Generate the "Launch"/"Sandbox" environment choices
    		var getEnvLabel = function(env, lane, build) {
    			return (env ? env : 'production') + (lane ? lane : '') + (build ? ' #' + build : '');
    		};
    		
    		var appUrl = this._embeddedProduct.getData().appUrl + '',
    			envData = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(appUrl),
    			widgetEnv = envData.env,
    			widgetEnvLane = envData.envLane,
    			widgetBuildNumber = envData.buildNumber,
    			widgetEnvLabel = getEnvLabel(widgetEnv, widgetEnvLane, widgetBuildNumber);
    		
    		var role = this._embeddedProduct.getPopup().getExtensionOptions().role;
    		
    		// console.log('Launch Product: ROLE == ' + role);
    		
    		var envValues = ['', 'staging', 'qa', 'dev']; // , 'local'];
    		if (role == 'dev') {
    			envValues.push('local');
    		}
    		$.each(envValues, function(index, env) {
    			var envChoices = [];
    			
    			if (_.contains(['', 'staging'], env)) {
    				envChoices.push(
      			    {
    			    	label: getEnvLabel(env),
    			    	element: $('<a href="#" data-env="' + env + '">' + getEnvLabel(env) + '</a>')
    			    });
    			}
    			else {
    				envChoices.push({
    			    	label: getEnvLabel(env, widgetEnvLane),
    			    	element: $('<a href="#" data-env="' + env + widgetEnvLane + '">' + getEnvLabel(env, widgetEnvLane) + '</a>')
    			    });
    			}
    			
    			if (env == widgetEnv) {
    				envChoices.push({
    			    	label: getEnvLabel(env, widgetEnvLane, widgetBuildNumber),
    			    	element: $('<a href="#" data-env="' + env + widgetEnvLane + '" data-build="' + widgetBuildNumber + '">' + getEnvLabel(env, widgetEnvLane, widgetBuildNumber) + '</a>')
    			    });
    			}
    			
    			// Changing this value to false allows for the iteration through env/build# choices to halt
    			var continueIteration = true;
    			
    			var addedLabels = [];
    			$.each(envChoices, function(i, envChoice) {
    				// console.log('envChoice.label == ', envChoice.label);
    				
    				// If this environment choice has not been added yet
    				if (!_.contains(addedLabels, envChoice.label)) {
    					// Add the choice to the list, indicating whether it is the one describing the environment the widget is currently in
    					html.container.append(envChoice.element.toggleClass('active', envChoice.label == widgetEnvLabel));
    					
    					// Remember that this choice was added
    					addedLabels.push(envChoice.label);
    					
    					// If this is currently being used by an "Account Manager" role the active env/build# label has just been added, do not continue to list any more env/build# choices
    					if (envChoice.label == widgetEnvLabel && role == 'am') {
    						continueIteration = false;
    						return false;
    					}
    				}
    			});
    			
    			return continueIteration;
    		});
    	},
    	
    	initializeEventListeners: function() {
    		this._dom.container.on('click', '> a', $.proxy(function(event) {
    			var envName = $(event.currentTarget).attr('data-env') + '',
    				envInfo = Ndn_Utils_PlayerSuiteApp.getEnvNameInfo(envName),
    				env = envInfo.env,
    				envLane = envInfo.envLane,
    				buildNumber = $(event.currentTarget).attr('data-build') || '';
    			
    			require([
			        'models/Ndn/ChromeExt/Popup',
			        'models/Ndn/ChromeExt/SandboxProduct'
		        ], $.proxy(function(
	                Ndn_ChromeExt_Popup,
	                Ndn_ChromeExt_SandboxProduct
                ) {
                    if (Ndn_ChromeExt_Popup.isPressingAltKey()) {
                        var sandboxData = {
                            embedCode: this._embeddedProduct.getSandboxHtml(),
                            envName: envName,
                            buildNumber: buildNumber
                        };
                        
                        // If either the "appSettings" or "debugModes" are available, pass them along as well to open the appropriate URL with said settings and modes left intact
                        var forwardedGetParams = '';
                        if (this._embeddedProduct.getData().appSettings) {
                            // Only pass along "appSettings" if there are any overrides present
                            var numberOfAppSettingsOverrides = (function(overrides) {
                                result = 0;
                                $.each(overrides, function() { result++; });
                                return result;
                            })(this._embeddedProduct.getData().appSettings['overrides']);
                            if (numberOfAppSettingsOverrides) {
                                forwardedGetParams += 'ndn_appSettings=' + encodeURIComponent($.param(this._embeddedProduct.getData().appSettings['overrides']));
                            }
                        }
                        if (this._embeddedProduct.getData().isStoringEventTimelineData) {
                            forwardedGetParams += (forwardedGetParams ? '&' : '') + 'ndn_debug';
                            var debugModes = this._embeddedProduct.getData().debugModes;
                            if (debugModes) {
                                // If the debug modes list is not simply a list of 1 value that is the empty string (this should data should not be stored this way)
                                if (debugModes.length != 1 && debugModes[0] == '') {
                                    forwardedGetParams += '=' + encodeURIComponent(debugModes.join(','));
                                }
                            }
                        }
                        if (forwardedGetParams) {
                            sandboxData['forwardedGetParams'] = forwardedGetParams;
                        }
                        
                        Ndn_ChromeExt_SandboxProduct.sandbox(sandboxData);
                    }
                    else {
                        var landingPageUrlData = {
                            envName: envName,
                            buildNumber: buildNumber
                        };
                        
                        // If either the "appSettings" or "debugModes" are available, pass them along as well to open the appropriate landing page URL with said settings and modes left intact
                        if (this._embeddedProduct.getData().appSettings) {
                            // Only pass along "appSettings" if there are any overrides present
                            var numberOfAppSettingsOverrides = (function(overrides) {
                                result = 0;
                                $.each(overrides, function() { result++; });
                                return result;
                            })(this._embeddedProduct.getData().appSettings['overrides']);
                            if (numberOfAppSettingsOverrides) {
                                landingPageUrlData['appSettings'] = this._embeddedProduct.getData().appSettings['overrides'];
                            }
                        }
                        if (this._embeddedProduct.getData().isStoringEventTimelineData) {
                            landingPageUrlData['debugModes'] = this._embeddedProduct.getData().debugModes;
                        }
                        
                        Ndn_ChromeExt.openUrlInAdjacentTab(Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl(this._embeddedProduct.getData().initialConfig, landingPageUrlData));
                    }
    			}, this));
    			
    			event.preventDefault();
    		}, this));
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_LaunchProduct;
});