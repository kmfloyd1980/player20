define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/PlayerSuiteApp'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_Utils_PlayerSuiteApp
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_GetEmbedCode = Ndn_Utils_Base.extend({
    	constructor: function(embeddedProduct) {
    		/**
    		 * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
    		 */
    		this._embeddedProduct = embeddedProduct;
    	},
    	
    	initializeDom: function() {
    		var html =
    			this._dom = {
    				container: null
    			};
    		
    		this._dom.container =
    		$(document.createElement('div'))
    		.addClass('embedCodeContainer')
    		.append('Get embed code: <a href="#" class="js">javascript</a><a href="#" class="iframe">iframe</a>')
    		.append(
    			html.embedCodeResultContainer =
    			$(document.createElement('div'))
    			.append(
    				html.embedCodeResult =
    				$(document.createElement('textarea'))
    			)
    			/*
    			.append(
    				html.embedCodeOptionsContainer =
    				$(document.createElement('div'))
    				.addClass('embedCodeContainerOptions')
    				.append(
    					$('<a href="#" data-env="">production</a>')
    				)
    				.append(
    					$('<a href="#" data-env="staging">staging</a>')
    				)
    				.append(
    					$('<a href="#" data-env="qa">qa</a>')
    				)
    				.append(
    					$('<a href="#" data-env="dev">dev</a>')
    				)
    				.append(
    					$('<a href="#" data-env="local">local</a>')
    				)
    				.append(
    					html.embedCodeContainerClose =
    					$('<a href="#" class="closeLink">close</a>')
    				)
    			)
    			*/
    			.hide()
    		);
    	},
    	
    	initializeEventListeners: function() {
    		// console.log('GetEmbedCode.initializeEventListeners();');
    		
    		var html = this._dom,
    			productData = this._embeddedProduct.getData();
    
    		var _updateEmbedCode = function(appDomain) {
    			var envName = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo('http://' + appDomain).envName;
    			
    			var embedMethod = _getEmbedMethod();
    			
    			var embedCode = Ndn_Utils_PlayerSuiteApp.getEmbedCode(embedMethod, envName, productData);
    			
    			html.embedCodeResult.val(embedCode);
    			
    			// html.embedCodeOptionsContainer.children().removeClass('active');
    			// html.embedCodeOptionsContainer.children('[data-env="' + appDomain + '"]').addClass('active');
    			
    			html.embedCodeResult.select().focus();
    		};
    		
    		var _getEmbedMethod = function() {
    			return html.container.find('> .iframe.active').length ? 'iframe' : 'js';
    		};
    		
    		html.embedCodeResultContainer.on('change', function(event) {
    			_updateEmbedCode(productData.appDomain);
    		});
    		
    		html.container
    		.on('click', '> a', function(event) {
    			var aElement = $(event.currentTarget);
    			
    			if ($(this).hasClass('active')) {
    				$(this).removeClass('active');
    				html.embedCodeResultContainer.hide();
    			}
    			else {
    				// Indicate using the dom what type of embed method is currently selected
    				aElement.parent().children('a').removeClass('active');
    				aElement.addClass('active');
    				
    				html.embedCodeResultContainer.show();
    				_updateEmbedCode(productData.appDomain);
    				
    			}
    			
    			event.preventDefault();
    		})
    		.on('click', '.embedCodeContainer .closeLink', $.proxy(function(event) {
    			html.container.find('> a').removeClass('active');
    			html.embedCodeResultContainer.hide();
    			
    			event.preventDefault();
    		}, this))
    		.on('click', '.embedCodeContainerOptions > a', function(event) {
    			if (!$(event.currentTarget).hasClass('closeLink')) {
    				_updateEmbedCode($(event.currentTarget).attr('data-env'));
    			}
    			event.preventDefault();
    		});
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_GetEmbedCode;
});