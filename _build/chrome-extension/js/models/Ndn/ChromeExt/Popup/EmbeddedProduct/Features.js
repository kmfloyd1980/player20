define([
    'jquery',
    'underscore',
    'models/Ndn/Utils/Base',
    'models/Ndn/Utils/UrlParser',
    'models/Ndn/ChromeExt',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct/Features/EventTimeline',
    'models/Ndn/ChromeExt/SandboxAmp'
], function(
    $,
    _,
    Ndn_Utils_Base,
    Ndn_Utils_UrlParser,
    Ndn_ChromeExt,
    Ndn_ChromeExt_Popup_EmbeddedProduct_Features_EventTimeline,
    Ndn_ChromeExt_SandboxAmp
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_Features = Ndn_Utils_Base.extend({
    	constructor: function(embeddedProduct) {
    		/**
    		 * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
    		 */
    		this._embeddedProduct = embeddedProduct;
    	},
    	
    	initializeDom: function() {
    		var html =
    			this._dom = {
    				container: null
    			};
    		
    		this._dom.container =
    		$(document.createElement('div'))
    		.addClass('featuresContainer popupSectionContainer');
    	},
    	
    	initializeEventListeners: function() {
    		// console.log('Features.initializeEventListeners();');
    		
    		var html = this._dom,
    			productData = this._embeddedProduct.getData();
    
    		html.container
    		.on('click', 'a[data-type="scrollTo"]', $.proxy(function(event) {
    			this.scrollToEmbedLocation();
    			event.preventDefault();
    		}, this))
    		.on('click', 'a[data-type="getDebugInfo"]', $.proxy(function(event) {
    			this.sandboxAmpPlayer();
    			event.preventDefault();
    		}, this))
    		.on('click', 'a[data-type="pause"]', $.proxy(function(event) {
    			// Instruct the content script to pause to this embedded product
    			Ndn_ChromeExt.sendRequestToContentScript({pause: this._embeddedProduct.getData().containerId});
    			event.preventDefault();
    		}, this))
    		.on('click', 'a[data-type="pauseOthers"]', $.proxy(function(event) {
    			// Instruct the content script to pause all other embedded products detected on this page
    			Ndn_ChromeExt.sendRequestToContentScript({pauseOthers: this._embeddedProduct.getData().containerId});
    			event.preventDefault();
    		}, this))
    		.on('click', 'a[data-type="viewTimeline"]', $.proxy(function(event) {
    			var embeddedProduct = this._embeddedProduct,
    				containerId = embeddedProduct.getData().containerId;
    			
    			var linkElement = $(event.currentTarget);
    			
    			this.needsRefreshForTimelineData().done($.proxy(function(needsRefresh) {
    				if (!needsRefresh) {
    					// Request the data pertaining to this embedded product's analytics timeline
    					Ndn_ChromeExt.sendRealRequestToContentScript({'getProductTimelineData?': containerId})
    					.done($.proxy(function(eventsData) {
    						// Indicate that the timeline is being displayed
    						linkElement.toggleClass('active');
    						
    						if (linkElement.hasClass('active')) {
    							this._dom.container.find('.eventTimeline')
    							.html('')
    							.append(new Ndn_ChromeExt_Popup_EmbeddedProduct_Features_EventTimeline(embeddedProduct, eventsData))
    							.show();
    						}
    						else {
    							this._dom.container.find('.eventTimeline').hide();
    						}
    					}, this));
    				}
    				else {
    					Ndn_ChromeExt.getCurrentTabData().done(function(tabData) {
    					    // console.log('Ndn_ChromeExt_Popup_EmbeddedProduct_Features. tabData ==', tabData);
    					    
						    var topWindowHref = tabData.topWindowHref || '',
						        parsedTopWindowHref = Ndn_Utils_UrlParser.parseHref(topWindowHref);
                            
						    // console.log('(((( **** )))))) parsedTopWindowHref ==', parsedTopWindowHref);
						    
						    /**
						     * Whether the "ndn_debug" parameter should be added as a GET parameter or within the #hash as a parameter
						     * @var {Boolean}
						     */
						    var addToQueryParams = (function() {
						        return _.contains(['developers.newsinc.com', 'developers.newsinc-sb.com', 'localhost', 'localhost-sb'], parsedTopWindowHref['domain']) || !!(parsedTopWindowHref['domain'] || '').match(/\.newsinc\.com$/);
						    })();
						    
						    var query = parsedTopWindowHref['query'],
						        hash = parsedTopWindowHref['hash'];
                            
                            // console.log('query ==', query);                                
                            // console.log('hash ==', hash);
                            
                            if (addToQueryParams) {
                                query += (query ? '&' : '') + 'ndn_debug';
                            }
                            else {
                                hash += (hash ? '&' : '') + 'ndn_debug';
                            }
                            
                            var debugModeUrl = parsedTopWindowHref['protocol'] + '//' + parsedTopWindowHref['domain'] + parsedTopWindowHref['path'] +
                                (query ? '?' + query : '') +
                                (hash ? '#' + hash : '');
                            
    						// console.log('debugModeUrl ==', debugModeUrl); return;
    						
    						Ndn_ChromeExt.openUrlInCurrentTab(debugModeUrl).done(function() {
    						    // Force the tab to be reloaded if just changing the hash, as doing so does not reload the page
    							if (!addToQueryParams) {
    							    Ndn_ChromeExt.reloadCurrentTab();
    							}
    							
                                // Close the popup window
                                window.close();
    						});
    					});
    				}
    			}, this));
    			
    			event.preventDefault();
    		}, this));
    	},
    	
    	/**
    	 * @return {jQuery.Deferred} Resolves with boolean parameter indicating whether a refresh is required in order to retrieve timeline data
    	 */
    	needsRefreshForTimelineData: function() {
    		return $.Deferred($.proxy(function(deferred) {
    		    /*
    			Ndn_ChromeExt.getCurrentTabData().done(function(tabData) {
    				deferred.resolve(!Ndn_Utils_UrlParser.parseHref(tabData.topWindowHref || '')['hashParams'].hasOwnProperty('ndn_debug'));
    			});
    			*/
    		    
    		    deferred.resolve(!this._embeddedProduct.getData().isStoringEventTimelineData);
    		    
    		}, this));
    	},
    	
    	synchronizeInterface: function() {
    		var role = this._embeddedProduct.getPopup().getExtensionOptions().role;
    		
    		this.needsRefreshForTimelineData().done($.proxy(function(timelineRequiresRefresh) {
    			this._dom.container.html('Features: <a href="#" data-type="scrollTo">scroll to</a> <a href="#" data-type="getDebugInfo">sandbox amp</a>' +
    				(role == 'dev' ? ' <a href="#" data-type="pause">pause</a> <a href="#" data-type="pauseOthers">pause others</a>' : '') +
    				' <a href="#" data-type="viewTimeline">timeline' + (timelineRequiresRefresh ? ' (refresh)' : '') + '</a>'
    				+ '<div class="eventTimeline"></div>');
    			
    			// If this feature is not available using the current build, then hide this feature's link since it will not work
    			if (typeof this._embeddedProduct.getData().isStoringEventTimelineData == 'undefined') {
    			    this._dom.container.find('> a[data-type="viewTimeline"]').hide();
    			}
    		}, this));
    	},
    
    	/**
    	 * Scroll to the location on the page where this product is embedded
    	 */
    	scrollToEmbedLocation: function() {
    		// Instruct the content script to scroll to the appropriate dom element using jquery.scrollTo plugin
    		Ndn_ChromeExt.sendRequestToContentScript({scrollTo: this._embeddedProduct.getData().containerId});
    	},
    	
    	/**
    	 * Instruct the content script to sandbox the Akamai Media Player's API per this embedded product's usage
    	 */
    	sandboxAmpPlayer: function() {
    		Ndn_ChromeExt_SandboxAmp.sandbox(this._embeddedProduct.getData().containerId);
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_Features;
});