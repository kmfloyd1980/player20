define([
    'jquery',
    'models/Ndn/ChromeExt',
    'models/Ndn/Utils/Base',
    
    'lib/jquery/plugins/jquery.jsonPresenter'
], function(
    $,
    Ndn_ChromeExt,
    Ndn_Utils_Base
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_Features_EventTimeline = Ndn_Utils_Base.extend({
    	/**
    	 * @param {Ndn_ChromeExt_Popup_EmbeddedProduct} embeddedProduct
    	 * @param {Object} eventsData
    	 */
    	constructor: function(embeddedProduct, eventsData) {
    		/**
    		 * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
    		 */
    		this._embeddedProduct = embeddedProduct;
    		
    		/**
    		 * @var {Object}
    		 */
    		this._eventsData = eventsData;
    		
    		/**
    		 * The index of the event to use as the time delta for all other time deltas displayed
    		 * @var {Integer}
    		 */
    		this._timeDeltaEventIndex = 0;
    		
    		/**
    		 * List of integers describing which events have their event data being displayed. Format:
    		 * <pre>[
    		 * 	Integer, // The "event index" describing an event that has its event data being displayed
    		 * 	... // All other event indices describing events showing their event data
    		 * ]</pre>
    		 * @var {Array}
    		 */
    		this._displayedEvents = [];
    	},
    	
    	initializeDom: function() {
    		var html =
    			this._dom = {
    				container: null
    			};
    		
    		this._dom.container =
    		$(document.createElement('div'))
    		.addClass('popupSubsectionContainer eventTimeline_events');
    	},
    	
    	initializeEventListeners: function() {
    		var self = this;
    		
    		this._dom.container
    		.on('mouseover', '.deltaTime', function(event) {
    			$(event.currentTarget).parents('.eventTimeline_event').addClass('deltaTimeHover');
    		})
    		.on('mouseout', '.deltaTime', function(event) {
    			$(event.currentTarget).parents('.eventTimeline_event').removeClass('deltaTimeHover');
    		})
    		.on('click', '.deltaTime', function(event) {
    			self.setTimelineDelta($(event.currentTarget).data('index'));
    			
    			event.preventDefault();
    			return false;
    		})
    		.on('click', '.eventTimeline_event a', function(event) {
    			event.stopPropagation();
    		})
    		.on('click', '.eventTimeline_event', function(event) {
    			if ($(event.currentTarget).find('.seeData.active').length) {
    				self.hideEventData($(event.currentTarget).find('.seeData.active').data('index'));
    			}
    			else {
    				self.displayEventData($(event.currentTarget).data('index'));
    			}
    			
    			event.preventDefault();
    			return false;
    		})
    		.on('click', '.updateTimeline', function(event) {
    			self.updateEventTimelineData();
    		});
    	},
    	
    	hideEventData: function(eventIndex) {
    		if (typeof eventIndex != 'undefined') {
    			// Hide the event data for the specified event
    			this._dom.container.find('.eventTimeline_event:eq(' + eventIndex + ') .eventTimeline_event_eventData').remove();
    			this._dom.container.find('.eventTimeline_event:eq(' + eventIndex + ') .seeData.active').removeClass('active');
    		}
    		else {
    			// If no event index is specified, hide event data for all events
    			this._dom.container.find('.eventTimeline_event_eventData').remove();
    			this._dom.container.find('.seeData.active').removeClass('active');
    		}
    	},
    	
    	displayEventData: function(eventIndex) {
    		// console.log('displayEventData(' + eventIndex + ');');
    		
    		// this.hideEventData();
    		
    		this._dom.container.find('.eventTimeline_event:eq(' + eventIndex + ')').append(
    			'<div class="eventTimeline_event_eventData json"></div>'
    		);
    		
    		this._dom.container.find('.eventTimeline_event:eq(' + eventIndex + ')').find('.seeData').addClass('active');
    		
    		var jsonContainer = this._dom.container.find('.eventTimeline_event:eq(' + eventIndex + ') .eventTimeline_event_eventData');
    		if (!jsonContainer.html()) {
    			jsonContainer.jsonPresenter({json: this._eventsData[eventIndex]})
    		}
    		
    		// Keep track of which events are displaying their event data
    		var displayedEvents = [];
    		this._dom.container.find('.eventTimeline_event .seeData.active').each(function() {
    			displayedEvents.push($(this).data('index'));
    		});
    		
    		this._displayedEvents = displayedEvents;
    	},
    	
    	updateEventTimelineData: function() {
    		// Request the data pertaining to this embedded product's analytics timeline
    		Ndn_ChromeExt.sendRealRequestToContentScript({'getProductTimelineData?': this._embeddedProduct.getData().containerId})
    		.done($.proxy(function(eventsData) {
    			this._eventsData = eventsData;
    			this.synchronizeInterface();
    		}, this));
    	},
    	
    	setTimelineDelta: function(eventIndex) {
    		this._timeDeltaEventIndex = eventIndex;
    		this.synchronizeInterface();
    	},
    	
    	/**
    	 * TODO: Must optimize this so that the JSON presenter plugin does not get recreated each time. Doing so will keep the collapsed levels intact when resynchronizing the interface.
    	 */
    	synchronizeInterface: function() {
    		var events = this._eventsData;
    		
    		console.log('Events: ', events);
    		
    		// If there are no events to synchronize, then finish executing this method
    		if (!events.length) return;
    		
    		var eventIndex = 0,
    			firstTimestamp = events[this._timeDeltaEventIndex].eventData.system.timestamp,
    			html = this._dom;
    		
    		html.container.html('<div class="header">Event Timeline: <i class="fa fa-refresh updateTimeline" alt="update timeline events"></i></div>');
    		
    		$.each(events, function() {
    			// console.log('events iteration, this ==', this);
    			
    			var eventIdentifier = this.eventIdentifier,
    				eventData = this.eventData,
    				eventTimestamp = eventData.system.timestamp;
    			
    			var eventTimestampOffset = eventTimestamp - firstTimestamp,
    				eventTimestampOffsetInSeconds = eventTimestampOffset / 1000;
    			
    			var shortEventIdentifier = eventData['public'].eventName;
    			
    			var hasData = eventData['public'].data;
    			
    			var eventHtml = '<div class="eventTimeline_event" data-index="' + eventIndex + '">' + 
    				'<span class="eventName">' +
    					'<a href="https://newsinc.atlassian.net/wiki/display/PROD/NDN+Player+Suite+Internal+Events#NDNPlayerSuiteInternalEvents-' + shortEventIdentifier + '" target="_blank">' + shortEventIdentifier + '</a>' +
    					'<span class="containerElementEventTypeIndex">' + eventData.system.containerElementEventTypeIndex + '</span>' + 
    				'</span> ' +
    				'<span class="eventTimestampOffsetInSeconds">' + eventTimestampOffsetInSeconds + 's</span>' +
    				'<span class="deltaTime" data-index="' + eventIndex + '">&Delta;t</span>' +
    				'<span class="seeData" data-index="' + eventIndex + '">view data</span>' +
    			'</div>';
    			
    			html.container.append(eventHtml);
    			
    			eventIndex++;
    		});
    		
    		html.container.find('.eventTimeline_event:eq(' + this._timeDeltaEventIndex + ')').addClass('baseForTimelineDelta');
    		
    		/*
    		// Re-display each of the events' data that were previously showing them
    		var self = this;
    		$.each(this._displayedEvents, function() {
    			var eventIndex = this;
    			self.displayEventData(eventIndex);
    		});
    		*/
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_Features_EventTimeline;
});