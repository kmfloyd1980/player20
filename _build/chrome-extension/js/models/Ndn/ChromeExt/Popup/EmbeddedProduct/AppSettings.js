define([
    'jquery',
    'models/Ndn/Utils/Base'
], function(
    $,
    Ndn_Utils_Base
) {
    var Ndn_ChromeExt_Popup_EmbeddedProduct_AppSettings = Ndn_Utils_Base.extend({
        constructor: function(embeddedProduct) {
            /**
             * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
             */
            this._embeddedProduct = embeddedProduct;
        },
        
        initializeDom: function() {
            var configSettingsToHideForNonDevelopers = [
                'acceptsConfigFromUrl',
                'acceptsConfigFromUrlPrefix'
            ];
            
            var html =
                this._dom = {
                    container: null
                },
                productData = this._embeddedProduct.getData();
            
            this._dom.container =
            $(document.createElement('div'))
            .addClass('showConfigContainer appSettingsContainer')
            .append('<span class="uiLabel">Application Settings:</span> <a href="#" data-type="default">default</a><a href="#" data-type="overrides">overrides</a><a href="#" data-type="used">used</a>')
            .append(
                html.defaultContainer =
                $(document.createElement('div'))
                .addClass('providedConfigContainer')
                .append('<p class="header">Default:</p>')
                .hide()
            )
            .append(
                html.overridesContainer =
                $(document.createElement('div'))
                .addClass('usedConfigContainer')
                .append('<p class="header">Overrides:</p>')
                .hide()
            )
            .append(
                html.usedContainer =
                $(document.createElement('div'))
                .addClass('usedConfigContainer')
                .append('<p class="header">Used:</p>')
                .hide()
            );
            
            var role = this._embeddedProduct.getPopup().getExtensionOptions().role;
    
            // var configSettingsToListAtTop = ['type', 'splitVersion'];
            
            var listConfigSettings = function(containerElement, settingsLabel, omitConfigSettings) {
                omitConfigSettings = omitConfigSettings || {};
                
                return function(settingName, settingValue) {
                    // If this config setting is not supposed to be listed, do not display it
                    // if ($.inArray(configSetting, omitConfigSettings) !== -1) return;
                    
                    // If this config setting should be hidden from any role except the "Developer" role, do not display it
                    // if (role != 'dev' && $.inArray(configSetting, configSettingsToHideForNonDevelopers) !== -1) return;
                    
                    var settingElement = 
                    $(document.createElement('p'))
                    .addClass('appSetting')
                    .html('<b>' + settingName + ':</b> ' + settingValue);
                    
                    // Determine whether this setting is overridding the default setting
                    if (settingsLabel != 'default') {
                        /*
                        console.log("******");
                        console.log("productData.appSettings['default'][settingName] ==", productData.appSettings['default'][settingName]);
                        console.log('settingValue ==', settingValue);
                        console.log("productData.appSettings['default'][settingName] != settingValue, ==", productData.appSettings['default'][settingName] != settingValue);
                        console.log("******");
                        */
                        
                        if (productData.appSettings['default'][settingName] != settingValue) {
                            settingElement.addClass('override');
                            
                            // Make sure the link to show these settings indicates that one or more of the listed settings are overridding default settings
                            html[settingsLabel + 'Container'].addClass('overrides');
                            html.container.find('> a[data-type="' + settingsLabel + '"]').addClass('overrides');
                            html.container.addClass('overrides');
                        }
                    }
                    
                    containerElement.append(settingElement);
                };
            };
            
            // If there is no application settings data, then do not display this user interface component
            if (!productData.appSettings) {
                html.container.hide();
                return;
            }
            
            var countProperties = function(obj) {
                var result = 0;
                $.each(obj, function() {
                    result++;
                });
                return result;
            }
            
            var displaySettingsData = function(settingsLabel, settingsData, containerElement) {
                // List the remaining config settings
                $.each(settingsData, listConfigSettings(containerElement, settingsLabel)); // , configSettingsToListAtTop));
                
                var numberOfSettings = countProperties(settingsData);
                
                var settingsLabelElement = html.container.find('a[data-type="' + settingsLabel + '"]');
                settingsLabelElement.append('&nbsp;<span>(' + numberOfSettings + ')</span>');
                
                if (!numberOfSettings) {
                    settingsLabelElement.addClass('disabled');
                }
            };
            
            var totalAppSettingsCount = countProperties(productData.appSettings['used']);
            
            if (!totalAppSettingsCount) {
                html.container.html('<span class="uiLabel">Application Settings:</span> <a class="disabled">none</a>');
            }
            
            // Generate the "default" application settings table
            displaySettingsData('default', productData.appSettings['default'], html.defaultContainer);
    
            // Generate the "overrides" application settings table
            displaySettingsData('overrides', productData.appSettings['overrides'], html.overridesContainer);
    
            // Generate the "used" application settings table
            displaySettingsData('used', productData.appSettings['used'], html.usedContainer);
        },
        
        initializeEventListeners: function() {
            // console.log('ShowConfig.initializeEventListeners();');
            
            var html = this._dom,
                productData = this._embeddedProduct.getData();
    
            html.container
            .on('click', 'a[data-type]', function(event) {
                if (!$(this).hasClass('disabled')) {
                    if ($(this).attr('data-type') == 'default') {
                        html.defaultContainer.toggle();
                        $(this).toggleClass('active');
                    }
                    else if ($(this).attr('data-type') == 'overrides') {
                        html.overridesContainer.toggle();
                        $(this).toggleClass('active');
                    }
                    else if ($(this).attr('data-type') == 'used') {
                        html.usedContainer.toggle();
                        $(this).toggleClass('active');
                    }
                }
                
                event.preventDefault();
            });
        }
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProduct_AppSettings;
});