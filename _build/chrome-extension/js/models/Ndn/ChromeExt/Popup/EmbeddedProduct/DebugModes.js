define([
    'jquery',
    'models/Ndn/Utils/Base'
], function(
    $,
    Ndn_Utils_Base
) {
    return Ndn_Utils_Base.extend({
        constructor: function(embeddedProduct) {
            /**
             * @var {Ndn_ChromeExt_Popup_EmbeddedProduct}
             */
            this._embeddedProduct = embeddedProduct;
        },
        
        initializeDom: function() {
            var html =
                this._dom = {
                    container: null
                },
                productData = this._embeddedProduct.getData();
            
            html.container =
            $(document.createElement('div'))
            .addClass('showConfigContainer appSettingsContainer')
            .append('<span class="uiLabel">Debug Modes:</span> <a href="#" data-type="show-enabled">show</a>')
            .append(
                html.debugModesContainer =
                $(document.createElement('div'))
                .addClass('providedConfigContainer')
                .append('<p class="header">Enabled:</p>')
                .hide()
            );

            var role = this._embeddedProduct.getPopup().getExtensionOptions().role;

            // If debug modes are not detected to be enabled for this product, do not show this component in the user interface
            if (typeof productData.isStoringEventTimelineData == 'undefined') {
                html.container.hide();
                return;
            }
            
            // List the debug modes enabled
            var containerElement = html.debugModesContainer;
            $.each(productData.debugModes, function() {
                var debugMode = this + '';
                
                // Add the debug mode to the user interface
                html.debugModesContainer.append(
                    $(document.createElement('p'))
                    .addClass('appSetting')
                    .html(debugMode)
                );
            });
            
            var numberOfSettings = productData.debugModes.length;
            
            var settingsLabelElement = html.container.find('a[data-type="show-enabled"]');
            settingsLabelElement.append('&nbsp;<span>(' + numberOfSettings + ')</span>');
            
            if (!numberOfSettings) {
                settingsLabelElement.addClass('disabled');
            }

            // If debug modes are not detected to be enabled for this product, do not show this component in the user interface
            if (!productData.isStoringEventTimelineData) {
                if (role == 'am') {
                    html.container.hide();
                }
                else {
                    html.container.html('<span class="uiLabel">Debug Modes:</span> <a class="disabled">none</a>');
                }
            }
            else if (numberOfSettings == 0) {
                html.container.html('<span class="uiLabel">Debug Mode:</span> <a class="disabled">enabled</a>');
            }
        },
        
        initializeEventListeners: function() {
            var html = this._dom,
                productData = this._embeddedProduct.getData();
    
            html.container
            .on('click', 'a[data-type]', function(event) {
                if (!$(this).hasClass('disabled')) {
                    if ($(this).attr('data-type') == 'show-enabled') {
                        html.debugModesContainer.toggle();
                        $(this).toggleClass('active');
                    }
                }
                
                event.preventDefault();
            });
        }
    });
});