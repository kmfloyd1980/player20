define([
    'jquery',
    'models/Ndn/Utils/Base',
    'models/Ndn/ChromeExt/Popup/EmbeddedProduct',
    'models/Ndn/ChromeExt'
], function(
    $,
    Ndn_Utils_Base,
    Ndn_ChromeExt_Popup_EmbeddedProduct,
    Ndn_ChromeExt
) {
    var Ndn_ChromeExt_Popup_EmbeddedProducts = Ndn_Utils_Base.extend({
    	constructor: function(popup) {
    		/**
    		 * @var {Ndn_ChromeExt_Popup}
    		 */
    		this._popup = popup;
    		
    		/**
    		 * Structured data concerning the embedded products that this interface component is responsible for displaying. Format:
    		 * <pre>{
    		 * 	String: Object, // The container element's "id" attribute of the embedded product => structured data concerning the embedded product
    		 * 	... // All other embedded products
    		 * }</pre>
    		 * @var {Object}
    		 */
    		this._products = {};
    		
    		/**
    		 * @var {Object}
    		 */
    		this._dom = {
    			container: $('.embeddedProducts')
    		};
    		
    		this.synchronizeInterface();
    	},
    	
    	/**
    	 * @param {Object} productData Structured data concerning an embedded product on the page
    	 */
    	addProduct: function(productData) {
    		// console.log('EmbeddedProducts.addProduct(', arguments, ');');
    		
    		// console.log('typeof this._products["landing-page-embed-container"] ==', typeof this._products["landing-page-embed-container"]);
    		// console.log('this._products ==', this._products);
    		
    		if (typeof this._products[productData.containerId] == 'undefined') {
    			this._products[productData.containerId] = productData;
    			
    			this._dom.container.append(new Ndn_ChromeExt_Popup_EmbeddedProduct(productData, this._popup));
    		}
    	},
    	
    	/**
    	 * @return {String} The sandbox HTML of all the listed products 
    	 */
    	getSandboxHtml: function(embedMethod) {
    		
    	},
    	
    	/**
    	 * Use the registered embedded products data (from "background.js") to synchronize this component's interface
    	 */
    	synchronizeInterface: function() {
    		var self = this;
    
    		// Detect any embedded products on the active tab's current page
    		Ndn_ChromeExt.detectEmbedsForActiveTab();
    		
    		// Ensure that each detected embedded product is already being displayed
            Ndn_ChromeExt.getActiveTabData().done(function(tabData) {
                //console.log('^^^^^ *****  >>>>>   tabData ==', tabData);
                
                $.each(tabData.embeddedProductsData, function(containerId, productData) {
                    self.addProduct(productData);
                });
            });
    		/*
    		Ndn_ChromeExt.getCurrentTabId().done(function(tabId) {
                chrome.extension.getBackgroundPage().require(['models/Ndn/ChromeExt'], function(Ndn_ChromeExt) {
                    Ndn_ChromeExt.getTabData(tabId).done(function(tabData) {
                        $.each(tabData.embeddedProductsData, function(containerId, productData) {
                            self.addProduct(productData);
                        });
                    });
                });
            });
    		
    		/*
    		Ndn_ChromeExt.getActiveTabData().done(function(tabData) {
    		    console.log('^^^^^ *****  >>>>>   tabData ==', tabData);
    		    
                $.each(tabData.embeddedProductsData, function(containerId, productData) {
                    self.addProduct(productData);
                });
            });
    		/*
    		Ndn_ChromeExt.getCurrentTabId().done(function(tabId) {
    			chrome.extension.getBackgroundPage().Ndn_ChromeExt.getTabData(tabId).done(function(tabData) {
    	            $.each(tabData.embeddedProductsData, function(containerId, productData) {
    	                self.addProduct(productData);
    	            });
    			});
    		});
    		*/
    	}
    });
    
    return Ndn_ChromeExt_Popup_EmbeddedProducts;
});