define(['jquery'], function($) {
    var Ndn_ChromeExt = {
    	/**
    	 * @return {String} The version number of this Chrome Extension
    	 */
    	getVersion: function() {
    		return chrome.runtime.getManifest().version;
    	},
    	
    	/**
    	 * Register which tab is currently in focus (or was last in focus) and should be associated with this Chrome extension's popup if it were to load
    	 */
    	setActiveTab: function(tabId) {
    	    selectedTabId = tabId;
    	},
    	
    	getActiveTab: function() {
    	    if (selectedTabId) {
    	        return browserTabs.getTab(selectedTabId);
    	    }
    	},
    	
    	getEnv: function() {
    	    return ''; // 'dev';
    	},
    
    	/**
    	 * @return {Boolean} Whether the tab is actually a Chrome Developer Tools window pane
    	 */
    	isDevToolsTab: function(tabData) {
    	    return (tabData && tabData.url || '').split(':')[0] == 'chrome-devtools';
    	},
    	
    	/**
    	 * Synchronize the browser action of this Chrome Extension with the appropriate badge text
    	 * NOTE: This method is only available within the background page context.
    	 */
    	synchronizeBrowserAction: function() {
    	    // staticDebug('synchronizeBrowserAction();... selectedTabId == ' + selectedTabId);
    	    
    	    try {
        	    var tab = browserTabs.getTab(selectedTabId),
        	        tabData = tab.getData(),
        	        numberOfEmbeds = tab.getNumberOfEmbeds(),
        	        hasEnvOverrides = !!tabData.overriddenEnvs.length,
        	        hasAppSettingsOverrides = tabData.hasOverriddenAppSettings;
        
        	    // If there were embeds detected for this tab, update the badge text and badge icon appropriately
        	    if (numberOfEmbeds) {
        	        if ((tabData.hasHardcodedBuildNumber && !tabData.legacyMigrationAddedEmbedJs) || hasEnvOverrides || hasAppSettingsOverrides) { // hasHardcodedBuildNumber(selectedTabId)) {
        	            chrome.browserAction.setIcon({path: 'img/browser-action-icons/active-warning.png'});
        	        }
        	        else {
        	            chrome.browserAction.setIcon({path: 'img/browser-action-icons/active.png'});
        	        }
        	        
        	        if (tab.hasNonProductionEmbeds()) {
        	            chrome.browserAction.setBadgeBackgroundColor({color: '#5FB4E8'});
        	        }
        	        else {
        	            chrome.browserAction.setBadgeBackgroundColor({color: '#0A3A63'});
        	        }
        	        
        	        chrome.browserAction.setBadgeText({text: numberOfEmbeds + ''});
        	    }
        	    else {
        	        chrome.browserAction.setIcon({path: 'img/browser-action-icons/inactive.png'});
        	        chrome.browserAction.setBadgeText({text: ''});
        	    }
    	    }
    	    catch (e) {
    	        console.log("******** SYNCHRONIZE BROWSER ACTION, EXCEPTION: e ==", e);
    	    }
    	    // console.log("END SYNCHRONIZE BROWSER ACTION...... <<<<<");
    	},
    	
    	/**
    	 * @param {String} url The URL to open in the current tab in the browser
    	 */
    	openUrlInCurrentTab: function(url) {
    		return $.Deferred(function(deferred) {
    			Ndn_ChromeExt.getCurrentTabId().done(function(tabId) {
    				chrome.tabs.update(tabId, {url: url}, function() {
    					deferred.resolve();
    				});
    			});
    		}).promise();
    	},
    	
    	reloadCurrentTab: function() {
            Ndn_ChromeExt.sendRequestToContentScript({reloadTab: true});
    	},
    	
    	/**
    	 * @param {String} url The URL to open in an adjacent tab (to the right of the current tab) in the browser
    	 */
    	openUrlInAdjacentTab: function(url) {
    		return $.Deferred(function(deferred) {
    			chrome.tabs.query({
    				active: true, 
    				currentWindow: true
    			}, function(tabs) {
    				var adjacentTabIndex = tabs[0].index + 1;
    				
    				chrome.tabs.create({
    					url: url,
    					index: adjacentTabIndex
    				});
    				
    				deferred.resolve();
    			});
    		}).promise();
    	},
    
        /**
         * @param {unknown_type} data The data to send in a message to the current tab's associated content script
         * @return {jQuery.Deferred} Deferred object where .done(function(response) { allows access to the content script's response
         */
    	sendMessageToContentScript: function(data) {
            return $.Deferred(function(deferred) {
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    var tabId = tabs[0].id;
                    chrome.tabs.sendMessage(tabId, data, function(response) {
                        deferred.resolve(response);
                    });
                }); 
            }).promise();
    	},
    	
    	/**
    	 * @deprecated Use the .sendMessageToContentScript() method instead
    	 */
    	sendRequestToContentScript: function(data) {
    		return this.sendMessageToContentScript(data);
    	},
    
    	sendRealRequestToContentScript: function(data) {
    		console.log('Ndn_ChromeExt.sendRealRequestToContentScript(', data, ');');
    		
    		return $.Deferred(function(deferred) {
    			chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    				var tabId = tabs[0].id;
    				chrome.tabs.sendRequest(tabId, data, function(response) {
    					deferred.resolve(response);
    				});
    			});	
    		}).promise();
    	},
    	
    	/**
    	 * @return {jQuery.Deferred} Deferred object that sends the currently active browser tab's ID as the first parameter to the closure passed to its .done() method
    	 */
    	getCurrentTabId: function() {
    		return $.Deferred(function(deferred) {
    			if (chrome && chrome.extension && chrome.extension.getBackgroundPage) {
    				deferred.resolve(chrome.extension.getBackgroundPage().selectedTabId);
    			}
    			else if (chrome && chrome.tabs && chrome.tabs.query) {
    				chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    					var tabId = tabs[0].id;
    					deferred.resolve(tabId);
    				});
    			}
    			else if (chrome && chrome.runtime && chrome.runtime.sendMessage) {
    				// Talk to the "background.js" file
    				chrome.runtime.sendMessage({getCurrentTabId: true}, function(tabId) {
    					deferred.resolve(tabId);
    				});
    			}
    		}).promise();
    	},

        /**
         * @return {jQuery.Deferred} Deferred object that sends the currently active browser tab's stored data as the first parameter to the closure passed to its .done() method
         */
    	getActiveTabData: function() {
            return $.Deferred($.proxy(function(deferred) {
                if (chrome && chrome.runtime && chrome.runtime.sendMessage) {
                    // Talk to the "background.js" file to retrieve the needed data
                    chrome.runtime.sendMessage({getCurrentTabData: true}, function(tabData) {
                        deferred.resolve(tabData);
                    });
                }
            }, this)).promise();
    	},

        /**
         * @deprecated Use .getActiveTabData() method instead
         */
    	getCurrentTabData: function() {
    		return this.getActiveTabData();
    	},
    
    	/**
    	 * @return {jQuery.Deferred} Deferred object that sends the currently active browser tab's stored data as the first parameter to the closure passed to its .done() method
    	 */
    	getTabData: function() {
    		return $.Deferred($.proxy(function(deferred) {
    		    if (this.isInBackground()) {
    		        deferred.resolve(this.getActiveTab().getData());
    		    }
    		    else {
    	            if (chrome && chrome.runtime && chrome.runtime.sendMessage) {
    	                // Talk to the "background.js" file to retrieve the needed data
    	                chrome.runtime.sendMessage({getTabData: true}, function(tabData) {
    	                    deferred.resolve(tabData);
    	                });
    	            }
    		    }
    		}, this)).promise();
    	},
    	
    	getTopWindowOrigin: function() {
    		return $.Deferred($.proxy(function(deferred) {
    			this.getTabData().done(function(tabData) {
    				deferred.resolve(tabData.topWindowOrigin);
    			});
    		}, this)).promise();
    	},
    	
    	getMainWindowOrigin: function() {
    		return $.Deferred($.proxy(function(deferred) {
    			this.getTabData().done(function(tabData) {
    				deferred.resolve(tabData.mainWindowOrigin);
    			});
    		}, this)).promise();
    	},
    
    	/**
    	 * @return {jQuery.Deferred} Deferred object that sends the currently active browser tab's stored data as the first parameter to the closure passed to its .done() method
    	 */
    	updateTabData: function(tabData) {
    	    // console.log('Ndn_ChromeExt.updateTabData(', tabData, ');');
    	    
    		return $.Deferred($.proxy(function(deferred) {
    			if (chrome && chrome.runtime && chrome.runtime.sendMessage) {
    			    // console.log(' >> About to send message with tabData ==', tabData);
    			    
    				// Talk to the "background.js" file to update the current tab with the provided data
    				chrome.runtime.sendMessage({updateTabData: tabData}, function(tabData) {
    				    // console.log(' >> Sent message about updating tab data with tabData ==', tabData);
    					
    				    deferred.resolve(tabData);
    				});
    			}
    		}, this)).promise();
    	},
    	
    	getExtensionOptions: function() {
    		return $.Deferred(function(deferred) {
    			chrome.storage.sync.get({
    				"role": 'am',
    	            "sandbox_env": ''
    			}, function(options) {
    				deferred.resolve(options);
    			});
    		}).promise();
    	},
    	
    	/**
    	 * NOTE: May be used from background or popup
    	 * TODO: Verify if this may be called from the content script... ***
    	 */
    	detectEmbedsForActiveTab: function() {
    	    if (this.isInBackground()) {
    	        this.getActiveTab().detectEmbeds();
    	    }
    	    else {
                this.sendRequestToContentScript({getEmbeds: true});
    	    }
    	},
    	
        initialize: function() {
            if (this.isInBackground()) {
                 // Initialize message listeners that may come from other components of this Chrome Extension (such as the popup or content-script)
                 chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
                     if (request.registerEmbed) {
                         // console.log('request.registerEmbed, request ==', request);
                         
                         browserTabs.registerEmbed(sender.tab.id, request.registerEmbed);
                         Ndn_ChromeExt.synchronizeBrowserAction();
                         sendResponse({
                             currentTabId: selectedTabId
                             // embeds: getEmbeds()
                         });
                     }
                     else if (request.unregisterEmbed) {
                         browserTabs.unregisterEmbed(sender.tab.id, request.unregisterEmbed);
                         Ndn_ChromeExt.synchronizeBrowserAction();
                         sendResponse({});
                     }
                     else if (request.detectEmbeds) {
                         browserTabs.getTab(sender.tab.id).detectEmbeds();
                         sendResponse({});
                     }
                     else if (request.hardcodedBuildNumberDetected) {
                         browserTabs.getTab(sender.tab.id).update('hasHardcodedBuildNumber', true);
                         Ndn_ChromeExt.synchronizeBrowserAction();
                         sendResponse({});
                     }
                     else if (request.debug) {
                         debug(sender.tab.id, request.debug);
                         sendResponse({});
                     }
                     else if (request.getDebugLog) {
                         sendResponse({
                             selectedTabId: selectedTabId,
                             debugLog: browserTabs.getDebugLogs()
                         });
                     }
                     else if (request.clearDebug) {
                         browserTabs.getTab(sender.tab.id).update('debugLog', '');
                         sendResponse({});
                     }
                     else if (request.legacyMigrationAddedEmbedJs) {
                         browserTabs.getTab(sender.tab.id).update('legacyMigrationAddedEmbedJs', request.legacyMigrationAddedEmbedJs);
                         Ndn_ChromeExt.synchronizeBrowserAction();
                         sendResponse({});
                     }
                     else if (request.getCurrentTabId) {
                         sendResponse(selectedTabId);
                     }
                     else if (request.getCurrentTabData) {
                         sendResponse(browserTabs.getTab(selectedTabId).getData());
                     }
                     else if (request.updateTabData) {
                         browserTabs.getTab(sender.tab.id).update(request.updateTabData);
                         sendResponse(browserTabs.getTab(sender.tab.id).getData());
                     }
                     else if (request.getTabData) {
                         sendResponse(browserTabs.getTab(sender.tab.id).getData());
                     }
                     else if (request.synchronizeBrowserAction) {
                         Ndn_ChromeExt.synchronizeBrowserAction();
                         sendResponse({});
                     }
                 });
        
                 // Detect embedded products on tabs when they update their page's content
                 chrome.tabs.onUpdated.addListener(function(tabId, change, tab) {
                     console.log('onUpdated, arguments ==', arguments);
                     
                     if (change.status == "complete") {
                         // If no selected tab ID has been set yet, use this tab ID as the selected one (situation where there is only one tab open)
                         if ((!Ndn_ChromeExt.getActiveTab() && !Ndn_ChromeExt.isDevToolsTab(tab)) || tab.active) { // if (selectedTabId == null && !isDevToolsTab(tab)) {
                             Ndn_ChromeExt.setActiveTab(tabId); // selectedTabId = tabId;

                             Ndn_ChromeExt.getActiveTab().detectEmbeds();
                             Ndn_ChromeExt.synchronizeBrowserAction();
                         }
                         
                         browserTabs.getTab(tabId).initialize();
                         browserTabs.getTab(tabId).detectEmbeds();
                         Ndn_ChromeExt.synchronizeBrowserAction();
                     }
                 });
        
                 // When a different window is selected, identify the appropriately selected tab ID, then synchronize the browser action
                 chrome.windows.onFocusChanged.addListener(function(windowId) {
                     console.log('onFocusChanged, arguments ==', arguments);
                     
                     chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                         // If a Chrome window was focused on
                         if (tabs.length) {
                             Ndn_ChromeExt.setActiveTab(tabs[0].id); // selectedTabId = tabs[0].id;
                             
                             Ndn_ChromeExt.getActiveTab().detectEmbeds();
                             Ndn_ChromeExt.synchronizeBrowserAction();
                         }
                     });
                 });
        
                 // When a new tab is selected, make sure to synchronize the tab's browser action appropriately
                 chrome.tabs.onActivated.addListener(function(eventData) {
                     console.log('onActivated, arguments ==', arguments);
                     
                     // Get this newly activated tab's data
                     chrome.tabs.get(eventData.tabId, function(tabData) {
                         if (!Ndn_ChromeExt.isDevToolsTab(tabData)) {
                             // console.log('[NDN Player Suite Chrome Extension] Browser tab activated. tabData ==', tabData);
                             
                             // Keep track of which tab is currently selected
                             Ndn_ChromeExt.setActiveTab(eventData.tabId);
        
                             Ndn_ChromeExt.getActiveTab().detectEmbeds();
                             Ndn_ChromeExt.synchronizeBrowserAction();
                         }
                     });
                 });
            }
        },
        
        /**
         * @param {Function} closure The logic to repeat x-number of times at the intervals specified within this method
         * @param {Array<int>} [times] The intervals (measured in seconds) to repeat the provided logic after the first call is made (defaults to [1, 2, 4, 8])
         */
        echoClosure: function(closure, times) {
            // Call the closure immediately the first time
            closure();
            
            if (!times) times = [1, 2, 4, 8];
            for (var i = 0; i < times.length; i++) {
                var time = times[i];
        
                setTimeout(function() {
                    closure();
                }, time * 1000);
            }
        },
    	
    	/**
    	 * @return {Boolean} Whether JavaScript is currently operating within the context of the Chrome Extension's background
    	 */
    	isInBackground: function() {
    		return NDN_PLAYER_SUITE_CHROME_EXTENSION.realm == 'background';
    	},
    
    	/**
    	 * @return {Boolean} Whether JavaScript is currently operating within the context of the Chrome Extension's popup window
    	 */
    	isInPopup: function() {
    		return NDN_PLAYER_SUITE_CHROME_EXTENSION.realm == 'popup';
    	},
    
    	/**
    	 * @return {Boolean} Whether JavaScript is currently operating within the context of the Chrome Extension's content script
    	 */
    	isInContentScript: function() {
    		return NDN_PLAYER_SUITE_CHROME_EXTENSION.realm == 'contentScript';
    	}
    };
    
    return Ndn_ChromeExt;
});