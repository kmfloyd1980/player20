define(['jquery'], function($) {
    var Ndn_Utils_PlayerSuiteApp = (function() {
        /**
         * @param {String} name The name of the config setting
         * @param {String} value The value of the config setting
         * @return {Boolean} Whether the value for the specified config setting is actual information 
         */
        function _isConfigSettingNeeded(name, value) {
            /**
             * A list of config settings that when the empty string value is provided for it, it is actual information, as opposed to being omittable
             * @var {Array<String>}
             */
            var configSettingsWithMeaningfulEmptyValues = ['acceptsConfigFromUrlPrefix'];
            
            return value !== '' || $.inArray(name, configSettingsWithMeaningfulEmptyValues) !== -1
        }
        
        return {
            isAppDomain: function(domain) {
                return !!(domain + '').match(/^([^0-9-]+)?([0-9]*)?[-]?launch\.newsinc\.com$/);
            },

            /**
             * @param {String} url A URL that may or may not point to an asset used by the NDN Player Suite application
             * @return {Object?} Structured data concerning the protocol, environment, environment lane, and environment build number (or null if the provided URL is not an application URL). Format:
             * <pre>{
             *  'protocol': String, // The protocol; example values: 'https:', 'http:', or ''
             *  'env': String, // The environment; example values: 'staging', 'qa', 'dev', 'local', or '' (for production). This value does not include the environment lane.
             *  'envLane': String, // The environment lane; example values: '' (for production or staging since they have no lanes or "qa-launch.newsinc.com", etc.), '2' (for "dev2", "qa2", "local2", etc.)
             *  'envName': String, // The environment name; example values: "dev2", "dev", "qa4", "qa", "local2", "", "staging"
             *  'buildNumber': String, // The environment build number; for example "0" for "http://local-launch.newsinc.com/0" or "26" for "http://launch.newsinc.com/26"
             *  'path': String, // The URL's path relative to the application's base application URL
             *  'subdomain': String, // The URL's subdomain; for example "qa2-launch.newsinc.com" for "http://qa2-launch.newsinc.com/27/js/embed.js"
             *  'baseUrl': String, // The environment's base application URL; for example "http://local-launch.newsinc.com" for "http://local-launch.newsinc.com/js/embed.js", or "http://launch.newsinc.com/46" for "http://launch.newsinc.com/46/css/NdnEmbed.css"
             * }</pre>
             */
            getAppUrlInfo: function(appUrl) {
                var matches = (appUrl + '').match(/^((https?:)?\/\/(([^0-9-]+)?([0-9]*)?[-]?launch\.newsinc\.com))\/?(\d+)?(\/.*)?$/);
                
                if (!matches) return null;
                
                var result = {
                    protocol: matches[2] || '',
                    env: matches[4] || '',
                    envLane: matches[5] || '',
                    buildNumber: matches[6] || '',
                    path: matches[7] || ''
                };
                
                result.envName = result.env + result.envLane;
                result.subdomain = (result.envName ? result.envName + '-' : '') + 'launch.newsinc.com';
                result.baseUrl = result.protocol + '//' + result.subdomain + (result.buildNumber ? '/' + result.buildNumber : '');
                
                return result;
            },
            
            /**
             * @param {String} envName The environment name, which includes the environment as well as the environment lane number
             * @return {Object} Structured data concerning the environment and its environment lane number. Format:
             * <pre>{
             *  'env': {String}, // The environment (for example: "dev", "qa", "local", "staging", or "")
             *  'envLane': {String}, // The environment build number (for example "2" when envName is "dev2")
             * }</pre>
             */
            getEnvNameInfo: function(envName) {
                var matches = (envName || '').match(/^([^0-9-]+)?([0-9]*)?$/) || {};
                
                return {
                    env: matches[1],
                    envLane: matches[2]
                };
            },
            
            /**
             * @param {String} jsEmbedCode The configurable <div> element used to embed a product
             * @param {Object} [settings] Structured data concerning the application landing page URL to generate. See Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl() for more documentation.
             */
            getAppLandingPageUrlFromJsEmbedCode: function(jsEmbedCode, settings) {
                // Retrieve the config from the provided JavaScript embed code, Renaming "style" attribute to "data-style" to workaround Chrome Extension's Content Security Policy directive: "style-src 'self'"
                var modifiedEmbedCode = (jsEmbedCode + '').replace(/style=/g, "data-style="),
                    configurableDiv = $(modifiedEmbedCode),
                    config = Ndn_Utils_PlayerSuiteApp.getConfigFromElement(configurableDiv, true, true);
                
                return Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl(config, settings);
            },
            
            /**
             * @param {Object} config Structured data concerning the config settings to specify in the generated URL
             * @param {Object} [settings] Structured data concerning the application landing page URL to generate. Format:
             * <pre>{
             *  'envName': {String}, // The URL's environment name (defaults to ""); example values: "dev2", "dev", "qa3" 
             *  'buildNumber': {int}, // The URL's build number (defaults to "")
             *  'protocol': {String}, // The URL's protocol (defaults to "http:")
             *  'isIframe': {Boolean}, // Whether this URL is being used for an iframe (defaults to false). If set to true, the "width" and "height" config settings are omitted in the returned URL.
             *  'appSettings': {Object}, // Optional. Structured data concerning the application settings to apply to the resulting URL
             *  'debugModes': {Array<String>}, // Optional. If defined, this will pass along the "?ndn_debug" parameter in the URL. All listed modes will also be passed in a comma-delimited list as the value of said parameter.
             * }</pre>
             * @return {String} The application landing page URL
             */
            getAppLandingPageUrl: function(config, settings) {
                // console.log('Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl(', arguments, ');');
                
                settings = $.extend({
                    envName: '',
                    buildNumber: '',
                    protocol: 'http:',
                    isIframe: false
                }, settings || {});
                
                var urlParams = {};
                $.each(config, function(configSettingName, value) {
                    // There is never a reason why these parameters would need to be sent via the query string in a generated URL, as they can only be 
                    // provided directly on the configurable <div> element used for embedding a product
                    if ($.inArray(configSettingName, ['acceptsConfigFromUrl', 'acceptsConfigFromUrlPrefix']) !== -1) {
                        return;
                    }
                    
                    // If the URL needed is for an iframe element, the "width" and "height" in the URL is redundant and should only be changed by modifying 
                    // the width and height of the <iframe> element itself
                    if (settings.isIframe && $.inArray(configSettingName, ['width', 'height']) !== -1) {
                        return;
                    }
                    
                    urlParams[configSettingName] = value;
                });

                var result = settings.protocol + '//' + (settings.envName ? settings.envName + '-' : '') + 'launch.newsinc.com/' + 
                    (settings.buildNumber ? settings.buildNumber + '/' : '') + 'embed.html?' + $.param(urlParams);
                
                if (typeof settings.appSettings != 'undefined') {
                    result += '&ndn_appSettings=' + encodeURIComponent($.param(settings.appSettings));
                }
                
                if (typeof settings.debugModes != 'undefined') {
                    result += '&ndn_debug';
                    if (settings.debugModes.length) {
                        result += '=' + settings.debugModes.join(',');
                    }
                }
                
                return result;
            },
            
            /**
             * @param {String} embedMethod Recognized values: "js" or "iframe"
             * @param {String} appDomain 
             */
            getEmbedCode: function(embedMethod, envName, productData) {
                var appDomain = (envName ? envName + '-' : '') + 'launch.newsinc.com';
                
                if (embedMethod == 'js') {
                    return Ndn_Utils_PlayerSuiteApp.getJsEmbedCodeFromConfig(productData.initialConfig, appDomain, productData);
                }
                else {
                    var iframeSrc = Ndn_Utils_PlayerSuiteApp.getAppLandingPageUrl(productData.initialConfig, {
                        isIframe: true
                    });
                    
                    return '<iframe' + "\n\t" + 'style="width:' + productData.container.width + 'px;height:' + productData.container.height + 'px;"' + "\n\t" + 'src="' + iframeSrc + '"' + "\n\t" + 'frameBorder="0"></iframe>';
                }
            },
            
            /**
             * @param {jQueryNode} element
             * @param {Boolean} [strict] Whether the dom is validated first as a configurable <div> element (defaults to true)
             * @param {Boolean} [getWidthAndHeight] Whether the "width" and "height" CSS properties are also pulled out of the provided element to determine the config (defaults to false)
             * @return {Object} Structured data representing a widget's config settings
             */
            getConfigFromElement: function(element, strict, getWidthAndHeight) {
                if (typeof strict == 'undefined') strict = true;
                if (typeof getWidthAndHeight == 'undefined') getWidthAndHeight = false;
                
                if (!element.length) {
                    throw "No HTML element found to embed.";
                }
                
                if (strict && !element.hasClass('ndn_embed')) {
                    throw "The configurable HTML element is not embeddable as it does not have the required \"ndn_embed\" class.";
                }
                
                var result = {};
                
                $.each(element.get(0).attributes, function(index, attribute) {
                    var matches;
                    // It is odd that the leading and trailing whitespace from attribute names must be trimmed 
                    if (matches = attribute.name.toLowerCase().match(/^\s*data-config-(.*)\s*$/)) {
                        var configSettingId = matches[1].replace(/-(\w)/g, function(match, p1) {
                            return p1.toUpperCase();
                        });
                        
                        result[configSettingId] = attribute.value;
                    }
                });
                
                // console.log('element.css("width") == ', element.prop("style")[$.camelCase('width')]);

                if (getWidthAndHeight) {
                    var getInlineCssProperty = function(element, propertyName) {
                        // Looking for the "data-style" attribute instead of the "style" attribute (see Chrome's limitations on using the "style" attribute)
                        var styles = element.attr("data-style"),
                            value;
                        
                        styles && styles.split(";").forEach(function(e) {
                            var style = e.split(":");

                            // console.log('style ==', style);
                            if ($.trim(style[0]) === propertyName) {
                                value = style[1];           
                            }                    
                        });
                        
                        return value;
                    };
                    
                    var width = getInlineCssProperty(element, 'width');
                    if ((width + '').indexOf('px') !== -1 && width !== NaN) {
                        result['width'] = parseInt(width);
                    }

                    var height = getInlineCssProperty(element, 'height');
                    if ((height + '').indexOf('px') !== -1 && height !== NaN) {
                        result['height'] = parseInt(height);
                    }
                }
                
                return result;
            },
            
            /**
             * @param {Object} config Structured data concerning an embedded widget's config
             * @param {String} appDomain The domain name where the application is hosted, ie. "launch.newsinc.com" or "qa-launch.newsinc.com"
             * @param {Object} widgetData Structured data concerning an embedded widget. Format:
             * <pre>{
             *  'container': {
             *      'width': int,
             *      'height': int
             *  }
             * }</pre>
             * @param {Array<String>} [configSettingsToIgnore] A list of config settings to ignore when generating the appropriate embed code (if not provided, defaults to a list specifying "acceptsConfigFromUrl" and "acceptsConfigFromUrlPrefix")
             * @returns {String} The configured <div> element for the JavaScript embed code representing the appropriately configured widget
             */
            getJsEmbedCodeFromConfig: function(config, appDomain, widgetData, configSettingsToIgnore) {
                // console.log('Ndn_Utils_PlayerSuiteApp.getJsEmbedCodeFromConfig(', arguments, ');');
                
                if (typeof configSettingsToIgnore == 'undefined') {
                    configSettingsToIgnore = ['acceptsConfigFromUrl', 'acceptsConfigFromUrlPrefix'];
                }
                
                var domAttributes = {};
                
                $.each(config, function(index, value) {
                    // If this config setting is not in the provided list of config settings to ignore and if the config setting's value is omittable
                    if ($.inArray(index, configSettingsToIgnore) === -1 && _isConfigSettingNeeded(index, value)) {
                        var attributeName = 'data-config-' + index.replace(/([A-Z])/g, function(match, p1) {
                            return '-' + p1.toLowerCase();
                        });
                        
                        domAttributes[attributeName] = (value + '').replace('"', '\\"');
                    }
                });
                
                (function() {
                    var type = (config.type + '').toLowerCase();
                    if (type == 'videoplayer/single' || type == 'videoplayer/default' || type == 'videolauncher/horizontal') {
                        if (typeof domAttributes['data-config-width'] == 'undefined') {
                            domAttributes['data-config-width'] = widgetData.container.width;
                        }
                    }
                })();
                
                (function() {
                    var type = (config.type + '').toLowerCase();
                    if (type == 'videoplayer/single' || type == 'videoplayer/default') {
                        if (typeof domAttributes['data-config-height'] == 'undefined') {
                            domAttributes['data-config-height'] = widgetData.container.height;
                        }
                    }
                })();
                
                /*
                // Include the appropriate "style" attribute to the embed code
                var styleAttribute = (domWidth || domHeight) 
                    ? (' style="' + (domWidth ? ('width:' + domWidth + 'px;') : '') + (domHeight ? ('height:' + domHeight + 'px;') : '') + '"')
                    : '';
                */
                
                var result = '';
                
                /*
                result += "<!-- Place the <script> tag in the webpage's <head></head> section -->\n" +
                    '<' + 'script type="text/javascript" async src="http://' + appDomain + '/js/embed.js"></' + 'script>' + "\n\n";
                */
                
                result += '<div' + "\n\t" + 'class="ndn_embed"'; // + styleAttribute;
                
                $.each(domAttributes, function(attributeName, attributeValue) {
                    result += "\n\t" + attributeName + '="' + attributeValue + '"';
                });
                
                return result + '></div>';
            }
        };
    })();
    
    return Ndn_Utils_PlayerSuiteApp;
});
