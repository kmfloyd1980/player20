/*
	Base.js, version 1.1a
	Copyright 2006-2009, Dean Edwards
	License: http://www.opensource.org/licenses/mit-license.php
*/
define(['jquery'], function(jQuery) {
    var Ndn_Utils_Base = (function() {
    	var Base = function() {
    		// dummy
    	};
    
    	Base.extend = function(_instance, _static) { // subclass
    		var extend = Base.prototype.extend;
    		
    		// build the prototype
    		Base._prototyping = true;
    		var proto = new this;
    		extend.call(proto, _instance);
    	  proto.base = function() {
    	    // call this method from any other method to invoke that method's ancestor
    	  };
    		delete Base._prototyping;
    		
    		// create the wrapper for the constructor function
    		//var constructor = proto.constructor.valueOf(); //-dean
    		var constructor = proto.constructor;
    		var klass = proto.constructor = function() {
    			if (!Base._prototyping) {
    				if (this._constructing || this.constructor == klass) { // instantiation
    					this._constructing = true;
    					constructor.apply(this, arguments);
    					delete this._constructing;
    				} else if (arguments[0] != null) { // casting
    					return (arguments[0].extend || extend).call(arguments[0], proto);
    				}
    			}
    		};
    		
    		// build the class interface
    		klass.ancestor = this;
    		klass.extend = this.extend;
    		klass.forEach = this.forEach;
    		klass.implement = this.implement;
    		klass.prototype = proto;
    		klass.toString = this.toString;
    		klass.valueOf = function(type) {
    			//return (type == "object") ? klass : constructor; //-dean
    			return (type == "object") ? klass : constructor.valueOf();
    		};
    		extend.call(klass, _static);
    		// class initialisation
    		if (typeof klass.init == "function") klass.init();
    		return klass;
    	};
    
    	Base.prototype = {	
    		extend: function(source, value) {
    			if (arguments.length > 1) { // extending with a name/value pair
    				var ancestor = this[source];
    				if (ancestor && (typeof value == "function") && // overriding a method?
    					// the valueOf() comparison is to avoid circular references
    					(!ancestor.valueOf || ancestor.valueOf() != value.valueOf()) &&
    					/\bbase\b/.test(value)) {
    					// get the underlying method
    					var method = value.valueOf();
    					// override
    					value = function() {
    						var previous = this.base || Base.prototype.base;
    						this.base = ancestor;
    						var returnValue = method.apply(this, arguments);
    						this.base = previous;
    						return returnValue;
    					};
    					// point to the underlying method
    					value.valueOf = function(type) {
    						return (type == "object") ? value : method;
    					};
    					value.toString = Base.toString;
    				}
    				this[source] = value;
    			} else if (source) { // extending with an object literal
    				var extend = Base.prototype.extend;
    				// if this object has a customised extend method then use it
    				if (!Base._prototyping && typeof this != "function") {
    					extend = this.extend || extend;
    				}
    				var proto = {toSource: null};
    				// do the "toString" and other methods manually
    				var hidden = ["constructor", "toString", "valueOf"];
    				// if we are prototyping then include the constructor
    				var i = Base._prototyping ? 0 : 1;
    				while (key = hidden[i++]) {
    					if (source[key] != proto[key]) {
    						extend.call(this, key, source[key]);
    
    					}
    				}
    				// copy each of the source object's properties to this object
    				for (var key in source) {
    					if (!proto[key]) extend.call(this, key, source[key]);
    				}
    			}
    			return this;
    		}
    	};
    	
    	// If jQuery is accessible globally, override the .append() method to allow for the direct appending of Ndn_Utils_Base instances.
    	if (jQuery) {
    		(function() {
    			var originalMethod = jQuery.fn.append;
    			
    			jQuery.fn.append = function() {
    				// console.log('jQuery.fn.append(', arguments, ');');
    				
    				if (arguments[0] && arguments[0].__isBaseComponent) {
    					// console.log('About to call arguments[0].appendTo(this);...');
    					arguments[0].appendTo(this);
    				}
    				else {
    					if (jQuery.isArray(arguments[0])) {
    						// console.log('Array of elements passed to jQuery.append...');
    						var component = this;
    						
    						jQuery.each(arguments[0], function(index, value){
    							jQuery.fn.append.apply(component, [this]);
    						});
    					}
    					else {
    						// console.log('About to apply original .append() method...');
    						originalMethod.apply(this, arguments);
    					}
    				}
    				
    				return this;
    			};
    		})();
    	}
    
    	// initialise
    	Base = Base.extend({
    		constructor: function() {
    			this.extend(arguments[0]);
    		}
    	}, {
    		ancestor: Object,
    		version: "1.1",
    		
    		forEach: function(object, block, context) {
    			for (var key in object) {
    				if (this.prototype[key] === undefined) {
    					block.call(context, object[key], key, object);
    				}
    			}
    		},
    			
    		implement: function() {
    			for (var i = 0; i < arguments.length; i++) {
    				if (typeof arguments[i] == "function") {
    					// if it's a function, call it
    					arguments[i](this.prototype);
    				} else {
    					// add the interface using the extend method
    					this.prototype.extend(arguments[i]);
    				}
    			}
    			return this;
    		},
    		
    		toString: function() {
    			return String(this.valueOf());
    		}
    	});
    	
    	return Base.extend({
    		__isBaseComponent: true,
    		
    		constructor: function() {
    			/**
    			 * @var array An associative array mapping custom event names to a structured array. Format:
    			 * <pre>{
    			 * 	string: [ // The name of the custom event, ie. 'change'
    			 * 		{ // Structured array describing the observing object
    			 * 			'context': object, // An object to apply the associated function with
    			 * 			'callback': function // The associated function to apply to the provided context
    			 * 		},
    			 * 		... // Any other structured arrays describing additional observing objects
    			 * 	]
    			 * }</pre>
    			 */
    			this.__observers = [];
    		},
    
    		/**
    		 * Initializes a custom event listener for this object.
    		 * @param string|array event The name of the event that should be handled. Alternatively, an array of event names may be provided.
    		 * For example:
    		 * <pre>[
    		 * 	string, // The name of the event that should be handled
    		 * 	... // Any other event names that should also have the provided callback function executed when the event is triggered
    		 * ]</pre>
    		 * @param function callback The function to apply to the observer object when the custom event specified is triggered
    		 * @return Observable This instance to allow for function chaining
    		 */
    		on: function(event, callback) {
    			if (jQuery.isArray(event)) {
    				var eventNames = event;
    				
    				for (var i = 0; i < eventNames.length; i++) {
    					var eventName = eventNames[i];
    					
    					this.bind(eventName, callback);
    				}
    			}
    			else {
    				var eventName = event;
    				
    				// If the needed event observers array has not yet been defined, define it
    				this.__observers = this.__observers || [];
    				this.__observers[eventName] = this.__observers[eventName] || [];
    				
    				this.__observers[eventName].push({
    					'callback': callback
    				});
    			}
    			
    			return this;
    		},
    
    		/**
    		 * Triggers the specified event(s) so that functions observing this event may be executed. 
    		 * @param string events The name of the event to trigger. Alternatively, an array of event names may be provided.
    		 * For example:
    		 * <pre>[
    		 * 	string, // The name of the event that should be triggered
    		 * 	... // Any other event names that should also be triggered
    		 * ]</pre>
    		 * @return this
    		 */
    		trigger: function(events, eventData) {
    			if (jQuery.isArray(events)) {
    				for (var i = 0; i < events.length; i++) {
    					var eventName = events[i];
    					
    					this.trigger(eventName, eventData);
    				}
    			}
    			else {
    				var eventName = events;
    				
    				if (typeof this.__observers[eventName] != 'undefined') {
    					for (var i = 0; i < this.__observers[eventName].length; i++) {
    						var data = this.__observers[eventName][i];
    						var callback = data['callback'];
    						
    						callback.apply(this, [eventData]);
    					}
    				}
    			}
    			
    			return this;
    		},
    		
    		initializeInterface: function() {
    			if (!this._dom || !this._dom.container) {
    				this.initializeDom();
    				this.initializeEventListeners();
    				this.synchronizeInterface();
    			}
    		},
    		
    		initializeEventListeners: function() {},
    		
    		synchronizeInterface: function() {},
    		
    		/**
    		 * @param {DOMNode|jQuerySelector} component The dom element to append this component's container element to
    		 */
    		appendTo: function(component) {
    			// console.log('Base.appendTo(', arguments, ');');
    			
    			this.initializeInterface();
    			
    			if (this._dom && this._dom.container) {
    				this._dom.container.appendTo(component);
    				return this;
    			}
    			else {
    				throw "This component does not have a container dom element.";
    			}
    		}
    	});
    })();
    
    return Ndn_Utils_Base;
});