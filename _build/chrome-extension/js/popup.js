var NDN_PLAYER_SUITE_CHROME_EXTENSION = NDN_PLAYER_SUITE_CHROME_EXTENSION || {};
NDN_PLAYER_SUITE_CHROME_EXTENSION.realm = 'popup';

/**
 * TODO: REMOVE THIS GLOBAL VARIABLE.
 * 
 * The most recently created sandbox html generated via either the "Sandbox product:" option or the "Sandbox All Products" link
 * @var {String}
 */
var _mostRecentlyCreatedSandboxHtml = '';

var popup;

require([
    'jquery',
    'models/Ndn/ChromeExt/Popup'
], function(
    $,
    Ndn_ChromeExt_Popup
) {
    $(function() {
        popup = new Ndn_ChromeExt_Popup();
    });
});