var NDN_PLAYER_SUITE_CHROME_EXTENSION = NDN_PLAYER_SUITE_CHROME_EXTENSION || {};
NDN_PLAYER_SUITE_CHROME_EXTENSION.realm = 'background';

/**
 * Structured data concerning the embedded products detected on each tab. Format:
 * <pre>{
 *  int: { // The tab ID => structured data concerning the embeds detected on that tab
 *      String: Object, // The embedded product's container element's "id" attribute => structured data concerning the embedded product
 *      ... // All other embedded products for concerning this tab
 *  },
 *  ... // All other tabs
 * }</pre>
 * @var {Object}
 */
var embeds = embeds || {};

/**
 * The ID of the currently selected tab
 * @var {int}
 */
var selectedTabId = null;

/**
 * @var {Ndn_ChromeExt_BrowserTabs}
 */
var browserTabs;

/**
 * Responsible for managing this extension's interaction with the browser's omnibox
 * @var {Ndn_ChromeExt_BrowserOmnibox}
 */
var omnibox;

require([
    'jquery',
    'underscore',
    'models/Ndn/ChromeExt',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/ChromeExt/BrowserTabs',
    'models/Ndn/ChromeExt/BrowserOmnibox',
    
    'lib/require/plugins/text'
], function(
    $,
    _,
    Ndn_ChromeExt,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_ChromeExt_BrowserTabs,
    Ndn_ChromeExt_BrowserOmnibox
) {
    browserTabs = new Ndn_ChromeExt_BrowserTabs();
    
    omnibox = new Ndn_ChromeExt_BrowserOmnibox();
    
    Ndn_ChromeExt.initialize();
});

