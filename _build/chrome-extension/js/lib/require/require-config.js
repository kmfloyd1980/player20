require.config({
    baseUrl: chrome.extension.getURL('js'),
    
    // waitSeconds: 0, // Do not timeout for requiring dependencies
    // optimizeAllPluginResources: true,
    
    paths: {
        jquery: 'lib/jquery/jquery',
        underscore: 'lib/underscore'
    }
});