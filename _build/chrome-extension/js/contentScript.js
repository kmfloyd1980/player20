var NDN_PLAYER_SUITE_CHROME_EXTENSION = NDN_PLAYER_SUITE_CHROME_EXTENSION || {};
NDN_PLAYER_SUITE_CHROME_EXTENSION.realm = 'contentScript';

require([
    'jquery',
    'underscore',
    'models/Ndn/ChromeExt',
    'models/Ndn/Utils/PlayerSuiteApp',
    'models/Ndn/Utils/UrlParser',
    
    'lib/jquery/plugins/jquery.scrollTo.min',
    'lib/require/plugins/text'
], function(
    $,
    _,
    Ndn_ChromeExt,
    Ndn_Utils_PlayerSuiteApp,
    Ndn_Utils_UrlParser
) {
    var isMainWindow = (function() {
        if (Ndn_Utils_UrlParser.getUrlVar('ndn_playerSuiteSandbox') == 'true') {
            return true;
        }
        else if (window == top) {
            if (document.getElementById('ndn-player-suite-sandbox-iframe')) {
                return false;
            }
            else {
                return true;
            }
        }
        
        return false;
    })();

    // Keep track of what the "main" window's origin is, as well as what the top window's origin is (these origins are used for messaging)
    if (window == top) {
        Ndn_ChromeExt.updateTabData({
            topWindowOrigin: window.location.protocol + '//' + window.location.hostname,
            topWindowHref: window.location.href + ''
        });
    }
    if (isMainWindow) {
        Ndn_ChromeExt.updateTabData({
            mainWindowOrigin: window.location.protocol + '//' + window.location.hostname
        });
    }

    // If the "main" window is not the top window, enable messaging that relays its messages to the "main" window. This is needed because the webpage 
    // always sends its responses to the top window, and it is the "main" window that needs to hear/process the responses.
    if (!isMainWindow && window == top) {
        var sandboxIframeOrigin,
            topWindowOrigin = window.location.protocol + '//' + window.location.hostname,
            getSandboxIframeOrigin = function() {
                return $.Deferred(function(deferred) {
                    if (typeof sandboxIframeOrigin != 'undefined') {
                        deferred.resolve(sandboxIframeOrigin);
                    }
                    else {
                        $(function() {
                            var src = $('#ndn-player-suite-sandbox-iframe').attr('src'),
                                matches = (src + '').match(/^(https?:)?\/\/([^\/])*/);
                            
                            sandboxIframeOrigin = matches[0] || '';
                            
                            deferred.resolve(sandboxIframeOrigin);
                        });
                    }
                }).promise();
            };
        
        window.addEventListener("message", function(message) {
            if (message && message.data) { // && message.data._nw2eOrigin) {
                getSandboxIframeOrigin().done(function(origin) {
                    // console.log('[X] sandboxIframeOrigin ==', origin, '; CONTENT SCRIPT RECEIVED DATA FROM PLAYER SUITE: message.data ==', message.data);
                    
                    document.getElementById('ndn-player-suite-sandbox-iframe').contentWindow.postMessage($.extend(message.data, {
                        _nw2eOrigin: topWindowOrigin
                    }), origin);
                });
            }
        });
    }

    // console.log(' [*] >>>>>>>> CONTENT SCRIPT LOADED.... isMainWindow ==', isMainWindow, '; window.location.href ==', window.location.href);
    // console.log("Ndn_Utils_UrlParser.getUrlVar('ndn_playerSuiteSandbox') ==", Ndn_Utils_UrlParser.getUrlVar('ndn_playerSuiteSandbox'));
    if (isMainWindow) {
        /**
         * Structured data concerning which embeds have already been detected identified by their dom container "id" attribute
         * @var {Object}
         */
        var embedContainerIdsDetected = {};
        
        /**
         * Whether communication has been established yet with the NDN Player Suite on the current tab's top window
         * @var {Boolean}
         */
        var hasConnectedWithWindowPlayerSuite = false;
        
        /**
         * Structured data concerning whether iframe data has been received for elements with specified "id" attributes. Format:
         * <pre>{
         *  String: true, // The "id" attribute of an <iframe> element that has been received for this tab
         *  ... // All other "id" attributes belonging to detected <iframes> for this page
         * }</pre>
         * @var {Object}
         */
        var iframeDataReceived = {};
        
        function isContainerIdForIframe(containerId) {
            return !!iframeDataReceived[containerId];
        }
        
        /**
         * Whether the "legacyMigrationAddedEmbedJs" response has been received 
         * @var {Boolean}
         */
        var legacyMigrationAddedEmbedJsResponseReceived = false;
        
        /**
         * Structured data concerning the debug info requests. Format:
         * <pre>{
         *  {String} => {Function}, // The "id" attribute of the container <div> => the "sendResponse" function to respond to the "popup.js" file
         *  ... // Any other pending debug info requests
         * }</pre>
         * @var {Object}
         */
        var _debugInfoRequests = {};
        
        /**
         * Structured data concerning info requests. Format:
         * <pre>{
         *  {String} => {Function}, // The "id" attribute of the container <div> => the "sendResponse" function to respond to the "popup.js" file
         *  ... // Any other pending debug info requests
         * }</pre>
         * @var {Object}
         */
        var _infoResponders = {};
        
        /**
         * @return {String} The next default "id" attribute to assign to embedded iframes without an "id" attribute already assigned
         */
        var getNextNdnWidgetIframeUid = (function() {
            var nextUid = 1;
            return function() {
                return 'ndn-widget-iframe-embed-' + nextUid++;
            };
        })();
        
        function scrollToElement(elementId) {
            $.scrollTo('#' + elementId, 'fast', {
                onAfter: function() {
                    $('#' + elementId).hide().fadeIn('fast');
                }
            });
        }
        
        function detectHardcodedBuildNumbers() {
            $(function() {
                // console.log('detectHardcodedBuildNumbers();');
        
                // Determine whether the window has used the forced legacy migration via JavaScript
                echoClosure(function() {
                    if (!legacyMigrationAddedEmbedJsResponseReceived) {
                        sendMessageToWindow('legacyMigrationAddedEmbedJs?');
                    }
                });
                
                var currentPageUrlInfo = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(window.location.href + '');
                if (currentPageUrlInfo && currentPageUrlInfo.buildNumber) {
                    chrome.runtime.sendMessage({hardcodedBuildNumberDetected: true}, function() {});
                }
                
                $('script').each(function() {
                    var appUrlInfo = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo($(this).attr('src'));
                    
                    if (appUrlInfo && appUrlInfo.path == '/js/embed.js' && appUrlInfo.buildNumber) {
                        // Register that a hardcoded build number has been detected in "background.js"
                        chrome.runtime.sendMessage({hardcodedBuildNumberDetected: true}, function() {});
                        
                        // No longer need to traverse the rest of the <script> elements since one was detected
                        return false;
                    }
                });
            });
        }
        
        function debug(str) {
            // Register the embedded product's data in "background.js"
            chrome.runtime.sendMessage({debug: str}, function() {});
        }
        
        function clearDebugConsole() {
            // Register the embedded product's data in "background.js"
            chrome.runtime.sendMessage({clearDebug: true}, function() {});
        }
        
        function detectIframeEmbeds() {
            // console.log('>>>>>> * >>>> detectIframeEmbeds();');
            
            $(function() {
                // Scan the page for any iframes that may be embedded NDN Player Suite products
                $('iframe').each(function() {
                    var iframeSrc = $(this).attr('src') + '',
                        isAppIframe = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(iframeSrc) || iframeSrc.match(/^http:\/{2}embed\.newsinc\.com\//) || $(this).hasClass('ndn_embed_iframe');
                    
                    if (isAppIframe) {
                        requestEmbedDataFromIframe($(this));
                    }
                });
            });
        }
        
        /**
         * @param {Function} closure The logic to repeat x-number of times at the intervals specified within this method
         * @param {Array<int>} [times] The intervals (measured in seconds) to repeat the provided logic after the first call is made (defaults to [1, 2, 4, 8])
         */
        var echoClosure = function(closure, times) {
            // Call the closure immediately the first time
            closure();
            
            if (!times) times = [1, 2, 4, 8];
            for (var i = 0; i < times.length; i++) {
                var time = times[i];
        
                setTimeout(function() {
                    closure();
                }, time * 1000);
            }
        };
        
        /**
         * Request embed data from the provided iframe element
         * @param {jQueryElement} iframeElement
         */
        var requestEmbedDataFromIframe = function(iframeElement) {
            // console.log('requestEmbedDataFromIframe(', arguments, ');');
            
            // If this iframe element does not have an "id" attribute yet, give it one
            if (!iframeElement.attr('id')) {
                iframeElement.attr('id', getNextNdnWidgetIframeUid());
            }
            
            var iframeElementId = iframeElement.attr('id');
            
            // Repeatedly request embed data from this iframe (in case the NDN Player Suite takes a long time to load)
            echoClosure(function() {
                // If information about this iframe has not been received yet
                if (!iframeDataReceived[iframeElementId]) {
                    sendMessageToIframe(iframeElementId, 'getEmbedData', {
                        containerId: iframeElementId
                    });
                }
            });
        };
        
        var requestEmbedDataFromWindow = function() {
            // Send message to page with NDN's Player Suite library already embedded on the page to use window.postMessage() to receive information
            // about products embedded on the page
            echoClosure(function() {
                // If no information has been received from the NDN Player Suite's top window, then attempt to communicate with it
                if (!hasConnectedWithWindowPlayerSuite) {
                    sendMessageToWindow('getEmbedData');
                }
            });
        };
        
        // Listen to window.postMessage() messages that can be received from a webpage that has the NDN Player Suite JavaScript library included on the page
        window.addEventListener("message", function(message) {
            if (message && message.data) {
                // console.log('[YES!!] CONTENT SCRIPT RECEIVED DATA FROM PLAYER SUITE: message.data ==', message.data);
        
                if (message.data._nw2e == 'reply' && message.data._nw2eMessageId) {
                    // If this message is a reply to a previously sent message from the Chrome extension, handle the response data appropriately
                    _openMessages[message.data._nw2eMessageId](message.data);
                }
                else if (message.data._nw2e == 'widgetEmbed') {
                    var widgetData = message.data.widget;
                    
                    // console.log('widgetData ==', widgetData);
                    
                    if (!embedContainerIdsDetected[widgetData.containerId]) {
                        // debug('New "widgetEmbed" has been received by contentScript.js!!!');
                        
                        // Remember that this embedded product has been detected
                        embedContainerIdsDetected[widgetData.containerId] = true;
                        
                        // If this product data is coming from the top window (and not from within an iframe)
                        if (!widgetData.isIframe) {
                            hasConnectedWithWindowPlayerSuite = true;
                        }
                        else {
                            iframeDataReceived[widgetData.containerId] = true;
                        }
                        
                        if (widgetData.appUrl == '') {
                            widgetData.appUrl = window.location.protocol + '//' + window.location.hostname;
                        }
                
                        // Register this embed data with the "background.js" file
                        chrome.runtime.sendMessage({registerEmbed: widgetData}, function() {});
                    }           
                }
                else if (message.data._nw2e == 'widgetDestruct') {
                    chrome.runtime.sendMessage({unregisterEmbed: message.data.containerElementId}, function() {});
                }
                else if (message.data._nw2e == 'widgetDebugInfo') {
                    // console.log('Received:::::::::::::::widgetDebugInfo, message.data ==', message.data); // _debugInfoRequests[message.data.containerId] ==', _debugInfoRequests[message.data.containerId]);
                    
                    // Handle debug information received from NDN Player Suite by relaying the debug info appropriately back to the "popup.js" file
                    _debugInfoRequests[message.data.containerId](message.data.debugInfo);
                    
                    // console.log('<<SENT>>>');
                }
                else if (message.data._nw2e == 'legacyMigrationAddedEmbedJs') {
                    legacyMigrationAddedEmbedJsResponseReceived = true;
                    
                    // Register this embed data with the "background.js" file
                    chrome.runtime.sendMessage({legacyMigrationAddedEmbedJs: message.data.value}, function() {});
                }
                else if (message.data._nw2e == 'getProductTimelineData') {
                    // Handle the information received from NDN Player Suite by relaying the this info appropriately back to the "popup.js" file
                    _infoResponders[message.data.containerId](message.data.events);
                }
                else if (message.data._nw2e == 'getDebugLog') {
                    chrome.runtime.sendMessage({'getDebugLog': true}, function(response) {
                        // console.log('>>>>>> [getDebugLog] response ==', response);
                    });
                }
                else if (message.data._nw2e == 'getRoleData') {
                    Ndn_ChromeExt.getExtensionOptions().done(function(options) {
                        sendMessageToWindow('roleData', {
                            role: options.role
                        });
                    });
                }
            }
        }, false);
        
        function handleEmbedDetection() {
            // console.log('[NDN Player Suite Chrome Extension] contentScript.js handleEmbedDetection();');
            
            $(function() {
                // Detect iframe embeds using contentScript.js
                echoClosure(function() {
                    detectIframeEmbeds();
                });
                
                // Detect any hardcoded build numbers in NDN Player Suite <script> elements
                detectHardcodedBuildNumbers();
                
                requestEmbedDataFromWindow();
                // awaitPlayerSuiteResponse();
            });
        }
        
        function getContentScriptOrigin() {
            return window.location.protocol + '//' + window.location.hostname;
        }
        
        /**
         * @param {jQueryElement} iframeElement 
         * @return The origin URL to use for passing messages via window.postMessage() for the provided iframe element
         */
        function getIframeEmbedOrigin(iframeElement) {
            // console.log('>>>> getIframeEmbedOrigin(', iframeElement, ');');
            
            if (iframeElement.attr('src')) {
                var urlInfo = Ndn_Utils_PlayerSuiteApp.getAppUrlInfo(iframeElement.attr('src'));
                if (urlInfo) {
                    return urlInfo.baseUrl;
                }
                else if (iframeElement.attr('src').match(/^http:\/{2}embed\.newsinc\.com\//)) {
                    return 'http://launch.newsinc.com';
                }
            }
            else {
                return window.location.origin;
            }
        }
        
        /**
         * Structured data concerning the messages that have been sent from this Chrome extension, but are awaiting a reply to all messages sent. Format:
         * <pre>{
         *  String: Function, // The message's unique identifier => the function to handle receiveing the data of this message that was sent out to the browser's page
         *  ... // All other open messages awaiting replies
         * }</pre>
         * @var {Object}
         */
        var _openMessages = {};
        
        /**
         * @param {String} messageSubject The subject of the message to send
         * @param {Object} [messageData] Data to send to the window and <iframe> embedded products if specified (defaults to an empty Object)
         * @param {Boolean} [sendToIframeEmbeds] Whether to send this message to all <iframe> embedded products as well (defaults to false)
         */
        function sendMessageToWindow(messageSubject, messageData, sendToIframeEmbeds) {
            messageData = messageData || {};
            sendToIframeEmbeds = sendToIframeEmbeds || false;
            
            Ndn_ChromeExt.getTopWindowOrigin().done(function(topWindowOrigin) {
                // Add the "return address" to our message so that the NDN Player Suite will know which origin to send a response to
                messageData = $.extend({}, messageData, {
                    _nw2e: messageSubject,
                    _nw2eOrigin: topWindowOrigin
                });
                
                // Send message to top window frame
                top.postMessage(messageData, topWindowOrigin);
                
                // If sending this message to iframe embedded products as well, do so
                if (sendToIframeEmbeds) {
                    $.each(iframeDataReceived, function(iframeElementId) {
                        sendMessageToIframe(iframeElementId, messageSubject, messageData);
                    });
                }
            });
        }
        
        /**
         * @param {String} containerId The "id" attribute of the <iframe> element responsible for containing the specified embedded product
         * @param {String} messageSubject The subject of the message to send
         * @param {Object} [messageData] Data to send to the window and <iframe> embedded products if specified (defaults to an empty Object)
         */
        function sendMessageToIframe(iframeElementId, messageSubject, messageData) {
            // console.log('>>>> sendMessageToIframe(', arguments, ');');
            
            messageData = messageData || {};
            
            Ndn_ChromeExt.getTopWindowOrigin().done(function(topWindowOrigin) {
                // Add the "return address" to our message so that the NDN Player Suite will know which origin to send a response to
                messageData = $.extend({}, messageData, {
                    _nw2e: messageSubject,
                    _nw2eOrigin: topWindowOrigin
                });
                
                var iframeElement = $('#' + iframeElementId);
                if (iframeElement.length) {
                    /*
                    console.log('*************************');
                    console.log('iframeElement ==', iframeElement);
                    console.log('getIframeEmbedOrigin(iframeElement) ==', getIframeEmbedOrigin(iframeElement));
                    console.log('*************************');
                    */
                    
                    iframeElement.get(0).contentWindow.postMessage(messageData, getIframeEmbedOrigin(iframeElement));
                }
            });
        }
        
        /**
         * Send the provided message data to the main window by sending the message data to the top window (which may or may not be the main window). 
         * If the top window is not the main window, the top window will redirect its received message to main window.
         * @param {Object} messageData Structured data concerning the message intended for the main window
         */
        function sendMessageDataToMainWindow(messageData) {
            Ndn_ChromeExt.getTopWindowOrigin().done(function(topWindowOrigin) {
                // console.log('topWindowOrigin RECIEVED ==',topWindowOrigin);
                
                top.postMessage($.extend(messageData, {
                    _nw2eOrigin: topWindowOrigin
                }), topWindowOrigin);
            });
        }
        
        /**
         * @param {String} containerId The "id" attribute of the <div> or <iframe> element responsible for containing the specified embedded product
         * @param {String} messageSubject The subject of the message to send
         * @param {Object} [messageData] Data to send to the window and <iframe> embedded products if specified (defaults to an empty Object)
         */
        function sendMessageToEmbeddedProduct(containerId, messageSubject, messageData) {
            messageData = messageData || {};
            
            Ndn_ChromeExt.getTopWindowOrigin().done(function(topWindowOrigin) {
                // Add the "return address" to our message so that the NDN Player Suite will know which origin to send a response to
                $.extend(messageData, {
                    _nw2e: messageSubject,
                    _nw2eOrigin: topWindowOrigin
                });
                
                if (isContainerIdForIframe(containerId)) {
                    var iframeElement = $('#' + containerId);
                    if (iframeElement.length) {
                        iframeElement.get(0).contentWindow.postMessage(messageData, getIframeEmbedOrigin(iframeElement));
                    }
                }
                else {
                    // Send message to top window frame
                    top.postMessage(messageData, topWindowOrigin); // window.postMessage(messageData, contentScriptOrigin);
                }
            });
        }
        
        function getLastClickedContainerId(frameUrl) {
            return $.Deferred(function(deferred) {
                // Remember the timestamp just before all the messages are sent out to the page
                var startOfRequestTimestamp = Date.now();
                
                var hasProcessedData = false;
                var processLastClickedData = function(responses) {
                    // console.log('processLastClickedData(', responses, ');');
                    
                    // Make sure this closure's logic is only executed once
                    if (!hasProcessedData) {
                        hasProcessedData = true;
                        
                        // The timestamp of the latest click found
                        var latestTimestamp,
                            lastClickedContainerId;
                        
                        $.each(responses, function(index, responseData) {
                            // Only consider timestamps that have happened prior to messages being sent out to the page
                            if (responseData.timestamp <= startOfRequestTimestamp) {
                                // If this is the latest timestamp found, remember the container "id" attribute that identifies this embedded product
                                if (!latestTimestamp || responseData.timestamp > latestTimestamp) {
                                    lastClickedContainerId = responseData.containerId;
                                }
                            }
                        });
                        
                        deferred.resolve(lastClickedContainerId);
                    }
                };
                
                chrome.runtime.sendMessage({getTabData: true}, function(tabData) {
                    // Whether the last clicked embed container was within an iframe
                    var lastClickedInIframe = !!frameUrl;
                    
                    var numberOfMessagesSent = 0,
                        messageSubject = 'getLastClickedContainerData',
                        messageId = Date.now(),
                        messageData = {
                            _nw2eMessageId: messageId   
                        };
                    
                    var messages = [];
                    _openMessages[messageId] = function(messageData) {
                        if (messageData) messages.push(messageData);
                        return messages;
                    };
        
                    // console.log('lastClickedInIframe ==', lastClickedInIframe);
                    
                    if (lastClickedInIframe) {
                        // Send messages to all iframes on the page, requesting information about its last clicked embedded product
                        $.each(tabData.embeddedProductsData, function(index, productData) {
                            // console.log('searching through productData, productData ==', productData);
                            
                            if (productData.isIframe) { 
                                sendMessageToIframe(productData.containerId, messageSubject, messageData);
                                numberOfMessagesSent++;
                            }
                        });
                    }
                    else {
                        // console.log('about to send message to window...');
                        
                        sendMessageToWindow(messageSubject, messageData);
                        numberOfMessagesSent++;
                    }
                    
                    _openMessages[messageId] = (function() {
                        var messages = _openMessages[messageId]();
                        
                        // console.log('messages ==', messages);
                        
                        // If the number of messages that have been received equal (or exceed -- as a precaution) the number of messages sent...
                        if (messages.length >= numberOfMessagesSent) {
                            processLastClickedData(messages);
                            
                            return function() {};
                        }
                        
                        return function(messageData) {
                            if (messageData) {
                                messages.push(messageData);
                            }
                            
                            // If the number of messages that have been received equal (or exceed -- as a precaution) the number of messages sent...
                            if (messages.length >= numberOfMessagesSent) {
                                processLastClickedData(messages);
                            }
                        };
                    })();
                });
            }).promise();
        }

        // Apparently "chrome.extension.onRequest" must be used instead of "chrome.runtime.onMessage", whereas "chrome.runtime.onMessage" does not allow the storing of the "sendResponse" closure
        chrome.extension.onRequest.addListener(function(req, sender, sendResponse) {
            if (req.requestDebugInfo) {
                var containerId = req.requestDebugInfo;
                
                // Store the closure to send a reply for this request once it is received from the NDN Player Suite
                _debugInfoRequests[containerId] = sendResponse;
                
                sendMessageToEmbeddedProduct(containerId, 'requestDebugInfo', {
                    containerId: containerId
                });
            }
            else if (req.getLastClickedContainerId) {
                // console.log('contentScript: detected getLastClickedContainerId request!!');
                
                getLastClickedContainerId(req.frameUrl).done(function(response) {
                    sendResponse(response);
                });
                
                /*
                chrome.runtime.sendMessage({getTabData: true}, function(tabData) {
                    sendResponse(tabData);
                });
                */
            }
            else if (req['getProductTimelineData?']) {
                var containerId = req['getProductTimelineData?'];
                
                // Store the closure to send a reply for this request once it is received from the NDN Player Suite
                _infoResponders[containerId] = sendResponse;
                
                sendMessageToEmbeddedProduct(containerId, 'getProductTimelineData?', {
                    containerId: containerId
                });
            }
        });
        
        // console.log('isMainWindow... about to add onMessage listeners...');
        
        // Listen for messages sent from other files within this extension, you may send response by calling "sendResponse(response);", using the "sendResponse" parameter
        chrome.runtime.onMessage.addListener(function(req, sender, sendResponse) { // chrome.extension.onRequest.addListener(function(req, sender, sendResponse) {
            // console.log('[>>] chrome.runtime.onMessage ... req == ' + JSON.stringify(req));
            
            if (req.getEmbeds) {
                // If the message received from within this extension is requesting that embedded products data be sent
                handleEmbedDetection();
                
                // sendResponse({});
                sendResponse({'from':'getEmbeds'});
            }
            else if (req.scrollTo) {
                // Scroll to the specified element on the page
                scrollToElement(req.scrollTo);
                sendResponse({'from':'scrollTo'});
            }
            else if (req.reloadTab) {
                window.location.reload();
            }
            else if (req.pause) {
                var containerIdToPause = req.pause;
                
                sendMessageToEmbeddedProduct(containerIdToPause, 'pause', {
                    containerId: containerIdToPause
                });
            }
            else if (req.pauseOthers) {
                var containerIdToNotPause = req.pauseOthers;
                
                // Notify the top window to pause all embeds other than the one specified
                sendMessageToWindow('pauseOthers', {
                    containerId: containerIdToNotPause
                });
                
                // Pause every <iframe> embed unless its "id" attribute matches the "id" of the embed not to pause
                $.each(iframeDataReceived, function(iframeElementId) {
                    // console.log('Checking iframeElementId == "' + iframeElementId + '" and containerIdToNotPause == "' + containerIdToNotPause + '"');
                    
                    if (iframeElementId != containerIdToNotPause) {
                        sendMessageToEmbeddedProduct(iframeElementId, 'pause');
                    }
                });
            }
            else if (req.detectHardcodedBuildNumbers) {
                detectHardcodedBuildNumbers();
                sendResponse({'from':'detectHardcodedBuildNumbers'});
            }
        });
    }
    
    // console.log('Done executing inside the contentScript.js module...');
});
