require([
    'jquery',
    'models/Ndn/ChromeExt'
], function(
    $,
    Ndn_ChromeExt
) {
    $('.ndn_optionsMainContainer').css('opacity', '0');

    $(function() {
        // Update the version number displayed on the options page once its loaded
        $('.ndn_versionNumber').html('v' + Ndn_ChromeExt.getVersion());
        
        synchronizeInterface();
        initializeEventListeners();
    });

    function synchronizeInterface() {
        // Retrieve the "role" option's value, defaulting to "am" if no option value has already been set
        chrome.storage.sync.get({
            "role": 'am',
            "sandbox_env": ''
        }, function(items) {
            // Update what role is currently selected
            document.getElementById('role').value = items.role;
            
            // Update whether the "sandbox dev" mode is enabled
            $('#sandbox-dev-mode').prop('checked', items.sandbox_env == 'dev');
            
            $('.ndn_optionsMainContainer').animate({
                'opacity': '1'
            }, 200);
        });
    }

    function initializeEventListeners() {
        $('#save').on('click', function() {
            var saveButton = this;
            var role = document.getElementById('role').value,
                sandboxDevModeEnabled = $('#sandbox-dev-mode').prop('checked');
            chrome.storage.sync.set({
                'role': role,
                'sandbox_env': sandboxDevModeEnabled ? 'dev' : ''
            }, function() {
                $(saveButton).html('Saved!');
            });
        });
        
        $('.ndn_optionsMainContainer').on('change', function() {
            $('#save').html('Save');
        });
    }
});
