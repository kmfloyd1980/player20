<?php

$trackingGroup = isset($_GET['freewheel']) ? $_GET['freewheel'] : '';
$containerElementId = isset($_GET['parent']) ? $_GET['parent'] : '';

$optInTrackingGroups = array(
	90121,
	91206,
	90277
);

$legacyEmbedUrl = explode('/demos/legacy-embed/', $_SERVER['REQUEST_URI']);
$legacyEmbedUrl = $legacyEmbedUrl[1];
$legacyEmbedUrl = explode('?', $legacyEmbedUrl);
$legacyEmbedUrl = $legacyEmbedUrl[0];

$urlParams = array();
foreach ($_GET as $key => $value) {
	$urlParams[] = "$key=$value";
}
$urlParams = '?' . implode('&', $urlParams);

if (in_array($trackingGroup, $optInTrackingGroups)) {
	$js = <<<JS
		var _nw2e = _nw2e || [];
	
		// If the new players 2.0 library has not been added to the page yet, do so
		if (!document.getElementById('_nw2e-js')) {
			(function() {
				var nw = document.createElement('script'); nw.id = '_nw2e-js'; nw.type = 'text/javascript'; nw.async = true; nw.src = 'http://spease.projects.assets.newsinc.com/media-widgets/js/main.js.php';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(nw, s);
			})();
		}
	
		_nw2e.push({
			containerElementId: '$containerElementId',
			config: {
				'type': '__legacy-embed__',
				'legacyEmbedUrl': '/$legacyEmbedUrl$urlParams'
			}
		});
JS;
	echo $js;
}
else {
	// TODO: Add redirect logic to /Single/embed.js like normal
	// TODO: Fake the browser client/OS using curl
	
	echo "http://embed.newsinc.com/$legacyEmbedUrl$urlParams";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_URL, "http://embed.newsinc.com/$legacyEmbedUrl$urlParams");
	echo curl_exec($ch);
	
}