<?php 
// echo $_GET['url']; exit;

if (isset($_GET['url'])) {
	$url = 'http://' . $_GET['url'];
	$pathInfo = pathinfo($url);
	
	switch ($pathInfo['extension']) {
		case 'png': header('Content-type:image/png'); break;
		case 'jpeg':
		case 'jpg': header('Content-type:image/jpeg'); break;
		case 'css': header('Content-type:text/css'); break;
	}
	
	echo file_get_contents($url);
}