In order to minify this repository's JavaScript files, make sure you have nodejs installed. 
Navigate to the repository's folder via command-line and execute the following command:

node ./frontend_shared/js/libs/requirejs/r.js -o ./_build/app.build.js

If you have node and bash scripting installed (within your "/bin/bash" folder, ie. you're running
Mac OS X on your computer), you may execute the a bash script to do this work for you in addition 
to removing the unneeded folders for deployment. You must first make sure the "/_build/build.sh"
file is executable (ie. you can execute "chmod +x ./_build/build.sh" within this repository's root
folder). Then execute the following code (while still within the repository's root folder):

./_build/build.sh

Executing either of the two commands above will create a copy of the current repository inside the
folder located at "../Build/<repository-name>" relative to the repository's current location.




For the build process, install "node" and "npm". 

Visit: http://nodejs.org/
Install "node" and "npm"
Install npm modules: "recess", "aws-sdk", and "optimist" via the following commands on command line:
npm install recess;
npm install aws-sdk;
npm install optimist;

Now that the installation is complete, navigate to the "/.build" folder within this git repository:
cd .build;

Then execute the following:

./build.js --accessKey [AWS ACCESS KEY] --secretKey [AWS SECRET KEY] --bucket dev.assets.newsinc.com --version [VERSION STRING] --basePath [BASE PATH (optional)]
that will build and push to [bucket]/[basePath]/[version]
basePath defaults to "player20" (just basically a placeholder I put in there)


