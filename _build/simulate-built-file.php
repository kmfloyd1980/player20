<?php 

$appUrlWithoutProtocol = (isset($_GET['appUrl']) ? $_GET['appUrl'] : $_SERVER['HTTP_HOST']);
$appUrl =  'http://' . $appUrlWithoutProtocol;

if (preg_match('%^/5/([^?]*)%ims', $_SERVER['REQUEST_URI'], $matches)) {
	$appPath = dirname(__DIR__);
	$fileName = $matches[1] ? $matches[1] : 'landing-page.html';
	$filePath = $appPath . '/' . $fileName;
	$filePathInfo = pathinfo($filePath);

	$mimeTypes = array(
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',
		'png' => 'image/png',
		'js' => 'application/javascript'
	);
	
	if (isset($mimeTypes[$filePathInfo['extension']])) {
		header('Content-type: ' . $mimeTypes[$filePathInfo['extension']]);
	}
	
	// If this is the landing page or another page with a ".html" extension
	if ($matches[1] == '' || in_array($filePathInfo['extension'], array('html', 'htm'))) {
		// Replace all instances of the <script> tags used to include the NDN Player Suite library in development with a simulated "built" environment
		echo preg_replace_callback(
			"|<\s*script\b.*src=['\"](?'src'[^'\"]*)['\"][^>]*[^<]*</script>|iUms",
			function ($matches) use ($appUrl) {
				if (preg_match('|/js/require.js$|iUms', $matches['src'])) {
					// Get rid of the <script> tag pointing to "/js/require.js"
					return '';
				}
				else if (preg_match('|/js/embed.js|iUms', $matches['src'])) {
					return '<script type="text/javascript" src="' . $appUrl . '/5/js/embed.js" id="_nw2e-js"></script>'; // '/js/embed.js.php?baseUrl=' . $appUrl . '" id="_nw2e-js"></script>';
					// return '<script type="text/javascript" src="' . $appUrl . '/js/embed.js.php?baseUrl=' . $appUrl . '" id="_nw2e-js"></script>'; // '/js/embed.js.php?baseUrl=' . $appUrl . '" id="_nw2e-js"></script>';
				}
				else {
					return $matches[0];
				}
			},
			file_get_contents($filePath)
		);
	}
	else {
		if ($matches[1] == 'js/dynamic.js') {
			echo preg_replace('|\blaunch\.newsinc\.com\b|iUms', $appUrlWithoutProtocol, file_get_contents("$appPath/{$matches[1]}"));
		}
		else {
			if ($matches[1] == 'js/embed.js') {
				echo file_get_contents("$appPath/js/require.js");
				echo "\nvar _nw2e = _nw2e || [];\n_nw2e.appUrl = '" . $appUrl . "';\n";
			}
			
			// Output the file's contents
			echo file_get_contents($filePath);
		}
		
	}
}
