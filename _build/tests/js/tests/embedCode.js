(function() {
	var getEmbedContainerId = (function() {
		var containerId = 1;
		return function() {
			return 'my-test-player-' + (containerId++);
		};
	})();

	module("Embed Code Tests", {
		setup: function() {
			QUnit.stop();

			_nw2e.push(['when', 'initialized', function() {
				QUnit.start();
				
				// Run tests synchronously
				_nw2e.jQuery.ajaxSetup({
					async: false
				});
			}]);
			
		},
		teardown: function() {
			_nw2e.push(['when', 'initialized', function() {
				_nw2e.jQuery.mockjaxClear();
			}]);
		}
	});

	test('Embed Single Player', function() {
		QUnit.stop();
		
		$('#qunit-fixture').append(
			'<div id="my-test-player" class="ndn_embed" style="width:590px;height:332px;" data-config-widget-id="1" data-config-type="VideoPlayer/Single" data-config-tracking-group="10557" data-config-video-id="24887508" data-config-site-section="site_section_id"></div>'
		);
		
		_nw2e.Ndn_Widget.embedWidgetsFromContainersOnPage();
		
		_nw2e.push(['hook', 'my-test-player/configComplete', function(event) {
			// console.log('configComplete, config ==', event.data);
			
			ok($('#qunit-fixture').find('#my-test-player').hasClass('ndn_embedded'), "Embedded player!");
			
			QUnit.start();
		}]);
	});

	/*
	(function() {
		var configs = [
	       		{
	    			widgetId: '1',
	    			type: 'VideoPlayer/Single',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id',
	    			width: '590',
	    			height: '332'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoPlayer/16x9',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoPlayer/Inline300',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '2',
	    			type: 'VideoPlayer/Inline300',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoPlayer/Inline590',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoLauncher/Slider300x250',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoLauncher/Playlist300x250',
	    			trackingGroup: '10557',
	    			siteSection: 'site_section_id'
	    		},
	    		{
	    			widgetId: '1',
	    			type: 'VideoLauncher/VerticalTower',
	    			trackingGroup: '34567',
	    			siteSection: 'site_section_id'
	    		}
		];
		
		$.each(configs, function() {
			var config = this;
			var type = config.type;
			
			// Generate test URL to isolate/debug a player's unit test
			var testUrl = (function() {
				var result = '/?';
				
				var segments = [];
				$.each(config, function(index) {
					var configSetting = index,
						configValue = this;
					
					segments.push(configSetting + '=' + encodeURIComponent(configValue));
				});
				
				return result + segments.join('&');
			})();
			
			test('Embed Player: ' + type, function() {
				// console.log('Test URL: http://local.newsinc.com' + testUrl);
				var testsToExpect = 0;
				
				QUnit.stop();

				var embedContainerId = getEmbedContainerId();
				var configurableDiv = $('<div id="' + embedContainerId + '" class="ndn_embed" style="width:590px;height:332px;"></div>'); // data-config-widget-id="1" data-config-type="' + type + '" data-config-tracking-group="10557"></div>');
				
				// Add the appropriate dom attribute
				$.each(config, function(index) {
					var configValue = this;
					var attributeName = 'data-config-' + (index + '').replace(/([A-Z])/g, function(match, p1) {
						return '-' + p1.toLowerCase();
					});
					configurableDiv.attr(attributeName, configValue);
				});
				
				// console.log('configurableDiv ==', configurableDiv.get(0));
				
				$('#qunit-fixture').html('').append(configurableDiv);
				
				// console.log('embedContainerId ==', embedContainerId);
				// console.log("$('#" + embedContainerId + "') ==", $('#' + embedContainerId));
				
				testsToExpect++;
				_nw2e.push(['hook', embedContainerId + '/configComplete', function(event) {
					// console.log(embedContainerId + '/configComplete');
					
					ok($(event.containerElement).hasClass('ndn_embedded'), "Embedded player of type \"" + event.data.type + '"');
					
					QUnit.start();
				}]);
				
				// If an RMM should be played, listen and make sure that it will
				if (config.widgetId == '2' && (config.type == 'VideoPlayer/Inline300' || config.type == 'VideoPlayer/Inline590')) {
					testsToExpect++;
					QUnit.stop();
					_nw2e.push(['hook', embedContainerId + '/videoLoad', function(event) {
						console.log(embedContainerId + '/videoLoad', event);
						
						ok(true, 'Detected expected "videoLoad" event');
						ok(event.data.params.mediaAnalyticsC5Value == '02', "RMM plays");
						
						QUnit.start();
					}]);
				}
				
				// If using the trackingGroup config setting that ensures ads will play, test that ads do in fact begin
				if (config.trackingGroup == '34567') {
					// TODO: Add tests for detecting ads
				}

				_nw2e.Ndn_Widget.embedWidgetsFromContainersOnPage();
				
				// Indicate to the unit test driver, how many tests are expected
				// expect(testsToExpect);
			});
		});
	})();
	*/
})();