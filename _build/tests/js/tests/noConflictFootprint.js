(function() {
	module('No Conflict Footprint');
	
	test('jQuery', function() {
		ok(jQuery.fn.jquery != _nw2e.jQuery.fn.jquery, "External jQuery library is not overridden by NDN Player Suite");
	});
	
	test('requirejs', function() {
		ok(typeof requirejs == 'undefined', 'No "requirejs" global variable');
		ok(typeof require == 'undefined', 'No "require" global variable');
		ok(typeof define == 'undefined', 'No "define" global variable');
	});
})();