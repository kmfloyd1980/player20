(function() {
	module('Internal Libraries');
	
	test('Ndn_Utils_UrlParser', function() {
		QUnit.stop();
	
		_nw2e.push(['when', 'initialized', function() {
			_nw2e.require([
					'models/Ndn/Utils/UrlParser',
				],
				function(Ndn_Utils_UrlParser) {
					var testUrl = 'http://launch.newsinc.com/?trackingGroup=34567&ndn-testParam1=hello7&ndn.trackingGroup=99999&emptyParam=';
					var parseResult = Ndn_Utils_UrlParser.parseHref(testUrl);
					
					ok(parseResult.url == 'http://launch.newsinc.com/', "Parsed URL without query string");
					ok(parseResult.queryParams.trackingGroup == '34567', "Parsed typical parameter with no special characters");
					ok(parseResult.queryParams['ndn-testParam1'] == 'hello7', 'Parsed parameter name with "-" character');
					ok(parseResult.queryParams['ndn.trackingGroup'] == '99999', 'Parsed parameter name with "." character ');
					
					testUrl = 'http://local.newsinc.com/?type=VideoPlayer/16x9&widgetId=3233&trackingGroup=34567&playlistId=10205&videoId=24997697&embedOriginUrl=http%3A%2F%2Flocal.newsinc.com%2F5%2Ftests&emptyParam=';
					parseResult = Ndn_Utils_UrlParser.parseHref(testUrl);
					
					ok(parseResult.queryParams.type == 'VideoPlayer/16x9', "Parsed parameter value with \"/\" character");
					ok(parseResult.queryParams.emptyParam == '', 'Parsed parameter with empty value');
					ok(parseResult.queryParams.embedOriginUrl == 'http://local.newsinc.com/5/tests', "Parsed URL encoded parameter value");
				}
			);
			
			QUnit.start();
		}]);
	});
})();
