(function() {
	module('Ndn_Utils_AspectRatio');
	
	test('Maintains aspect ratio', function() {
		QUnit.stop();
	
		_nw2e.push(['when', 'initialized', function() {
			// Require the "/js/models/Ndn/Widget.js" file, which bundles the Ndn_Utils_AspectRatio module in with said compiled file
			_nw2e.require([
			    'models/Ndn/Widget'           
            ],
            function(Ndn_Widget) {
				_nw2e.require([
					'models/Ndn/Utils/AspectRatio'
				],
				function(
					Ndn_Utils_AspectRatio
				) {
					/**
					 * TODO: Document this structure.
					 */
					var testsData = [
						['6:5', '<div style="width:600px;"></div>', 'Evaluate 6:5 ratio with no pre-existing height', function(element) { return element.height() == '500'; }],
						['6:5', '<div style="width:600px;height:400px;"></div>', 'Evaluate 6:5 ratio with pre-existing height', function(element) { return element.height() == '500'; }],
						['3:2', '<div style="width:600px;height:300px;"></div>', 'Evaluate 3:2 ratio with pre-existing height', function(element) { return element.height() == '400'; }],
						['4:3', '<div style="height:600px;"></div>', 'Evaluate 4:3 ratio using the height as the input dimension', function(element) { return element.width() == '800'; }, 'height']
					];
					
					$.each(testsData, function() {
						var testData = this,
							testElement = $(testData[1]),
							testRatio = testData[0],
							testDescription = testData[2],
							testEvaluation = testData[3],
							testInputDimension = testData[4],
							testToggleClosure = testData[5];
						
						// Append the test element to the page for testing
						$('#qunit-fixture').append(testElement);
						
						// Maintain the aspect ratio specified on the specified test element
						Ndn_Utils_AspectRatio.maintain(testElement, testRatio, testToggleClosure, testInputDimension);
						
						// Use the evaluation logic provided to evaluate the result of this test
						ok(testEvaluation(testElement), testDescription);
					});
				});
				
				QUnit.start();
			});
		}]);
	});
})();