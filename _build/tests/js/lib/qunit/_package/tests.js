QUnit.config.testTimeout = 30000;

(function() {
	function includeEmbedJs() {
		var nw = document.createElement('script'); nw.type = 'text/javascript'; nw.src = '/5/js/embed.js'; nw.id = '_nw2e-js'; 
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(nw, s);
	}
	
	var getEmbedContainerId = (function() {
		var containerId = 1;
		return function() {
			return 'my-test-player-' + (containerId++);
		};
	})();
	
	// ****
	// TODO: Continue looking at answer at http://stackoverflow.com/questions/12074913/how-do-i-unit-test-this-javascript-function-including-mocking-of-ajax-call
	// ****
	module("Embed Code Tests", {
		setup: function() {
			// Run tests synchronously
			$.ajaxSetup({
				async: false
			});
		},
		teardown: function() {
			$.mockjaxClear();
		}
	});

	test('Embed Single Player', function() {
		QUnit.stop();
		
		$('#qunit-fixture').append(
			'<div id="my-test-player" class="ndn_embed" style="width:590px;height:332px;" data-config-widget-id="1" data-config-type="VideoPlayer/Single" data-config-tracking-group="10557" data-config-video-id="24887508" data-config-site-section="site_section_id"></div>'
		);
		
		_nw2e.Ndn_Widget.embedWidgetsFromContainersOnPage();
		
		_nw2e.push(['hook', 'my-test-player/configComplete', function(event) {
			// console.log('configComplete, config ==', event.data);
			
			ok($('#qunit-fixture').find('#my-test-player').hasClass('ndn_embedded'), "Embedded player!");
			
			QUnit.start();
		}]);
	});
	
	test('Embed 16x9 Player', function() {
		QUnit.stop();
		
		$('#qunit-fixture').append(
			'<div id="my-test-player1" class="ndn_embed" style="width:590px;height:332px;" data-config-widget-id="1" data-config-type="VideoPlayer/Default" data-config-tracking-group="10557" data-config-video-id="24887508" data-config-site-section="site_section_id"></div>'
		);
		
		_nw2e.Ndn_Widget.embedWidgetsFromContainersOnPage();
		
		_nw2e.push(['hook', 'my-test-player1/configComplete', function(event) {
			// console.log('configComplete, config ==', event.data);
			
			ok($('#qunit-fixture').find('#my-test-player').hasClass('ndn_embedded'), "Embedded player!");
			
			QUnit.start();
		}]);
	});
	
	/*
	QUnit.cases([
	    {type: 'VideoPlayer/Single'},
		{type: 'VideoPlayer/16x9'},
		{type: 'VideoPlayer/Inline300'},
		{type: 'VideoPlayer/Inline590'},
		{type: 'VideoLauncher/Slider300x250'},
		{type: 'VideoLauncher/Playlist300x250'},
		{type: 'VideoLauncher/Horizontal'},
		{type: 'VideoLauncher/VerticalTower'}
	])
	.test("Embed Test", function(params) {
		var embedContainerId = getEmbedContainerId();
		var siteSection = 'site_section_id';
		
		QUnit.stop();

		$('#qunit-fixture').html('').append(
			'<div id="' + embedContainerId + '" class="ndn_embed" style="width:590px;height:332px;" data-config-widget-id="1" data-config-type="' + params.type + '" data-config-tracking-group="10557" data-config-site-section="' + siteSection + '"></div>'
		);
		
		console.log('embedContainerId ==', embedContainerId);
		console.log("$('#" + embedContainerId + "') ==", $('#' + embedContainerId));
		
		_nw2e.push(['hook', embedContainerId + '/configComplete', function(event) {
			console.log(embedContainerId + '/configComplete');
			
			ok($(event.containerElement).hasClass('ndn_embedded'), "Embedded player of type \"" + event.data.type + '"');
			
			QUnit.start();
		}]);
	});
	
	/*
	(function() {
		var originalTestTimeout = QUnit.config.testTimeout;
		var widgetTypes = [
			'VideoPlayer/16x9',
			'VideoPlayer/Inline300',
			'VideoPlayer/Inline590',
			'VideoLauncher/Slider300x250', 
			'VideoLauncher/Playlist300x250', 
			'VideoLauncher/Horizontal',
			'VideoLauncher/VerticalTower'
		];

		// Give a decent amount of time for each of the tests about to be executed
		QUnit.config.testTimeout *= widgetTypes.length;
		
		test('Embed Products', function() {

			// The number of embed tests about to be executed
			var embedTests = widgetTypes.length;
			
			$.each(widgetTypes, function(index) {
				var type = this;
				var embedContainerId = getEmbedContainerId();
				var siteSection = 'site_section_id';
				
				QUnit.stop();

				$('#qunit-fixture').append(
					'<div id="' + embedContainerId + '" class="ndn_embed" style="width:590px;height:332px;" data-config-widget-id="1" data-config-type="' + type + '" data-config-tracking-group="10557" data-config-site-section="' + siteSection + '"></div>'
				);


				console.log('Listening for: ' + embedContainerId + '/configComplete');
				_nw2e.push(['hook', embedContainerId + '/configComplete', function(event) {
					console.log(embedContainerId + '/configComplete');
					
					ok($(event.containerElement).hasClass('ndn_embedded'), "Embedded player of type \"" + event.data.type + '"');
					
					if (--embedTests == 0) {
						QUnit.start();
					}
				}]);

			});
		});
		
		// Revert the test timeout to what it was originally
		QUnit.config.testTimeout = originalTestTimeout;
	})();
	*/
	
	module("JavaScript API");
	
	test('Hooks', function() {
		QUnit.stop();
		
		// console.log(">>>>>> $('#qunit-fixture').html() ==", $('#qunit-fixture').html());
		ok(1, '1 is true');
		QUnit.start();
	});
})();

