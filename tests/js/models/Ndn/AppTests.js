(function() {
    module('Ndn_App');
    
    test('Generates needed script tag for embed code', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'models/Ndn/App'
        ],
        function(
            $,
            Ndn_App
        ) {
            /**
             * Structured data concerning the tests to run on the method being tested. Format:
             * <pre>[ // List of test data concerning a particular config to pass to the method being tested
             *  [ // Structured data concerning a particular config and its assertions to apply to the results of passing said config to the method being tested
             *      Object, // The config to be passed into the method being tested
             *      Array<Function>, // List of functions to execute by passing the result of the config passed into the method being tested, these functions will have certain assertions to test the result
             *  ],
             *  ... // All structured data concerning to additional tests
             * ]</pre>
             * @var {Array<Array>}
             */
            var testsData = [
                 [
                     [ 
                         '',
                         false
                     ],
                     [
                         function(result) {
                             ok(result == '<script type="text/javascript" src="/js/require.js"></script><script type="text/javascript" src="/js/embed.js" id="_nw2e-js"></script>', 'Generates script tag for unbuilt environment');
                         }
                     ]
                 ],
                 [
                     [ 
                         'http://launch.newsinc.com/98',
                         true
                     ],
                     [
                         function(result) {
                             ok(result == '<script type="text/javascript" src="http://launch.newsinc.com/98/js/embed.js" id="_nw2e-js"></script>', 'Generates script tag for built environment');
                         }
                     ]
                 ],
            ];

            $.each(testsData, function() {
                var testData = this,
                    testParams = testData[0],
                    tests = testData[1];
                
                // Apply all provided tests for each of the provided test parameters to pass into the method being tested
                $.each(tests, function() {
                    this(Ndn_App.getEmbedCodeScriptTag.apply(Ndn_App, testParams));
                });
            });
            
            done();
        });
    });
})();