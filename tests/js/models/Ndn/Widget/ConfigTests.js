(function() {
    module('Ndn_Widget_Config');

    test('Cleans config', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'models/Ndn/Widget/Config'
        ],
        function(
            $,
            Ndn_Widget_Config
        ) {
            /**
             * Structured data concerning the tests to run on the method being tested. Format:
             * <pre>[ // List of test data concerning a particular config to pass to the method being tested
             *  [ // Structured data concerning a particular config and its assertions to apply to the results of passing said config to the method being tested
             *      Object, // The config to be passed into the method being tested
             *      Array<Function>, // List of functions to execute by passing the result of the config passed into the method being tested, these functions will have certain assertions to test the result
             *  ],
             *  ... // All structured data concerning to additional tests
             * ]</pre>
             * @var {Array<Array>}
             */
            var testsData = [
                 [
                     {
                         'pb': ' 1',
                         'type': 'videoplayer/Single',
                         'trackingGroup': "\n\t33333 "
                     },
                     [
                         function(result) {
                             ok(result.pb === 1, 'Parsed string value with leading whitespace of a config setting that is supposed to have an integer value');
                             ok(result.type == 'VideoPlayer/Default', 'Parsed "type" config setting with differing case than expected');
                             ok(result.trackingGroup === '33333', 'Trimmed all whitespace around string config setting');
                         }
                     ]
                 ],
                 [
                     {
                         'notAConfigSetting': '1',
                         'type': 'videoplayer/Default',
                         'width': '500',
                         'height': '9/16w',
                         'acceptsConfigFromUrl': '',
                         'autoPlay': 'true',
                         'playOnMouseover': true,
                         'adsEnabled': 'false'
                     },
                     [
                         function(result) {
                             ok(typeof result.notAConfigSetting == 'undefined', 'Unrecognized config setting removed from config');
                             ok(result.autoPlay === true, 'Boolean config setting converted to actual boolean value (true) from string');
                             ok(result.adsEnabled === false, 'Boolean config setting converted to actual boolean value (false) from string');
                             ok(result.playOnMouseover === true, 'Boolean config setting remained actual boolean value');
                             ok(result.acceptsConfigFromUrl === true, 'Boolean config setting that points to empty string evaluates to true');
                         }
                     ]
                 ],
                 [
                     {
                         'type': 'videoplayer/NotAPlayer',
                         'width': '500',
                         'height': '9/16w'
                     },
                     [
                         function(result) {
                             ok(typeof result.type == 'undefined', 'Removed unrecognized "type" config setting value');
                         }
                     ]
                 ]
            ];
            
            $.each(testsData, function() {
                var testData = this,
                    config = testData[0],
                    resultTests = testData[1];
                
                // Apply all provided tests for each of the provided config objects to pass into the method being tested
                $.each(resultTests, function() {
                    this(Ndn_Widget_Config.cleanConfig(config));
                });
            });
            
            done();
        });
    });
    
    test('Detects updated embed code', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'models/Ndn/Widget/Config'
        ],
        function(
            $,
            Ndn_Widget_Config
        ) {

            /**
             * Structured data concerning the tests to run on the method being tested. Format:
             * <pre>[ // List of test data concerning a particular config to pass to the method being tested
             *  [ // Structured data concerning a particular config and its assertions to apply to the results of passing said config to the method being tested
             *      Object, // The config to be passed into the method being tested
             *      Array<Function>, // List of functions to execute by passing the result of the config passed into the method being tested, these functions will have certain assertions to test the result
             *  ],
             *  ... // All structured data concerning to additional tests
             * ]</pre>
             * @var {Array<Array>}
             */
            var testsData = [
                 [
                     '<div class="ndn_embed" data-config-pb="4" data-type="VideoPlayer/Single"></div>',
                     [
                         function(result) {
                             ok(!result, 'Detected data-config-* attributes indicating that the updated embed code is not being used');
                         }
                     ]
                 ],
                 [
                     '<div id="my-test-player-1" class="ndn_embed" data-pb="4"></div>',
                     [
                         function(result) {
                             ok(result, 'Recognized updated embed code with only data-* attributes');
                         }
                     ]
                 ]
            ];
            
            $.each(testsData, function() {
                var testData = this,
                    embedCodeHtml = testData[0],
                    resultTests = testData[1];
                
                // Apply all provided tests for each of the provided test parameters to pass into the method being tested
                $.each(resultTests, function() {
                    this(Ndn_Widget_Config.hasUpdatedEmbedCode($(embedCodeHtml)));
                });
            });
            
            done();
        });
    });
    
    test('Process default dimensions', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'models/Ndn/Widget/Config'
        ],
        function(
            $,
            Ndn_Widget_Config
        ) {

            /**
             * Structured data concerning the tests to run on the method being tested. Format:
             * <pre>[ // List of test data concerning a particular config to pass to the method being tested
             *  [ // Structured data concerning a particular config and its assertions to apply to the results of passing said config to the method being tested
             *      Array, // The parameters to be passed into the method being tested
             *      Array<Function>, // List of functions to execute by passing the result of the config passed into the method being tested, these functions will have certain assertions to test the result
             *  ],
             *  ... // All structured data concerning to additional tests
             * ]</pre>
             * @var {Array<Array>}
             */
            var testsData = [
                 [
                     [
                         {
                             'type': 'VideoPlayer/Default',
                             'aspectRatio': '4:3'
                         },
                         true
                     ],
                     [
                         function(result) {
                             ok(result.width == '100%', 'Set "width" config setting to "100%" when "aspectRatio" config setting is present but neither "width" nor "height" config settings are');
                             ok(result.height == '3/4w', 'Set "height" config setting to the appropriate formula based on the "width" config setting');
                         }
                     ]
                 ],
                 [
                     [
                         {
                             'type': 'VideoPlayer/Default'
                         },
                         false
                     ],
                     [
                         function(result) {
                             ok(typeof result.aspectRatio == 'undefined', 'The "aspectRatio" config setting is not given a default value for non-updated embed code');
                         }
                     ]
                 ],
                 [
                     [
                         {
                             'type': 'VideoPlayer/Default'
                         }
                     ],
                     [
                         function(result) {
                             ok(typeof result.aspectRatio == 'undefined', 'The second parameter defaults to false when not provided');
                         }
                     ]
                 ],
                 [
                     [
                         {
                             'type': 'VideoPlayer/Default'
                         },
                         true
                     ],
                     [
                         function(result) {
                             ok(result.aspectRatio == '16:9', 'The "aspectRatio" config setting defaults to "16:9" when it does not exist, the updated embed code is being used, and neither the "width" nor "height" config settings are provided');
                         }
                     ]
                 ],
                 [
                     [
                         {
                             'type': 'VideoPlayer/Default',
                             'width': '600',
                             'height': '400'
                         },
                         true
                     ],
                     [
                         function(result) {
                             ok(typeof result.aspectRatio == 'undefined', 'The "aspectRatio" config setting does not default to anything when both the "width" or "height" config settings are provided');
                         }
                     ]
                 ]
            ];
            
            $.each(testsData, function() {
                var testData = this,
                    testParams = testData[0],
                    tests = testData[1];
                
                // Apply all provided tests for each of the provided test parameters to pass into the method being tested
                $.each(tests, function() {
                    this(Ndn_Widget_Config.processDefaultDimensions.apply(Ndn_Widget_Config, testParams));
                });
            });
            
            done();
        });
    });
    
    test('Get embed code from provided config', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'models/Ndn/Widget/Config'
        ],
        function(
            $,
            Ndn_Widget_Config
        ) {
            /**
             * Structured data concerning the tests to run on the method being tested. Format:
             * <pre>[ // List of test data concerning a particular config to pass to the method being tested
             *  [ // Structured data concerning a particular config and its assertions to apply to the results of passing said config to the method being tested
             *      Object, // The config to be passed into the method being tested
             *      Array<Function>, // List of functions to execute by passing the result of the config passed into the method being tested, these functions will have certain assertions to test the result
             *  ],
             *  ... // All structured data concerning to additional tests
             * ]</pre>
             * @var {Array<Array>}
             */
            var testsData = [
                 [
                     [ 
                         {
                             'pb': 1,
                             'type': 'VideoPlayer/Single',
                             'trackingGroup': "33333"
                         }
                     ],
                     [
                         function(result) {
                             ok(result == '<div class="ndn_embed" data-config-pb="1" data-config-type="VideoPlayer/Single" data-config-tracking-group="33333"></div>', 'Embed code created from provided config');
                         }
                     ]
                 ],
                 [
                     [ 
                         {
                             'pb': 1,
                             'type': 'VideoPlayer/Single',
                             'trackingGroup': "33333"
                         },
                         true
                     ],
                     [
                         function(result) {
                             ok(result == '<div class="ndn_embed" data-pb="1" data-type="VideoPlayer/Single" data-tracking-group="33333"></div>', 'Updated embed code created from provided config');
                         }
                     ]
                 ]
            ];

            $.each(testsData, function() {
                var testData = this,
                    testParams = testData[0],
                    tests = testData[1];
                
                // Apply all provided tests for each of the provided test parameters to pass into the method being tested
                $.each(tests, function() {
                    this(Ndn_Widget_Config.getEmbedCode.apply(Ndn_Widget_Config, testParams));
                });
            });
            
            done();
        });
    });
})();