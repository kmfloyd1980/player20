(function() {
    module('Ndn_EventHub');
    
    test('Widget Observation', function(assert) {
        var done = assert.async();
        
        require([
            'jquery',
            'backbone',
            'models/Ndn/EventHub'
        ],
        function(
            $,
            Backbone,
            Ndn_EventHub
        ) {
            var ObservableObject = Backbone.Model.extend({
                /**
                 * @param {Array} identifiers
                 */
                initialize: function(identifiers, observableObjectEventData) {
                    this.set({
                        'identifiers': identifiers,
                        'observableObjectEventData': observableObjectEventData
                    });
                },
                
                /**
                 * @return {Object} Structured data concerning the data available to the Ndn_EventHub module when it observes events triggered by this object. Format:
                 * <pre>{
                 *   data: Object, // Structured data that is passed each time via the Ndn_EventHub
                 *   identifiers: Array<String>, // List of identifiers to use when describing events passing through the Ndn_EventHub module
                 * }</pre>
                 */
                getObservableData: function() {
                    return {
                        data: this.get('observableObjectEventData'),
                        identifiers: this.get('identifiers')
                    };
                }
            });
            
            // Ensure that the Ndn_EventHub module is storing its event data
            Ndn_EventHub.toggleEventDataStorage(true);
            
            var observableObjectIdentifiers = [
               'my-test-1',
               '#4815'
            ];
            
            var observableObjectEventData = {
               sampleObservableObjectEventData: 'testing1',
               sampleObservableObjectEventData2: 'testing2'
            };
            
            var test1 = new ObservableObject(observableObjectIdentifiers, observableObjectEventData);
            
            Ndn_EventHub.observeWidget(test1);
            
            var eventData = {
                description: 'This is the first "videoLoad" event being triggered and passed into the "hook" event namespace.',
                id: 19
            };
            
            test1.trigger('hook:videoLoad', eventData);
            
            var receivedEventData = Ndn_EventHub.getEventData('hook:my-test-1/videoLoad[0]');
            
            ok(receivedEventData['public'].data == eventData, 'Event data is preserved');
            
            (function() {
                var allIdentifyingEventDataIsPresent = true;
                $.each(observableObjectEventData, function(index, value) {
                    if (receivedEventData['public'][index] !== value) {
                        allIdentifyingEventDataIsPresent = false;
                        return false;
                    }
                });
                
                ok(allIdentifyingEventDataIsPresent, 'All event data that identifies an observable object is present in received event data');
            })();
            
            // Make sure that unregistered event namespaces do not get their event data stored/processed by the Ndn_EventHub module
            (function() {
                // Trigger an event that should not get registered as "notanamespace" is not a registered event namespace within the Ndn_EventHub module
                test1.trigger('notanamespace:videoLoad', {id: 20});
                
                var receivedEventData = Ndn_EventHub.getEventData('notanamespace:my-test-1/videoLoad[0]');
                
                ok(!receivedEventData, 'Unregistered namespace does not store events within Ndn_EventHub module');
            })();
            
            // Make sure the observable object's identifier aliases work just like its primary observable object identifier
            deepEqual(Ndn_EventHub.getEventData('hook:#4815/videoLoad[0]'), receivedEventData, 'Observable object identifier alias returns same event object as when its associated primary observable object identifier is used');
            
            // Make sure that there is no "videoLoad" event with an event index of "1" when only 1 "videoLoad" event has been fired
            ok(typeof Ndn_EventHub.getEventData('hook:#4815/videoLoad[1]') == 'undefined', 'Unfired event is not registered');
            
            // Turn off event data storage
            Ndn_EventHub.toggleEventDataStorage(false);
            
            // Trigger another "videoLoad" event, which should not have its event data stored within the Ndn_EventHub module but instead should act like such event never happened at all
            test1.trigger('hook:videoLoad', eventData);
            
            // Make sure that there is no "videoLoad" event with an event index of "1" when only 1 "videoLoad" event has been fired (while the Ndn_EventHub module had event data storage enabled)
            ok(typeof Ndn_EventHub.getEventData('hook:#4815/videoLoad[1]') == 'undefined', "Fired event's data is not recorded while event data storage is disabled");
            
            // Turn event data storage back on
            Ndn_EventHub.toggleEventDataStorage(true);

            // Trigger another "videoLoad" event, which should not have its event data stored within the Ndn_EventHub module but instead should act like such event never happened at all
            test1.trigger('hook:videoLoad', eventData);

            // Make sure that there is no "videoLoad[1]" event is still not stored as it was fired while the Ndn_EventHub module had event data storage disabled
            ok(!Ndn_EventHub.getEventData('hook:#4815/videoLoad[1]'), "Event data is undefined when attempting to access event that was fired while event data storage was disabled");
            
            // Make sure that there is "videoLoad[2]" event data available as it was just fired after event data storage was re-enabled
            ok(Ndn_EventHub.getEventData('hook:#4815/videoLoad[2]'), "Expected event data available after firing an event where event data storage was re-enabled");
            
            done();
        });
    });
})();
