(function() {
    module('Ndn_Timer');
    
    test('Waits Correctly', function(assert) {
        var doneLoadingModule = assert.async();
        
        require([
           'models/Ndn/Timer'
        ],
        function(
            Ndn_Timer
        ) {
            var done = assert.async(),
                startTime = Date.now(),
                timer = new Ndn_Timer(function() {
                    var waitTime = Date.now() - startTime;
                    ok((waitTime >= 100 && waitTime < 200), 'Callback wait time is correct: ' + waitTime);
                    done();
                }, 100, 1);
            
            timer.start();
            
            doneLoadingModule();
        });
    });
    
    test('Iterates Correctly', function(assert) {
        var doneLoadingModule = assert.async();
        
        require([
           'models/Ndn/Timer',	
        ],
        function(Ndn_Timer) {
            assert.expect(4);
            
            var done = [assert.async(),assert.async(),assert.async(),assert.async()];
            var count = 0;
            var timer = new Ndn_Timer(function() {
                assert.ok(true, 'Iteration ' + (count+1) + ' of 4 called correctly');
                done[count++]();
            }, 100, 4);
            timer.start();
            
            doneLoadingModule();
        });
    });
    
    /* 
    test('Set Number of Times to Execute', function(assert) {
        QUnit.stop();
        
        require([
           'models/Ndn/Timer',	
        ],
        function(
            Ndn_Timer
        ) {
            var done = [assert.async(),assert.async()],
            count = 0;
            var timer = new Ndn_Timer(function() {
                assert.ok(count == 0, 'Should be called only once');
                done[count++]();
            }, 100, 2);
            timer.setNumberOfTimesToExecute(1);
            timer.start();
        });
        
        QUnit.start();
    });
    */
})();
